        <?php echo form_open_multipart(base_url('customers/getAQuotePost/'.$quoteData->id),array('class'=>'request-form', 'onsubmit'=>'return validateQuoteDraftForm()')); ?>
            <div class="row">
                <div class="col-12 col-xl-6 col-lg-6">
                    <div class="form-group">
                        <label class="label">Description</label>
                        <input type="text" name="title" id="quatetitle" value="<?= $quoteData->title; ?>" onkeyup="errorremove('title-quote')" class="form-control" />
                        <p style="color: red;" class="title-quote"></p>
                    </div>
                </div>
                <div class="col-12 col-xl-6 col-lg-6">
                    <div class="form-group">
                        <label class="label">Choose Your Location</label>
                        <select name="locationid" class="form-control chosen-select">
                            <option value=""><option>
                            <?php
                                if($city_result)
                                {
                                    foreach($city_result as $rrr)
                                    {
                                        ?>
                                            <option value="<?= $rrr->id; ?>" <?= ($rrr->id == $quoteData->locationid)?"selected":""; ?>><?= $rrr->name; ?></option>
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
				<div class="col-12 col-xl-4 col-lg-4" style="display:none;">
                    <div class="form-group">
                        <label class="label">Address</label>
						<textarea name="address" maxlength="35" class="form-control" ><?= $quoteData->address; ?></textarea>
                    </div>
                </div>
            </div>
            
            <?php
                $categoryArr = explode(',',$quoteData->categoryid);
                $categorySubArr = explode(',',$quoteData->subcategoryid);
                $f = 1;
                if($categoryArr)
                {
                    for($k=0; $k < count($categoryArr); $k++)
                    {
                        if($f == 1)
                        {
                            ?>
                                <div class="row">
                                    <div class="col-12 col-xl-6 col-lg-6">
                                        <div class="form-group">
                                            <label class="label">Choose Car Category</label>
                                            <select class="form-control chosen-select" name="categoryid[]" onchange="getSubCategorys(this.value);">
                                                <?php
                                                    if($category_result)
                                                    {
                                                        foreach($category_result as $res)
                                                        {
                                                            ?>
                                                                <option value="<?= $res->id; ?>" <?= ($res->id == $categoryArr[$k])?"selected":""; ?>><?= $res->name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    
                                                    $subcategoryArr = $this->db->get_where(db_prefix().'category', array('parent_id' => $categoryArr[$k]))->result();
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-xl-5 col-lg-5">
                                        <div class="form-group">
                                            <label class="label">Choose Service Sub Category</label>
                                            <select class="form-control chosen-select" name="subcategoryid[]" id="sub_category_ids">
                                                <?php
                                                    if($subcategoryArr)
                                                    {
                                                        foreach($subcategoryArr as $rr)
                                                        {
                                                            ?>
                                                                <option value="<?= $rr->id ?>" <?= ($rr->id == $categorySubArr[$k])?"selected":""; ?>><?= $rr->name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-xl-1 col-lg-1">
                                        <div class="form-group">
                                            <label class="label">&nbsp;</label>
                                            <span class="btn btn-info" style="cursor:pointer" onclick="addMoreCatSubCatr()"><i class="fa fa-plus"></i></span>
                                        </div>
                                    </div>
                                </div>
                            <?php
                        }
                        else
                        {
                            ?>
                                <div class="row" id="removecats_<?= $f; ?>">
                                    <div class="col-12 col-xl-6 col-lg-6">
                                        <div class="form-group">
                                            <label class="label">Choose Car Category</label>
                                            <select class="form-control chosen-select" name="categoryid[]" onchange="getSubCategorys(this.value);">
                                                <?php
                                                    if($category_result)
                                                    {
                                                        foreach($category_result as $res)
                                                        {
                                                            ?>
                                                                <option value="<?= $res->id; ?>" <?= ($res->id == $categoryArr[$k])?"selected":""; ?>><?= $res->name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    
                                                    $subcategoryArr = $this->db->get_where(db_prefix().'category', array('parent_id' => $categoryArr[$k]))->result();
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-xl-5 col-lg-5">
                                        <div class="form-group">
                                            <label class="label">Choose Service Sub Category</label>
                                            <select class="form-control chosen-select" name="subcategoryid[]" id="sub_category_ids">
                                                <?php
                                                    if($subcategoryArr)
                                                    {
                                                        foreach($subcategoryArr as $rr)
                                                        {
                                                            ?>
                                                                <option value="<?= $rr->id ?>" <?= ($rr->id == $categorySubArr[$k])?"selected":""; ?>><?= $rr->name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-xl-1 col-lg-1">
                                        <div class="form-group">
                                            <label class="label">&nbsp;</label>
                                            <span class="btn btn-warning" style="cursor:pointer" onclick="removeMoreCatSubCatr('<?= $f ?>')"><i class="fa fa-trash"></i></span>
                                        </div>
                                    </div>
                                </div>
                            <?php
                        }
                        $f++;
                    }
                }
            ?>
            </div>
            <div class="addmorecatr">
            </div>
            <div class="row">
                <div class="col-12 col-xl-6 col-lg-6">
                    <div class="form-group">
                        <label class="label">Choose Vehicle Category</label>
                        <select name="vehiclecategory" class="form-control chosen-select">
                            <option value="Sedan" <?= ($quoteData->vehiclecategory == 'Sedan')?"selected":""; ?>>Sedan</option>
                            <option value="SUV" <?= ($quoteData->vehiclecategory == 'SUV')?"selected":""; ?>>SUV</option> 
                            <option value="Truck" <?= ($quoteData->vehiclecategory == 'Truck')?"selected":""; ?>>Truck</option> 
                        </select>
                    </div>
                </div>
                <div class="col-12 col-xl-6 col-lg-6">
                    <div class="form-group">
                        <label class="label">Choose Car Brand</label>
                        <select class="form-control chosen-select" id="make_ids" name="brandid" onChange="getModelLists(this.value)">
                            <option value=""></option>
                            <?php
                                if($carmake_result)
                                {
                                    foreach($carmake_result as $res)
                                    {
                                        ?>
                                            <option value="<?= $res->id; ?>" <?= ($res->id == $quoteData->brandid)?"selected":""; ?>><?= $res->name; ?></option>
                                        <?php
                                    }
                                }
                                $modellist = $this->db->get_where(db_prefix().'car_model', array('make_id' => $quoteData->brandid))->result();
                                $yearlist = $this->db->get_where(db_prefix().'car_year', array('make_id' => $quoteData->brandid, 'model_id' => $quoteData->carmodelid))->result();
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-xl-6 col-lg-6">
                    <div class="form-group">
                        <label class="label">Choose Car Model</label>
                        <select class="form-control chosen-select" name="carmodelid" id="model_ids" onChange="getYearLists(this.value)">
                            <?php
                                if($modellist)
                                {
                                    foreach($modellist as $rr)
                                    {
                                        ?>
                                            <option value="<?= $rr->id ?>" <?= ($rr->name == $quoteData->carmodelid)?"selected":""; ?>><?= $rr->name; ?></option>
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-12 col-xl-6 col-lg-6">
                    <div class="form-group">
                        <label class="label">Choose Car Make Year</label>
                        <select class="form-control chosen-select" name="carmakeyearid" id="year_ids">
                            <?php
                                if($yearlist)
                                {
                                    foreach($yearlist as $r)
                                    {
                                        ?>
                                           <option value="<?= $r->id ?>" <?= ($r->year == $quoteData->carmakeyearid)?"selected":""; ?>><?= $r->year; ?></option>                                         
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6 col-xl-6 col-lg-6">
                    <div class="form-group">
                        <label class="label">Upload Vehicle Registration Card<sup style="color: red;">*</sup></label>
                        <div class="bg-white mb-20">
                            <input type="file" class="imgInp" name="registrationcard" id="file-upload-quote">
                            <input style="display:none;" type="hide" id="oldfilename" value="<?= $quoteData->registrationcard; ?>">
                            <!--<label for="file-upload" class="file-upload mb-0">Browse</label>-->
                            <div id="file-upload-filename"></div>
                        </div>
                        <img id="blah" height="100px" width="100px" src="<?= base_url('uploads/quote/'.$quoteData->registrationcard); ?>" alt="" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';" />
                    </div>
                </div>
                <div class="col-6 col-xl-6 col-lg-6">
                    <div class="form-group">
                        <label class="label">Chassis No<sup style="color: red;">*</sup></label>
                        <input type="text" class="form-control" maxlength="25" value="<?= $quoteData->chassis_no; ?>" onkeyup="errorremove('chassis_no-quote')" name="chassis_no" id="chassis_no-quote" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-xl-12 col-lg-12">
                    <div class="form-group">
                        <label class="label">Detailed Description</label>
                        <textarea class="form-control" rows="4" name="description" placeholder="Detailed description here"><?= $quoteData->description; ?></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-xl-12 col-lg-12">
                    <div class="form-group">
                        <label class="label">Upload photo / audio / video<b class="text-info"><sub>Note: allow extension ( "jpg", "jpeg", "gif", "png", "mp3", "mp4", "wma" ) </sub></span></b></label>
                        <div class="bg-white mb-20">
                            <input type="file" name="upload_photo_video" id="file-upload1">
                            <!--<label for="file-upload1" class="file-upload mb-0">Browse</label>-->
                            <div id="file-upload1-filename"></div>
                        </div>
                        <p><?= $quoteData->upload_photo_video; ?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-xl-12 col-lg-12">
                    <div class="main-btn-wrap">
                        <button type="submit" name="submit" value="submit" class="btn main-btn uppercase mt-2">Get Quote</button>
                        <button type="submit" name="submit" value="save" class="btn main-btn uppercase mt-2">Save and Continue Later</button>
                        <a href="<?= base_url(); ?>" type="button" class="btn main-btn uppercase mt-2">Cancel and Go To Home Page</a>
                    </div>
                </div>
                <div class="col-12 col-xl-12 col-lg-12">
                    <p style="color: red;" class="title-quote"></p>
                    <p style="color: red;" class="chassis_no-quote"></p>
                </div>
            </div>
        </form>
        <script>
            function getSubCategorys(Id)
            {
                $('#sub_category_ids').html('<option value="">Please wait....</option>');
                var str = "catid="+Id+"&"+csrfData['token_name']+"="+csrfData['hash'];
        	    $.ajax({
        	        url: '<?= site_url()?>customers/getSubCategory',
        	        type: 'POST',
        	        data: str,
        	        dataType: 'json',
        	        cache: false,
        	        success: function(resp){
        	            $resp = '<option value=""></option>';
                        for(var i=0; i< resp.length; i++)
                        {
                            $resp += '<option value="'+resp[i].id+'">'+resp[i].name+'</option>';
                        }
                        $('#sub_category_ids').html($resp);
                        setTimeout(function(){  
                            $('#sub_category_ids').chosen('destroy'); 
                            $('#sub_category_ids').chosen(); 
                        }, 1000);
        	        }
        	    });
            }
            function getModelLists(Id)
            {
                $('#year_ids').html('<option value=""></option>');
                var str = "catid="+Id+"&"+csrfData['token_name']+"="+csrfData['hash'];
        	    $.ajax({
        	        url: '<?= site_url()?>customers/getModelList',
        	        type: 'POST',
        	        data: str,
        	        dataType: 'json',
        	        cache: false,
        	        success: function(resp){
        	            console.log(resp);
        	            $resp = '<option value=""></option>';
                        for(var i=0; i< resp.length; i++)
                        {
                            $resp += '<option value="'+resp[i].id+'">'+resp[i].name+'</option>';
                        }
                        $('#model_ids').html($resp);
                        setTimeout(function(){  
                            $('#model_ids').chosen('destroy'); 
                            $('#model_ids').chosen(); 
                        }, 1000);
                        // $('.chosen-select').chosen({}).change( function(obj, result) {});
        	        }
        	    });
            }
            
            /* getYearList */
            function getYearLists(Id)
            {
                var  values = $("#model_ids").chosen().val();
                var make_id = $( "#make_ids option:selected" ).val();
                var str = "catid="+values+"&make_id="+make_id+"&"+csrfData['token_name']+"="+csrfData['hash'];
        	    $.ajax({
        	        url: '<?= site_url()?>customers/getYearList',
        	        type: 'POST',
        	        data: str,
        	        dataType: 'json',
        	        cache: false,
        	        success: function(resp){
        	            console.log(resp);
        	            $resp = '<option value=""></option>';
                        for(var i=0; i< resp.length; i++)
                        {
                            $resp += '<option value="'+resp[i].id+'">'+resp[i].year+'</option>';
                        }
                        $('#year_ids').html($resp);
                        setTimeout(function(){  
                            $('#year_ids').chosen('destroy'); 
                            $('#year_ids').chosen(); 
                        }, 1000);
        	        }
        	    });
            }
        </script>
        <script>
            function getSubCategoryExtrar(Id,sn)
            {
                $('#sub_category_idr'+sn).html('<option value=""></option>');
                var str = "catid="+Id+"&"+csrfData['token_name']+"="+csrfData['hash'];
        	    $.ajax({
        	        url: '<?= site_url()?>customers/getSubCategory',
        	        type: 'POST',
        	        data: str,
        	        dataType: 'json',
        	        cache: false,
        	        success: function(resp){
        	            $resp = '<option value=""></option>';
                        for(var i=0; i< resp.length; i++)
                        {
                            $resp += '<option value="'+resp[i].id+'">'+resp[i].name+'</option>';
                        }
                        $('#sub_category_idr'+sn).html($resp);
                        setTimeout(function(){  
                            $('#sub_category_idr'+sn).chosen('destroy'); 
                            $('#sub_category_idr'+sn).chosen(); 
                        }, 1000);
        	        }
        	    });
            }
            
            var mcategoryr = '<?= count($categoryArr); ?>';
            function addMoreCatSubCatr()
            {
                mcategoryr += 1;
                var category_result = JSON.parse('<?= json_encode($category_result); ?>');
                var html = '';
                html += '<div class="row" id="removecats_'+mcategoryr+'">';
                html += '<div class="col-12 col-xl-6 col-lg-6">';
                html += '<div class="form-group">';
                html += '<label class="label">Choose Service Category</label>';
                html += '<select class="form-control chosen-select dclassr" name="categoryid[]" onchange="getSubCategoryExtrar(this.value,'+mcategoryr+');">';
                html += '<option value="">Select Categories<option>';
                for(var s=0; s < category_result.length; s++)
                {
                    html +='<option value="'+category_result[s].id+'">'+category_result[s].name+'</option>';
                }
                html += '</select>';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-12 col-xl-5 col-lg-5">';
                html += '<div class="form-group">';
                html += '<label class="label">Choose Service Sub Category</label>';
                html += '<select class="form-control" name="subcategoryid[]" id="sub_category_idr'+mcategoryr+'">';
                html += '<option value="">Select sub category<option>';
                html += '</select>';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-12 col-xl-1 col-lg-1">';
                html += '<div class="form-group">';
                html += '<label class="label">&nbsp;</label>';
                html += '<span class="btn btn-warning" style="cursor:pointer" onclick="removeMoreCatSubCatr('+mcategoryr+')"><i class="fa fa-trash"></i></span>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                
                setTimeout(function(){  
                    $('.dclassr').chosen('destroy'); 
                    $('.dclassr').chosen(); 
                }, 1000);
                $('.addmorecatr').append(html);
            }
            
            function removeMoreCatSubCatr(id)
            {
                $('#removecats_'+id).empty();
            }
        </script>