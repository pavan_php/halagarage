<?php include('include/header.php')?>
<?php include('include/navbar.php')?><!--Full Width Sider Start-->
<div class="breadcrumb-area" style="background-image: url('<?= base_url(); ?>assets/themes/immersive/images/about/about-us-banner-bg.png')">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner text-center">
                    <h1 class="page-title">Find a Car Part/ Service</h1>
                    <ul class="page-list">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li>Find a Car Part/ Service</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Breadcrumb End-->

<!--About Story Start-->
<section class="car-part padding-top-120 padding-bottom-110">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="input-group" id="adv-search">
                    <select class="chosen-select" id="filtercategory">
                        <option value="">Find a car part/ service category</option>
                        <?php
                            if($category_result)
                            {
                                foreach($category_result as $rrr)
                                {
                                    ?>
                                        <option value="<?= $rrr->name; ?>"><?= $rrr->name; ?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                    <div class="input-group-btn">
                        <span class="btn btn-info" onclick="filterctegory()" style="cursor:pointer;padding: 5px 50px;margin-left: 5px;border-radius: 5px;border: 1px solid #fff!important;">Search</span>
                    </div>
                    <!--
                    <div class="input-group-btn">
                        <div class="btn-group" role="group">
                            <div class="dropdown dropdown-lg">
                                <button type="button" onclick="myFunction()" class="btn btn-default dropdown-toggle br-0" data-toggle="dropdown"><span class="caret"></span></button>
                                <div class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dLabel" id="myDropdown" >
                                    <form class="form-horizontal" role="form">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h5>ADVANCED KEYWORD SEARCH</h5>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="filter">All of these words</label>
                                                    <input class="form-control" type="text" />
                                                </div>
                                                <div class="form-group">
                                                    <label for="contain">Any of these words</label>
                                                    <input class="form-control" type="text" />
                                                </div>
                                                <div class="form-group">    
                                                    <label for="contain">The exact phrase</label>
                                                    <input class="form-control" type="text" />
                                                </div>
                                                <div class="form-group">    
                                                    <label for="contain">Exclude these words</label>
                                                    <input class="form-control" type="text" />
                                                </div>
                                                <div class="form-group">    
                                                    <label for="contain">Title Search</label>
                                                    <input class="form-control" type="text" />
                                                </div>
                                                <div class="form-group">    
                                                    <label for="contain">Skill Search</label>
                                                    <input class="form-control" type="text" />
                                                </div>
                                                <button type="submit" class="btn btn-primary blue-border br-0">Search</button>
                                                <a href="javascript:void(0);">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary blue-border br-0" id="togglle"><i class="fa fa-sliders" aria-hidden="true"></i> Filter</button>
                        </div>
                    </div>
                    -->
                </div>
                <div class="filter-wrapper" id="filter">
                    <form class="form-horizontal" role="form">
                        <div class="row">
                            <div class="col-xl-4 col-lg-3 col-12 border-right">
                                <div class="row">
                                    <div class="col-12">
                                        <h5>Vehicle Filter</h5>
                                        <hr>
                                        <div class="form-group">
                                            <label for="carbrand">Car Brand</label>
                                            <select class="chosen-select" name="make_id" id="make_id" onChange="getModelList(this.value);">
                                                <option>Please Select</option>
                                                <?php
                                                    if($carmake_result)
                                                    {
                                                        foreach($carmake_result as $res)
                                                        {
                                                            ?>
                                                                <option value="<?= $res->id; ?>"><?= $res->name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="carbrand">Car Mode</label>
                                            <select class="chosen-select" id="model_id" name="model_id" multiple onChange="getYearList(this.value);">
                                                <option>Please Select</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="carbrand">Year</label>
                                            <select class="form-control chosen-select" id="year_id" multiple name="year_id">
                                                <option>Please Select</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="display:none;" class="col-xl-4 col-lg-3 col-12 border-right">
                                <div class="row">
                                    <div class="col-12">
                                        <h5>Vehicle Tyre Brand / Size Filter</h5>
                                        <hr>
                                        <div class="form-group">
                                            <label for="carbrand">Tyre Brand</label>
                                            <select class="chosen-select" id="brand_id" name="brand_id"  onChange="getWidthList(this.value)">
                                                <option value="">Please Select</option>
                                                <?php
                                                    if($tyrebrand_result)
                                                    {
                                                        foreach($tyrebrand_result as $res)
                                                        {
                                                            ?>
                                                                <option value="<?= $res->id; ?>"><?= $res->name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="carbrand">Width</label>
                                            <select class="chosen-select" name="width_id" id="width_id" onChange="getHeightList(this.value);">
                                                <option value="">Please Select</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="carbrand">Height</label>
                                            <select class="chosen-select" id="height_id" value="height_id" onChange="getRimList(this.value);">
                                                <option value="">Please Select</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="carbrand">Rim Size</label>
                                            <select class="chosen-select" id="rim_id" name="rim_id">
                                                <option value="">Please Select</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-4 col-lg-3 col-12 border-right">
                                <div class="row">
                                    <div class="col-12">
                                        <h5>Location Filter</h5>
                                        <hr>
                                        <div class="form-group">
                                            <label for="carbrand">City</label>
                                            <select class="chosen-select" name="city_id" id="city_id" onChange="getAreaList(this.value); ">
                                                <option value="">Please Select</option>
                                                <?php
                                                    if($city_result)
                                                    {
                                                        foreach($city_result as $rrr)
                                                        {
                                                            ?>
                                                                <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="carbrand">Area</label>
                                            <select class="form-control chosen-select" id="area_id" multiple name="area_id">
                                                <option>Please Select</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-4 col-lg-3 col-12">
                                <div class="row">
                                    <div class="col-12">
                                        <h5>Fitting Filter</h5>
                                        <hr>
                                        <div class="checkbox switcher">
                                            <label for="test">
                                                <small>With fitting</small>
                                                <input type="checkbox" id="test" value="">
                                                <span><small></small></span>
                                                <small>No fitting</small>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="category-wrap">
                    <!-- Shop Page Grig View-->
                    <div class="shop-page-grid-view">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home-grid">
                                <div class="grid-list-wrapper filterctegory">
                                    <?php
                                        if($category_result)
                                        {
                                            foreach($category_result as $res)
                                            {
                                                if($res->parent_id == 0)
                                                {
                                                    $img_name = $this->db->get_where(db_prefix().'files', array('rel_id' => $res->id, 'rel_type' => "category"))->row('attachment_key');
                                                    ?>
                                                        <div class="grid-list-column-item style-02">
                                                            <h5 class="title"><a href="<?= base_url('customers/carPartService/'.$res->id); ?>"><?= _l($res->name); ?></a></h5>
                                                            <div class="thumb">
                                                                <a href="<?= base_url('customers/carPartService/'.$res->id); ?>">    
                                                                    <img src="<?= site_url('download/file/taskattachment/'.$img_name); ?>" alt="<?= _l($img_name); ?>" class="img-fluid">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    <?php
                                                }
                                            }
                                        }
                                    ?>
                                    
                                </div>
                                <div id="more" style="display:none;">
                                    <div class="grid-list-wrapper">
                                        <!-- Grid List Column-->
                                        <div class="grid-list-column-item style-02">
                                            <h5 class="title"><a href="javascript:void(0);">&nbsp;</a></h5>
                                            <div class="thumb">
                                                <img src="<?= base_url() ?>assets/themes/immersive/images/parts/fog_light_trim.webp" alt="img" class="img-fluid">
                                            </div>
                                        </div>
                                        <!--// Grid List Column-->
    
                                        <!-- Grid List Column-->
                                        <div class="grid-list-column-item style-02">
                                            <h5 class="title"><a href="javascript:void(0);">wheel</a></h5>
                                            <div class="thumb">
                                                <img src="<?= base_url() ?>assets/themes/immersive/images/parts/wheel.webp" alt="img" class="img-fluid">
                                            </div>
                                        </div>
                                        <!--// Grid List Column-->
    
                                        <!-- Grid List Column-->
                                        <div class="grid-list-column-item style-02">
                                            <h5 class="title"><a href="javascript:void(0);">Engine Apron</a></h5>
                                            <div class="thumb">
                                                <img src="<?= base_url() ?>assets/themes/immersive/images/parts/engine_apron.webp" alt="img" class="img-fluid">
                                            </div>
                                        </div>
                                        <!--// Grid List Column-->
    
                                        <!-- Grid List Column-->
                                        <div class="grid-list-column-item style-02">
                                            <h5 class="title"><a href="javascript:void(0);">Catalytic Converter</a></h5>
                                            <div class="thumb">
                                                <img src="<?= base_url() ?>assets/themes/immersive/images/parts/catalytic_converter.webp" alt="img" class="img-fluid">
                                            </div>
                                        </div>
                                        <!--// Grid List Column-->

                                        <!-- Grid List Column-->
                                        <div class="grid-list-column-item style-02">
                                            <h5 class="title"><a href="javascript:void(0);">Valve Stem Cap</a></h5>
                                            <div class="thumb">
                                                <img src="<?= base_url() ?>assets/themes/immersive/images/parts/valve_stem_cap.webp" alt="img" class="img-fluid">
                                            </div>
                                        </div>
                                        <!--// Grid List Column-->
    
                                        <!-- Grid List Column-->
                                        <div class="grid-list-column-item style-02">
                                            <h5 class="title"><a href="javascript:void(0);">Bumper End</a></h5>
                                            <div class="thumb">
                                                <img src="<?= base_url() ?>assets/themes/immersive/images/parts/bumper_end.webp" alt="img" class="img-fluid">
                                            </div>
                                        </div>
                                        <!--// Grid List Column-->
                                    </div>
                                </div>
                                <!--// Grid List Wrapper-->
                                <!--
                                <div class="toggle-wrapper margin-top-20 text-center" >
                                    <a href="javascript:void(0);" id="toggle">Show More Categories</a>
                                </div>
                                -->
                            </div>
                        </div>
                    </div>
                    <!--// Shop Page Grid View-->         
                </div>
            </div>
        </div>
        <!--// Row End-->
    </div>
    <!--// Container End-->
</section>
<!--// About Story End-->
<?php include('include/footer.php')?>
<?php include('include/scripts.php')?>

<script>
    $(document).ready(function() {
        $("#toggle").click(function() {
            var elem = $("#toggle").text();
            if (elem == "Show More Categories") {
                //Stuff to do when btn is in the read more state
                $("#toggle").text("Show Less Categories");
                $("#more").slideDown();
            } else {
                //Stuff to do when btn is in the read less state
                $("#toggle").text("Show More Categories");
                $("#more").slideUp();
            }
        });
    });

    /* When the user clicks on the button,
    toggle between hiding and showing the dropdown content */
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }

    // Close the dropdown menu if the user clicks outside of it
    window.onclick = function(event) {
        if (!event.target.matches('.dropdown-toggle')) {
                var dropdowns = document.getElementsByClassName("dropdown-menu");
                var i;
                for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }

    $(document).ready(function(){
        $("#togglle").click(function(){
            $(".filter-wrapper").slideToggle();
        });
    });
    
    function filterctegory()
    {
        var filtercategory = $('#filtercategory option:selected').val();
        var parentid = 0;
        var str = "name="+filtercategory+"&parentid="+parentid+"&"+csrfData['token_name']+"="+csrfData['hash'];
	    $.ajax({
	        url: '<?= site_url()?>customers/filterctegory',
	        type: 'POST',
	        data: str,
	        dataType: 'html',
	        cache: false,
	        success: function(resp){
	            $('.filterctegory').html(resp);
	        }
	    });
    }
</script>