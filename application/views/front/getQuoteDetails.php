        <?php echo form_open_multipart(base_url('customers/getAQuotePost/'.$quoteData->id),array('class'=>'request-form')); ?>
            <div class="row">
                <div class="col-12 col-xl-6 col-lg-6">
                    <div class="form-group">
                        <label class="label">Description</label>
                        <input type="text" name="title" disabled value="<?= $quoteData->title; ?>" class="form-control" />
                    </div>
                </div>
                <div class="col-12 col-xl-6 col-lg-6">
                    <div class="form-group">
                        <label class="label">Choose Your Location</label>
                        <select name="locationid" disabled required class="form-control chosen-select">
                            <option value=""><option>
                            <?php
                                if($city_result)
                                {
                                    foreach($city_result as $rrr)
                                    {
                                        ?>
                                            <option value="<?= $rrr->id; ?>" <?= ($rrr->id == $quoteData->locationid)?"selected":""; ?>><?= $rrr->name; ?></option>
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
				<div class="col-12 col-xl-4 col-lg-4" style="display:none;">
                    <div class="form-group">
                        <label class="label">Address</label>
                        <textarea name="address" maxlength="35" class="form-control" disabled><?= $quoteData->address; ?></textarea>
					</div>
                </div>
            </div>
            
            <?php
                $categoryArr = explode(',',$quoteData->categoryid);
                $categorySubArr = explode(',',$quoteData->subcategoryid);
                $f = 1;
                if($categoryArr)
                {
                    for($k=0; $k < count($categoryArr); $k++)
                    {
                        ?>
                            <div class="row">
                                <div class="col-12 col-xl-6 col-lg-6">
                                    <div class="form-group">
                                        <label class="label">Choose Car Category</label>
                                        <select class="form-control chosen-select" name="categoryid[]" disabled onchange="getSubCategorys(this.value);">
                                            <?php
                                                if($category_result)
                                                {
                                                    foreach($category_result as $res)
                                                    {
                                                        ?>
                                                            <option value="<?= $res->id; ?>" <?= ($res->id == $categoryArr[$k])?"selected":""; ?>><?= $res->name; ?></option>
                                                        <?php
                                                    }
                                                }
                                                
                                                $subcategoryArr = $this->db->get_where(db_prefix().'category', array('parent_id' => $categoryArr[$k]))->result();
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-xl-6 col-lg-6">
                                    <div class="form-group">
                                        <label class="label">Choose Service Sub Category</label>
                                        <select class="form-control chosen-select" name="subcategoryid[]" disabled id="sub_category_ids">
                                            <?php
                                                if($subcategoryArr)
                                                {
                                                    foreach($subcategoryArr as $rr)
                                                    {
                                                        ?>
                                                            <option value="<?= $rr->id ?>" <?= ($rr->id == $categorySubArr[$k])?"selected":""; ?>><?= $rr->name; ?></option>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        <?php
                        $f++;
                    }
                }
            ?>
            
            <div class="row">
                <div class="col-12 col-xl-6 col-lg-6">
                    <div class="form-group">
                        <label class="label">Choose Vehicle Category</label>
                        <select name="vehiclecategory" disabled required class="form-control chosen-select">
                            <option value="Sedan" <?= ($quoteData->vehiclecategory == 'Sedan')?"selected":""; ?>>Sedan</option>
                            <option value="SUV" <?= ($quoteData->vehiclecategory == 'SUV')?"selected":""; ?>>SUV</option> 
                            <option value="Truck" <?= ($quoteData->vehiclecategory == 'Truck')?"selected":""; ?>>Truck</option> 
                        </select>
                    </div>
                </div>
                <div class="col-12 col-xl-6 col-lg-6">
                    <div class="form-group">
                        <label class="label">Choose Car Brand</label>
                        <select required class="form-control chosen-select" disabled id="make_ids" name="brandid" onChange="getModelLists(this.value)">
                            <option value=""></option>
                            <?php
                                if($carmake_result)
                                {
                                    foreach($carmake_result as $res)
                                    {
                                        ?>
                                            <option value="<?= $res->id; ?>" <?= ($res->id == $quoteData->brandid)?"selected":""; ?>><?= $res->name; ?></option>
                                        <?php
                                    }
                                }
                                $modellist = $this->db->get_where(db_prefix().'car_model', array('make_id' => $quoteData->brandid))->result();
                                $yearlist = $this->db->get_where(db_prefix().'car_year', array('make_id' => $quoteData->brandid, 'model_id' => $quoteData->carmodelid))->result();
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-xl-6 col-lg-6">
                    <div class="form-group">
                        <label class="label">Choose Car Model</label>
                        <select required class="form-control chosen-select" disabled name="carmodelid" id="model_ids" onChange="getYearLists(this.value)">
                            <?php
                                if($modellist)
                                {
                                    foreach($modellist as $rr)
                                    {
                                        ?>
                                            <option value="<?= $rr->id ?>" <?= ($rr->name == $quoteData->carmodelid)?"selected":""; ?>><?= $rr->name; ?></option>
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-12 col-xl-6 col-lg-6">
                    <div class="form-group">
                        <label class="label">Choose Car Make Year</label>
                        <select required class="form-control chosen-select" disabled name="carmakeyearid" id="year_ids">
                            <?php
                                if($yearlist)
                                {
                                    foreach($yearlist as $r)
                                    {
                                        ?>
                                           <option value="<?= $r->id ?>" <?= ($r->year == $quoteData->carmakeyearid)?"selected":""; ?>><?= $r->year; ?></option>                                         
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6 col-xl-6 col-lg-6">
                    <div class="form-group">
                        <label class="label">Upload Vehicle Registration Card<sup style="color: red;">*</sup></label>
                        <?php
                            if($quoteData->registrationcard)
                            {
                                ?>
                                    <img id="blah" data-toggle="modal" data-target="#galleryModal" onClick="showGalleryImg('<?= base_url('uploads/quote/'.$quoteData->registrationcard); ?>')"  height="100px" width="100px" src="<?php echo base_url('uploads/quote/'.$quoteData->registrationcard); ?>" alt="" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';"/>
                                <?php
                            }
                            else
                            {
                                ?>
                                    <img id="blah" height="100px" width="100px" src="#" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';">
                                <?php
                            }
                        ?>
                        
                    </div>
                </div>
                <div class="col-6 col-xl-6 col-lg-6">
                    <div class="form-group">
                        <label class="label">Chassis No<sup style="color: red;">*</sup></label>
                        <input type="text" class="form-control" disabled maxlength="25" value="<?= $quoteData->chassis_no; ?>" onkeyup="errorremove('chassis_no-reuse')"  name="chassis_no" id="chassis_no-reuse"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-xl-12 col-lg-12">
                    <div class="form-group">
                        <label class="label">Detailed Description</label>
                        <textarea class="form-control" rows="4" disabled name="description" placeholder="Detailed description here"><?= $quoteData->description; ?></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-xl-12 col-lg-12">
                    <div class="form-group">
                        <label class="label">Upload photo / audio / video<b class="text-info"><sub>Note: allow extension ( "jpg", "jpeg", "gif", "png", "mp3", "mp4", "wma" ) </sub></span></b></label>
                        <div class="bg-white mb-20">
                        </div>
                        <p>
							<?php
								if($quoteData->ext == 'mp3')
								{
									?>
										<audio controls>
											<source src="horse.ogg" type="audio/ogg">
											<source src="<?= base_url('uploads/quote/'.$quoteData->upload_photo_video); ?>" type="audio/mpeg" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';">
										</audio>
									<?php
								}
								elseif($quoteData->ext == 'mp4')
								{
									?>
										<video width="100" height="100" controls>
											<source src="<?= base_url('uploads/quote/'.$quoteData->upload_photo_video); ?>" type="video/mp4" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';">
											<source src="movie.ogg" type="video/ogg">
											Your browser does not support the video tag.
										</video>
									<?php
								}
								elseif($quoteData->ext == 'pdf' || $quoteData->ext == 'PDF' )
								{
									?>
										<a target="_blank" href="<?= base_url('uploads/quote/'.$quoteData->upload_photo_video); ?>"><img id="blah" height="80px" width="80px" src="#" alt="" /></a>
									<?php
								}
								elseif($quoteData->upload_photo_video != '')
								{
									?>
										<img id="blah" data-toggle="modal" data-target="#galleryModal" onClick="showGalleryImg('<?= base_url('uploads/quote/'.$quoteData->upload_photo_video); ?>')" height="100px" width="100px" src="<?= base_url('uploads/quote/'.$quoteData->upload_photo_video); ?>" alt="" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';"/>
									<?php
								}
								else
								{
									?>
										<img id="blah" height="100px" width="100px" alt="" src="#" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';"/>
									<?php
								}
							?>
						</p>
                    </div>
                </div>
            </div>
        </form>
        <!-- Gallery Modal -->
        <div id="galleryModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body" style="display:inline-block;">
                        <div class="img-wrap showgimg">
        					<img src="#" class="img-responsive" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';">
        				</div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showGalleryImg(img)
        	{
        	    $('.showgimg').html('<img src="'+img+'" class="img-responsive">');
        	}
        </script>