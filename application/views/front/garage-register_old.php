<?php include('include/header.php')?>
<div class="sign padding-120">
  <div class="container">
    <div class="row">
      <div class="offset-lg-4 col-lg-4 col-12">
        <div class="logo-wrapper text-center mb-40">
          <a href="<?= base_url(); ?>">
            <img src="<?= base_url() ?>assets/themes/immersive/images/logo/160x69-logo.jpg" alt="Logo">
          </a>
        </div>
        <div class="sign-in-area">
          <h5 class="text-center"><b>Create an Account</b></h5>
          <br>
          <div id="svg_wrap" style="display:none;"></div>
            <?php echo form_open_multipart($this->uri->uri_string(),array('id'=>'ms-form', 'class'=>'register-form')); ?>
                <section>
                    <p><b>Primary Account Contact Information</b></p>
                    <?php echo form_error('email'); ?>
                    <input type="email" name="email" class="form-control mb-20" placeholder="Account Email" />
                    <?php echo form_error('password'); ?>
                    <input type="password" name="password" class="form-control mb-20" placeholder="Password" />
                    <?php echo form_error('passwordr'); ?>
                    <input type="password" name="passwordr" class="form-control mb-20" placeholder="Confirm Password" />
                </section>
            
                <section>
                    <p><b>Company/ Garage Information</b></p>
                    <?php echo form_error('name'); ?>
                    <input type="text" name="name" class="form-control mb-20" placeholder="Name" />
                    <div class="input-group upload-wrap rounded-pill bg-white mb-20">
                        <input type="file" name="logo" id="file-upload" />
                        <label for="file-upload" class="file-upload">Upload Logo</label>
                        <div id="file-upload-filename"></div>
                    </div>
                    <?php echo form_error('fax'); ?>
                    <input type="text" name="fax" class="form-control mb-20" placeholder="Fax" />
                    <?php echo form_error('company_email'); ?>
                    <input type="email" class="form-control mb-20"name="company_email" placeholder="Email Address" />
                    <?php echo form_error('website_url'); ?>
                    <input type="url" class="form-control mb-20" name="website_url" placeholder="Website URL" />
                    <?php echo form_error('city_emirate'); ?>
                    <input type="text" class="form-control mb-20" name="city_emirate" placeholder="City/ Emirate" />
                    <?php echo form_error('address'); ?>
                    <input type="text" class="form-control mb-20" name="address" placeholder="Address" />
                    <?php echo form_error('latitude'); ?>
                    <input type="text" class="form-control mb-20" name="latitude" placeholder="Google Map Location" />
                    <div class="input-group upload-wrap rounded-pill bg-white">
                        <input type="file" name="image" id="file-upload1" />
                        <label for="file-upload" class="file-upload">Upload License</label>
                        <div id="file-upload-filename1"></div>
                    </div>
                </section>
            
                <div class="button w-100" id="next">Next</div>
                <!--<div class="button w-100" onclick="formsubmit()" id="submit">Register</div>-->
                <button class="button w-100" id="submit" type="submit">Register</button>
                <div class="button w-100" id="prev">Previous</div>
                <hr />
                <div class="text-center">
                    <a href="<?= base_url() ?>garage/login">Already have an account? <b>Signin</b></a>
                </div>
            </form>
        </div>
        <!--// Sign In Area-->
      </div>
    </div>
  </div>
</div>
<script>
    function formsubmit()
    {
        $('#ms-form').submit();
    }
</script>
<?php include('include/scripts.php')?>