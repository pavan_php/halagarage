<?php include('include/header.php')?>
<?php include('include/navbar.php')?>
<section class="our-service padding-top-50">
    <!--Container-->
    <div class="container">
        <div class="row">
        	<div class="col-md-12">
        		<div class="panel_s">
        			<div class="video-wrap w-100">
        			    <?php
        			        if($video_customer)
        			        {
        			            $imageArray2 = $this->db->get_where(db_prefix().'files', array('rel_id' => $video_customer->id, 'rel_type' => "garagevideo"))->row();
        			            $code1 = $video_customer->code;
        		                $url1 = '';
        		                if($code1 != '')
        		                {
        		                    ?>
        		                        <iframe width="100%" height="450" src="https://www.youtube.com/embed/<?= $code1; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        		                    <?php
        		                }
        		                else
        		                {
        		                    $url1 = site_url('download/file/taskattachment/'. $imageArray2->attachment_key);
        		                    ?>
        		                        <video width="100%" height="450" controls>
                                            <source src="<?= $url1; ?>" type="video/mp4">
                                        </video>
        		                    <?php
        		                }
        			        }
        			        else
        			        {
        			            ?>
        			                <iframe width="100%" height="450" src="https://www.youtube.com/embed/SGL2pFyNVTQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        			            <?php
        			        }
        			    ?>
        			</div>
        		</div>
        	</div>
        </div><br>
        <div class="row">
        	<div class="offset-md-1 col-md-5">
        		<a href="<?= base_url('customers/findCarPartService'); ?>" class="btn btn-info btn-md btn-dashboard">Find Car Part or Service</a>
        	</div>
        	<div class="col-md-5 text-right">
        		<a href="<?= base_url('customers/getAQuote'); ?>" class="btn btn-info btn-md btn-dashboard">Get Quote</a>
        	</div>
        </div><br>
    </div><hr>
    <div class="container">
		<div class="row">
			<div class="col-md-12 text-right">
				<!--<span class="copyright-footer">2020 Copyright Hala Garage Admin</span>
													- <a href="http://php.manageprojects.in/halagarage/clients/gdpr" class="gdpr-footer">GDPR</a>
				-->
				<ul style="list-style: none;margin: 0;padding: 0;font-weight: normal;">
					<li style="display: inline-block;padding: 10px 5px;margin: 0;line-height: 20px;text-align: center;">
						<a href="<?= base_url('contact-us'); ?>">Contact Us</a>
					</li>
					<li style="display: inline-block;padding: 10px 5px;margin: 0;line-height: 20px;text-align: center;">
						<a href="<?= base_url('term-and-conditions'); ?>">Terms & Conditions</a>
					</li>
					<li style="display: inline-block;padding: 10px 5px;margin: 0;line-height: 20px;text-align: center;">
						<a href="javascript:void(0);">Policy</a>
					</li>
					<li style="display: inline-block;padding: 10px 5px;margin: 0;line-height: 20px;text-align: center;">
						<a href="<?= base_url('faq'); ?>">Faq</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!--
    <div class="container">
        <div class="row form-group">
            <div class="col-md-3">
                <a href="<?= base_url('contact-us'); ?>">Contact Us</a>
            </div>
            <div class="col-md-3 text-center">
                <a href="<?= base_url('term-and-conditions'); ?>">Terms & Conditions</a>
            </div>
            <div class="col-md-3 text-center">
                <a href="javascript:void(0);">Policy</a>
            </div>
            <div class="col-md-3 text-center">
                <a href="javascript:void(0);">FAQ</a>
            </div>
        </div>
    </div>
    -->
    <!--// Container-->
</section>
<?php include('include/scripts.php')?>




        
    