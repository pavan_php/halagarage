<?php include('include/header.php')?>
<?php include('include/navbar.php')?><!--Full Width Sider Start-->
<!--Breadcrumb Start-->
<div class="breadcrumb-area" style="background-image: url('assets/themes/immersive/images/about/about-us-banner-bg.png')">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner text-center">
                    <h1 class="page-title">Contact Us</h1>
                    <ul class="page-list">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li>Contact</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Breadcrumb End-->

<!--Cantact-->
<div class="contact-page padding-top-115 padding-bottom-120">
    <div class="container">
        <div class="row">
            <?php
                if($content)
                {
                    echo $content->description;
                }
            ?>
            <div class="col-lg-9">
                <form action="javascript:void(0);" method="post" id="configform" class="contact-form">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" onkeyup="errorremove('name')" id="name">
                                <p class="text-danger name"></p>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" onkeyup="errorremove('email')" id="email">
                                <p class="text-danger email"></p>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="tel" class="form-control" onkeyup="errorremove('phone')" id="phone">
                                <p class="text-danger phone"></p>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="subject">Subject</label>
                                <input type="text" class="form-control" onkeyup="errorremove('subject')" id="subject">
                                <p class="text-danger subject"></p>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="message">Message</label>
                                <textarea class="form-control" cols="30" onkeyup="errorremove('message')" rows="10" id="message"></textarea>
                                <p class="text-danger message"></p>
                            </div>
                            <div class="main-btn-wrap">
                                <button class="main-btn" type="button" onClick="contactFormSubmit();">Send Message</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--// Cantact-->

<!--Google Map -->
<div class="google-map-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 px-0">
                <div id="google-map">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3070.1899657893728!2d90.42380431666383!3d23.779746865573756!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c7499f257eab%3A0xe6b4b9eacea70f4a!2sManama+Tower!5e0!3m2!1sen!2sbd!4v1561542597668!5m2!1sen!2sbd"
                        style="border:0; width: 100%; height: 100%;" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Google Map End-->
<?php include('include/footer.php')?>
<?php include('include/scripts.php')?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
<script>
    function contactFormSubmit()
    {
        var name = $('#name').val();
        var email = $('#email').val();
        var phone = $('#phone').val();
        var subject = $('#subject').val();
        var message = $('#message').val();
        if(name == '' || email == '' || phone == '' || subject == '' || message == '')
        {
            if(name == '')
            {
                $('.name').text('Name field is required');
            }
            if(email == '')
            {
                $('.email').text('Email field is required');
            }
            if(phone == '')
            {
                $('.phone').text('Phone field is required');
            }
            if(subject == '')
            {
                $('.subject').text('Subject field is required');
            }
            if(message == '')
            {
                $('.message').text('Message field is required');
            }
            return false;
        }
        else
        {
            if(name != '')
            {
                if(name.length < 3 || name.length > 15){
                    $('.name').text('Make sure the name is between 3-15 characters long');
                    return false;
                }
            }
            if(email != '')
            {
                var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                if (reg.test(email) == false) 
                {
                    $('.email').text('Invalid email address!');
                    return false;
                }
            }
            if(phone == '')
            {
                var mob = /^[1-9]{1}[0-9]{9}$/;
                if (mob.test(phone) == false) {
                    $('.email').text('Please enter valid mobile number!');
                    return false;
                }
            }
            if(name != '' && email != '' && phone != '' && subject != '' && message != '')
            {
                var str = "name="+name+"&email="+email+"&phone="+phone+"&subject="+subject+"&message="+message+"&"+csrfData['token_name']+"="+csrfData['hash'];
        	    $.ajax({
        	        url: '<?= site_url()?>notifications/contactUs',
        	        type: 'POST',
        	        data: str,
        	        //dataType: 'json',
        	        cache: false,
        	        success: function(resp){
        	            if(resp)
        	            {
        	                $('#configform')[0].reset();
        	                //successmsg('Thank you for contacting us – we will get back to you soon ...');
        	                swal(
                				'Success',
                				'Thank you for contacting us – we will get back to you soon ...',
                				'success'
                			)
                			return true;
        	            }
        	            else
        	            {
        	                errormsg('Message are not send')
        	            }
        	        }
        	    });
            }
        }
    }
</script>