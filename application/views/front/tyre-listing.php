<?php include('include/header.php')?>
<?php include('include/navbar.php')?>
<!--Breadcrumb Start-->
<div class="breadcrumb-area" style="background-image: url('<?= base_url() ?>assets/themes/immersive/images/about/about-us-banner-bg.png')">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner text-center">
                    <h1 class="page-title"><?= $title; ?></h1>
                    <ul class="page-list">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li>Product-listing</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Breadcrumb End-->
<div class="terms-and-conditions-page padding-top-115 padding-bottom-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="input-group" id="adv-search">
                    <input type="text" class="form-control" placeholder="Find a car part/ service" />
                    <div class="input-group-btn">
                        <div class="btn-group" role="group">
                            <div class="dropdown dropdown-lg">
                                <button type="button" onclick="myFunction()" class="btn btn-default dropdown-toggle br-0" data-toggle="dropdown"><span class="caret"></span></button>
                                <div class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dLabel" id="myDropdown" >
                                    <form class="form-horizontal" role="form">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h5>ADVANCED KEYWORD SEARCH</h5>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="filter">All of these words</label>
                                                    <input class="form-control" type="text" />
                                                </div>
                                                <div class="form-group">
                                                    <label for="contain">Any of these words</label>
                                                    <input class="form-control" type="text" />
                                                </div>
                                                <div class="form-group">    
                                                    <label for="contain">The exact phrase</label>
                                                    <input class="form-control" type="text" />
                                                </div>
                                                <div class="form-group">    
                                                    <label for="contain">Exclude these words</label>
                                                    <input class="form-control" type="text" />
                                                </div>
                                                <div class="form-group">    
                                                    <label for="contain">Title Search</label>
                                                    <input class="form-control" type="text" />
                                                </div>
                                                <div class="form-group">    
                                                    <label for="contain">Skill Search</label>
                                                    <input class="form-control" type="text" />
                                                </div>
                                                <button type="submit" class="btn btn-primary blue-border br-0">Search</button>
                                                <a href="javascript:void(0);">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary blue-border br-0" id="togglle"><i class="fa fa-sliders" aria-hidden="true"></i> Filter</button>
                        </div>
                    </div>
                </div>
                <div class="filter-wrapper" id="filter">
                    <form class="form-horizontal" id="searchfeeorm">
                        <div class="row">
                            <div class="col-md-12">
                                <label style="cursor:pointer" class="vehiclebtn btn btn-default">
                                    <input type="radio" style="display:none;" name="filtertype" onclick="setFilterType(1);" value="1"> Search By Vehicle
                                </label>|
                                <label style="cursor:pointer" class="sizebtn btn btn-default">
                                    <input type="radio" style="display:none;" name="filtertype" onclick="setFilterType(2);" value="2"> Search By Size
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div id="vehicletab" class="col-xl-4 col-lg-3 col-12 border-right">
                                <div class="row">
                                    <div class="col-12">
                                        <h5>Vehicle Filter</h5>
                                        <hr>
                                        <div class="form-group">
                                            <label for="carbrand">Car Brand</label>
                                            <div class="select-dropdown form-group">
                                                <input id="search-field" type="text" class="form-control" placeholder="Search by Brand" >
                                                <input id="make_id" type="text" class="form-control hide" style="display:none;">
                                            </div>
                                            <?php /* ?>
                                            <select class="chosen-select" name="make_id" id="make_id" onChange="getModelList(this.value);">
                                                <option></option>
                                                <?php
                                                    if($carmake_result)
                                                    {
                                                        foreach($carmake_result as $res)
                                                        {
                                                            ?>
                                                                <option value="<?= $res->id; ?>"><?= $res->name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                            <?php */ ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="carbrand">Car Mode</label>
                                            <select class="chosen-select" id="model_id" name="model_id" multiple onChange="getYearList(this.value);">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="carbrand">Year</label>
                                            <select class="form-control chosen-select" id="year_id" multiple name="year_id">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="tyretab" style="display:none;" class="col-xl-4 col-lg-3 col-12 border-right">
                                <div class="row">
                                    <div class="col-12">
                                        <h5>Tyre Brand / Size Filter</h5>
                                        <hr>
                                        <div class="select-dropdown form-group">
                                            <input id="search-field-tyre" type="text" class="form-control" placeholder="Search by Brand" >
                                            <input id="brand_id" type="text" class="form-control hide" style="display:none;">
                                        </div>
                                        <?php /* ?>
                                        <div class="form-group">
                                            <label for="carbrand">Tyre Brand</label>
                                            <select class="chosen-select" id="brand_id" name="brand_id"  onChange="getWidthList(this.value)">
                                                <?php
                                                    if($tyrebrand_result)
                                                    {
                                                        foreach($tyrebrand_result as $res)
                                                        {
                                                            ?>
                                                                <option value="<?= $res->id; ?>"><?= $res->name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <?php */ ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="carbrand">Width</label>
                                            <select class="chosen-select" name="width_id " id="width_id" onChange="getHeightList(this.value);">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="carbrand">Height</label>
                                            <select class="chosen-select" id="height_id" value="height_id" onChange="getRimList(this.value);">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="carbrand">Rim Size</label>
                                            <select class="chosen-select" id="rim_id" name="rim_id">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-4 col-lg-3 col-12 border-right">
                                <div class="row">
                                    <div class="col-12">
                                        <h5>Location Filter</h5>
                                        <hr>
                                        <div class="form-group">
                                            <label for="carbrand">City</label>
                                            <select class="chosen-select" name="city_id" id="city_id" onChange="getAreaList(this.value); ">
                                                <option></option>
                                                <?php
                                                    if($city_result)
                                                    {
                                                        foreach($city_result as $rrr)
                                                        {
                                                            ?>
                                                                <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="carbrand">Area</label>
                                            <select class="form-control chosen-select" id="area_id" multiple name="area_id">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-4 col-lg-3 col-12">
                                <div class="row">
                                    <div class="col-12">
                                        <h5>Fitting Filter</h5>
                                        <hr>
                                        <div class="checkbox switcher">
                                            <label for="fitting_filter">
                                                <small>With fitting</small>
                                                <input type="checkbox" id="fitting_filter">
                                                <span><small></small></span>
                                                <small>No fitting</small>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <span class="btn btn-info" style="cursor: pointer;" onclick="filterProduct();">Search</span>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-warning" style="cursor: pointer;" onclick="clearallfields();">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><br>
                <section class="product-list-wrap">
                    <h4><?= ucfirst($title); ?></h4>
                    <div class="filterproductlist">    
                        <?php
                            if($product_list)
                            {
                                foreach($product_list as $rrr)
                                {
                                    ?>
                                        <div class="row">
                                            <div class="col-md-9">
                                                <a href="<?= base_url('customers/productDetails/'.$rrr->id); ?>" style="display:block;">
                                                    <div class="product-listing-wrapper">
                                                        <div class="product-list">
                                                            <div class="image-holder">
                                                                <?php
                                                                    $src = '#';
                                                                    $attachment_key = $this->db->get_where(db_prefix().'files', array('rel_id' => $rrr->id, 'rel_type' => "product"))->row('file_name');
                                                                    if($attachment_key != '')
                                                                    {
                                                                        $src = base_url('uploads/product/'.$rrr->id.'/'.$attachment_key); 
                                                                    }
                                                                ?>
                                                                <img src="<?= $src; ?>" alt="<?= $attachment_key; ?>" class="img-fluid">
                                                            </div>
                                                            <div class="product-content">
                                                                <h5 class="title"><b><?= $rrr->product_name; ?></b></h5>
                                                                <span class="description"><?= $rrr->description; ?></span><br>
                                                                <span class="price"><?= CURRENCY_NAME.' '.$rrr->price_with_fitting; ?></span>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div tabindex="0" class="see-all-products-details text-right">
                                                            <span class="styledtext-sc"><a href="<?= base_url('customers/productDetails/'.$rrr->id); ?>">See Product Details</a></span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-md-3">
                                                <a target="_blank" href="<?= base_url('customers/garageProfile/'.$rrr->garage_id); ?>" class="btn btn-outline-info btn-xl btn-block"><i class="fa fa-folder-o" aria-hidden="true"></i> Garage Details</a><br>
                                                <a target="_blank" href="<?= base_url('customers/message/'.$rrr->garage_id); ?>" class="btn btn-outline-success btn-xl btn-block"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send Enquiry</a><br>
                                                <a target="_blank" href="<?= base_url('customers/buyNow/'.$rrr->garage_id.'/'.$rrr->id);?>" class="btn btn-outline-warning btn-xl btn-block"><i class="fa fa-credit-card" aria-hidden="true"></i> Buy Now</a>
                                            </div>
                                        </div><hr>
                                    <?php
                                }
                            }
                            else
                            {
                                echo '<p class="dataTables_empty">No product found!</p>';
                            }
                        ?>
                    </div>   
                </section>
            </div>
        </div>
    </div>
</div>

<?php include('include/footer.php')?>
<?php include('include/scripts.php')?>
<script>
    var base_url = '<?= base_url() ?>';
    $(document).ready(function(){
        $("#togglle").click(function(){
            $(".filter-wrapper").slideToggle();
        });
    });
    var filtertype_ = 1;
    function setFilterType(id)
    {
        $(".chosen-select").val('').trigger("chosen:updated");
        if(id == 1)
        {
            filtertype_ = 1;
            $('.vehiclebtn').removeClass('default');
            $('.vehiclebtn').addClass('info');
            $('.sizebtn').removeClass('info');
            $('.sizebtn').addClass('default');
            $('#tyretab').css('display','none');
            $('#vehicletab').css('display','block');
        }
        else
        {
            filtertype_ = 2;
            $('.vehiclebtn').removeClass('info');
            $('.vehiclebtn').addClass('default');
            $('.sizebtn').removeClass('default');
            $('.sizebtn').addClass('info');
            
            $('#tyretab').css('display','block');
            $('#vehicletab').css('display','none');
        }
    }
    
    // Product filter search
    function filterProduct()
    {
        $('.filterproductlist').html('<p>Loading...</p>');
        if ($('#fitting_filter').is(":checked"))
        {
            var fittingval = 1;
        }
        else
        {
            var fittingval = 0;
        }
        
        var brand_id = $("#brand_id option:selected").val();
        if(brand_id == '')
        {
            brand_id = null;
        }
        var width_id = $("#width_id option:selected").val();
        if(width_id == '')
        {
            width_id = null;
        }
        var height_id = $("#height_id option:selected").val();
        if(height_id == '')
        {
            height_id = null;
        }
        var rim_id = $("#rim_id option:selected").val();
        if(rim_id == '')
        {
            rim_id = null;
        }
        
        /*var car_brand = $("#make_id option:selected").val();*/
        var car_brand = $("#make_id").val();
        if(car_brand == '')
        {
            car_brand = null;
        }
        var car_model = $("#model_id").chosen().val();
        var car_year = $("#year_id").chosen().val();
        var city_id = $("#city_id option:selected").val();
        if(city_id == '')
        {
            city_id = null;
        }
        var area_id = $("#area_id").chosen().val();
        var catid = <?= $id ?>;
        if(filtertype_ == 1)
        {
            var str = "car_brand="+car_brand+"&car_model="+car_model+"&car_year="+car_year+"&city_id="+city_id+"&area_id="+area_id+"&catid="+catid+"&fittingval="+fittingval+"&"+csrfData['token_name']+"="+csrfData['hash'];
            $.ajax({
                url: '<?= site_url()?>customers/filterProduct',
                type: 'POST',
                data: str,
                dataType: 'html',
                cache: false,
                success: function(resp){
                    $('.filterproductlist').html(resp);
                }
            });
        }
        else
        {
            var str = "brand_id="+brand_id+"&width_id="+width_id+"&height_id="+height_id+"&rim_id="+rim_id+"&city_id="+city_id+"&area_id="+area_id+"&catid="+catid+"&fittingval="+fittingval+"&"+csrfData['token_name']+"="+csrfData['hash'];
            $.ajax({
                url: '<?= site_url()?>customers/filterTyre',
                type: 'POST',
                data: str,
                dataType: 'html',
                cache: false,
                success: function(resp){
                    $('.filterproductlist').html(resp);
                }
            });
        }
    }
    
    // Functio clear
    function clearallfields()
    { 
        $('#searchfeeorm')[0].reset();
        $(".chosen-select").val('').trigger("chosen:updated");
    }
</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-typeahead/2.11.0/jquery.typeahead.css" crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.3.1/typeahead.bundle.js" crossorigin="anonymous"></script>
<script>
    (function() {
        var predictionsOld;
        var locMatches = '';
        var cid = '';
        var uniqueLocName = '';
        var uniqueCid = '';
    
        function PlacesSearch(element, options) {
            this.element = $(element);
            //  this.options = $.extend({}, options);
            //  this.onSelectAddress = this.options.onSelectAddress || $.noop;
            this.element.typeahead({
                highlight: true
            }, {
                source: this.places,
                name: "States",
                async: true,
                hint: true,
                minLength: 3,
                templates: {
                    empty: ['<div class="empty-message">',
                        'No Result Found', '</div>'
                    ].join('\n'),
                    suggestion: function(item) {
    
                        var checked = (item.checked ? "checked" : "");
                        return '<div data-id="' + item.location_id + '" data-location="' + item.location + '" class="' + item.location + '" ><div class="sadik custom-control custom-checkbox"><input data-cid="' + item.location_id + '"  value="' + item.location + '" type="checkbox" class="custom-control-input" ' + checked + '><span class="custom-control-label"> <span class="tt-country"> ' + item.location + '</span></span></div></div>';
    
                    }
                },
                limit: 1000,
            });
            var obj = this;
            this.element.on('typeahead:selected', function(e, address) {
                var check = $('.tt-dataset-States').find("[data-id='" + address.location_id + "']").find("[type='checkbox']");
                checked = !check.prop("checked");
                check.prop("checked", checked);
                address.checked = checked;
                obj.typeaheadSelectedItem(e, address);
            });
    
            var multipleActive = false;
    
            this.element.data('ttTypeahead').select = function select($selectable) {
                var data = this.menu.getSelectableData($selectable);
                if (data && !this.eventBus.before("select", data.obj)) {
                  
                    this.input.setQuery(data.obj.location, true);
                    this.eventBus.trigger("select", data.obj);
                    if (!multipleActive) {
                        this.close();
                    }
                    return true;
                }
                return false;
            }
        };
    
        /**
         * Extending the Method
         */
        PlacesSearch.prototype = {
            constructor: PlacesSearch,
            /**
             * This will help in getting places data from google web services
             * @param q
             * @param cb  : sync
             * @param fc  : async   : see typeahead documentations for this
             */
            places : function(q, cb, fc) {
                var c = new Array();
                //c.push('');
                var c, substringRegex;
                $.get(base_url+"customers/getBrandList/"+q, {input: q}, function(predictions, status){
                    //vmSearch.locQ = q;
                    substrRegex = new RegExp(q, 'i');
                    predictions1 = JSON.parse(predictions);
                    var predictions2 = predictions1.result;
                    $.map(predictions2, function(prediction) {
                         
                        if (substrRegex.test(prediction.location)) {
                            c.push(prediction);
                        }
                    });
                    fc(c);
                });
            },
    
            /**
             * When and item get selected we need to pull its detail from google
             * 
             * @param e
             * @param address
             */
            typeaheadSelectedItem: function(e, address) {
                getModelList(address.location_id);
                $('#make_id').val(address.location_id);
                /*$('#search-field').placesSearch();*/
            },
        };
    
        /**
         * STARTER
         */
        $.fn.placesSearch = function(options) {
            return this.each(function() {
                new PlacesSearch(this, options);
            });
        };
    
    }());

    $('#search-field').placesSearch();
</script>

<script>
    (function() {    
        function PlacesSearchTyre(element, options) {
            this.element = $(element);
            this.element.typeahead({
                highlight: true
            }, {
                source: this.places,
                name: "States",
                async: true,
                hint: true,
                minLength: 3,
                templates: {
                    empty: ['<div class="empty-message">',
                        'No Result Found', '</div>'
                    ].join('\n'),
                    suggestion: function(item) {
    
                        var checked = (item.checked ? "checked" : "");
                        return '<div data-id="' + item.tyres_id + '" data-tyres="' + item.tyres + '" class="' + item.tyres + '" ><div class="sadik custom-control custom-checkbox"><input data-cid="' + item.tyres_id + '"  value="' + item.tyres + '" type="checkbox" class="custom-control-input" ' + checked + '><span class="custom-control-label"> <span class="tt-country"> ' + item.tyres + '</span></span></div></div>';    
                    }
                },
                limit: 1000,
            });
            var obj = this;
            this.element.on('typeahead:selected', function(e, address) {
                var check = $('.tt-dataset-States').find("[data-id='" + address.tyres_id + "']").find("[type='checkbox']");
                checked = !check.prop("checked");
                check.prop("checked", checked);
                address.checked = checked;
                obj.typeaheadSelectedItemTyre(e, address);
            });
    
            var multipleActive = false;
    
            this.element.data('ttTypeahead').select = function select($selectable) {
                var data = this.menu.getSelectableData($selectable);
                if (data && !this.eventBus.before("select", data.obj)) {                  
                    this.input.setQuery(data.obj.tyres, true);
                    this.eventBus.trigger("select", data.obj);
                    if (!multipleActive) {
                        this.close();
                    }
                    return true;
                }
                return false;
            }
        };    
        /**
         * Extending the Method
         */
        PlacesSearchTyre.prototype = {
            constructor: PlacesSearchTyre,            
            places : function(q, cb, fc) {
                var c = new Array();
                //c.push('');
                var c, substringRegex;
                $.get(base_url+"customers/getTyreList/"+q, {input: q}, function(predictions, status){
                    //vmSearch.locQ = q;
                    substrRegex = new RegExp(q, 'i');
                    predictions1 = JSON.parse(predictions);
                    var predictions2 = predictions1.result;
                    $.map(predictions2, function(prediction) {                         
                        if (substrRegex.test(prediction.tyres)) {
                            c.push(prediction);
                        }
                    });
                    fc(c);
                });
            },    
            /**
             * When and item get selected we need to pull its detail from google
             * 
             * @param e
             * @param address
             */
            typeaheadSelectedItemTyre: function(e, address) {
                getWidthList(address.tyres_id);
                $('#brand_id').val(address.tyres_id);
                /*$('#search-field').PlacesSearchTyre();*/
            },
        };
    
        /**
         * STARTER
         */
        $.fn.PlacesSearchTyre = function(options) {
            return this.each(function() {
                new PlacesSearchTyre(this, options);
            });
        };
    
    }());

    $('#search-field-tyre').PlacesSearchTyre();
</script>