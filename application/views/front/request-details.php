<?php include('include/header.php')?>
<?php include('include/navbar.php')?><!--Full Width Sider Start-->
<div class="breadcrumb-area" style="background-image: url('<?= site_url() ?>assets/themes/immersive/images/about/about-us-banner-bg.png')">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner text-center">
                    <h1 class="page-title">Request Details</h1>
                    <ul class="page-list">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li>Request Details</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Breadcrumb End-->
<!--About Story Start-->
<section class="car-part padding-top-120 padding-bottom-110">
    <div class="container">
        <div class="row">
            <div class="offset-lg-1 col-lg-10 col-12">
               <div class="panel_s">
                  <div class="panel-body">
                     <div class="request-details-wrap">
                        <div class="d-md-flex align-items-center">
                            <div><h4 class="uppercase"><a href="javascript:void(0);" data-toggle="modal" onclick="getPreviewReq(<?= $id; ?>)" data-target="#detailsModal"><b class="text-info"><?= $quoteTitle; ?></b></a></h4></div>
                            <div class="ml-auto align-items-center">
                                <div class="dl">
                                    <a href="<?= base_url('customers/myQuote'); ?>">Back</a>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                           <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Garage Name</th>
                                        <th>Price <?= CURRENCY_NAME; ?></th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if($request_result)
                                        {
                                            foreach($request_result as $rrr)
                                            {
                                                $garageName = $this->db->get_where(db_prefix().'clients', array('userid' => $rrr->garage_id))->row('company');
                                                ?>
                                                    <tr>
                                                        <td><a href="<?= base_url('customers/garageProfile/'.$rrr->garage_id)?>" target="_blank" class="text-black"><?= $garageName; ?></a></td>
                                                        <td><?= $rrr->price; ?></td>
                                                        <td>
                                                            <?php
                                                            if($rrr->garage_confirm==0){
                                                                
                                                                if($rrr->qty == '')
                                                                {
                                                                    ?>
                                                                        <button type="button" class="btn btn-re-activate" data-toggle="modal" id="sendreqbtn<?= $rrr->id ?>" onclick="sendmsgModal('<?= $garageName ?>', <?= $rrr->id ?>)" data-target="#sendmsgModal"><i class="fa fa-reply" aria-hidden="true"></i> Reply / Send Message</button>
                                                                        <a href="<?= base_url('customers/message/'.$rrr->garage_id); ?>" style="display:none;" class="btn btn-re-activate" id="sendreq<?= $rrr->id?>"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                                                    <?php
                                                                }
                                                                else
                                                                {
                                                                    ?>
                                                                        <a href="<?= base_url('customers/message/'.$rrr->garage_id); ?>" class="btn btn-re-activate"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                                                    <?php
                                                                }
                                                                if($rrr->booking_request_status == 0)
                                                                {
                                                                    ?>
                                                                        <a href="<?= base_url('customers/scheduleappointment/'.$rrr->id);?>" class="btn btn-confirm"><i class="fa fa-calendar" aria-hidden="true"></i> Book/Schedule Appointment</a>
                                                                    <?php
                                                                }
                                                                else
                                                                {
                                                                    echo '<a href="javascript:void(0)" class="btn btn-confirm">Pending confirmation from garage</a>';
                                                                }
                                                            }else if($rrr->garage_confirm==1){ ?>
                                                                <p>Confirm By Garage</p>
                                                          <?php  }else{ ?>
                                                               <p>Cancelled By Garage</p>
                                                        <?php   }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                <?php
                                            }
                                        }
                                        else
                                        {
                                            ?>
                                                <tr><td valign="top" colspan="4" class="dataTables_empty">No matching records found</td></tr>
                                            <?php
                                        }
                                    ?>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!--// Row End-->
     </div>
     <!--// Container End-->
 </section>
 <!--// About Story End-->
 <?php include('include/footer.php')?>
 <?php include('include/scripts.php')?>
 
 <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet"/> 
 <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
 
<!-- The Sendmsg Modal -->
<div class="modal" id="sendmsgModal">
   <div class="modal-dialog">
      <div class="modal-content">
         <!-- Modal Header -->
         <div class="modal-header">
            <h5 class="modal-title">Send Message</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div> 
         <!-- Modal body -->
         <div class="modal-body">
            <div class="row">
               <div class="col-md-12">
                     <div class="send-wrap">
                        <h4><b id="garagename">APEX AUTO GARAGE</b></h4>
                        <hr>
                        <form id="msgform">
                           <div class="row">
                              <div class="form-group col-md-12">
                                 <label for="product" class="mb-5">Description</label>
                                 <input class="form-control" value="<?= $quoteTitle; ?>" id="product" disabled/>
                              </div>
                              <div class="form-group col-md-6" style="display:none;">
                                 <label for="qty" class="mb-5">Qty. Required</label>
                                 <input type="number" class="form-control" onkeyup="removeError('qty')" maxlength="5" placeholder="Enter quantity here" id="qty">
                                 <p class="text-danger qty"></p>
                              </div>
                           </div>
                           <div class="row">
                              <div class="form-group col-md-12">
                                 <label for="msg" class="mb-5">Message</label>
                                 <textarea class="form-control" rows="6" onkeyup="removeError('msg')" maxlength="700" placeholder="Do you have the required quantity?" id="msg"></textarea>
                                 <p class="text-danger msg"></p>
                              </div>
                           </div>
                           <div class="row">
                              <div class="form-group col-md-12">
                                 <span onclick="sendReplyToGarage()" class="btn btn-confirm sendmsg"> Send </span>
                                 <button type="button" class="btn btn-appologise" data-dismiss="modal">Discard</button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- The detail Modal -->
<div class="modal" id="detailsModal">
   <div class="modal-dialog modal-lg modal-dialog-scrollable-2">
      <div class="modal-content">
         <!-- Modal Header -->
         <div class="modal-header">
            <h5 class="modal-title">Details</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div> 
         <!-- Modal body -->
         <div class="modal-body">
            <div class="row">
               <div class="col-md-12">
                   <div class="previewrequest">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
    /* Get Draft Request */
    function getPreviewReq(id)
    {
        $('.previewrequest').html('');
        if(id)
        {
            var str = "id="+id+"&"+csrfData['token_name']+"="+csrfData['hash'];
    	    $.ajax({
    	        url: '<?= site_url()?>customers/getQuoteDetails',
    	        type: 'POST',
    	        data: str,
    	        cache: false,
    	        success: function(resp){
    	            if(resp)
    	            {
    	                $('.previewrequest').html(resp);
    	            }
    	            else
    	            {
    	                $('.previewrequest').html('');
    	            }
    	        }
    	    });
        }
        else
        {
            $('.previewrequest').html('');
        }
    }
    
    /**
    *   @Function: sendmsgModal
    */
    var quoteReqId = '';
    function sendmsgModal(garagename, id)
    {
        if(garagename)
        {
            $('#garagename').text(garagename);
            quoteReqId = id;
        }
    }
    /**
    *   @Function: sendReplyToGarage
    */
    function sendReplyToGarage()
    {
        //var qty = $('#qty').val();
        var qty = 1;
        var msg = $('#msg').val();
		if(qty == 0 || qty < 0)
		{
			if(qty == '')
			{
				$('.qty').text('Qty field is required');
			}
			else{
				$('.qty').text('Qty should be greater than 0');
			}				
			return false;
		}
        if(qty != '' && msg != '' && quoteReqId != '')
        {
            //$('.sendmsg').css('display','none');
            var str = "id="+quoteReqId+"&qty="+qty+"&msg="+msg+"&"+csrfData['token_name']+"="+csrfData['hash'];
            $.ajax({
    	        url: '<?= base_url()?>customers/sendReplyToGarage',
    	        type: 'POST',
    	        data: str,
    	        cache: false,
    	        success: function(resp){
    	            if(resp)
    	            {
    	                //$('.sendmsg').css('display','block');
    	                $('.close').click();
    	                $('#msgform')[0].reset();
    	                $('#sendreqbtn'+quoteReqId).css('display','none');
    	                $('#sendreq'+quoteReqId).css('display','block');
    	                swal("Your request has been sent successfully!");
    	                return true;
    	            }
    	            else
    	            {
    	                //$('.sendmsg').css('display','block');
    	                swal("Access denied");
    	                return false;
    	            }
    	        }
    	    });
        }
        else
        {
            if(qty == '')
            {
                $('.qty').text('Qty field is required');
                return false;
            }
            if(msg == '')
            {
                $('.msg').text('Message field is required');
                return false;
            }
            if(quoteReqId == '')
            {
                alert('Select any request');
                return false;
            }
        }
    }
    
    function removeError(name)
    {
        $('.'+name).text('');
    }
    
    $(document).ready(function() {
        $('.table-bordered').DataTable( {
            "order": [[ 2, "desc" ]]
        } );
    } );
 </script>