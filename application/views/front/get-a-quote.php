<?php include('include/header.php')?>
<?php include('include/navbar.php')?>
<!--Breadcrumb Start-->
<div class="breadcrumb-area" style="background-image: url('<?= base_url() ?>assets/themes/immersive/images/about/about-us-banner-bg.png')">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner text-center">
                    <h1 class="page-title">Get a Quote</h1>
                    <ul class="page-list">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li>Get a Quote</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Breadcrumb End-->
<div class="quote-page padding-top-115 padding-bottom-100">
    <div class="container">
        <div class="row">
            <div class="offset-lg-1 col-lg-10 col-12">
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <div class="panel_s">
                            <div class="panel-body">
                                <div class="get-quote-wrap">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs nav-justified">
                                        <li class="nav-item">
                                            <a class="nav-link active uppercase new" data-toggle="tab" href="#home">Request New Quote</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link uppercase draft" data-toggle="tab" href="#menu1">Modify Existing Draft Request</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link uppercase preview" data-toggle="tab" href="#menu2">Reuse Previous Request</a>
                                        </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane container active" id="home">
                                            <div class="request-wrapper">
                                                <h3><b>Request New Quote</b></h3><hr>
                                                <!--<form class="request-form" id="" >-->
                                                <?php echo form_open_multipart(base_url('customers/getAQuotePost'),array('class'=>'request-form', 'id'=>'quoteform', 'onsubmit'=>'return validateQuoteForm()')); ?>
                                                    <div class="row">
                                                        <div class="col-12 col-xl-6 col-lg-6">
                                                            <div class="form-group">
                                                                <label class="label">Description<sup style="color: red;">*</sup></label>
                                                                <input type="text" name="title" id="title" maxlength="950" onkeyup="errorremove('title')" class="form-control" />
                                                                <p style="color: red;" class="title"></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-xl-6 col-lg-6">
                                                            <div class="form-group">
                                                                <label class="label">Choose Your Location<sup style="color: red;">*</sup></label>
                                                                <select name="locationid" id="locationid" onchange="errorremove('locationid')" class="form-control chosen-select">
                                                                    <option value=""><option>
                                                                    <?php
                                                                        if($city_result)
                                                                        {
                                                                            foreach($city_result as $rrr)
                                                                            {
                                                                                ?>
                                                                                    <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                    ?>
                                                                </select>
                                                                <p style="color: red;" class="locationid"></p>
                                                            </div>
                                                        </div>
														<div class="col-12 col-xl-4 col-lg-4" style="display:none;">
                                                            <div class="form-group">
                                                                <label class="label">Address<sup style="color: red;">*</sup></label>
																<textarea name="address" id="address" maxlength="35" class="form-control"></textarea>
																<p style="color: red;" class="address"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 col-xl-6 col-lg-6">
                                                            <div class="form-group">
                                                                <label class="label">Choose Service Category<sup style="color: red;">*</sup></label>
                                                                <select class="form-control chosen-select" name="categoryid[]" id="categoryid" onchange="getSubCategory(this.value);">
                                                                    <option value="">Select Categories<option>
                                                                    <?php
                                                                        if($category_result)
                                                                        {
                                                                            foreach($category_result as $res)
                                                                            {
                                                                                ?>
                                                                                    <option value="<?= $res->id; ?>"><?= $res->name; ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                    ?>
                                                                </select>
                                                                <p style="color: red;" class="categoryid"></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-xl-5 col-lg-5">
                                                            <div class="form-group">
                                                                <label class="label">Choose Service Sub Category<sup style="color: red;">*</sup></label>
                                                                <select class="chosen-select" name="subcategoryid[]" onchange="errorremove('sub_category_id')" id="sub_category_id">
                                                                    <option value="">Select sub category<option>
                                                                </select>
                                                                <p style="color: red;" class="sub_category_id"></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-xl-1 col-lg-1">
                                                            <div class="form-group">
                                                                <label class="label">&nbsp;</label>
                                                                <span class="btn btn-info" style="cursor:pointer" onclick="addMoreCatSubCat()"><i class="fa fa-plus"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="addmorecat">
                                                        
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 col-xl-6 col-lg-6">
                                                            <div class="form-group">
                                                                <label class="label">Choose Vehicle Category<sup style="color: red;">*</sup></label>
                                                                <select name="vehiclecategory" id="vehiclecategory" onchange="errorremove('vehiclecategory')"  class="form-control chosen-select">
                                                                    <option value="">Select vehicle<option>
                                                                    <option value="Sedan">Sedan</option>
                                                                    <option value="SUV">SUV</option> 
                                                                    <option value="Truck">Truck</option> 
                                                                </select>
                                                                <p style="color: red;" class="vehiclecategory"></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-xl-6 col-lg-6">
                                                            <div class="form-group">
                                                                <label class="label">Choose Car Brand<sup style="color: red;">*</sup></label>
                                                                <select class="form-control chosen-select" id="make_id" name="brandid" onChange="getModelList(this.value)">
                                                                    <option value=""></option>
                                                                    <?php
                                                                        if($carmake_result)
                                                                        {
                                                                            foreach($carmake_result as $res)
                                                                            {
                                                                                ?>
                                                                                    <option value="<?= $res->id; ?>"><?= $res->name; ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                    ?>
                                                                </select>
                                                                <p style="color: red;" class="make_id"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 col-xl-6 col-lg-6">
                                                            <div class="form-group">
                                                                <label class="label">Choose Car Model<sup style="color: red;">*</sup></label>
                                                                <select class="form-control chosen-select" name="carmodelid" id="model_id" onChange="getYearList(this.value)">
                                                                    <option>Select car model<option>
                                                                </select>
                                                                <p style="color: red;" class="model_id"></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-xl-6 col-lg-6">
                                                            <div class="form-group">
                                                                <label class="label">Choose Car Make Year<sup style="color: red;">*</sup></label>
                                                                <select class="form-control chosen-select" name="carmakeyearid" onchange="errorremove('year_id')" id="year_id">
                                                                    <option>Select Year<option>
                                                                </select>
                                                                <p style="color: red;" class="year_id"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6 col-xl-6 col-lg-6">
                                                            <div class="form-group">
                                                                <label class="label">Upload Vehicle Registration Card<sup style="color: red;">*</sup><!--<span class="pull-right">Or</span>--></label>
                                                                <div class="bg-white mb-20">
                                                                    <input type="file" class="imgInp" name="registrationcard" id="file-upload">
                                                                    <!--<label for="file-upload" class="file-upload mb-0">Browse</label>-->
                                                                    <div id="file-upload-filename"></div>
                                                                </div>
                                                                <img id="blah" height="100px" width="100px" alt=""  />
                                                            </div>
                                                        </div>
                                                        <div class="col-6 col-xl-6 col-lg-6">
                                                            <div class="form-group">
                                                                <label class="label">Chassis No<sup style="color: red;">*</sup></label>
                                                                <input type="text" class="form-control" maxlength="50" id="chassis_no" onkeyup="errorremove('chassis_no')" name="chassis_no" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 col-xl-12 col-lg-12">
                                                            <div class="form-group">
                                                                <label class="label">Detailed Description</label>
                                                                <textarea class="form-control" rows="4" id="description" name="description" placeholder="Detailed description here"></textarea>
                                                                <p style="color: red;" class="description"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 col-xl-12 col-lg-12">
                                                            <div class="form-group">
                                                                <label class="label">Upload photo / audio / video<b class="text-info"><sub>Note: allow extension ( "jpg", "jpeg", "gif", "png", "mp3", "mp4", "wma" ) </sub></span></b></label>
                                                                <div class="bg-white mb-20">
                                                                    <input type="file" name="upload_photo_video" accept="image/png, image/jpeg, image/gif, image/jpg, video/mp4, video/wma" id="file-upload1">
                                                                    <!--<label for="file-upload1" class="file-upload mb-0">Browse</label>-->
                                                                    <div id="file-upload1-filename"></div>
                                                                    <p style="color: red;" class="upload1"></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 col-xl-12 col-lg-12">
                                                            <div class="main-btn-wrap">
                                                                <button type="submit" name="submit" value="submit" class="btn main-btn uppercase mt-2">Get Quote</button>
                                                                <button type="submit" name="submit" value="save" class="btn main-btn uppercase mt-2">Save and Continue Later</button>
                                                                <a href="<?= base_url(); ?>" type="button" class="btn main-btn uppercase mt-2">Cancel and Go To Home Page</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-xl-12 col-lg-12">
                                                            <p style="color: red;" class="title"></p>
                                                            <p style="color: red;" class="chassis_no"></p>
                                                            <p style="color: red;" class="upload1"></p>
                                                            <p style="color: red;" class="description"></p>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="tab-pane container fade" id="menu1">
                                            <div class="modify-request-wrap">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered">
                                                        <tbody>
                                                            <?php
                                                                if($quoteResult_draft)
                                                                {
                                                                    foreach($quoteResult_draft as $res)
                                                                    {
                                                                        ?>
                                                                            <tr>
                                                                                <td class="title-td"><?= $res->title; ?></td>
                                                                                <td><?= date('d-M-Y', strtotime($res->created_date)); ?></td>
                                                                                <td>
                                                                                    <?php
                                                                                        $categoryArr = [];
                                                                                        $categoryArr = explode(',',$res->categoryid);
                                                                                        $f = 1;
                                                                                        foreach($categoryArr as $c)
                                                                                        {
                                                                                            if($f == 1)
                                                                                               echo categoryname($c);
                                                                                            else
                                                                                                echo ', '.categoryname($c);
                                                                                            $f++;
                                                                                        }
                                                                                    ?>
                                                                                </td>
                                                                                <td>
                                                                                    <div class="btn-group" role="group" aria-label="Basic example">
                                                                                        <button type="button" class="btn btn-confirm modify-btn mr-3 btn-sm" onClick="getDraftReq(<?= $res->id ?>)"><i class="fa fa-edit" aria-hidden="true"></i> Modify</button>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        <?php
                                                                    }
                                                                }
                                                            ?> 
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="modify-wrapper request-wrapper">
                                                <h3><b>Modify Request</b></h3>
                                                <div class="draftrequest">  </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane container fade" id="menu2">
                                            <div class="reuse-request-wrap">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered">
                                                        <tbody>
                                                            <?php
                                                                if($quoteResult_reuse)
                                                                {
                                                                    foreach($quoteResult_reuse as $r)
                                                                    {
                                                                        ?>
                                                                            <tr>
                                                                                <td class="title-td"><?= $r->title; ?></td>
                                                                                <td><?= date('d-M-Y', strtotime($r->created_date)); ?></td>
                                                                                <td>
                                                                                    <?php
                                                                                        $categoryArr = [];
                                                                                        $categoryArr = explode(',',$r->categoryid);
                                                                                        $f = 1;
                                                                                        foreach($categoryArr as $c)
                                                                                        {
                                                                                            if($f == 1)
                                                                                               echo categoryname($c);
                                                                                            else
                                                                                                echo ', '.categoryname($c);
                                                                                            $f++;
                                                                                        }
                                                                                    ?>
                                                                                </td>
                                                                                <td>
                                                                                    <div class="btn-group" role="group" aria-label="Basic example">
                                                                                        <button type="button" class="btn btn-confirm reuse-btn mr-3 btn-sm" onClick="getReuseReq(<?= $r->id ?>)"><i class="fa fa-reply" aria-hidden="true"></i> Reuse</button>
                                                                                    </div>
                                                                                </td>
                                                                            </tr> 
                                                                        <?php
                                                                    }
                                                                }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="reuse-wrapper request-wrapper">
                                                <h3><b>Reuse Request</b></h3>
                                                <hr><div class="previewrequest">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('include/footer.php')?>
<?php include('include/scripts.php')?>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>
    $(".modify-btn").click(function(){
        $(".modify-request-wrap").hide();
        $(".modify-wrapper").show();
    });
    $(".draft-btn").click(function(){
        $(".modify-request-wrap").show();
        $(".modify-wrapper").hide();
    });
    $(".new").click(function(){
        $(".modify-request-wrap").show();
        $(".modify-wrapper").hide();
    });
    $(".draft").click(function(){
        $(".modify-request-wrap").show();
        $(".modify-wrapper").hide();
    });
    $(".preview").click(function(){
        $(".reuse-request-wrap").show();
        $(".reuse-wrapper").hide();
    });

    $(".reuse-btn").click(function(){
        $(".reuse-request-wrap").hide();
        $(".reuse-wrapper").show();
    });
    $(".drafts-btn").click(function(){
        $(".reuse-request-wrap").show();
        $(".reuse-wrapper").hide();
    });
    
    /* Get Draft Request */
    function getDraftReq(id)
    {
        $('.draftrequest').html('');
        if(id)
        {
            var str = "id="+id+"&"+csrfData['token_name']+"="+csrfData['hash'];
    	    $.ajax({
    	        url: '<?= site_url()?>customers/getDraftReq',
    	        type: 'POST',
    	        data: str,
    	        cache: false,
    	        success: function(resp){
    	            if(resp)
    	            {
    	                $('.draftrequest').html(resp);
    	            }
    	            else
    	            {
    	                $('.draftrequest').html('');
    	            }
    	        }
    	    });
        }
        else
        {
            $('.draftrequest').html('');
        }
    }
    
    /* Get Draft Request */
    function getPreviewReq(id)
    {
        $('.previewrequest').html('Loading...');
        if(id)
        {
            var str = "id="+id+"&"+csrfData['token_name']+"="+csrfData['hash'];
    	    $.ajax({
    	        url: '<?= site_url()?>customers/getPreviewReq',
    	        type: 'POST',
    	        data: str,
    	        cache: false,
    	        success: function(resp){
    	            if(resp)
    	            {
    	                $('.previewrequest').html(resp);
    	            }
    	            else
    	            {
    	                $('.previewrequest').html('<p valign="top" colspan="4" class="dataTables_empty">No matching records found</p>');
    	            }
    	        }
    	    });
        }
        else
        {
            $('.previewrequest').html('');
        }
    }
    /* Get Reuse Request */
    function getReuseReq(id)
    {
        $('.previewrequest').html('Loading...');
        if(id)
        {
            var str = "id="+id+"&"+csrfData['token_name']+"="+csrfData['hash'];
    	    $.ajax({
    	        url: '<?= site_url()?>customers/getReuseReq',
    	        type: 'POST',
    	        data: str,
    	        cache: false,
    	        success: function(resp){
    	            if(resp)
    	            {
    	                $('.previewrequest').html(resp);
    	            }
    	            else
    	            {
    	                $('.previewrequest').html('<p valign="top" colspan="4" class="dataTables_empty">No matching records found</p>');
    	            }
    	        }
    	    });
        }
        else
        {
            $('.previewrequest').html('');
        }
    }
    
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }
    
    $(".imgInp").change(function() {
      readURL(this);
    });
    
    function getSubCategoryExtra(Id,sn)
    {
        $('#sub_category_id'+sn).html('<option value=""></option>');
        var str = "catid="+Id+"&"+csrfData['token_name']+"="+csrfData['hash'];
	    $.ajax({
	        url: '<?= site_url()?>customers/getSubCategory',
	        type: 'POST',
	        data: str,
	        dataType: 'json',
	        cache: false,
	        success: function(resp){
	            $resp = '<option value=""></option>';
                for(var i=0; i< resp.length; i++)
                {
                    $resp += '<option value="'+resp[i].id+'">'+resp[i].name+'</option>';
                }
                $('#sub_category_id'+sn).html($resp);
                setTimeout(function(){  
                    $('#sub_category_id'+sn).chosen('destroy'); 
                    $('#sub_category_id'+sn).chosen(); 
                }, 1000);
	        }
	    });
    }
    
    var mcategory = 0;
    function addMoreCatSubCat()
    {
        mcategory += 1;
        var category_result = JSON.parse('<?= json_encode($category_result); ?>');
        var html = '';
        html += '<div class="row" id="removecat_'+mcategory+'">';
        html += '<div class="col-12 col-xl-6 col-lg-6">';
        html += '<div class="form-group">';
        html += '<label class="label">Choose Service Category</label>';
        html += '<select class="form-control chosen-select dclass" name="categoryid[]" onchange="getSubCategoryExtra(this.value,'+mcategory+');">';
        html += '<option value="">Select Categories<option>';
        for(var s=0; s < category_result.length; s++)
        {
            html +='<option value="'+category_result[s].id+'">'+category_result[s].name+'</option>';
        }
        html += '</select>';
        html += '</div>';
        html += '</div>';
        html += '<div class="col-12 col-xl-5 col-lg-5">';
        html += '<div class="form-group">';
        html += '<label class="label">Choose Service Sub Category</label>';
        html += '<select class="form-control" name="subcategoryid[]" id="sub_category_id'+mcategory+'">';
        html += '<option value="">Select sub category<option>';
        html += '</select>';
        html += '</div>';
        html += '</div>';
        html += '<div class="col-12 col-xl-1 col-lg-1">';
        html += '<div class="form-group">';
        html += '<label class="label">&nbsp;</label>';
        html += '<span class="btn btn-warning" style="cursor:pointer" onclick="removeMoreCatSubCat('+mcategory+')"><i class="fa fa-trash"></i></span>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        
        setTimeout(function(){  
            $('.dclass').chosen('destroy'); 
            $('.dclass').chosen(); 
        }, 1000);
        $('.addmorecat').append(html);
    }
    
    function removeMoreCatSubCat(id)
    {
        $('#removecat_'+id).empty();
    }
    
    /*$(function(){
        appValidateForm($('#quoteform'),{upload_photo_video:{required:true,extension: "csv"}});
    });*/
    
    //$( "#quoteform" ).validate();
</script>