<?php include('include/header.php')?>
<?php include('include/navbar.php')?>

<!--Breadcrumb Start-->
<div class="breadcrumb-area" style="background-image: url('<?= site_url() ?>assets/themes/immersive/images/about/about-us-banner-bg.png')">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner text-center">
                    <h1 class="page-title">Buy-now</h1>
                    <ul class="page-list">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li>Buy-now</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Breadcrumb End-->
<div class="terms-and-conditions-page padding-top-115 padding-bottom-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="request-wrapper">
                    <h4><b>Buy Now</b></h4>
                    <hr>
                    <?= form_open('',array('onsubmit' => 'return bookValidateForm()')); ?>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="location" class="mb-5"><?= $garagename; ?></label>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="location" class="mb-5">Rating: 
                                    <?php
                                        $garagerating = $garageRating[0]->rating;
                                        if($garagerating > 0)
                                        {
                                            $str = '';
                                            for($i=0; $i < intval(5); $i++)
                                            {
                                                if($i < intval($garagerating))
                                                {
                                                    $str .= '<i class="fa fa-star" aria-hidden="true"></i>';   
                                                }
                                                else
                                                {
                                                    $str .= '<i class="fa fa-star-o" aria-hidden="true"></i>';
                                                }
                                            }
                                            echo $str;
                                        }
                                        else
                                        {
                                            echo '<i class="fa fa-star" aria-hidden="true"></i>';
                                            echo '<i class="fa fa-star-o" aria-hidden="true"></i>';
                                            echo '<i class="fa fa-star-o" aria-hidden="true"></i>';
                                            echo '<i class="fa fa-star-o" aria-hidden="true"></i>';
                                            echo '<i class="fa fa-star-o" aria-hidden="true"></i>';
                                        }
                                    ?>
                                </label>
                            </div>
                        </div><hr>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="location" class="mb-5">Garage Address</label>
                                <input type="text" placeholder="Location" id="product" value="<?= $address; ?>" readonly>
                                <input type="hide" name="productid" value="<?= $pid; ?>" style="display:none;">
                                <input type="hide" name="garageid" value="<?= $id; ?>" style="display:none;"> 
                            </div>
                        </div>
                        <div class="row" style="display:none;">
                            <div class="form-group col-md-12">
                                <label for="location" class="mb-5">Pickup Address</label>
                                <input id="autocomplete" name="pickup_address" placeholder="Pickup address" type="text">
                                <input type="hidden" id="street_number">
                                <input type="hidden" name="route" id="route">
                                <input type="hidden" name="city" id="locality">
                                <input type="hidden" name="area" id="administrative_area_level_1">
                                <input type="hidden" name="postal_code" id="postal_code">
                                <input type="hidden" name="country" id="country">
                                <input type="hidden" name="latitude" id="latitude">
                                <input type="hidden" name="longitude" id="longitude">
                            </div>
                        </div>
                        <div class="row" style="display:none;">
                            <div class="form-group col-md-12">
                                <label for="location" class="mb-5">Delivery Address</label>
                                <input type="text" name="delivery_address" id="autocomplete2" placeholder="Delivery address">
                                <input type="hidden" id="street_number_to">
                                <input type="hidden" name="route_to" id="route_to">
                                <input type="hidden" name="city_to" id="locality_to">
                                <input type="hidden" name="area_to" id="administrative_area_level_1_to">
                                <input type="hidden" name="postal_code_to" id="postal_code_to">
                                <input type="hidden" name="country_to" id="country_to">
                                <input type="hidden" name="latitude_to" id="latitude_to">
                                <input type="hidden" name="longitude_to" id="longitude_to">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="location" class="mb-5">Quantity</label>
                                <input type="text" minlength="1" maxlength="3" placeholder="Quantity" id="qty" name="qty">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-8">
                                <div id="filterDate2">
                                    <label class="control-label">Book Appointment</label>
                                    <div class='input-group date' id='datetimepicker1' data-date-format="dd.mm.yyyy">
                                        <input type='text' name="appointment_date" autocomplete="off" required class="form-control" placeholder="dd.mm.yyyy"/>
                                        <span class="input-group-addon" style="position: absolute;right: 0px;width: 45px;min-height: 50px;height: 50px;background-color: #eee;padding: 13px 15px;">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div id="filterDate2">
                                    <label class="control-label">Book Time</label>
                                    <div class="input-group">
                                        <select name="bookingtime" id="bookingtime" onchange="errorremove('error_bookingtime');" data-placeholder="Choose a time..." class="form-control chosen-select" >
                                            <option value=""></option>
                                            <option value="5:00 - 6:00">5:00 - 6:00</option>
                                            <option value="6:00 - 7:00">6:00 - 7:00</option>
                                            <option value="7:00 - 8:00">7:00 - 8:00</option>
                                            <option value="8:00 - 9:00">8:00 - 9:00</option>
                                            <option value="9:00 - 10:00">9:00 - 10:00</option>
                                            <option value="10:00 - 11:00">10:00 - 11:00</option>
                                            <option value="11:00 - 12:00">11:00 - 12:00</option>
                                            <option value="12:00 - 13:00">12:00 - 13:00</option>
                                            <option value="13:00 - 14:00">13:00 - 14:00</option>
                                            <option value="14:00 - 15:00">14:00 - 15:00</option>
                                            <option value="15:00 - 16:00">15:00 - 16:00</option>
                                            <option value="16:00 - 17:00">16:00 - 17:00</option>
                                            <option value="17:00 - 18:00">17:00 - 18:00</option>
                                            <option value="18:00 - 19:00">18:00 - 19:00</option>
                                            <option value="19:00 - 20:00">19:00 - 20:00</option>
                                            <option value="20:00 - 21:00">20:00 - 21:00</option>
                                        </select>
                                        <div class="text-danger error_bookingtime"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="location" class="mb-5">Fitting Option With Price</label>                                
                                    <div class="radio-btn">
                                        <?php
                                            if($product_price->offer_start_date < time() && $product_price->offer_end_date  > time())
                                            {
                                                ?>
                                                    <span class="d-inline-block"><input class="width-auto" type="radio" name="fitting_type" value="With_Fitting"> AED <?= $product_price->price_before_offer; ?> With Fitting</span>
                                                <?php
                                            }
                                            else
                                            {
                                                if($product_price->price_with_fitting != '')
                                                {
                                                    ?>
                                                        <span class="d-inline-block"><input class="width-auto" type="radio" name="fitting_type" value="With_Fitting"> AED <?= $product_price->price_with_fitting; ?> With Fitting </span>
                                                    <?php
                                                }
                                            }
                                            
                                            if($product_price->offer_start_date_without < time() && $product_price->offer_end_date_without  > time())
                                            {
                                                ?>
                                                    <span class="d-inline-block"><input type="radio" class="width-auto" name="fitting_type" value="Without_Fitting"> AED <?= $product_price->price_before_offer_without; ?> Without Fitting</span>
                                                <?php
                                            }
                                            else
                                            {
                                                if($product_price->price_without_fitting != '')
                                                {
                                                    ?>
                                                        <span class="d-inline-block"><input type="radio" class="width-auto" name="fitting_type" value="Without_Fitting"> AED <?= $product_price->price_without_fitting; ?> Without Fitting</span>
                                                    <?php
                                                }
                                            }
                                        ?> 
                                    </div>
                                </div>
                            </div>
                        </div><hr>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <button type="submit" class="btn btn-outline-success btn-md"> Buy Now</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('include/footer.php')?>
<?php include('include/scripts.php')?>
<script type="text/javascript">
    $('.input-group.date').datepicker({autoclose: true,format: "dd.mm.yyyy",startDate: "+1d"}); 
    $('#datetimepicker2').datetimepicker({
        datepicker:false,
        format:'h:i a'
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJ2Tlhs9oB6ByIP4yPWBu-aFAVrhLWt2M&libraries=places&callback=initialize"
         async defer></script>
<script>// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

$("#autocomplete").on('focus', function () {
    geolocate();
});
 

var placeSearch, autocomplete;
var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
};
 


function initialize() {
    // Create the autocomplete object, restricting the search
    // to geographical location types.
    autocomplete = new google.maps.places.Autocomplete(
    /** @type {HTMLInputElement} */ (document.getElementById('autocomplete')), {
        types: ['geocode']
    });
 
    // populate the address fields in the form.
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        fillInAddress();
    });
    
     autocomplete2 = new google.maps.places.Autocomplete(
    /** @type {HTMLInputElement} */ (document.getElementById('autocomplete2')), {
        types: ['geocode']
    });
 
    // populate the address fields in the form.
    google.maps.event.addListener(autocomplete2, 'place_changed', function () {
        fillInAddressTo();
    });
    
    
    
     autocomplete3 = new google.maps.places.Autocomplete(
    /** @type {HTMLInputElement} */ (document.getElementById('autocomplete3')), {
        types: ['geocode']
    });
 
    // populate the address fields in the form.
    google.maps.event.addListener(autocomplete3, 'place_changed', function () {
        fillInAddress3();
    });
  
}
 

// [START region_fillform]
function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();
 vm.immediateShipping.fromAddress = place.formatted_address;
 vm.immediateShipping.from_address = place.formatted_address;
    document.getElementById("latitude").value = place.geometry.location.lat();
    document.getElementById("longitude").value = place.geometry.location.lng();
    
      vm.immediateShipping.from_latitude =  place.geometry.location.lat();
      vm.immediateShipping.from_longitude =  place.geometry.location.lng();
      
    for (var component in componentForm) {
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        // console.log(addressType);
        if (componentForm[addressType]) {
        // console.log(place.address_components[i][componentForm[addressType]]);
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
             if(addressType =='street_number'){
                vm.immediateShipping.from_street_number = val;
            } 
            if(addressType =='route'){
                vm.immediateShipping.from_route = val;
            } 
             if(addressType =='locality'){
                vm.immediateShipping.from_locality = val;
                vm.immediateShipping.from_city = val;
            } 
             if(addressType =='administrative_area_level_1'){
                vm.immediateShipping.administrative_area_level_1 = val;
                vm.immediateShipping.from_state = val;
            } 
            if(addressType =='country'){
                vm.immediateShipping.from_country = val;
            } 
            if(addressType =='postal_code'){
                vm.immediateShipping.from_postal_code = val;
                vm.immediateShipping.from_origin_pin_code = val;
            } 
        }
    }
}

// [START region_fillform]
function fillInAddressTo() {
    // Get the place details from the autocomplete object.
    var place = autocomplete2.getPlace();
        vm.immediateShipping.toAddress = place.formatted_address;
        vm.immediateShipping.to_address = place.formatted_address;
    document.getElementById("latitude_to").value = place.geometry.location.lat();
    document.getElementById("longitude_to").value = place.geometry.location.lng();
    
        vm.immediateShipping.latitude_to =  place.geometry.location.lat();
        vm.immediateShipping.longitude_to =  place.geometry.location.lng();
      
    for (var component in componentForm) {
        document.getElementById(component+'_to').value = '';
        document.getElementById(component+'_to').disabled = false;
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
       
        if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType+'_to').value = val;
             
            if(addressType =='street_number'){
                vm.immediateShipping.to_street_number = val;
            } 
            if(addressType =='route'){
                vm.immediateShipping.to_route = val;
            } 
             if(addressType =='locality'){
                vm.immediateShipping.to_locality = val;
                  vm.immediateShipping.to_city = val;
            } 
             if(addressType =='administrative_area_level_1'){
                vm.immediateShipping.to_administrative_area_level_1 = val;
                vm.immediateShipping.to_state = val;
            } 
            if(addressType =='country'){
                vm.immediateShipping.to_country = val;
            } 
            if(addressType =='postal_code'){
                vm.immediateShipping.to_postal_code = val;
                vm.immediateShipping.to_origin_pin_code = val;
            } 
        }
    }
}

 



function fillInAddress3() {
    // Get the place details from the autocomplete object.
    var place = autocomplete3.getPlace();

    document.getElementById("latitude").value = place.geometry.location.lat();
    document.getElementById("longitude").value = place.geometry.location.lng();
    
        vm.SignUp.latitude =  place.geometry.location.lat();
        vm.SignUp.longitude =  place.geometry.location.lng();
      
    for (var component in componentForm) {
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
       
        if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
             
            if(addressType =='street_number'){
                
                vm.SignUp.street_number = val;
            } 
            if(addressType =='route'){
                vm.SignUp.route = val;
            } 
             if(addressType =='locality'){
                vm.SignUp.locality = val;
            } 
             if(addressType =='administrative_area_level_1'){
                vm.SignUp.administrative_area_level_1 = val;
            } 
            if(addressType =='country'){
                vm.SignUp.country = val;
            } 
            if(addressType =='postal_code'){
                vm.SignUp.postal_code = val;
            } 
        }
    }
}

// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var geolocation = new google.maps.LatLng(
            position.coords.latitude, position.coords.longitude);

            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            document.getElementById("latitude").value = latitude;
            document.getElementById("longitude").value = longitude;

            autocomplete.setBounds(new google.maps.LatLngBounds(geolocation, geolocation));
            autocomplete.setComponentRestrictions({'country': 'USA'}); 
        });
    }
}

initialize();

// [END region_geolocation]

/*$('#myCarousel').carousel({
    pause: 'none'
})*/

function bookValidateForm()
{
    var  bookingtime = $("#bookingtime").chosen().val();
    if(bookingtime == '')
    {
        $('.error_bookingtime').text('Enter booking time');
        return false;
    }
    else
    {
        $('.error_bookingtime').text('');
    }
}
</script>