<?php include('include/header.php')?>
<?php include('include/navbar.php')?>
<!--Breadcrumb Start-->
<div class="breadcrumb-area" style="background-image: url('assets/themes/immersive/images/about/about-us-banner-bg.png')">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner text-center">
                    <h1 class="page-title">Product-listing</h1>
                    <ul class="page-list">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li>Product-listing</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Breadcrumb End-->
<div class="terms-and-conditions-page padding-top-115 padding-bottom-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <section class="product-list-wrap">
                    <h4>Bumpers</h4>
                    <div class="row">
                        <div class="col-md-9">
                            <a href="<?= base_url('customers/productDetails'); ?>" style="display:block;">
                                <div class="product-listing-wrapper">
                                    <div class="product-list">
                                        <div class="image-holder">
                                            <img src="assets/images/bumper_bracket.webp" alt="bumper" class="img-fluid">
                                        </div>
                                        <div class="product-content">
                                            <h5 class="title"><b>Bumper Bracket</b></h5>
                                            <span class="description">Chrome Shell with Painted Black Insert</span><br>
                                            <span class="more-description">Front, Lower Bumper Cover - Plastic, Without Sport Pckg, Without Fog Light Holes</span><br>
                                            <span class="part-number">Part Number: 9363-1</span><br>
                                            <span class="price">AED 130</span>
                                        </div>
                                    </div>
                                    <hr>
                                    <div tabindex="0" class="see-all-products-details text-right">
                                        <span class="styledtext-sc"><a href="<?= base_url('customers/productDetails'); ?>">See All Products Details</a></span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="<?= base_url('customers/garageDetails'); ?>" class="btn btn-outline-info btn-xl btn-block"><i class="fa fa-folder-o" aria-hidden="true"></i> Garage Details</a><br>
                            <a href="<?= base_url('send-enquiry'); ?>" class="btn btn-outline-success btn-xl btn-block"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send Enquiry</a><br>
                            <a href="<?= base_url('buy-now');?>" class="btn btn-outline-warning btn-xl btn-block"><i class="fa fa-credit-card" aria-hidden="true"></i> Buy Now</a>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-9">
                            <a href="<?= base_url('customers/productDetails'); ?>" style="display:block;">
                                <div class="product-listing-wrapper">
                                    <div class="product-list">
                                        <div class="image-holder">
                                            <img src="assets/images/bumper_cover.webp" alt="bumper" class="img-fluid">
                                        </div>
                                        <div class="product-content">
                                            <h5 class="title"><b>Bumper Cover</b></h5>
                                            <span class="description">Chrome Shell with Painted Black Insert</span><br>
                                            <span class="more-description">Replacement Bumper Cover, Bumper and Fog Light Kit</span><br>
                                            <span class="part-number">Part Number: 9363-1</span><br>
                                            <span class="price">AED 250</span>
                                        </div>
                                    </div>
                                    <hr>
                                    <div tabindex="0" class="see-all-products-details text-right">
                                        <span class="styledtext-sc"><a href="<?= base_url('customers/productDetails'); ?>">See All Products Details</a></span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="<?= base_url('customers/garageDetails'); ?>" class="btn btn-outline-info btn-xl btn-block"><i class="fa fa-folder-o" aria-hidden="true"></i> Garage Details</a><br>
                            <a href="javascript:void(0);" class="btn btn-outline-success btn-xl btn-block"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send Enquiry</a><br>
                            <a href="<?= base_url('buy-now');?>" class="btn btn-outline-warning btn-xl btn-block"><i class="fa fa-credit-card" aria-hidden="true"></i> Buy Now</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-9">
                            <a href="<?= base_url('customers/productDetails'); ?>" style="display:block;">
                                <div class="product-listing-wrapper">
                                    <div class="product-list">
                                        <div class="image-holder">
                                            <img src="assets/images/bumper_end.webp" alt="bumper" class="img-fluid">
                                        </div>
                                        <div class="product-content">
                                            <h5 class="title"><b>Bumper End</b></h5>
                                            <span class="description">Chrome Shell with Painted Black Insert</span><br>
                                            <span class="more-description">Front, Driver and Passenger Side Plastic Bumper End, Chrome</span><br>
                                            <span class="part-number">Part Number: 9363-1</span><br>
                                            <span class="price">AED 30</span>
                                        </div>
                                    </div>
                                    <hr>
                                    <div tabindex="0" class="see-all-products-details text-right">
                                        <span class="styledtext-sc"><a href="<?= base_url('customers/productDetails'); ?>">See All Products Details</a></span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="<?= base_url('customers/garageDetails'); ?>" class="btn btn-outline-info btn-xl btn-block"><i class="fa fa-folder-o" aria-hidden="true"></i> Garage Details</a><br>
                            <a href="javascript:void(0);" class="btn btn-outline-success btn-xl btn-block"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send Enquiry</a><br>
                            <a href="<?= base_url('buy-now');?>" class="btn btn-outline-warning btn-xl btn-block"><i class="fa fa-credit-card" aria-hidden="true"></i> Buy Now</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-9">
                            <a href="<?= base_url('customers/productDetails'); ?>" style="display:block;">
                                <div class="product-listing-wrapper">
                                    <div class="product-list">
                                        <div class="image-holder">
                                            <img src="assets/images/bumper_trim.webp" alt="bumper" class="img-fluid">
                                        </div>
                                        <div class="product-content">
                                            <h5 class="title"><b>Bumper Trim</b></h5>
                                            <span class="description">Chrome Shell with Painted Black Insert</span><br>
                                            <span class="more-description">Front, Driver and Passenger Side Bumper Trim, Chrome</span><br>
                                            <span class="part-number">Part Number: 9363-1</span><br>
                                            <span class="price">AED 90</span>
                                        </div>
                                    </div>
                                    <hr>
                                    <div tabindex="0" class="see-all-products-details text-right">
                                        <span class="styledtext-sc"><a href="<?= base_url('customers/productDetails'); ?>">See All Products Details</a></span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="<?= base_url('customers/garageDetails'); ?>" class="btn btn-outline-info btn-xl btn-block"><i class="fa fa-folder-o" aria-hidden="true"></i> Garage Details</a><br>
                            <a href="javascript:void(0);" class="btn btn-outline-success btn-xl btn-block"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send Enquiry</a><br>
                            <a href="<?= base_url('buy-now');?>" class="btn btn-outline-warning btn-xl btn-block"><i class="fa fa-credit-card" aria-hidden="true"></i> Buy Now</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-9">
                            <a href="<?= base_url('customers/productDetails'); ?>" style="display:block;">
                                <div class="product-listing-wrapper">
                                    <div class="product-list">
                                        <div class="image-holder">
                                            <img src="assets/images/bumper_filler.webp" alt="bumper" class="img-fluid">
                                        </div>
                                        <div class="product-content">
                                            <h5 class="title"><b>Bumper Filler</b></h5>
                                            <span class="description">Chrome Shell with Painted Black Insert</span><br>
                                            <span class="more-description">Front, Lower Bumper Cover - Plastic, Without Sport Pckg, Without Fog Light Holes</span><br>
                                            <span class="part-number">Part Number: 9363-1</span><br>
                                            <span class="price">AED 300</span>
                                        </div>
                                    </div>
                                    <hr>
                                    <div tabindex="0" class="see-all-products-details text-right">
                                        <span class="styledtext-sc"><a href="<?= base_url('customers/productDetails'); ?>">See All Products Details</a></span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="<?= base_url('customers/garageDetails'); ?>" class="btn btn-outline-info btn-xl btn-block"><i class="fa fa-folder-o" aria-hidden="true"></i> Garage Details</a><br>
                            <a href="javascript:void(0);" class="btn btn-outline-success btn-xl btn-block"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send Enquiry</a><br>
                            <a href="<?= base_url('buy-now');?>" class="btn btn-outline-warning btn-xl btn-block"><i class="fa fa-credit-card" aria-hidden="true"></i> Buy Now</a>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

<?php include('include/footer.php')?>
<?php include('include/scripts.php')?>