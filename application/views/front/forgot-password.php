<?php include('include/header.php')?>
<?php
  if($settingRes)
  {
    if($settingRes->background_type == 'color')
    {
        ?>
          <body style="background-color: <?= $settingRes->background_color; ?>;">
        <?php
    }
    else
    {
        ?>
          <body style="background-image: url('<?= base_url() ?>uploads/loginPage/<?= $settingRes->background_image; ?>');">
        <?php
    }
  }
  else
  {
    ?>
     <body class="login_admin"<?php if(is_rtl()){ echo ' dir="rtl"'; } ?>>
    <?php
  }
?>
<div class="sign sign-area padding-120">
    <div class="container">
        <div class="row">
            <div class="offset-lg-4 col-lg-4">
                <div class="logo-wrapper text-center mb-40">
                    <a href="<?= base_url(); ?>">
                        <!--<img src="<?= base_url() ?>assets/themes/immersive/images/logo/160x69-logo.jpg" alt="Logo">-->
                        <?php
                            if($settingRes->logo_image != '')
                            {
                                ?>
                                    <img src="<?= base_url() ?>uploads/loginPage/<?= $settingRes->logo_image; ?>" alt="" style="width:50%;">
                                <?php
                            }
                        ?>
                    </a>
                </div>
                <div class="sign-in-area">
                    <h5 class="text-center"><b>RESET PASSWORD</b></h5>
                    <br>
                    <!--Form-->
                    <form>
                        <div class="input-group mb-20">
                            <input type="email" class="form-control" id="email" placeholder="Enter Your Email">
                        </div>  
                        <!--// Email-->
                        <div class="main-btn-wrap text-center">
                            <input type="submit" class="main-btn uppercase" value="SUBMIT">
                        </div>
                        <br>
                        <div class="text-center">
                            <a href="<?= base_url('login'); ?>"><b>Back</b></a>
                        </div>
                    </form>
                    <!--// Form-->
                </div>
                <!--// Sign In Area-->
            </div>
        </div>
    </div>
</div>
<?php include('include/scripts.php')?>