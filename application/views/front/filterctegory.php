<?php
    if($category_result)
    {
        foreach($category_result as $res)
        {
            $img_name = $this->db->get_where(db_prefix().'files', array('rel_id' => $res->id, 'rel_type' => "category"))->row('attachment_key');
            ?>
                <div class="grid-list-column-item style-02">
                    <h5 class="title"><a href="<?= base_url('customers/carPartService/'.$res->id); ?>"><?= _l($res->name); ?></a></h5>
                    <div class="thumb">
                        <a href="<?= base_url('customers/carPartService/'.$res->id); ?>">    
                            <img src="<?= site_url('download/file/taskattachment/'.$img_name); ?>" alt="<?= _l($img_name); ?>" class="img-fluid">
                        </a>
                    </div>
                </div>
            <?php
        }
    }
    else
    {
       echo '<p class="dataTables_empty">No category found</p>';
    }
?>