<?php include('include/header.php')?>
<?php include('include/navbar.php')?>
<!--Breadcrumb Start-->
<div class="breadcrumb-area" style="background-image: url('assets/themes/immersive/images/about/about-us-banner-bg.png')">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner text-center">
                    <h1 class="page-title">Privacy-Policy</h1>
                    <ul class="page-list">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li>Privacy Policy</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Breadcrumb End-->

<!--About Story Start-->
<section class="padding-60">
    <div class="container">
        <?php
            if($content)
            {
                echo $content->description;
            }
        ?>
    </div>
</section>
<!--// About Story End-->
<?php include('include/footer.php')?>
<?php include('include/scripts.php')?>
        