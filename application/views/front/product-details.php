<?php include('include/header.php')?>
<?php include('include/navbar.php')?><!--Full Width Sider Start-->
<div class="breadcrumb-area" style="background-image: url('<?= site_url() ?>assets/themes/immersive/images/about/about-us-banner-bg.png')">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner text-center">
                    <h1 class="page-title">Product details</h1>
                    <ul class="page-list">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li>Product details</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Breadcrumb End-->
<!-- Shop details Area start -->
<section class="product-details-area padding-top-120 padding-bottom-110">
    <div class="container">
        <?php
            if($productDetails)
            {
                ?>
                    <div class="row">
                        <div class="form-group col-md-7">
                            <label for="location" class="mb-5"><?= $garagename; ?></label>
                        </div>
                        <div class="form-group col-md-5">
                            <label for="location" class="mb-5">Rating: 
                                <?php
                                    $garagerating = $garageRating[0]->rating;
                                    if($garagerating > 0)
                                    {
                                        $str = '';
                                        for($i=0; $i < intval(5); $i++)
                                        {
                                            if($i < intval($garagerating))
                                            {
                                                $str .= '<i class="fa fa-star" aria-hidden="true"></i>';   
                                            }
                                            else
                                            {
                                                $str .= '<i class="fa fa-star-o" aria-hidden="true"></i>';
                                            }
                                        }
                                        echo $str;
                                    }
                                    else
                                    {
                                        echo '<i class="fa fa-star" aria-hidden="true"></i>';
                                        echo '<i class="fa fa-star-o" aria-hidden="true"></i>';
                                        echo '<i class="fa fa-star-o" aria-hidden="true"></i>';
                                        echo '<i class="fa fa-star-o" aria-hidden="true"></i>';
                                        echo '<i class="fa fa-star-o" aria-hidden="true"></i>';
                                    }
                                ?>
                            </label>
                        </div>
                    </div><hr>
                    <div class="row">
                        <div class="col-sm-7 margin-b-40">
                            <div class="ms-showcase2-template ms-showcase2-vertical">
                                <?php
                                    $src = '#';
                                    $attachment_key = $this->db->get_where(db_prefix().'files', array('rel_id' => $productDetails->id, 'rel_type' => "product"))->row('file_name');
                                    if($attachment_key != '')
                                    {
                                        $src = base_url('uploads/product/'.$productDetails->id.'/'.$attachment_key); 
                                    }
                                ?>
                                <div class="master-slider ms-skin-default" id="masterslider">
                                    <div class="ms-slide">
                                        <img src="<?= $src; ?>" data-src="<?= $src; ?>" alt="<?= $attachment_key; ?>"/> 
                                        <img class="ms-thumb" src="<?= $src; ?>" alt="<?= $attachment_key; ?>" />
                                    </div>
                                    <?php
                                        $productImageArr = [];
                                        for($k=1; $k<7; $k++)
                                        {
                                            $imagename = $this->db->get_where(db_prefix().'files', array('rel_id' => $productDetails->id, 'rel_type' => "productimg".$k))->row('file_name');
                                            if($imagename)
                                            {
                                                $img = base_url().'uploads/productimg/'.$productDetails->id.'/'.$imagename;
                                                array_push($productImageArr, $img);
                                            }
                                        }
                                        if($productImageArr)
                                        {
                                            foreach($productImageArr as $src)
                                            {
                                                //echo '<br><img src="'.$r.'" width="80px" height="80px"><br>';
                                                ?>
                                                    <div class="ms-slide">
                                                        <img src="<?= $src; ?>" data-src="<?= $src; ?>" alt="" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';"/> 
                                                        <img class="ms-thumb" src="<?= $src; ?>" alt="" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';"/>
                                                    </div>
                                                <?php
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="item-description">
                                <h3><?= $productDetails->product_name; ?></h3>
                                <p><?php echo '<b>Brand</b> - ';
                                    echo $productDetails->product_brand;
                                ?></p>
                                <br>
                                <p>
                                    <?php
                                        echo $productDetails->description;
                                    ?>
                                </p>
                                <?php
                                    if($productDetails->offer_start_date_without < time() && $productDetails->offer_end_date_without > time())
                                    {
                                        ?>
                                        <p><b>Without Fitting : </b></p>
                                            <p><b>Price : </b><?= CURRENCY_NAME.' <del>'.$productDetails->price_without_fitting.'</del>'; ?></p>
                                        <?php
                                    }
                                    elseif($productDetails->price_without_fitting != '')
                                    {
                                        ?>
                                            <p><b>Without Fitting : </b></p>
                                            <p><b>Price : </b><?= CURRENCY_NAME.' '.$productDetails->price_without_fitting; ?></p>
                                        <?php
                                    }
                                ?>
                                <?php
                                    if($productDetails->offer_start_date_without < time() && $productDetails->offer_end_date_without > time())
                                    {
                                        if($productDetails->price_before_offer_without > 0)
                                        {
                                            ?>
                                                <p><b>Price during offer: </b><?= CURRENCY_NAME.' '.$productDetails->price_before_offer_without; ?></p>
                                            <?php
                                        }
                                        if($productDetails->offer_end_date_without > 0)
                                        {
                                            ?>
                                                <p><b>Offer From : </b><?= date('d-M-Y', $productDetails->offer_start_date_without); ?> <b>To</b> <?= date('d-M-Y', $productDetails->offer_end_date_without); ?></p>
                                            <?php
                                        }
                                    }
                                ?>
                                <hr>
                                <!--<p><b>Price : </b><?= CURRENCY_NAME.' '.$productDetails->price_with_fitting; ?></p>-->
                                <?php
                                    if($productDetails->offer_start_date < time() && $productDetails->offer_end_date > time())
                                    {
                                        ?>
                                            <p><b>With Fitting : </b></p>
                                            <p><b>Price : </b><?= CURRENCY_NAME.' <del>'.$productDetails->price_with_fitting.'</del>'; ?></p>
                                        <?php
                                    }
                                    elseif($productDetails->price_with_fitting != '')
                                    {
                                        ?>
                                            <p><b>With Fitting : </b></p>
                                            <p><b>Price : </b><?= CURRENCY_NAME.' '.$productDetails->price_with_fitting; ?></p>
                                        <?php
                                    }
                                    if($productDetails->offer_start_date < time() && $productDetails->offer_end_date > time())
                                    {
                                        if($productDetails->price_before_offer > 0)
                                        {
                                            ?>
                                                <p><b>Price during offer: </b><?= CURRENCY_NAME.' '.$productDetails->price_before_offer; ?></p>
                                            <?php
                                        }
                                        if($productDetails->offer_end_date > 0)
                                        {
                                            ?>
                                                <p><b>Offer From : </b><?= date('d-M-Y', $productDetails->offer_start_date); ?> <b>To</b> <?= date('d-M-Y', $productDetails->offer_end_date); ?></p>
                                            <?php
                                        }
                                    }
                                ?><hr>
                                <br>
                                <a target="_blank" href="<?= base_url('customers/garageProfile/'.$productDetails->garage_id); ?>" class="btn btn-outline-info btn-xl btn-block"><i class="fa fa-folder-o" aria-hidden="true"></i> Garage Details</a><br>
                                <a target="_blank" href="<?= base_url('customers/message/'.$productDetails->garage_id); ?>" class="btn btn-outline-success btn-xl btn-block"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send Enquiry</a><br>
                                <a target="_blank" href="<?= base_url('customers/buyNow/'.$productDetails->garage_id.'/'.$productDetails->id);?>" class="btn btn-outline-warning btn-xl btn-block"><i class="fa fa-credit-card" aria-hidden="true"></i> Buy Now</a>
                            </div>
                        </div>
                    </div>
                <?php
            }
            else
            {
                echo '<p class="dataTables_empty">No product found!</p>';
            }
        ?>
    </div>
</section>
<!-- Shop details Area End -->
<?php include('include/footer.php')?>
<?php include('include/scripts.php')?>
<script>
    // item carousel (uses the Owl Carousel library)
    $(".item-carousel").owlCarousel({
	    autoplay: true,
        loop:true,
        margin:10,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                dots: true,
                nav:false
                
            },
            600:{
                items:1,
                dots: true,
                nav:false
                
            },
            1000:{
                items:1,
                dots: true,
                nav:false,
                loop:true
                
            }
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        var slider = new MasterSlider();
        slider.control('scrollbar', {dir: 'h'});
        slider.control('thumblist', {autohide: false, dir: 'v', arrows: false, align: 'left', width: 127, height: 84, margin: 5, space: 5, hideUnder: 300});
        slider.setup('masterslider', {
            width: 540,
            height: 586,
            space: 5
        });
    });

    $(".incr-btn").on("click", function (e) {
        var $button = $(this);
        var oldValue = $button.parent().find('.quantity').val();
        $button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
        if ($button.data('action') == "increase") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below 1
            if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 1;
                $button.addClass('inactive');
            }
        }
        $button.parent().find('.quantity').val(newVal);
        e.preventDefault();
    });
</script>
