<?php include('include/header.php')?>
<?php include('include/navbar.php')?>
<style>
    ::placeholder {
      color: white;
      opacity: 1; /* Firefox */
    }
    
    :-ms-input-placeholder { /* Internet Explorer 10-11 */
     color: white;
    }
    
    ::-ms-input-placeholder { /* Microsoft Edge */
     color: white;
    }
</style>
<!--Full Width Sider Start-->
<div class="breadcrumb-area" style="background-image: url('<?= site_url() ?>assets/themes/immersive/images/about/about-us-banner-bg.png')">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner text-center">
                    <h1 class="page-title">My Order</h1>
                    <ul class="page-list">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li>My Order</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Breadcrumb End-->
<!--About Story Start-->
<section class="car-part padding-top-120 padding-bottom-110">
    <div class="container">
        <div class="row">
            <div class="offset-lg-1 col-lg-10 col-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="category-wrap">
                            <div class="table-responsive">
                                <table id="example_" class="table table-bordered display myorder ">
                                    <thead>
                                        <tr>
                                            <th>Products/Services Name</th>
                                            <th>Id</th>
                                            <th title="d-M-Y">Created Date</th>
                                            <th title="d-M-Y">Appointment Date</th>
                                            <th>Price <?= CURRENCY_NAME; ?></th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
											if($orderResult)
											{
												$s = 1;
												foreach($orderResult as $rrr)
												{
												   // print_r($rrr);
												    if($rrr->quote_id > 0)
												    {
												        $s++;
														$quoteDetails = $this->db->select('id,title,created_date')->get_where(db_prefix().'requestAQuote', array('id' => $rrr->quote_id))->row();
														$price_ = $this->db->get_where(db_prefix().'quoteRequest_garageReply', array('quote_id' => $rrr->quote_id, 'garage_id' => $rrr->garage_id))->row('price');
														?>
															<tr>
																<td><a href="javascript:void(0);" data-toggle="modal" onclick="getPreviewReq(<?= $quoteDetails->id; ?>)" data-target="#detailsModal"><?= $quoteDetails->title; ?></a></td>
																<td><?= $rrr->id; ?></td>
																<td><span style="display:none;"><?php echo strtotime($rrr->created_date); ?></span><?= date('d-M-Y', strtotime($rrr->created_date)); ?></td>
																<td><span style="display:none;"><?php echo $rrr->appointment_date; ?></span><?= date('d-M-Y', $rrr->appointment_date); ?></td>
																<td><?= $price_; ?></td>
																<!--<td <?= ($rrr->status == 4)?'style="background:red;"':'';?>>-->
																<td>
																    <?php
																        if(time() < $rrr->appointment_date)
																        {
																            if($rrr->status == 1)
																              echo '<span class="badge badge-pending">On Schedule</span>';
																           else if($rrr->status == 4)
																              echo '<span class="badge badge-pending">On Schedule</span>';
																            else if($rrr->status == 3)
																                echo '<span class="badge badge-active">Completed</span>';
																            else if($rrr->status == 5)
																                echo '<span class="badge badge-cancelled">Cancelled</span>';
																            else
    																            echo '<span class="badge badge-cancelled">Expired</span>';
																        }
																        else
																        {
																            if($rrr->status == 1)
																              echo '<span class="badge badge-pending">On Schedule</span>';
																            else if($rrr->status == 3)
																                echo '<span class="badge badge-active">Completed</span>';
																            else if($rrr->status == 5)
																                echo '<span class="badge badge-cancelled">Cancelled</span>';
																            else if($rrr->status == 4)
																                echo '<span class="badge badge-pending">On Schedule</span>';
																            else
																                echo '<span class="badge badge-cancelled">Expired</span>';
																        }
																    ?>
																</td>
																<td>
																	<p class="btn-group" role="group" aria-label="Basic example">
																	    <!--
																		<button type="button" class="btn btn-confirm mr-3"><i class="fa fa-calendar" aria-hidden="true"></i> Re-schedule</button>
																		<button type="button" class="btn btn-appologise mr-10"><i class="fa fa-trash" aria-hidden="true"></i> Cancel</button>
																		<button type="button" class="btn btn-re-activate mr-3"><i class="fa fa-repeat" aria-hidden="true"></i> Re-order</button>
																		-->
																		<?php
																		    if($rrr->status == 3)
																		    {
																		        ?>
																		            <button type="button" class="btn btn-rate mr-10" data-toggle="modal" data-toggle="modal" onclick="setReviewRatingProduct(<?= $rrr->quote_id ?>,<?= $rrr->customer_id ?>,<?= $rrr->garage_id ?>)" data-target="#rateModal"><i class="fa fa-star" aria-hidden="true"></i> Rate</button>
																		        <?php
																		    }
																		?>
																	</p>
																</td>
															</tr>
														<?php
												    }
												    else
												    {
												        $s++;
														$productDetails = $this->db->select('id,price_with_fitting,product_name,offer_start_date,offer_end_date,price_before_offer,offer_start_date_without,offer_end_date_without,price_before_offer_without,price_without_fitting')->get_where(db_prefix().'_product', array('id' => $rrr->product_id))->row();
														?>
															<tr>
																<td><a href="javascript:void(0);" data-toggle="modal" onclick="getPreviewProductReq(<?= $rrr->product_id; ?>)" data-target="#detailsModal"><?= $productDetails->product_name; ?></a></td>
																<td><?= $rrr->id; ?></td>
																<td><span style="display:none;"><?php echo strtotime($rrr->created_date); ?></span><?= date('d-M-Y', strtotime($rrr->created_date)); ?></td>
														    	<td><span style="display:none;"><?php echo $rrr->appointment_date; ?></span><?= date('d-M-Y', $rrr->appointment_date); ?></td>
																<td>
																    <?php
																        if($rrr->fitting_type != '')
																        {
																            if($rrr->fitting_type == 'With_Fitting')
																            {
																                if($productDetails->offer_start_date < time() && $productDetails->offer_end_date  > time())
    																            {
    																                echo $productDetails->price_before_offer;
    																            }    
    																            else
    																            {
    																                echo $productDetails->price_with_fitting;
    																            }
																            }
																            else
																            {
																                if($productDetails->offer_start_date_without < time() && $productDetails->offer_end_date_without  > time())
    																            {
    																                echo $productDetails->price_before_offer_without;
    																            }    
    																            else
    																            {
    																                echo $productDetails->price_without_fitting;
    																            }
																            }
																        }
																        else
																        {
																            echo $productDetails->price_with_fitting;
																        }
																    ?>
																</td>
																<!--<td <?= ($rrr->status == 4)?'style="background:red;"':'';?>>-->
																<td>
																    <?php
																        if(time() < $rrr->appointment_date)
																        {
																            if($rrr->status == 1)
																                echo '<span class="badge badge-pending">Pending confirmation from garage</span>';
																            elseif($rrr->status == 6)
																                echo '<span class="badge badge-pending">Pending confirmation from garage</span>';
																            else if($rrr->status == 3)
																                echo '<span class="badge badge-active">Completed</span>';
																            else if($rrr->status == 4)
																              echo '<span class="badge badge-pending">On Schedule</span>';
																            else if($rrr->status == 5)
																                echo '<span class="badge badge-cancelled">Cancelled</span>';
																            else
    																            echo '<span class="badge badge-cancelled">Cancelled</span>';
																        }
																        else
																        {
																            if($rrr->status == 1)
																                echo '<span class="badge badge-pending">On Schedule</span>';
																			elseif($rrr->status == 6)
																                echo '<span class="badge badge-pending">Pending confirmation from garage</span>';
																            else if($rrr->status == 3)
																                echo '<span class="badge badge-active">Completed</span>';
																            else if($rrr->status == 4)
																              echo '<span class="badge badge-pending">On Schedule</span>';
																            else if($rrr->status == 5)
																                echo '<span class="badge badge-cancelled">Cancelled</span>';
																            else
																                echo '<span class="badge badge-cancelled">Expired</span>';
																        }
																    ?>
															    </td>
																<td>
																	<p class="btn-group" role="group" aria-label="Basic example">
																	    <!--
																		<button type="button" class="btn btn-confirm mr-3"><i class="fa fa-calendar" aria-hidden="true"></i> Re-schedule</button>
																		<button type="button" class="btn btn-appologise mr-10"><i class="fa fa-trash" aria-hidden="true"></i> Cancel</button>
																		<button type="button" class="btn btn-re-activate mr-3"><i class="fa fa-repeat" aria-hidden="true"></i> Re-order</button>
																		-->
																		<?php
																		    if($rrr->status == 3)
																		    {
																		        ?>
																		            <button type="button" class="btn btn-rate mr-10" data-toggle="modal" onclick="setReviewRatingProduct(<?= $rrr->product_id ?>,<?= $rrr->customer_id ?>,<?= $rrr->garage_id ?>)" data-target="#rateModal"><i class="fa fa-star" aria-hidden="true"></i> Rate</button>
																		        <?php
																		    }
																		?>
																	</p>
																</td>
															</tr>
														<?php
												    }
												}
												if($s == 1)
												{
													?>
														<tr>
															<td valign="top" colspan="7" class="dataTables_empty">No matching records found</td>
														</tr>
													<?php
												}
											}
											else
											{
												?>
													<tr>
														<td valign="top" colspan="7" class="dataTables_empty">No matching records found</td>
													</tr>
												<?php
											}
										?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// Container End-->
</section>
<!--// About Story End-->
<!-- The detail Modal -->
<div class="modal" id="detailsModal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Order Details</h5>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="previewrequest">                  
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include('include/footer.php')?>
<?php include('include/scripts.php')?>

<!-- Rate Modal -->
<div class="modal" id="rateModal">
    <div class="modal-dialog">
        <div class="modal-content">
             <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Rating Box</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <?php echo form_open($this->uri->uri_string(),array('class'=>'login-form', 'onsubmit'=>'return ratingByCustomer()')); ?>
                <!-- Modal body -->
                <div class="modal-body">
                    <input type="hide" style="display:none;" name="ordertype" id="ordertype">
                    <input type="hide" style="display:none;" name="quote_id" id="quote_id">
                    <input type="hide" style="display:none;" name="customer_id" id="customer_id">
                    <input type="hide" style="display:none;" name="garage_id" id="garage_id">
                    <div class="row">
                        <div class="col-12">
                            <div class="order-rate-wrap">
                                <section class="star-rating">
                                    <h6>Rate the order</h6>
                                    <input id="star-1" type="radio" name="order_rating" value="1" />
                                    <label for="star-1" title="1 stars">
                                        <i class="active fa fa-star" aria-hidden="true"></i>
                                    </label>
                                    <input id="star-2" type="radio" name="order_rating" value="2" />
                                    <label for="star-2" title="2 stars">
                                        <i class="active fa fa-star" aria-hidden="true"></i>
                                    </label>
                                    <input id="star-3" type="radio" name="order_rating" value="3" />
                                    <label for="star-3" title="3 stars">
                                        <i class="active fa fa-star" aria-hidden="true"></i>
                                    </label>
                                    <input id="star-4" type="radio" name="order_rating" value="4" />
                                    <label for="star-4" title="4 stars">
                                        <i class="active fa fa-star" aria-hidden="true"></i>
                                    </label>
                                    <input id="star-5" type="radio" name="order_rating" value="5" />
                                    <label for="star-5" title="5 star">
                                        <i class="active fa fa-star" aria-hidden="true"></i>
                                    </label>
                                    <input type="radio" name="order_rating" value="0" id="star-null" />
                                    <label for="star-null" class="star-null">
                                        Clear
                                    </label>
                                </section>
                                <!--<input type="radio" name="order_rating" value="0" id="star-null" />
                                <input type="radio" name="order_rating" value="1" id="star-1" />
                                <input type="radio" name="order_rating" value="2" id="star-2" />
                                <input type="radio" name="order_rating" value="3" id="star-3" />
                                <input type="radio" name="order_rating" value="4" id="star-4" checked />
                                <input type="radio" name="order_rating" value="5" id="star-5" />
    
                                <section>
                                    <h6>Rate the order</h6>
                                    <label for="star-1">
                                        <svg width="255" height="240" viewBox="0 0 51 48">
                                        <path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z" />
                                        </svg>
                                    </label>
                                    <label for="star-2">
                                        <svg width="255" height="240" viewBox="0 0 51 48">
                                        <path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z" />
                                        </svg>
                                    </label>
                                    <label for="star-3">
                                        <svg width="255" height="240" viewBox="0 0 51 48">
                                        <path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z" />
                                        </svg>
                                    </label>
                                    <label for="star-4">
                                        <svg width="255" height="240" viewBox="0 0 51 48">
                                        <path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z" />
                                        </svg>
                                    </label>
                                    <label for="star-5">
                                        <svg width="255" height="240" viewBox="0 0 51 48">
                                        <path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z" />
                                        </svg>
                                    </label>
                                    <label for="star-null">
                                        Clear
                                    </label>
                                </section>-->
                            </div>
                            <div class="garage-rate-wrap">
                                <!--<input type="radio" name="garage_rating" value="0" id="star-nul" />
                                <input type="radio" name="garage_rating" value="1" id="star-6" />
                                <input type="radio" name="garage_rating" value="2" id="star-7" />
                                <input type="radio" name="garage_rating" value="3" id="star-8" />
                                <input type="radio" name="garage_rating" value="4" id="star-9" checked />
                                <input type="radio" name="garage_rating" value="5" id="star-10" />
    
                                <section>
                                    <h6>Rate the garage</h6>
                                    <label for="star-6">
                                        <svg width="255" height="240" viewBox="0 0 51 48">
                                        <path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z" />
                                        </svg>
                                    </label>
                                    <label for="star-7">
                                        <svg width="255" height="240" viewBox="0 0 51 48">
                                        <path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z" />
                                        </svg>
                                    </label>
                                    <label for="star-8">
                                        <svg width="255" height="240" viewBox="0 0 51 48">
                                        <path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z" />
                                        </svg>
                                    </label>
                                    <label for="star-9">
                                        <svg width="255" height="240" viewBox="0 0 51 48">
                                        <path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z" />
                                        </svg>
                                    </label>
                                    <label for="star-10">
                                        <svg width="255" height="240" viewBox="0 0 51 48">
                                        <path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z" />
                                        </svg>
                                    </label>
                                    <label for="star-nul">
                                        Clear
                                    </label>
                                </section>-->
                                <section class="star-rating">
                                    <h6>Rate the garage</h6>
                                    <input id="star-6" type="radio" name="garage_rating" value="6" />
                                    <label for="star-6" title="6 stars">
                                        <i class="active fa fa-star" aria-hidden="true"></i>
                                    </label>
                                    <input id="star-7" type="radio" name="garage_rating" value="7" />
                                    <label for="star-7" title="7 stars">
                                        <i class="active fa fa-star" aria-hidden="true"></i>
                                    </label>
                                    <input id="star-8" type="radio" name="garage_rating" value="8" />
                                    <label for="star-8" title="8 stars">
                                        <i class="active fa fa-star" aria-hidden="true"></i>
                                    </label>
                                    <input id="star-9" type="radio" name="garage_rating" value="9" />
                                    <label for="star-9" title="9 stars">
                                        <i class="active fa fa-star" aria-hidden="true"></i>
                                    </label>
                                    <input id="star-10" type="radio" name="garage_rating" value="10" />
                                    <label for="star-10" title="10 star">
                                        <i class="active fa fa-star" aria-hidden="true"></i>
                                    </label>
                                    <input type="radio" name="garage_rating" value="0" id="star-nul" />
                                    <label for="star-nul" class="star-null">
                                        Clear
                                    </label>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
    
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" >Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<script>
    /*
    *  @Function: ratingByCustomer
    **/
    var customer_id = '';
    var garage_id = '';
    var quote_id = '';
    var ordertype = '';
    function setReviewRatingProduct(pid, cid, gid)
    {
        ordertype = 'product';
        quote_id = pid;
        customer_id = cid;
        garage_id = gid;
        $('#ordertype').val(ordertype);
        $('#quote_id').val(quote_id);
        $('#customer_id').val(customer_id);
        $('#garage_id').val(garage_id);
    }
    function setReviewRatingQuote(pid, cid, gid)
    {
        ordertype = 'quote';
        quote_id = pid;
        customer_id = cid;
        garage_id = gid;
        $('#ordertype').val(ordertype);
        $('#quote_id').val(quote_id);
        $('#customer_id').val(customer_id);
        $('#garage_id').val(garage_id);
    }
    function ratingByCustomer()
    {
        var order_rating = $("input[name='order_rating']:checked"). val();
        var garage_rating = $("input[name='garage_rating']:checked"). val();
        
        if(order_rating > 0 && garage_rating > 0)
        {
            return true;
        }
        else
        {
            if(order_rating == 0)    
                errormsg('Order rating is required!');
                return false;
            if(garage_rating == 0)    
                errormsg('Garage rating is required!');
                return false;
        }
    }

	/* Get Draft Request */
    function getPreviewReq(id)
    {
        $('.previewrequest').html('');
        if(id)
        {
            var str = "id="+id+"&"+csrfData['token_name']+"="+csrfData['hash'];
    	    $.ajax({
    	        url: '<?= site_url()?>customers/getQuoteDetails',
    	        type: 'POST',
    	        data: str,
    	        cache: false,
    	        success: function(resp){
    	            if(resp)
    	            {
    	                $('.previewrequest').html(resp);
    	            }
    	            else
    	            {
    	                $('.previewrequest').html('');
    	            }
    	        }
    	    });
        }
        else
        {
            $('.previewrequest').html('');
        }
    }
    
	/* Get Draft Request */
    function getPreviewProductReq(id)
    {
        $('.previewrequest').html('');
        if(id)
        {
            var str = "id="+id+"&"+csrfData['token_name']+"="+csrfData['hash'];
    	    $.ajax({
    	        url: '<?= site_url()?>customers/getPreviewProductReq',
    	        type: 'POST',
    	        data: str,
    	        cache: false,
    	        success: function(resp){
    	            if(resp)
    	            {
    	                $('.previewrequest').html(resp);
    	            }
    	            else
    	            {
    	                $('.previewrequest').html('');
    	            }
    	        }
    	    });
        }
        else
        {
            $('.previewrequest').html('');
        }
    }
    
    /*
    $(document).ready(function() {
        $('#example_').DataTable( {
            "order": [[ 1, "desc" ]]
        } );
    } );
    */
    $(document).ready(function() {
        $('#example_ thead tr').clone(true).appendTo( '#example_ thead' );
        $('#example_ thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            if(title == 'Action')
                $(this).html( '<input type="text" disabled />' );
            else
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
     
            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
     
        var table = $('#example_').DataTable( {
            order: [[ 1, "desc" ]],
            orderCellsTop: true,
            fixedHeader: true
        } );
    } );
</script>