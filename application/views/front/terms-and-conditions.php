<?php include('include/header.php')?>
<?php include('include/navbar.php')?>
<!--Breadcrumb Start-->
<div class="breadcrumb-area" style="background-image: url('assets/themes/immersive/images/about/about-us-banner-bg.png')">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner text-center">
                    <h1 class="page-title">Terms & Conditions</h1>
                    <ul class="page-list">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li>Terms & Conditions</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Breadcrumb End-->

<!--Contact-->
<!--<div class="terms-and-conditions-page padding-top-115 padding-bottom-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h4 class="theme-color">1. Description of Service</h4>
                <p class="mb-20">Distinctio iste praesentium totam quasi tempore, magnam ipsum cum animi at fuga alias harum quo quibusdam odit eum reprehenderit consectetur suscipit! lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente, </p>
                <h4 class="theme-color mt-30">2. Your Registration Obligations</h4>
                <p class="mb-20">Consectetur adipisicing elit. Distinctio nesciunt officia culpa nostrum maxime vero architecto, corporis placeat repudiandae minima facere animi, pariatur fugit dignissimos qui error est nulla. Doloribus. lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio nesciunt officia culpa nostrum maxime vero architecto, corporis placeat repudiandae minima facere animi, pariatur fugit dignissimos qui error est nulla. Doloribus.Lorem ipsum dolor sit amet, </p>
                <h4 class="theme-color mt-30"> 3. User Account, Password, and Security</h4>
                <p class="mb-20">Sapiente, distinctio iste praesentium totam quasi tempore, magnam ipsum cum animi. lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
                <h4 class="theme-color mt-30">4. User Conduct</h4>
                <p class="mb-20">Vitae facere expedita! Voluptatem iure dolorem dignissimos nisi magni a dolore, et inventore optio, voluptas, obcaecati. lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente, distinctio iste praesentium totam quasi tempore, magnam ipsum cum animi. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium vel recusandae ad impedit ipsum, </p>
                <ul class="list list-unstyled theme-color mb-20">
                    <li> <i class="fa fa-angle-right"></i> Consectetur lorem ipsum dolor sit amet </li>
                    <li> <i class="fa fa-angle-right"></i> Quidem error quae illo excepturi nostrum blanditiis laboriosam </li>
                    <li> <i class="fa fa-angle-right"></i> Molestias, eum nihil expedita dolorum odio dolorem</li>
                    <li> <i class="fa fa-angle-right"></i> Eum nihil expedita dolorum odio dolorem</li>
                    <li> <i class="fa fa-angle-right"></i> Explicabo rem illum magni perferendis</li>
                    </ul>
                <h4 class="theme-color mt-30">5. International Use</h4>
                <p class="mb-30">Sapiente, distinctio iste praesentium totam quasi tempore, lorem ipsum dolor sit amet, consectetur adipisicing elit. magnam ipsum cum animi. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium vel recusandae ad impedit ipsum, vitae facere expedita! Voluptatem iure dolorem dignissimos nisi magni a dolore, et inventore optio, voluptas, obcaecati. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate incidunt aliquam sint, magnam excepturi quas a, id doloremque quasi iusto quo consequuntur dolorum neque optio ipsum, rerum nesciunt illo iure. </p>
            </div>
        </div>
    </div>
</div>-->
<!--// Contact-->
<div class="container">
<?php
    if($content)
    {
        echo $content->description;
    }
?>
</div>
<?php include('include/footer.php')?>
<?php include('include/scripts.php')?>