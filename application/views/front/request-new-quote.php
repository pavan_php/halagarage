<?php include('include/header.php')?>
<?php include('include/navbar.php')?>
<!--Breadcrumb Start-->
<div class="breadcrumb-area" style="background-image: url('<?= site_url() ?>assets/themes/immersive/images/about/about-us-banner-bg.png')">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner text-center">
                    <h1 class="page-title">Request New Quote</h1>
                    <ul class="page-list">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li><a href="<?= base_url('customers/getAQuote'); ?>">Get a Quote</a></li>
                        <li>Request New Quote</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Breadcrumb End-->
    
<div class="new-quote-page padding-top-115 padding-bottom-100">
    <div class="container">
        <div class="row">
            <div class="offset-lg-2 col-lg-8 col-12">
                <div class="request-wrapper">
                    <h3><b>Request New Quote</b></h3>
                    <hr>
                    <form class="request-form">
                        <div class="row">
                            <div class="col-12 col-xl-6 col-lg-6">
                                <div class="form-group">
                                    <label class="label">Title</label>
                                    <input type="text" readonly class="form-control" value="Title">
                                </div>
                            </div>
                            <div class="col-12 col-xl-6 col-lg-6">
                                <div class="form-group">
                                    <label class="label">Choose Your Location</label>
                                    <select class="form-control chosen-select">
                                        <option>Select location<option>
                                        <option>location 1<option>
                                        <option>location 2<option>
                                        <option>location 3<option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-xl-6 col-lg-6">
                                <div class="form-group">
                                    <label class="label">Choose Category</label>
                                    <select class="form-control chosen-select" name="category_id" onchange="getSubCategory(this.value);">
                                        <option value="">Select Categories<option>
                                            <?php
                                                if($category_result)
                                                {
                                                    foreach($category_result as $res)
                                                    {
                                                        ?>
                                                            <option value="<?= $res->id; ?>"><?= $res->name; ?></option>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-xl-6 col-lg-6">
                                <div class="form-group">
                                    <label class="label">Choose Service Sub Category</label>
                                    <select class="form-control chosen-select" name="sub_category_id" id="sub_category_id">
                                        <option value="">Select sub category<option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-xl-6 col-lg-6">
                                <div class="form-group">
                                    <label class="label">Choose Vehicle Category</label>
                                    <select class="form-control chosen-select">
                                        <option>Select vehicle<option>
                                        <option>vehicle 1<option>
                                        <option>vehicle 2<option>
                                        <option>vehicle 3<option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-xl-6 col-lg-6">
                                <div class="form-group">
                                    <label class="label">Choose Brand</label>
                                    <select class="form-control chosen-select">
                                        <option>Select brand<option>
                                        <option>Brand 1<option>
                                        <option>Brand 2<option>
                                        <option>Brand 3<option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-xl-6 col-lg-6">
                                <div class="form-group">
                                    <label class="label">Choose Car Model</label>
                                    <select class="form-control chosen-select">
                                        <option>Select car model<option>
                                        <option>car model 1<option>
                                        <option>car model 2<option>
                                        <option>car model 3<option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-xl-6 col-lg-6">
                                <div class="form-group">
                                    <label class="label">Choose Car Make Year</label>
                                    <select class="form-control chosen-select">
                                        <option>Select Year<option>
                                        <option>2014<option>
                                        <option>2015<option>
                                        <option>2018<option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-xl-12 col-lg-12">
                                <div class="form-group">
                                    <label class="label">Upload Vehicle Registration Card</label>
                                    <div class="input-group upload-wrap rounded-pill bg-white mb-20">
                                        <input type="file" id="file-upload">
                                        <label for="file-upload" class="file-upload mb-0">Browse</label>
                                        <div id="file-upload-filename"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-xl-12 col-lg-12">
                                <div class="form-group">
                                    <label class="label">Detailed Description</label>
                                    <textarea class="form-control" rows="4" placeholder="Detailed description here"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-xl-12 col-lg-12">
                                <div class="form-group">
                                    <label class="label">Upload photo / audio / video</label>
                                    <div class="input-group upload-wrap rounded-pill bg-white mb-20">
                                        <input type="file" id="file-upload">
                                        <label for="file-upload" class="file-upload mb-0">Browse</label>
                                        <div id="file-upload-filename"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-xl-12 col-lg-12">
                                <a href="javascript:void(0);" type="submit" class="btn main-btn uppercase mt-2">Get Quote</a>
                                <a href="javascript:void(0);" type="button" class="btn main-btn uppercase mt-2">Save and Continue Later</a>
                                <a href="<?= base_url(); ?>" type="button" class="btn main-btn uppercase mt-2">Cancel and Go To Home Page</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('include/footer.php')?>
<?php include('include/scripts.php')?>
        