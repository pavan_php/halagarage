<?php include('include/header.php')?>
<?php include('include/navbar.php')?>
<!--Breadcrumb Start-->
<div class="breadcrumb-area" style="background-image: url('<?= base_url() ?>assets/themes/immersive/images/about/about-us-banner-bg.png')">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner text-center">
                    <h1 class="page-title">Start a Quote</h1>
                    <ul class="page-list">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li>Start a Quote</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Breadcrumb End-->
<!--About Story Start-->
<section class="car-part padding-top-120 padding-bottom-110">
    <div class="container">
        <div class="row">
            <div class="offset-lg-2 col-lg-8 col-12">
                <div class="category-wrap">
                    <!--<div class="panel-body">
                        <!-<h4 class="no-margin section-text"></h4>--
                        <div class="header-wrap p-10 bg-white">
                            <div class="d-flex align-items-center">
                                <div>
                                    <h4 class="fw-700 mt-0">Start Quote</h4>
                                </div>
                                <div class="ml-auto align-items-center">
                                    <div class="dl">
                                        <form class="form-inline">
                                            <div class="form-group">
                                                <div class="dropdown bootstrap-select form-control bs3">
                                                <select class="form-control chosen-select" tabindex="-98">
                                                    <option>Select filter</option>
                                                    <option>Location</option>
                                                    <option>Category </option>
                                                    <option>Sub Category</option>
                                                    <option>Vehicle Brand</option>
                                                    <option>Vehicle Model</option>
                                                    <option>Vehicle Year</option>
                                                    <option>Vehicle category assigned</option>
                                                </select>
                                                <button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="combobox" aria-owns="bs-select-1" aria-haspopup="listbox" aria-expanded="false" title="Select filter"><div class="filter-option"><div class="filter-option-inner"><div class="filter-option-inner-inner">Select filter</div></div> </div><span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" style="max-height: 497.5px; overflow: hidden; min-height: 122px;"><div class="inner open" role="listbox" id="bs-select-1" tabindex="-1" aria-activedescendant="bs-select-1-0" style="max-height: 485.5px; overflow-y: auto; min-height: 110px;"><ul class="dropdown-menu inner " role="presentation" style="margin-top: 0px; margin-bottom: 0px;"><li class="selected active"><a role="option" id="bs-select-1-0" tabindex="0" aria-setsize="8" aria-posinset="1" class="active selected" aria-selected="true"><span class="text">Select filter</span></a></li><li><a role="option" id="bs-select-1-1" tabindex="0"><span class="text">Location</span></a></li><li><a role="option" id="bs-select-1-2" tabindex="0"><span class="text">Category </span></a></li><li><a role="option" id="bs-select-1-3" tabindex="0"><span class="text">Sub Category</span></a></li><li><a role="option" id="bs-select-1-4" tabindex="0"><span class="text">Vehicle Brand</span></a></li><li><a role="option" id="bs-select-1-5" tabindex="0"><span class="text">Vehicle Model</span></a></li><li><a role="option" id="bs-select-1-6" tabindex="0"><span class="text">Vehicle Year</span></a></li><li><a role="option" id="bs-select-1-7" tabindex="0"><span class="text">Vehicle category assigned</span></a></li></ul></div></div></div>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-theme">Reset</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>-->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th style="width:15%;">Request Date</th>
                                        <th>Location</th>
                                        <th style="width:15%;">Category</th>
                                        <th style="width:15%;">Sub Category</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>#001</td>
                                        <td>13 May 2020</td>
                                        <td>Ali Bin Abi Taleb Street , Bur Dubai Creekside, behind Dubai Museum, Bur Dubai, Dubai, United Arab</td>
                                        <td>Turbo Systems</td>
                                        <td>AC Filter</td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <button type="button" class="btn btn-confirm mr-3" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-reply mr-3" data-toggle="modal" data-target="#quoteModal"><i class="fa fa-reply" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-appologise mr-3" data-toggle="tooltip" title="Send inquiry"><i class="fa fa-envelope" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-appologise mr-10" data-toggle="modal" data-target="#rejectModal"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>#002</td>
                                        <td>10 June 2020</td>
                                        <td>Baniyas Road, Dubai, United Arab Emirates</td>
                                        <td>Body Kits</td>
                                        <td>Wheel Allignment</td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <button type="button" class="btn btn-confirm mr-3" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-reply mr-3" data-toggle="modal" data-target="#quoteModal"><i class="fa fa-reply" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-appologise mr-3" data-toggle="tooltip" title="Send inquiry"><i class="fa fa-envelope" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-appologise mr-10" data-toggle="modal" data-target="#rejectModal"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>#003</td>
                                        <td>03 June 2020</td>
                                        <td>Ali Bin Abi Taleb Street , Bur Dubai Creekside, United Arab</td>
                                        <td>Painting</td>
                                        <td>Sparay Paint</td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <button type="button" class="btn btn-confirm mr-3" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-reply mr-3" data-toggle="modal" data-target="#quoteModal" title="Quote now"><i class="fa fa-reply" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-appologise mr-3" data-toggle="tooltip" title="Send inquiry"><i class="fa fa-envelope" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-appologise mr-10" data-toggle="modal" data-target="#rejectModal"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>#004</td>
                                        <td>10 Apr 2020</td>
                                        <td>Ali Bin Abi Taleb Street , Bur Dubai Creekside, behind Dubai Museum, Bur Dubai, Dubai, United Arab</td>
                                        <td>Battery</td>
                                        <td>Battery Charging</td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <button type="button" class="btn btn-confirm mr-3" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-reply mr-3" data-toggle="modal" data-target="#quoteModal" title="Quote now"><i class="fa fa-reply" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-appologise mr-3" data-toggle="tooltip" title="Send inquiry"><i class="fa fa-envelope" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-appologise mr-10" data-toggle="modal" data-target="#rejectModal"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
        <!--// Row End-->
    </div>
    <!--// Container End-->
</section>
<!--// About Story End-->
<?php include('include/footer.php')?>
<?php include('include/scripts.php')?>