<?php include('include/header.php')?>
<style>
    .mb-0{margin-bottom:0px!important;}
	.mb1-0{margin-bottom:10px!important;}
	.login-register {
		padding: 5px 0;
		z-index: 999;
		position: relative;
		min-height: 100vh;
		text-align: center;
		display: -webkit-box;
		display: -moz-box;
		display: -ms-flexbox;
		display: -webkit-flex;
		display: flex;
		justify-content: center;
		align-items: center;
	}

	.login-register .form-section .form-box {
		/*float: left;*/
		width: 100%;
		text-align: left;
		position: relative;
	}

	.login-register h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
		font-family: 'Jost', sans-serif;
	}

	.login-register .bg-img {
		background: url(http://php.manageprojects.in/halagarage/uploads/login-bg.jpg);
		background-size: cover;
		top: 0;
		bottom: 0;
		opacity: 1;
		width: 100%;
		z-index: 999;
		padding-left: 50px;
		position: relative;
		display: flex;
		justify-content: center;
		align-items: center;
		padding: 30px 50px;
	}

	.login-register .bg-img::before {
		background: #3385d9;
	}
	.login-register .bg-img::before {
		opacity: 0.8;
		content: "";
		display: block;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		height: 100%;
		width: 100%;
		position: absolute;
	}

	.login-register .logo {
		border-radius: 50px;
		margin-bottom: 0;
		display: inline-block;
		top: 40px;
		position: absolute;
		left: 80px;
	}

	.login-register .logo img {
		height: 80px;
	}

	.login-register .login-box {
		background: #fff;
		margin: 0 auto;
		box-shadow: 0 0 35px rgba(0, 0, 0, 0.1);
	}

	.login-register .login-box .form-info {
		background: #fff;
	}

	.login-register .form-section {
		padding: 180px 80px 100px;
		border-radius: 10px 0 0 10px;
		text-align: left;
	}

	.login-register-bg {
		background: #f7f7f7;
	}

	.login-register .pad-0 {
		padding: 0;
	}

	.login-register label {
		color: #333;
		font-size: 16px;
		font-weight: 500;
		margin-bottom: 10px;
	}

	.login-register .form-section p {
		color: #717171;
		font-size: 13px;
		font-weight: 500;
	}
	
	.login-register .text-danger{
		bottom: -90px;
	}

	.login-register .form-section p a {
		color: #717171;
		font-weight: 500;
	}

	.login-register .form-section p a:hover {
	}

	.login-register .form-section ul {
		list-style: none;
		padding: 0;
		margin: 0 0 20px;
	}

	.login-register .form-section .social-list li {
		display: inline-block;
		margin-bottom: 5px;
	}

	.login-register .form-section .thembo {
		margin-left: 4px;
	}

	.login-register .form-section h1 {
		font-size: 27px;
		font-weight: 600;
		color: #3385d9;
		text-align: left;
	}

	.login-register .form-section h3 {
		margin: 0 0 50px;
		font-size: 23px;
		font-weight: 400;
		color: #313131;
	}

	.login-register .form-section .form-group {
		margin-bottom: 25px;
	}

	.login-register.form-section .form-box {
		float: left;
		width: 100%;
		text-align: left;
		position: relative;
	}

	.login-register .form-section .form-box input {
		float: left;
		width: 100%;
	}

	.login-register .form-section .input-text {
		padding: 10px 20px;
		font-size: 16px;
		outline: none;
		height: 50px;
		color: #717171;
		border-radius: 3px;
		font-weight: 500;
		border: 1px solid transparent;
		background: #fff;
		box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
	}

	.login-register .form-section .checkbox .terms {
		margin-left: 3px;
	}

	.login-register .form-section .btn-md {
		cursor: pointer;
		padding: 13px 50px 12px 50px;
		font-size: 17px;
		font-weight: 400;
		font-family: 'Jost', sans-serif;
		border-radius: 3px;
	}

	.login-register .form-section input[type=checkbox], input[type=radio] {
		margin-right: 3px;
	}

	.login-register .form-section button:focus {
		outline: none;
		outline: 0 auto -webkit-focus-ring-color;
	}

	.login-register .form-section .btn-theme.focus, .btn-theme:focus {
		box-shadow: none;
	}

	.login-register .form-section .btn-theme {
		background: #3385d9;
		border: none;
		color: #fff;
		box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
		display:block;
	}

	.login-register .form-section .btn-theme:hover {
		box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
		background: #24963e;
	}

	.login-register .none-2 {
		display: none;
	}

	.login-register .form-section .terms {
		margin-left: 3px;
	}

	.login-register .btn-section {
		border-radius: 50px;
		margin-bottom: 0;
		display: inline-block;
		top: 80px;
		position: absolute;
		right: 80px;
	}

	.login-register .info {
		max-width: 500px;
		margin: 0 auto;
		align-self: center !important;
	}

	.login-register .btn-section .link-btn {
		font-size: 14px;
		float: left;
		background: transparent;
		font-weight: 400;
		text-align: center;
		text-decoration: none;
		text-decoration: blink;
		width: 100px;
		padding: 6px 5px;
		margin-right: 5px;
		color: #000;
		border-radius: 3px;
		background: #fff;
		box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
	}

	.login-register .btn-section .active-bg {
		color: #fff;
		background: #3385d9;
	}

	.login-register .btn-section .link-btn:hover {
		color: #fff;
		background: #3385d9;
	}

	.login-register .form-section .checkbox {
		font-size: 14px;
	}

	.login-register .form-section .form-check {
		float: left;
		margin-bottom: 0;
	}

	.login-register .form-section .form-check a {
		color: #717171;
		float: right;
	}

	.login-register .form-section .form-check-input {
		position: absolute;
		margin-left: 0;
	}

	.login-register .form-section .form-check label::before {
		content: "";
		display: inline-block;
		position: absolute;
		width: 18px;
		height: 18px;
		top: 2px;
		margin-left: -25px;
		border: 1px solid #c5c3c3;
		border-radius: 3px;
		background-color: #fff;
	}

	.login-register .form-section .form-check-label {
		padding-left: 25px;
		margin-bottom: 0;
		font-size: 16px;
		font-weight: 500;
		color: #717171;
	}

	.login-register .form-section .checkbox-theme input[type="checkbox"]:checked + label::before {
		background-color: #fea900;
		border-color: #fea900;
	}

	.login-register .form-section input[type=checkbox]:checked + label:before {
		font-weight: 300;
		color: #e6e6e6;
		line-height: 15px;
		font-size: 14px;
		content: "\2713";
	}

	.login-register .form-section input[type=checkbox], input[type=radio] {
		margin-top: 4px;
	}

	.login-register .form-section a.forgot-password {
		font-size: 16px;
		color: #3385d9;
		float: right;
	}

	.login-register .social-list a {
		text-align: center;
		display: inline-block;
		font-size: 18px;
		margin-right: 20px;
		color: #717171;
	}

	.login-register .social-list a:hover {
		color: #28a745;
	}

	/** Social media **/
	.login-register .facebook-bg {
		color: #4867aa;
	}

	@media (max-width: 1200px) {
		.login-register .form-section {
			padding: 150px 60px 60px;
		}

		.login-register .logo {
			left: 60px;
			top: 60px
		}

		.login-register .btn-section {
			right: 60px;
			top: 60px
		}
	}

	@media (max-width: 992px) {
		.login-register .form-section {
			width: 100%;
		}

		.login-register .bg-img {
			min-height: 100%;
			border-radius: 5px;
		}

		.none-992 {
			display: none !important;
		}

		.login-register .login-box {
			max-width: 500px;
			margin: 0 auto;
			padding: 0;
		}
	}

	@media (max-width: 768px) {
		.login-register .form-section {
			padding: 30px;
		}

		.login-register .form-section {
			padding: 150px 30px 60px;
		}

		.login-register .logo {
			left: 30px;
		}

		.login-register .btn-section {
			right: 30px;
		}
	}
</style>
<?php
  if($settingRes)
  {
    if($settingRes->background_type == 'color')
    {
        ?>
          <body style="background-color: <?= $settingRes->background_color; ?>;">
        <?php
    }
    else
    {
        ?>
          <body style="background-image: url('<?= base_url() ?>uploads/loginPage/<?= $settingRes->background_image; ?>');">
        <?php
    }
  }
  else
  {
    ?>
     <body class="login_admin"<?php if(is_rtl()){ echo ' dir="rtl"'; } ?>>
    <?php
  }
?>
<div class="sign padding-50">
	<div class="login-register">
		<div class="container">
			<div class="row login-box">
				<div class="col-lg-5 bg-color-15 pad-0 none-992 bg-img"></div>
				<div class="col-lg-7 pad-0 form-info">
					<div class="form-section align-self-center">
						<div class="btn-section clearfix">
							<a href="<?= base_url() ?>login" class="link-btn active btn-1">Login</a>
							<a href="register-31.html" class="link-btn btn-2 default-bg active-bg">Register</a>
						</div>
						<div class="logo">
							<a href="<?= base_url(); ?>">
								<img src="<?= base_url() ?>uploads/loginPage/<?= $settingRes->logo_image; ?>" alt="logo">
							</a>
						</div>
						<h1>Welcome!</h1>
						<h3>Create an Account!</h3>
						<div class="clearfix"></div>
						<?php echo form_open_multipart($this->uri->uri_string(),array('id'=>'ms-form', 'class'=>'register-form')); ?>
							<section>
								<div class="form-group form-box">
									<label class="mb-10">Email address</label>
									<?php echo form_error('email'); ?>
									<input type="email" name="email" id="company_email" class="form-control mb-20" onkeyup="errorRemove('company_email')" onchange="checkemailvalidation()" placeholder="Email" />
									<p class="text-danger company_email"></p>
								</div>
								<div class="form-group form-box">
									<label class="mb-10">Password</label>
									<?php echo form_error('password'); ?>
									<input type="password" onkeyup="errorRemove('company_password')" name="password" id="password" class="form-control mb-20" placeholder="Password" />
									<p class="text-danger company_password"></p>
								</div>
								<div class="form-group form-box">
									<label class="mb-10">Confirm Password</label>
									<?php echo form_error('passwordr'); ?>
									<input type="password" onkeyup="errorRemove('company_passwordr')" name="passwordr" id="passwordr" class="form-control mb-20" placeholder="Confirm Password" />
									<p class="text-danger company_passwordr"></p>
								</div>
							</section>
							<section>
								<p><b>Company/ Garage Information</b></p>
								<div class="row">
								  <div class="form-group col-xl-12 col-lg-12 col-12">
									<?php echo form_error('name'); ?>
									<input type="text" name="name" onkeyup="errorRemove('garagename')" id="garagename" class="form-control mb-20" placeholder="Garage Name" />
									<p class="text-danger garagename"></p>
								  </div>
								</div>
								<div class="row">
									<div class="form-group col-xl-12 col-lg-12 col-12">
										<label>Services</label>
										<div class="input-group mb-3">
											<select class="form-control chosen-select mb-20" autocomplete="off" multiple name="servicesID[]">
												<option value=""></option>
												<?php
													if($ServiceResult)
													{
														foreach($ServiceResult as $sr)
														{
															?>
																<option value="<?= $sr->id; ?>"><?= $sr->name; ?></option>
															<?php
														}
													}
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="row" style="display:none;">
								  <div class="form-group col-xl-12 col-lg-12 col-12">
									<div class="input-group mb-3">
									  <input type="text" name="activities" class="form-control" placeholder="Activities">
									  <div class="input-group-append">
										<button class="btn btn-secondary add_button" type="button">
										  <i class="fa fa-plus" aria-hidden="true"></i>
										</button>
									  </div>
									</div>
									<div class="row field_wrapperActivities"></div>
								  </div>
								  
								</div>
								<div class="row">
								  <div class="form-group col-xl-6 col-lg-6 col-12">
									<div class="input-group mb-3">
									  <input type="text" name="mobile" onkeyup="errorRemove('garagemobile')" id="garagemobile" class="form-control" maxlength="10" autocomplete="off" placeholder="Mobile">
									  <div class="input-group-append">
										<button class="btn btn-secondary add_mobile" type="button">
										  <i class="fa fa-plus" aria-hidden="true"></i>
										</button>
									  </div>
									</div>
									<p class="text-danger garagemobile"></p>
									<div class="row field_mobile"></div>
								  </div>
								  <div class="form-group col-xl-6 col-lg-6 col-12">
									<div class="input-group mb-3">
									  <input type="text" name="telephone" onkeyup="errorRemove('garagetelphone')" id="garagetelphone" class="form-control" autocomplete="off" maxlength="8" placeholder="Telephone">
									  <div class="input-group-append">
										<button class="btn btn-secondary add_telephone" type="button">
										  <i class="fa fa-plus" aria-hidden="true"></i>
										</button>
									  </div>
									</div>
									<p class="text-danger garagetelphone"></p>
									<div class="row field_telephone"></div>
								  </div>
								  <div class="form-group col-xl-6 col-lg-6 col-12 hide" style="display:none;">
									<div class="input-group mb-3">
									  <?php echo form_error('fax'); ?>
									  <input type="text" name="fax" class="form-control" placeholder="Fax">
									  <div class="input-group-append">
										<button class="btn btn-secondary add_fax" type="button">
										  <i class="fa fa-plus" aria-hidden="true"></i>
										</button>
									  </div>
									</div>
									<div class="row field_fax"></div>
								  </div>
								</div>
								<div class="row">
								  <div class="form-group col-xl-12 col-lg-12 col-12" style="display:none;">
									<div class="input-group mb-3">
									  <?php echo form_error('company_email'); ?>
									  <input type="email" name="company_email" class="form-control" placeholder="Email Address">
									  <div class="input-group-append">
										<button class="btn btn-secondary add_company_email" type="button">
										  <i class="fa fa-plus" aria-hidden="true"></i>
										</button>
									  </div>
									</div>
									<div class="row field_company_email"></div>
								  </div>
								</div>
								<div class="row" style="display:none;">
								  <div class="form-group col-xl-12 col-lg-12 col-12">
									<?php echo form_error('website_url'); ?>
									<input type="text" name="website_url" class="form-control mb-20" autocomplete="off" placeholder="Website URL" />
								  </div>
								  
								</div>
								
								<div class="row">
									<div class="form-group col-xl-12 col-lg-12 col-12">
										<label>City / Emirate</label>
										<div class="input-group mb-3">
											<?php echo form_error('city_emirate'); ?>
											<select class="form-control chosen-select mb-20" autocomplete="off" name="city_emirate" id="city_id" onChange="getAreaList(this.value); ">
												<option></option>
												<?php
													if($city_result)
													{
														foreach($city_result as $rrr)
														{
															?>
																<option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
															<?php
														}
													}
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-xl-12 col-lg-12 col-12">
										<label>Area</label>
										<div class="input-group mb-3">
											<?php echo form_error('area'); ?>
											<select class="form-control chosen-select mb-20" autocomplete="off" id="area_id" multiple name="area[]">
											</select>
										</div>
									</div>
								</div>
								<div class="row">
								  <div class="form-group col-xl-12 col-lg-12 col-12">
									<div class="input-group mb-3">
										<?php echo form_error('address'); ?>
										<input type="text" name="address" onkeyup="errorRemove('autocomplete')" autocomplete="off"  id="autocomplete" class="form-control" placeholder="Address">
									</div>
									<p class="text-danger autocomplete"></p>
								  </div>
								</div>
								<div class="row hide" style="display:none;">
								  <div class="form-group col-xl-12 col-lg-12 col-12">
									<div class="input-group mb-3">
									  <?php echo form_error('latitude'); ?>
									  <input type="text" class="form-control" placeholder="Google Map Location">
									  <div class="input-group-append">
										<button class="btn btn-secondary" type="button">
										  <i class="fa fa-plus" aria-hidden="true"></i>
										</button>
									  </div>
									</div>
								  </div>
								</div>
								<div class="row">
								  <div class="form-group col-xl-12 col-lg-12 col-12">
									<div class="input-group upload-wrap rounded-pill bg-white">
									  <input type="file" accept=".jpg,.png,.jpeg,image/jpeg,image/png,image/jpg" name="image" id="file-upload1" />
									  <label for="file-upload1" class="file-upload">Upload License</label>
									  <div id="file-upload1-filename"></div>
									</div>
								  </div>
								</div>
								<div class="row">
									<div class="form-group col-xl-12 col-lg-12 col-12">
										<div class="input-group upload-wrap rounded-pill bg-white mb-20">
											<p><?php echo form_error('logo'); ?></p>
										  <input type="file" name="logo" accept=".jpg,.png,.jpeg,image/jpeg,image/png,image/jpg" id="file-upload" class="form-control" />
										  <label for="file-upload" class="file-upload">Upload Logo</label>
										  <div id="file-upload-filename"></div>
										</div>
									</div>
								</div>
							</section>
							<div class="form-group clearfix">
								<div class="button w-100" onclick="stepfirstreq()" id="pnext">Next</div>
								<div class="button w-100" onclick="stepfirstreq()" id="next" style="display:none;">Next</div>
								<button class="button w-100" id="errrosubmit" style="display:none;" >Register</button>
								<button class="button w-100" onclick="formsubmit();" id="submit" type="button">Register</button>
								<div class="button w-100" onclick="stepsecondreq()"  id="prev">Previous</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
  <!--<div class="container">
    <div class="row">
      <div class="offset-lg-3 col-lg-6 col-12">
        <div class="logo-wrapper text-center mb-40">
          <a href="<?= base_url(); ?>">
            <?php
              if($settingRes->logo_image != '')
              {
                ?>
                  <img src="<?= base_url() ?>uploads/loginPage/<?= $settingRes->logo_image; ?>" alt="" style="width:40%;">
                <?php
              }
            ?>
          </a>
        </div>
        <div class="sign-in-area">
          <h5 class="text-center"><b>Create an Account</b></h5>
          <br>
          <div id="svg_wrap" style="display:none;"></div>
            <p><?php echo form_error('logo'); ?></p>
            <?php echo form_open_multipart($this->uri->uri_string(),array('id'=>'ms-form', 'class'=>'register-form')); ?>
                <section>
                    <p><b>Primary Account Contact Information</b></p>
                    <div class="row">
                      <div class="form-group col-xl-12 col-lg-12 col-12">
                        <?php echo form_error('email'); ?>
                        <input type="email" name="email" id="company_email" class="form-control mb-20" onkeyup="errorRemove('company_email')" onchange="checkemailvalidation()" placeholder="Email" />
                        <p class="text-danger company_email"></p>
                      </div>
                      <div class="form-group col-xl-12 col-lg-12 col-12">
                        <?php echo form_error('password'); ?>
                        <input type="password" onkeyup="errorRemove('company_password')" name="password" id="password" class="form-control mb-20" placeholder="Password" />
                        <p class="text-danger company_password"></p>
                      </div>
                      <div class="form-group col-xl-12 col-lg-12 col-12">
                        <?php echo form_error('passwordr'); ?>
                        <input type="password" onkeyup="errorRemove('company_passwordr')" name="passwordr" id="passwordr" class="form-control mb-20" placeholder="Confirm Password" />
                        <p class="text-danger company_passwordr"></p>
                      </div>
                    </div>
                </section>
            
                <section>
                    <p><b>Company/ Garage Information</b></p>
                    <div class="row">
                      <div class="form-group col-xl-12 col-lg-12 col-12">
                        <?php echo form_error('name'); ?>
                        <input type="text" name="name" onkeyup="errorRemove('garagename')" id="garagename" class="form-control mb-20" placeholder="Garage Name" />
                        <p class="text-danger garagename"></p>
                      </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xl-12 col-lg-12 col-12">
                            <label>Services</label>
                            <div class="input-group mb-3">
                                <select class="form-control chosen-select mb-20" autocomplete="off" multiple name="servicesID[]">
                                    <option value=""></option>
                                    <?php
                                        if($ServiceResult)
                                        {
                                            foreach($ServiceResult as $sr)
                                            {
                                                ?>
                                                    <option value="<?= $sr->id; ?>"><?= $sr->name; ?></option>
                                                <?php
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="display:none;">
                      <div class="form-group col-xl-12 col-lg-12 col-12">
                        <div class="input-group mb-3">
                          <input type="text" name="activities" class="form-control" placeholder="Activities">
                          <div class="input-group-append">
                            <button class="btn btn-secondary add_button" type="button">
                              <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                          </div>
                        </div>
                        <div class="row field_wrapperActivities"></div>
                      </div>
                      
                    </div>
                    <div class="row">
                      <div class="form-group col-xl-6 col-lg-6 col-12">
                        <div class="input-group mb-3">
                          <input type="text" name="mobile" onkeyup="errorRemove('garagemobile')" id="garagemobile" class="form-control" maxlength="10" autocomplete="off" placeholder="Mobile">
                          <div class="input-group-append">
                            <button class="btn btn-secondary add_mobile" type="button">
                              <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                          </div>
                        </div>
                        <p class="text-danger garagemobile"></p>
                        <div class="row field_mobile"></div>
                      </div>
                      <div class="form-group col-xl-6 col-lg-6 col-12">
                        <div class="input-group mb-3">
                          <input type="text" name="telephone" onkeyup="errorRemove('garagetelphone')" id="garagetelphone" class="form-control" autocomplete="off" maxlength="8" placeholder="Telephone">
                          <div class="input-group-append">
                            <button class="btn btn-secondary add_telephone" type="button">
                              <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                          </div>
                        </div>
                        <p class="text-danger garagetelphone"></p>
                        <div class="row field_telephone"></div>
                      </div>
                      <div class="form-group col-xl-6 col-lg-6 col-12 hide" style="display:none;">
                        <div class="input-group mb-3">
                          <?php echo form_error('fax'); ?>
                          <input type="text" name="fax" class="form-control" placeholder="Fax">
                          <div class="input-group-append">
                            <button class="btn btn-secondary add_fax" type="button">
                              <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                          </div>
                        </div>
                        <div class="row field_fax"></div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-xl-12 col-lg-12 col-12" style="display:none;">
                        <div class="input-group mb-3">
                          <?php echo form_error('company_email'); ?>
                          <input type="email" name="company_email" class="form-control" placeholder="Email Address">
                          <div class="input-group-append">
                            <button class="btn btn-secondary add_company_email" type="button">
                              <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                          </div>
                        </div>
                        <div class="row field_company_email"></div>
                      </div>
                    </div>
                    <div class="row" style="display:none;">
                      <div class="form-group col-xl-12 col-lg-12 col-12">
                        <?php echo form_error('website_url'); ?>
                        <input type="text" name="website_url" class="form-control mb-20" autocomplete="off" placeholder="Website URL" />
                      </div>
                      
                    </div>
                    
                    <div class="row">
                        <div class="form-group col-xl-12 col-lg-12 col-12">
                            <label>City / Emirate</label>
                            <div class="input-group mb-3">
                                <?php echo form_error('city_emirate'); ?>
                                <select class="form-control chosen-select mb-20" autocomplete="off" name="city_emirate" id="city_id" onChange="getAreaList(this.value); ">
                                    <option></option>
                                    <?php
                                        if($city_result)
                                        {
                                            foreach($city_result as $rrr)
                                            {
                                                ?>
                                                    <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
                                                <?php
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xl-12 col-lg-12 col-12">
                            <label>Area</label>
                            <div class="input-group mb-3">
                                <?php echo form_error('area'); ?>
                                <select class="form-control chosen-select mb-20" autocomplete="off" id="area_id" multiple name="area[]">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-xl-12 col-lg-12 col-12">
                        <div class="input-group mb-3">
                            <?php echo form_error('address'); ?>
                            <input type="text" name="address" onkeyup="errorRemove('autocomplete')" autocomplete="off"  id="autocomplete" class="form-control" placeholder="Address">
                        </div>
                        <p class="text-danger autocomplete"></p>
                      </div>
                    </div>
                    <div class="row hide" style="display:none;">
                      <div class="form-group col-xl-12 col-lg-12 col-12">
                        <div class="input-group mb-3">
                          <?php echo form_error('latitude'); ?>
                          <input type="text" class="form-control" placeholder="Google Map Location">
                          <div class="input-group-append">
                            <button class="btn btn-secondary" type="button">
                              <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-xl-12 col-lg-12 col-12">
                        <div class="input-group upload-wrap rounded-pill bg-white">
                          <input type="file" accept=".jpg,.png,.jpeg,image/jpeg,image/png,image/jpg" name="image" id="file-upload1" />
                          <label for="file-upload1" class="file-upload">Upload License</label>
                          <div id="file-upload1-filename"></div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xl-12 col-lg-12 col-12">
                            <div class="input-group upload-wrap rounded-pill bg-white mb-20">
                                <p><?php echo form_error('logo'); ?></p>
                              <input type="file" name="logo" accept=".jpg,.png,.jpeg,image/jpeg,image/png,image/jpg" id="file-upload" class="form-control" />
                              <label for="file-upload" class="file-upload">Upload Logo</label>
                              <div id="file-upload-filename"></div>
                            </div>
                        </div>
                    </div>
                </section>            
                <div class="button w-100" onclick="stepfirstreq()" id="pnext" style="display:block;">Next</div>
                <div class="button w-100" onclick="stepfirstreq()" id="next" style="display:none;">Next</div>
                <button class="button w-100" id="errrosubmit" style="display:none;" >Register</button>
                <button class="button w-100" onclick="formsubmit();" id="submit" type="button">Register</button>
                <div class="button w-100" onclick="stepsecondreq()"  id="prev">Previous</div>
                <hr />
                <div class="text-center">
                    <a href="<?= base_url() ?>login">Already have an account? <b>Login</b></a>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>-->
</div>
<script>
    function formsubmit()
    {
      var fuData = document.getElementById('file-upload');
      var FileUploadPath = fuData.value;
      if (FileUploadPath == '') 
      {
        //alert("Logo is required");
        $('#file-upload-filename').html('<span class="text-danger">Logo is required</span>');
      }
      else
      {
        upload_check();
      }
      var fuDatas = document.getElementById('file-upload1');
      var FileUploadPaths = fuDatas.value;
      if (FileUploadPaths == '') 
      {
        //alert("Logo is required");
        $('#file-upload1-filename').html('<span class="text-danger">Logo is required</span>');
      }
      else
      {
        upload_checkLicense();
      }
      var err = 1;
      var garagename = $('#garagename').val();
      var garagemobile = $('#garagemobile').val();
      var garagetelphone = $('#garagetelphone').val();
      var autocomplete = $('#autocomplete').val();
      if(garagename == '')
      {
        $('.garagename').text('Garage Name field is required');
        err++;
      }
      if(garagemobile == '')
      {
        $('.garagemobile').text('Mobile field is required');
        err++;
      }
      if(garagetelphone == '')
      {
        $('.garagetelphone').text('Telephone field is required');
        err++;
      }
      if(autocomplete == '')
      {
        $('.autocomplete').text('Address field is required');
        err++;
      }
      if(err == 1)
      {
        $('#ms-form').submit(); 
      }
      else
      {
        return false;
      }
    }
    
    function upload_check()
    { 
      var upl = document.getElementById("file-upload");
      var max = (2 * 1024 * 1024);

      if(upl.files[0].size > max)
      {
        //alert("File too big! Maximum size upto 2MB");
        upl.value = "";
        $('#file-upload-filename').html('<span class="text-danger">File too big! Maximum size upto 2MB</span>');
      }
    };
    function upload_checkLicense()
    { 
      var upl = document.getElementById("file-upload1");
      var max = (2 * 1024 * 1024);

      if(upl.files[0].size > max)
      {
        //alert("File too big! Maximum size upto 2MB");
        upl.value = "";
        $('#file-upload1-filename').html('<span class="text-danger">File too big! Maximum size upto 2MB</span>');
      }
    };


    function stepfirstreq()
    {
      var email = $('#company_email').val();
      var password = $('#password').val();
      var passwordr = $('#passwordr').val();
      var sfirst = 1;
      if(email == '')
      {
        checkemailvalidation();
        sfirst++;
      }
      if(password == '')
      {
        $('.company_password').text('Password field is required');
        sfirst++;
      }
      if(passwordr == '')
      {
        $('.company_passwordr').text('Password field is required');
        sfirst++;
        return false;
      }
      if(passwordr != password)
      {
        $('.company_passwordr').text('Confirm password is not match password');
        sfirst++;
      }
      if(sfirst == 1)
      {
        $('#pnext').css('display','none');
        $('#next').css('display','block');
        $('#next').click();
      }
    }

    function stepsecondreq()
    {
      $('#pnext').css('display','block');
      $('#next').css('display','none');
    }

    function test_str() { 
        var res; 
        var str = 
            document.getElementById("company_passwordr").value; 
        if (str.match(/[a-z]/g) && str.match( 
                /[A-Z]/g) && str.match( 
                /[0-9]/g) && str.match( 
                /[^a-zA-Z\d]/g) && str.length >= 8) 
            res = "TRUE"; 
        else 
            res = "FALSE"; 
    } 

    $(document).ready(function(){
        var maxField = 4; //Input fields increment limitation
        var addButton = $('.add_button'); //Add button selector
        var wrapper = $('.field_wrapperActivities'); //Input field wrapper
        var fieldHTML = '<div class="col-md-12"><div class="input-group mb-3"><input type="text" name="activities_extra[]" required class="form-control" placeholder="Activities"><button class="btn btn-secondary remove_buttonActivities" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button></div></div>';
        var x = 1; //Initial field counter is 1
        
        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){ 
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
            }
        });
        
        //Once remove button is clicked
        $(wrapper).on('click', '.remove_buttonActivities', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
        });
        
        
        /* Telephone */
        var maxTelephone = 4; //Input fields increment limitation
        var addButtonTele = $('.add_telephone'); //Add button selector
        var wrapperTele = $('.field_telephone'); //Input field wrapper
        var fieldHTMLTele = '<div class="col-md-12"><div class="input-group mb-3"><input type="text" name="telephone_extra[]" required class="form-control" placeholder="Telephone"><button class="btn btn-secondary remove_telephone" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button></div></div>';
        var xx = 1; //Initial field counter is 1
        
        $(addButtonTele).click(function(){
            if(xx < maxTelephone){ 
                xx++; //Increment field counter
                $(wrapperTele).append(fieldHTMLTele); //Add field html
            }
        });
        
        $(wrapperTele).on('click', '.remove_telephone', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            xx--; //Decrement field counter
        });
        /*---// End ---*/
        
        /* Mobile */
        var maxMobile = 4; //Input fields increment limitation
        var addButtonMobile = $('.add_mobile'); //Add button selector
        var wrapperMobile = $('.field_mobile'); //Input field wrapper
        var fieldHTMLMobile = '<div class="col-md-12"><div class="input-group mb-3"><input type="text" name="mobile_extra[]" required class="form-control" placeholder="Mobile"><button class="btn btn-secondary remove_mobile" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button></div></div>';
        var mob = 1;
        
        $(addButtonMobile).click(function(){
            if(mob < maxMobile){ 
                mob++; //Increment field counter
                $(wrapperMobile).append(fieldHTMLMobile); //Add field html
            }
        });
        
        $(wrapperMobile).on('click', '.remove_mobile', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            mob--; //Decrement field counter
        });
        /*---// End ---*/
        
        /* Fax */
        var maxfax = 4; //Input fields increment limitation
        var addButtonfax = $('.add_fax'); //Add button selector
        var wrapperfax = $('.field_fax'); //Input field wrapper
        var fieldHTMLfax = '<div class="col-md-12"><div class="input-group mb-3"><input type="text" name="fax_extra[]" required class="form-control" placeholder="Fax"><button class="btn btn-secondary remove_fax" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button></div></div>';
        var fax = 1;
        
        $(addButtonfax).click(function(){
            if(fax < maxfax){ 
                fax++; //Increment field counter
                $(wrapperfax).append(fieldHTMLfax); //Add field html
            }
        });
        
        $(wrapperfax).on('click', '.remove_fax', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            fax--; //Decrement field counter
        });
        /*---// End ---*/
        /* company email */
        var maxcompany_email = 4; //Input fields increment limitation
        var addButtoncompany_email = $('.add_company_email'); //Add button selector
        var wrappercompany_email = $('.field_company_email'); //Input field wrapper
        var fieldHTMLcompany_email = '<div class="col-md-12"><div class="input-group mb-3"><input type="text" name="company_email_extra[]" required class="form-control" placeholder="Email Address"><button class="btn btn-secondary remove_company_email" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button></div></div>';
        var company_email = 1;
        
        $(addButtoncompany_email).click(function(){
            if(company_email < maxcompany_email){ 
                company_email++; //Increment field counter
                $(wrappercompany_email).append(fieldHTMLcompany_email); //Add field html
            }
        });
        
        $(wrappercompany_email).on('click', '.remove_company_email', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            company_email--; //Decrement field counter
        });
        /*---// End ---*/
        
        /* company email */
        var maxaddress = 4; //Input fields increment limitation
        var addButtonaddress = $('.add_address'); //Add button selector
        var wrapperaddress = $('.field_address'); //Input field wrapper
        var fieldHTMLaddress = '<div class="col-md-12"><div class="input-group mb-3"><textarea rows="3" name="address_extra[]" required class="form-control" placeholder="Address"></textarea><button class="btn btn-secondary remove_address" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button></div></div>';
        var add = 1;
        
        $(addButtonaddress).click(function(){
            if(add < maxaddress){ 
                add++; //Increment field counter
                $(wrapperaddress).append(fieldHTMLaddress); //Add field html
            }
        });
        
        $(wrapperaddress).on('click', '.remove_address', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            add--; //Decrement field counter
        });
        /*---// End ---*/
    });
    
    /* checkemailvalidation */
    function checkemailvalidation()
    {
        var company_email = $('#company_email').val();
        if(company_email)
        {
            var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            if(company_email.match(mailformat))
            {
                var str = "email="+company_email+"&"+csrfData['token_name']+"="+csrfData['hash'];
        	    $.ajax({
        	        url: '<?= site_url()?>garage/checkemailvalidation',
        	        type: 'POST',
        	        data: str,
        	        //dataType: 'json',
        	        cache: false,
        	        success: function(resp){
        	            if(resp)
        	            {
        	                $('#submit').css('display','none');
        	                $('#errrosubmit').css('display','block');
        	                $('.company_email').text('Email is already exists');
        	                return false;
        	            }
        	            else
        	            {
        	                $('#submit').css('display','block');
        	                $('#errrosubmit').css('display','none');
        	            }
        	        }
        	    });
            }
            else{
                $('.company_email').text('You have entered an invalid email address!');
                return false;
            }
        }
        else{
            $('.company_email').text('Account email is required!');
            return false;
        }
    }
    function errorRemove(name)
    {
        $('.'+name).text('');
    }
</script>
<?php include('include/scripts.php')?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJ2Tlhs9oB6ByIP4yPWBu-aFAVrhLWt2M&libraries=places&callback=initialize" async defer></script>
<script>// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

$("#autocomplete").on('focus', function () {
    geolocate();
});
 

var placeSearch, autocomplete;
var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
};
 


function initialize() {
    // Create the autocomplete object, restricting the search
    // to geographical location types.
    autocomplete = new google.maps.places.Autocomplete(
    /** @type {HTMLInputElement} */ (document.getElementById('autocomplete')), {
        types: ['geocode']
    });
 
    // populate the address fields in the form.
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        fillInAddress();
    });
    
     autocomplete2 = new google.maps.places.Autocomplete(
    /** @type {HTMLInputElement} */ (document.getElementById('autocomplete2')), {
        types: ['geocode']
    });
 
    // populate the address fields in the form.
    google.maps.event.addListener(autocomplete2, 'place_changed', function () {
        fillInAddressTo();
    });
    
    
    
     autocomplete3 = new google.maps.places.Autocomplete(
    /** @type {HTMLInputElement} */ (document.getElementById('autocomplete3')), {
        types: ['geocode']
    });
 
    // populate the address fields in the form.
    google.maps.event.addListener(autocomplete3, 'place_changed', function () {
        fillInAddress3();
    });
  
}
 

// [START region_fillform]
function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();
 vm.immediateShipping.fromAddress = place.formatted_address;
 vm.immediateShipping.from_address = place.formatted_address;
    document.getElementById("latitude").value = place.geometry.location.lat();
    document.getElementById("longitude").value = place.geometry.location.lng();
    
      vm.immediateShipping.from_latitude =  place.geometry.location.lat();
      vm.immediateShipping.from_longitude =  place.geometry.location.lng();
      
    for (var component in componentForm) {
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        // console.log(addressType);
        if (componentForm[addressType]) {
        // console.log(place.address_components[i][componentForm[addressType]]);
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
             if(addressType =='street_number'){
                vm.immediateShipping.from_street_number = val;
            } 
            if(addressType =='route'){
                vm.immediateShipping.from_route = val;
            } 
             if(addressType =='locality'){
                vm.immediateShipping.from_locality = val;
                vm.immediateShipping.from_city = val;
            } 
             if(addressType =='administrative_area_level_1'){
                vm.immediateShipping.administrative_area_level_1 = val;
                vm.immediateShipping.from_state = val;
            } 
            if(addressType =='country'){
                vm.immediateShipping.from_country = val;
            } 
            if(addressType =='postal_code'){
                vm.immediateShipping.from_postal_code = val;
                vm.immediateShipping.from_origin_pin_code = val;
            } 
        }
    }
}

// [START region_fillform]
function fillInAddressTo() {
    // Get the place details from the autocomplete object.
    var place = autocomplete2.getPlace();
        vm.immediateShipping.toAddress = place.formatted_address;
        vm.immediateShipping.to_address = place.formatted_address;
    document.getElementById("latitude_to").value = place.geometry.location.lat();
    document.getElementById("longitude_to").value = place.geometry.location.lng();
    
        vm.immediateShipping.latitude_to =  place.geometry.location.lat();
        vm.immediateShipping.longitude_to =  place.geometry.location.lng();
      
    for (var component in componentForm) {
        document.getElementById(component+'_to').value = '';
        document.getElementById(component+'_to').disabled = false;
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
       
        if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType+'_to').value = val;
             
            if(addressType =='street_number'){
                vm.immediateShipping.to_street_number = val;
            } 
            if(addressType =='route'){
                vm.immediateShipping.to_route = val;
            } 
             if(addressType =='locality'){
                vm.immediateShipping.to_locality = val;
                  vm.immediateShipping.to_city = val;
            } 
             if(addressType =='administrative_area_level_1'){
                vm.immediateShipping.to_administrative_area_level_1 = val;
                vm.immediateShipping.to_state = val;
            } 
            if(addressType =='country'){
                vm.immediateShipping.to_country = val;
            } 
            if(addressType =='postal_code'){
                vm.immediateShipping.to_postal_code = val;
                vm.immediateShipping.to_origin_pin_code = val;
            } 
        }
    }
}

 



function fillInAddress3() {
    // Get the place details from the autocomplete object.
    var place = autocomplete3.getPlace();

    document.getElementById("latitude").value = place.geometry.location.lat();
    document.getElementById("longitude").value = place.geometry.location.lng();
    
        vm.SignUp.latitude =  place.geometry.location.lat();
        vm.SignUp.longitude =  place.geometry.location.lng();
      
    for (var component in componentForm) {
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
       
        if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
             
            if(addressType =='street_number'){
                
                vm.SignUp.street_number = val;
            } 
            if(addressType =='route'){
                vm.SignUp.route = val;
            } 
             if(addressType =='locality'){
                vm.SignUp.locality = val;
            } 
             if(addressType =='administrative_area_level_1'){
                vm.SignUp.administrative_area_level_1 = val;
            } 
            if(addressType =='country'){
                vm.SignUp.country = val;
            } 
            if(addressType =='postal_code'){
                vm.SignUp.postal_code = val;
            } 
        }
    }
}

// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var geolocation = new google.maps.LatLng(
            position.coords.latitude, position.coords.longitude);

            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            document.getElementById("latitude").value = latitude;
            document.getElementById("longitude").value = longitude;

            autocomplete.setBounds(new google.maps.LatLngBounds(geolocation, geolocation));
            autocomplete.setComponentRestrictions({'country': 'USA'}); 
        });
    }

}


initialize();

</script>