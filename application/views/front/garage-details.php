<?php include('include/header.php')?>
<?php include('include/navbar.php')?><!--Full Width Sider Start-->
<div class="breadcrumb-area" style="background-image: url('<?= site_url() ?>assets/themes/immersive/images/about/about-us-banner-bg.png')">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner text-center">
                    <h1 class="page-title">Garage Details</h1>
                    <ul class="page-list">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li>Garage Details</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Breadcrumb End-->
<!--About Story Start-->
<section class="car-part padding-top-120 padding-bottom-110">
    <div class="container">
        <div class="row">
            <div class="offset-lg-1 col-lg-10 col-12">
                <div class="panel_s section-heading section-<?php echo _l($title); ?>">
                    <div class="panel-body p-0">
                        <div class="banner-bg">
                            <h2 class="no-margin section-text font-24">APEX AUTO GARAGE <a href="javascript:void(0);" data-toggle="modal" data-target="#shareModal"><i class="fa fa-share-alt" aria-hidden="true"></i></a></h2>
                            <hr>
                            <h5 class="address mb-20 font-14"><i class="fa fa-map-marker" aria-hidden="true"></i> Location : 7 B St - Al Quoz - Dubai - United Arab Emirates</h5>
                            <h5 class="address mb-20 font-14"><i class="fa fa-phone" aria-hidden="true"></i> Contact : +971 50 937 7714</h5>
                            <div class="rating_wrap mt-0 font-14" style="display: flex;align-items: top;">
                                <h5 class="rating-number font-14"><i class="fa fa-star-o" aria-hidden="true"></i> Rating :</h5>
                                <div class="rating" style="width: 85px;">
                                    <div id="full-stars-example-two">
                                        <div class="rating-group">
                                            <label aria-label="1 star" class="rating__label" for="rating3-1" style="font-size: .8rem;"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <label aria-label="2 stars" class="rating__label" for="rating3-2" style="font-size: .8rem;"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <label aria-label="3 stars" class="rating__label" for="rating3-3" style="font-size: .8rem;"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <label aria-label="4 stars" class="rating__label" for="rating3-4" style="font-size: .8rem;"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                            <label aria-label="5 stars" class="rating__label" for="rating3-5" style="font-size: .8rem;"><i class="rating__icon  fa fa-star"></i></label>
                                        </div>
                                    </div>
                                </div>
                                <a href="javascript:void(0);" class="total-count">[86 Reviews]</a>
                            </div>
                            <div class="custom-options">
                                <ul>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <i class="fa fa-envelope" aria-hidden="true"></i><br>
                                            <span>SMS/ Email</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <i class="fa fa-whatsapp" aria-hidden="true"></i><br>
                                            <span>WhatsApp</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="edit-wrap">
                                <a href="javascript:void(0);">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
		<div class="col-xl-5 col-lg-5 col-12">
			<!-- Gallery Start -->
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-12">
					<div class="panel_s">
						<div class="panel-body">
							<!-- Filled Gallery Start -->
							<div class="filled-gallery-wrap">
								<div class="header-wrap p-10 bg-white">
									<div class="d-flex align-items-center">
										<div><h4 class="fw-700">Gallery</h4></div>
										<div class="ml-auto align-items-center">
											<div class="dl">
												<button class="btn btn-default" type="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="edit"> Edit</span></button>
											</div>
										</div>
									</div>
								</div>
								<hr class="mt-0 mb-0">
								<div class="row">
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery1.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery2.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery3.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery4.png" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery5.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery2.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery6.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery1.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery7.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery8.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery3.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery1.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Filled Gallery End -->
						</div>
					</div>
				</div>
			</div>
			<!-- Gallery End -->

			<!-- Services Start -->
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-12">
					<div class="panel_s">
						<div class="panel-body">
							<!-- Filled Services End -->
							<div class="filled-service-wrap">
								<div class="header-wrap p-10 bg-white">
									<div class="d-flex align-items-center">
										<div><h4 class="fw-700">Services</h4></div>
										<div class="ml-auto align-items-center">
											<div class="dl">
												<button class="btn btn-default" type="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="services-upload"> Edit</span></button>
											</div>
										</div>
									</div>
								</div>
								<hr class="mt-0 mb-0">
								<div class="garage-profile">
									<div class="product">
										<img src="http://php.manageprojects.in/halagarage/assets/images/air-conditioning.jpg">
										<h3 class="mt-5 mb-5">Car Air Conditioning Repairing</h3>
										<div class="shrt_des" style="margin-bottom:5px;"><p><span style="font-weight: 400;">For your sedan, small car or SUV or MUV, air-conditioning system is the most vital need to provide you relaxation from the outside atmosphere.</span></p>
										</div>
									</div>
								</div>
								<div class="garage-profile">
									<div class="product">
										<img src="http://php.manageprojects.in/halagarage/assets/images/painting.jpg">
										<h3 class="mt-5 mb-5">Car Painting Services</h3>
										<div class="shrt_des" style="margin-bottom:5px;"><p><span style="font-weight: 400;">Car painting services, the most common type of services required once in a year or at a certain years’ gap, is the routine service at workshop.</span></p>
										</div>
									</div>
								</div>
								<div class="garage-profile">
									<div class="product">
										<img src="http://php.manageprojects.in/halagarage/assets/images/denting.jpg">
										<h3 class="mt-5 mb-5">Auto Denting & Scratching Repairing</h3>
										<div class="shrt_des" style="margin-bottom:5px;"><p<span style="font-weight: 400;">An unwanted mark or scratch on your car after a road traffic accident or due to any other reason may spoil the overall look of your car or SUV.</span></p>
										</div>
									</div>
								</div>
							</div>
							<!-- Filled Services End -->
						</div>
					</div>
				</div>
			</div>
			<!-- Services End -->

			<!-- Products Start -->
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-12">
					<div class="panel_s">
						<div class="panel-body">
							<!-- Filled Products Starts -->
							<div class="filled-product-wrap">
								<div class="header-wrap p-10 bg-white">
									<div class="d-flex align-items-center">
										<div><h4 class="fw-700">Products</h4></div>
										<div class="ml-auto align-items-center">
											<div class="dl">
												<button class="btn btn-default" type="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="products-upload"> Edit</span></button>
											</div>
										</div>
									</div>
								</div>
								<hr class="mt-0 mb-0">
								<div class="garage-profile-product">
									<div class="product">
										<img src="http://php.manageprojects.in/halagarage/assets/images/fog_light_cover.webp">
										<h3 class="mt-5 mb-5 font-18">Fog Light Cover</h3>
										<div class="shrt_des" style="margin-bottom:5px;"><p><span style="font-weight: 400;">Electrical Services</span></p>
										</div>
									</div>
								</div>
								<div class="garage-profile-product">
									<div class="product">
										<img src="http://php.manageprojects.in/halagarage/assets/images/wheel.webp">
										<h3 class="mt-5 mb-5 font-18">Wheel Rim</h3>
										<div class="shrt_des" style="margin-bottom:5px;"><p><span style="font-weight: 400;">Tyre Replacement & Services</span></p>
										</div>
									</div>
								</div>
								<div class="garage-profile-product">
									<div class="product">
										<img src="http://php.manageprojects.in/halagarage/assets/images/bumper_bracket.webp">
										<h3 class="mt-5 mb-5 font-18">Bumper Bracket</h3>
										<div class="shrt_des" style="margin-bottom:5px;"><p><span style="font-weight: 400;">Body Parts Repairing</span></p>
										</div>
									</div>
								</div>
							</div>
							<!-- Filled Products End -->
						</div>
					</div>
				</div>
			</div>
			<!-- Products End -->
		</div>
		<div class="col-xl-7 col-lg-7 col-12">
			<div class="panel_s">
				<div class="panel-body">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#home">Newsfeed</a></li>
						<li><a data-toggle="tab" href="#menu1">About</a></li>
						<li><a data-toggle="tab" href="#menu2">Reviews</a></li>
					</ul>

					<div class="tab-content">
						<div id="home" class="tab-pane active">
							<div class="newsfeed-wrap">
								<div class="card card-tabel mt-20 text-post newsfeed-data" style="display: block;">
									<div class="card-body">
										<div class="media media-author">
											<div class="posted-logo mr-2">
												<img src="http://php.manageprojects.in/halagarage/assets/images/user-placeholder.jpg" alt="" class="img-fluid">
											</div>
											<div class="media-body">
												<div class="tooltip-container">
													<h6><a href="javascript:void(0);" class="media"> Test  One </a></h6>
												</div> 
												<p>1 week ago</p>
											</div> 
										</div>
										<div class="post-description mt-10 ">
											<p class="title">xx </p>
											<p class="description">If you're in search of the best HD Wallpapers for Windows 7, you've come to the right place. We offer an extraordinary number of HD images that will instantly freshen up your smartphone or computer... <a href="javascript:void(0)" class="read-more">read more</a></p>
										</div>
									</div>
								</div>
								<div class="card card-tabel mt-20 text-post newsfeed-data" style="display: block;">
									<div class="card-body">
										<div class="media media-author">
											<div class="posted-logo mr-2">
												<img src="http://php.manageprojects.in/halagarage/assets/images/user-placeholder.jpg" alt="" class="img-fluid">
											</div>
											<div class="media-body">
												<div class="tooltip-container">
													<h6><a href="javascript:void(0);" class="media"> Test  One </a></h6>
												</div> 
												<p>1 week ago</p>
											</div> 
										</div>
										<div class="post-description mt-10 ">
											<div id="gallery1"></div>
										</div>
									</div>
								</div>
								<div class="card card-tabel mt-20 text-post newsfeed-data" style="display: block;">
									<div class="card-body">
										<div class="media media-author">
											<div class="posted-logo mr-2">
												<img src="http://php.manageprojects.in/halagarage/assets/images/user-placeholder.jpg" alt="" class="img-fluid">
											</div>
											<div class="media-body">
												<div class="tooltip-container">
													<h6><a href="javascript:void(0);" class="media"> Test  One </a></h6>
												</div> 
												<p>1 week ago</p>
											</div> 
										</div>
										<div class="post-description mt-10 ">
											<p>55 Windows 8 Wallpapers in HD For Free Download</p>
											<div id="gallery6"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="menu1" class="tab-pane fade">
							<div class="d-flex align-items-center">
								<div><h4 class="fw-700">Basic Business Information</h4></div>
							</div>
							<p class="font-14 lh-20">A retailer cum service provider, Car Master Garage in UAE is in the business of automotive products as well as full-scale automotive services since 2017. This establishment offers comprehensive car and bike care across the city through its wide network, comprising of multiple stores and mobile vehicles. Find a vast range of automotive products such as tyres, batteries, alloy wheels, oil, accessories and genuine parts for cars, two-wheelers and commercial vehicles. Run by professionals in a professional manner, this dealer looks to deliver beyond expectations when providing world-class products and services to their customers day in and day out. It employs a proficient and competent team of sales personnel and technicians,who uphold this firm's commitment towards offering impeccable services. </p>
						</div>
						<div id="menu2" class="tab-pane fade">
							<div class="review-wrap">
								<div class="profile-wrap">
									<img src="http://php.manageprojects.in/halagarage/assets/images/user-placeholder.jpg" class="img-fluid">
								</div>
								<div class="profile-content">
									<span class="user-name">Harish Paul</span>
									<div class="date-time">
										<div class="rating_wrap mt-5">
											<div class="rating">
												<div id="full-stars-example-two">
													<div class="rating-group">
														<label aria-label="1 star" class="rating__label" for="rating3-1"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="2 stars" class="rating__label" for="rating3-2"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="3 stars" class="rating__label" for="rating3-3"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="4 stars" class="rating__label" for="rating3-4"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="5 stars" class="rating__label" for="rating3-5"><i class="rating__icon  fa fa-star"></i></label>
													</div>
												</div>
											</div>
											<a href="javascript:void(0);" class="rating_num">May 4, 2020</a>
										</div>
									</div>
									<div class="review-text"> 
										<span>he processes are constantly shifting and changing which makes it difficult at times to know what is expected of you and what the current objectives are. - Leadership can be unresponsive with follow through. A lot is promised to workers, and some of these things are swept aside or forgotten as other things gain priority. - Work life balance is lacking at times. Sales reps are often responding to emails or calls during their personal time</span>
									</div>
								</div>
							</div>
							<div class="review-wrap">
								<div class="profile-wrap">
									<img src="http://php.manageprojects.in/halagarage/assets/images/user-placeholder.jpg" class="img-fluid">
								</div>
								<div class="profile-content">
									<span class="user-name">Harish Paul</span>
									<div class="date-time">
										<div class="rating_wrap mt-5">
											<div class="rating">
												<div id="full-stars-example-two">
													<div class="rating-group">
														<label aria-label="1 star" class="rating__label" for="rating3-1"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="2 stars" class="rating__label" for="rating3-2"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="3 stars" class="rating__label" for="rating3-3"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="4 stars" class="rating__label" for="rating3-4"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="5 stars" class="rating__label" for="rating3-5"><i class="rating__icon  fa fa-star"></i></label>
													</div>
												</div>
											</div>
											<a href="javascript:void(0);" class="rating_num">May 4, 2020</a>
										</div>
									</div>
									<div class="review-text"> 
										<span>Start within 30 days, finish within 48 hours.</span>
									</div>
								</div>
							</div>
							<div class="review-wrap">
								<div class="profile-wrap">
									<img src="http://php.manageprojects.in/halagarage/assets/images/user-placeholder.jpg" class="img-fluid">
								</div>
								<div class="profile-content">
									<span class="user-name">Harish Paul</span>
									<div class="date-time">
										<div class="rating_wrap mt-5">
											<div class="rating">
												<div id="full-stars-example-two">
													<div class="rating-group">
														<label aria-label="1 star" class="rating__label" for="rating3-1"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="2 stars" class="rating__label" for="rating3-2"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="3 stars" class="rating__label" for="rating3-3"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="4 stars" class="rating__label" for="rating3-4"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="5 stars" class="rating__label" for="rating3-5"><i class="rating__icon  fa fa-star"></i></label>
													</div>
												</div>
											</div>
											<a href="javascript:void(0);" class="rating_num">May 4, 2020</a>
										</div>
									</div>
									<div class="review-text"> 
										<span>Start within 30 days, finish within 48 hours.</span>
									</div>
								</div>
							</div>
							<div class="review-wrap">
								<div class="profile-wrap">
									<img src="http://php.manageprojects.in/halagarage/assets/images/user-placeholder.jpg" class="img-fluid">
								</div>
								<div class="profile-content">
									<span class="user-name">Harish Paul</span>
									<div class="date-time">
										<div class="rating_wrap mt-5">
											<div class="rating">
												<div id="full-stars-example-two">
													<div class="rating-group">
														<label aria-label="1 star" class="rating__label" for="rating3-1"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="2 stars" class="rating__label" for="rating3-2"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="3 stars" class="rating__label" for="rating3-3"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="4 stars" class="rating__label" for="rating3-4"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="5 stars" class="rating__label" for="rating3-5"><i class="rating__icon  fa fa-star"></i></label>
													</div>
												</div>
											</div>
											<a href="javascript:void(0);" class="rating_num">May 4, 2020</a>
										</div>
									</div>
									<div class="review-text"> 
										<span>Start within 30 days, finish within 48 hours.</span>
									</div>
								</div>
							</div>
							<div class="review-wrap">
								<div class="profile-wrap">
									<img src="http://php.manageprojects.in/halagarage/assets/images/user-placeholder.jpg" class="img-fluid">
								</div>
								<div class="profile-content">
									<span class="user-name">Harish Paul</span>
									<div class="date-time">
										<div class="rating_wrap mt-5">
											<div class="rating">
												<div id="full-stars-example-two">
													<div class="rating-group">
														<label aria-label="1 star" class="rating__label" for="rating3-1"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="2 stars" class="rating__label" for="rating3-2"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="3 stars" class="rating__label" for="rating3-3"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="4 stars" class="rating__label" for="rating3-4"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
														<label aria-label="5 stars" class="rating__label" for="rating3-5"><i class="rating__icon  fa fa-star"></i></label>
													</div>
												</div>
											</div>
											<a href="javascript:void(0);" class="rating_num">May 4, 2020</a>
										</div>
									</div>
									<div class="review-text"> 
										<span>Start within 30 days, finish within 48 hours.</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
            </div>
        </div>
        <!--// Row End-->
    </div>
    <!--// Container End-->
</section>
<!--// About Story End-->
<?php include('include/footer.php')?>
<?php include('include/scripts.php')?>
<!-- Gallery Modal -->
<div id="galleryModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="display:inline-block;">
                <div class="img-wrap">
					<img src="http://php.manageprojects.in/halagarage/assets/images/gallery1.jpg" class="img-fluid w-100">
				</div>
            </div>
        </div>
    </div>
</div>
<script>
	var images = [
		'https://unsplash.it/1300/800?image=875',
		'https://unsplash.it/1300/800?image=874',
		'https://unsplash.it/1300/800?image=872',
		'https://unsplash.it/1300/800?image=868',
		'https://unsplash.it/1300/800?image=839',
		'https://unsplash.it/1300/800?image=838'
	];

	$(function() {

		$('#gallery1').imagesGrid({
			images: images
		});
		$('#gallery2').imagesGrid({
			images: images.slice(0, 5)
		});
		$('#gallery3').imagesGrid({
			images: images.slice(0, 4)
		});
		$('#gallery4').imagesGrid({
			images: images.slice(0, 3)
		});
		$('#gallery5').imagesGrid({
			images: images.slice(0, 2)
		});
		$('#gallery6').imagesGrid({
			images: images.slice(0, 1)
		});
		$('#gallery7').imagesGrid({
			images: [
				'https://unsplash.it/660/440?image=875',
		'https://unsplash.it/660/990?image=874',
		'https://unsplash.it/660/440?image=872',
		'https://unsplash.it/750/500?image=868',
		'https://unsplash.it/660/990?image=839',
		'https://unsplash.it/660/455?image=838'
			],
			align: true,
			getViewAllText: function(imgsCount) { return 'View all' }
		});

	});
</script>