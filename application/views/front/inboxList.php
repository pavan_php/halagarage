<?php
    if($message_list)
    {
        foreach($message_list as $res)
        {
            ?>
                <div class="mail-item mailInbox" id="remove_<?= $res->id; ?>">
                    <div class="mail-item-heading collapsed" data-toggle="collapse" role="navigation" data-target="#mailCollapseFive" aria-expanded="false">
                        <div class="mail-item-inner">
                            <div class="d-flex">
                                <div class="f-body">
                                    <div class="meta-mail-time">
                                        <p class="user-email"><?= $res->title; ?></p>
                                    </div>
                                    <div class="meta-title-tag">
                                        <p data-toggle="modal" data-target="#exampleModal" onClick="msgDetails('<?= $res->id; ?>');" id="msg<?= $res->id ?>" class="mail-content-excerpt"><?= $res->message; ?></p>
                                        <p class="meta-time align-self-center"><?= date('H:i a', strtotime($res->createddate)); ?></p>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <!--<button type="button" class="btn btn-success br-0 mr-3" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>-->
                                        <button type="button" class="btn btn-warning br-0 mr-3" data-target="#replyModal" onClick="replymessage(<?= $res->id?>,<?= $res->token_id?>)" data-toggle="modal" title="Reply"><i class="fa fa-retweet" aria-hidden="true"></i></button>
                                        <button type="button" class="btn btn-danger br-0 mr-10" data-toggle="tooltip" onClick="removemsg(<?= $res->id ?>)" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
        }
    }
    else
    {
        ?>
            <div class="mail-item mailInbox">
                <p> No message found </p>
            </div>
        <?php
    }
?>