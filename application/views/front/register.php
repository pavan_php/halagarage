<?php include('include/header.php')?>
<!-- preloader area end -->
<div class="sign sign-area padding-120">
    <div class="container">
        <div class="row">
            <div class="offset-lg-4 col-lg-4">
                <div class="logo-wrapper text-center mb-40">
                    <a href="<?= base_url(); ?>">
                        <img src="<?= base_url() ?>assets/themes/immersive/images/logo/160x69-logo.jpg" alt="Logo" style="width:50%;">
                    </a>
                </div>
                <div class="sign-in-area">
                    <h5 class="text-center"><b>SIGNUP</b></h5>
                    <br>
                    <!--Form-->
                    <form action="#">
                        <div class="input-group mb-20">
                            <input type="text" class="form-control" id="name" placeholder="Enter Your Name">
                        </div>
                        <div class="input-group mb-20">
                            <input type="email" class="form-control" id="email" placeholder="Enter Your Email">
                        </div>
                        <div class="input-group mb-20">
                            <input type="password" class="form-control" id="password" placeholder="Enter Your Password">
                        </div>
                        <div class="input-group mb-20">
                            <input type="password" class="form-control" id="confirm_password" placeholder="Confirm Password">
                        </div>
                        <div class="input-group mb-20">
                            <div class="form-check">
                                <label class="form-check-label" for="check1">
                                    <input type="checkbox" class="form-check-input" id="check1" name="option1">By signing up, you agree to the <a href="javascript:void(0);"><b> Terms of Service</a></b>
                                </label>
                            </div>
                        </div>
                        <div class="main-btn-wrap text-center">
                            <input type="submit" class="main-btn uppercase" value="REGISTER">
                        </div>
                        <br>
                        <div class="text-center">
                            <a href="<?= base_url('login'); ?>">Already have an account? <b>Signin</b></a>
                        </div>

                    </form>
                    <!--// Form-->
                </div>
                <!--// Sign In Area-->
            </div>
        </div>
    </div>
</div>
<?php include('include/scripts.php')?>