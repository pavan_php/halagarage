<?php include('include/header.php')?>
<?php include('include/navbar-home.php')?>
<style>
    .post-tags ul li {
        display: inline-block;
        background: transparent;
        padding: 15px 17px;
        border: 1px solid #ffffff;
        border-radius: 29px;
        margin-bottom: 8px;
        -webkit-transition: all 0.3s ease 0s;
        -moz-transition: all 0.3s ease 0s;
        -o-transition: all 0.3s ease 0s;
        transition: all 0.3s ease 0s;
        margin: 0 10px 20px 0;
        height: 58px;
    }
    .post-tags ul li a{
        color:#ffffff;
    }
    .garage-wrap .section-title .title.both-line::before, .garage-wrap .section-title .title.both-line::after{
        background:#ffffff!important;
    }
</style>
<section class="our-service padding-top-100 padding-bottom-50">
    <!--Container-->
    <div class="container">
        <!--Row-->
        <div class="row">
            <div class="col-lg-6 m-auto">
                <!-- Section Title Wrap -->
                <div class="section-title-wrap text-center">
                    <!--Section Title-->
                    <div class="section-title">
                        <div class="padding-bottom-15">
                            <h6 class="title both-line uppercase">Our Services</h6>
                        </div>
                        <h2 class="heading-02">Services We Offers</h2>
                    </div>
                    <!--// Section Title-->
                </div>
                <!--// Section Title Wrapper End-->
            </div>
        </div>
        <!--// Row-->

        <!--Row-->
        <div class="row">
            <!--Service Items Wrap-->
            <div class="service-items-wrap">
                <!--Items-->
                <div class="service-items-wrap__items">
                    <div class="overlay-img left" style="background-image: url('assets/themes/immersive/images/services/bg/car_ac_repair_bg.png');">
                        <a href="#" class="service-btn">Service Details</a>
                    </div>
                    <div class="items__content">
                        <!--Items Content Wrap-->
                        <div class="items-content-wrap text-center">
                            <div class="items-icon">
                                <img src="<?= base_url() ?>assets/themes/immersive/images/services/caracrepair.svg" class="img-fluid w-100px">
                            </div>
                            <div class="padding-top-10 padding-bottom-20">
                                <h4 class="heading-04">AC Services</h4>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed eiusmod tempor</p>
                        </div>
                        <!--// Items Content Wrap-->
                    </div>
                </div>
                <!--// Items-->

                <!--Items-->
                <div class="service-items-wrap__items">
                    <div class="overlay-img top" style="background-image: url('assets/themes/immersive/images/services/bg/body_part_repairing_bg.png');">
                        <a href="#" class="service-btn">Service Details</a>
                    </div>
                    <div class="items__content">
                        <!--Items Content Wrap-->
                        <div class="items-content-wrap text-center">
                            <div class="items-icon">
                                <img src="<?= base_url() ?>assets/themes/immersive/images/services/bodypartsrepair.svg" class="img-fluid">
                            </div>
                            <div class="padding-top-10 padding-bottom-20">
                                <h4 class="heading-04">Body Parts Repairing</h4>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed eiusmod tempor</p>
                        </div>
                    </div>
                </div>
                <!--// Items-->

                <!--Items-->
                <div class="service-items-wrap__items">
                    <div class="overlay-img right" style="background-image: url('assets/themes/immersive/images/services/bg/washing_bg.png');">
                        <a href="#" class="service-btn">Service Details</a>
                    </div>
                    <div class="items__content">
                        <!--Items Content Wrap-->
                        <div class="items-content-wrap text-center">
                            <div class="items-icon">
                                <img src="<?= base_url() ?>assets/themes/immersive/images/services/cleaningwashing.svg" class="img-fluid">
                            </div>
                            <div class="padding-top-10 padding-bottom-20">
                                <h4 class="heading-04">Cleaning & Washing Services</h4>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed eiusmod tempor</p>
                        </div>
                        <!--// Items Content Wrap-->
                    </div>
                </div>
                <!--// Items-->

                <!--Items-->
                <div class="service-items-wrap__items">
                    <div class="overlay-img left" style="background-image: url('assets/themes/immersive/images/services/bg/wheel_bg.png');">
                        <a href="#" class="service-btn">Service Details</a>
                    </div>
                    <div class="items__content">
                        <!--Items Content Wrap-->
                        <div class="items-content-wrap text-center">
                            <div class="items-icon">
                                <img src="<?= base_url() ?>assets/themes/immersive/images/services/wheel.svg" class="img-fluid">
                            </div>
                            <div class="padding-top-10 padding-bottom-20">
                                <h4 class="heading-04">Tyre Replacement & Services</h4>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed eiusmod tempor</p>
                        </div>
                        <!--// Items Content Wrap-->
                    </div>
                </div>
                <!--// Items-->

                <!--Items-->
                <div class="service-items-wrap__items">
                    <div class="overlay-img bottom" style="background-image: url('assets/themes/immersive/images/services/bg/painting_bg.png');">
                        <a href="#" class="service-btn">Service Details</a>
                    </div>
                    <div class="items__content">
                        <!--Items Content Wrap-->
                        <div class="items-content-wrap text-center">
                            <div class="items-icon">
                                <img src="<?= base_url() ?>assets/themes/immersive/images/services/painting.svg" class="img-fluid">
                            </div>
                            <div class="padding-top-10 padding-bottom-20">
                                <h4 class="heading-04">Painting Services</h4>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed eiusmod tempor</p>
                        </div>
                        <!--// Items Content Wrap-->
                    </div>
                </div>
                <!--// Items-->

                <!--Items-->
                <div class="service-items-wrap__items">
                    <div class="overlay-img right" style="background-image: url('assets/themes/immersive/images/services/bg/electrical_bg.png');">
                        <a href="#" class="service-btn">Service Details</a>
                    </div>
                    <div class="items__content">
                        <!--Items Content Wrap-->
                        <div class="items-content-wrap text-center">
                            <div class="items-icon">
                                <img src="<?= base_url() ?>assets/themes/immersive/images/services/electrical.svg" class="img-fluid">
                            </div>
                            <div class="padding-top-10 padding-bottom-20">
                                <h4 class="heading-04">Electrical Services</h4>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed eiusmod tempor</p>
                        </div>
                        <!--// Items Content Wrap-->
                    </div>
                </div>
                <!--// Items-->
            </div>
            <!--// Service Items Wrap-->
        </div>
        <!--// Row-->
    </div>
    <!--// Container-->
</section>

<section class="our-process-section padding-top-50 padding-bottom-50 style-02 showcontent">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="service-section-right">
                    <div class="section-title text-center">
                        <div class="padding-bottom-15 wow fadeInUp" data-wow-duration="2s" data-wow-delay="5s">
                            <h6 class="title uppercase both-line"> How We Are </h6>
                        </div>
                        <div class="padding-bottom-30">
                            <h2 class="heading-02"> How Halagarage Works </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-bottom-40">
            <div class="offset-lg-2 col-lg-4 col-md-6">
                <!-- Work Items-->
                <div class="our-process-section__work-items d-flex">
                    <div class="item-center w-25 mr-20">
                        <img src="<?= base_url(); ?>assets/themes/immersive/images/how-we-work/perfect-car-service-1.png" class="img-fluid w-100">
                    </div>
                    <div class="work-tiems-inner w-500px">
                        <div class="padding-bottom-20">
                            <h5 class="items-title">Select The Perfect Car Service</h5>
                        </div>
                        <p>From Halagarage's broad portfolio of services</p>
                    </div>
                </div>
                <!--// Work Items-->
            </div>
            <div class="col-lg-4 col-md-6">
                <!-- Work Items-->
                <div class="our-process-section__work-items d-flex">
                    <div class="item-center w-25 mr-20">
                        <img src="<?= base_url() ?>assets/themes/immersive/images/how-we-work/schedule-pickup-1.png" class="img-fluid w-100">
                    </div>
                    <div class="work-tiems-inner w-500px">
                        <div class="padding-bottom-20">
                            <h5 class="items-title">Schedule Free Doorstep Pick-up</h5>
                        </div>
                        <p>We offer free pick up and drop for all services booked</p>
                    </div>
                </div>
                <!--// Work Items-->
            </div>
        </div>
        <!--// Row End-->
        <!--// Row End-->
        <div class="row">
            <div class="offset-lg-2 col-lg-4 col-md-6">
                <!-- Work Items-->
                <div class="our-process-section__work-items d-flex">
                    <div class="item-center w-25 mr-20">
                        <img src="<?= base_url() ?>assets/themes/immersive/images/how-we-work/real-time-car-service-1.png" class="img-fluid w-100">
                    </div>
                    <div class="work-tiems-inner w-500px">
                        <div class="padding-bottom-20">
                            <h5 class="items-title">Track Your Car Service Real-Time</h5>
                        </div>
                        <p>We will take care of everything from here!</p>
                    </div>
                </div>
                <!--// Work Items-->
            </div>
            <div class="col-lg-4 col-md-6">
                <!-- Work Items-->
                <div class="our-process-section__work-items d-flex">
                    <div class="item-center w-25 mr-20">
                        <img src="<?= base_url() ?>assets/themes/immersive/images/how-we-work/earn-while-we-service-1.png" class="img-fluid w-100">
                    </div>
                    <div class="work-tiems-inner w-500px">
                        <div class="padding-bottom-20">
                            <h5 class="items-title">Earn While We Service</h5>
                        </div>
                        <p>Spread the word! You get Rs.500. Your friends get Rs.500!</p>
                    </div>
                </div>
                <!--// Work Items-->
            </div>
        </div>
        <!--// Row End-->
    </div>
    <!--// Container-->
</section>

<!--Our Process Section-->
<section class="our-process-section padding-top-50 padding-bottom-50 style-02 showcontent" style="background-image: url(assets/themes/immersive/assets/img/bg/our-process-bg.png)">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title-wrap text-center">
                    <!--Section Title-->
                    <div class="section-title">
                        <h6 class="title uppercase both-line"> Our Services </h6>
                        <div class="padding-15">
                            <h2 class="heading-02">Benefits</h2>
                        </div>
                    </div>
                    <!--// Section Title-->
                </div>
            </div>
        </div>
        <!--// Row End-->
        <div class="row margin-bottom-40">
            <div class="offset-lg-2 col-lg-4 col-md-6">
                <!-- Work Items-->
                <div class="our-process-section__work-items d-flex">
                    <div class="item-center w-25 mr-20">
                        <img src="<?= base_url() ?>assets/themes/immersive/images/benefits/free-pickup-and-drop.png" class="img-fluid w-100">
                    </div>
                    <div class="work-tiems-inner w-500px">
                        <div class="padding-bottom-20">
                            <h5 class="items-title">Free Doorstep Pick-up and Drop</h5>
                        </div>
                        <p>No more unnecessary workshop visits!</p>
                    </div>
                </div>
                <!--// Work Items-->
            </div>
            <div class="col-lg-4 col-md-6">
                <!-- Work Items-->
                <div class="our-process-section__work-items d-flex">
                    <div class="item-center w-25 mr-20">
                        <img src="<?= base_url() ?>assets/themes/immersive/images/benefits/price.png" class="img-fluid w-100">
                    </div>
                    <div class="work-tiems-inner w-500px">
                        <div class="padding-bottom-20">
                            <h5 class="items-title">Upfront & Competitive Pricing</h5>
                        </div>
                        <p>Save Upto 40% on your car’s service</p>
                    </div>
                </div>
                <!--// Work Items-->
            </div>
        </div>
        <!--// Row End-->
        <!--// Row End-->
        <div class="row">
            <div class="offset-lg-2 col-lg-4 col-md-6">
                <!-- Work Items-->
                <div class="our-process-section__work-items d-flex">
                    <div class="item-center w-25 mr-20">
                        <img src="<?= base_url() ?>assets/themes/immersive/images/benefits/service.png" class="img-fluid w-100">
                    </div>
                    <div class="work-tiems-inner w-500px">
                        <div class="padding-bottom-20">
                            <h5 class="items-title">Network Warranty on Car Service</h5>
                        </div>
                        <p>1 Month/1000kms unconditional warranty on car service. No questions asked!</p>
                    </div>
                </div>
                <!--// Work Items-->
            </div>
            <div class="col-lg-4 col-md-6">
                <!-- Work Items-->
                <div class="our-process-section__work-items d-flex">
                    <div class="item-center w-25 mr-20">
                        <img src="<?= base_url() ?>assets/themes/immersive/images/benefits/genuine-parts.png" class="img-fluid w-100">
                    </div>
                    <div class="work-tiems-inner w-500px">
                        <div class="padding-bottom-20">
                            <h5 class="items-title">100% Genuine Spare Parts</h5>
                        </div>
                        <p>Only OEM/OES spare parts used. Quality Assured!</p>
                    </div>
                </div>
                <!--// Work Items-->
            </div>
        </div>
        <!--// Row End-->
    </div>
    <!--// Container-->
</section>
<!--Our Process Section End-->

<section class="home-testimonial-section padding-top-50 padding-bottom-120 showcontent">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <!--Section Title-->
                <div class="section-title">
                    <h6 class="title both-line uppercase gray">Garages</h6>
                    <div class="padding-15">
                        <h2 class="heading-02">Featured Garages</h2>
                    </div>
                </div>
                <!--// Section Title-->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 px-0">
                <div class="owl-carousel">
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>Powertech Auto Services</h4>
                    </div>
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>Wheelocity Garage</h4>
                    </div>
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>Max Garage</h4>
                    </div>
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>Motorworks Auto Garage</h4>
                    </div>
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>Radiant Car Workshop</h4>
                    </div>
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>Axis Auto Garage</h4>
                    </div>
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>MIK Garage</h4>
                    </div>
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>Zamani Garage</h4>
                    </div>
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>Leo’s Garage</h4>
                    </div>
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>Al Barsha Garage</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="home-testimonial-section padding-top-50 padding-bottom-120 showcontent garage-wrap" style="background: url(http://php.manageprojects.in/halagarage/assets/themes/immersive/images/garage-bg.png) no-repeat center center/cover;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <!--Section Title-->
                <div class="section-title">
                    <h6 class="title both-line uppercase text-white">Garages</h6>
                    <div class="padding-15">
                        <h2 class="heading-02 text-white">Featured Garages</h2>
                    </div>
                </div>
                <!--// Section Title-->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 px-0">
                <div class="overlay overlay-bg"></div>
                <div class="container">
                    <div class="single-info row">
                        <div class="col-lg-10 col-md-12 text-center no-padding margin-auto">
                            <div class="post-tags clr">
                                <ul>
                                    <li>
                                        <a href="javascript:void(0);">Powertech Auto Services</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Wheelocity Garage</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Max Garage</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Motorworks Auto Garage</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Radiant Car Workshop</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Axis Auto Garage</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">MIK Garage</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Zamani Garage</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Leo’s Garage</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Al Barsha Garage</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">WRG Garage</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">ZNZ Garage EST</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="home-testimonial-section padding-top-50 padding-bottom-120 showcontent">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <!--Section Title-->
                <div class="section-title">
                    <h6 class="title both-line uppercase">Our client say</h6>
                    <div class="padding-15">
                        <h2 class="heading-02">Ratings & Reviews</h2>
                    </div>
                </div>
                <!--// Section Title-->
            </div>
        </div>
        <div class="row">
            <div class="offset-lg-2 col-lg-8 col-md-12 px-0">
                <!--Testimonial Inner-->
                <div class="testimonial-inner style-02  padding-top-95">
                    <div class="testimonial-left">
                        <div class="testimonial-assets/themes/immersive/images slick-initialized slick-slider"><div class="slick-list draggable"><div class="slick-track" style="opacity: 1; width: 780px;"><div class="slick-slide" data-slick-index="0" aria-hidden="true" style="width: 260px; position: relative; left: 0px; top: 0px; z-index: 998; opacity: 0; transition: opacity 1000ms ease 0s;" tabindex="-1"><div><div class="items" style="width: 100%; display: inline-block;"><img src="<?= base_url() ?>assets/themes/immersive/images/01.png" alt="img"> </div></div></div><div class="slick-slide" data-slick-index="1" aria-hidden="true" style="width: 260px; position: relative; left: -260px; top: 0px; z-index: 998; opacity: 0; transition: opacity 1000ms ease 0s;"><div><div class="items" style="width: 100%; display: inline-block;"><img src="<?= base_url() ?>assets/themes/immersive/images/01.png" alt="img"> </div></div></div><div class="slick-slide slick-current slick-active" data-slick-index="2" aria-hidden="false" style="width: 260px; position: relative; left: -520px; top: 0px; z-index: 1000; opacity: 1; transition: opacity 1000ms ease 0s;" tabindex="-1"><div><div class="items" style="width: 100%; display: inline-block;"><img src="<?= base_url() ?>assets/themes/immersive/images/01.png" alt="img"> </div></div></div></div></div></div>
                        <!--// Testimonial assets/themes/immersive/images Slider Active Class-->
                    </div>
                    <!--// Testimonial Left End-->

                    <div class="testimonial-right">
                        <div class="quote-icon">
                            <i class="flaticon-quote"></i>
                        </div>
                        <!--// Quote Icon-->
                        <div class="testimonial-content-wrap">
                            <div class="testimonial-content slick-initialized slick-slider slick-dotted"><div class="slick-list draggable"><div class="slick-track" style="opacity: 1; width: 5530px; transform: translate3d(-2370px, 0px, 0px); transition: transform 1000ms ease 0s;"><div class="slick-slide slick-cloned" data-slick-index="-1" aria-hidden="true" style="width: 790px;" tabindex="-1"><div><div class="items" style="width: 100%; display: inline-block;">
                                    <p>Here are many variations of passages of Lorem Ipsum available,
                                        butinjected of the humour, or randomised words which don't look even
                                        slightly in the rom believable. If you are going to use a passage of
                                        Lorem Ipsum, you Here are many variations of passages of Lorem Ipsum
                                        available, butinjected of the humour, or randomised word</p>
                                    <h5 class="honor-name">Martin Alexa</h5>
                                    <div class="honor-disignation">Designer</div>
                                </div></div></div><div class="slick-slide" data-slick-index="0" aria-hidden="true" style="width: 790px;" role="tabpanel" id="slick-slide10" aria-describedby="slick-slide-control10" tabindex="-1"><div><div class="items" style="width: 100%; display: inline-block;">
                                    <p>Here are many variations of passages of Lorem Ipsum available,
                                        butinjected of the humour, or randomised words which don't look even
                                        slightly in the rom believable. If you are going to use a passage of
                                        Lorem Ipsum, you Here are many variations of passages of Lorem Ipsum
                                        available, butinjected of the humour, or randomised word</p>
                                    <h5 class="honor-name">Martin Alexa</h5>
                                    <div class="honor-disignation">Designer</div>
                                </div></div></div><div class="slick-slide" data-slick-index="1" aria-hidden="true" style="width: 790px;" role="tabpanel" id="slick-slide11" aria-describedby="slick-slide-control11"><div><div class="items" style="width: 100%; display: inline-block;">
                                    <p>Here are many variations of passages of Lorem Ipsum available,
                                        butinjected of the humour, or randomised words which don't look even
                                        slightly in the rom believable. If you are going to use a passage of
                                        Lorem Ipsum, you Here are many variations of passages of Lorem Ipsum
                                        available, butinjected of the humour, or randomised word</p>
                                    <h5 class="honor-name">Martin Alexa</h5>
                                    <div class="honor-disignation">Designer</div>
                                </div></div></div><div class="slick-slide slick-current slick-active" data-slick-index="2" aria-hidden="false" style="width: 790px;" role="tabpanel" id="slick-slide12" aria-describedby="slick-slide-control12" tabindex="-1"><div><div class="items" style="width: 100%; display: inline-block;">
                                    <p>Here are many variations of passages of Lorem Ipsum available,
                                        butinjected of the humour, or randomised words which don't look even
                                        slightly in the rom believable. If you are going to use a passage of
                                        Lorem Ipsum, you Here are many variations of passages of Lorem Ipsum
                                        available, butinjected of the humour, or randomised word</p>
                                    <h5 class="honor-name">Martin Alexa</h5>
                                    <div class="honor-disignation">Designer</div>
                                </div></div></div><div class="slick-slide slick-cloned" data-slick-index="3" aria-hidden="true" style="width: 790px;" tabindex="-1"><div><div class="items" style="width: 100%; display: inline-block;">
                                    <p>Here are many variations of passages of Lorem Ipsum available,
                                        butinjected of the humour, or randomised words which don't look even
                                        slightly in the rom believable. If you are going to use a passage of
                                        Lorem Ipsum, you Here are many variations of passages of Lorem Ipsum
                                        available, butinjected of the humour, or randomised word</p>
                                    <h5 class="honor-name">Martin Alexa</h5>
                                    <div class="honor-disignation">Designer</div>
                                </div></div></div><div class="slick-slide slick-cloned" data-slick-index="4" aria-hidden="true" style="width: 790px;" tabindex="-1"><div><div class="items" style="width: 100%; display: inline-block;">
                                    <p>Here are many variations of passages of Lorem Ipsum available,
                                        butinjected of the humour, or randomised words which don't look even
                                        slightly in the rom believable. If you are going to use a passage of
                                        Lorem Ipsum, you Here are many variations of passages of Lorem Ipsum
                                        available, butinjected of the humour, or randomised word</p>
                                    <h5 class="honor-name">Martin Alexa</h5>
                                    <div class="honor-disignation">Designer</div>
                                </div></div></div><div class="slick-slide slick-cloned" data-slick-index="5" aria-hidden="true" style="width: 790px;" tabindex="-1"><div><div class="items" style="width: 100%; display: inline-block;">
                                    <p>Here are many variations of passages of Lorem Ipsum available,
                                        butinjected of the humour, or randomised words which don't look even
                                        slightly in the rom believable. If you are going to use a passage of
                                        Lorem Ipsum, you Here are many variations of passages of Lorem Ipsum
                                        available, butinjected of the humour, or randomised word</p>
                                    <h5 class="honor-name">Martin Alexa</h5>
                                    <div class="honor-disignation">Designer</div>
                                </div></div></div></div></div></div>
                            <!--// Testimonial Content Slider Active Class-->

                            <div class="testimonial-arrow-02"><div class="slick-prev slick-arrow" style=""> <i class="fa fa-long-arrow-left" aria-hidden="true"></i> </div><div class="slick-next slick-arrow" style=""> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> </div></div>
                            <div class="testimonial-dots-02"><ul class="slick-dots" role="tablist" style=""><li class="" role="presentation"><button type="button" role="tab" id="slick-slide-control10" aria-controls="slick-slide10" aria-label="1 of 3" tabindex="-1">1</button></li><li role="presentation" class=""><button type="button" role="tab" id="slick-slide-control11" aria-controls="slick-slide11" aria-label="2 of 3" tabindex="0" aria-selected="true">2</button></li><li role="presentation" class="slick-active"><button type="button" role="tab" id="slick-slide-control12" aria-controls="slick-slide12" aria-label="3 of 3" tabindex="-1">3</button></li></ul></div>
                        </div>
                        <!--// Testimonial Content Wrap-->
                    </div>
                    <!--// Testimonial Right End-->
                </div>
                <!--// Testimonial Inner End-->
            </div>
        </div>
        <!--// Row-->
    </div>
    <!--// Container-->
</section>
<?php include('include/footer.php')?>
<script>
    $(function(){
        $('.showcontent').css('display','block');
        $('.showcontent').css('transform','');
    });
    $(document).ready(function(){
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            autoPlay: 1000,
            navText : ["<i class='fa fa-chevron-left' style='position: absolute;top: -70px;background-color: #3f8ddb;padding: 10px 12px;right: 35px;font-weight: 100;border-radius: 50%;font-size: 12px;color:#ffffff;'></i>","<i class='fa fa-chevron-right'  style='position: absolute;top: -70px;background-color: #3f8ddb;padding: 10px 12px;right: 0;font-weight: 100;border-radius: 50%;font-size: 12px;color:#ffffff;'></i>"],
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                1000:{
                    items:6
                }
            }
        })
    });
</script>
<?php include('include/scripts.php')?>




        
    