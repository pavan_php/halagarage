<?php
    if($product_list)
    {
        foreach($product_list as $rrr)
        {
            ?>
                <div class="row">
                    <div class="col-md-9">
                        <a href="<?= base_url('customers/productDetails/'.$rrr->id); ?>" style="display:block;">
                            <div class="product-listing-wrapper">
                                <div class="product-list">
                                    <div class="image-holder">
                                        <?php
                                            $src = '#';
                                            $attachment_key = $this->db->get_where(db_prefix().'files', array('rel_id' => $rrr->id, 'rel_type' => "product"))->row('file_name');
                                            if($attachment_key != '')
                                            {
                                                $src = base_url('uploads/product/'.$rrr->id.'/'.$attachment_key); 
                                            }
                                        ?>
                                        <img src="<?= $src; ?>" alt="<?= $attachment_key; ?>" class="img-fluid">
                                    </div>
                                    <div class="product-content">
                                        <h5 class="title"><b><?= $rrr->product_name; ?></b></h5>
                                        <span class="description"><?= $rrr->description; ?></span><br>
                                        <span class="price"><?= CURRENCY_NAME.' '.$rrr->price_with_fitting; ?></span>
                                    </div>
                                </div>
                                <hr>
                                <div tabindex="0" class="see-all-products-details text-right">
                                    <span class="styledtext-sc"><a href="<?= base_url('customers/productDetails/'.$rrr->id); ?>">See All Products Details</a></span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a target="_blank" href="<?= base_url('customers/garageProfile/'.$rrr->garage_id); ?>" class="btn btn-outline-info btn-xl btn-block"><i class="fa fa-folder-o" aria-hidden="true"></i> Garage Details</a><br>
                        <a target="_blank" href="<?= base_url('customers/message/'.$rrr->garage_id); ?>" class="btn btn-outline-success btn-xl btn-block"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send Enquiry</a><br>
                        <a target="_blank" href="<?= base_url('customers/buyNow/'.$rrr->garage_id.'/'.$rrr->id);?>" class="btn btn-outline-warning btn-xl btn-block"><i class="fa fa-credit-card" aria-hidden="true"></i> Buy Now</a>
                    </div>
                </div><hr>
            <?php
        }
    }
    else
    {
        echo '<p class="dataTables_empty">No product found!</p>';
    }
?>