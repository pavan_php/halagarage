<?php include('include/header.php')?>
<?php include('include/navbar.php')?>
<!--Breadcrumb Start-->
<div class="breadcrumb-area" style="background-image: url('<?= base_url() ?>assets/themes/immersive/images/about/about-us-banner-bg.png')">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner text-center">
                    <h1 class="page-title">Profile</h1>
                    <ul class="page-list">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li>Profile</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Breadcrumb End-->
<!--About Story Start-->
<section class="car-part padding-top-120 padding-bottom-110">
    <div class="container">
        <div class="row">
            <div class="offset-md-3 col-md-6 profile">
                <div class="request-wrapper">
                    <h3><b>Profile Update</b></h3>
                    <hr>
                    <form id="profilefrm">
                        <div class="form-group">
                            <label for="name"><b>First Name</b></label>
                            <input type="text" class="form-control" onkeyup="errorremove('firstname')" value="<?= $profileResult->firstname; ?>" name="firstname" id="firstname">
                            <p class="text-danger firstname"></p>
                        </div>
                        <div class="form-group">
                            <label for="name"><b>Last Name</b></label>
                            <input type="text" class="form-control" onkeyup="errorremove('lastname')" value="<?= $profileResult->lastname; ?>" name="lastname" id="lastname">
                            <p class="text-danger lastname"></p>
                        </div>
                        <div class="form-group">
                            <label for="name"><b>Email</b></label>
                            <input type="email" class="form-control" readonly id="email" value="<?= $profileResult->email; ?>">
                        </div>
                        <div class="form-group">
                            <label for="name"><b>Old password</b> [<span class="text-info">If you want change password, then fill this below entry.</span>]</label>
                            <input type="password" class="form-control" maxlength="15" onkeyup="errorremove('oldpassword')" name="oldpassword" onchange="getpassword()" id="oldpassword" >
                            <p class="text-danger oldpassword"></p>
                        </div>
                        <div class="form-group">
                            <label for="name"><b>New password</b></label>
                            <input type="password" class="form-control" maxlength="15" onkeyup="errorremove('newpassword')" name="newpassword" id="newpassword">
                            <p class="text-danger newpassword"></p>
                        </div>
                        <div class="form-group">
                            <label for="name"><b>Confirm password</b></label>
                            <input type="password" class="form-control" maxlength="15" onkeyup="errorremove('confirmpassword')" name="confirmpassword" id="confirmpassword">
                            <p class="text-danger confirmpassword"></p>
                        </div>
                        <div class="form-group">
                            <button type="button" id="proupdate" onClick="profileUpdate()" class="btn btn-info btn-block">Update</button>
                        </div>
                    </form>	
                </div>
            </div>
        </div>
        <!--// Row End-->
    </div>
    <!--// Container End-->
</section>
<!--// About Story End-->
<?php include('include/footer.php')?>
<script>
    function profileUpdate()
    {
        var firstname = $('#firstname').val();
        var lastname  = $('#lastname').val();
        var oldpassword = $('#oldpassword').val();
        var newpassword = $('#newpassword').val();
        var confirmpassword = $('#confirmpassword').val();
        
        if(firstname != '' && lastname != '')
        {
            if(firstname.length < 3 || firstname.length > 15)
            {
                $('.firstname').text('Make sure the first name is between 3-15 characters long');
                return false;
            }
            else if(lastname.length < 3 || lastname.length > 15)
            {
                $('.lastname').text('Make sure the last name is between 3-15 characters long');
                return false;
            }
            else
            {
                var str = "firstname="+firstname+"&lastname="+lastname+"&oldpassword="+oldpassword+"&newpassword="+newpassword+"&confirmpassword="+confirmpassword+"&"+csrfData['token_name']+"="+csrfData['hash'];
                //console.log(str);
                if(oldpassword != '' || newpassword != '' || confirmpassword != '')
                {
                    if(oldpassword == '')
                    {
                        $('.oldpassword').text('Old password is required');
                        return false;
                    }
                    else if(oldpassword != '')
                    {
                        $sns = '';
                        if(newpassword != '' && confirmpassword != '')
                        {
                            if(newpassword != confirmpassword)
                            {
                                $('.confirmpassword').text('New password and confirm password must be same');
                                return false;
                            }
                            else
                            {
                                /*var str = "oldpassword="+oldpassword+"&"+csrfData['token_name']+"="+csrfData['hash'];*/
                        	    $.ajax({
                        	        url: '<?= site_url()?>customers/getpassword',
                        	        type: 'POST',
                        	        data: str,
                        	        //dataType: 'json',
                        	        cache: false,
                        	        success: function(resp){
                            	       if(resp)
                            	       {
                            	           $sns = 1;
                            	           $('#proupdate').show();
                            	           //return true;
                            	           $.ajax({
                                    	        url: '<?= site_url()?>customers/profileUpdate',
                                    	        type: 'POST',
                                    	        data: str,
                                    	        dataType: 'json',
                                    	        cache: false,
                                    	        success: function(resp){
                                    	            if(resp)
                                    	            {
                                    	                successmsg('Profile update successfully!');
                                    	                //$('#navbardrop').text(firstname+' '+lastname);
                                    	                $('.companyname').text(firstname+' '+lastname);
                                    	                $('#oldpassword').val('');
                                    	                $('#newpassword').val('');
                                    	                $('#confirmpassword').val('');
                                                        window.location.replace('<?= base_url() ?>');
                                    	            }
                                    	            else
                                    	            {
                                    	                $("#profilefrm")[0].reset();
                                    	                //errormsg('First name and last name are required');
                                    	                return false;
                                    	            }
                                    	        }
                                    	    });
                            	       }
                            	       else
                            	       {
                            	           $('#oldpassword').val('');
                            	           //$('#proupdate').hide();
                            	          // errormsg('Your old password does not match');
                            	           $('.oldpassword').text('Your old password does not match');
                            	           return false;
                            	       }
                        	        }
                        	    });
                            }
                            if($sns == 1)
                            {
                                
                            }
                        }
                        else
                        {
                            if(newpassword == '')
                            {
                                $('.newpassword').text('New password is required');
                                return false;
                            }   
                            if(confirmpassword == '')
                            {
                                $('.confirmpassword').text('Confirm password is required');
                                return false;
                            }
                        }
                    }
                    else
                    {
                        $.ajax({
                	        url: '<?= site_url()?>customers/profileUpdate',
                	        type: 'POST',
                	        data: str,
                	        dataType: 'json',
                	        cache: false,
                	        success: function(resp){
                	            if(resp)
                	            {
                	                successmsg('Profile update successfully!');
                	                //$('#navbardrop').text(firstname+' '+lastname);
                	                $('.companyname').text(firstname+' '+lastname);
                	            }
                	            else
                	            {
                	                $("#profilefrm")[0].reset();
                	                //errormsg('First name and last name are required');
                	                return false;
                	            }
                	        }
                	    });
                    }
                }
                else
                {
                    $.ajax({
            	        url: '<?= site_url()?>customers/profileUpdate',
            	        type: 'POST',
            	        data: str,
            	        dataType: 'json',
            	        cache: false,
            	        success: function(resp){
            	            if(resp)
            	            {
            	                successmsg('Profile update successfully!');
            	                //$('#navbardrop').text(firstname+' '+lastname);
            	                $('.companyname').text(firstname+' '+lastname);
            	            }
            	            else
            	            {
            	                $("#profilefrm")[0].reset();
            	                //errormsg('First name and last name are required');
            	                return false;
            	            }
            	        }
            	    });
                }
            }
        }
        else
        {
            if(firstname == '')
            {
                $('.firstname').text('First name is required');
            }
            if(lastname == '')
            {
                $('.lastname').text('Last name is required');
            }
            return false;
        }
    }
    
    /* getpassword */
    function getpassword()
    {
        var oldpassword = $('#oldpassword').val();
        if(oldpassword == '')
        {
            $('#proupdate').show();
            $('#newpassword').val('');
            $('#confirmpassword').val('');
            return true;
        }
        else
        {
            var str = "oldpassword="+oldpassword+"&"+csrfData['token_name']+"="+csrfData['hash'];
    	    $.ajax({
    	        url: '<?= site_url()?>customers/getpassword',
    	        type: 'POST',
    	        data: str,
    	        //dataType: 'json',
    	        cache: false,
    	        success: function(resp){
        	       if(resp)
        	       {
        	           $('#proupdate').show();
        	           return true;
        	       }
        	       else
        	       {
        	           $('#oldpassword').val('');
        	           //$('#proupdate').hide();
        	           errormsg('Your old password does not match');
        	           return false;
        	       }
    	        }
    	    });
        }
    }
</script>
<?php include('include/scripts.php')?>