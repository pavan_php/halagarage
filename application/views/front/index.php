    <?php include('include/header.php')?>
<?php include('include/navbar-home.php')?>
<style>
    .post-tags ul li {
        display: inline-block;
        background: transparent;
        padding: 10px 17px;
        border: 1px solid #ffffff;
        border-radius: 0px;
        margin-bottom: 8px;
        -webkit-transition: all 0.3s ease 0s;
        -moz-transition: all 0.3s ease 0s;
        -o-transition: all 0.3s ease 0s;
        transition: all 0.3s ease 0s;
        margin: 0 10px 20px 0;
    }
    .post-tags ul li a{
        color:#ffffff;
    }
    .garage-wrap .section-title .title.both-line::before, .garage-wrap .section-title .title.both-line::after{
        background:#ffffff!important;
    }
	.item-carousel .owl-nav{text-align:center!important;}
	.item-carousel .div-inner{text-align:center!important;}
	.item-carousel .div-inner .photo{border:none!important;}
	.item-carousel button{background-color:#3f8ddc;}
	.item-carousel-wrap{
		background-image: url('uploads/testimonial-bg.png');
		background-position:center center;
		background-size:cover;
		background-repeat:no-repeat;
	}
	.item-carousel .title{color:#3f8ddc;font-size:20px;font-weight:500;}
	.item-carousel .content{color:#181818;font-size:14px;font-weight:400;}
</style>


<section class="our-service padding-top-100 padding-bottom-50">
    <!--Container-->
    <div class="container">
        <!--Row-->
        <div class="row">
            <div class="col-lg-6 m-auto">
                <!-- Section Title Wrap -->
                <div class="section-title-wrap text-center">
                    <!--Section Title-->
                    <div class="section-title">
                        <div class="padding-bottom-15">
                            <h6 class="title both-line uppercase">Our Services</h6>
                        </div>
                        <h2 class="heading-02">Services We Offers</h2>
                    </div>
                    <!--// Section Title-->
                </div>
                <!--// Section Title Wrapper End-->
            </div>
        </div>
        <!--// Row-->

        <!--Row-->
        <div class="row">
            <!--Service Items Wrap-->
            <div class="service-items-wrap">
                <!--Items-->
                <div class="service-items-wrap__items">
                    <div class="overlay-img left" style="background-image: url('assets/themes/immersive/images/services/bg/car_ac_repair_bg.png');">
                        <a href="#" class="service-btn">Service Details</a>
                    </div>
                    <div class="items__content">
                        <!--Items Content Wrap-->
                        <div class="items-content-wrap text-center">
                            <div class="items-icon">
                                <img src="<?= base_url() ?>assets/themes/immersive/images/services/car-ac.svg" class="img-fluid w-100px">
                            </div>
                            <div class="padding-top-10 padding-bottom-20">
                                <h4 class="heading-04">AC Services</h4>
                            </div>
                            <p>HalaGarage.com is covering all your AC services you will need by introducing you to all specialized garages. We understand how finding the right AC services is not much easy. So, with HalaGarage.com, you can easily connect with professional AC services providers of your choice as per your convenience, where you can find all you need such as repairing or replacing Air filters, condensers, evaporator coils and fans, drainage cleaning, leakage check, Coolant level check in air conditioner, Overall inspection of the AC unit.</p>
                        </div>
                        <!--// Items Content Wrap-->
                    </div>
                </div>
                <!--// Items-->

                <!--Items-->
                <div class="service-items-wrap__items">
                    <div class="overlay-img top" style="background-image: url('assets/themes/immersive/images/services/bg/body_part_repairing_bg.png');">
                        <a href="#" class="service-btn">Service Details</a>
                    </div>
                    <div class="items__content">
                        <!--Items Content Wrap-->
                        <div class="items-content-wrap text-center">
                            <div class="items-icon">
                                <img src="<?= base_url() ?>assets/themes/immersive/images/services/car-repairing.svg" class="img-fluid">
                            </div>
                            <div class="padding-top-10 padding-bottom-20">
                                <h4 class="heading-04">Body Parts Repairing</h4>
                            </div>
                            <p>HalaGarage.com is covering all your Body Parts Repairing services you will need by introducing you to all specialized garages. We understand how finding the right Body Parts Repairing Services with the best price is not much easy. So, with HalaGarage.com, you can easily connect with professional Body Parts Repairing services providers of your choice as per your convenience, where we can help you to find the best garage which can Restores your vehicle after minor or major collisions. Repairs dents in the sheet metal. Restores paint and repaints to match factory colors. Replaces bumpers, damaged body panels, and other components that are not part of the engine.</p>
                        </div>
                    </div>
                </div>
                <!--// Items-->

                <!--Items-->
                <div class="service-items-wrap__items">
                    <div class="overlay-img right" style="background-image: url('assets/themes/immersive/images/services/bg/washing_bg.png');">
                        <a href="#" class="service-btn">Service Details</a>
                    </div>
                    <div class="items__content">
                        <!--Items Content Wrap-->
                        <div class="items-content-wrap text-center">
                            <div class="items-icon">
                                <img src="<?= base_url() ?>assets/themes/immersive/images/services/car-washing.svg" class="img-fluid">
                            </div>
                            <div class="padding-top-10 padding-bottom-20">
                                <h4 class="heading-04">Cleaning & Washing Services</h4>
                            </div>
                            <p>HalaGarage.com will offer you all the Cleaning and Washing Services across the UAE with full details which you may need, you can easily find the best service center near to your location, book online an appointment, see their prices before heading to them, check their ratings and reviews, save you time and take an advantage to not to wait in the row and pick your suitable time.</p>
                        </div>
                        <!--// Items Content Wrap-->
                    </div>
                </div>
                <!--// Items-->

                <!--Items-->
                <div class="service-items-wrap__items">
                    <div class="overlay-img left" style="background-image: url('assets/themes/immersive/images/services/bg/wheel_bg.png');">
                        <a href="#" class="service-btn">Service Details</a>
                    </div>
                    <div class="items__content">
                        <!--Items Content Wrap-->
                        <div class="items-content-wrap text-center">
                            <div class="items-icon">
                                <img src="<?= base_url() ?>assets/themes/immersive/images/services/car-wheel-replacement.svg" class="img-fluid">
                            </div>
                            <div class="padding-top-10 padding-bottom-20">
                                <h4 class="heading-04">Tyre Replacement & Services</h4>
                            </div>
                            <p>At HalaGarage.com we gathered all Tires providers and specialized garages to help you to find the best car tyres services for you within seconds by just click, with HalaGarage.com  you will just enter your location and your car model and then you will find all the nearest car tyres providers which provides the suitable tyres for your car, you can easily see their prices before heading to them, check their ratings and reviews, save you time and take an advantage to not to wait in the row and pick your suitable time.</p>
                        </div>
                        <!--// Items Content Wrap-->
                    </div>
                </div>
                <!--// Items-->

                <!--Items-->
                <div class="service-items-wrap__items">
                    <div class="overlay-img bottom" style="background-image: url('assets/themes/immersive/images/services/bg/painting_bg.png');">
                        <a href="#" class="service-btn">Service Details</a>
                    </div>
                    <div class="items__content">
                        <!--Items Content Wrap-->
                        <div class="items-content-wrap text-center">
                            <div class="items-icon">
                                <img src="<?= base_url() ?>assets/themes/immersive/images/services/car-painting.svg" class="img-fluid">
                            </div>
                            <div class="padding-top-10 padding-bottom-20">
                                <h4 class="heading-04">Painting Services</h4>
                            </div>
                            <p>HalaGarage.com will introduce you to all specialized painting services providers across the UAE. We understand how finding the best service with the best price is not much easy. So, with HalaGarage.com, you can easily connect with professional painting services providers of your choice as per your convenience, where you can find all you need such as full painting, detailing, polishing, customized paint services.</p>
                        </div>
                        <!--// Items Content Wrap-->
                    </div>
                </div>
                <!--// Items-->

                <!--Items-->
                <div class="service-items-wrap__items">
                    <div class="overlay-img right" style="background-image: url('assets/themes/immersive/images/services/bg/electrical_bg.png');">
                        <a href="#" class="service-btn">Service Details</a>
                    </div>
                    <div class="items__content">
                        <!--Items Content Wrap-->
                        <div class="items-content-wrap text-center">
                            <div class="items-icon">
                                <img src="<?= base_url() ?>assets/themes/immersive/images/services/car-electrical.svg" class="img-fluid">
                            </div>
                            <div class="padding-top-10 padding-bottom-20">
                                <h4 class="heading-04">Electrical Services</h4>
                            </div>
                            <p>We know that the most annoying problem in any car is the electrical defaults, At HalaGarage.com we will help you to know exactly the problem of your car from Electrical services providers and specialists across the UAE, you can easily contact with them by just a click and once you get all you may need to understand the problem of your car, you can easily get your car repaired by the best electrical services providers of your choice as per your convenience, where you can find all you need such as repairing or replacing your car battery, ignition system, starting and electrical switches.</p>
                        </div>
                        <!--// Items Content Wrap-->
                    </div>
                </div>
                <!--// Items-->
            </div>
            <!--// Service Items Wrap-->
        </div>
        <!--// Row-->
    </div>
    <!--// Container-->
</section>

<section class="h2-service-section padding-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="section-border-image margin-right white-gray">
                    <div class="border-design">
                        <div class="border-bg top"></div>
                        <div class="we-are-img">
                            <img src="assets/themes/immersive/images/about-us.png" alt="img">
                        </div>
                    </div>
                </div>
                <!--// Section Border Image-->
            </div>
            <div class="col-lg-6">
                <div class="service-section-right">
                    <div class="section-title">
                        <div class="padding-bottom-15">
                            <h6 class="title uppercase gray"> Who We Are </h6>
                        </div>
                        <div class="padding-bottom-30">
                            <h2 class="heading-02"> How Halagarage Works </h2>
                        </div>
                    </div>
                    <!--// Section title-->
                    <div class="service-section-bottom">
                        <ul class="items-wrap">
                            <li class="home-02-our-service-items color-01">
                                <div class="item-left">
                                    <div class="item-border">
                                        <div class="border-bg"></div>
                                        <div class="icon">
                                            <img src="assets/themes/immersive/images/how-we-work/car-service.svg" alt="img" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="item-right">
                                    <h5 class="title">Select The Perfect Car Service</h5>
                                    <p>From Halagarage's broad portfolio of services</p>
                                </div>
                            </li>
                            <!--// Service items-->
                            <li class="home-02-our-service-items color-02">
                                <div class="item-left">
                                    <div class="item-border">
                                        <div class="border-bg"></div>
                                        <div class="icon">
                                            <img src="assets/themes/immersive/images/how-we-work/car-schedule.svg" alt="img" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="item-right">
                                    <h5 class="title">Schedule Free Doorstep Pick-up</h5>
                                    <p>We offer free pick up and drop for all services booked</p>
                                </div>
                            </li>
                            <!--// Service items-->
                            <li class="home-02-our-service-items color-03">
                                <div class="item-left">
                                    <div class="item-border">
                                        <div class="border-bg"></div>
                                        <div class="icon">
                                            <img src="assets/themes/immersive/images/how-we-work/car.svg" alt="img" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="item-right">
                                    <h5 class="title">Track Your Car Service Real-Time</h5>
                                    <p>We will take care of everything from here!</p>
                                </div>
                            </li>
                            <!--// Service items-->
                            <li class="home-02-our-service-items color-04">
                                <div class="item-left">
                                    <div class="item-border">
                                        <div class="border-bg"></div>
                                        <div class="icon">
                                            <img src="assets/themes/immersive/images/how-we-work/money.svg" alt="img" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="item-right">
                                    <h5 class="title">Earn While We Service</h5>
                                    <p>Spread the word! You get Rs.500. Your friends get Rs.500!</p>
                                </div>
                            </li>
                            <!--// Service items-->
                        </ul>
                    </div>
                    <!--// Service Section bottom-->
                </div>
                <!--// Service Section Right-->
            </div>
        </div>
        <!--// Row-->
    </div>
    <!--// Container-->
</section>

<section class="how-we-are padding-top-40 padding-bottom-110">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!--Section Left Content-->
                <div class="common-section-content">
                    <!--Section Title-->
                    <div class="section-title">
                        <div class="padding-bottom-10">
                            <h6 class="title uppercase gray"> How we are </h6>
                        </div>
                        <h2 class="heading-02"> Welcome to Auto shop </h2>
                    </div>
                    <div class="how-we-are__offer-section">
                        <div class="how-we-are__offer-section__item">
                            <div class="icon-area">
                                <div class="icon">
                                    <img src="assets/img/icons/03.png" alt="img">
                                </div>
                            </div>
                            <h6 class="heading-06">Free Doorstep Pick-up and Drop</h6>
                            <p>No more unnecessary workshop visits!</p>
                        </div>
                        <div class="how-we-are__offer-section__item">
                            <div class="icon-area">
                                <div class="icon">
                                    <img src="assets/img/icons/04.png" alt="img">
                                </div>
                            </div>
                            <h6 class="heading-06">Upfront & Competitive Pricing</h6>
                            <p>Save Upto 40% on your car’s service</p>
                        </div>
                        <div class="how-we-are__offer-section__item">
                            <div class="icon-area">
                                <div class="icon">
                                    <img src="assets/img/icons/05.png" alt="img">
                                </div>
                            </div>
                            <h6 class="heading-06">Network Warranty on Car Service</h6>
                            <p>1 Month/1000kms unconditional warranty on car service. No questions asked!</p>
                        </div>
                        <div class="how-we-are__offer-section__item">
                            <div class="icon-area">
                                <div class="icon">
                                    <img src="assets/img/icons/03.png" alt="img">
                                </div>
                            </div>
                            <h6 class="heading-06">100% Genuine Spare Parts</h6>
                            <p>Only OEM/OES spare parts used. Quality Assured!</p>
                        </div>
                    </div>
                    <!--// Offer Section-->

                </div>
                <!--// Section Left Content End-->
            </div>
        </div>
        <!--// Row End-->
    </div>
    <!--// Container End-->
</section>

<section class="home-testimonial-section padding-top-50 padding-bottom-50 showcontent garage-wrap" style="background: url(<?= base_url() ?>/assets/themes/immersive/images/about/about-us-banner-bg.png) no-repeat center center/cover;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <!--Section Title-->
                <div class="section-title">
                    <h6 class="title both-line uppercase text-white">Garages</h6>
                    <div class="padding-15">
                        <h2 class="heading-02 text-white">Featured Garages</h2>
                    </div>
                </div>
                <!--// Section Title-->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 px-0">
                <div class="overlay overlay-bg"></div>
                <div class="container">
                    <div class="single-info row">
                        <div class="col-lg-10 col-md-12 text-center no-padding margin-auto">
                            <div class="post-tags clr">
                                <ul class="featuregaragelist">
                                    <?php
                                        if($activity_result)
                                        {
                                            foreach($activity_result as $rrr)
                                            {
                                                ?>
                                                    <li onClick="getFeaturedGarage(<?= $rrr->id ?>)">
                                                        <a href="javascript:void(0);"><?= $rrr->name; ?></a>
                                                    </li>
                                                <?php
                                            }
                                        }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Our Process Section-->
<section class="our-process-section padding-top-50 padding-bottom-50 style-02 showcontent" style="background-image: url(assets/themes/immersive/assets/img/bg/our-process-bg.png)">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title-wrap text-center">
                    <!--Section Title-->
                    <div class="section-title">
                        <h6 class="title uppercase both-line"> Our Services </h6>
                        <div class="padding-15">
                            <h2 class="heading-02">Benefits</h2>
                        </div>
                    </div>
                    <!--// Section Title-->
                </div>
            </div>
        </div>
        <!--// Row End-->
        <div class="row margin-bottom-40">
            <div class="offset-lg-2 col-lg-4 col-md-6">
                <!-- Work Items-->
                <div class="our-process-section__work-items d-flex">
                    <div class="item-center w-25 mr-20">
                        <img src="<?= base_url() ?>assets/themes/immersive/images/benefits/free-pickup-and-drop.png" class="img-fluid w-100">
                    </div>
                    <div class="work-tiems-inner w-500px">
                        <div class="padding-bottom-20">
                            <h5 class="items-title">Free Doorstep Pick-up and Drop</h5>
                        </div>
                        <p>No more unnecessary workshop visits!</p>
                    </div>
                </div>
                <!--// Work Items-->
            </div>
            <div class="col-lg-4 col-md-6">
                <!-- Work Items-->
                <div class="our-process-section__work-items d-flex">
                    <div class="item-center w-25 mr-20">
                        <img src="<?= base_url() ?>assets/themes/immersive/images/benefits/price.png" class="img-fluid w-100">
                    </div>
                    <div class="work-tiems-inner w-500px">
                        <div class="padding-bottom-20">
                            <h5 class="items-title">Upfront & Competitive Pricing</h5>
                        </div>
                        <p>Save Upto 40% on your car’s service</p>
                    </div>
                </div>
                <!--// Work Items-->
            </div>
        </div>
        <!--// Row End-->
        <!--// Row End-->
        <div class="row">
            <div class="offset-lg-2 col-lg-4 col-md-6">
                <!-- Work Items-->
                <div class="our-process-section__work-items d-flex">
                    <div class="item-center w-25 mr-20">
                        <img src="<?= base_url() ?>assets/themes/immersive/images/benefits/service.png" class="img-fluid w-100">
                    </div>
                    <div class="work-tiems-inner w-500px">
                        <div class="padding-bottom-20">
                            <h5 class="items-title">Network Warranty on Car Service</h5>
                        </div>
                        <p>1 Month/1000kms unconditional warranty on car service. No questions asked!</p>
                    </div>
                </div>
                <!--// Work Items-->
            </div>
            <div class="col-lg-4 col-md-6">
                <!-- Work Items-->
                <div class="our-process-section__work-items d-flex">
                    <div class="item-center w-25 mr-20">
                        <img src="<?= base_url() ?>assets/themes/immersive/images/benefits/genuine-parts.png" class="img-fluid w-100">
                    </div>
                    <div class="work-tiems-inner w-500px">
                        <div class="padding-bottom-20">
                            <h5 class="items-title">100% Genuine Spare Parts</h5>
                        </div>
                        <p>Only OEM/OES spare parts used. Quality Assured!</p>
                    </div>
                </div>
                <!--// Work Items-->
            </div>
        </div>
        <!--// Row End-->
    </div>
    <!--// Container-->
</section>
<!--Our Process Section End-->
<?php /* ?>
<section class="home-testimonial-section padding-top-50 padding-bottom-120 showcontent">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <!--Section Title-->
                <div class="section-title">
                    <h6 class="title both-line uppercase gray">Garages</h6>
                    <div class="padding-15">
                        <h2 class="heading-02">Featured Garages</h2>
                    </div>
                </div>
                <!--// Section Title-->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 px-0">
                <div class="owl-carousel">
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>Powertech Auto Services</h4>
                    </div>
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>Wheelocity Garage</h4>
                    </div>
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>Max Garage</h4>
                    </div>
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>Motorworks Auto Garage</h4>
                    </div>
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>Radiant Car Workshop</h4>
                    </div>
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>Axis Auto Garage</h4>
                    </div>
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>MIK Garage</h4>
                    </div>
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>Zamani Garage</h4>
                    </div>
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>Leo’s Garage</h4>
                    </div>
                    <div class="item text-center">
                        <img src = "<?= base_url() ?>assets/themes/immersive/images/garage-logo.png" style="border: 1px solid #CCD3DC;padding: 2px;margin-bottom:15px;" class="img-fluid">
                        <h4>Al Barsha Garage</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php */ ?>


<section class="home-testimonial-section item-carousel-wrap padding-top-50 padding-bottom-50 showcontent">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <!--Section Title-->
                <div class="section-title">
                    <h6 class="title both-line uppercase white">Our client say</h6>
                    <div class="padding-15">
                        <h2 class="heading-02 white">Ratings & Reviews</h2>
                    </div>
                </div>
                <!--// Section Title-->
            </div>
        </div>
        <div class="row">
            <div class="offset-lg-2 col-lg-8 col-md-12 px-0">
                <div id="owl-demo" class="owl-carousel item-carousel wow fadeInUp">
					<div class="item col-lg-12">
						<div class="div-inner" style="background-color:#fff;padding:20px;box-shadow:0 3px 16px #3f8ddc54;">
							<div class="photo" style="border:1px solid #eee; border-radius:50%;padding:10px;width:100px;height:100px;margin: 0 auto;">
								<img src="https://codingeek.net/html/coche/assets/img/home-05/avatar-03.png" alt="Owl Image">
							</div>
							<div class="content-body">
								<p class="content">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
								<h4 class="title">Mahfuz Riad</h4>
							</div>
						</div>
					</div>
					<div class="item col-lg-12">
						<div class="div-inner" style="background-color:#fff;padding:20px;box-shadow:0 3px 16px #3f8ddc54;">
							<div class="photo" style="border:1px solid #eee; border-radius:50%;padding:10px;width:100px;height:100px;margin: 0 auto;">
								<img src="https://codingeek.net/html/coche/assets/img/home-05/avatar-03.png" alt="Owl Image">
							</div>
							<div class="content-body">
								<p class="content">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
								<h4 class="title">Mahfuz Riad</h4>
							</div>
						</div>
					</div>
					<div class="item col-lg-12">
						<div class="div-inner" style="background-color:#fff;padding:20px;box-shadow:0 3px 16px #3f8ddc54;">
							<div class="photo" style="border:1px solid #eee; border-radius:50%;padding:10px;width:100px;height:100px;margin: 0 auto;">
								<img src="https://codingeek.net/html/coche/assets/img/home-05/avatar-03.png" alt="Owl Image">
							</div>
							<div class="content-body">
								<p class="content">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
								<h4 class="title">Mahfuz Riad</h4>
							</div>
						</div>
					</div>
					<div class="item col-lg-12">
						<div class="div-inner" style="background-color:#fff;padding:20px;box-shadow:0 3px 16px #3f8ddc54;">
							<div class="photo" style="border:1px solid #eee; border-radius:50%;padding:10px;width:100px;height:100px;margin: 0 auto;">
								<img src="https://codingeek.net/html/coche/assets/img/home-05/avatar-03.png" alt="Owl Image">
							</div>
							<div class="content-body">
								<p class="content">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
								<h4 class="title">Mahfuz Riad</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
        <!--// Row-->
    </div>
    <!--// Container-->
</section>
<?php include('include/footer.php')?>
<script>
    $(function(){
        $('.showcontent').css('display','block');
        $('.showcontent').css('transform','');
    });
    $(document).ready(function(){
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            autoPlay: 1000,
            dots:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        })
    });
    
    /* getFeaturedGarage */
    function getFeaturedGarage(id)
    {
        $('.featuregaragelist').html('<li>Loading..</li>');
        var str = "id="+id+"&"+csrfData['token_name']+"="+csrfData['hash'];
	    $.ajax({
	        url: '<?= site_url()?>garage/getFeaturedGarage',
	        type: 'POST',
	        data: str,
	        dataType: 'html',
	        cache: false,
	        success: function(resp){
	            if(resp)
	            {
	                $('.featuregaragelist').html(resp);
	            }
	        }
	    });
    }
	
	
</script>
<?php include('include/scripts.php')?>




        
    