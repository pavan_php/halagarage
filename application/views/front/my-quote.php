<?php include('include/header.php')?>
<?php include('include/navbar.php')?><!--Full Width Sider Start-->
<div class="breadcrumb-area" style="background-image: url('<?= site_url() ?>assets/themes/immersive/images/about/about-us-banner-bg.png')">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner text-center">
                    <h1 class="page-title">Quotes</h1>
                    <ul class="page-list">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li>Quotes</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Breadcrumb End-->
<!--About Story Start-->
<section class="car-part padding-top-120 padding-bottom-110">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="category-wrap">
                            <!--<div class="panel-body">
                                <!-<h4 class="no-margin section-text"></h4>--
                                <div class="header-wrap p-10 bg-white">
                                    <div class="d-flex align-items-center">
                                        <div>
                                            <h4 class="fw-700 mt-0">My quote</h4>
                                        </div>
                                        <div class="ml-auto align-items-center">
                                            <div class="dl">
                                                <form class="form-inline">
                                                    <div class="form-group mr-2 br-0">
                                                        <select class="form-control chosen-select" tabindex="-98">
                                                            <option>Select filter</option>
                                                            <option>Status</option>
                                                            <option>Quote date</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <button class="btn btn-secondary br-0">Reset</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            </div>-->
                            
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-justified">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#home">Active Request</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#menu1">Closed Requests</a>
                                </li>
                                <!--
                                <li class="nav-item">
                                    <a class="nav-link uppercase" data-toggle="tab" href="#menu2">Draft Requests</a>
                                </li>
                                -->
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane container active" id="home">
                                    <div class="active-wrap">
                                        <div class="table-responsive">
                                            <table id="myquotelist" class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Request Date</th>
                                                        <th class="service-td">Service</th>
                                                        <th>Quote</th>
                                                        <!--<th>Message</th>-->
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        if($quoteResult)
                                                        {
                                                            foreach($quoteResult as $rrr)
                                                            {
                                                                $garageReply = $this->db->get_where(db_prefix().'quoteRequest_garageReply', array('quote_id' => $rrr->id))->num_rows();
                                                                $quoteCount = $this->db->get_where(db_prefix().'quoteRequest_garageReply', array('quote_id' => $rrr->id, 'customer_view' => 0))->num_rows();
                                                                if($garageReply > 0)
                                                                {
                                                                    ?>
                                                                        <tr id="removequote<?= $rrr->id; ?>">
                                                                            <td><?= $rrr->id; ?></td>
                                                                            <!--<td><?= _d(date('d-M-Y', strtotime($rrr->created_date))); ?></td>-->
                                                                            <td><span style="display:none;"><?php echo strtotime($rrr->created_date); ?></span><?= date('d-M-Y', strtotime($rrr->created_date)); ?></td>
                                                                            <td class="service-td">
                                                                                <!--<a href="<?= base_url('customers/requestdetails/'.$rrr->id); ?>" class="text-black"><?= $rrr->title; ?>-->
                                                                                <a href="javascript:void(0);" data-toggle="modal" onclick="getPreviewReq(<?= $rrr->id; ?>)" data-target="#quoteDetailsModal" class="text-black"><?= substr($rrr->title,0,50).'...'; ?>
                                                                                <?php
                                                                                    if($quoteCount > 0)
                                                                                    {
                                                                                        echo '<button class="btn btn-sm text-white"><span class="badge navigationbadge">New</span></button>';
                                                                                    }
                                                                                ?>
                                                                                </a>
                                                                            </td>
                                                                            <td><a href="<?= base_url('customers/requestdetails/'.$rrr->id); ?>" class="text-black"><?= $garageReply; ?></a></td>
                                                                            <td>
                                                                                <button type="button" onClick="removeQuote(<?= $rrr->id ?>);" class="btn btn-close btn-sm"><i class="fa fa-close" aria-hidden="true"></i> Close</button>
                                                                                <button type="button" onClick="removeQuoteReq(<?= $rrr->id ?>);" class="btn btn-appologise btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> Remove</button>
                                                                            </td>
                                                                        </tr>
                                                                    <?php
                                                                }
                                                                else
                                                                {
                                                                    ?>
                                                                        <tr id="removequote<?= $rrr->id; ?>">
                                                                            <td><?= $rrr->id; ?></td>
                                                                            <!--<td><?= _d(date('d-M-Y', strtotime($rrr->created_date))); ?></td>-->
                                                                            <td><span style="display:none;"><?php echo strtotime($rrr->created_date); ?></span><?= date('d-M-Y', strtotime($rrr->created_date)); ?></td>
                                                                            <td class="service-td">
                                                                                <!--<a href="<?= base_url('customers/requestdetails/'.$rrr->id); ?>" class="text-black"><?= $rrr->title; ?>-->
                                                                                <a href="javascript:void(0);" data-toggle="modal" onclick="getPreviewReq(<?= $rrr->id; ?>)" data-target="#quoteDetailsModal" class="text-black"><?= substr($rrr->title,0,50).'...'; ?>
                                                                                <?php
                                                                                    if($quoteCount > 0)
                                                                                    {
                                                                                        echo '<button class="btn btn-sm text-white"><span class="badge navigationbadge">New</span></button>';
                                                                                    }
                                                                                ?>
                                                                                </a>
                                                                            </td>
                                                                            <td><a href="javascript:void(0);" class="text-black"><?= 0; ?></a></td>
                                                                            <td>
                                                                                <button type="button" onClick="removeQuote(<?= $rrr->id ?>);" class="btn btn-close btn-sm"><i class="fa fa-close" aria-hidden="true"></i> Close</button>
                                                                                <button type="button" onClick="removeQuoteReq(<?= $rrr->id ?>);" class="btn btn-appologise btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> Remove</button>
                                                                            </td>
                                                                        </tr>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            ?>
                                                                <tr>
                                                                    <td valign="top" colspan="5" class="dataTables_empty"  style="text-align:center!important;">No matching records found</td>
                                                                </tr>
                                                            <?php
                                                        }
                                                    ?>
                                                 </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane container fade" id="menu1">
                                    <div class="closed-wrap">
                                        <div class="table-responsive">
                                            <table id="example11" class="table table-striped commontable">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Request Date</th>
                                                        <th>Service</th>
                                                        <th>Quote</th>
                                                        <!--<th>Message</th>-->
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        if($quoteResult_close)
                                                        {
                                                            foreach($quoteResult_close as $rrr)
                                                            {
                                                                $garageReply = $this->db->get_where(db_prefix().'quoteRequest_garageReply', array('remove_id' => 2, 'quote_id' => $rrr->id))->num_rows();
                                                                if($garageReply > 0)
                                                                {
                                                                    ?>
                                                                        <tr id="removequote<?= $rrr->id; ?>">
                                                                            <td><?= $rrr->id; ?></td>
                                                                            <!--<td><?= _d(date('d-M-Y', strtotime($rrr->created_date))); ?></td>-->
                                                                            <td><span style="display:none;"><?php echo strtotime($rrr->created_date); ?></span><?= date('d-M-Y', strtotime($rrr->created_date)); ?></td>
                                                                            <td>
                                                                                <!--<a href="<?= base_url('customers/requestdetails/'.$rrr->id); ?>" class="text-black"><?= $rrr->title; ?></a>-->
                                                                                <a href="javascript:void(0);" data-toggle="modal" onclick="getPreviewReq(<?= $rrr->id; ?>)" data-target="#quoteDetailsModal" class="text-black"><?= $rrr->title; ?></a>
                                                                            </td>
                                                                            <td><a href="<?= base_url('customers/requestdetails/'.$rrr->id); ?>" class="text-black"><?= $garageReply; ?></a></td>
                                                                            <td>
                                                                                <button type="button" class="btn btn-re-activate btn-sm" onClick="reActivateQuote(<?= $rrr->id ?>)"><i class="fa fa-reply" aria-hidden="true"></i> Re-Activate</button>
                                                                                <button type="button" onClick="removeQuoteReq(<?= $rrr->id ?>);" class="btn btn-appologise btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> Remove</button>
                                                                            </td>
                                                                        </tr>
                                                                    <?php
                                                                }
                                                                else
                                                                {
                                                                    ?>
                                                                        <tr id="removequote<?= $rrr->id; ?>">
                                                                            <td><?= $rrr->id; ?></td>
                                                                            <!--<td><?= _d(date('d-M-Y', strtotime($rrr->created_date))); ?></td>-->
                                                                            <td><span style="display:none;"><?php echo strtotime($rrr->created_date); ?></span><?= date('d-M-Y', strtotime($rrr->created_date)); ?></td>
                                                                            <td>
                                                                                <!--<a href="<?= base_url('customers/requestdetails/'.$rrr->id); ?>" class="text-black"><?= $rrr->title; ?></a>-->
                                                                                <a href="javascript:void(0);" data-toggle="modal" onclick="getPreviewReq(<?= $rrr->id; ?>)" data-target="#quoteDetailsModal" class="text-black"><?= $rrr->title; ?></a>
                                                                            </td>
                                                                            <td><a href="javascript:void(0);" class="text-black"><?= 0; ?></a></td>
                                                                            <td>
                                                                                <button type="button" class="btn btn-re-activate btn-sm" onClick="reActivateQuote(<?= $rrr->id ?>)"><i class="fa fa-reply" aria-hidden="true"></i> Re-Activate</button>
                                                                                <button type="button" onClick="removeQuoteReq(<?= $rrr->id ?>);" class="btn btn-appologise btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> Remove</button>
                                                                            </td>
                                                                        </tr>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            ?>
                                                                <tr>
                                                                    <td valign="top" colspan="5" style="text-align:center!important;">No matching records found</td>
                                                                </tr>
                                                            <?php
                                                        }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--
                                <div class="tab-pane container fade" id="menu2">
                                    <div class="draft-wrap">
                                        <div class="table-responsive">
                                            <table id="example21" class="table table-bordered commontable">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Request Date</th>
                                                        <th>Service</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td valign="top" colspan="2" class="dataTables_empty">No matching records found</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--// Row End-->
    </div>
    <!--// Container End-->
</section>
<!--// About Story End-->
<?php include('include/footer.php')?>
<?php include('include/scripts.php')?>
<script>
    function removeQuote(id)
    {
        if(id)
        {
            if (confirm("Confirm close this")) {
                var token_name = csrfData['token_name'];
                var token_hash = csrfData['hash'];
                
                //var str = {"id":id,token_name:token_hash};
                var str = "id="+id+"&"+csrfData['token_name']+"="+csrfData['hash'];
        	    $.ajax({
        	        url: '<?= base_url()?>customers/removeQuote',
        	        type: 'POST',
        	        data: str,
        	        //dataType: 'json',
        	        cache: false,
        	        success: function(resp){
        	            $('#removequote'+id).addClass('hide');
        	            $('#removequote'+id).css('display','none');
    	                return true;
        	        }
        	    });
            }
        }
    }
    function removeQuoteReq(id)
    {
        if(id)
        {
            if (confirm("Confirm remove this")) {
                var token_name = csrfData['token_name'];
                var token_hash = csrfData['hash'];
                
                //var str = {"id":id,token_name:token_hash};
                var str = "id="+id+"&"+csrfData['token_name']+"="+csrfData['hash'];
        	    $.ajax({
        	        url: '<?= base_url()?>customers/removeQuoteReq',
        	        type: 'POST',
        	        data: str,
        	        //dataType: 'json',
        	        cache: false,
        	        success: function(resp){
        	            $('#removequote'+id).addClass('hide');
        	            $('#removequote'+id).css('display','none');
    	                return true;
        	        }
        	    });
            }
        }
    }
    function reActivateQuote(id)
    {
        if(id)
        {
            if (confirm("Confirm re-active this")) {
                var token_name = csrfData['token_name'];
                var token_hash = csrfData['hash'];
                
                //var str = {"id":id,token_name:token_hash};
                var str = "id="+id+"&"+csrfData['token_name']+"="+csrfData['hash'];
        	    $.ajax({
        	        url: '<?= base_url()?>customers/reActivateQuote',
        	        type: 'POST',
        	        data: str,
        	        //dataType: 'json',
        	        cache: false,
        	        success: function(resp){
        	            $('#removequote'+id).addClass('hide');
        	            $('#removequote'+id).css('display','none');
        	            
    	                return true;
        	        }
        	    });
            }
        }
    }
    
    /* Get Draft Request */
    function getPreviewReq(id)
    {
        $('.previewrequest').html('');
        if(id)
        {
            var str = "id="+id+"&"+csrfData['token_name']+"="+csrfData['hash'];
    	    $.ajax({
    	        url: '<?= site_url()?>customers/getQuoteDetails',
    	        type: 'POST',
    	        data: str,
    	        cache: false,
    	        success: function(resp){
    	            if(resp)
    	            {
    	                $('.previewrequest').html(resp);
    	            }
    	            else
    	            {
    	                $('.previewrequest').html('');
    	            }
    	        }
    	    });
        }
        else
        {
            $('.previewrequest').html('');
        }
    }
    
    $(document).ready(function() {
        $('#myquotelist').DataTable( {
            "order": [[ 0, "desc" ]]
        } );
    } );
</script>

<!-- The detail Modal -->
<div class="modal" id="quoteDetailsModal">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <!-- Modal Header -->
         <div class="modal-header">
            <h5 class="modal-title">Details</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div> 
         <!-- Modal body -->
         <div class="modal-body">
            <div class="row">
               <div class="col-md-12">
                   <div class="previewrequest">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- The Delete Modal -->
<div class="modal" id="deleteModal">
   <div class="modal-dialog">
      <div class="modal-content">
         <!-- Modal Header -->
         <div class="modal-header">
            <h5 class="modal-title">Confirm delete</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <!-- Modal body -->
         <div class="modal-body">
            <h>Are you sure, want to delete this request?</h6>
         </div>
         <!-- Modal footer -->
         <div class="modal-footer">
            <button type="submit" class="btn btn-success">Confirm</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Discard</button>
         </div>
      </div>
   </div>
</div>
<!-- The detail Modal -->
<div class="modal" id="detailsModal">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <!-- Modal Header -->
         <div class="modal-header">
            <h5 class="modal-title">Details</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div> 
         <!-- Modal body -->
         <div class="modal-body">
            <div class="row">
               <div class="col-md-12">
                  <div class="request-wrapper">
                     <form class="request-form">
                        <div class="row">
                           <div class="col-12 col-xl-6 col-lg-6">
                              <div class="form-group">
                                 <label class="label">Title</label>
                                 <input type="text" readonly class="form-control" value="Title">
                              </div>
                           </div>
                           <div class="col-12 col-xl-6 col-lg-6">
                              <div class="form-group">
                                 <label class="label">Choose Your Location</label>
                                 <select class="form-control chosen-select">
                                    <option>Select location<option>
                                    <option>location 1<option>
                                    <option>location 2<option>
                                    <option>location 3<option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-12 col-xl-6 col-lg-6">
                              <div class="form-group">
                                 <label class="label">Choose Category</label>
                                 <select class="form-control chosen-select" name="category_id" onchange="getSubCategory(this.value);">
                                    <option value="">Select Categories<option>
                                          <?php
                                             if($category_result)
                                             {
                                                foreach($category_result as $res)
                                                {
                                                      ?>
                                                         <option value="<?= $res->id; ?>"><?= $res->name; ?></option>
                                                      <?php
                                                }
                                             }
                                          ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-12 col-xl-6 col-lg-6">
                              <div class="form-group">
                                 <label class="label">Choose Service Sub Category</label>
                                 <select class="form-control chosen-select" name="sub_category_id" id="sub_category_id">
                                    <option value="">Select sub category<option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-12 col-xl-6 col-lg-6">
                              <div class="form-group">
                                 <label class="label">Choose Vehicle Category</label>
                                 <select class="form-control chosen-select">
                                    <option>Select vehicle<option>
                                    <option>vehicle 1<option>
                                    <option>vehicle 2<option>
                                    <option>vehicle 3<option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-12 col-xl-6 col-lg-6">
                              <div class="form-group">
                                 <label class="label">Choose Brand</label>
                                 <select class="form-control chosen-select">
                                    <option>Select brand<option>
                                    <option>Brand 1<option>
                                    <option>Brand 2<option>
                                    <option>Brand 3<option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-12 col-xl-6 col-lg-6">
                              <div class="form-group">
                                 <label class="label">Choose Car Model</label>
                                 <select class="form-control chosen-select">
                                    <option>Select car model<option>
                                    <option>car model 1<option>
                                    <option>car model 2<option>
                                    <option>car model 3<option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-12 col-xl-6 col-lg-6">
                              <div class="form-group">
                                 <label class="label">Choose Car Make Year</label>
                                 <select class="form-control chosen-select">
                                    <option>Select Year<option>
                                    <option>2014<option>
                                    <option>2015<option>
                                    <option>2018<option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-12 col-xl-12 col-lg-12">
                                 <div class="form-group">
                                    <label class="label">Upload Vehicle Registration Card</label>
                                    <div class="input-group upload-wrap rounded-pill bg-white mb-20">
                                       <input type="file" id="file-upload1">
                                       <label for="file-upload" class="file-upload mb-0">Browse</label>
                                       <div id="file-upload-filename"></div>
                                    </div>
                                 </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-12 col-xl-12 col-lg-12">
                              <div class="form-group">
                                 <label class="label">Detailed Description</label>
                                 <textarea class="form-control" rows="4" placeholder="Detailed description here"></textarea>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-12 col-xl-12 col-lg-12">
                              <div class="form-group">
                                 <label class="label">Upload photo / audio / video</label>
                                 <div class="input-group upload-wrap rounded-pill bg-white mb-20">
                                    <input type="file" id="file-upload">
                                    <label for="file-upload" class="file-upload mb-0">Browse</label>
                                    <div id="file-upload-filename"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="form-group col-md-12">
                              <hr>
                              <button type="button" class="btn btn-appologise btn-block" data-dismiss="modal">cancel</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>