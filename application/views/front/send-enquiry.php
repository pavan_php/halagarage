<?php include('include/header.php')?>
<?php include('include/navbar.php')?>
<!--Breadcrumb Start-->
<div class="breadcrumb-area" style="background-image: url('assets/themes/immersive/images/about/about-us-banner-bg.png')">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner text-center">
                    <h1 class="page-title">Send Enquiry</h1>
                    <ul class="page-list">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li>Send Enquiry</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Breadcrumb End-->
<div class="terms-and-conditions-page padding-top-115 padding-bottom-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="request-wrapper">
                    <h4><b>APEX AUTO GARAGE</b></h4>
                    <hr>
                    <form>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="product" class="mb-5">Product Enquired For</label>
                                <input class="form-control" value="Lightings" id="product" disabled/>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="qty" class="mb-5">Qty. Required</label>
                                <input class="form-control" placeholder="Enter quantity here" id="qty">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="msg" class="mb-5">Message</label>
                                <textarea class="form-control" rows="8" placeholder="Place your text here..." id="msg"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <a href="javascript:void(0);" class="btn btn-outline-success btn-md"> Send Enquiry</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('include/footer.php')?>
<?php include('include/scripts.php')?>