        <!-- jquery -->
        <!--<script src="<?= base_url() ?>assets/themes/immersive/js/jquery-3.4.1.min.js"></script>-->
        <!--<script src="https://code.jquery.com/jquery-3.5.1.js"></script>-->
        
        <!--migrate-->
        <script src="<?= base_url() ?>assets/themes/immersive/js/jquery-migrate.min.js"></script>
        <!-- bootstrap -->
        <script src="<?= base_url() ?>assets/themes/immersive/js/bootstrap.min.js"></script>
        <!--Mean Menu-->
        <script src="<?= base_url() ?>assets/themes/immersive/js/jquery.meanmenu.min.js"></script>
        <!-- waypoint -->
        <script src="<?= base_url() ?>assets/themes/immersive/js/waypoints.min.js"></script>
        <!-- wow -->
        <script src="<?= base_url() ?>assets/themes/immersive/js/wow.min.js"></script>
        <!--Slick Js-->
        <script src="<?= base_url() ?>assets/themes/immersive/js/slick.min.js"></script>
        <!-- counterup -->
        <script src="<?= base_url() ?>assets/themes/immersive/js/jQuery.counter.js"></script>
        <!--Custom Video Popup-->
        <script src="<?= base_url() ?>assets/themes/immersive/js/jquery.Popup.js"></script>
        <!--Nice Select-->
        <script src="<?= base_url() ?>assets/themes/immersive/js/jquery.nice-select.min.js"></script>
        <!-- imageloaded -->
        <script src="<?= base_url() ?>assets/themes/immersive/js/imagesloaded.pkgd.min.js"></script>
        <!-- main js -->
        <script src="<?= base_url() ?>assets/themes/immersive/js/main.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
       
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
        <script src="http://bootstraplovers.com/templates/boland-shop-v1.1/plugins/masterslider/masterslider.min.js"></script>
        <script src="<?= base_url() ?>assets/js/images-grid.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
        <script src="<?= base_url() ?>assets/themes/immersive/js//bootstrap-table-expandable.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/datetimepicker/jquery.datetimepicker.full.min.js?v=2.4.2"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.min.js"></script>
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/component-chosen.css">
        <script>
            $('.chosen-select').chosen({}).change( function(obj, result) {});
            $('.chosen-select').chosen({}).trigger('chosen-select:updated');
        </script>
        <?php /* ?>
        <script src="<?= base_url() ?>assets/themes/immersive/js/select2.min.js"></script>
        <script>
            $("#make_id").select2( {
            	placeholder: "Select Brand",
            	allowClear: true
        	} );
        </script>
        <?php */ ?>
        <script>
            function getSubCategory(Id)
            {
                $('#sub_category_id').html('<option value=""></option>');
                var str = "catid="+Id+"&"+csrfData['token_name']+"="+csrfData['hash'];
        	    $.ajax({
        	        url: '<?= site_url()?>customers/getSubCategory',
        	        type: 'POST',
        	        data: str,
        	        dataType: 'json',
        	        cache: false,
        	        success: function(resp){
        	            $resp = '<option value=""></option>';
                        for(var i=0; i< resp.length; i++)
                        {
                            $resp += '<option value="'+resp[i].id+'">'+resp[i].name+'</option>';
                        }
                        $('#sub_category_id').html($resp);
                        setTimeout(function(){  
                            $('#sub_category_id').chosen('destroy'); 
                            $('#sub_category_id').chosen(); 
                        }, 1000);
        	        }
        	    });
        	    errorremove('categoryid');
            }
            
            function getModelList(Id)
            {
                $('#year_id').html('<option value=""></option>');
                var str = "catid="+Id+"&"+csrfData['token_name']+"="+csrfData['hash'];
        	    $.ajax({
        	        url: '<?= site_url()?>customers/getModelList',
        	        type: 'POST',
        	        data: str,
        	        dataType: 'json',
        	        cache: false,
        	        success: function(resp){
        	            console.log(resp);
        	            $resp = '<option value=""></option>';
                        for(var i=0; i< resp.length; i++)
                        {
                            $resp += '<option value="'+resp[i].id+'">'+resp[i].name+'</option>';
                        }
                        $('#model_id').html($resp);
                        setTimeout(function(){  
                            $('#model_id').chosen('destroy'); 
                            $('#model_id').chosen(); 
                        }, 1000);
                        // $('.chosen-select').chosen({}).change( function(obj, result) {});
        	        }
        	    });
        	    errorremove('make_id');
            }
            
            /* getYearList */
            function getYearList(Id)
            {
                var  values = $("#model_id").chosen().val();
                /*var make_id = $( "#make_id option:selected" ).val();*/
                var make_id = $( "#make_id" ).val();
                var str = "catid="+values+"&make_id="+make_id+"&"+csrfData['token_name']+"="+csrfData['hash'];
        	    $.ajax({
        	        url: '<?= site_url()?>customers/getYearList',
        	        type: 'POST',
        	        data: str,
        	        dataType: 'json',
        	        cache: false,
        	        success: function(resp){
        	            //console.log(resp);
        	            $resp = '<option value=""></option>';
                        for(var i=0; i< resp.length; i++)
                        {
                            $resp += '<option value="'+resp[i].id+'">'+resp[i].year+'</option>';
                        }
                        $('#year_id').html($resp);
                        setTimeout(function(){  
                            $('#year_id').chosen('destroy'); 
                            $('#year_id').chosen(); 
                        }, 1000);
        	        }
        	    });
        	    errorremove('model_id');
            }
            
            /* Get Width List */
            function getWidthList(Id)
            {
                $('#height_id').html('<option value=""></option>');
                $('#rim_id').html('<option value=""></option>');
                var str = "catid="+Id+"&"+csrfData['token_name']+"="+csrfData['hash'];
        	    $.ajax({
        	        url: '<?= site_url()?>customers/getWidthList',
        	        type: 'POST',
        	        data: str,
        	        dataType: 'json',
        	        cache: false,
        	        success: function(resp){
        	            console.log(resp);
        	            $resp = '<option value=""></option>';
                        for(var i=0; i< resp.length; i++)
                        {
                            $resp += '<option value="'+resp[i].id+'">'+resp[i].name+'</option>';
                        }
                        $('#width_id').html($resp);
                        setTimeout(function(){  
                            $('#width_id').chosen('destroy'); 
                            $('#width_id').chosen(); 
                        }, 1000);
        	        }
        	    });
            }
            
            /* getHeightList */
            function getHeightList(Id)
            {
                $('#rim_id').html('<option value=""></option>');
                var brand_id = $( "#brand_id option:selected" ).val();
                var str = "catid="+Id+"&brand_id="+brand_id+"&"+csrfData['token_name']+"="+csrfData['hash'];
        	    $.ajax({
        	        url: '<?= site_url()?>customers/getHeightList',
        	        type: 'POST',
        	        data: str,
        	        dataType: 'json',
        	        cache: false,
        	        success: function(resp){
        	            console.log(resp);
        	            $resp = '<option value=""></option>';
                        for(var i=0; i< resp.length; i++)
                        {
                            $resp += '<option value="'+resp[i].id+'">'+resp[i].value+'</option>';
                        }
                        $('#height_id').html($resp);
                        setTimeout(function(){  
                            $('#height_id').chosen('destroy'); 
                            $('#height_id').chosen(); 
                        }, 1000);
        	        }
        	    });
            }
            
            /* getRimList */
            function getRimList(Id)
            {
                var brand_id = $( "#brand_id option:selected" ).val();
                var width_id = $( "#width_id option:selected" ).val();
                var str = "catid="+Id+"&width_id="+width_id+"&brand_id="+brand_id+"&"+csrfData['token_name']+"="+csrfData['hash'];
        	    $.ajax({
        	        url: '<?= site_url()?>customers/getRimList',
        	        type: 'POST',
        	        data: str,
        	        dataType: 'json',
        	        cache: false,
        	        success: function(resp){
        	            console.log(resp);
        	            $resp = '<option value=""></option>';
                        for(var i=0; i< resp.length; i++)
                        {
                            $resp += '<option value="'+resp[i].id+'">'+resp[i].diameter+'</option>';
                        }
                        $('#rim_id').html($resp);
                        setTimeout(function(){  
                            $('#rim_id').chosen('destroy'); 
                            $('#rim_id').chosen(); 
                        }, 1000);
        	        }
        	    });
            }
            
            /* getAreaList */
            function getAreaList(Id)
            {
                var str = "catid="+Id+"&"+csrfData['token_name']+"="+csrfData['hash'];
        	    $.ajax({
        	        url: '<?= site_url()?>garage/getAreaList',
        	        type: 'POST',
        	        data: str,
        	        dataType: 'json',
        	        cache: false,
        	        success: function(resp){
        	            $resp = '<option value=""></option>';
                        for(var i=0; i< resp.length; i++)
                        {
                            $resp += '<option value="'+resp[i].id+'">'+resp[i].name+'</option>';
                        }
                        $('#area_id').html($resp);
                        setTimeout(function(){  
                            $('#area_id').chosen('destroy'); 
                            $('#area_id').chosen(); 
                        }, 1000);
        	        }
        	    });
            }
        </script>
        <script>
            function ValidateEmail(mail) 
            {
                if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
                {
                    return (true)
                }
                return (false)
            }
            
            function newsletters()
            {
                var email = $('#newsletter').val();
                if(email)
                {
                    var validemal = ValidateEmail(email);
                    if(validemal)
                    {
                        var str = "email="+email+"&"+csrfData['token_name']+"="+csrfData['hash'];
            		    $.ajax({
            		        url: '<?= site_url()?>garage/newsletter',
            		        type: 'POST',
            		        data: str,
            		        dataType: 'json',
            		        cache: false,
            		        success: function(resp){
            		            if(resp.status == 'success')
            		            {
            		                toastr.success('','Thank You For Subscribing!');
            		            }
            		            else
            		            {
            		                toastr.error(resp.msg,'Error!');
            		                $('#newsletter').val('');
            		            }
            		        }
            		    });
                    }
                    else
                    {
                        $('#newsletter').val('');
                        toastr.error('Invalid email, try again','Error!');
                    }
                }
                else
                {
                    //toastr.error('Email field is required','Error!');
                    $('.newsletter').text('Email field is required');
                }
            }
            
            function errorremove(id)
            {
                $('.'+id).text('');
            }
            
            /* Success message  */
            function successmsg(msg)
            {
                toastr.success('',msg);
            }
            
            function errormsg(msg)
            {
                toastr.error('',msg);
            }
        </script>
        <script>
            function composemsg()
            {
                var title = $('#title').val();
                var message = $('#message').val();
                if(title != '' && message != '')
                {
                    var str = "title="+title+"&message="+message+"&"+csrfData['token_name']+"="+csrfData['hash'];
            	    $.ajax({
            	        url: '<?= site_url()?>customers/composemsg',
            	        type: 'POST',
            	        data: str,
            	        cache: false,
            	        success: function(resp){
            	            if(resp)
            	            {
            	                $("#msg")[0].reset() ;
            	                successmsg('Message are send successfully');
            	                $('#closemodal').click(); 
            	            }
            	            else
            	            {
            	                errormsg('Message are not send');
            	            }
            	        }
            	    });
                }
                else
                {
                    var error = 'Title and message both field are required';
                    errormsg(error);
                }
            }
            
            function validateQuoteForm()
            {
                var title = $('#title').val();
                
                var description = $('#description').val();
                var chassis_no = $('#chassis_no').val();
                var fuData = document.getElementById('file-upload');
                var FileUploadPath = fuData.value;
                var fuDatas = document.getElementById('file-upload1');
                var FileUploadPaths = fuDatas.value;
                
                var locationid = $("#locationid").chosen().val();
                var categoryid = $("#categoryid").chosen().val();
                
                var sub_category_id = $("#sub_category_id").chosen().val();
                var vehiclecategory = $("#vehiclecategory").chosen().val();
                var make_id = $("#make_id").chosen().val();
                var model_id = $("#model_id").chosen().val();
                var year_id = $("#year_id").chosen().val();
                
                /*if(title == '' || chassis_no == '' || FileUploadPath == '' || FileUploadPaths == '' || description == '')*/
                if(title == '' || chassis_no == '' || FileUploadPath == '' || locationid == '' || categoryid == '' || sub_category_id == '' || vehiclecategory == '' || make_id == '' || model_id == '' || year_id == '')
                {
                    if(title == '')
                    {
                        $('.title').text('Description field is required');
                        return false;   
                    }
                    /*
                    if(FileUploadPaths == '')
                    {
                        $('.upload1').text('Photo / audio / video field is required');
                        return false;   
                    }
                    if(description == '')
                    {
                        $('.description').text('Detailed description field is required');
                        return false;   
                    }
                    */
                    
                    if(locationid == '')
                    {
                        $('.locationid').text('Location field is required');
                        return false;   
                    }
                    if(categoryid == '')
                    {
                        $('.categoryid').text('Category field is required');
                        return false;   
                    }
                    if(sub_category_id == '')
                    {
                        $('.sub_category_id').text('Sub category field is required');
                        return false;   
                    }
                    if(vehiclecategory == '')
                    {
                        $('.vehiclecategory').text('Vehiclecategory field is required');
                        return false;   
                    }
                    if(make_id == '')
                    {
                        $('.make_id').text('Make field is required');
                        return false;   
                    }
                    if(model_id == '')
                    {
                        $('.model_id').text('Model field is required');
                        return false;   
                    }
                    if(year_id == '')
                    {
                        $('.year_id').text('Year field is required');
                        return false;   
                    }
                    if(chassis_no == '' && FileUploadPath == '')
                    {
                        $('.chassis_no').text('Upload vehicle registration card or enter chassis number.');
                        return false; 
                    }
                    return true;
                }
                else
                {
                    return true;
                }
            }
            function validateQuoteDraftForm()
            {
                var title = $('#quatetitle').val();
                var oldfilename = $('#oldfilename').val();
                var chassis_no = $('#chassis_no-quote').val();
                var fuData = document.getElementById('file-upload-quote');
                var FileUploadPath = fuData.value;
                if(title == '' || chassis_no == '' || FileUploadPath == '')
                {
                    if(title == '')
                    {
                        $('.title-quote').text('Description field is required');
                        return false;   
                    }
                    if(chassis_no == '' && FileUploadPath == '')
                    {
                        if(oldfilename != '')
                        {
                            return true;
                        }
                        else
                        {
                            $('.chassis_no-quote').text('Upload vehicle registration card or enter chassis number.');
                            return false; 
                        }
                    }
                    return true;
                }
                else
                {
                    return true;
                }
            }
            function validateReuseDraftForm()
            {
                var title = $('#reusetitle').val();
                var oldfilename = $('#oldfilename-reuse').val();
                var chassis_no = $('#chassis_no-reuse').val();
                var fuData = document.getElementById('file-upload-reuse');
                var FileUploadPath = fuData.value;
                if(title == '' || chassis_no == '' || FileUploadPath == '')
                {
                    if(title == '')
                    {
                        $('.title-reuse').text('Description field is required');
                        return false;   
                    }
                    if(chassis_no == '' && FileUploadPath == '')
                    {
                        if(oldfilename != '')
                        {
                            return true;
                        }
                        else
                        {
                            $('.chassis_no-reuse').text('Upload vehicle registration card or enter chassis number.');
                            return false; 
                        }
                    }
                    return true;
                }
                else
                {
                    return true;
                }
            }
        </script>
        <script type="text/javascript">
            <?php if($this->session->flashdata('status') == 'success'){ ?>
                toastr.success("<?php echo $this->session->flashdata('msg') ?>");
            <?php }else if($this->session->flashdata('status') == 'error'){  ?>
                toastr.error("<?php echo $this->session->flashdata('msg') ?>");
            <?php }else if($this->session->flashdata('status') == 'warning'){  ?>
                toastr.warning("<?php echo $this->session->flashdata('msg') ?>");
            <?php }else if($this->session->flashdata('status') == 'info'){  ?>
                toastr.info("<?php echo $this->session->flashdata('msg') ?>");
            <?php } ?>
        </script>
    </body>
</html>
<script>
    /*------------------------------------
            Home Slider 01
    --------------------------------------*/ 
    var $AChomeSliderOne = $('.home-slider-one');
    if($AChomeSliderOne.length > 0){
        $AChomeSliderOne.slick({
            dots: false,
            infinite: true,
            speed: 1600,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            arrows: true,
            fade: true,
            appendDots: $('.carousel-dots'),
            appendArrows: $('.carousel-dots'),
            prevArrow: '<div class="slick-prev"> <i class="fa fa-long-arrow-left" aria-hidden="true"></i> </div>',
            nextArrow: '<div class="slick-next"> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> </div>',    
        });      
    }

    /*------------------------------------
        Home-01 Testimonial slider 
    --------------------------------------*/ 
    var $ACtestimonialSliderImage = $('.testimonial-images');
    var $ACtestimonialSliderContent = $('.testimonial-content');

    if($ACtestimonialSliderImage.length > 0 &&  $ACtestimonialSliderContent.length > 0 ){

        $ACtestimonialSliderImage.slick({
            dots: false,
            infinite: true,
            speed: 1000,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            arrows: false,    
            fade: true,
            asNavFor: $ACtestimonialSliderContent,
        }); 

        $ACtestimonialSliderContent.slick({
            dots: true,
            infinite: true,
            speed: 1000,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            arrows: true,    
            asNavFor: $ACtestimonialSliderImage,
            appendDots: $('.testimonial-dots-02'),
            appendArrows: $('.testimonial-arrow-02'),
            prevArrow: '<div class="slick-prev"> <i class="fa fa-long-arrow-left" aria-hidden="true"></i> </div>',
            nextArrow: '<div class="slick-next"> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> </div>',
        }); 
    }

    $(window).on('load', function(){
        new WOW().init(); 
    });

    var base_color = "white";
var active_color = "rgb(237, 40, 70)";

var child = 1;
var length = $("section").length - 1;
$("#prev").addClass("disabled");
$("#submit").addClass("disabled");

$("section").not("section:nth-of-type(1)").hide();
$("section")
  .not("section:nth-of-type(1)")
  .css("transform", "translateX(100px)");

var svgWidth = length * 200 + 24;
$("#svg_wrap").html(
  '<svg version="1.1" id="svg_form_time" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 ' +
    svgWidth +
    ' 24" xml:space="preserve"></svg>'
);

function makeSVG(tag, attrs) {
  var el = document.createElementNS("http://www.w3.org/2000/svg", tag);
  for (var k in attrs) el.setAttribute(k, attrs[k]);
  return el;
}

for (i = 0; i < length; i++) {
  var positionX = 12 + i * 200;
  var rect = makeSVG("rect", { x: positionX, y: 9, width: 200, height: 6 });
  document.getElementById("svg_form_time").appendChild(rect);
  // <g><rect x="12" y="9" width="200" height="6"></rect></g>'
  var circle = makeSVG("circle", {
    cx: positionX,
    cy: 12,
    r: 12,
    width: positionX,
    height: 6
  });
  document.getElementById("svg_form_time").appendChild(circle);
}

var circle = makeSVG("circle", {
  cx: positionX + 200,
  cy: 12,
  r: 12,
  width: positionX,
  height: 6
});
document.getElementById("svg_form_time").appendChild(circle);

$("circle:nth-of-type(1)").css("fill", active_color);

$(".button").click(function () {
  $("#svg_form_time rect").css("fill", active_color);
  $("#svg_form_time circle").css("fill", active_color);
  var id = $(this).attr("id");
  if (id == "next") {
    $("#prev").removeClass("disabled");
    if (child >= length) {
      $(this).addClass("disabled");
      $("#submit").removeClass("disabled");
    }
    if (child <= length) {
      child++;
    }
  } else if (id == "prev") {
    $("#next").removeClass("disabled");
    $("#submit").addClass("disabled");
    if (child <= 2) {
      $(this).addClass("disabled");
    }
    if (child > 1) {
      child--;
    }
  }
  var circle_child = child + 1;
  $("#svg_form_time rect:nth-of-type(n + " + child + ")").css(
    "fill",
    base_color
  );
  $("#svg_form_time circle:nth-of-type(n + " + circle_child + ")").css(
    "fill",
    base_color
  );
  var currentSection = $("section:nth-of-type(" + child + ")");
  currentSection.fadeIn();
  currentSection.css("transform", "translateX(0)");
  currentSection.prevAll("section").css("transform", "translateX(-100px)");
  currentSection.nextAll("section").css("transform", "translateX(100px)");
  $("section").not(currentSection).hide();
});


var input = document.getElementById( 'file-upload' );
var infoArea = document.getElementById( 'file-upload-filename' );

input.addEventListener( 'change', showFileName );

function showFileName( event ) {
  
  // the change event gives us the input it occurred in 
  var input = event.srcElement;
  
  // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
  var fileName = input.files[0].name;
  
  // use fileName however fits your app best, i.e. add it into a div
  infoArea.textContent = 'File name: ' + fileName;
}

var inputt = document.getElementById( 'file-upload1' );
var infoAreaa = document.getElementById( 'file-upload1-filename' );

inputt.addEventListener( 'change', showFilesName );

function showFilesName( event ) {
  
  // the change event gives us the input it occurred in 
  var inputt = event.srcElement;
  
  // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
  var filesName = inputt.files[0].name;
  
  // use fileName however fits your app best, i.e. add it into a div
  infoAreaa.textContent = 'File name: ' + filesName;
}
</script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#example').DataTable();
        } );
        
        $(document).ready( function () {
            $('.commontable').DataTable();
        } );
    </script>
    <!--
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.7/js/dataTables.fixedHeader.min.js"></script>
    <script>
            $(document).ready(function() {
                // Setup - add a text input to each footer cell
                $('#example thead tr').clone(true).appendTo( '#example thead' );
                $('#example thead tr:eq(1) th').each( function (i) {
                    var title = $(this).text();
                    $(this).html( '<input type="text" style="color: #ffffff;" placeholder="Search '+title+'" />' );
             
                    $( 'input', this ).on( 'keyup change', function () {
                        if ( table.column(i).search() !== this.value ) {
                            table
                                .column(i)
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );
             
                var table = $('#example').DataTable( {
                    orderCellsTop: true,
                    fixedHeader: true
                } );
                
                /* 2 */
                $('#example1 thead tr').clone(true).appendTo( '#example1 thead' ); 
                $('#example1 thead tr:eq(1) td').each( function (i) {
                    var title = $(this).text();
                    $(this).html( '<input type="text" style="color: #ffffff;" class="form-control" placeholder="Search'+title+'" />' );
             
                    $( 'input', this ).on( 'keyup change', function () {
                        if ( table.column(i).search() !== this.value ) {
                            table
                                .column(i)
                                .search( this.value )
                                .draw();
                        }
                    });
                });
             
                var table = $('#example1').DataTable( {
                    orderCellsTop: true,
                    fixedHeader: true
                } );
                
                /* 3 */
                $('#example2 thead tr').clone(true).appendTo( '#example2 thead' );
                    $('#example2 thead tr:eq(1) td').each( function (i) {
                    var title = $(this).text();
                    $(this).html( '<input type="text" style="color: #ffffff;" class="form-control" placeholder="Search'+title+'" />' );
             
                    $( 'input', this ).on( 'keyup change', function () {
                        if ( table.column(i).search() !== this.value ) {
                            table
                                .column(i)
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );
             
                var table = $('#example2').DataTable( {
                    orderCellsTop: true,
                    fixedHeader: true
                } );
            } );
        </script>
        -->