            <!-- footer area start -->
        <!--<footer class="footer-area style-02" style="background-image: url('assets/themes/immersive/images/bg/footer-bg.png')">-->
        <footer class="footer-area style-02">
            <div class="footer-top ">
                <div class="container">
                    <div class="row">
                        <div class="footer-subscribe-area">
                            <h4 class="title">Subscribe News Letter & Get The Latest Updates</h4>
                            <div class="subscribe-area">
                                <div class="input-group">
                                    <input type="email" id="newsletter" class="form-control" onkeyup="errorremove('newsletter')" placeholder="Enter your email address">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <input type="button" onClick="newsletters()" value="Subscribe">
                                        </div>
                                    </div>
                                    <p class="text-danger newsletter"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-top-125 padding-bottom-45">
                        <div class="col-lg-4 col-md-6">
                            <div class="footer-widget widget">
                                <div class="about_us_widget">
                                    <a href="javascript:void(0);" class="footer-logo"> <img src="<?= base_url() ?>assets/themes/immersive/images/logo/logo-2.png"
                                            alt="footer logo"></a>
                                    <p>We believe brand interaction is key in commu- nication. Real innovations and a
                                        positive customer
                                        experience are the heart of successful communication.</p>

                                    <div class="footer-social-icon padding-top-10">
                                        <div class="banner__header__follow_us">
                                            <div class="text">FOLLOW US</div>
                                            <div class="banner__header__icon">
                                                <ul>
                                                    <li><a class="icon" href="https://www.facebook.com/Hala-Garage-104104681347866" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a class="icon" href="https://www.instagram.com/halagarage/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                                    <li><a class="icon" href="https://www.youtube.com/channel/UCsZswIJcWHjQkVnKoGM3hqw" target="_blank"><i class="fa fa-youtube"></i></a></li>
                                                    <!--<li><a class="icon" href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>-->
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="footer-widget widget widget_nav_menu">
                                <h5 class="widget-title">USEFUL LINKS</h5>
                                <ul class="useful-links">
                                    <li><a href="<?= base_url('contact-us'); ?>">Contact Us</a></li>
                                    <li><a href="<?= base_url('about-us'); ?>">About Us</a></li>
                                    <li><a href="<?= base_url('privacy-policy'); ?>">Privacy Policy</a></li>
                                    <li><a href="<?= base_url('term-and-conditions'); ?>">Terms and Conditions</a></li>
                                    <li><a href="<?= base_url('faq'); ?>">FAQ</a></li>
                                    <!--
                                    <li><a href="javascript:void(0);">24x7 Support</a></li>
                                    <li><a href="javascript:void(0);">Sitemap</a></li>
                                    <li><a href="javascript:void(0);">Testimonials</a></li>
                                    <li class="last-child"><a href="<?= base_url('blog'); ?>">Blog</a></li>
                                    -->
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6">
                            <div class="footer-widget widget widget_nav_menu">
                                <h5 class="widget-title">SERVICES</h5>
                                <ul>
                                    <li><a href="<?= base_url('services'); ?>">AC Services</a></li>
                                    <li><a href="<?= base_url('services'); ?>">Body Parts Repairing</a></li>
                                    <li><a href="<?= base_url('services'); ?>">Cleaning & Washing</a></li>
                                    <li><a href="<?= base_url('services'); ?>">Painting</a></li>
                                    <li><a href="<?= base_url('services'); ?>">Electrical</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="footer-widget widget widget_nav_menu">
                                <h5 class="widget-title">ADDRESS</h5>
                                <div class="contact-area">
                                    <ul>
                                        <li>
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            <a href="javascript:void(0);"> Lavaca Street, Suite 2000 Austin, TX 24141</a>
                                        </li>
                                        <li><i class="fa fa-envelope" aria-hidden="true"></i> <a href="javascript:void(0);"> info@halagarage.com</a></li>
                                        <li><i class="fa fa-phone" aria-hidden="true"></i> <a href="javascript:void(0);"> (+88) 0172 570051</a></li>
                                        <li><i class="fa fa-globe" aria-hidden="true"></i> <a href="<?= base_url(); ?>"> www.halagarage.com</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="copyright-area-inner">
                                Copyright &copy; Halagarage 2021, All rights reserved.<br>
                                <span style="font-size:12px;">Powered By <a style="font-size:12px;" href="https://www.immersiveinfotech.com/" target="_blank">Immersive Infotech.</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer area end -->

        <!-- back to top area start -->
        <div class="back-to-top">
            <span class="back-top"> <i class="fa fa-chevron-up"></i> </span>
        </div>
        <!-- back to top area end -->
        