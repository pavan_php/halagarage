<!--Full Width Sider Start-->
<div class="full-width-slider">
    <!--Main Header Start-->
    <header>
        <div class="container">
            <div class="row">
                <div class="header-bottom-area">
                    <!--Logo Area Start-->
                    <div class="logo-area">
                        <a href="<?= base_url(); ?>">
                            <!--<img src="<?= base_url() ?>assets/themes/immersive/images/logo/logo-2.png" alt="Logo">-->
                            <?php
                                $settinglogo = $this->db->get_where('tbllogin_setting')->row();
                                if($settinglogo->logo_image != '')
                                {
                                    ?>
                                        <img src="<?= base_url() ?>uploads/loginPage/<?= $settinglogo->logo_image; ?>" alt="" class="img-fluid">
                                    <?php
                                }
                            ?>
                        </a>
                    </div>
                    <!--// Logo Area End-->

                    <!--Navbar Area Start Here-->
                    <nav class="navbar navbar-area navbar-expand-lg style-02">
                        <div class="container nav-container">
                            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#autoshop_main_menu" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="humberger-menu black">
                                    <span class="one"></span>
                                    <span class="two"></span>
                                    <span class="three"></span>
                                </span>
                            </button>
                            <div class="navbar-collapse collapse" id="autoshop_main_menu" style="">
                                <ul class="navbar-nav">
                                    <?php
                                        if(is_client_logged_in())
                                        {
                                            ?>
                                                <!--<li class="menu-item">
                                                    <a href="<?= base_url(); ?>">
                                                        Home
                                                    </a>
                                                </li>-->
                                                <li class="menu-item">
                                                    <a href="<?= base_url('customers'); ?>">
                                                        Home
                                                    </a>
                                                </li>
                                                <li class="menu-item">
                                                    <a href="<?= base_url('customers/message'); ?>">
                                                        Message
                                                    </a>
                                                </li>
                                                <li class="menu-item">
                                                    <a href="<?= base_url('customers/findCarPartService'); ?>">
                                                        Find car part/Services
                                                    </a>
                                                </li>
                                                
                                                <li class="menu-item dropdown">
                                                    <span class="badge navigationbadge newquotes text-white">0</span>
                                                    <a class="dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                                        <?= _l('Quotes'); ?>
                                                    </a>
                                                    <div class="dropdown-menu">
                                                        <a href="<?= base_url('customers/myQuote'); ?>">
                                                            My Quotes
                                                        </a>
                                                        <a href="<?= base_url('customers/getAQuote'); ?>">
                                                            Get a quote
                                                        </a>
                                                    </div>
                                                </li>
                                                <!--<li class="menu-item">
                                                    <a href="<?= base_url('customers/requestAQuote'); ?>">
                                                        Request a quote
                                                    </a>
                                                </li>-->
                                                <li class="menu-item">
                                                    <a href="<?= base_url('customers/myOrder'); ?>">
                                                        My Orders
                                                    </a>
                                                </li>
                                                <!--
                                                <li class="menu-item">
                                                    <a href="<?= base_url('customers/getAQuote'); ?>">
                                                        Get a quote
                                                    </a>
                                                </li>
                                                <li class="menu-item">
                                                    <a href="<?= base_url('customers/startQuote'); ?>">
                                                        Start quote
                                                    </a>
                                                </li>
                                                -->
                                                <!--<li class="login-li">
                                                    <a href="<?php echo site_url('garage/logout'); ?>">
                                                       <?php echo _l('clients_nav_logout'); ?>
                                                    </a>
                                                </li>-->
                                                <!-- Dropdown -->
                                                <li class="menu-item dropdown">
                                                    <a class="dropdown-toggle companyname" href="#" id="navbardrop" data-toggle="dropdown">
                                                        <?= _l(get_company_name(get_client_user_id())); ?>
                                                    </a>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="<?= base_url('customers/profile'); ?>">Account Settings</a>
                                                        <!--<a class="dropdown-item" href="#">Profile</a>-->
                                                        <a href="<?php echo site_url('garage/logout'); ?>">
                                                            <?php echo _l('clients_nav_logout'); ?>
                                                        </a>
                                                    </div>
                                                </li>
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                                <li class="menu-item">
                                                    <a href="<?= base_url('garage/register'); ?>"> Register as a Garage</a>
                                                </li>
                                                <li class="menu-item">
                                                    <a href="<?= base_url('blog');?>">Blog</a>
                                                </li>
                                                <li class="menu-item">
                                                    <a href="<?= base_url('about-us');?>">About Us</a>
                                                </li>
                                                <li class="login-li">
                                                    <a href="<?= base_url('login'); ?>">Login/ Signup</a>
                                                </li>
                                            <?php
                                        }
                                    ?>
                                </ul>
                            </div>
                            <!--Nav Right Content-->
                            
                            <!--// Nav Right Content-->
                        </div>
                    </nav>
                    <!-- navbar area end -->
                </div>
                <!--// header Bottom-->
            </div>
        </div>
    </header>
    <!--// Main Header End Here-->

    <!--Slider Area Start-->
    <div class="home-slider-area style-02">
        <div class="video-header">
            <div class="overlay"></div>
            <?php
                $video_home = $this->db->get_where(db_prefix().'video_section', array('id' => 1))->row(); 
                $video_garage = $this->db->get_where(db_prefix().'video_section', array('id' => 3))->row();
                if(get_client_user_id())
                {
                    $imageArray = $this->db->get_where(db_prefix().'files', array('rel_id' => $video_garage->id, 'rel_type' => "garagevideo"))->row();
                    $code2 = $video_garage->code;
	                $url2 = '';
	                if($code2 != '')
	                {
	                    ?>
	                        <iframe width="100%" height="450" src="https://www.youtube.com/embed/<?= $code2; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	                    <?php
	                }
	                else
	                {
	                    $url2 = site_url('uploads/tasks/'.$video_garage->id.'/'.$imageArray->file_name);
	                    ?>
	                        <video width="100%" height="450" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
                                <source src="<?= $url2; ?>" type="video/mp4">
                            </video>
	                    <?php
	                }
                }
                else
                {
                    if($video_home)
                    {
                        $imageArray1 = $this->db->get_where(db_prefix().'files', array('rel_id' => $video_home->id, 'rel_type' => "homevideo"))->row();
                        $code = $video_home->code;
		                $url = '';
		                if($code != '')
		                {
		                    ?>
		                        <iframe width="100%" height="450" src="https://www.youtube.com/embed/<?= $code; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		                    <?php
		                }
		                else
		                {
		                    $url = site_url('uploads/tasks/'.$video_home->id.'/'. $imageArray1->file_name);
		                    ?>
		                        <video width="100%" height="450" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
                                    <source src="<?= $url; ?>" type="video/mp4">
                                </video>
		                    <?php
		                }
                    }
                    else{
                        ?>
                            <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
                                <source src="<?= base_url() ?>assets/themes/immersive/images/bg/video/Engine - 5497.mp4" type="video/mp4">
                            </video>
                        <?php
                    }
                }
            ?>
            <div class="container h-100">
                <div class="d-flex h-100 text-center align-items-center">
                    <div class="w-100 text-white">
                        <h1 class="display-3">The Best Car Service <br> Awaits You</h1>
                        <h5 class="lead mb-0 text-white">Get instant quotes for your car service</h5>
                        <br>
                        <div class="main-btn-wrap">
                            <a href="<?= base_url('customers/findCarPartService'); ?>" class="main-btn" tabindex="0">Find Car Part or Service</a>
                            <a href="<?= base_url('customers/getAQuote'); ?>" class="main-btn" tabindex="0">Get Quote</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--//Slider Area End-->
</div>
<!--// Full Width Sider End-->