<!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <!--<link rel="apple-touch-icon”" id="favicon-apple-touch-icon" href="<?= base_url('uploads/company/favicon.png'); ?>">-->
            <link rel="icon" type="image/png" href="<?= base_url('uploads/company/favicon.png'); ?>"/>
            <title><?= ($title != '')?$title:"Halagarage"; ?></title>
            
            <!-- flaticon -->
            <link rel="stylesheet" href="<?= base_url() ?>assets/themes/immersive/css/flaticon.css">
            <!-- Fonts Awesome Icons -->
            <link rel="stylesheet" href="<?= base_url() ?>assets/themes/immersive/css/font-awesome.min.css">
            <!--Themefy Icons-->
            <link rel="stylesheet" href="<?= base_url() ?>assets/themes/immersive/css/themify-icons.css">
            <!-- bootstrap -->
            <link rel="stylesheet" href="<?= base_url() ?>assets/themes/immersive/css/bootstrap.min.css">
            <!-- animate -->
            <link rel="stylesheet" href="<?= base_url() ?>assets/themes/immersive/css/animate.css">
            <!--Video Popup-->
            <link rel="stylesheet" href="<?= base_url() ?>assets/themes/immersive/css/popup.min.css">
            <!--Slick Carousel-->
            <link rel="stylesheet" href="<?= base_url() ?>assets/themes/immersive/css/slick.css">
            <!-- Main Stylesheet -->
            <link rel="stylesheet" href="<?= base_url() ?>assets/themes/immersive/css/style.css">
            <!--<link rel="stylesheet" href="<?= base_url() ?>assets/themes/immersive/css/select2.min.css" />-->
            <!-- responsive Stylesheet -->
            <link rel="stylesheet" href="<?= base_url() ?>assets/themes/immersive/css/responsive.css">
            <link rel="stylesheet" href="<?= base_url() ?>assets/themes/immersive/css/owl.carousel.min.css">
            <link rel="stylesheet" href="<?= base_url() ?>assets/themes/immersive/css/masterslider.css">
            <link rel="stylesheet" href="<?= base_url() ?>assets/themes/immersive/css/maeterslider-style.css">
            <link rel="stylesheet" href="<?= base_url() ?>assets/themes/immersive/css/images-grid.css">
            <link rel="stylesheet" href="<?= base_url() ?>assets/themes/immersive/css/jquery.datetimepicker.min.css">
            <link rel="stylesheet" href="<?= base_url() ?>assets/themes/immersive/css/bootstrap-datepicker3.min.css">
            <link rel="stylesheet" href="<?= base_url() ?>assets/themes/immersive/css/assets/themes/immersive/css/bootstrap-table-expandable.css">
            <link rel="stylesheet" href="<?= base_url() ?>assets/themes/immersive/css/datatables.min.css">
            
            <!--<link href"https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" />-->
            <link rel="stylesheet" href="<?= base_url() ?>assets/themes/immersive/css/fixedHeader.dataTables.min.css" />
            <style>
        	    .navigationbadge{
        	        position: absolute;
                    top: 18px;
                    right: 0px;
                    background: red;
                }
                .owl-prev, .owl-next{padding:6px 12px;    margin-top: 10px;}
        	</style>
	    
            <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>-->
            <?php// echo compile_theme_css(); ?>
        	<script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js'); ?>"></script>
        	<?php app_customers_head(); ?>
        	<style>
                .dataTables_empty {
                    background: url(<?= site_url() ?>assets/images/table-no-data.png);
                    background-repeat: no-repeat;
                    background-position: center;
                    height: 228px;
                    padding-top: 14px !important;
                    text-align: left !important;
                    color: #777777;
                    font-size: 15px;
                }
            </style>
        </head>
        <script>
    	    var lid = '<?= get_client_user_id() ?>';
    	    
    	    function quoteNotification()
	        {
	            if(lid != '')
	            {
                    var str = "id="+lid+"&"+csrfData['token_name']+"="+csrfData['hash'];
            	    $.ajax({
            	        url: '<?= site_url()?>notifications/customerNotify',
            	        type: 'POST',
            	        data: str,
            	        //dataType: 'json',
            	        cache: false,
            	        success: function(resp){
            	            if(resp)
            	            {
            	                $('.newquotes').html(resp);
            	                return true;
            	            }
            	            else
            	            {
            	                $('.newquotes').html(0);
            	                return false;
            	            }
            	        }
            	    });   
	            }
	            else
	            {
	               $('.newquotes').html(0); 
	            }
	        }
	        
	        setInterval(function(){ quoteNotification(); }, 3000);
	        quoteNotification();
    	</script>

        <body>
            <!-- preloader area start -->
            <div class="preloader" id="preloader">
                <div class="preloader-inner">
                    <div class="spinner">
                        <div class="dot1"></div>
                        <div class="dot2"></div>
                    </div>
                </div>
            </div>
            <!-- preloader area end -->