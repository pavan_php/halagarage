<!--Full Width Sider Start-->
<div class="full-width-slider">
    <!--Main Header Start-->
    <header>
        <div class="container">
            <div class="row">
                <div class="header-bottom-area">
                    <!--Logo Area Start-->
                    <div class="logo-area">
                        <a href="<?= base_url(); ?>">
                            <!--<img src="<?= base_url() ?>assets/themes/immersive/images/logo/logo-2.png" alt="Logo">-->
                            <?php
                                $settinglogo = $this->db->get_where('tbllogin_setting')->row();
                                if($settinglogo->logo_image != '')
                                {
                                    ?>
                                        <img src="<?= base_url() ?>uploads/loginPage/<?= $settinglogo->logo_image; ?>" alt="" class="img-fluid">
                                    <?php
                                }
                            ?>
                        </a>
                    </div>
                    <!--// Logo Area End-->

                    <!--Navbar Area Start Here-->
                    <nav class="navbar navbar-area navbar-expand-lg style-02">
                        <div class="container nav-container">
                            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#autoshop_main_menu" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="humberger-menu black">
                                    <span class="one"></span>
                                    <span class="two"></span>
                                    <span class="three"></span>
                                </span>
                            </button>
                            <div class="navbar-collapse collapse" id="autoshop_main_menu" style="">
                                <!--<ul class="navbar-nav">
                                    <li class="menu-item">
                                        <a href="<?= site_url('garage/register'); ?>"> Register as a Garage</a>
                                    </li>
                                    <li class="menu-item">
                                        <a href="javascript:void(0);">Blog</a>
                                    </li>
                                    <li class="menu-item">
                                        <a href="javascript:void(0);">More</a>
                                    </li>
                                    <?php
                                        if($this->session->userdata('is_login'))
                                        {
                                            ?>
                                                <li class="menu-item">
                                                    <a href="#"><?php echo html_escape($this->session->userdata('garage_name')); ?></a>
                                                </li>
                                                <li class="login-li">
                                                    <a href="<?php echo site_url('garage/logout'); ?>">
                                                       <?php echo _l('clients_nav_logout'); ?>
                                                    </a>
                                                </li>
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                                <li class="login-li">
                                                    <a href="<?= base_url('login'); ?>">Login/ Signup</a>
                                                </li>
                                            <?php
                                        }
                                    ?>
                                </ul>-->
                                <ul class="navbar-nav">
                                    <?php
                                        if(is_client_logged_in())
                                        {
                                            ?>
                                                <!--<li class="menu-item">
                                                    <a href="<?= base_url(); ?>">
                                                        Home
                                                    </a>
                                                </li>-->
                                                <li class="menu-item">
                                                    <a href="<?= base_url('customers'); ?>">
                                                        Home
                                                    </a>
                                                </li>
                                                <li class="menu-item">
                                                    <a href="<?= base_url('customers/message'); ?>">
                                                        Message
                                                    </a>
                                                </li>
                                                <li class="menu-item">
                                                    <a href="<?= base_url('customers/findCarPartService'); ?>">
                                                        Find car part/Services
                                                    </a>
                                                </li>
                                                
                                                <li class="menu-item dropdown">
                                                    <span class="badge navigationbadge newquotes text-white">0</span>
                                                    <a class="dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                                        <?= _l('Quotes'); ?>
                                                    </a>
                                                    <div class="dropdown-menu">
                                                        <a href="<?= base_url('customers/myQuote'); ?>">
                                                            My Quotes
                                                        </a>
                                                        <a href="<?= base_url('customers/getAQuote'); ?>">
                                                            Get a quote
                                                        </a>
                                                    </div>
                                                </li>
                                                <!--<li class="menu-item">
                                                    <a href="<?= base_url('customers/requestAQuote'); ?>">
                                                        Request a quote
                                                    </a>
                                                </li>-->
                                                <li class="menu-item">
                                                    <a href="<?= base_url('customers/myOrder'); ?>">
                                                        My Orders
                                                    </a>
                                                </li>
                                                <!--
                                                <li class="menu-item">
                                                    <a href="<?= base_url('customers/getAQuote'); ?>">
                                                        Get a quote
                                                    </a>
                                                </li>
                                                <li class="menu-item">
                                                    <a href="<?= base_url('customers/startQuote'); ?>">
                                                        Start quote
                                                    </a>
                                                </li>
                                                -->
                                                <!--<li class="login-li">
                                                    <a href="<?php echo site_url('garage/logout'); ?>">
                                                       <?php echo _l('clients_nav_logout'); ?>
                                                    </a>
                                                </li>-->
                                                <!-- Dropdown -->
                                                <li class="menu-item dropdown">
                                                    <a class="dropdown-toggle companyname" href="#" id="navbardrop" data-toggle="dropdown">
                                                        <?= _l(get_company_name(get_client_user_id())); ?>
                                                    </a>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="<?= base_url('customers/profile'); ?>">Account Settings</a>
                                                        <!--<a class="dropdown-item" href="#">Profile</a>-->
                                                        <a href="<?php echo site_url('garage/logout'); ?>">
                                                            <?php echo _l('clients_nav_logout'); ?>
                                                        </a>
                                                    </div>
                                                </li>
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                                <li class="menu-item">
                                                    <a href="<?= base_url('garage/register'); ?>"> Register as a Garage</a>
                                                </li>
                                                <li class="menu-item">
                                                    <a href="<?= base_url('blog');?>">Blog</a>
                                                </li>
                                                <li class="menu-item">
                                                    <a href="<?= base_url('about-us');?>">About Us</a>
                                                </li>
                                                <li class="login-li">
                                                    <a href="<?= base_url('login'); ?>">Login/ Signup</a>
                                                </li>
                                            <?php
                                        }
                                    ?>
                                </ul>
                            </div>
                            <!--Nav Right Content-->
                            
                            <!--// Nav Right Content-->
                        </div>
                    </nav>
                    <!-- navbar area end -->
                </div>
                <!--// header Bottom-->
            </div>
        </div>
    </header>
    <!--// Main Header End Here-->
</div>
<!--// Full Width Sider End-->