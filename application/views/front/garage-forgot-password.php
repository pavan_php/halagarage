<?php include('include/header.php')?>
<style>
    .login-wrap {
        background: #f5f5f5;
        min-height: 100vh;
        position: relative;
        text-align: center;
        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 30px 0;
    }
    .login-wrap .login-box-6 {
        margin: 0 0 0 100px;
        max-width: 100%;
        border-radius: 5px;
        box-shadow: 0 0 35px rgb(0 0 0 / 10%);
        background: #fff;
    }
    .login-wrap .bg-img {
        background-size: cover;
        width: 100%;
        bottom: 0;
        border-radius: 5px;
        padding: 60px 20px!important;
        background: #3385d9;
        left: -100px;
        z-index: 999;
    }
    .login-wrap .col-pad-0 {
        padding: 0;
    }
    .login-wrap .bg-img .logo {
        height: 30px;
        margin-bottom: 30px;
    }
    .login-wrap .bg-img p {
        font-size: 15px;
        color: #fff;
        margin-bottom: 30px;
        padding-top:30px;
    }
    .login-wrap .btn-outline {
        cursor: pointer;
        height: 50px;
        color: #fff;
        padding: 11px 45px 10px 45px;
        font-size: 16px;
        font-weight: 400;
        font-family: 'Jost', sans-serif;
        border-radius: 3px;
        border: solid 2px #de2f2f;
    }
    .login-wrap .bg-img .social-list {
        padding: 0;
        margin: 35px 0 0;
    }
    .login-wrap .bg-img .social-list li {
        display: inline-block;
        font-size: 16px;
    }
    .login-wrap .bg-img .social-list li a {
        margin: 0 5px;
        font-size: 22px;
        color: #de2f2f;
        border-radius: 3px;
        display: inline-block;
    }
    .login-wrap .col-pad-0 {
        padding: 0;
    }
    .login-wrap .login-inner-form {
        color: #717171;
        text-align: center;
    }
    .login-wrap .login-inner-form .details {
        padding: 100px 100px 100px 0;
    }
    .login-wrap .logo-2 {
        display: none;
    }
    .login-wrap .login-inner-form h3 {
        margin: 0 0 30px;
        font-size: 20px;
        font-weight: 500;
        color: #3385d9;
    }
    .login-wrap .login-inner-form h5{color: #3385d9;}
    .login-wrap .login-inner-form .form-group {
        margin-bottom: 25px;
    }
    .login-wrap .login-inner-form .input-text {
        outline: none;
        width: 100%;
        padding: 10px 20px;
        font-size: 16px;
        height: 50px;
        outline: 0;
        font-weight: 500;
        color: #717171;
        border-radius: 3px;
        border: 1px solid #dbdbdb;
        box-shadow: 0 1px 3px 0 rgb(0 0 0 / 6%);
    }
    .login-wrap .btn{
        border:none!important;
    }
    .login-wrap .login-inner-form .btn-theme {
        background: #3385d9;
        border: none;
        color: #fff;
        border-radius:0px!important;
    }
    .login-wrap .login-inner-form .btn-md {
        cursor: pointer;
        height: 50px;
        color: #fff;
        padding: 13px 50px 12px 50px;
        font-size: 15px;
        font-weight: 400;
        font-family: 'Jost', sans-serif;
        border-radius: 3px;
        text-transform: uppercase;
    }
    .login-wrap .login-inner-form .details p {
        margin-bottom: 0;
        font-size: 16px;
        color: #404040;
    }
    .text-danger {
        color: #dc3545!important;
        position: absolute;
        bottom: -20px;
        font-size: 11px;
        font-weight: 400!important;
        text-transform: none!important;
        font-size: 12px!important;
    }
    @media (max-width: 992px){
        .none-992 {
            display: none !important;
        }
    }
    @media (max-width: 768px){
        .login-wrap .login-inner-form .details {
            padding: 60px 30px;
        }
    }
    @media (max-width: 992px){
        .login-wrap .login-inner-form .details {
            padding: 30px;
        }
    }
    @media (max-width: 768px){
        .login-wrap .login-box-6 {
            margin: 0;
        }
    }
    @media (max-width: 992px){
        .login-wrap .logo-2 {
            display: block;
        }
    }
    .login-wrap .form-bottom-area{
        margin-top:10px;
    }
    .login-wrap .forgot-password a{
        color: #3385d9;
        text-transform: none;
        font-size: 14px;
    }
    .login-wrap .remember-me a{
        color: #000;
        font-size: 14px;
        font-weight: 400;
    }
    .login-wrap .remember-me a b{color: #3385d9;}
    .login-wrap .back-link{color: #3385d9;}
</style>
<div class="login-wrap">
    <div class="container">
        <div class="offset-md-2 col-md-8 col-sm-12 pad-0">
            <div class="row login-box-6">
                <div class="col-lg-6 col-md-12 col-sm-12 col-pad-0 bg-img align-self-center none-992">
                    <a href="<?= base_url(); ?>">
                        <img src="<?= base_url() ?>assets/themes/immersive/images/logo/logo-white.png" alt="Logo" style="width:50%;">
                    </a>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy.</p>
                </div>
                <div class="col-lg-6 col-sm-12 col-sm-12 col-pad-0 align-self-center">
                    <div class="login-inner-form">
                        <div class="details">
                            <div class="logo-2 clearfix">
                                <a href="<?= base_url(); ?>">
                                    <img src="<?= base_url() ?>assets/themes/immersive/images/logo/logo-2.png" alt="Logo" style="width:100%;margin-bottom:20px;">
                                </a>
                            </div>
                            <h5><b>Recover your password (Garage)</b></h5>
                            <p>Enter email to reset password</p>
                            <br>
                            <!--Form-->
                            <?= form_open($this->uri->uri_string());?> 
                                <div class="input-group mb-20">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Enter your email">
                                    <?= form_error('email'); ?>
                                </div>
                                <!--// Email-->
                                <div class="main-btn-wrap text-center">
                                    <input type="submit" class="btn main-btn btn-theme uppercase btn-block" value="SUBMIT">
                                </div>
                                <br>
                                <div class="text-center">
                                    <a href="<?= base_url('garage/login'); ?>" class="back-link">Back</a>
                                </div>
                            </form>
                            <!--// Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<div class="sign padding-120">
    <div class="container">
        <div class="row">
            <div class="offset-lg-4 col-lg-4">
                <div class="logo-wrapper text-center mb-40">
                    <a href="<?= base_url(); ?>">
                        <img src="<?= base_url() ?>assets/themes/immersive/images/logo/logo-2.png" alt="Logo" style="width:50%;">
                    </a>
                </div>
                <div class="sign-in-area">
                    <h5 class="text-center"><b>RESET PASSWORD (GARAGE)</b></h5>
                    <br>
                    <?= form_open($this->uri->uri_string());?> 
                        <div class="input-group mb-20">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Enter Your Email">
                            <?= form_error('email'); ?>
                        </div>
                        <div class="main-btn-wrap text-center">
                            <input type="submit" class="main-btn uppercase btn-block" value="SUBMIT">
                        </div>
                        <br>
                        <div class="text-center">
                            <a href="<?= base_url('garage/login'); ?>"><b>Back</b></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>-->
<?php include('include/scripts.php')?>