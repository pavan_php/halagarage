<?php include('include/header.php')?>
<div class="sign padding-120">
    <div class="container">
        <div class="row">
            
            <div class="offset-lg-4 col-lg-4">
                <div class="logo-wrapper text-center mb-40">
                    <a href="<?= base_url(); ?>">
                        <img src="<?= base_url() ?>assets/themes/immersive/images/logo/160x69-logo.jpg" alt="Logo" style="width:50%;">
                    </a>
                </div>
                <div class="sign-in-area">
                    <h5><b>Welcome Back!</b></h5>
                    <p>Login To See More in HalaGarage</p>
                    <br>
                    <?= $msg; ?>
                    <!--Form-->
                    <?php echo form_open($this->uri->uri_string(),array('class'=>'login-form')); ?>
                        <div class="input-group mb-20">
                            <?php echo form_error('email'); ?>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter Your Email">
                        </div>
                        <!--// Email-->

                        <div class="input-group mb-20">
                            <?php echo form_error('password'); ?>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Enter Your Password">
                        </div>
                        <!--// Password-->
                        
                        <div class="main-btn-wrap text-center">
                            <input type="submit" class="main-btn uppercase" value="LOGIN">
                        </div>
                        <div class="form-bottom-area padding-top-30">
                            <div class="remember-me w-70">
                                <a href="<?= base_url() ?>garage/register">Not An Hala Garage User? <b>Sign Up</b></a>
                            </div>
                            <div class="forgot-password w-50">
                                <a href="<?= base_url() ?>garage/forgot_password"><b>Forgot Password</b></a>
                            </div>
                        </div>
                    </form>
                    <!--// Form-->
                </div>
                <!--// Sign In Area-->
            </div>
        </div>
    </div>
</div>
<?php include('include/scripts.php')?>
