<?php include('include/header.php')?>
<div class="sign sign-area padding-120">
    <div class="container">
        <div class="row">
            
            <div class="offset-lg-4 col-lg-4">
                <div class="logo-wrapper text-center mb-40">
                    <a href="<?= base_url(); ?>">
                        <img src="<?= base_url() ?>assets/themes/immersive/images/logo/160x69-logo.jpg" alt="Logo" style="width:50%;">
                    </a>
                </div>
                <div class="sign-in-area">
                    <h5 class="text-center"><b>LOGIN</b></h5>
                    <br>
                    <!--Form-->
                    <?php echo form_open('home/login',array('class'=>'login-form')); ?>
                    <?php hooks()->do_action('clients_login_form_start'); ?>
                        <div class="input-group mb-20">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter Your Email">
                            <p><?php echo form_error('email'); ?></p>
                        </div>
                        <!--// Email-->

                        <div class="input-group mb-20">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Enter Your Password">
                            <p><?php echo form_error('password'); ?></p>
                        </div>
                        <!--// Password-->
                        
                        <div class="main-btn-wrap text-center">
                            <input type="submit" class="main-btn uppercase" value="LOGIN">
                        </div>
                        <div class="form-bottom-area padding-top-30">
                            <div class="remember-me">
                                <a href="<?php echo site_url('home/register'); ?>">New User? <b>Register</b></a>
                            </div>
                            <div class="forgot-password">
                                <a href="<?php echo site_url('home/forgot_password'); ?>"><b>Forgot Password</b></a>
                            </div>
                        </div>
                    <?php hooks()->do_action('clients_login_form_end'); ?>
                    <?php echo form_close(); ?>
                    <!--// Form-->
                </div>
                <!--// Sign In Area-->
            </div>
        </div>
    </div>
</div>
<?php include('include/scripts.php')?>