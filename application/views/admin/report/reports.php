<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l($title); ?></h4>
						<hr class="hr-panel-heading" />
						<div class="row mbot15">
                             <div class="col-md-4 col-xs-6 border-right">
                                <h3 class="bold"><?php 
                                    $totalRevenue = $this->db->select('SUM(payment_gross) as price')->get_where(db_prefix().'_payments')->result(); 
                                    echo $totalRevenue[0]->price.' '.CURRENCY_NAME;
                                ?></h3>
                                <span class="text-success"><?php echo _l('Total Revenue'); ?></span>
                             </div>
                             <div class="col-md-4 col-xs-6 border-right">
                                <h3 class="bold"><?php $onemonth = strtotime("-1 months");
                                    $monthlyRevenue = $this->db->select('SUM(payment_gross) as price')->get_where(db_prefix().'_payments', array('expiry_date > ' => $onemonth))->result(); 
                                    echo $monthlyRevenue[0]->price.' '.CURRENCY_NAME;
                                ?></h3>
                                <span class="text-dark"><?php echo _l('Monthly Revenue'); ?></span>
                             </div>
                             <div class="col-md-4 col-xs-6 border-right">
                                <h3 class="bold"><?php $lastweek = strtotime('last week');
                                    $weeklyRevenue = $this->db->select('SUM(payment_gross) as price')->get_where(db_prefix().'_payments', array('expiry_date > ' => $lastweek))->result(); 
                                    echo $weeklyRevenue[0]->price.' '.CURRENCY_NAME;
                                ?></h3>
                                <span class="text-dark"><?php echo _l('Last Week Revenue'); ?></span>
                             </div>
                        </div>
                        <hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Name'),
							_l('Email'),
							_l('Subscription'),
							_l('Price'),
							_l('Payment Date'),
							_l('Expiry Date')
							),'report'); 
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php init_tail(); ?>
	<script>
		initDataTable('.table-report', window.location.href, [1], [1]);
		
		$(document).ready(function() {
            $('#DataTables_Table_0').DataTable( {
                "order": [[ 0, "desc" ]]
            } );
        } );
	</script>
</body>
</html>
