<?php defined('BASEPATH') or exit('No direct script access allowed');

render_datatable([
    _l('Sn.'),
    _l('Garage'),
    _l('Product Name'),
    _l('Price'),
    _l('Appointment Date'),
    _l('Request Date'),
], (isset($class) ? $class : 'payments'), [], [
    'data-last-order-identifier' => 'payments',
    'data-default-order'         => get_table_last_order('payments'),
]);
