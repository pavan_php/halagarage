<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
    				<div class="panel-body">
    				    <h4 class="customer-profile-group-heading"><?= _l('Home Video'); ?></h4>
					    <hr class="hr-panel-heading" />
    					<?= form_open_multipart(admin_url('videoSection/homeVideo_update'));  ?> 
    					    <div class="row form-group">
        					    <div class="col-md-3">
        					       <lable>
        					            <?= _l('Selected Video'); ?><br>
        					            <?php $imageArray1 = $this->db->get_where(db_prefix().'files', array('rel_id' => $video_home->id, 'rel_type' => "homevideo"))->row(); ?>
        					            <?php
        					                $code = $video_home->code;
        					                $url = '';
        					                if($code != '')
        					                {
        					                    ?>
        					                        <iframe width="200" height="150" src="https://www.youtube.com/embed/<?= $code; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        					                    <?php
        					                }
        					                else
        					                {
        					                    $url = site_url('download/file/taskattachment/'. $imageArray1->attachment_key);
        					                    ?>
        					                        <video width="200" height="150" controls>
                                                        <source src="<?= $url; ?>" type="video/mp4">
                                                    </video>
        					                    <?php
        					                }
        					            ?>
        					       </lable>
        					    </div>
        					    <div class="col-md-3">
        					       <?= _l('Change Video'); ?>
        					       <input type="file" filesize="<?php echo file_upload_max_size(); ?>" class="form-control" name="homevideo">
        					       <input type="text" class="form-control hide" required  name="name" value="Home">
        					    </div>
        					    <div class="col-md-3">
        					       <?= _l('Or URL'); ?>[<small>Youtube code</small>]
    					           <input type="text" autocomplete="off" value="<?= $video_home->code; ?>" class="form-control" name="code">
        					    </div>
        					    <div class="col-md-3"><br/>
        					       <button type="submit" class="btn btn-info">Home Video</button>
        					    </div>
    					    </div>
    				    </form>
    				</div>
    			</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
    				    <h4 class="customer-profile-group-heading"><?= _l('Garage Video'); ?></h4>
					    <hr class="hr-panel-heading" />
    					<?= form_open_multipart(admin_url('videoSection/customerVideo_update'));  ?> 
    					    <div class="row form-group">
        					    <div class="col-md-3">
        					       <lable>
        					           <?= _l('Selected Video'); ?><br>
        					           <?php $imageArray2 = $this->db->get_where(db_prefix().'files', array('rel_id' => $video_customer->id, 'rel_type' => "customervideo"))->row(); ?>
        					           <?php
        					                $code1 = $video_customer->code;
        					                $url1 = '';
        					                if($code1 != '')
        					                {
        					                    ?>
        					                        <iframe width="200" height="150" src="https://www.youtube.com/embed/<?= $code1; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        					                    <?php
        					                }
        					                else
        					                {
        					                    $url1 = site_url('download/file/taskattachment/'. $imageArray2->attachment_key);
        					                    ?>
        					                        <video width="200" height="150" controls>
                                                        <source src="<?= $url1; ?>" type="video/mp4">
                                                    </video>
        					                    <?php
        					                }
        					            ?>
                					            
        					       </lable>
        					    </div>
        					    <div class="col-md-3">
        					       <?= _l('Change Video'); ?>
        					       <input type="file" filesize="<?php echo file_upload_max_size(); ?>" class="form-control" name="customervideo">
        					       <input type="text" class="form-control hide" required  name="name" value="Customer">
        					    </div>
        					    <div class="col-md-3">
        					       <?= _l('Or URL'); ?>[<small>Youtube code</small>]
    					           <input type="text" autocomplete="off" value="<?= $video_customer->code; ?>" class="form-control" name="code">
        					    </div>
        					    <div class="col-md-3"><br/>
        					       <button type="submit" class="btn btn-info">Save Video</button>
        					    </div>
    					    </div>
    				    </form>
    				</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
    				    <h4 class="customer-profile-group-heading"><?= _l('Customer Video'); ?></h4>
					    <hr class="hr-panel-heading" />
    					<?= form_open_multipart(admin_url('videoSection/garageVideo_update'));  ?> 
    					    <div class="row form-group">
        					    <div class="col-md-3">
        					       <lable>
        					           <?= _l('Selected Video'); ?><br>
        					           <?php $imageArray = $this->db->get_where(db_prefix().'files', array('rel_id' => $video_garage->id, 'rel_type' => "garagevideo"))->row(); ?>
        					           <?php
        					                $code2 = $video_garage->code;
        					                $url2 = '';
        					                if($code2 != '')
        					                {
        					                    ?>
        					                        <iframe width="200" height="150" src="https://www.youtube.com/embed/<?= $code2; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        					                    <?php
        					                }
        					                else
        					                {
        					                    $url2 = site_url('download/file/taskattachment/'. $imageArray->attachment_key);
        					                    ?>
        					                        <video width="200" height="150" controls>
                                                        <source src="<?= $url2; ?>" type="video/mp4">
                                                    </video>
        					                    <?php
        					                }
        					            ?>
        					       </lable>
        					    </div>
        					    <div class="col-md-3">
        					       <?= _l('Change Video'); ?>
        					       <input type="file" filesize="<?php echo file_upload_max_size(); ?>" class="form-control" name="garagevideo">
        					       <input type="text" class="form-control hide" required  name="name" value="Garage">
        					    </div>
        					    <div class="col-md-3">
        					       <?= _l('Or URL'); ?>[<small>Youtube code</small>]
    					           <input type="text" autocomplete="off" value="<?= $video_garage->code; ?>" class="form-control" name="code">
        					    </div>
        					    <div class="col-md-3"><br/>
        					       <button type="submit" class="btn btn-info">Save Video</button>
        					    </div>
    					    </div>
    				    </form>
    				</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php init_tail(); ?>
	<script>
		initDataTable('.table-videoSection', window.location.href, [1], [1]);
		initDataTable('.table-videoSectionCustomer', '<?= admin_url() ?>videoSection/videoSectionCustomer', [1], [1]);
		//initDataTable('.table-sub_videoSection', '<?= admin_url() ?>videoSection/videoSectionGarage', [1], [1]);
		initDataTable('.table-videoSectionGarage', '<?= admin_url() ?>videoSection/videoSectionGarage', [1], [1]);
	</script>
</body>
</html>
