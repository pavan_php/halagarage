<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-4 hide">
				<div class="panel_s">
				    <?= form_open_multipart(admin_url('subscription/add'));  ?>
    					<div class="panel-body">
    					   <div class="form-group">
    					       <?= _l('Title'); ?>
    					       <input type="text" required class="form-control" name="title" >
    					   </div>
    					   <div class="form-group">
    					       <?= _l('Quantity'); ?>
    					       <input type="number" required class="form-control" name="qty" >
    					   </div>
    					   <div class="form-group">
    					       <?= _l('Price'); ?>
    					       <input type="text" required class="form-control" name="price" >
    					   </div>
    					   <div class="form-group">
    					       <?= _l('Description'); ?>
    					       <textarea class="form-control" rows="5" name="description" required></textarea>
    					   </div>
    					   <div class="form-group">
    					       <button type="submit" class="btn btn-info">Save</button>
    					   </div>
    					</div>
    				</form>
    			</div>
			</div>
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l($title); ?></h4>
						<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Title'),
							_l('Days'),
							_l('Price'),
							_l('Description'),
							_l('Date'),
							_l('options')
							),'subscription'); 
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php init_tail(); ?>
	<script>
		initDataTable('.table-subscription', window.location.href, [1], [1]);
		
		function setSubscriptionStatus(id, status_)
        {
            if ($('#status_'+id).is(":checked"))
                var sts = 1;
            else
                var sts = 0;
            var token_name = csrfData['token_name'];
            var token_hash = csrfData['hash'];
            var str = {"id":id,"status":sts,token_name:token_hash};
            $.ajax({
                url: admin_url+'subscription/change_status',
                type: 'POST',
                data: str,
                dataType: 'json',
                cache: false,
                success: function(resp){
                    if(resp)
                    {
                        alert_float('success', resp.message);
                        return true;
                    }
                    else
                    {
                        
                        return false;
                    }
                }
            });
        }
	</script>
</body>
</html>
