<div class="col-md-12">
	<div class="panel_s">
		<div class="panel-body">
			<h4 class="customer-profile-group-heading"><?= _l('Add Model'); ?></h4>
			<hr class="hr-panel-heading" />
			<?= form_open(admin_url('car/addmodel'));  ?>
				<div class="row form-group">
				    <div class="col-md-4">
				       <?= _l('Make'); ?>
				       <select class="form-control" name="make_id" required>
				           <option value=""></option>
				           <?php
				                if($carmake)
				                {
				                    foreach($carmake as $rrr)
				                    {
				                        ?>
				                            <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
				                        <?php
				                    }
				                }
				           ?>
				       </select>
				   </div>
				    <div class="col-md-4">
				       <?= _l('Model'); ?>
				       <input type="text" required class="form-control" name="name">
				   </div>
				   <div class="col-md-4"><br>
				       <button type="submit" class="btn btn-info">Save</button>
				   </div>
				</div>
			</form>
			<hr class="hr-panel-heading" />
			<?php render_datatable(array(
				_l('Make Name'),
				_l('Model Name'),
				_l('options')
				),'car_model'); 
			?>
		</div>
	</div>
</div>
<script>
	initDataTable('.table-car_model', window.location.href, [1], [1]);
</script>