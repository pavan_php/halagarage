<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l('Make/Model/Year Settings Page'); ?></h4>
						<hr class="hr-panel-heading" />
						<?= form_open(admin_url('car/add/'.$article->id));  ?>
        					<div class="row form-group">
        					    <div class="col-md-6">
        					       <?= _l('Make'); ?>
        					       <input type="text" required value="<?= (isset($article) ? $article->name : '')?>" class="form-control" name="name">
        					   </div>
        					   <div class="col-md-6"><br>
        					       <button type="submit" class="btn btn-info">Update Make</button>
        					   </div>
        					</div>
        				</form>
        				<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Make'),
							_l('options')
							),'car_make'); 
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="carmodel">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l('Add Model'); ?></h4>
						<hr class="hr-panel-heading" />
						<?= form_open(admin_url('car/addmodel'));  ?>
        					<div class="row form-group">
        					    <div class="col-md-4">
        					       <?= _l('Make'); ?>
        					       <select class="form-control" name="make_id" required>
        					           <option value=""></option>
        					           <?php
        					                if($carmake)
        					                {
        					                    foreach($carmake as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
        					                        <?php
        					                    }
        					                }
        					           ?>
        					       </select>
        					   </div>
        					    <div class="col-md-4">
        					       <?= _l('Model'); ?>
        					       <input type="text" required class="form-control" name="name">
        					   </div>
        					   <div class="col-md-4"><br>
        					       <button type="submit" class="btn btn-info">Update Model</button>
        					   </div>
        					</div>
        				</form>
        				<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Make Name'),
							_l('Model Name'),
							_l('options')
							),'car_model'); 
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="caryear">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l('Add Year'); ?></h4>
						<hr class="hr-panel-heading" />
						<?= form_open(admin_url('car/addyear'));  ?>
        					<div class="row form-group">
        					    <div class="col-md-3">
        					       <?= _l('Make'); ?>
        					       <select class="form-control" name="make_id" required>
        					           <option value=""></option>
        					           <?php
        					                if($carmake)
        					                {
        					                    foreach($carmake as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
        					                        <?php
        					                    }
        					                }
        					           ?>
        					       </select>
        					   </div>
        					   <div class="col-md-3">
        					       <?= _l('Model'); ?>
        					       <select class="form-control" name="model_id" required>
        					           <option value=""></option>
        					           <?php
        					                if($carmodel)
        					                {
        					                    foreach($carmodel as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
        					                        <?php
        					                    }
        					                }
        					           ?>
        					       </select>
        					   </div>
        					    <div class="col-md-3">
        					       <?= _l('Year'); ?>
        					       <input type="text" required class="form-control" name="year">
        					   </div>
        					   <div class="col-md-3"><br>
        					       <button type="submit" class="btn btn-info">Save Year</button>
        					   </div>
        					</div>
        				</form>
        				<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Make Name'),
							_l('Model Name'),
							_l('Year'),
							_l('options')
							),'car_year'); 
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php init_tail(); ?>
	<script>
		initDataTable('.table-car_make', window.location.href, [1], [1]);
		initDataTable('.table-car_model', '<?= admin_url() ?>car/modeltable', [1], [1]);
		initDataTable('.table-car_year', '<?= admin_url() ?>car/yeartable', [1], [1]);
	</script>
</body>
</html>
