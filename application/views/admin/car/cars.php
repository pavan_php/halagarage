<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-3">
			    <div class="panel_s">
			        <div class="panel-body">
			            <h4 class="customer-profile-group-heading"><?= _l('Make bulk upload'); ?></h4>
						<hr class="hr-panel-heading" />
    				    <div class="form-group">
					       <a href="<?= admin_url('car/sampledata'); ?>" class="btn btn-success">Download Sample</a>
					   </div>
					   <?= form_open_multipart(admin_url('car/save'));  ?>
    					   <div class="form-group">
    					       <input type="file" name="fileURL" required id="file-url" class="filestyle" data-allowed-file-extensions="[CSV, csv]" accept=".CSV, .csv" data-buttontext="Choose File">
    					   </div>
    					   <div class="form-groupt">
                                <button type="submit" class="btn btn-primary mrgT">Import</button>
                            </div>  
                        </form>
    				</div>
			    </div>
			</div>
			<div class="col-md-9">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l('Make/Model/Year/Varient'); ?></h4>
						<hr class="hr-panel-heading" />
						<?= form_open(admin_url('car/add'));  ?>
        					<div class="row form-group">
        					    <div class="col-md-6">
        					       <?= _l('Make'); ?>
        					       <input type="text" required class="form-control" name="name">
        					   </div>
        					   <div class="col-md-6"><br>
        					       <button type="submit" class="btn btn-info">Save Make</button>
        					   </div>
        					</div>
        				</form>
        				<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Make'),
							_l('options')
							),'car_make'); 
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="carmodel">
		    <div class="col-md-3">
			    <div class="panel_s">
			        <div class="panel-body">
			            <h4 class="customer-profile-group-heading"><?= _l('Model bulk upload'); ?></h4>
						<hr class="hr-panel-heading" />
    				    <div class="form-group">
					       <a href="<?= admin_url('car/sampledataModel'); ?>" class="btn btn-success">Download Sample</a>
					   </div>
					   <?= form_open_multipart(admin_url('car/saveModel'));  ?>
    					   <div class="form-group">
    					       <input type="file" name="fileURL" required id="file-url" class="filestyle" data-allowed-file-extensions="[CSV, csv]" accept=".CSV, .csv" data-buttontext="Choose File">
    					   </div>
    					   <div class="form-groupt">
                                <button type="submit" class="btn btn-primary mrgT">Import</button>
                            </div>  
                        </form>
    				</div>
			    </div>
			</div>
			<div class="col-md-9">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l('Add Model'); ?></h4>
						<hr class="hr-panel-heading" />
						<?= form_open(admin_url('car/addmodel'));  ?>
        					<div class="row form-group">
        					    <div class="col-md-4">
        					       <?= _l('Make'); ?>
        					       <select class="form-control" name="make_id" required>
        					           <option value=""></option>
        					           <?php
        					                if($carmake)
        					                {
        					                    foreach($carmake as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
        					                        <?php
        					                    }
        					                }
        					           ?>
        					       </select>
        					   </div>
        					    <div class="col-md-4">
        					       <?= _l('Model'); ?>
        					       <input type="text" required class="form-control" name="name">
        					   </div>
        					   <div class="col-md-4"><br>
        					       <button type="submit" class="btn btn-info">Update Model</button>
        					   </div>
        					</div>
        				</form>
        				<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Make Name'),
							_l('Model Name'),
							_l('options')
							),'car_model'); 
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="caryear">
		    <div class="col-md-3">
			    <div class="panel_s">
			        <div class="panel-body">
			            <h4 class="customer-profile-group-heading"><?= _l('Year bulk upload'); ?></h4>
						<hr class="hr-panel-heading" />
    				    <div class="form-group">
					       <a href="<?= admin_url('car/sampledataYear'); ?>" class="btn btn-success">Download Sample</a>
					   </div>
					   <?= form_open_multipart(admin_url('car/saveYear'));  ?>
    					   <div class="form-group">
    					       <input type="file" name="fileURL" required id="file-url" class="filestyle" data-allowed-file-extensions="[CSV, csv]" accept=".CSV, .csv" data-buttontext="Choose File">
    					   </div>
    					   <div class="form-groupt">
                                <button type="submit" class="btn btn-primary mrgT">Import</button>
                            </div>  
                        </form>
    				</div>
			    </div>
			</div>
			<div class="col-md-9">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l('Add Year'); ?></h4>
						<hr class="hr-panel-heading" />
						<?= form_open(admin_url('car/addyear'));  ?>
        					<div class="row form-group">
        					    <div class="col-md-3">
        					       <?= _l('Make'); ?>
        					       <select class="form-control" name="make_id" required onchange="getmodellist(this.value);">
        					           <option value=""></option>
        					           <?php
        					                if($carmake)
        					                {
        					                    foreach($carmake as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
        					                        <?php
        					                    }
        					                }
        					           ?>
        					       </select>
        					   </div>
        					   <div class="col-md-3">
        					       <?= _l('Model'); ?>
        					       <select class="form-control" name="model_id" required id="model_id">
        					           <option value=""></option>
        					           <?php
        					           /*
        					                if($carmodel)
        					                {
        					                    foreach($carmodel as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
        					                        <?php
        					                    }
        					                }
        					                */
        					           ?>
        					       </select>
        					   </div>
        					    <div class="col-md-3">
        					       <?= _l('Year'); ?>
        					       <input type="text" required class="form-control" name="year">
        					   </div>
        					   <div class="col-md-3"><br>
        					       <button type="submit" class="btn btn-info">Save Year</button>
        					   </div>
        					</div>
        				</form>
        				<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Make Name'),
							_l('Model Name'),
							_l('Year'),
							_l('options')
							),'car_year'); 
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="carVarient">
		    <div class="col-md-3">
			    <div class="panel_s">
			        <div class="panel-body">
			            <h4 class="customer-profile-group-heading"><?= _l('Varient bulk upload'); ?></h4>
						<hr class="hr-panel-heading" />
    				    <div class="form-group">
					       <a href="<?= admin_url('car/sampledataVarient'); ?>" class="btn btn-success">Download Sample</a>
					   </div>
					   <?= form_open_multipart(admin_url('car/saveVarient'));  ?>
    					   <div class="form-group">
    					       <input type="file" name="fileURL" required id="file-url" class="filestyle" data-allowed-file-extensions="[CSV, csv]" accept=".CSV, .csv" data-buttontext="Choose File">
    					   </div>
    					   <div class="form-groupt">
                                <button type="submit" class="btn btn-primary mrgT">Import</button>
                            </div>  
                        </form>
    				</div>
			    </div>
			</div>
			<div class="col-md-9">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l('Varient List'); ?></h4>
						<hr class="hr-panel-heading" />
						<!--<?= form_open(admin_url('car/addvarient'));  ?>
        					<div class="row form-group">
        					    <div class="col-md-3">
        					       <?= _l('Make'); ?>
        					       <select class="form-control" name="make_id" required onchange="getmodellist(this.value);">
        					           <option value=""></option>
        					           <?php
        					                if($carmake)
        					                {
        					                    foreach($carmake as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
        					                        <?php
        					                    }
        					                }
        					           ?>
        					       </select>
        					   </div>
        					   <div class="col-md-3">
        					       <?= _l('Model'); ?>
        					       <select class="form-control" name="model_id" required id="model_id">
        					           <option value=""></option>
        					           <?php
        					           /*
        					                if($carmodel)
        					                {
        					                    foreach($carmodel as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
        					                        <?php
        					                    }
        					                }
        					                */
        					           ?>
        					       </select>
        					   </div>
        					   <div class="col-md-3">
        					       <?= _l('Year'); ?>
        					       <select class="form-control" name="year_id" required id="year_id">
        					           <option value=""></option>
        					       </select>
        					   </div>
        					    <div class="col-md-3">
        					       <?= _l('Varient'); ?>
        					       <input type="text" required class="form-control" name="varient">
        					   </div>
        					   <div class="col-md-3"><br>
        					       <button type="button" class="btn btn-info">Save Varient</button>
        					   </div>
        					</div>
        				</form>
        				<hr class="hr-panel-heading" />-->
						<?php render_datatable(array(
							_l('Make Name'),
							_l('Model Name'),
							_l('Year'),
							_l('Varient'),
							_l('options')
							),'car_varient'); 
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php init_tail(); ?>
	<script>
		initDataTable('.table-car_make', window.location.href, [1], [1]);
		initDataTable('.table-car_model', '<?= admin_url() ?>car/modeltable', [1], [1]);
		initDataTable('.table-car_year', '<?= admin_url() ?>car/yeartable', [1], [1]);
		initDataTable('.table-car_varient', '<?= admin_url() ?>car/varienttable', [1], [1]);
		
		function getmodellist(Id)
		{
		    $('#model_id').html('<option value="">Please waite...</option>');
		    var str = "makeid="+Id+"&"+csrfData['token_name']+"="+csrfData['hash'];
		    $.ajax({
		        url: '<?= admin_url()?>car/getmodellist',
		        type: 'POST',
		        data: str,
		        datatype: 'json',
		        cache: false,
		        success: function(resp_){
		            if(resp_)
		            {
		                var resp = JSON.parse(resp_);
		                var res = '<option value=""></option>';
		                for(var i=0; i<resp.length; i++)
		                {
		                   res += '<option value="'+resp[i].id+'">'+resp[i].name+'</option>';
		                }
		                $('#model_id').html(res);
		            }
		            else
		            {
		                $('#model_id').html('<option value=""></option>');
		            }
		        }
		    });
		}
	</script>
</body>
</html>
