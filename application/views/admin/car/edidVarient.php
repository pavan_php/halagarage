<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row" id="caryear">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l('Edit Verient'); ?></h4>
						<hr class="hr-panel-heading" />
						<?= form_open(admin_url('car/edidVarient/'.$year_result->id));  ?>
        					<div class="row form-group">
        					    <div class="col-md-3">
        					       <?= _l('Make'); ?>
        					       <select class="form-control" name="make_id" required onchange="getmodellist(this.value);">
        					           <option value=""></option>
        					           <?php
        					                if($carmake)
        					                {
        					                    foreach($carmake as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>" <?= ($year_result->make_id == $rrr->id)?"selected":"";?>><?= $rrr->name; ?></option>
        					                        <?php
        					                    }
        					                }
        					           ?>
        					       </select>
        					   </div>
        					   <div class="col-md-3">
        					        <?= _l('Model'); ?>
        					        <select class="form-control" name="model_id" required id="model_id">
        					            <option value=""></option>
        					            <?php
        					                if($carmodel)
        					                {
        					                    foreach($carmodel as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>" <?= ($year_result->model_id == $rrr->id)?"selected":"";?>><?= $rrr->name; ?></option>
        					                        <?php
        					                    }
        					                }
        					           ?>
        					       </select>
        					   </div>
        					    <div class="col-md-3">
        					       <?= _l('Year'); ?>
        					       <select class="form-control" name="year_id" required id="year_id">
        					            <option value=""></option>
        					            <?php
        					                if($yearResult)
        					                {
        					                    foreach($yearResult as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>" <?= ($year_result->year_id == $rrr->id)?"selected":"";?>><?= $rrr->year; ?></option>
        					                        <?php
        					                    }
        					                }
        					           ?>
        					       </select>
        					   </div>
        					    <div class="col-md-3">
        					       <?= _l('Varient'); ?>
        					       <input type="text" required class="form-control" value="<?= $year_result->varient; ?>" name="varient">
        					   </div>
        					   <div class="col-md-3"><br>
        					       <button type="submit" class="btn btn-info">Update Year</button>
        					   </div>
        					</div>
        				</form>
        				<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Make Name'),
							_l('Model Name'),
							_l('Year'),
							_l('options')
							),'car_year'); 
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php init_tail(); ?>
	<script>
		initDataTable('.table-car_make', window.location.href, [1], [1]);
		initDataTable('.table-car_model', '<?= admin_url() ?>car/modeltable', [1], [1]);
		initDataTable('.table-car_year', '<?= admin_url() ?>car/yeartable', [1], [1]);
		
		function getmodellist(Id)
		{
		    $('#model_id').html('<option value="">Please waite...</option>');
		    var str = "makeid="+Id+"&"+csrfData['token_name']+"="+csrfData['hash'];
		    $.ajax({
		        url: '<?= admin_url()?>car/getmodellist',
		        type: 'POST',
		        data: str,
		        datatype: 'json',
		        cache: false,
		        success: function(resp_){
		            if(resp_)
		            {
		                var resp = JSON.parse(resp_);
		                var res = '<option value=""></option>';
		                for(var i=0; i<resp.length; i++)
		                {
		                   res += '<option value="'+resp[i].id+'">'+resp[i].name+'</option>';
		                }
		                $('#model_id').html(res);
		            }
		            else
		            {
		                $('#model_id').html('<option value=""></option>');
		            }
		        }
		    });
		}
	</script>
</body>
</html>
