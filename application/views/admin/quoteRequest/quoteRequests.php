<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l($title); ?></h4>
						<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Customer'),
							_l('Request Title'),
							_l('Location'),
							_l('Category'),
							_l('Vehicle'),
							_l('Brand'),
							_l('Model'),
							_l('Year'),
							_l('Status'),
							_l('Price'),
						/*	_l('Description'), */
							_l('Created Date')
							//_l('Options')
							),'quoteRequest',[],[
                              'data-last-order-identifier' => 'requestAQuote'
                            ]); 
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php init_tail(); ?>
	<script>
		/*initDataTable('.table-quoteRequest', window.location.href, [1], [1]);*/
		initDataTable('.table-quoteRequest', window.location.href, [1], [1], '', [9, 'desc']);
		
		$(document).ready(function() {
            var table = $('#DataTables_Table_0').DataTable();
            $('#DataTables_Table_0 thead tr').clone(true).appendTo( '#DataTables_Table_0 thead' );
            $('#DataTables_Table_0 thead tr:eq(1) th').each( function (i) {
                var title = $(this).text();
                if(title == 'Created Date')
                {
                    $(this).html( '' );
                }
                else if(title == 'Price')
                {
                    $(this).html( '' );
                }
                else
                {
                    $(this).html( '<input type="text" class="form-control" placeholder="Search'+title+'" />' );   
                }
                    
         
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
				
				$( 'select', this ).on( 'change', function () {
					if ( table.column(i).search() !== this.value ) {
						table
							.column(i)
							.search( this.value )
							.draw();
					}
				} );
				
            } );
         
            var table = $('#DataTables_Table_0').DataTable( {
                orderCellsTop: true,
                fixedHeader: true,
                "order": [[ 0, "desc" ]]
            } );
        } );
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <script>
        /* Success message  */
        function successmsg(msg)
        {
            toastr.success('',msg);
        }
        
        function errormsg(msg)
        {
            toastr.error('',msg);
        }
    </script>
</body>
</html>
