<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
	    <div class="row">
	        <div class="col-md-3">
			    <div class="panel_s">
			        <div class="panel-body">
			            <h4 class="customer-profile-group-heading"><?= _l('Brand bulk upload'); ?></h4>
						<hr class="hr-panel-heading" />
    				    <div class="form-group">
					       <a href="<?= admin_url('tyre/sampledata'); ?>" class="btn btn-success">Download Sample</a>
					   </div>
					   <?= form_open_multipart(admin_url('tyre/save'));  ?>
    					   <div class="form-group">
    					       <input type="file" name="fileURL" required id="file-url" class="filestyle" data-allowed-file-extensions="[CSV, csv]" accept=".CSV, .csv" data-buttontext="Choose File">
    					   </div>
    					   <div class="form-groupt">
                                <button type="submit" class="btn btn-primary mrgT">Import</button>
                            </div>  
                        </form>
    				</div>
			    </div>
			</div>
			<div class="col-md-9">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l('Tyre Brand/Width/Profile/Diameter'); ?></h4>
						<hr class="hr-panel-heading" />
						<?= form_open(admin_url('tyre/addbrand'));  ?>
        					<div class="row form-group">
        					    <div class="col-md-6">
        					       <?= _l('Brand'); ?>
        					       <input type="text" required class="form-control" name="name">
        					   </div>
        					   <div class="col-md-6"><br>
        					       <button type="submit" class="btn btn-info">Add Brand</button>
        					   </div>
        					</div>
        				</form>
        				<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Brand'),
							_l('options')
							),'tyre_Brand'); 
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="tyrewidth">
		    <div class="col-md-3">
			    <div class="panel_s">
			        <div class="panel-body">
			            <h4 class="customer-profile-group-heading"><?= _l('Width bulk upload'); ?></h4>
						<hr class="hr-panel-heading" />
    				    <div class="form-group">
					       <a href="<?= admin_url('tyre/sampledataWidth'); ?>" class="btn btn-success">Download Sample</a>
					   </div>
					   <?= form_open_multipart(admin_url('tyre/saveWidth'));  ?>
    					   <div class="form-group">
    					       <input type="file" name="fileURL" required id="file-url" class="filestyle" data-allowed-file-extensions="[CSV, csv]" accept=".CSV, .csv" data-buttontext="Choose File">
    					   </div>
    					   <div class="form-groupt">
                                <button type="submit" class="btn btn-primary mrgT">Import</button>
                            </div>  
                        </form>
    				</div>
			    </div>
			</div>
			<div class="col-md-9">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l('Add Width'); ?></h4>
						<hr class="hr-panel-heading" />
						<?= form_open(admin_url('tyre/add'));  ?>
        					<div class="row form-group">
        					    <div class="col-md-4">
        					       <?= _l('Brand'); ?>
        					       <select class="form-control" name="brand_id" required>
        					           <option value=""></option>
        					           <?php
        					                if($tyrebrand)
        					                {
        					                    foreach($tyrebrand as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
        					                        <?php
        					                    }
        					                }
        					           ?>
        					       </select>
        					    </div>
        					    <div class="col-md-4">
        					       <?= _l('Width'); ?>
        					       <input type="text" required class="form-control" name="name">
        					   </div>
        					   <div class="col-md-4"><br>
        					       <button type="submit" class="btn btn-info">Save Width</button>
        					   </div>
        					</div>
        				</form>
        				<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Brand'),
							_l('Width'),
							_l('options')
							),'tyre_Width'); 
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="tyreprofile">
		    <div class="col-md-3">
			    <div class="panel_s">
			        <div class="panel-body">
			            <h4 class="customer-profile-group-heading"><?= _l('Profile bulk upload'); ?></h4>
						<hr class="hr-panel-heading" />
    				    <div class="form-group">
					       <a href="<?= admin_url('tyre/sampledataProfile'); ?>" class="btn btn-success">Download Sample</a>
					   </div>
					   <?= form_open_multipart(admin_url('tyre/saveProfile'));  ?>
    					   <div class="form-group">
    					       <input type="file" name="fileURL" required id="file-url" class="filestyle" data-allowed-file-extensions="[CSV, csv]" accept=".CSV, .csv" data-buttontext="Choose File">
    					   </div>
    					   <div class="form-groupt">
                                <button type="submit" class="btn btn-primary mrgT">Import</button>
                            </div>  
                        </form>
    				</div>
			    </div>
			</div>
			<div class="col-md-9">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l('Add Profile'); ?></h4>
						<hr class="hr-panel-heading" />
						<?= form_open(admin_url('tyre/addProfile'));  ?>
        					<div class="row form-group">
        					    <div class="col-md-3">
        					       <?= _l('Brand'); ?>
        					       <select class="form-control" name="brand_id" required>
        					           <option value=""></option>
        					           <?php
        					                if($tyrebrand)
        					                {
        					                    foreach($tyrebrand as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
        					                        <?php
        					                    }
        					                }
        					           ?>
        					       </select>
        					    </div>
        					    <div class="col-md-3">
        					       <?= _l('Width'); ?>
        					       <select class="form-control" name="width_id" required>
        					           <option value=""></option>
        					           <?php
        					                if($tyrewidth)
        					                {
        					                    foreach($tyrewidth as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
        					                        <?php
        					                    }
        					                }
        					           ?>
        					       </select>
        					   </div>
        					    <div class="col-md-3">
        					       <?= _l('Profile'); ?>
        					       <input type="text" required class="form-control" name="value">
        					   </div>
        					   <div class="col-md-3"><br>
        					       <button type="submit" class="btn btn-info">Update Profile</button>
        					   </div>
        					</div>
        				</form>
        				<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Width Name'),
							_l('Profile Name'),
							_l('options')
							),'tyre_Profile'); 
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="tyrediameter">
		    <div class="col-md-3">
			    <div class="panel_s">
			        <div class="panel-body">
			            <h4 class="customer-profile-group-heading"><?= _l('Diameter bulk upload'); ?></h4>
						<hr class="hr-panel-heading" />
    				    <div class="form-group">
					       <a href="<?= admin_url('tyre/sampledataDiameter'); ?>" class="btn btn-success">Download Sample</a>
					   </div>
					   <?= form_open_multipart(admin_url('tyre/saveDiameter'));  ?>
    					   <div class="form-group">
    					       <input type="file" name="fileURL" required id="file-url" class="filestyle" data-allowed-file-extensions="[CSV, csv]" accept=".CSV, .csv" data-buttontext="Choose File">
    					   </div>
    					   <div class="form-groupt">
                                <button type="submit" class="btn btn-primary mrgT">Import</button>
                            </div>  
                        </form>
    				</div>
			    </div>
			</div>
			<div class="col-md-9">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l('Add Diameter'); ?></h4>
						<hr class="hr-panel-heading" />
						<?= form_open(admin_url('tyre/addDiameter'));  ?>
        					<div class="row form-group">
        					    <div class="col-md-3">
        					       <?= _l('Brand'); ?>
        					       <select class="form-control" name="brand_id" required>
        					           <option value=""></option>
        					           <?php
        					                if($tyrebrand)
        					                {
        					                    foreach($tyrebrand as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
        					                        <?php
        					                    }
        					                }
        					           ?>
        					       </select>
        					    </div>
        					    <div class="col-md-3">
        					       <?= _l('Width'); ?>
        					       <select class="form-control" name="width_id" onchange="getprofilelist(this.value);" required>
        					           <option value=""></option>
        					           <?php
        					                if($tyrewidth)
        					                {
        					                    foreach($tyrewidth as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
        					                        <?php
        					                    }
        					                }
        					           ?>
        					       </select>
        					   </div>
        					   <div class="col-md-3">
        					       <?= _l('Profile'); ?>
        					       <select class="form-control" name="profile_id" required id="profile_id">
        					           <option value=""></option>
        					           <?php
        					                /*
        					                if($tyreprofile)
        					                {
        					                    foreach($tyreprofile as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>"><?= $rrr->value; ?></option>
        					                        <?php
        					                    }
        					                }
        					                */
        					           ?>
        					       </select>
        					   </div>
        					    <div class="col-md-2">
        					       <?= _l('Diameter'); ?>
        					       <input type="text" required class="form-control" name="diameter">
        					   </div>
        					   <div class="col-md-1"><br>
        					       <button type="submit" class="btn btn-info">Save</button>
        					   </div>
        					</div>
        				</form>
        				<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Width Name'),
							_l('Profile Name'),
							_l('Diameter'),
							_l('options')
							),'tyre_Diameter'); 
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php init_tail(); ?>
	<script>
		initDataTable('.table-tyre_Width', window.location.href, [1], [1]);
		initDataTable('.table-tyre_Profile', '<?= admin_url() ?>tyre/profiletable', [1], [1]);
		initDataTable('.table-tyre_Diameter', '<?= admin_url() ?>tyre/diametertable', [1], [1]);
		initDataTable('.table-tyre_Brand', '<?= admin_url() ?>tyre/brandtable', [1], [1]);
		
		function getprofilelist(Id)
		{
		    $('#profile_id').html('<option value="">Please waite...</option>');
		    var str = "widthid="+Id+"&"+csrfData['token_name']+"="+csrfData['hash'];
		    $.ajax({
		        url: '<?= admin_url()?>tyre/getprofilelist',
		        type: 'POST',
		        data: str,
		        datatype: 'json',
		        cache: false,
		        success: function(resp_){
		            if(resp_)
		            {
		                var resp = JSON.parse(resp_);
		                var res = '<option value=""></option>';
		                for(var i=0; i<resp.length; i++)
		                {
		                   res += '<option value="'+resp[i].id+'">'+resp[i].value+'</option>';
		                }
		                $('#profile_id').html(res);
		            }
		            else
		            {
		                $('#profile_id').html('<option value=""></option>');
		            }
		        }
		    });
		}
	</script>
</body>
</html>
