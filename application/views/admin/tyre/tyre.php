<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
	    <div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l('Tyre Brand/Width/Profile/Diameter'); ?></h4>
						<hr class="hr-panel-heading" />
						<?= form_open(admin_url('tyre/addbrand'));  ?>
        					<div class="row form-group">
        					    <div class="col-md-6">
        					       <?= _l('Brand'); ?>
        					       <input type="text" required class="form-control" name="name">
        					   </div>
        					   <div class="col-md-6"><br>
        					       <button type="submit" class="btn btn-info">Add Brand</button>
        					   </div>
        					</div>
        				</form>
        				<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Brand'),
							_l('options')
							),'tyre_Brand'); 
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="tyrewidth">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l('Update Width'); ?></h4>
						<hr class="hr-panel-heading" />
						<?= form_open(admin_url('tyre/add/'.$article->id));  ?>
        					<div class="row form-group">
        					    <div class="col-md-4">
        					       <?= _l('Brand'); ?>
        					       <select class="form-control" name="brand_id" required>
        					           <option value=""></option>
        					           <?php
        					                if($tyrebrand)
        					                {
        					                    foreach($tyrebrand as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>" <?= ($article->brand_id == $rrr->id)?"selected":""; ?>><?= $rrr->name; ?></option>
        					                        <?php
        					                    }
        					                }
        					           ?>
        					       </select>
        					   </div>
        					    <div class="col-md-4">
        					       <?= _l('Width'); ?>
        					       <input type="text" value="<?= $article->name; ?>" required class="form-control" name="name">
        					   </div>
        					   <div class="col-md-4"><br>
        					       <button type="submit" class="btn btn-info">Update Width</button>
        					   </div>
        					</div>
        				</form>
        				<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Brand'),
							_l('Width'),
							_l('options')
							),'tyre_Width'); 
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="tyreprofile">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l('Add Profile'); ?></h4>
						<hr class="hr-panel-heading" />
						<?= form_open(admin_url('tyre/addProfile'));  ?>
        					<div class="row form-group">
        					    <div class="col-md-4">
        					       <?= _l('Width'); ?>
        					       <select class="form-control" name="width_id" required>
        					           <option value=""></option>
        					           <?php
        					                if($tyrewidth)
        					                {
        					                    foreach($tyrewidth as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
        					                        <?php
        					                    }
        					                }
        					           ?>
        					       </select>
        					   </div>
        					    <div class="col-md-4">
        					       <?= _l('Profile'); ?>
        					       <input type="text" required class="form-control" name="value">
        					   </div>
        					   <div class="col-md-4"><br>
        					       <button type="submit" class="btn btn-info">Update Profile</button>
        					   </div>
        					</div>
        				</form>
        				<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Width Name'),
							_l('Profile Name'),
							_l('options')
							),'tyre_Profile'); 
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="tyrediameter">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l('Add Diameter'); ?></h4>
						<hr class="hr-panel-heading" />
						<?= form_open(admin_url('tyre/addDiameter'));  ?>
        					<div class="row form-group">
        					    <div class="col-md-3">
        					       <?= _l('Width'); ?>
        					       <select class="form-control" name="width_id" required>
        					           <option value=""></option>
        					           <?php
        					                if($tyrewidth)
        					                {
        					                    foreach($tyrewidth as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>"><?= $rrr->name; ?></option>
        					                        <?php
        					                    }
        					                }
        					           ?>
        					       </select>
        					   </div>
        					   <div class="col-md-3">
        					       <?= _l('Profile'); ?>
        					       <select class="form-control" name="profile_id" required>
        					           <option value=""></option>
        					           <?php
        					                if($tyreprofile)
        					                {
        					                    foreach($tyreprofile as $rrr)
        					                    {
        					                        ?>
        					                            <option value="<?= $rrr->id; ?>"><?= $rrr->value; ?></option>
        					                        <?php
        					                    }
        					                }
        					           ?>
        					       </select>
        					   </div>
        					    <div class="col-md-3">
        					       <?= _l('Diameter'); ?>
        					       <input type="text" required class="form-control" name="diameter">
        					   </div>
        					   <div class="col-md-3"><br>
        					       <button type="submit" class="btn btn-info">Save Diameter</button>
        					   </div>
        					</div>
        				</form>
        				<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Width Name'),
							_l('Profile Name'),
							_l('Diameter'),
							_l('options')
							),'tyre_Diameter'); 
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php init_tail(); ?>
	<script>
		initDataTable('.table-tyre_Width', window.location.href, [1], [1]);
		initDataTable('.table-tyre_Profile', '<?= admin_url() ?>tyre/profiletable', [1], [1]);
		initDataTable('.table-tyre_Diameter', '<?= admin_url() ?>tyre/diametertable', [1], [1]);
		initDataTable('.table-tyre_Brand', '<?= admin_url() ?>tyre/brandtable', [1], [1]);
	</script>
</body>
</html>
