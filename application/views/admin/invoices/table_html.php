<?php defined('BASEPATH') or exit('No direct script access allowed');

$table_data = array(
  _l('ID'),
  _l('Item'),
  _l('Amount'),
  _l('Status'),
  _l('Payment Date'),
  _l('Next Billing Date'));
  
  
$table_data = hooks()->apply_filters('invoices_table_columns', $table_data);
render_datatable($table_data, (isset($class) ? $class : 'invoices'));
?>
