<?php

defined('BASEPATH') or exit('No direct script access allowed');
$CI = &get_instance();
$aColumns = [
    'make_id',
    'model_id',
    'year_id',
    'varient',
    ];

$sIndexColumn = 'id';
$sTable       = db_prefix().'car_varient';

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], ['id']);
$output  = $result['output'];
$rResult = $result['rResult'];
$sn = 1;
foreach ($rResult as $aRow) {
    $row = [];
    $row[] = $CI->db->get_where(db_prefix().'car_make', array('id' => $aRow['make_id']))->row('name');
    $row[] = $CI->db->get_where(db_prefix().'car_model', array('id' => $aRow['model_id']))->row('name');
    $row[] = $CI->db->get_where(db_prefix().'car_year', array('id' => $aRow['year_id']))->row('year');
    $row[] = $aRow['varient'];
    //$row[] = '<a href="' . admin_url('car/edidVarient/' . $aRow['id']) . '#caryear" class="mbot10 display-block">' . $aRow['varient'] . '</a>';
    $options = icon_btn('car/edidVarient/' . $aRow['id'], 'pencil-square-o');
    $row[]   = $options .= icon_btn('car/deleteVarient/' . $aRow['id'], 'remove', 'btn-danger _delete');
    $output['aaData'][] = $row;
}
