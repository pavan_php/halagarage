<?php

defined('BASEPATH') or exit('No direct script access allowed');
$CI = &get_instance();
$aColumns = [
    'diameter',
    'width_id',
    'profile_id',
    ];

$sIndexColumn = 'id';
$sTable       = db_prefix().'tyre_diameter';

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], ['id']);
$output  = $result['output'];
$rResult = $result['rResult'];
$sn = 1;
foreach ($rResult as $aRow) {
    $row = [];
    $row[] = $CI->db->get_where(db_prefix().'tyre_width', array('id' => $aRow['width_id']))->row('name');
    $row[] = $CI->db->get_where(db_prefix().'tyre_profile', array('id' => $aRow['profile_id']))->row('value');
    $row[] = '<a href="' . admin_url('tyre/adddiameter/' . $aRow['id']) . '#tyrediameter" class="mbot10 display-block">' . $aRow['diameter'] . '</a>';
    $options = icon_btn('tyre/adddiameter/' . $aRow['id'].'#tyrediameter', 'pencil-square-o');
    $row[]   = $options .= icon_btn('tyre/delete_tyrediameter/' . $aRow['id'], 'remove', 'btn-danger _delete');

    $output['aaData'][] = $row;
}
