<?php
//error_reporting(-1); ini_set('display_errors', 1);
defined('BASEPATH') or exit('No direct script access allowed');
$CI = &get_instance();
$aColumns = [
    'name',
    'country_id',
    'city_id',
    ];

$sIndexColumn = 'id';
$sTable       = db_prefix().'area';

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], ['id']);
$output  = $result['output'];
$rResult = $result['rResult'];
$sn = 1;
foreach ($rResult as $aRow) {
    $row = [];
    $row[] = $CI->db->get_where(db_prefix().'country', array('id' => $aRow['country_id']))->row('name');
    $row[] = $CI->db->get_where(db_prefix().'city', array('id' => $aRow['city_id']))->row('name');
    $row[] = '<a href="' . admin_url('location/addArea/' . $aRow['id']) . '#locationyear" class="mbot10 display-block">' . $aRow['name'] . '</a>';
    $options = icon_btn('location/addArea/' . $aRow['id'].'#locationyear', 'pencil-square-o');
    $row[]   = $options .= icon_btn('location/delete_locationArea/' . $aRow['id'], 'remove', 'btn-danger _delete');

    $output['aaData'][] = $row;
}
