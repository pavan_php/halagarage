<?php

defined('BASEPATH') or exit('No direct script access allowed');
$CI = &get_instance();
$aColumns = [
    'name',
    'brand_id',
    ];

$sIndexColumn = 'id';
$sTable       = db_prefix().'tyre_width';

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], ['id']);
$output  = $result['output'];
$rResult = $result['rResult'];
$sn = 1;
foreach ($rResult as $aRow) {
    $row = [];
    $row[] = $CI->db->get_where(db_prefix().'tyre_brand', array('id' => $aRow['brand_id']))->row('name');
    $row[] = '<a href="' . admin_url('tyre/add/' . $aRow['id']) . '#tyrewidth" class="mbot10 display-block">' . $aRow['name'] . '</a>';
    $options = icon_btn('tyre/add/' . $aRow['id'].'#tyrewidth', 'pencil-square-o');
    $row[]   = $options .= icon_btn('tyre/delete_tyrewidth/' . $aRow['id'], 'remove', 'btn-danger _delete');

    $output['aaData'][] = $row;
}
