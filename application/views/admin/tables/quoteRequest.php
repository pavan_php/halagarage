<?php

defined('BASEPATH') or exit('No direct script access allowed');
$CI = & get_instance();
$aColumns = [
    'id',
    'userid',
    'title',
    'locationid',
    'categoryid',
    'vehiclecategory',
    'brandid',
    'carmodelid',
    'carmakeyearid',
    'registrationcard',
    'description',
    'status',
    'created_date',
    ];
$nosWhere = ''; 
$sIndexColumn = 'id';
$sTable       = db_prefix().'requestAQuote';
//$this->ci->db->order_by('id', 'desc');
$where = [];

$name  = $_POST['columns'][0]['search']['value'];
if ($name) {
      array_push($where, 'AND '.db_prefix().'requestAQuote.userid IN (SELECT userid FROM '.db_prefix().'contacts WHERE firstname like "%'.$name.'%" or lastname like "%'.$name.'%")');
     $nosWhere  = 1; 
}


$title  = $_POST['columns'][1]['search']['value'];
if ($title) {
      array_push($where, 'AND '.db_prefix().'requestAQuote.title  like "%'.$title.'%"');
     $nosWhere  = 1; 
}


$location  = $_POST['columns'][2]['search']['value'];
if ($location) {
      array_push($where, 'AND '.db_prefix().'requestAQuote.locationid IN (SELECT id FROM '.db_prefix().'city WHERE name like "%'.$location.'%")');
     $nosWhere  = 1; 
}

$category  = $_POST['columns'][3]['search']['value'];
 if ($category) {
    
     array_push($where, 'AND '.db_prefix().'requestAQuote.categoryid IN (SELECT id FROM '.db_prefix().'category WHERE name like "%'.$category.'%")');
     $nosWhere  = 1;
    
} 

$vehiclecategory  = $_POST['columns'][4]['search']['value'];
 if ($vehiclecategory) {
    
     array_push($where, 'AND '.db_prefix().'requestAQuote.vehiclecategory like "%'.$vehiclecategory.'%"');
     $nosWhere  = 1;
    
} 

$brand  = $_POST['columns'][5]['search']['value'];
if ($brand) {
      array_push($where, 'AND '.db_prefix().'requestAQuote.brandid IN (SELECT id FROM '.db_prefix().'car_make WHERE name like "%'.$brand.'%")');
     $nosWhere  = 1; 
}
 
$carmodelid  = $_POST['columns'][6]['search']['value'];
if ($carmodelid) {
      array_push($where, 'AND '.db_prefix().'requestAQuote.carmodelid IN (SELECT id FROM '.db_prefix().'car_model WHERE name like "%'.$carmodelid.'%")');
     $nosWhere  = 1; 
}


$years  = $_POST['columns'][7]['search']['value'];
if ($years) {
      array_push($where, 'AND '.db_prefix().'requestAQuote.carmakeyearid IN (SELECT id FROM '.db_prefix().'car_year WHERE year like "%'.$years.'%")');
     $nosWhere  = 1; 
}

//array_push($where, 'WHERE receiver_id=0');

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], $where, ['id','vehiclecategory','carmakeyearid','created_date'],'','',$nosWhere);


$output  = $result['output'];
$rResult = $result['rResult'];
$sn = 1;
foreach ($rResult as $aRow) {
    $row = [];
    
    $row[] = customername($aRow['userid']);
    $row[] = $aRow['title'];
    $row[] = locationname($aRow['locationid']);
    $row[] = categoryname($aRow['categoryid']);
    $row[] = $aRow['vehiclecategory'];
    $row[] = makename($aRow['brandid']);
    $row[] = modelname($aRow['carmodelid']);
    $row[] = yearname($aRow['carmakeyearid']);
   // $row[] = $aRow['description'];
   $garagerep = $CI->db->get_where(db_prefix().'quoteRequest_garageReply', array('quote_id' => $aRow['id'],'garage_confirm' => 1 ))->row();
   if($garagerep)
   {
       $row[] = '<span class="label label-success">Success</span>';
	   $row[] = $garagerep->price;
   }
   else
   {
       $row[] = '<span class="label label-warning">Pending</span>';
	   $row[] = '';
   }
    $row[] = '<span style="display:none;">'.strtotime($aRow['created_date']).'</span>'.$aRow['created_date'];
    //$options = icon_btn('car/add/' . $aRow['id'], 'pencil-square-o');
    //$row[]   = $options .= icon_btn('car/delete_carmake/' . $aRow['id'], 'remove', 'btn-danger _delete');

    $output['aaData'][] = $row;
}
