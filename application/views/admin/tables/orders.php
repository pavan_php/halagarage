<?php

defined('BASEPATH') or exit('No direct script access allowed');
$CI          = & get_instance();

$aColumns = [
    'id',
    'quote_id',
    'product_id',
    'customer_id',
    'garage_id',
    'appointment_date',
    'payment_status',
    'pickup_address',
    'delivery_address',
    'status',
    'created_date',
    'updated_date',
    ];
    
$nosWhere = ''; 
$sIndexColumn = 'id';
$sTable       = db_prefix().'schedule_appoinment';

$where = [];

$name  = $_POST['columns'][1]['search']['value'];
if ($name) {

      array_push($where, 'AND '.db_prefix().'schedule_appoinment.customer_id IN (SELECT userid FROM '.db_prefix().'contacts WHERE firstname like "%'.$name.'%" or lastname like "%'.$name.'%")');
     $nosWhere  = 1; 
}

$gname  = $_POST['columns'][2]['search']['value'];
if ($gname) {

      array_push($where, 'AND '.db_prefix().'schedule_appoinment.garage_id IN (SELECT userid FROM '.db_prefix().'clients WHERE company like "%'.$gname.'%")');
     $nosWhere  = 1; 
}

$pname  = $_POST['columns'][3]['search']['value'];
if ($pname) {

      array_push($where, 'AND '.db_prefix().'schedule_appoinment.quote_id IN (SELECT id FROM '.db_prefix().'requestAQuote WHERE title like "%'.$pname.'%")');
      //array_push($where, 'AND '.db_prefix().'schedule_appoinment.product_id IN (SELECT id FROM '.db_prefix().'_product WHERE product_name like "%'.$pname.'%")');
     $nosWhere  = 1; 
}
 
$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], $where, [],'','',$nosWhere);
$output  = $result['output'];
$rResult = $result['rResult'];
$sn = 1;
foreach ($rResult as $aRow) {
    $row = [];
    $row[] = $sn++;
    $customername = $this->ci->db->select('firstname,lastname')->get_where(db_prefix().'contacts', array('userid' => $aRow['customer_id']))->row();
    $row[] = $customername->firstname.' '.$customername->lastname;
    $row[] = $this->ci->db->get_where(db_prefix().'clients', array('userid' => $aRow['garage_id']))->row('company');
    if($aRow['quote_id'] > 0)
    {
        $price_ = $this->ci->db->get_where(db_prefix().'quoteRequest_garageReply', array('quote_id' => $aRow['quote_id']))->row('price');
        $quoteDetails = $this->ci->db->select('id,title,categoryid,subcategoryid')->get_where(db_prefix().'requestAQuote', array('id' => $aRow['quote_id']))->row();
        $row[] = $quoteDetails->title;
        $row[] = $price_.' '.CURRENCY_NAME;
        $row[] = categoryname($quoteDetails->categoryid);
        $row[] = categoryname($quoteDetails->subcategoryid);
    }
    else
    {
        $productDetails = $this->ci->db->select('id,price_with_fitting,product_name,category,subcategory,car_category')->get_where(db_prefix().'_product', array('id' => $aRow['product_id']))->row();
        $row[] = $productDetails->product_name;
        $row[] = $productDetails->price_with_fitting.' '.CURRENCY_NAME;
        $subcatarr = explode(',', $productDetails->subcategory);
        $subcat = '';
        if($subcatarr)
        {
            foreach($subcatarr as $rr)
            {
                $subcat .= categoryname($rr).', ';
            }
            //echo $subcat;
        }
        
        $row[] = categoryname($productDetails->category);
        $row[] = $subcat;
    }
    $row[] = date('d M Y', $aRow['appointment_date']);
    $row[] = _dt($aRow['created_date']);
    $output['aaData'][] = $row;
}
