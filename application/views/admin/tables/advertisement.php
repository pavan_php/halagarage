<?php
defined('BASEPATH') or exit('No direct script access allowed');
$CI          = & get_instance();
$aColumns = [
    'id',
    'title',
    'start_date',
    'end_date',
    'location',
    ];

$sIndexColumn = 'id';
$sTable       = db_prefix().'advertisement';
$where        = [];


$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], $where, ['id']);
$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];
    
    $attachment_key = $CI->db->get_where(db_prefix().'files', array('rel_id' => $aRow['id'], 'rel_type' => "advertisement"))->row();
    $row[] = '<img src="'.site_url('uploads/advertisement/'.$aRow['id'].'/'. $attachment_key->file_name).'" width="50" height="50" alt="">';
    $row[] = $aRow['title'];
    $row[] = date('d-M-Y', strtotime($aRow['start_date']));
    $row[] = date('d-M-Y', strtotime($aRow['end_date']));
    $row[] = ucwords($aRow['location']);
    $options = icon_btn('advertisement/add/' . $aRow['id'], 'pencil-square-o');
    $row[]   = $options .= icon_btn('advertisement/delete_advertisement/' . $aRow['id'], 'remove', 'btn-danger _delete');

    $output['aaData'][] = $row;
}
