<?php

defined('BASEPATH') or exit('No direct script access allowed');
$CI = & get_instance();
$aColumns = [
    'id',
    'sender_id',
    'receiver_id',
    'title',
    'token_id',
    'message',
    'createddate',
    ];

$sIndexColumn = 'id';
$sTable       = db_prefix().'message';
//$CI->db->order_by('id', 'desc');
$where = [];

array_push($where, 'WHERE sender_id=0');

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], $where, ['id']);
$output  = $result['output'];
$rResult = $result['rResult'];
$sn = 1;
foreach ($rResult as $aRow) {
    $row = [];
    
    $row[] = customername($aRow['receiver_id']);
    $row[] = $aRow['title'];
    $row[] = $aRow['message'];
    $row[] = $aRow['createddate'];
    //$row[] = '<i class="fa fa-reply fa-2x" style="cursor:pointer" data-toggle="modal" onClick="replymsg('.$aRow['sender_id'].','.$aRow['token_id'].')" data-target="#replyModal" aria-hidden="true"></i>';

    $output['aaData'][] = $row;
}
