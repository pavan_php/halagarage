<?php
defined('BASEPATH') or exit('No direct script access allowed');
$CI          = & get_instance();
$aColumns = [
        'payment_id',
        'garage_id',
        'item_number',
        'payment_gross',
        'currency_code',
        'expiry_date',
        'created_date',
    ];

$sIndexColumn = 'payment_id';
$sTable       = db_prefix().'_payments';
$where        = [];
array_push($where, 'AND (item_number = 2)');

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], $where, ['payment_id']);
$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];
    
    $row[] = $CI->db->get_where(db_prefix().'clients', array('userid' => $aRow['garage_id']))->row('company');
    $row[] = $CI->db->get_where(db_prefix().'clients', array('userid' => $aRow['garage_id']))->row('company_email');
    $row[] = $aRow['payment_gross'].' '.CURRENCY_NAME;
    $row[] = _d($aRow['created_date']);
    $row[] = date('d-M-Y', $aRow['expiry_date']);
    if($aRow['expiry_date'] > time())
    {
        $row[] = '<span class="label label-success">Active</span>';
    }
    else
    {
        $row[] = '<span class="label label-warning">Inactive</span>';
    }
    
    $output['aaData'][] = $row;
}
