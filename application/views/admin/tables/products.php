<?php

defined('BASEPATH') or exit('No direct script access allowed');
$CI          = & get_instance();

$aColumns = [
    'id',
    'garage_id',
    'product_name',
    'category',
    'subcategory',
    'car_category',
    'car_brand',
    'car_model',
    'make_year',
    'tyre_brand',
    'width',
    'height',
    'rim_size',
    'product_brand',
    'description',
    'fitting_filter',
    'garage_id',
    'status',
    'created_at',
    ];

$sIndexColumn = 'id';
$sTable       = db_prefix().'_product';

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], ['id']);
$output  = $result['output'];
$rResult = $result['rResult'];
$sn = 1;
foreach ($rResult as $aRow) {
    $row = [];
    $subcatarr = explode(',', $aRow['subcategory']);
    $subcat = '';;
    if($subcatarr)
    {
        foreach($subcatarr as $rr)
        {
            $subcat .= categoryname($rr).', ';
        }
        //echo $subcat;
    }
    
    $row[] = $aRow['id'];
    $garagedetails = $CI->db->select('firstname,lastname')->get_where(db_prefix().'contacts', array('userid' => $aRow['garage_id']))->row();
    $row[] = $garagedetails->firstname.' '.$garagedetails->lastname;
    $row[] = $aRow['product_name'];
    $row[] = categoryname($aRow['category']);
    $row[] = $subcat;
    $row[] = $CI->db->get_where(db_prefix().'clients', array('userid' => $aRow['garage_id']))->row('address');
    if($aRow['fitting_filter'] == 1)
    {
        $row[] = 'Yes';
    }
    else
    {
        $row[] = 'No';
    }
    $status = '';
    if($aRow['status'] == 1)
    {
        $status = 'Published';
    }
    else
    {
        $status = 'Draft';
    }
    $row[] = $status;
    $row[] = _d($aRow['created_at']);

    $output['aaData'][] = $row;
}
