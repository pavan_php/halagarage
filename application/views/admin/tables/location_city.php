<?php

defined('BASEPATH') or exit('No direct script access allowed');
$CI = &get_instance();
$aColumns = [
    'name',
    'country_id',
    ];

$sIndexColumn = 'id';
$sTable       = db_prefix().'city';

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], ['id']);
$output  = $result['output'];
$rResult = $result['rResult'];
$sn = 1;
foreach ($rResult as $aRow) {
    $row = [];
    $row[] = $CI->db->get_where(db_prefix().'country', array('id' => $aRow['country_id']))->row('name');
    $row[] = '<a href="' . admin_url('location/addCity/' . $aRow['id']) . '#locationmodel" class="mbot10 display-block">' . $aRow['name'] . '</a>';
    $options = icon_btn('location/addCity/' . $aRow['id'].'#locationmodel', 'pencil-square-o');
    $row[]   = $options .= icon_btn('location/delete_locationCity/' . $aRow['id'], 'remove', 'btn-danger _delete');

    $output['aaData'][] = $row;
}
