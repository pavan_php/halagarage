<?php

defined('BASEPATH') or exit('No direct script access allowed');
$CI = &get_instance();
$aColumns = [
    'name',
    'make_id',
    ];

$sIndexColumn = 'id';
$sTable       = db_prefix().'car_model';

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], ['id']);
$output  = $result['output'];
$rResult = $result['rResult'];
$sn = 1;
foreach ($rResult as $aRow) {
    $row = [];
    $row[] = $CI->db->get_where(db_prefix().'car_make', array('id' => $aRow['make_id']))->row('name');
    $row[] = '<a href="' . admin_url('car/addmodel/' . $aRow['id']) . '#carmodel" class="mbot10 display-block">' . $aRow['name'] . '</a>';
    $options = icon_btn('car/addmodel/' . $aRow['id'].'#carmodel', 'pencil-square-o');
    $row[]   = $options .= icon_btn('car/delete_carmodel/' . $aRow['id'], 'remove', 'btn-danger _delete');

    $output['aaData'][] = $row;
}
