<?php

    defined('BASEPATH') or exit('No direct script access allowed');
    $garageid = $this->ci->uri->segment(4);
    $aColumns = [
            'id',
            'quote_id',
            'customer_id',
            'product_id',
            'garage_id',
            'appointment_date',
            'status',
            'updated_date',
        ];
    
    $sIndexColumn = 'id';
    $sTable       = db_prefix().'schedule_appoinment';
    $where = [];
    $join = [];
    if ($garageid) {
        array_push($where, 'AND garage_id=' . $garageid);
    }
    array_push($where, 'AND status=1');
    $result  = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, ['id']);
    
    $output  = $result['output'];
    $rResult = $result['rResult'];
    $sn = 1;
    foreach ($rResult as $aRow) {
        $row = [];
        $row[] = $sn++;
        
        $row[] = $this->ci->db->get_where(db_prefix().'clients', array('userid' => $aRow['garage_id']))->row('company');
        if($aRow['quote_id'] > 0)
        {
            $price_ = $this->ci->db->get_where(db_prefix().'quoteRequest_garageReply', array('quote_id' => $aRow['quote_id']))->row('price');
            $quoteDetails = $this->ci->db->select('id,title,categoryid,subcategoryid')->get_where(db_prefix().'requestAQuote', array('id' => $aRow['quote_id']))->row();
            $row[] = $quoteDetails->title;
            $row[] = $price_.' '.CURRENCY_NAME;
            //$row[] = categoryname($quoteDetails->categoryid);
            //$row[] = categoryname($quoteDetails->subcategoryid);
        }
        else
        {
            $productDetails = $this->ci->db->select('id,price_with_fitting,product_name,category,subcategory,car_category')->get_where(db_prefix().'_product', array('id' => $aRow['product_id']))->row();
            $row[] = $productDetails->product_name;
            $row[] = $productDetails->price_with_fitting.' '.CURRENCY_NAME;
            /*
            $subcatarr = explode(',', $productDetails->subcategory);
            $subcat = '';
            if($subcatarr)
            {
                foreach($subcatarr as $rr)
                {
                    $subcat .= categoryname($rr).', ';
                }
                //echo $subcat;
            }
            
            $row[] = categoryname($productDetails->category);
            $row[] = $subcat;
            */
        }
       
       
       
        $row[] = date('Y-m-d',$aRow['appointment_date']);
        
        $row[] = _dt($aRow['updated_date']);
        
        $output['aaData'][] = $row;
    }