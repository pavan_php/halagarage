<?php

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    'name',
    ];

$sIndexColumn = 'id';
$sTable       = db_prefix().'tyre_brand';

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], ['id']);
$output  = $result['output'];
$rResult = $result['rResult'];
$sn = 1;
foreach ($rResult as $aRow) {
    $row = [];
    $row[] = '<a href="' . admin_url('tyre/addbrand/' . $aRow['id']) . '" class="mbot10 display-block">' . $aRow['name'] . '</a>';
    $options = icon_btn('tyre/addbrand/' . $aRow['id'], 'pencil-square-o');
    $row[]   = $options .= icon_btn('tyre/delete_tyrebrand/' . $aRow['id'], 'remove', 'btn-danger _delete');

    $output['aaData'][] = $row;
}
