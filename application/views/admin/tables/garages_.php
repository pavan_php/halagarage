<?php
defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    'email',
    'name',
    'fax',
    'company_email',
    'website_url',
    'city_emirate',
    'address',
    'status',
    ];

$sIndexColumn = 'id';
$sTable       = db_prefix().'garage';

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], ['id']);
$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];
    $company = $aRow['name'];
    
    $company = '<a href="' . admin_url('garages/garage/' . $aRow['id']) . '">' . $company . '</a>';

    $company .= '<div class="row-options">';
    $company .= '<a href="' . admin_url('garages/add/' . $aRow['id']) . '">' . _l('Edit') . '</a>';

    $company .= ' | <a href="' . admin_url('garages/delete_garages/' . $aRow['id']) . '" class="text-danger _delete">' . _l('delete') . '</a>';

    $company .= '</div>';

    $row[] = $company;
    
    $row[] = $aRow['email'];
    $row[] = $aRow['fax'];
    $row[] = $aRow['company_email'];
    $row[] = $aRow['website_url'];
    $row[] = $aRow['city_emirate'];
    $row[] = $aRow['address'];
    
    //$options = icon_btn('garages/add/' . $aRow['id'], 'pencil-square-o');
    $options = '';
    //$row[]   = $options .= icon_btn('garages/delete_garages/' . $aRow['id'], 'remove', 'btn-danger _delete');

    $toggleActive = '<div class="onoffswitch" data-toggle="tooltip" data-title="' . _l('customer_active_inactive_help') . '">
    <input type="checkbox" data-switch-url="' . admin_url() . 'garages/change_client_status" name="onoffswitch" class="onoffswitch-checkbox" id="' . $aRow['id'] . '" data-id="' . $aRow['id'] . '" ' . ($aRow['status'] == 1 ? 'checked' : '') . '>
    <label class="onoffswitch-label" for="' . $aRow['id'] . '"></label>
    </div>';

    // For exporting
    $toggleActive .= '<span class="hide">' . ($aRow['status'] == 1 ? _l('is_active_export') : _l('is_not_active_export')) . '</span>';

    $row[] = $toggleActive;

    $output['aaData'][] = $row;
}
