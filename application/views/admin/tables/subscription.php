<?php
defined('BASEPATH') or exit('No direct script access allowed');
$CI          = & get_instance();
$aColumns = [
    'id',
    'title',
    'qty',
    'day_limit',
    'description',
    'price',
    'status',
    'createddate',
    ];

$sIndexColumn = 'id';
$sTable       = db_prefix().'subscription';

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], ['id']);
$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];
    
    $row[] = $aRow['title'];
    $row[] = $aRow['day_limit'];
    $row[] = $aRow['price'].' '.CURRENCY_NAME;
    $row[] = $aRow['description'];
    $row[] = _d($aRow['createddate']);
    $options = '';
    if($aRow['status'] == 1)
    {
        $options = '<a href="javascript:void(0)" class="btn btn-success btn-icon"><input type="checkbox" id="status_'.$aRow['id'].'" onclick="setSubscriptionStatus('.$aRow['id'].',0)" checked></a>';
    }
    else
    {
        $options = '<a href="javascript:void(0)" class="btn btn-success btn-icon"><input type="checkbox" id="status_'.$aRow['id'].'" onclick="setSubscriptionStatus('.$aRow['id'].',1)"></a>';
    }
    
    $row[] = $options .= icon_btn('subscription/add/' . $aRow['id'], 'pencil-square-o');
    //$row[]   = $options .= icon_btn('subscription/delete_subscription/' . $aRow['id'], 'remove', 'btn-danger _delete');

    $output['aaData'][] = $row;
}
