<?php

defined('BASEPATH') or exit('No direct script access allowed');
$CI = &get_instance();
$aColumns = [
    'year',
    'make_id',
    'model_id',
    ];

$sIndexColumn = 'id';
$sTable       = db_prefix().'car_year';

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], ['id']);
$output  = $result['output'];
$rResult = $result['rResult'];
$sn = 1;
foreach ($rResult as $aRow) {
    $row = [];
    $row[] = $CI->db->get_where(db_prefix().'car_make', array('id' => $aRow['make_id']))->row('name');
    $row[] = $CI->db->get_where(db_prefix().'car_model', array('id' => $aRow['model_id']))->row('name');
    $row[] = '<a href="' . admin_url('car/addyear/' . $aRow['id']) . '#caryear" class="mbot10 display-block">' . $aRow['year'] . '</a>';
    $options = icon_btn('car/addyear/' . $aRow['id'].'#caryear', 'pencil-square-o');
    $row[]   = $options .= icon_btn('car/delete_caryear/' . $aRow['id'], 'remove', 'btn-danger _delete');

    $output['aaData'][] = $row;
}
