<?php
defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
        'id',
        'name',
        'email',
        'phone',
        'subject',
        'message',
        'updated_date',
    ];

$sIndexColumn = 'id';
$sTable       = db_prefix().'contact_us';

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], ['id']);
$output  = $result['output'];
$rResult = $result['rResult'];
$sn = 1;
foreach ($rResult as $aRow) {
    $row = [];
    $row[] = $sn++;
    $row[] = $aRow['name'];
    $row[] = $aRow['email'];
    $row[] = $aRow['phone'];
    $row[] = $aRow['subject'];
    $row[] = $aRow['message'];
    $row[] = _d($aRow['updated_date']);
    $row[]   = icon_btn('lead/delete_lead/' . $aRow['id'], 'remove', 'btn-danger _delete');

    $output['aaData'][] = $row;
}
