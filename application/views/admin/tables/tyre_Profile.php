<?php

defined('BASEPATH') or exit('No direct script access allowed');
$CI = &get_instance();
$aColumns = [
    'value',
    'width_id',
    ];

$sIndexColumn = 'id';
$sTable       = db_prefix().'tyre_profile';

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], ['id']);
$output  = $result['output'];
$rResult = $result['rResult'];
$sn = 1;
foreach ($rResult as $aRow) {
    $row = [];
    $row[] = $CI->db->get_where(db_prefix().'tyre_width', array('id' => $aRow['width_id']))->row('name');
    $row[] = '<a href="' . admin_url('tyre/addprofile/' . $aRow['id']) . '#tyreprofile" class="mbot10 display-block">' . $aRow['value'] . '</a>';
    $options = icon_btn('tyre/addprofile/' . $aRow['id'].'#tyreprofile', 'pencil-square-o');
    $row[]   = $options .= icon_btn('tyre/delete_tyreprofile/' . $aRow['id'], 'remove', 'btn-danger _delete');

    $output['aaData'][] = $row;
}
