<?php

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    'email',
    'createddate',
    ];

$sIndexColumn = 'id';
$sTable       = db_prefix().'newsletter';

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], ['id']);
$output  = $result['output'];
$rResult = $result['rResult'];
$sn = 1;
foreach ($rResult as $aRow) {
    $row = [];
    
    $row[] = $aRow['email'];
    $row[] = _d($aRow['createddate']);

    $output['aaData'][] = $row;
}
