<?php

    defined('BASEPATH') or exit('No direct script access allowed');
    $garageid = $this->ci->uri->segment(4);
    $aColumns = [
            'payment_id',
            'garage_id',
            'item_number',
            'txn_id',
            'payment_gross',
            'expiry_date',
            'created_date',
        ];
    
    $sIndexColumn = 'payment_id';
    $sTable       = db_prefix().'_payments';
    $where = [];
    $join = [];
    if ($garageid) {
        array_push($where, 'AND garage_id=' . $garageid);
    }
    $result  = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, ['payment_id']);
    
    $output  = $result['output'];
    $rResult = $result['rResult'];
    
    foreach ($rResult as $aRow) {
        $row = [];

        ///$row[] = '<a href="'.admin_url().'clients/pdf/'.$aRow['payment_id'].'" target="_blank">'.$aRow['payment_id'].'</a>';
        $row[] = '<a href="#" target="_blank">'.$aRow['payment_id'].'</a>';
        $row[] = $this->ci->db->get_where(db_prefix().'subscription', array('id' => $aRow['item_number']))->row('title');
        $row[] = $aRow['payment_gross'];
        $row[] = '<span class="label label-success s-status invoice-status-4">Paid</span>';
        $row[] = _d($aRow['created_date']);
        $row[] = _d(date('Y-m-d',$aRow['expiry_date']));
    
        $output['aaData'][] = $row;
    }