<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <?php echo form_open_multipart($this->uri->uri_string(),array('id'=>'article-form')); ?>
      <div class="row">
         <div class="col-md-8 col-md-offset-2">
            <div class="panel_s">
               <div class="panel-body">
                  <h4 class="no-margin">
                     <?php echo $title; ?>
                  </h4>
                    <hr class="hr-panel-heading" />
                    <div class="clearfix"></div>
                    <?php $value = (isset($article) ? $article->name : ''); ?>
                    <?php $attrs = (isset($article) ? array() : array('autofocus'=>true)); ?>
                    <?php echo render_input('name','Name',$value,'text',$attrs); ?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Account Email</label>
                                <input type="email" class="form-control" value="<?= (isset($article) ? $article->email : '')?>" name="email">
                            </div>
                        </div>
                        <?php
                            if(isset($article))
                            {}
                            else
                            {
                                ?>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <label>Password</label>
                                                <input type="password" class="form-control password" name="password" autocomplete="off">
                                                <span class="input-group-addon"><label></label>
                                                    <a href="#password" class="show_password" onclick="showPassword('password'); return false;"><i class="fa fa-eye"></i></a>
                                                </span>
                                                <span class="input-group-addon"><label></label>
                                                    <a href="#" class="generate_password" onclick="generatePassword(this);return false;"><i class="fa fa-refresh"></i></a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                            }
                        ?>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Fax</label>
                                <input type="text" class="form-control" value="<?= (isset($article) ? $article->fax : '')?>" name="fax">
                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <label>Company Email</label>
                                <input type="email" class="form-control" value="<?= (isset($article) ? $article->company_email : '')?>" name="company_email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Website URL</label>
                                <input type="url" class="form-control" value="<?= (isset($article) ? $article->website_url : '')?>" name="website_url">
                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <label>City/Emirate</label>
                                <input type="text" class="form-control" value="<?= (isset($article) ? $article->city_emirate : '')?>" name="city_emirate">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" value="<?= (isset($article) ? $article->address : '')?>" name="address">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Logo</label>
                                <?php
                                    if(isset($article))
                                    {
                                        $imageArray = $this->db->get_where(db_prefix().'files', array('rel_id' => $article->id, 'rel_type' => "garageslogo"))->row();
                                        ?>
                                            <img src="<?= base_url('download/file/taskattachment/'. $imageArray->attachment_key);?>" width="100" height="100" alt="">
                                        <?php
                                    }
                                ?>
                                <input type="file" name="logo">
                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <label>Image</label>
                                <?php
                                    if(isset($article))
                                    {
                                        $imageArray = $this->db->get_where(db_prefix().'files', array('rel_id' => $article->id, 'rel_type' => "garagesimage"))->row();
                                        ?>
                                            <img src="<?= base_url('download/file/taskattachment/'. $imageArray->attachment_key);?>" width="100" height="100" alt="">
                                        <?php
                                    }
                                ?>
                                <input type="file" name="image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         </div>
         <?php if((has_permission('garages','','create') && !isset($article)) || has_permission('garages','','edit') && isset($article)){ ?>
         <div class="btn-bottom-toolbar btn-toolbar-container-out text-right">
            <button type="submit" class="btn btn-info pull-right"><?php echo _l('submit'); ?></button>
         </div>
         <?php } ?>
      </div>
      <?php echo form_close(); ?>
   </div>
</div>
<?php init_tail(); ?>
</body>
</html>
