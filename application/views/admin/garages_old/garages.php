<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<div class="_buttons">
							<a href="<?php echo admin_url('garages/add'); ?>" class="btn btn-info pull-left display-block"><?php echo _l('new '.$sh_text); ?></a>
						</div>
						<!--<h4 class="customer-profile-group-heading"><?= _l($title); ?></h4>-->
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						<div class="row mbot15">
                            <div class="col-md-12">
                                <h4 class="no-margin"><?php echo _l($heading_text.' Summary'); ?></h4>
                                <?php $where_summary; ?>
                            </div>
                            <div class="col-md-4 col-xs-6 border-right">
                                <h3 class="bold"><?= $total_garage = $this->db->get_where(db_prefix().'garage')->num_rows(); ?></h3>
                                <span class="text-dark"><?php echo _l($heading_text.' Summary total'); ?></span>
                            </div>
                            <div class="col-md-4 col-xs-6 border-right">
                                <h3 class="bold"><?= $total_garage = $this->db->get_where(db_prefix().'garage', array('status' => 1))->num_rows(); ?></h3>
                                <span class="text-success"><?php echo _l('Active '.$heading_text); ?></span>
                            </div>
                            <div class="col-md-4 col-xs-6 border-right">
                                <h3 class="bold"><?= $total_garage = $this->db->get_where(db_prefix().'garage', array('status' => 0))->num_rows(); ?></h3>
                                <span class="text-danger"><?php echo _l('Inactive Active '.$heading_text); ?></span>
                            </div>
                        </div>
						<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Name'),
							_l('Email'),
							_l('Fax'),
							_l('Company Email'),
							_l('Website Url'),
							_l('City/Emirate'),
							_l('Location'),
							_l('Status')
							),'garages'); 
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php init_tail(); ?>
	<script>
		initDataTable('.table-garages', window.location.href, [1], [1]);
	</script>
</body>
</html>
