<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading">&nbsp; <a class="btn btn-info" href="<?= admin_url('message'); ?>">Inbox</a>&nbsp;&nbsp;<a href="<?= admin_url('message/sendboxList'); ?>" class="btn btn-info"> Sendbox </a> <?= _l($title); ?></h4>
						<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Sender'),
							_l('Subject'),
							_l('Message'),
							_l('Date'),
							_l('Reply')
							),'message'); 
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php init_tail(); ?>
	<script>
		initDataTable('.table-message', window.location.href, [1], [1]);

		function replymsg(id, token)
		{
		    $('#senderID').val(id);
		    $('#token').val(token);
		}
		function replybyAdmin()
		{
		    var id = $('#senderID').val();
		    var token = $('#token').val();
		    var title = $('#title').val();
            var message = $('#message').val();
            if(title != '' && message != '')
            {
                var str = "receiver_id="+id+"&tokenID="+token+"&title="+title+"&message="+message+"&"+csrfData['token_name']+"="+csrfData['hash'];
        	    $.ajax({
        	        url: '<?= admin_url()?>message/composemsg',
        	        type: 'POST',
        	        data: str,
        	        cache: false,
        	        success: function(resp){
        	            if(resp)
        	            {
        	                $("#msg")[0].reset() ;
        	                successmsg('Message are send successfully');
        	                $('#closemodal').click(); 
        	            }
        	            else
        	            {
        	                errormsg('Message are not send')
        	            }
        	        }
        	    });
            }
            else
            {
                var error = 'Title and message both field are required';
                errormsg(error);
            }
		}
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <script>
        /* Success message  */
        function successmsg(msg)
        {
            toastr.success('',msg);
        }
        
        function errormsg(msg)
        {
            toastr.error('',msg);
        }
    </script>
</body>
<!-- Modal -->
<div class="modal fade" id="replyModal" tabindex="-1" role="dialog" aria-labelledby="replyModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="replyModalLabel">Reply</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="msg">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <input type="text" class="form-control" name="title" id="title" required placeholder="Enter title" id="title">
                            <input id="senderID" class="hide">
                            <input id="token" class="hide">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <textarea class="form-control" required name="message" id="message" placeholder="Describe here" rows="4"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="closemodal" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" onclick="replybyAdmin()" class="btn btn-primary">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>

</html>
