<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<link href="https://cdn.datatables.net/fixedheader/3.1.7/css/fixedHeader.dataTables.min.css" />
<script src="https://cdn.datatables.net/fixedheader/3.1.7/js/dataTables.fixedHeader.min.js"></script>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="customer-profile-group-heading"><?= _l($title); ?></h4>
						<hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Id'),
							_l('Garage'),
							_l('Product Name'),
							_l('Category'),
							_l('Sub Category'),
							_l('Location'),
							_l('With/Without Fittings'),
							_l('Status'),
							_l('Date')
							),'products'); 
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php init_tail(); ?>
    
	<script>
		initDataTable('.table-products', window.location.href, [1], [1]);
		
		$(document).ready(function() {
            $('#DataTables_Table_0 thead tr').clone(true).appendTo( '#DataTables_Table_0 thead' );
            $('#DataTables_Table_0 thead tr:eq(1) th').each( function (i) {
                var title = $(this).text();
                if(title == 'Action')
                {
                    $(this).html( '<input type="text" class="form-control" disabled />' );
                }
                else
                {
                    $(this).html( '<input type="text" class="form-control" placeholder="Search'+title+'" />' );   
                }
                    
         
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
         
            var table = $('#DataTables_Table_0').DataTable( {
                orderCellsTop: true,
                fixedHeader: true,
                "order": [[ 0, "desc" ]]
            } );
            
        } );
	</script>
</body>
</html>
