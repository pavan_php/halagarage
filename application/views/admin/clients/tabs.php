<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<ul class="nav navbar-pills navbar-pills-flat nav-tabs nav-stacked customer-tabs" role="tablist">
  <?php
        if($client->role ==2)
        {
            /*
            foreach(filter_client_visible_tabs($customer_tabs) as $key => $tab){
              if($tab['name'] == 'Contacts' || $tab['name'] == 'Proposals' || $tab['name'] == 'Credit Notes' || $tab['name'] == 'Estimates' || $tab['name'] == 'Expenses' || $tab['name'] == 'Contracts' || $tab['name'] == 'Projects' || $tab['name'] == 'Projects' || $tab['name'] == 'Tasks' || $tab['name'] == 'Tickets' || $tab['name'] == 'Vault')
              {}
              else
              {
                 ?>
                    <li class="<?php if($key == 'profile'){echo 'active ';} ?>customer_tab_<?php echo $key; ?>">
                      <a data-group="<?php echo $key; ?>" href="<?php echo admin_url('clients/client/'.$client->userid.'?group='.$key); ?>">
                        <?php if(!empty($tab['icon'])){ ?>
                            <i class="<?php echo $tab['icon']; ?> menu-icon" aria-hidden="true"></i>
                        <?php } ?>
                        <?php echo $tab['name']; ?>
                      </a>
                    </li>
                 <?php
              }
            }
            */
            ?>
                <li class="<?php if($_GET['group'] == 'profile'){echo 'active ';} ?> customer_tab_profile">
                    <a data-group="profile" href="<?php echo admin_url('garages/garage/'.$client->userid.'?group=profile'); ?>">
                        <i class="fa fa-user-circle menu-icon" aria-hidden="true"></i>
                        Profile
                    </a>
                </li><?php /*?>
                <li class="customer_tab_subscriptions">
                    <!--<a data-group="subscriptions" href="<?php echo admin_url('clients/client/'.$client->userid.'?group=subscriptions'); ?>">-->
                    <a data-group="subscriptions" href="#">
                        <i class="fa fa-repeat menu-icon" aria-hidden="true"></i>
                        Subscriptions
                    </a>
                </li><?php */ ?>
                <li class="<?php if($_GET['group'] == 'invoices'){echo 'active ';} ?>customer_tab_invoices">
                    <a data-group="invoices" href="<?php echo admin_url('clients/client/'.$client->userid.'?group=invoices'); ?>">
                        <i class="fa fa-file-text menu-icon" aria-hidden="true"></i>
                        Invoices
                    </a>
                </li><?php /* ?>
                <li class="customer_tab_notes">
                    <!--<a data-group="notes" href="http://php.manageprojects.in/halagarage/admin/clients/client/40?group=notes">-->
                    <a data-group="subscriptions" href="#">
                        <i class="fa fa-sticky-note-o menu-icon" aria-hidden="true"></i>
                        Notes
                    </a>
                </li><?php */ ?>
                <li class="<?php if($_GET['group'] == 'payments'){echo 'active ';} ?>customer_tab_payments">
                    <a data-group="payments" href="<?php echo admin_url('clients/client/'.$client->userid.'?group=payments'); ?>">
                        <i class="fa fa-line-chart menu-icon" aria-hidden="true"></i>
                        <?= _l('Orders'); ?>
                    </a>
                </li>
                
                <li class="customer_tab_statement">
                    <a data-group="statement" href="<?php echo admin_url('garages/garage/'.$client->userid.'?group=statement'); ?>">
                        <i class="fa fa-area-chart menu-icon" aria-hidden="true"></i>
                        <?= _l('Last Login'); ?>
                    </a>
                </li>
                <!--
                <li class="customer_tab_attachments">
                    <a data-group="attachments" href="http://php.manageprojects.in/halagarage/admin/clients/client/40?group=attachments">
                        <i class="fa fa-paperclip menu-icon" aria-hidden="true"></i>
                        Files
                    </a>
                </li>
                <li class="customer_tab_reminders">
                    <a data-group="reminders" href="http://php.manageprojects.in/halagarage/admin/clients/client/40?group=reminders">
                        <i class="fa fa-clock-o menu-icon" aria-hidden="true"></i>
                        Reminders
                    </a>
                </li>
                <li class="customer_tab_map">
                    <a data-group="map" href="http://php.manageprojects.in/halagarage/admin/clients/client/40?group=map">
                        <i class="fa fa-map-marker menu-icon" aria-hidden="true"></i>
                        Map
                    </a>
                </li>
                -->
            <?php
        }
        else
        {
            /*
            foreach(filter_client_visible_tabs($customer_tabs) as $key => $tab){
                if($tab['name'] == 'Reminders' || $tab['name'] == 'Subscriptions' || $tab['name'] == 'Payments' || $tab['name'] == 'Invoices' || $tab['name'] == 'Statement' || $tab['name'] == 'Contacts' || $tab['name'] == 'Proposals' || $tab['name'] == 'Credit Notes' || $tab['name'] == 'Estimates' || $tab['name'] == 'Expenses' || $tab['name'] == 'Contracts' || $tab['name'] == 'Projects' || $tab['name'] == 'Projects' || $tab['name'] == 'Tasks' || $tab['name'] == 'Tickets' || $tab['name'] == 'Vault')
                {}
                else
                {
                    ?>
                        <li class="<?php if($key == 'profile'){echo 'active ';} ?>customer_tab_<?php echo $key; ?>">
                          <a data-group="<?php echo $key; ?>" href="<?php echo admin_url('clients/client/'.$client->userid.'?group='.$key); ?>">
                            <?php if(!empty($tab['icon'])){ ?>
                                <i class="<?php echo $tab['icon']; ?> menu-icon" aria-hidden="true"></i>
                            <?php } ?>
                            <?php echo $tab['name']; ?>
                          </a>
                        </li>
                    <?php
                }
            }*/
            ?>
                <li class="active customer_tab_profile">
                    <a data-group="profile" href="<?php echo admin_url('clients/client/'.$client->userid.'?group=profile'); ?>">
                        <i class="fa fa-user-circle menu-icon" aria-hidden="true"></i>
                        Profile
                    </a>
                </li>
                
                <li class="customer_tab_statement">
                    <a data-group="statement" href="<?php echo admin_url('clients/client/'.$client->userid.'?group=statement'); ?>">
                        <i class="fa fa-area-chart menu-icon" aria-hidden="true"></i>
                        <?= _l('Last Login'); ?>
                    </a>
                </li>
            <?php
        }
    ?>
</ul>
