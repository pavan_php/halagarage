<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
					<h4 class="no-margin">
					<?php echo $title; ?>
					</h4>
					<hr class="hr-panel-heading" />
						<?php echo form_open($this->uri->uri_string()); ?>
						    <div class="row form-group">
						        <div class="col-md-4">
						            <?= _l('Days'); ?>
						            <input type="number" maxlength="3" class="form-control" name="free_limit" required value="<?= $daysLimit_res->free_limit; ?>">
						        </div>
						        <div class="col-md-4">
						            <?= _l('Status'); ?>
						            <select name="status" class="form-control">
						                <option value=""></option>
						                <option value="1" <?= ($daysLimit_res->status == 1)?"selected":""; ?>>Active</option>
						                <option value="2" <?= ($daysLimit_res->status == 2)?"selected":""; ?>>Inactive</option>
						            </select>
						        </div>
						        <div class="col-md-2"><br>
						            <button type="submit" class="btn btn-info pull-right"><?php echo _l('submit'); ?></button>
						        </div>
						    </div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php init_tail(); ?>
</body>
</html>
