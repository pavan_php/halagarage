<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
    				<div class="panel-body">
    					<?= form_open_multipart(admin_url('advertisement/add/'.$article->id));  ?> 
    					    <h4 class="customer-profile-group-heading"><?= _l('Advertisement'); ?></h4>
						    <hr class="hr-panel-heading" />
    					    <div class="row form-group">
        					    <div class="col-md-3">
        					       <?= _l('Title'); ?>
        					       <input type="text" class="form-control" required value="<?= $article->title; ?>" name="title">
        					    </div>
        					    <div class="col-md-2">
        					       <?= _l('Start Date'); ?>
        					       <input type="text" required class="form-control" autocomplete="off" value="<?= $article->start_date; ?>" name="start_date" id="start_date">
        					   </div>
        					   <div class="col-md-2">
        					       <?= _l('End Date'); ?>
        					       <input type="text" required class="form-control" autocomplete="off" value="<?= $article->end_date; ?>" name="end_date" id="end_date">
        					   </div>
        					    <div class="col-md-4">
        					       <?= _l('Location'); ?>
        					       <?php
        					            $locationarr = explode(',',$article->location);
        					       ?>
        					       <select name="location[]" class="form-control selectpicker" multiple>
        					           <option value="top-left"  <?= (in_array('top-left', $locationarr)?"selected":""); ?>>Top Left</option>
        					           <option value="top-right" <?= (in_array('top-right', $locationarr)?"selected":""); ?>>Top Right</option>
        					           <option value="center" <?= (in_array('center', $locationarr)?"selected":""); ?>>Center</option>
        					           <option value="bottom-left" <?= (in_array('bottom-left', $locationarr)?"selected":""); ?>>Bottom Left</option>
        					           <option value="bottom-right" <?= (in_array('bottom-right', $locationarr)?"selected":""); ?>>Bottom Right</option>
        					       </select>
        					    </div>
    					    </div>
    					    <div class="row form-group">
    					        <div class="col-md-2">
        					       <?= _l('Selected Image'); ?>
        					       <?php $imageArray = $this->db->get_where(db_prefix().'files', array('rel_id' => $article->id, 'rel_type' => "advertisement"))->row(); ?>
        					       <img src="<?= site_url('uploads/advertisement/'.$article->id.'/'. $imageArray->file_name); ?>" width="50xp" height="50px" />
        					    </div>
        					    <div class="col-md-3">
        					       <?= _l('New Image'); ?>
        					       <input type="file" class="form-control"  name="advertisement">
        					    </div>
        					    <div class="col-md-4"><br/>
        					       <button type="submit" class="btn btn-info">Update advertisement</button>
        					    </div>
    					    </div>
    				    </form>
    				    <hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Image'),
							_l('Title'),
							_l('Start Date'),
							_l('End Date'),
							_l('Location'),
							_l('options')
							),'advertisement'); 
						?>
    				</div>
    			</div>
			</div>
		</div>
	</div>
</div>
<?php init_tail(); ?>
	<script>
		initDataTable('.table-advertisement', window.location.href, [1], [1]);
		
		$(document).ready(function(){
            $("#start_date").datepicker({
                minDate:0,
                dateFormat: 'yy-mm-dd',
                onSelect: function(selected) {
                  $("#end_date").datepicker("option","minDate", selected)
                }
            });
            $("#end_date").datepicker({ 
                minDate:0,
                dateFormat: 'yy-mm-dd',
                onSelect: function(selected) {
                   $("#start_date").datepicker("option","maxDate", selected)
                }
            });  
        });
	</script>
</body>
</html>
