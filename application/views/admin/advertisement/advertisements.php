<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
		    <div class="col-md-3 hide">
			    <div class="panel_s">
			        <div class="panel-body">
			            <h4 class="customer-profile-group-heading"><?= _l('Advertisement'); ?></h4>
						<hr class="hr-panel-heading" />
    				    <div class="form-group">
					       <a href="<?= admin_url('advertisement/sampledata'); ?>" class="btn btn-success">Download Sample</a>
					   </div>
					   <?= form_open_multipart(admin_url('advertisement/save'));  ?>
    					   <div class="form-group">
    					       <input type="file" name="fileURL" required id="file-url" class="filestyle" data-allowed-file-extensions="[CSV, csv]" accept=".CSV, .csv" data-buttontext="Choose File">
    					   </div>
    					   <div class="form-groupt">
                                <button type="submit" class="btn btn-primary mrgT">Import</button>
                            </div>  
                        </form>
    				</div>
			    </div>
			</div>
			<div class="col-md-12">
				<div class="panel_s">
    				<div class="panel-body">
    					<?= form_open_multipart(admin_url('advertisement/add'));  ?> 
    					    <h4 class="customer-profile-group-heading"><?= _l('Advertisement'); ?></h4>
						    <hr class="hr-panel-heading" />
    					    <div class="row form-group">
        					    <div class="col-md-3">
        					       <?= _l('Title'); ?>
        					       <input type="text" class="form-control" required  name="title">
        					    </div>
        					    <div class="col-md-2">
        					       <?= _l('Start Date'); ?>
        					       <input type="text" required class="form-control" autocomplete="off" name="start_date" id="start_date">
        					   </div>
        					   <div class="col-md-2">
        					       <?= _l('End Date'); ?>
        					       <input type="text" required class="form-control" autocomplete="off" name="end_date" id="end_date">
        					   </div>
        					    <div class="col-md-4">
        					       <?= _l('Location'); ?>
        					       <select name="location[]" class="form-control selectpicker" multiple>
        					           <option value="top-left">Top Left</option>
        					           <option value="top-right">Top Right</option>
        					           <option value="center">Center</option>
        					           <option value="bottom-left">Bottom Left</option>
        					           <option value="bottom-right">Bottom Right</option>
        					       </select>
        					    </div>
    					    </div>
    					    <div class="row form-group">
    					        <div class="col-md-3">
        					       <?= _l('Image'); ?>
        					       <input type="file" class="form-control" required  name="advertisement">
        					    </div>
    					        <div class="col-md-4"><br/>
        					       <button type="submit" class="btn btn-info">Create advertisement</button>
        					    </div>
    					    </div>
    				    </form>
    				    <hr class="hr-panel-heading" />
						<?php render_datatable(array(
							_l('Image'),
							_l('Title'),
							_l('Start Date'),
							_l('End Date'),
							_l('Location'),
							_l('options')
							),'advertisement'); 
						?>
    				</div>
    			</div>
			</div>
		</div>
	</div>
</div>
<?php init_tail(); ?>
	<script>
		initDataTable('.table-advertisement', window.location.href, [1], [1]);
		
		$(document).ready(function(){
            $("#start_date").datepicker({
                minDate:0,
                dateFormat: 'yy-mm-dd',
                onSelect: function(selected) {
                  $("#end_date").datepicker("option","minDate", selected)
                }
            });
            $("#end_date").datepicker({ 
                minDate:0,
                dateFormat: 'yy-mm-dd',
                onSelect: function(selected) {
                   $("#start_date").datepicker("option","maxDate", selected)
                }
            });  
        });
	</script>
</body>
</html>
