<?php
    if($product_result)
    {
        foreach($product_result as $rrr)
        {
            $subcatarr = explode(',', $rrr->subcategory);
            ?>
                <tr class="table-row" id="remove_<?= $rrr->id ?>">
                    <td><?= $rrr->id; ?></td>
                    <td><?= $rrr->product_name; ?></td>
                    <td><?= $catname = $this->db->get_where(db_prefix().'category', array('id' => $rrr->category))->row('name');?></td>
                    <td>
                        <?php
                            if($subcatarr)
                            {
                                $subcat = '';
                                foreach($subcatarr as $rr)
                                {
                                    $subcat .= $this->db->get_where(db_prefix().'category', array('id' => $rr))->row('name').', '; 
                                }
                                echo $subcat;
                            }
                        ?>
                    </td>
                    <td>
                        <?php
                            if($rrr->price_with_fitting != '')
                            {
                                echo $rrr->price_with_fitting.' '.CURRENCY_NAME;
                            }
                        ?>
                    </td>
                    <td>
                        <?php
                            if($rrr->price_without_fitting != '')
                                echo $rrr->price_without_fitting.' '.CURRENCY_NAME;
                        ?>
                    </td>
                    <td>
                        <?php
                            if($rrr->status == 1)
                            {
                                echo '<span class="badge badge-active">Published</span>';
                            }
                            else
                            {
                                echo '<span class="badge badge-pending">Draft</span>'; 
                            }
                        ?>
                    </td>
                    <td>
                        <p class="btn-group" role="group" aria-label="Basic example">
                            <a href="<?= site_url('myProducts/viewProduct/'.$rrr->id); ?>" class="btn btn-confirm mr-3" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a href="<?= site_url('myProducts/editProduct/'.$rrr->id); ?>" class="btn btn-reply mr-3" data-toggle="tooltip" title="Edit"><i class="fa fa-edit" aria-hidden="true"></i></a>
                            <button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" onClick="removeProduct(<?= $rrr->id ?>);" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        </p>
                    </td>
                </tr>
            <?php
        }
    }
    else
    {
        ?>
            <tr>
                <td valign="top" colspan="8" class="dataTables_empty">No matching records found</td>
            </tr>
        <?php
    }
?>