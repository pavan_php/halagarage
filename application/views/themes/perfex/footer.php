<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="pusher"></div>
<style type="text/css">
    .searchselectoption li{
        border-radius: 3px!important;
        padding: 0!important;
        display: block!important;
        justify-content: space-between!important;
        margin-bottom: 0!important;
        font-size: 14px!important;
    }
</style>
<footer class="footer navbar-fixed-bottom <?= ($this->uri->segment(1) == 'authentication' || $this->uri->segment(1) == 'login' )?"hide":""; ?>">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-right">
				<!--<span class="copyright-footer"><?php echo date('Y'); ?> <?php echo _l('clients_copyright', get_option('companyname')); ?></span>
				<?php if(is_gdpr() && get_option('gdpr_show_terms_and_conditions_in_footer') == '1') { ?>
					- <a href="<?php echo terms_url(); ?>" class="terms-and-conditions-footer"><?php echo _l('terms_and_conditions'); ?></a>
				<?php } ?>
				<?php if(is_gdpr() && is_client_logged_in() && get_option('show_gdpr_link_in_footer') == '1') { ?>
					- <a href="<?php echo site_url('clients/gdpr'); ?>" class="gdpr-footer"><?php echo _l('gdpr_short'); ?></a>
				<?php } ?>-->
				<ul style="list-style: none;margin: 0;padding: 0;font-weight: normal;">
					<li style="display: inline-block;padding: 10px 5px;margin: 0;line-height: 20px;text-align: center;">
						<a href="<?= site_url('contact-us');?>">Conatct Us</a>
					</li>
					<li style="display: inline-block;padding: 10px 5px;margin: 0;line-height: 20px;text-align: center;">
						<a href="<?= site_url('term-and-conditions'); ?>">Terms & Conditions</a>
					</li>
					<li style="display: inline-block;padding: 10px 5px;margin: 0;line-height: 20px;text-align: center;">
						<a href="javascript:void(0);">Policy</a>
					</li>
					<li style="display: inline-block;padding: 10px 5px;margin: 0;line-height: 20px;text-align: center;">
						<a href="javascript:void(0);">Faq</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</footer>
 <script src="<?php echo  base_url('assets/js/bootbox.js')?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <script src="<?= base_url() ?>assets/vue/vue.min.js"></script>
    <script src="<?= base_url() ?>assets/vue/vue.js"></script>
    <script src="<?= base_url() ?>assets/vue/axios.min.js"></script>

    <script src="<?= base_url() ?>assets/chat-css-js/jquery.textcomplete.js"></script> 
    <script src="<?= base_url() ?>assets/chat-css-js/emojionearea.js"></script> 
    <script src="<?= base_url() ?>assets/chat-css-js/vue-resource.min.js"></script> 
    <script src="<?= base_url() ?>assets/chat-css-js/emojione.js"></script> 
    <script src="<?= base_url() ?>assets/chat-css-js/bootstrap-tagsinput.min.js"></script> 
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> 

<script>
    $(document).ready(function() {
// Setup - add a text input to each footer cell
        $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) td').each( function (i) {
            var title = $(this).text();
            if(title == 'Action')
            {
                $(this).html( '<input type="text" class="form-control" disabled />' );
            }
            else if(title == 'Status')
            {
                var segmentID = '<?= $this->uri->segment("1")?>';
                if(segmentID == 'myProducts')
                {
                    $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' ); 
                }
                else
                {
                    $(this).html('<select class="searchselectoption" style="border-radius: 3px!important;padding: 0!important;display: block!important;justify-content: space-between!important;margin-bottom: 0!important;font-size: 14px!important;"> <option value=""></option><option value="Pending">Pending</option><option value="Complete">Complete</option><option value="On Schedule">On Schedule</option><option value="Cancelled">Cancelled</option><option value="Expired">Expired</option></select>');
                }
            }
            else
            {
                $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );   
            }
                     
            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );

            $( 'select', this ).on( 'change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        });
        var table = $('#example').DataTable( {
            orderCellsTop: true,
            fixedHeader: true,
            "order": [[ 0, "desc" ]]
        });                 
    });

    setTimeout(function(){ 
        $('#loaderHide').hide();
        $('#example').show();      
    }, 100); 
    
</script>
<script>
    function getSubCategory(Id)
    {
        var str = "catid="+Id+"&"+csrfData['token_name']+"="+csrfData['hash'];
	    $.ajax({
	        url: '<?= site_url()?>myProducts/getSubCategory',
	        type: 'POST',
	        data: str,
	        dataType: 'json',
	        cache: false,
	        success: function(resp){
	            console.log(resp);
	            $resp = '<option value=""></option>';
                for(var i=0; i< resp.length; i++)
                {
                    $resp += '<option value="'+resp[i].id+'">'+resp[i].name+'</option>';
                }
                $('#sub_category_id').html($resp);
                setTimeout(function(){  
                        $('#sub_category_id').selectpicker('destroy'); 
                        $('#sub_category_id').selectpicker(); 
                    }, 1000);
	        }
	    });
	    $('.tyre-product').removeClass('stepwizard-row setup-panel');
	    if(Id == 4)
	    {
	        //$('#step-4').removeClass('hide');
	        //$('#step-4').addClass('setup-content');
	        $('.tyre-product').addClass('hide');
	        $('.yestyre-product').removeClass('hide');
	        $('.notyre-product').addClass('hide');
	        $('.notyre-product').removeClass('stepwizard-row setup-panel');
	        $('.yestyre-product').addClass('stepwizard-row setup-panel');
	        //$('#skiptyre').addClass('nextBtn');
	         $('#make_id').removeAttr('required');
	        $('#model_id').removeAttr('required');
	        $('#year_id').removeAttr('required');
	        //$('.tyre-product').html('<div class="stepwizard"><div class="stepwizard-row setup-panel"><div class="stepwizard-step col-xs-2"><a href="#step-1" type="button" class="btn btn-confirm btn-circle">1</a><p><small>Product</small></p></div><div class="stepwizard-step col-xs-2"><a href="#step-2" type="button" class="btn tn-default btn-circle" disabled="disabled">2</a><p><small>Category</small></p></div><div class="stepwizard-step col-xs-2"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a><p><small>Filter</small></p></div><div class="stepwizard-step col-xs-2"><a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a><p><small>Tyre Brand</small></p></div><div class="stepwizard-step col-xs-2"><a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a><p><small>Description</small></p></div><div class="stepwizard-step col-xs-2"><a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a><p><small>Price</small></p></div></div></div>');
	    }
	    else
	    {
	        //$("#skiptyre").click();
	        //$('#step-4').addClass('hide');
	        //$('#step-4').removeClass('setup-content');
	        $('.tyre-product').addClass('hide');
	        $('.notyre-product').removeClass('hide');
	        $('.yestyre-product').addClass('hide');
	        $('.notyre-product').addClass('stepwizard-row setup-panel');
	        $('.yestyre-product').removeClass('stepwizard-row setup-panel');
	        $('#make_id').attr('required','required');
	        $('#model_id').attr('required','required');
	        $('#year_id').attr('required','required');
	        //$('#skiptyre').removeClass('nextBtn');
	        //$('.tyre-product').html('<div class="stepwizard"><div class="stepwizard-row setup-panel"><div class="stepwizard-step col-xs-2"><a href="#step-1" type="button" class="btn btn-confirm btn-circle">1</a><p><small>Product</small></p></div><div class="stepwizard-step col-xs-2"><a href="#step-2" type="button" class="btn tn-default btn-circle" disabled="disabled">2</a><p><small>Category</small></p></div><div class="stepwizard-step col-xs-2"><a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a><p><small>Filter</small></p></div><div class="stepwizard-step col-xs-2"><a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a><p><small>Description</small></p></div><div class="stepwizard-step col-xs-2"><a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a><p><small>Price</small></p></div></div></div>');
	    }
    }

/***----------------------  Multipart filter -------------------------------***/
    function getMultiModelList()
    {
        var make_id = $("#make_id").selectpicker().val();
        $('#year_id').html('<option value=""></option>');
        var str = "catid="+make_id+"&"+csrfData['token_name']+"="+csrfData['hash'];
	    $.ajax({
	        url: '<?= site_url()?>myProducts/getMultiModelList',
	        type: 'POST',
	        data: str,
	        dataType: 'json',
	        cache: false,
	        success: function(resp){
	            console.log(resp);
	            $resp = '<option value=""></option>';
                for(var i=0; i< resp.length; i++)
                {
                    $resp += '<option value="'+resp[i].id+'">'+resp[i].name+'</option>';
                }
                $('#model_id').html($resp);
                setTimeout(function(){  
                    $('#model_id').selectpicker('destroy'); 
                    $('#model_id').selectpicker(); 
                }, 1000);
                // $('.chosen-select').chosen({}).change( function(obj, result) {});
	        }
	    });
    }

    /* getMultiYearList */
    function getMultiYearList()
    {
        var make_id = $("#make_id").selectpicker().val();
        var values = $("#model_id").selectpicker().val();
        //var make_id = $( "#make_id option:selected" ).val();
        var str = "catid="+values+"&make_id="+make_id+"&"+csrfData['token_name']+"="+csrfData['hash'];
	    $.ajax({
	        url: '<?= site_url()?>myProducts/getMultiYearList',
	        type: 'POST',
	        data: str,
	        dataType: 'json',
	        cache: false,
	        success: function(resp){
	            console.log(resp);
	            $resp = '<option value=""></option>';
                for(var i=0; i< resp.length; i++)
                {
                    $resp += '<option value="'+resp[i].id+'">'+resp[i].year+'</option>';
                }
                $('#year_id').html($resp);
                setTimeout(function(){  
                    $('#year_id').selectpicker('destroy'); 
                    $('#year_id').selectpicker(); 
                }, 1000);
	        }
	    });
    }

/***----------------------  //End --------- -------------------------------***/

    function getModelList(Id)
    {
        $('#year_id').html('<option value=""></option>');
        var str = "catid="+Id+"&"+csrfData['token_name']+"="+csrfData['hash'];
	    $.ajax({
	        url: '<?= site_url()?>myProducts/getModelList',
	        type: 'POST',
	        data: str,
	        dataType: 'json',
	        cache: false,
	        success: function(resp){
	            console.log(resp);
	            $resp = '<option value=""></option>';
                for(var i=0; i< resp.length; i++)
                {
                    $resp += '<option value="'+resp[i].id+'">'+resp[i].name+'</option>';
                }
                $('#model_id').html($resp);
                setTimeout(function(){  
                    $('#model_id').selectpicker('destroy'); 
                    $('#model_id').selectpicker(); 
                }, 1000);
                // $('.chosen-select').chosen({}).change( function(obj, result) {});
	        }
	    });
    }
    
    /* getYearList */
    function getYearList(Id)
    {
        var values = $("#model_id").selectpicker().val();
        var make_id = $( "#make_id option:selected" ).val();
        var str = "catid="+values+"&make_id="+make_id+"&"+csrfData['token_name']+"="+csrfData['hash'];
	    $.ajax({
	        url: '<?= site_url()?>myProducts/getYearList',
	        type: 'POST',
	        data: str,
	        dataType: 'json',
	        cache: false,
	        success: function(resp){
	            console.log(resp);
	            $resp = '<option value=""></option>';
                for(var i=0; i< resp.length; i++)
                {
                    $resp += '<option value="'+resp[i].id+'">'+resp[i].year+'</option>';
                }
                $('#year_id').html($resp);
                setTimeout(function(){  
                    $('#year_id').selectpicker('destroy'); 
                    $('#year_id').selectpicker(); 
                }, 1000);
	        }
	    });
    }
    
    /* Get Width List */
    function getWidthList(Id)
    {
        $('#height_id').html('<option value=""></option>');
        $('#rim_id').html('<option value=""></option>');
        var str = "catid="+Id+"&"+csrfData['token_name']+"="+csrfData['hash'];
	    $.ajax({
	        url: '<?= site_url()?>myProducts/getWidthList',
	        type: 'POST',
	        data: str,
	        dataType: 'json',
	        cache: false,
	        success: function(resp){
	            console.log(resp);
	            $resp = '<option value=""></option>';
                for(var i=0; i< resp.length; i++)
                {
                    $resp += '<option value="'+resp[i].id+'">'+resp[i].name+'</option>';
                }
                $('#width_id').html($resp);
                setTimeout(function(){  
                    $('#width_id').selectpicker('destroy'); 
                    $('#width_id').selectpicker(); 
                }, 1000);
	        }
	    });
    }
    
    /* getHeightList */
    function getHeightList(Id)
    {
        $('#rim_id').html('<option value=""></option>');
        var brand_id = $( "#brand_id option:selected" ).val();
        var str = "catid="+Id+"&brand_id="+brand_id+"&"+csrfData['token_name']+"="+csrfData['hash'];
	    $.ajax({
	        url: '<?= site_url()?>myProducts/getHeightList',
	        type: 'POST',
	        data: str,
	        dataType: 'json',
	        cache: false,
	        success: function(resp){
	            console.log(resp);
	            $resp = '<option value=""></option>';
                for(var i=0; i< resp.length; i++)
                {
                    $resp += '<option value="'+resp[i].id+'">'+resp[i].value+'</option>';
                }
                $('#height_id').html($resp);
                setTimeout(function(){  
                    $('#height_id').selectpicker('destroy'); 
                    $('#height_id').selectpicker(); 
                }, 1000);
	        }
	    });
    }
    
    /* getRimList */
    function getRimList(Id)
    {
        var brand_id = $( "#brand_id option:selected" ).val();
        var width_id = $( "#width_id option:selected" ).val();
        var str = "catid="+Id+"&width_id="+width_id+"&brand_id="+brand_id+"&"+csrfData['token_name']+"="+csrfData['hash'];
	    $.ajax({
	        url: '<?= site_url()?>myProducts/getRimList',
	        type: 'POST',
	        data: str,
	        dataType: 'json',
	        cache: false,
	        success: function(resp){
	            console.log(resp);
	            $resp = '<option value=""></option>';
                for(var i=0; i< resp.length; i++)
                {
                    $resp += '<option value="'+resp[i].id+'">'+resp[i].diameter+'</option>';
                }
                $('#rim_id').html($resp);
                setTimeout(function(){
                    $('#rim_id').selectpicker('destroy'); 
                    $('#rim_id').selectpicker(); 
                }, 1000);
	        }
	    });
    }
    
    /* getAreaList */
    function getAreaList(Id)
    {
        var str = "catid="+Id+"&"+csrfData['token_name']+"="+csrfData['hash'];
	    $.ajax({
	        url: '<?= site_url()?>myProducts/getAreaList',
	        type: 'POST',
	        data: str,
	        dataType: 'json',
	        cache: false,
	        success: function(resp){
	            $resp = '<option value=""></option>';
                for(var i=0; i< resp.length; i++)
                {
                    $resp += '<option value="'+resp[i].id+'">'+resp[i].name+'</option>';
                }
                $('#area_id').html($resp);
                setTimeout(function(){  
                    $('#area_id').selectpicker('destroy'); 
                    $('#area_id').selectpicker(); 
                }, 1000);
	        }
	    });
    }
    
    /*
    *   removeProduct
    */
    function removeProduct(Id)
    {
        var str = "id="+Id+"&"+csrfData['token_name']+"="+csrfData['hash'];
	    var x = confirm("Are you sure you want to delete?");
        if (x)
        {
            $.ajax({
    	        url: '<?= site_url()?>myProducts/removeProduct',
    	        type: 'POST',
    	        data: str,
    	        dataType: 'json',
    	        cache: false,
    	        success: function(resp){
    	            $('#remove_'+Id).empty();
    	        }
    	    });
        }
    }
    
    /*
    *   removeQuotes
    */
    function removeQuotes(Id)
    {
        var str = "id="+Id+"&"+csrfData['token_name']+"="+csrfData['hash'];
	    var x = confirm("Are you sure you want to delete?");
        if (x)
        {
            $.ajax({
    	        url: '<?= site_url()?>myOrders/removeQuotes',
    	        type: 'POST',
    	        data: str,
    	        dataType: 'json',
    	        cache: false,
    	        success: function(resp){
    	            $('#remove_'+Id).remove();
    	        }
    	    });
        }
    }
</script>
<script>
    function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;

        return true;
    } 
</script>