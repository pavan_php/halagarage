<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="panel_s">
    <div class="panel-body">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h3 class="modal-title" id="expiryModalTitle">Plan Upgrade Failure</h3>
            </div>
            <div class="modal-body text-center">
                <div class="box-wrap">
                    <img src="<?= base_url() ?>assets/images/error.svg" class="img-responsive" style="width: 15%;margin: 0 auto;"> <br>
                    <h1 style="color:#c10000;">Payment Failed</h1>
                    <p style="font-size: 20px;">Sorry, your plan upgradation has been failed.</p>
                    <br>
                    <?php
                        $plandetails_ = $this->db->get_where(db_prefix().'subscription')->row();
                    ?>
                </div>
            </div>
            <div class="modal-footer">
                <form action="<?php echo PAYPAL_URL; ?>" method="post">
                    <input type="hidden" name="business" value="<?php echo PAYPAL_ID; ?>">
                    <input type="hidden" name="cmd" value="_xclick">                                                                                  
                    <input type="hidden" name="item_name" value="<?php echo $plandetails_->title; ?>">
                    <input type="hidden" name="item_number" value="<?php echo $plandetails_->id; ?>">
                    <input type="hidden" name="amount" value="<?php echo ($plandetails_->price/3.26); ?>">
                    <input type="hidden" name="currency_code" value="<?php echo PAYPAL_CURRENCY; ?>">
                    <input type="hidden" name="return" value="<?php echo PAYPAL_RETURN_URL; ?>">
                    <input type="hidden" name="cancel_return" value="<?php echo PAYPAL_CANCEL_URL; ?>">
                    <input type="hidden" name="notify_url" value="<?php echo PAYPAL_NOTIFY_URL; ?>">                               
                    <input type="submit" name="submit" border="0" class="btn btn-info btn-lg" value="Try again">
                </form>
            </div>
        </div>
    </div>
</div>