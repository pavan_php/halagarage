<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="panel_s">
    <div class="panel-body">
        <h3 class="modal-title text-center" id="expiryModalTitle"><?= _l($title); ?></h3>
        <hr class="mt-0 mb-0">
        <table class="table dt-table table-announcements" data-order-col="1" data-order-type="desc">
            <thead>
                <tr>
                    <th><?php echo _l('SN'); ?></th>
                    <th><?php echo _l('Subscription'); ?></th>
                    <th><?php echo _l('Amount'); ?></th>
                    <th><?php echo _l('Status'); ?></th>
                    <th><?php echo _l('Payment Date'); ?></th>
                    <th><?php echo _l('Next Billing Date'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($billing_list)
                    {
                        $sn = 1;
                        foreach($billing_list as $rrr)
                        {
                            ?>
                                <tr>
                                    <td><?= $sn++; ?></td>
                                    <td><?= $this->db->get_where(db_prefix().'subscription', array('id'=> $rrr->item_number))->row('title'); ?></td>
                                    <td><?= $rrr->payment_gross.' '.CURRENCY_NAME; ?></td>
                                    <td><span class="label label-success s-status invoice-status-4">Paid</span></td>
                                    <td><?= _d($rrr->created_date); ?></td>
                                    <td><?= _d(date('Y-m-d',$rrr->expiry_date)); ?></td>
                                </tr>
                            <?php
                        }
                    }
                    else
                    {
                        ?>
                            <tr>
                                <td valign="top" colspan="6" class="dataTables_empty">No entries found</td>
                            </tr>
                        <?php
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>