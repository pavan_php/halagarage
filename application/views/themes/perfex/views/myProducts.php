<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="panel_s section-heading section-announcements">
    <div class="panel-body">
        <!--<h4 class="no-margin section-text"><?php echo _l($title); ?></h4>-->
        <div class="header-wrap p-10 bg-white">
            <div class="d-flex align-items-center">
                <div>
                    <h4 class="fw-700 mt-0">My Products</h4>
                </div>
                <div class="ml-auto align-items-center">
                    <div class="dl">
                        <form class="form-inline">
                            <div class="form-group hide">
                                <input class="search__input" type="text" placeholder="Search">
                            </div>
                            <div class="form-group">
                                <a href="<?= base_url('myProducts/publishProduct');?>" class="btn btn-create"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp; Publish Product</a>
                            </div>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel_s section-heading section-announcements">
    <div class="panel-body">
        <form class="form-inline" method="post" id="filterform">
            <div class="form-group">
                <select class="selectpicker" data-live-search="true" id="make_id" onChange="getModelList(this.value);">
                    <option value="">Vehicle Brand</option>
                    <?php
                        if($carmake_result)
                        {
                            foreach($carmake_result as $rrr){
                                ?>
                                    <option value="<?= $rrr->id?>"><?= $rrr->name; ?></option>
                                <?php
                            }
                        }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <select class="selectpicker form-control" id="model_id" onChange="getYearList(this.value);">
                    <option value="">Vehicle Model</option>
                </select>
            </div>
            <div class="form-group">
                <select class="selectpicker" id="year_id">
                    <option value="">Vehicle Year</option>
                </select>
            </div>
            <div class="form-group"><br>
                <button class="btn btn-theme" type="button" onclick="searchfillter()" style="margin-top: -17px;padding: 7px 12px;">Search</button>
            </div>
            <div class="form-group"><br>
                <button class="btn btn-theme" type="button" onclick="resetFillter()" style="margin-top: -17px;padding: 7px 12px;">Reset</button>
            </div>
        </form>
    </div>
</div>

<div class="panel_s"> 
    <div class="panel-body panel-sm-body">
        <div class="stats-wrap p-10 bg-white">
            <!--<table id="example" class="responsive-table" data-order-col="1" data-order-type="desc">-->
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
            <!--<table class="table dt-table table-announcements" data-order-col="1" data-order-type="desc">-->
                <thead>
                    <tr class="table-header">
                        <td>Id</td>
                        <td>Product Name</td>
                        <td>Category</td>
                        <td>Sub Category</td>
                        <td>With Fitting</td>
                        <td>Without Fitting</td>
                        <td>Status</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody id="productList">
                    <?php
                        if($product_result)
                        {
                            foreach($product_result as $rrr)
                            {
                                $subcatarr = explode(',', $rrr->subcategory);
                                //$carmodel = explode(',', $rrr->car_model);
                                //$makeyear = explode(',', $rrr->make_year);
                                ?>
                                    <tr class="table-row" id="remove_<?= $rrr->id ?>">
                                        <td><?= $rrr->id; ?></td>
                                        <td><?= $rrr->product_name; ?></td>
                                        <td><?= $catname = $this->db->get_where(db_prefix().'category', array('id' => $rrr->category))->row('name');?></td>
                                        <td class="sub-cat-wrap">
                                            <?php
                                                if($subcatarr)
                                                {
                                                    $subcat = '';
                                                    foreach($subcatarr as $rr)
                                                    {
                                                        $subcat .= $this->db->get_where(db_prefix().'category', array('id' => $rr))->row('name').', '; 
                                                    }
                                                    echo $subcat;
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if($rrr->price_with_fitting != '')
                                                {
                                                    echo $rrr->price_with_fitting.' '.CURRENCY_NAME;
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if($rrr->price_without_fitting != '')
                                                    echo $rrr->price_without_fitting.' '.CURRENCY_NAME;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if($rrr->status == 1)
                                                {
                                                    echo '<span class="badge badge-active">Published</span>';
                                                }
                                                else
                                                {
                                                    echo '<span class="badge badge-pending">Draft</span>'; 
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <p class="btn-group" role="group" aria-label="Basic example">
                                                <a href="<?= site_url('myProducts/viewProduct/'.$rrr->id); ?>" class="btn btn-confirm mr-3" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                <a href="<?= site_url('myProducts/editProduct/'.$rrr->id); ?>" class="btn btn-reply mr-3" data-toggle="tooltip" title="Edit"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                                <button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" onClick="removeProduct(<?= $rrr->id ?>);" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                            </p>
                                        </td>
                                    </tr>
                                <?php
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    function resetFillter()
    {
        $('#filterform')[0].reset();
        $('.selectpicker').selectpicker('refresh');
        $('#make_id').selectpicker('refresh');
        $('#model_id').selectpicker('refresh');
        $('#year_id').selectpicker('refresh');

    }
    
    function searchfillter()
    {
        var html = '';
        html += '<tr>';
        html += '<td collspan="7">Loading...</td>';
        html += '</tr>';
        $('#productList').html(html);
        var make_id = $( "#make_id option:selected" ).val();
        var model_id = $( "#model_id option:selected" ).val();
        var year_id = $( "#year_id option:selected" ).val();
        var str = "make_id="+make_id+"&model_id="+model_id+"&year_id="+year_id+"&"+csrfData['token_name']+"="+csrfData['hash'];
        $.ajax({
            url: '<?= site_url()?>myProducts/searchfillter',
            type: 'POST',
            data: str,
            dataType: 'html',
            cache: false,
            success: function(resp){
                if(resp)
                {
                    $('#productList').html(resp);
                }
            }
        });
    }
</script>