        <table class="responsive-table dataTable no-footer">
            <tr>
                <th width="20%" class="text-center">Product Name</th>
                <td><?= $quoteResult->product_name; ?></td>
            </tr>
            <tr>
                <th width="20%" class="text-center">Category</th>
                <td><?= categoryname($quoteResult->category); ?></td>
            </tr>
            <tr>
            <tr>
                <th width="20%" class="text-center">Product Brand</th>
                <td><?= $quoteResult->product_brand; ?></td>
            </tr>
            <tr>
                <th width="20%" class="text-center">Description</th>
                <td colspan="2"><?= $quoteResult->description; ?></td>
            </tr>
            <tr>
                <th width="20%" class="text-center">Price With Fitting</th>
                <td colspan="2">
                    <?php
                        if($quoteResult->offer_start_date < time() && $quoteResult->offer_end_date > time())
                        {
                            if($quoteResult->price_before_offer != '')
                            {
                                ?>
                                    <p><?= CURRENCY_NAME.' '.$quoteResult->price_before_offer; ?></p>
                                <?php   
                            }
                        }
                        elseif($quoteResult->price_with_fitting != '')
                        {
                            ?>
                                <p><?= CURRENCY_NAME.' '.$quoteResult->price_with_fitting; ?></p>
                            <?php
                        }
                    ?>
                </td>
            </tr>
            <tr>
                <th width="20%" class="text-center">Price Without Fitting</th>
                <td colspan="2">
                    <?php
                        if($quoteResult->offer_start_date_without < time() && $quoteResult->offer_end_date_without > time())
                        {
                            if($quoteResult->price_before_offer_without != '')
                            {
                                ?>
                                    <p><?= CURRENCY_NAME.' '.$quoteResult->price_before_offer_without; ?></p>
                                <?php
                            }
                        }
                        elseif($quoteResult->price_without_fitting != '')
                        {
                            ?>
                                <p><?= CURRENCY_NAME.' '.$quoteResult->price_without_fitting; ?></p>
                            <?php
                        }
                    ?>
                </td>
            </tr>
        </table>