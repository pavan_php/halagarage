<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="panel_s section-heading section-announcements">
    <div class="panel-body">
        <!--<h4 class="no-margin section-text"><?php echo _l($title); ?></h4>-->
        <div class="header-wrap p-10 bg-white">
            <div class="d-flex align-items-center">
                <div>
                    <h4 class="fw-700 mt-0">My Quotes</h4>
                </div>
                <div class="ml-auto align-items-center">
                    <div class="dl">
                        <form class="form-inline">
                            <div class="form-group">
                                <select class="form-control selectpicker" data-live-search="true">
                                    <option>Select filter</option>
                                    <option>Status</option>
                                    <option>Quote date</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input class="search__input" type="text" placeholder="Search">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-theme">Reset</button>
                            </div>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel_s">
    <div class="panel-body">
        <div class="stats-wrap p-10 bg-white">
            <ul class="responsive-table">
                <li class="table-header">
                    <div class="col col-1">Id</div>
                    <div class="col col-2">Name</div>
                    <div class="col col-3">Email</div>
                    <div class="col col-4">Quote Date</div>
                    <div class="col col-5">Quote Status</div>
                    <div class="col col-6">Quote price</div>
                    <div class="col col-7">Action</div>
                </li>
                <li class="table-row">
                    <div class="col col-1">#001</div>
                    <div class="col col-2">Tanmay Sasvadkar</div>
                    <div class="col col-3">tanmay@gmail.com</div>
                    <div class="col col-4">13 May 2020</div>
                    <div class="col col-5"><span class="badge badge-active">Confirmed</span></div>
                    <div class="col col-6">$200.00</div>
                    <div class="col col-7">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-confirm mr-3" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
                            <button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </li>
                <li class="table-row">
                    <div class="col col-1">#002</div>
                    <div class="col col-2">Pavan Prajapati</div>
                    <div class="col col-3">pavan@gmail.com</div>
                    <div class="col col-4">10 June 2020</div>
                    <div class="col col-5"><span class="badge badge-active">Confirmed</span></div>
                    <div class="col col-6">$130.00</div>
                    <div class="col col-7">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-confirm mr-3" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
                            <button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </li>
                <li class="table-row">
                    <div class="col col-1">#003</div>
                    <div class="col col-2">Krishnpal Yadav</div>
                    <div class="col col-3">krishna@gmail.com</div>
                    <div class="col col-4">03 June 2020</div>
                    <div class="col col-5"><span class="badge badge-cancelled">Cancelled</span></div>
                    <div class="col col-6">$90.00</div>
                    <div class="col col-7">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-confirm mr-3" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
                            <button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </li>
                <li class="table-row">
                    <div class="col col-1">#004</div>
                    <div class="col col-2">Basant Yaduvanshi</div>
                    <div class="col col-3">basant@gmail.com</div>
                    <div class="col col-4">10 Apr 2020</div>
                    <div class="col col-5"><span class="badge badge-pending">Pending Confirmation</span></div>
                    <div class="col col-6">$89.00</div>
                    <div class="col col-7">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-confirm mr-3" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
                            <button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </li>
                <li class="table-row">
                    <div class="col col-1">#005</div>
                    <div class="col col-2">Manas Phadnis</div>
                    <div class="col col-3">manas@gmail.com</div>
                    <div class="col col-4">23 May 2020</div>
                    <div class="col col-5"><span class="badge badge-cancelled">Cancelled</span></div>
                    <div class="col col-6">$250.00</div>
                    <div class="col col-7">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-confirm mr-3" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
                            <button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
