<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style>
    .bootstrap-select .btn-default{border-radius:4px;}
	.navigationbadge {
		background: #c10000!important;
		padding: 5px 9px!important;
		border-radius: 5px!important;
		position: absolute;
		top: 22px;
		right: 0px;
		color:#ffffff!important;
	}
</style>
<div class="panel_s section-heading section-announcements">
    <div class="panel-body">
        <!--<h4 class="no-margin section-text"><?php echo _l($title); ?></h4>-->
        <div class="header-wrap p-10 bg-white">
            <h4 class="fw-700 mt-0">Publish Product</h4>
        </div>
    </div>
</div>
<div class="panel_s section-heading section-publish-product">
    <div class="panel-body">
    <div class="stepwizard">
        <div class="stepwizard-row setup-panel tyre-product">
            <div class="stepwizard-step col-xs-2"> 
                <a href="#step-1" type="button" class="btn btn-confirm btn-circle">1</a>
                <p><small>Product</small></p>
            </div>
            <div class="stepwizard-step col-xs-2"> 
                <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                <p><small>Category</small></p>
            </div>
            <div class="stepwizard-step col-xs-2"> 
                <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                <p><small>Vehicle Filter</small></p>
            </div>
            <div class="stepwizard-step col-xs-2"> 
                <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                <p><small>Tyre Brand</small></p>
            </div>
            <div class="stepwizard-step col-xs-2"> 
                <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                <p><small>Description</small></p>
            </div>
            <div class="stepwizard-step col-xs-2"> 
                <a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
                <p><small>Price</small></p>
            </div>
        </div>
        <div class="stepwizard-row setup-panel notyre-product hide">
            <div class="stepwizard-step col-xs-2"> 
                <a href="#step-1" type="button" class="btn btn-confirm btn-circle">1</a>
                <p><small>Product</small></p>
            </div>
            <div class="stepwizard-step col-xs-2"> 
                <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                <p><small>Category</small></p>
            </div>
            <div class="stepwizard-step col-xs-2"> 
                <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                <p><small>Vehicle Filter</small></p>
            </div>
            <div class="stepwizard-step col-xs-2"> 
                <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                <p><small>Description</small></p>
            </div>
            <div class="stepwizard-step col-xs-2"> 
                <a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                <p><small>Price</small></p>
            </div>
        </div>
        
        <div class="stepwizard-row setup-panel yestyre-product hide">
            <div class="stepwizard-step col-xs-2"> 
                <a href="#step-1" type="button" class="btn btn-confirm btn-circle">1</a>
                <p><small>Product</small></p>
            </div>
            <div class="stepwizard-step col-xs-2"> 
                <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                <p><small>Category</small></p>
            </div>
            <div class="stepwizard-step col-xs-2"> 
                <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                <p><small>Tyre Brand</small></p>
            </div>
            <div class="stepwizard-step col-xs-2"> 
                <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                <p><small>Description</small></p>
            </div>
            <div class="stepwizard-step col-xs-2"> 
                <a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                <p><small>Price</small></p>
            </div>
        </div>
    </div>
    
    <?php echo form_open_multipart($this->uri->uri_string(),["role"=>"form", "onsubmit" => "return productValidateForm()", "id" => "productfrom"]); ?>
    <?php echo validation_errors('<div class="alert alert-danger text-center">', '</div>'); ?>
        <div class="panel panel-primary setup-content" id="step-1">
            <div class="panel-heading">
                <h3 class="panel-title">Product</h3>
            </div>
            <div class="panel-body">
                <div class="form-group col-xs-12">
                    <label class="control-label">Product Name</label>
                    <input maxlength="100" type="text" required="required" name="product_name" class="form-control" placeholder="Enter product name" />
                </div>
                <div class="form-group col-xs-12">
                    <label class="control-label">Product Id</label>
                    <input maxlength="100" type="text" required="required" class="form-control" placeholder="#<?= $product_id; ?>" disabled/>
                </div>
                <div class="form-group col-xs-12">
                    <button class="btn btn-confirm nextBtn pull-right" type="button">Next</button>
                </div>
            </div>
        </div>
        
        <div class="panel panel-primary setup-content" id="step-2">
            <div class="panel-heading">
                <h3 class="panel-title">Product Category</h3>
            </div>
            <div class="panel-body">
                <div class="form-group col-xs-12 col-md-6">
                    <label class="control-label">Choose Category</label>
                    <select class="selectpicker form-control" name="category" id="category" required="required" onChange="getSubCategory(this.value);" data-live-search="true">
                        <option value=""></option>
                        <?php
                            foreach($category_result as $res)
                            {
                                ?>
                                    <option value="<?= $res->id?>"><?= $res->name; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group col-xs-12 col-md-6">
                    <label class="control-label">Choose Sub Category</label>
                    <select class="selectpicker form-control" name="subcategory[]" required="required" multiple id="sub_category_id" data-live-search="true">
                        
                    </select>
                </div>
                <div class="form-group col-xs-12">
                    <button class="btn btn-confirm nextBtn pull-right" type="button">Next</button>
                </div>
            </div>
        </div>
        
        <div class="panel panel-primary setup-content" id="step-3">
            <div class="panel-heading">
                <h3 class="panel-title">Vehicle Filter</h3>
            </div>
            <div class="panel-body">
                <div class="form-group col-xs-12">
                    <span class="btn btn-warning pull-right" style="cursor:pointer" onClick="addMoreVehicleFilter();">Add</span>
                </div><hr class="hr-panel-heading">
                <div class="stepfirst">
                    <div class="form-group col-xs-12 col-md-3">
                        <label class="control-label">Choose Car Category</label>
                        <select class="selectpicker form-control" name="car_category[]" data-live-search="true">
                            <option value="">Select category</option>
                            <option value="Sedan">Sedan</option>
                            <option value="SUV">SUV</option> 
                            <option value="Truck">Truck</option> 
                        </select>
                    </div>
                    <div class="form-group col-xs-12 col-md-3">
                        <label class="control-label">Choose Car Brand</label>
                        <select class="selectpicker form-control" data-live-search="true" name="car_brand[]" id="make_id" onChange="getMultiModelList();">
                            <option value="">Select brand</option>
                            <?php
                                if($carmake_result)
                                {
                                    foreach($carmake_result as $rrr){
                                        ?>
                                            <option value="<?= $rrr->id?>"><?= $rrr->name; ?></option>
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-xs-12 col-md-3">
                        <label class="control-label">Choose Car Model</label>
                        <select class="selectpicker form-control" name="car_model[]" id="model_id" data-live-search="true" onChange="getMultiYearList();">
                            
                        </select>
                    </div>
                    <div class="form-group col-xs-12 col-md-3">
                        <label class="control-label">Choose Make Year</label>
                        <select class="selectpicker form-control" id="year_id" name="make_year[]" data-live-search="true">
                        </select>
                    </div>
                </div>
                <div class="stepsecond">
                </div><hr class="hr-panel-heading">
                <div class="form-group col-xs-12 col-md-12">
                    <button class="btn btn-confirm nextBtn pull-right" type="button">Next</button>
                </div>
            </div>
        </div>
        
        <div class="panel panel-primary setup-content" id="step-4">
            <div class="panel-heading">
                <h3 class="panel-title">Tyre Brand</h3>
            </div>
            <div class="panel-body">
                <div class="form-group col-xs-12 col-md-12">
                    <label class="control-label">Choose Tyre Brand</label>
                    <select class="selectpicker form-control" name="tyre_brand" data-live-search="true" id="brand_id" onChange="getWidthList(this.value)">
                        <option value="">Please Select</option>
                        <?php
                            if($tyrebrand_result)
                            {
                                foreach($tyrebrand_result as $res)
                                {
                                    ?>
                                        <option value="<?= $res->id; ?>"><?= $res->name; ?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group col-xs-12 col-md-4">
                    <label class="control-label">Width</label>
                    <select class="selectpicker form-control" data-live-search="true" name="width" id="width_id" onChange="getHeightList(this.value);">
                    </select>
                </div>
                <div class="form-group col-xs-12 col-md-4">
                    <label class="control-label">Height</label>
                    <select class="selectpicker form-control" name="height" data-live-search="true" id="height_id" onChange="getRimList(this.value);">
                    </select>
                </div>
                <div class="form-group col-xs-12 col-md-4">
                    <label class="control-label">Rim Size</label>
                    <select class="selectpicker form-control" id="rim_id" name="rim_size" data-live-search="true">
                    </select>
                </div>
                <div class="form-group col-xs-12 col-md-12">
                    <button class="btn btn-confirm nextBtn pull-right" type="button">Next</button>
                </div>
            </div>
        </div>

        <div class="panel panel-primary setup-content" id="step-5">
            <div class="panel-heading">
                 <h3 class="panel-title">Product Description</h3>
            </div>
            <div class="panel-body">
                <div class="form-group col-xs-12 col-md-6">
                    <label class="control-label">Product Brand</label>
                    <input maxlength="20" type="text" required="required" name="product_brand" class="form-control" placeholder="Enter Product Brand" />
                </div>
                <div class="form-group col-xs-12 col-md-6">
                    <label class="control-label">Detailed Description</label>
                    <!--<input maxlength="200" type="text" required="required" name="description" class="form-control" placeholder="Enter Description" />-->
                    <textarea required="required" name="description" class="form-control" placeholder="Enter Description"></textarea>
                </div>
                <div class="form-group col-xs-12 col-md-6">
                    <div class="row">
                        <div class="col-md-6 col-xs-9">
                            <label class="control-label">Product Photos</label>
                            <input name="product" accept=".jpg,.png,.jpeg,.gif,image/jpeg,image/png,image/jpg,image/gif" type="file"/>
                        </div>
                        <div class="col-md-6 col-xs-3 text-right"><br>
                            <span class="btn btn-info btn-sm" style="cursor:pointer" onclick="productMoreImg();"><i class="fa fa-plus" aria-hidden="true"></i></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="moreimg">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-md-12">
                    <button class="btn btn-confirm nextBtn pull-right" type="button">Next</button>
                </div>
            </div>
        </div>

        <div class="panel panel-primary setup-content" id="step-6">
            <div class="panel-heading">
                <h3 class="panel-title">Product Price</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="form-group col-xs-12 col-md-3">
                        <label class="control-label">Price with fitting</label>
                        <input maxlength="6" onkeypress="javascript:return isNumber(event)" type="text" name="price_with_fitting" id="price_with_fitting" class="form-control" placeholder="Price with fitting" />
                    </div>
                    <div class="form-group col-xs-12  col-md-4 hide">
                        <label class="control-label">Fitting Filter</label><br>
                        <input type="text" class="hide" id="fittingfilter" name="fitting_filter" value="0">
                        <button type="button" class="btn btn-lg btn-toggle" id="fitting_filter" data-toggle="button" aria-pressed="false" onClick="getfittingFilter()" autocomplete="off">
                            <div class="handle"></div>
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12 col-md-3">
                        <label class="control-label">Price during offer</label>
                        <input maxlength="6" onkeypress="javascript:return isNumber(event)" type="text" name="price_before_offer" class="form-control" placeholder="Price before offer" />
                    </div>
                    <div class="form-group col-xs-12 col-md-3 hide">
                        <label class="control-label">Price after offer</label>
                        <input maxlength="6" onkeypress="javascript:return isNumber(event)" type="text" name="price_after_offer" class="form-control" placeholder="Price after offer" />
                    </div>
                    <div class="form-group col-xs-12 col-md-3">
                        <div id="filterDate2">
                            <label class="control-label">Offer Start date</label>
                            <div class="input-group date" data-date-format="dd.mm.yyyy">
                                <input  type="text" autocomplete="off" class="form-control" name="offer_start_date" placeholder="dd.mm.yyyy">
                                <div class="input-group-addon" >
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>    
                    </div>
                    <div class="form-group col-xs-12 col-md-3">
                        <div id="filterDate3">
                            <label class="control-label">Offer expiry date</label>
                            <div class="input-group date" data-date-format="dd.mm.yyyy">
                                <input  type="text" autocomplete="off" class="form-control" name="offer_end_date" placeholder="dd.mm.yyyy">
                                <div class="input-group-addon" >
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div><hr class="hr-panel-heading">
                <div class="row">
                    <div class="form-group col-xs-12 col-md-3">
                        <label class="control-label">Price without fitting</label>
                        <input maxlength="6" type="text" onkeypress="javascript:return isNumber(event)" name="price_without_fitting" id="price_without_fitting" class="form-control" placeholder="Price without fitting" />
                    </div>
                    <div class="form-group col-xs-12 col-md-4 hide">
                        <label class="control-label">Fitting Filter</label><br>
                        <input type="text" class="hide" id="fittingfilterwithout" name="fitting_filterwithout" value="0">
                        <button type="button" class="btn btn-lg btn-toggle" id="fitting_filterwithout" data-toggle="button" aria-pressed="false" onClick="getfittingFilterWithout()" autocomplete="off">
                            <div class="handle"></div>
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12 col-md-3">
                        <label class="control-label">Price during offer</label>
                        <input maxlength="6" onkeypress="javascript:return isNumber(event)" type="text" name="price_before_offer_without" class="form-control" placeholder="Price before offer" />
                    </div>
                    <div class="form-group col-xs-12 col-md-3 hide">
                        <label class="control-label">Price after offer</label>
                        <input maxlength="6" onkeypress="javascript:return isNumber(event)" type="text" name="price_after_offer_without" class="form-control" placeholder="Price after offer" />
                    </div>
                    <div class="form-group col-xs-12 col-md-3">
                        <div id="filterDate3">
                            <label class="control-label">Offer Start date</label>
                            <div class="input-group date" data-date-format="dd.mm.yyyy">
                                <input type="text" autocomplete="off" class="form-control" name="offer_start_date_without" placeholder="dd.mm.yyyy">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>    
                    </div>
                    <div class="form-group col-xs-12 col-md-3">
                        <div id="filterDate3">
                            <label class="control-label">Offer expiry date</label>
                            <div class="input-group date" data-date-format="dd.mm.yyyy">
                                <input type="text" autocomplete="off" class="form-control" name="offer_end_date_without" placeholder="dd.mm.yyyy">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>
                <div class="row pricefield_validation" style="display:none">
                    <div class=" col-xs-12 col-md-12">
                        <p class="text-danger pull-right">Please fill atleast one field from price with fitting Or price without fitting</p>
                    </div>
                </div>
                <hr class="hr-panel-heading">
                <a href="<?= site_url('myProducts/publishProduct'); ?>" class="btn btn-appologise pull-right">Cancel & Go Back</a>
                <button class="btn btn-confirm pull-right mr-10" name="submit" value="save" type="submit">Confirm/ Publish Product</button>
                <button class="btn btn-theme pull-right mr-10" name="submit" value="draft" type="submit">Save and Continue Later</button>
            </div>
        </div>
    </form>
    </div>
</div>
<script>
    /* validateForm */
    function productValidateForm()
    {
        var price_with_fitting = $('#price_with_fitting').val();
        var price_without_fitting = $('#price_without_fitting').val();
        if(price_with_fitting == '' && price_without_fitting == '')
        {
            $('.pricefield_validation').css('display','block');
            return false;
        }
        else
        {
            $('.pricefield_validation').css('display','none');
            return true;
        }
    }
 
    $(document).ready(function () {

        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');
        
        allWells.hide();
        
        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);
        
            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-confirm').addClass('btn-theme');
                $item.addClass('btn-confirm');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
                $target.find('textarea:eq(0)').focus();
                $target.find('select:eq(0)').focus();
            }
        });
        
        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url'],textarea,select"),
                isValid = true;
        
            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }
        
            if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
        });
        
        $('div.setup-panel div a.btn-confirm').trigger('click');
    });
</script>
<script type="text/javascript">
    $('.input-group.date').datepicker({format: "dd.mm.yyyy", minDate:0, autoclose: true}); 
    
    function getfittingFilter()
    {
        if($('#fitting_filter').hasClass('active')){
        	var fitting  =  0;
	    }else{
        	var fitting  =  1;
        }
        $('#fittingfilter').val(fitting);
    } 
    
    function getfittingFilterWithout()
    {
        if($('#fitting_filterwithout').hasClass('active')){
        	var fitting  =  0;
	    }else{
        	var fitting  =  1;
        }
        $('#fittingfilterwithout').val(fitting);
    } 
    
    /*
    $("#fitting_filter").change(function(){ alert('ok');
        if($(this).prop("checked") == true){
           alert('true');
        }else{
           alert('false');
        }
    });
    */
    
    /* Product More Img */
    var clicks = 0;
    function productMoreImg()
    {
        clicks += 1;
        if(clicks < 7)
        {
            $('.moreimg').append('<input accept=".jpg,.png,.jpeg,.gif,image/jpeg,image/png,image/jpg,image/gif" type="file" name="productimg'+clicks+'">');
        }
        else
        {
            alert('You can add only 6 images or less.');
        }
    }
    /*
    var carmake_result = '<?= json_encode($carmake_result); ?>';
    for(var i = 0; i < carmake_result.length; i++)
    {
        //console.log(JSON.parse(carmake_result[i].id));
    }
    console.log('stringify');
    console.log(JSON.stringify('<?= json_encode($carmake_result); ?>'));
    console.log('parse');
    console.log(JSON.parse('<?= json_encode($carmake_result); ?>'));
    var parsearr = JSON.parse('<?= json_encode($carmake_result); ?>');
    console.log(parsearr[0].id);
    console.log('hello');
    console.log(carmake_result);
    */
    var moreclick = 0;
    function addMoreVehicleFilter()
    {
        moreclick += 1;
        var carmake_result = JSON.parse('<?= json_encode($carmake_result); ?>');
        var html = '';
        html +='<div id="moreid_'+moreclick+'">';
        html += '<div class="form-group col-xs-3">';
        html +='<label class="control-label">Choose Car Category</label>';
        html +='<select class="form-control dclass" name="car_category[]" id="car_category'+moreclick+'" required="required" data-live-search="true">';
        html +='<option value="">Select category</option>';
        html +='<option value="Sedan">Sedan</option>';
        html +='<option value="SUV">SUV</option>';
        html +='<option value="Truck">Truck</option>';
        html +='</select>';
        html +='</div>';
        html +='<div class="form-group col-xs-3">';
        html +='<label class="control-label">Choose Car Brand</label>';
        html +='<select class="form-control dclass" data-live-search="true" name="car_brand[]" required="required" id="make_id'+moreclick+'" onChange="getMultiModelListExt('+moreclick+');">';
        html +='<option value="">Select brand</option>';
        for(var s=0; s < carmake_result.length; s++)
        {
            html +='<option value="'+carmake_result[s].id+'">'+carmake_result[s].name+'</option>';
        }
        html +='</select>';
        html +='</div>';
        html +='<div class="form-group col-xs-3">';
        html +='<label class="control-label">Choose Car Model</label>';
        html +='<select class="form-control" name="car_model[]" id="model_id'+moreclick+'" required="required" data-live-search="true" onChange="getMultiYearListExt('+moreclick+');">';
        html +='</select>';
        html +='</div>';
        html +='<div class="form-group col-xs-2">';
        html +='<label class="control-label">Choose Make Year</label>';
        html +='<select class="form-control" id="year_id'+moreclick+'" name="make_year[]" required="required" data-live-search="true">';
        html +='</select>';
        html +='</div>';
        html +='<div class="form-group col-xs-1">';
        html +='<label class="control-label">Remove</label>';
        html +='<span class="btn btn-appologise" style="cursor:pointer" onclick="removeVehicleMoreField('+moreclick+')"><i class="fa fa-trash" aria-hidden="true"></i></span>';
        html +='</div>';
        html +='</div>';
        
        setTimeout(function(){  
            $('.dclass').selectpicker('destroy'); 
            $('.dclass').selectpicker(); 
        }, 1000);
        $('.stepsecond').append(html);
    }
    
    function getMultiModelListExt(id)
    {
        var make_id = $("#make_id"+id).selectpicker().val();
        $('#year_id'+id).html('<option value=""></option>');
        var str = "catid="+make_id+"&"+csrfData['token_name']+"="+csrfData['hash'];
	    $.ajax({
	        url: '<?= site_url()?>myProducts/getMultiModelList',
	        type: 'POST',
	        data: str,
	        dataType: 'json',
	        cache: false,
	        success: function(resp){
	            //console.log(resp);
	            $resp = '<option value=""></option>';
                for(var i=0; i< resp.length; i++)
                {
                    $resp += '<option value="'+resp[i].id+'">'+resp[i].name+'</option>';
                }
                $('#model_id'+id).html($resp);
                setTimeout(function(){  
                    $('#model_id'+id).selectpicker('destroy'); 
                    $('#model_id'+id).selectpicker(); 
                }, 1000);
	        }
	    });
    }

    /* getMultiYearList */
    function getMultiYearListExt(id)
    {
        var make_id = $("#make_id"+id).selectpicker().val();
        var values = $("#model_id"+id).selectpicker().val();
        //var make_id = $( "#make_id option:selected" ).val();
        var str = "catid="+values+"&make_id="+make_id+"&"+csrfData['token_name']+"="+csrfData['hash'];
	    $.ajax({
	        url: '<?= site_url()?>myProducts/getMultiYearList',
	        type: 'POST',
	        data: str,
	        dataType: 'json',
	        cache: false,
	        success: function(resp){
	            //console.log(resp);
	            $resp = '<option value=""></option>';
                for(var i=0; i< resp.length; i++)
                {
                    $resp += '<option value="'+resp[i].id+'">'+resp[i].year+'</option>';
                }
                $('#year_id'+id).html($resp);
                setTimeout(function(){  
                    $('#year_id'+id).selectpicker('destroy'); 
                    $('#year_id'+id).selectpicker(); 
                }, 1000);
	        }
	    });
    }
    
    function removeVehicleMoreField(id)
    {
        $('#moreid_'+id).empty();   
    }
</script>