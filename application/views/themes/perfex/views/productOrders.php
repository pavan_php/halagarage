<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="panel_s section-heading section-announcements">
    <div class="panel-body">
        <!--<h4 class="no-margin section-text"><?php echo _l($title); ?></h4>-->
        <div class="header-wrap p-10 bg-white">
            <div class="d-flex align-items-center">
                <div>
                    <h4 class="fw-700 mt-0">My Orders</h4>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel_s">
    <div class="panel-body">
        <div class="stats-wrap p-10 bg-white">
            <table id="example" class="responsive-table">
                <thead>
                    <tr class="table-header">
                        <td>Id</td>
                        <td>Customer Name</td>
                        <!--<td>Customer Email</td>-->
                        <td>Product Name</td>
                        <td>Location</td>
                        <!--
                        <td>Pickup Address</td>
                        <td>Delivery Address</td>
                        -->
                        <td>Appointment Date</td>
                        <td>Status</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
					<?php
						if($orderResult)
						{
							$s = 1;
							foreach($orderResult as $rrr)
							{
							    $s++;
							    $customerdetails = $this->db->select('firstname,lastname,email')->get_where(db_prefix().'contacts', array('userid' => $rrr->customer_id))->row();
							    $productname = $this->db->get_where(db_prefix().'_product', array('id' => $rrr->product_id))->row('product_name');
							    $address_ = $this->db->get_where(db_prefix().'clients', array('userid' => $rrr->garage_id))->row('address');
								?>
									<tr>
										<td><?= $rrr->id; ?></td></td>
										<td><?= $customerdetails->firstname.' '.$customerdetails->lastname; ?></td>
										<!--<td><?= $customerdetails->email; ?></td>-->
										<td><?= $productname; ?></td>
										<td><?= $address_; ?></td>
										<!--
										<td><?= $rrr->pickup_address; ?></td>
										<td><?= $rrr->delivery_address; ?></td>
										-->
										<td><?= date('d M Y', $rrr->appointment_date); ?></td>	
										<td>
										    <?php
										        if($rrr->appointment_date > time())
										        {
										            ?>
										                <span class="badge badge-pending">Pending</span>
										            <?php
										        }
										        elseif($rrr->status == 1)
										        {
										            ?>
										                <span class="badge badge-active">Confirmed</span>
										            <?php
										        }
										        else
										        {
										            ?>
										                <span class="badge badge-cancelled">Expired</span>
										            <?php
										        }
										    ?>
										</td>
										<td>
											<a href="javascript:void(0);">
												<button type="button" class="btn btn-re-activate mr-3" data-toggle="tooltip" title="Reschedule appointment"><i class="fa fa-calendar" aria-hidden="true"></i></button>
											</a>
										</td>
									</tr>
								<?php
							}
							if($s == 1)
							{
								?>
									<tr>
										<td valign="top" colspan="8" class="dataTables_empty">No matching records found</td>
									</tr>
								<?php
							}
						}
						else
						{
							?>
								<tr>
									<td valign="top" colspan="8" class="dataTables_empty">No matching records found</td>
								</tr>
							<?php
						}
					?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    function tagsNavBar() {
        $(".tag-dropdown").on("click", function(event) {
            event.stopPropagation();
            if (!$(this).hasClass('active')) {
                $('.tag-nav').removeClass('hidden-xs');
                $(this).addClass('active');
                $(this).next("i").addClass('active');
            } else {
                $('.tag-nav').addClass('hidden-xs');
                $(this).removeClass('active');
                $(this).next("i").removeClass('active');
            }
        });
        $(document).click(function() {
            $('.tag-nav').addClass('hidden-xs');
            $('.tag-dropdown').removeClass('active');
            $('.tag-dropdown').next("i").removeClass('active');
        });
        $(".tag-nav a").on("click", function() {
            var tagText = $(this).text();
            $(".tag-dropdown").text(tagText);
        });
    }
    
    //Filter TAB
    function filterTab(){
    	var tablist=$('.tag-nav .tags')
    	tablist.bind("click",function(){
    		$('.tag-nav .tags').removeClass("selected");
    		$(this).addClass("selected");
    		$('.tab-content').hide();	
    		var show2Tab=$('.'+$(this).attr('data-rel'));		
    		show2Tab.fadeIn();
    	});
    }
</script>