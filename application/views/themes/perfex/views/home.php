<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="panel_s">
			<div class="video-wrap w-100">
			    <?php
			        if($video_customer)
			        {
			            $imageArray2 = $this->db->get_where(db_prefix().'files', array('rel_id' => $video_customer->id, 'rel_type' => "customervideo"))->row();
			            $code1 = $video_customer->code;
		                $url1 = '';
		                if($code1 != '')
		                {
		                    ?>
		                        <iframe width="100%" height="450" src="https://www.youtube.com/embed/<?= $code1; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		                    <?php
		                }
		                else
		                {
		                    $url1 = site_url('download/file/taskattachment/'. $imageArray2->attachment_key);
		                    ?>
		                        <video width="100%" height="450" controls>
                                    <source src="<?= $url1; ?>" type="video/mp4">
                                </video>
		                    <?php
		                }
			        }
			        else
			        {
			            ?>
			                <iframe width="100%" height="450" src="https://www.youtube.com/embed/SGL2pFyNVTQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			            <?php
			        }
			    ?>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6 col-sm-6 text-center">
		<a href="<?= site_url('myProducts/publishProduct'); ?>" class="btn btn-info btn-md btn-dashboard">Publish Product</a>
	</div>
	<div class="col-md-6 col-sm-6 text-center">
		<a href="<?= site_url('start-quote'); ?>" class="btn btn-info btn-md btn-dashboard">Start Quote</a>
	</div>
</div>