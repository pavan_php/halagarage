<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!--<link href="https://cdnjs.cloudflare.com/ajax/libs/emojionearea/3.4.2/emojionearea.css" />-->
<link rel="stylesheet" href="<?php echo base_url('assets/css/chat.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/emojionearea.css'); ?>">
<div class="panel_s p-0">
    <div class="panel-body p-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="chat " id="desktop-chat" @keypress="sendMsg">
                        <div class="row  br-8 desktop-chat">
                              <!-- <div class=""> -->
                              <div class="col-lg-4 p-0">
                                 <div class="bg-white br-1 br-l-6">
                                    <div>
                                       <div class="search-box border-0 pr-3 pt-2 pb-2">
                                          <input type="text" class="form-control fz-16" placeholder="Search" v-model.trim="u_search" @keyup="user_search">
                                          <input type="hidden" v-model="user_to">
                                          <input type="hidden" v-model="last_msg">
                                          <input type="hidden" v-model="height">
                                          <input type="hidden" v-model="last_date"> 
                                       </div>
                                       <ul class="chat-list mb-0">
                                          <li v-for="user in users" :class="`bb-1 bt-1${user.user_id == user_to ? ' selected-chat' : ''}`" onclick="chatListShow()">
                                             <div class="d-flex p-3" @click="getChat(user.user_id)"  v-if="user.is_blocked == 0" :id="'user_id_'+user.user_id">
                                                <div class="row w-100">
                                                   <div class="col-md-3"><!--
                                                      <img v-if="user.profile_picture_path" :src="user.profile_picture_path" class="img-fluid img-shadow" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';">
                                                      <img v-else src="<?php echo  base_url(); ?>assets/images/user-placeholder.jpg" class="img-fluid img-shadow">
                                                      -->
                                                   </div>
                                                   <div class="col-md-9 ">
                                                      <div class="row">
                                                         <div class="col-md-6 p-0">
                                                            <h6 class="text-black"><span class="fw700">{{ user.name }}  <b v-if="user.count" class="fz-16 fw400 badge-secondary">{{ user.count }}</b></span><br></h6>
                                                         </div>
                                                         <div class="col-md-6 text-right ">
                                                            <span class=" pt-2 login-subtext fz-12">{{ user.time_differ }}</span>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <h6><span class="sub-head-text" style="color: #F2613C !important;" v-if="user.is_typing>0">{{ user.msg }}</span><span class="sub-head-text" v-else>{{ user.msg }}</span></h6>
                                                         
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                              <div class="d-flex p-3"  @click="getBlockChat(user.user_id)"  v-if="user.is_blocked == 1" :id="'user_id_'+user.user_id">
                                                <div class="row w-100">
                                                   <div class="col-md-3"><!--
                                                      <img v-if="user.profile_picture_path" :src="user.profile_picture_path" class="img-fluid img-shadow" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';">
                                                      <img v-else src="<?php echo  base_url(); ?>assets/images/user-placeholder.jpg" class="img-fluid img-shadow">
                                                      -->
                                                   </div>
                                                   <div class="col-md-9 ">
                                                      <div class="row">
                                                         <div class="col-md-6 p-0">
                                                            <h6 class="text-black"><span class="fw700">{{ user.name }}  <b v-if="user.count" class="fz-16 fw400 badge-secondary"></b></span><br></h6>
                                                         </div>
                                                         <div class="col-md-6 text-right ">
                                                            <span class=" pt-2 login-subtext fz-12"></span>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <h6><span class="sub-head-text" style="color: #F2613C !important;" v-if="user.is_typing>0"></span><span class="sub-head-text" v-else></span></h6>
                                                         
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                                 <!-- card -->
                              </div>
                              
                              <!-- <div class="col-lg-9 p-0 chat-bg-img br-r-6" v-if="is_MsgScreen"> -->
                              <div class="col-lg-8 p-0 chat-bg-img br-r-6">
                                 <div class="bg-white br-r-6">
                                    <div class="chat-header">
                                       <div class="chat-header-user">
                                          <figure class="avatar">
                                             <!--<img src="http://php.manageprojects.in/halagarage/assets/images/user-placeholder.jpg" class="rounded-circle" alt="image">-->
                                          </figure>
                                          <div>
                                             <h5 class="mb-0 mt-0 companyname_"></h5>
                                             <small class="text-success hide">
                                                   <i class="typing" v-if="is_typing"><?php echo _l('typing...');?></i>
                                             </small>
                                          </div>
                                       </div>
                                    </div>
                                    <!--<div class="text-right bb-1 pb-3 pl-4  pr-4">
                                       <button class="btn btn-outline-danger btn-sm" type="button" @click="deleteChatHistory">
                                          <i class="fa fa-trash" aria-hidden="true"></i> Delete Chat
                                       </button>
                                       <--<button class="btn setting-btn " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          <i class="fa fa-cog" aria-hidden="true"></i> <i class="fa fa-chevron-down ml-2 fz-14 login-subtext"></i> 
                                       </button>
                                       <div class="dropdown-menu shadow-lg chat-delete fz-16 fw600" aria-labelledby="dropdownMenuButton" @click="deleteChatHistory">
                                          <a class="" href="#"  >
                                             <span class="text-dark"><?php echo _l('delete_conversation');?></span>
                                          </a>
                                       </div>->
                                    </div>-->
                                    <div class="chat-screen pt-4 active-y"  id="info-box" ref="infoBox" @scroll="SrollUp">
                                       <div class="">
                                          <div class="panel-body msg_container_base">
                                             <div class="text-center" v-if="user_name"><span  v-if="user_name" class="chat-date"><?php echo _l('Chat start with') ?> <b>{{ user_name }}</b></span></div><br>
                                             <div v-for="(msg, index) in chat_msgs" :class="`msg_container${msg.chat_with == user_to ? ' base_sent' : ' base_receive'}`">
                                                <div class="text-center" v-if="msg.is_new_date == 1"><span class="chat-date">{{ msg.date }}</span>
                                                </div>
                                                <div class="row">
                                                   <div class="col-md-1 col-xs-1 p-0  avatar 1" :style="`display:${msg.chat_with == user_to ? 'none' : 'block'}`">
                                                  
                                                      <img v-if="msg.image" :src="profileImg(msg)" class="img-fluid">
                                                   </div>
                                                   <div class="col-md-1 col-xs-1 p-0  avatar 2" :style="`display:${msg.chat_with == user_to ? 'block' : 'none'}`">
                                                   </div>
                                                   <div class="col-md-11">
                                                      <div :class="`messages${msg.chat_with == user_to ? ' msg_sent' : ' msg_receive'}`">
                                                         <div v-if="msg.is_delete == 0" class="dropdown">
                                                           <button class="  dropdown-toggle option-icon" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                             <i class="fa fa-ellipsis-v" ></i>
                                                           </button>
                                                           <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                             <a class="dropdown-item" @click="deleteMsg(index)"><?php echo _l('delete');?></a>
                                                           </div>
                                                         </div>
                                                         <h6 class="text-black" :style="`display:${msg.chat_with == user_to ? 'none' : 'block'}`"><span class="fw700">{{ msg.username }}</span></h6>
                                                         <p :class="`d-${msg.is_delete == 0 ? 'none' : 'block'}`">&#8856;&nbsp;&nbsp;<i><?php echo _l('this_message_was_deleted ') ?>&emsp;</i></p>
            
                                                         <p :class="`d-${(msg.is_delete == 1 || msg.is_type != 0) ? 'none' : 'block'}`" v-html="msg.msg" >
                                                           <!--  <span v-if="msg.is_anc=1" v-for="link in msg.anc_array"><a v-if="msg.is_anc=1"  :href="link" target="_blank">{{ link }}</a><br></span> -->
                                                         </p>
            
                                                         <p v-if="msg.is_delete == 1 || msg.is_type==1">
                                                            <a :href="'<?php echo base_url();?>uploads/chat/'+msg.msg" target="_blank">
                                                               <div v-if="msg.is_type==1" class="chat-img-div">
                                                                  <img :class="`d-${msg.is_delete == 1 ? 'none' : 'block'}`" :src="'<?php echo base_url();?>uploads/chat/'+msg.msg" class=" img-fluid chat-msg-img">
                                                               </div>
                                                            </a>
                                                         </p>
            
                                                         <p v-if="msg.is_delete == 1 || msg.is_type==2">
                                                            <a :href="'<?php echo base_url();?>uploads/chat/'+msg.msg" target="_blank">
                                                               <div v-if="msg.is_delete==0 && msg.is_type==2">
                                                                  <div class="input-group mb-2">
                                                                    <div class="input-group-prepend">
                                                                      <div class="input-group-text"><img src="<?php echo  base_url(); ?>uploads/attachment.svg" class="w-100"></div>
                                                                    </div>
                                                                    <input type="text" disabled class="form-control" :value="msg.msg">
                                                                  </div>
                                                               </div>
                                                            </a>
                                                         </p>
                                                         <time datetime="2009-11-13T20:00">{{ msg.time }}</time>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <!-- send message input -->
                                    <div class="text-right chat-message-box b-t-r-r d-none">
                                       <!-- <i class="typing" v-if="is_typing"><?php echo _l('typing...');?></i> -->
                                       <input type="file" id="uploadimage" @change="uploadingimage" class="d-none" accept=".png,.PNG,.jpeg,.JPEG,.jpg,.JPG,.mp4,.3gp,.3gpp,.mkv,.avi" name="image"/>
                                       <input type="file" id="uploadfile" @change="uploadingfile" class="d-none" accept=".csv,.xls,.xlsx,.word,.doc,.docx,.pdf,.ppt,.pptx,.txt,.html,.htm" name="image"/>
                                          <div class="row file_name" v-if="is_post">
                                             <div class="col-7 text-left">
                                                <div class="row">
                                                   <div class="col-2">
                                                      <img v-if="url" :src="url" class="img-fluid" style="max-height: 47px;">
                                                      <img v-else src="<?php echo  base_url(); ?>uploads/file.png" class="img-fluid" style="max-height: 47px;">
                                                   </div>
                                                   <div class="col-10">
                                                      <p class="fz-16 fw600" v-if="is_picture">{{ filename }}</p>
                                                      <p class="fz-16 fw600" v-if="is_file">{{ filename }}</p>
                                                      <p class="fz-16 fw600"><?php echo _l('attachment_size ') ?>: {{ attech_size }} <?php echo _l('MB') ?></p>
                                                   </div>
                                                </div>                                 
                                             </div>
                                             <div class="col-5" v-if="is_post">
                                                <input type="submit" @click="onUpload" class="btn btn-success post-btn" value="POST"><span style="margin-left: 12
                                                px;" @click="cancelSending"><img src="<?php echo  base_url(); ?>uploads/cross.png" class="img-fluid"></span>
                                             </div>
                                          </div>
                                       <div class="popover_file d-none">
                                          <div class="row">
                                             <div class="col-4 attechment" @click="fileUpload">
                                                <i class="fa fa-file" aria-hidden="true"></i>
                                             </div>
                                             <div class="col-4 picture" @click="pichture">
                                                <i class="fa fa-picture-o" aria-hidden="true"></i>
                                             </div>
                                          </div>
                                          
                                       </div>
                                       <div>
                                          <div class="input-group">
                                             <message-box v-model="send_data" :data-position="position"></message-box>
                                             <div class="input-group-append">
                                                <span class="input-group-text b-t-r-r atteched-btn"  @click="atteched"><i class="fa fa-paperclip atteched-btn" aria-hidden="true"></i></span>
                                             </div>
                                          </div>
                                       </div>                           
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <script src="<?= base_url() ?>assets/vue/vue.min.js"></script>
    <script src="<?= base_url() ?>assets/vue/vue.js"></script>
    <script src="<?= base_url() ?>assets/vue/axios.min.js"></script>

    <script src="<?= base_url() ?>assets/chat-css-js/jquery.textcomplete.js"></script> 
    <script src="<?= base_url() ?>assets/chat-css-js/emojionearea.js"></script> 
    <script src="<?= base_url() ?>assets/chat-css-js/vue-resource.min.js"></script> 
    <script src="<?= base_url() ?>assets/chat-css-js/emojione.js"></script> 
    <script src="<?= base_url() ?>assets/chat-css-js/bootstrap-tagsinput.min.js"></script> 
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> 

   <script>
    var base_url   = '<?php echo  base_url(); ?>';
        var companyname_ = '<?= get_company_name(get_client_user_id()) ?>';
        $('.companyname_').text(companyname_);
      $(function() {
         $(".service-panel-toggle").on('click', function() {
            $(".customizer").toggleClass('show-service-panel');

         });
         $('.page-wrapper').on('click', function() {
            $(".customizer").removeClass('show-service-panel');
         });
      });
      $(".service-panel-toggle").click(function(){
         $(".arrow").toggle();
         $(".cross").toggle();
      });
      function new_msg(){
         $(".chat-bg-img").hide();
         $(".compose-msg").show();  
      }

      function chatListShow(){
         mobile_chat.msgId = '';
         $(".chat-bg-img").show();
         $(".compose-msg").hide();
      }
            
      $(document).click(function (e) {
         if (!$(e.target).hasClass("atteched-btn") 
            && $(e.target).parents(".popover_file").length === 0) 
         {
            $(".popover_file").addClass('d-none');
         }
      });

      $(document).ready(function(){
         $(".mobile-chat-screen chat-list li").click(function(){
            $(".mobile-chat-screen .chat-bg-img").show();
            $(".mobile-chat-screen .compose-msg1").hide();
            $(".mobile-chat-screen .show-div").hide();
         });
         $(".perv-div").click(function(){
            $(".mobile-chat-screen .chat-bg-img").hide();
            $(".compose-msg1").hide();
            $(".show-div").show();
         });
      });
      // mobile view commpose msg
      $("#new_msg1").click(function(){
         $(".mobile-chat-screen .chat-bg-img").hide();
         $(".show-div").hide();
         $(".compose-msg1").show();
      });
      $(".perv-div").click(function(){
         $(".mobile-chat-screen .chat-bg-img").hide();
         $(".compose-msg1").hide();
         $(".show-div").show();
      });
   </script>
       
   <script>
      $(".searchbox").click(function(){
         $(".search-form").css("display", "flex");
      });
      
      $(".fa-times").click(function(){
         $(".search-form").css("display", "none");
      });
   </script>
     
   <script type="x-template" id="messagebox">        
      <textarea id="call" maxlength="2500" rows="3" class="emoji-area form-control b-t-l-r fz-14 font-weight-bold700" ></textarea>
   </script>
   <script type="text/javascript">
      $('.desktop-chat .chat-message-box,.desktop-chat .typing,.desktop-chat .file_name,.desktop-chat .chat-screen,.desktop-chat .chat-list li, #mobile-chat .chat-list li').attr('style','display:block');
      $('.desktop-chat .file_name').attr('style','display:flex');

      $(".mobile-chat-screen .chat-bg-img").hide();
      $(".mobile-chat-screen .compose-msg1").hide();
      $(".mobile-chat-screen .show-div").show();
            
      Vue.component('message-box', {
         template: '#messagebox',
         mounted(){
         
         
         $(this.$el).emojioneArea({
            placeholder: "Write a Message",
            filtersPosition: "bottom"
         });
         }
      });
   </script>
   <script>
    // var user_from          =   '<?php echo $this->uri->segment('3'); ?>';
    var toUser          =   '<?php echo $this->uri->segment('3'); ?>';
     var recount = '1';

/*  document.querySelector('.chat[data-chat=person1]').classList.add('active-chat')
  document.querySelector('.person[data-chat=person1]').classList.add('active')

  let friends = {
    list: document.querySelector('ul.people'),
    all: document.querySelectorAll('.left .person'),
    name: ''
  },
  chat = {
    container: document.querySelector('.container .right'),
    current: null,
    person: null,
    name: document.querySelector('.container .right .top .name')
  }

  friends.all.forEach(f => {
  f.addEventListener('mousedown', () => {
    f.classList.contains('active') || setAciveChat(f)
  })
  });

  function setAciveChat(f) {
  friends.list.querySelector('.active').classList.remove('active')
  f.classList.add('active')
  chat.current = chat.container.querySelector('.active-chat')
  chat.person = f.getAttribute('data-chat')
  chat.current.classList.remove('active-chat')
  chat.container.querySelector('[data-chat="' + chat.person + '"]').classList.add('active-chat')
  friends.name = f.querySelector('.name').innerText
  chat.name.innerHTML = friends.name
  }
  
   */
   var token_name = csrfData['token_name'];
    var token_hash = csrfData['hash'];
   
var mobile_chat = new Vue({
    el: "#desktop-chat",
    
    data() {
        return { 
          elements:{'front':'#desktop-chat ', 'id':'#','provide':'active','emojione':'.emojionearea-button','makeId':'call','fun':'click','next':'uploadimage','nextfile':'uploadfile','setItem':'','dn':'d-none','popup':'.popover_file','db':'d-block','form':'.form','nextto':'uploadvideo'},
          users : [],
          chat_msgs : [],
          user_from : '<?php echo get_client_user_id(); ?>',
          user_to : '',
          last_msg : '',
          send_data : '',
          height: '',
          last_date:'',
          u_search:'',
          updated_data:'',
          is_show: true,
          is_typing:false,
          is_picture:false,
          position: 0,
          is_file:false,
          is_post:false,
          url:null,
          attech_size:null,
          filename:'',
          max_data:'',
          user_select:'',
          selectedfile:[],
          index_id:'',
          user_name:'',
          job_id:'',
          headers:{
                  'Content-Type':'application/json',
                  'Access-Token':'$2y$10$avVqjvLJhLN2liYyIiyB2.5QkVzQU06wM6LlzkBrKmIq.lBRY2HSK'
                  },
          chat_bg_img:true,
          msgId:'',        
        }
    },
     created() { 
   
        this.getUser();
        this.getChat(toUser);
       
    },    
    mounted: function () {

    },
    computed: {
         
    },
    watch: {

    },
    methods: {
      chatListShow: function(){

      },
        getUser: function(id){

            axios({
        method: 'post',
        url: base_url + 'api/chat/getUser',
        data: {
                    user_from:this.user_from,
                    job_id:this.job_id,
                    token_name:token_hash
        },
      }).then(response => {

          $('#user_id_'+this.msgId).click();
                if (response.data=='') {
                    Swal.fire({
                         icon: 'warning',
                         title: "You haven't start chat",
                         text: 'Please search name wise for starting new conversation!',
                       });
                    this.stopInterval();
                 }else{
                    this.users = response.data.result;
                 }

      }).catch(function (error) {
        console.log(error);
      });
 
          },
          getChat(chat_with, is_scroll=true){
               
            $(this.elements.front+' .chat-screen').removeClass('active-y'); 

            
            if(this.u_search!=''){this.stopInterval();}
            this.user_to = chat_with;
            this.$http.post(base_url+'api/chat/getChat', {"user_from":this.user_from, "user_to":chat_with,token_name:token_hash})
            .then(function(response) {
              // console.log(response.data.chat_data);
              response.data = response.data.result;
            
              
              var chat_array = [];
              var urlRegex = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
              for (var i = 0; i < response.data.chat_data.length; i++) {
                  response.data.chat_data[i].msg = response.data.chat_data[i].msg.replace(urlRegex, "<a href='$1' target='_blank'>$1</a>");
                  chat_array.push(response.data.chat_data[i]);
              }
             //  $(this.elements.front+' .msg_container_base').removeClass(this.elements.dn);
            if(response.data.is_blocked==1){ 
              
             }else{
                    $(this.elements.front+' .chat-message-box').removeClass(this.elements.dn);
             }
             
              this.user_name = response.data.user_name;

              if(recount==1 && response.data.chat_data=='' ){
                //this.getBlockChat(chat_with);
                this.u_search = response.data.user_name;
                this.user_search();
                recount = 0;
              }

              this.chat_msgs='';
              this.last_date= (response.data.last_date[0]=='')?'':response.data.last_date[0].date;
              this.chat_msgs = (response.data.chat_data=='')?'':chat_array;
              if (response.data.last_msg=='') {
                  this.last_msg = '';
              }else{
                if (response.data.last_msg!='')this.last_msg = response.data.last_msg[0].last_msg;
                  
              }
            }).catch(function (error) {
              console.log(error);
            });
            if(is_scroll){ 
            this.scrollDown();
            }
          },
          getBlockChat(chat_with){
            $(this.elements.front+' .chat-screen').removeClass('active-y'); 

            
            if(this.u_search!=''){this.stopInterval();}
            this.user_to = chat_with;
            this.$http.post(base_url+'api/chat/getChat', {"user_from":this.user_from, "user_to":chat_with,token_name:token_hash})
            .then(function(response) {
              // console.log(response.data.chat_data);
              response.data = response.data.result;

              
              var chat_array = [];
              var urlRegex = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
              for (var i = 0; i < response.data.chat_data.length; i++) {
                  response.data.chat_data[i].msg = response.data.chat_data[i].msg.replace(urlRegex, "<a href='$1' target='_blank'>$1</a>");
                  chat_array.push(response.data.chat_data[i]);
              }
             //  $(this.elements.front+' .msg_container_base').removeClass(this.elements.dn);
             //alert(this.elements.dn);
              $(this.elements.front+' .chat-message-box').addClass(this.elements.dn);
              this.user_name = response.data.user_name;
              this.chat_msgs='';
              this.last_date= (response.data.last_date[0]=='')?'':response.data.last_date[0].date;
              this.chat_msgs = (response.data.chat_data=='')?'':chat_array;
              if (response.data.last_msg=='') {
                  this.last_msg = '';
              }else{
               
                if (response.data.last_msg!='')this.last_msg = response.data.last_msg[0].last_msg;
                  
              }
            }).catch(function (error) {
              console.log(error);
            });          
          },
          getChatStatus(){
            if (this.user_to!='') {
               this.$http.post(base_url+'api/chat/getChatStatus', {"user_from":this.user_from, "user_to":this.user_to,token_name:token_hash})
               .then(function(response) {
                  // console.log(response.data);
                  this.is_typing=response.data.result;
               }).catch(function (error) {
                 console.log(error);
               });
            } 
          },
          sendMsg(e) {
            // console.log(e);
            if (this.user_to!='' && this.index_id==''){ 
                if (e.keyCode=== 10) {
                    $(this.elements.front+this.elements.id+this.elements.makeId).emojioneArea()[0].emojioneArea.setText($(this.elements.front+this.elements.id+this.elements.makeId).data("emojioneArea").getText() + "\n");
                    $(this.elements.front+this.elements.id+this.elements.makeId).emojioneArea()[0].emojioneArea.setText($(this.elements.front+this.elements.id+this.elements.makeId).data("emojioneArea").getText() + "\n");
                    var editableElm = document.querySelector('[contenteditable]');
                      editableElm.focus()
                     var sel = document.getSelection();
                     editableElm = editableElm.lastChild;
                   if( sel.rangeCount ){
                       ['Start', 'End'].forEach(pos =>
                         sel.getRangeAt(0)["set" + pos](editableElm, editableElm.length )
                       )
                   }
                } else if(e.keyCode === 13) {
                  this.send_data = ($(this.elements.front+this.elements.id+this.elements.makeId).data("emojioneArea").getText()).trim();
                  // console.log(this.send_data.length);
                  let words = this.send_data.length;
                  if (words<=2500){
                     $(this.elements.front+this.elements.id+this.elements.makeId).emojioneArea()[0].emojioneArea.setText(this.elements.setItem);
                     $(this.elements.front+this.elements.emojione).removeClass(this.elements.provide);
                     if (this.send_data!='' && this.send_data!=' ' && $.trim(this.send_data)!='') {
                     e.preventDefault();
                     
                     this.$http.post(base_url+'api/chat/sendMsg', {"user_from":this.user_from, "user_to":this.user_to, "msg":this.send_data, "last_date":this.last_date,token_name:token_hash})
                     .then(function(response) {
                        response.data  = response.data.result;
                        // console.log(response.data);
                       this.getUser();
                       this.scrollDown();
                       this.last_date= response.data.date;
                       let time =((new Date().getHours() < 10)?"0":"") + ((new Date().getHours()>12)?(new Date().getHours()-12):new Date().getHours()) +":"+ ((new Date().getMinutes() < 10)?"0":"") + new Date().getMinutes() +" "+ ((new Date().getSeconds() < 10)?"":"") + ((new Date().getHours()>12)?('PM'):'AM');
                       if (this.last_msg!=''){
                        this.chat_msgs.push({"chat_from": this.user_from,"chat_with": this.user_to,"msg": this.send_data,"time":time,"date": response.data.date,"is_new_date": response.data.is_new_date,"id": response.data.id,"is_update": 0, "is_delete": 0, "is_type": 0});
                        }
                       this.send_data='';
                       this.max_data='';
                       if(this.u_search!=''){this.u_search='',this.todo();}
                     }).catch(function (error) {
                       console.log(error);
                     });
                    }
                  }else{
                     Swal.fire({
                                icon: 'warning',
                                title: 'Word Limit Exceeded : you entered '+words,
                                text: 'Please send up to 2500 word in a message !',
                              })
                  }
               }else{
                  this.$http.post(base_url+'api/chat/UpdateChatStatus', {"user_from":this.user_from, "user_to":this.user_to,token_name:token_hash})
                  .then(function(response) {
                     setTimeout(function(){
                     this.deleteChatStatus();
                     }.bind(this), 5000);
                  }).catch(function (error) {
                    console.log(error);
                  });
               }
            }
            else{
           //   $(this.elements.front+this.elements.id+this.elements.makeId).emojioneArea()[0].emojioneArea.setText(this.elements.setItem); 
            }                 
          },
          updateMsg(){
            if (this.user_to!='')
               this.$http.post(base_url+'api/chat/updateMsg', {"user_from":this.user_from, "user_to":this.user_to,token_name:token_hash})
               .then(function(response) {
                  // console.log(response.data);
                  this.getChat(this.user_to, false);
               }).catch(function (error) {
                 console.log(error);
               });
          },
          NotifiMe(){
            if (Notification.permission === 'granted'){;
               this.$http.post(base_url+'api/chat/notifyMe', {"user_from":this.user_from,token_name:token_hash})
                  .then(function(response) {
                     if (response.data!=''){
                     new Notification(response.data.result[0].username, {
                        body: response.data.result[0].msg,
                       });
                     }
                  }).catch(function (error) {
                    console.log(error);
                  });
               }
          },
          user_search(){
            if (this.u_search=='') {this.autoStart();this.getUser();
            }else{
               this.stopInterval();
               this.$http.post(base_url + 'api/chat/getUser', {"user_from":this.user_from,"search":this.u_search,"job_id":this.job_id,token_name:token_hash})
               .then(function(response) {
                  // console.log(response.data);
                  this.users = '';
                  this.users = response.data.result;
                  
               }).catch(function (error) {
                 console.log(error);
               });
            }
          },
          deleteChatHistory(){
            if(this.user_to==''){
               Swal.fire({
                          icon: 'error',
                          title: 'Wrong Selection',
                          text: 'Please select person want to delete history!',
                          timer:  2000,
                        })
            }else{
               Swal.fire({
                    title: 'Are you sure want to delete ?',
                    text: "All msg is delete from your side !",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it !'
                  }).then((result) => {
                    if (result.value) {
                     this.user_name='';
                     this.$http.post(base_url+'api/chat/deleteChatHistory', {"user_from":this.user_from, "user_to":this.user_to,token_name:token_hash})
                     .then(function(response) {
                        // console.log(response.data);
                        
                        Swal.fire({
                                icon: 'success',
                                title: 'Deleted Successfully',
                                text: 'Deleted Successfully!',
                                timer:  2000,
                              })
                        this.getUser();
                        this.getChat(this.user_to);
                        // $(this.elements.front+' .chat-screen').addClass('active-y'); 
                        // $(this.elements.front+' .chat-message-box').addClass(this.elements.dn);
                        // this.user_name ='' ;
                       // this.getChat(this.user_to);
                     }).catch(function (error) {
                       console.log(error);
                     });
                    }
                  });
            }
          },
          deleteChatStatus(){
               this.$http.post(base_url+'api/chat/deleteChatStatus', {"user_from":this.user_from, "user_to":this.user_to,token_name:token_hash})
               .then(function(response) {
                  // console.log(response.data);
               }).catch(function (error) {
                 console.log(error);
               });
          },
          deleteMsg(index){
               this.$http.post(base_url+'api/chat/deleteMsg', {"id":this.chat_msgs[index].id,token_name:token_hash})
                  .then(function(response) {
                     // console.log(response.data);
                     this.getChat(this.chat_msgs[index].chat_with, false);
                  }).catch(function (error) {
                     console.log(error);
               });
          },
          newMessage(){
            this.stopScrolling();
          },
          scrollDown(){
             this.stopScrolling();
             this.scrollInterval = setInterval(function(){
               this.$refs.infoBox.scrollTop = this.$refs.infoBox.scrollHeight;
               // this.$refs.smallinfoBox.scrollTop = this.$refs.smallinfoBox.scrollHeight;
               this.height = this.$refs.infoBox.scrollTop;
               // console.log('ru');
              }.bind(this), 300);
             
          },
          SrollUp(){
            ((this.height-20)<this.$refs.infoBox.scrollTop)?this.scrollDown():this.stopScrolling();
          },
          stopScrolling : function(){          
              clearInterval(this.scrollInterval);
          },
          todo(){          
              this.interval = setInterval(function(){
                    this.NotifiMe();
                    this.getUser();
                    this.updateMsg();
                    this.getChatStatus();
              }.bind(this), 2000);
          },
          stopInterval(){          
              clearInterval(this.interval)
          },
          autoStart(){
            this.stopInterval();
            this.todo();
          },
          atteched(){
            if (this.user_to!='') {
               if(this.is_show==true){
               $(this.elements.front+this.elements.popup).removeClass(this.elements.dn);
               this.is_show=false;
               }else{
               $(this.elements.front+this.elements.popup).addClass(this.elements.dn);
               this.is_show=true;
               }
            }
          },
          pichture(){
            this.selectedfile=[];
            $(this.elements.front+this.elements.popup).addClass(this.elements.dn);
            $(this.elements.front+this.elements.id+this.elements.next).trigger(this.elements.fun);
            
          },
          fileUpload(){
            this.selectedfile=[];
            $(this.elements.front+this.elements.popup).addClass(this.elements.dn);
            $(this.elements.front+this.elements.id+this.elements.nextfile).trigger(this.elements.fun);
          },
          uploadingimage(e){
            const size = ((e.target.files[0].size/1024)/1024).toFixed(2);
            if (parseInt(size)>40) {
               $(this.elements.front+'#uploadimage, '+this.elements.front+'#uploadfile').val('');
               Swal.fire({
                          icon: 'warning',
                          title: 'Image size is Exceeded to  maximum range',
                          text:  'Range must be 1 KB to 40 MB'
                        })
            }else{
               // console.log(((e.target.files[0].name).split(".")).slice(-1)[0]);
               let name_file = ((e.target.files[0].name).split(".")).slice(-1)[0]; 
               if(name_file=='3gp' || name_file=='3gpp' || name_file=='mkv' || name_file=='mp4' || name_file=='avi')
               {
                     this.selectedfile=e.target.files[0];
                     this.attech_size = size;
                     this.url = null;
                     this.is_picture=false;
                     this.is_file=true;
                     this.is_post=true;
                     this.filename = e.target.files[0].name;
               }
               else if(name_file=='png' || name_file=='jpeg' || name_file=='JPEG' || name_file=='jpg' || name_file=='JPG')
               {
                     this.selectedfile=e.target.files[0];
                     this.url = URL.createObjectURL(this.selectedfile);
                     this.attech_size = size;
                     this.is_file=false;
                     this.is_picture=true;
                     this.is_post=true;
                     this.filename = e.target.files[0].name;
               }
               else
               {
                  Swal.fire({
                          icon: 'warning',
                          title: 'Wrong selection of file',
                          text:  'This file is not considerable'
                        });
               }
            }
          },
          uploadingfile(e){
            // console.log(e.target.files[0]);
            const size = ((e.target.files[0].size/1024)/1024).toFixed(2);
            if (parseInt(size)>10) {
               $(this.elements.front+'#uploadimage, '+this.elements.front+'#uploadfile').val('');
               Swal.fire({
                          icon: 'warning',
                          title: 'File size is Exceeded to  maximum range',
                          text:  'Range must be 1 KB to 10 MB'
                        })
            }else{
               let name_file = ((e.target.files[0].name).split(".")).slice(-1)[0]; 
               if(name_file=='csv' || name_file=='xls' || name_file=='xlsx' || name_file=='word' || name_file=='doc' || name_file=='docx' || name_file=='pdf' || name_file=='ppt' || name_file=='pptx' || name_file=='txt' || name_file=='html' || name_file=='htm'){
                  this.selectedfile=e.target.files[0];
                  this.attech_size = size;
                  this.url = null;
                  this.is_picture=false;
                  this.is_file=true;
                  this.is_post=true;
                  this.filename = e.target.files[0].name;
               }else{
                  Swal.fire({
                       icon: 'warning',
                       title: 'Wrong selection of file',
                       text:  'This file is not considerable'
                     });
               }
               
            }
          },
          onUpload(){
            Swal.fire({
                          title: 'Please wait we upload your data',
                          text:  'When item is uploaded it reflacted automatically !',
                          timer:  2000
                        });
             const fd = new FormData();
             fd.append('image',this.selectedfile,this.filename);
             this.$http.post(base_url+'api/chat/uplaodData',fd,)
                  .then(function(response) {
                     // console.log(response.data);
                     this.is_picture=false;
                     this.is_file=false;
                     this.is_post=false;
                     this.selectedfile=[];
                     if(response.data.result.filename!="No Image"){

                        this.$http.post(base_url+'api/chat/chat_notification_email',{"user_from":this.user_from, "user_to":this.user_to, "message":response.data.result.filename,token_name:token_hash})
                           .then(function(response) {
                              // console.log(response.data);
                              
                           }).catch(function (error) {
                              console.log(error);
                        });
                      
                        this.$http.post(base_url+'api/chat/uploadFileMsg',{"user_from":this.user_from, "user_to":this.user_to, "msg":response.data.result.filename, "is_type": response.data.result.is_type,"job_id":this.job_id,'size': response.data.result.size,token_name:token_hash})
                           .then(function(response) {
                              // console.log(response.data);
                              
                           }).catch(function (error) {
                              console.log(error);
                        });
                     }else{
                        Swal.fire({
                             icon:  'error',
                             title: 'File not uploaded',
                             text:  'somthing went wrong !',
                             timer:  3000
                           });
                     }
                  }).catch(function (error) {
                     console.log(error);
               });
               $(this.elements.front+'#uploadimage, '+this.elements.front+'#uploadfile').val('');

          },
          archive(val){
            this.stopInterval();
            setTimeout(function(){                           
               this.$http.post(base_url+'api/chat/archiveNow', {"user_from":this.user_from, "user_to":val,"archive":val,token_name:token_hash})
                  .then(function(response) {
                     // console.log(response.data);
                     this.user_to = '';
                     this.chat_msgs = '';
                     this.getUser();
                     setTimeout(function(){
                        this.autoStart();
                     }.bind(this), 4000);
                  }).catch(function (error) {
                    console.log(error);
                  });
            }.bind(this), 1000);
          },
          block(val){
            this.stopInterval();
            setTimeout(function(){                           
               this.$http.post(base_url+'api/chat/blockNow', {"user_from":this.user_from, "user_to":val,"block":val,"action":'add',token_name:token_hash})
                  .then(function(response) {
                     // console.log(response.data);
                     this.user_to = '';
                     this.chat_msgs = '';
                     this.getUser();
                     this.getChat(val);
              $(this.elements.front+' .chat-message-box').addClass(this.elements.dn);
                     setTimeout(function(){
                        this.autoStart();
                     }.bind(this), 4000);
                  }).catch(function (error) {
                    console.log(error);
                  });
            }.bind(this), 1000);
          },
          unblock(val){
            this.stopInterval();
            setTimeout(function(){                           
               this.$http.post(base_url+'api/chat/blockNow', {"user_from":this.user_from, "user_to":val,"block":val,"action":'delete',token_name:token_hash})
                  .then(function(response) {
                     // console.log(response.data);
                     this.user_to = '';
                     this.chat_msgs = '';
                     this.getUser();
                     this.getChat(val);
                     setTimeout(function(){
                        this.autoStart();
                     }.bind(this), 4000);
                  }).catch(function (error) {
                    console.log(error);
                  });
            }.bind(this), 1000);
          },
          deleteChatfromlist(val){
            this.user_to = (this.user_to=='')?val:this.user_to;
            this.deleteChatHistory();
          },
          getArchive(){
            this.stopInterval();
            this.$http.post(base_url + 'api/chat/getUser', {"user_from":this.user_from,"archive":'getArchive',"job_id":this.job_id,token_name:token_hash})
            .then(function(response) {
               // console.log(response.data);
               if (response.data=='') {
                  Swal.fire({
                       icon: 'warning',
                       title: "No data found",
                       text: 'Archived data not found!',
                     });
                  this.autoStart();
               }else{
                  this.users = '';
                  this.users = response.data.result;
                  setTimeout(function(){
                     this.autoStart();
                  }.bind(this), 3000);
               };
            }).catch(function (error) {
               console.log('error');
            });                
          },
          getAllUnread(e){
            this.stopInterval();
               this.$http.post(base_url + 'api/chat/getUser', {"user_from":this.user_from,"unread":'AllUnread',"job_id":this.job_id,token_name:token_hash})
               .then(function(response) {
                  // console.log(response.data);
                  if (response.data.status) {
                    
                    this.users = '';
                    this.users = response.data.result;
                    setTimeout(function(){
                       this.autoStart();
                    }.bind(this), 3000);

                  }else{

                    Swal.fire({
                        icon: 'warning',
                        title: "No data found",
                        text: 'You have not any unread message',
                      });
                     this.autoStart();
                   
                  }
                  
               }).catch(function (error) {
                 console.log(error);
               });
          },
          cancelSending(){
               this.is_post=false;
               $(this.elements.front+'#uploadimage, '+this.elements.front+'#uploadfile').val('');
          },
          multiDataSend(){
            if (this.user_select=='') {
                Swal.fire({
                          icon: 'warning',
                          title: "No User select",
                          text: 'Please select user first',
                          timer:2000
                        });
            }else if ($.trim(this.max_data)=='' || $.trim(this.max_data)==' ') {
               this.user_select==''
               Swal.fire({
                          icon: 'warning',
                          title: "Message is blank",
                          text: 'please write somthing to send',
                          timer:2000
                        });
            }else{
               // console.log(this.user_select+"  "+this.max_data);
               this.$http.post(base_url+'api/chat/sendMsg', {"user_from":this.user_from, "user_to":this.user_select, "msg":this.max_data, "last_date":this.last_date,token_name:token_hash})
                  .then(function(response) {
                     // console.log(response.data);
                     this.user_select = '';
                     this.max_data = '';
                     this.getUser();
                  }).catch(function (error) {
                    console.log(error);
                  });
            }
          }
        },              
        mounted : function(){
            this.todo(); 
            this.scrollDown(); 
            if (Notification.permission !== 'granted')Notification.requestPermission();
        },
        beforeDestroy () {
           clearInterval(this.interval);
           this.stopScrolling();
        },
     
}); 

//$('.atteched-btn').addClass('hide');

</script>
