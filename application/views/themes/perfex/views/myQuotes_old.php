<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="panel_s section-heading section-announcements">
    <div class="panel-body">
        <!--<h4 class="no-margin section-text"><?php echo _l($title); ?></h4>-->
        <div class="header-wrap p-10 bg-white">
            <div class="d-flex align-items-center">
                <div>
                    <h4 class="fw-700 mt-0">My Quotes</h4>
                </div>
                <div class="ml-auto align-items-center hide">
                    <div class="dl">
                        <form class="form-inline">
                            <div class="form-group">
                                <select class="form-control selectpicker" data-live-search="true">
                                    <option value="">Select filter</option>
                                    <option value="1">Status</option>
                                    <option value="2">Quote date</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input class="search__input" type="text" placeholder="Search">
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-theme">Reset</button>
                            </div>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel_s">
    <div class="panel-body">
        <div class="stats-wrap p-10 bg-white">
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a data-toggle="pill" href="#confirmed">Confirmed</a></li>
                <li><a data-toggle="pill" href="#pending">Pending Confirmation from Buyer</a></li>
                <li><a data-toggle="pill" href="#cancelled">Cancelled</a></li>
            </ul>
            <br>
            <div class="tab-content">
                <div id="confirmed" class="tab-pane fade in active">
                    <table id="example" class="responsive-table">
                        <thead>
                            <tr class="table-header">
                                <td>Id</td>
                                <td>Name</td>
                                <td>Quote Date</td>
                                <td>Quote Price</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if($confirmResult)
                                {
                                    $ss = 1;
                                    foreach($confirmResult as $rrr)
                                    {
										if($rrr->remove_id == 0)
										{
										    $bookingdetails = $this->db->get_where(db_prefix().'schedule_appoinment', array('quote_id' => $rrr->quote_id, 'garage_id' => $rrr->garage_id))->num_rows();
										    if($bookingdetails > 0)
										    {
										        $ss++;
										        $customernameArr = $this->db->select('firstname,lastname')->get_where(db_prefix().'contacts', array('userid' => $rrr->customer_id))->row();
    											$customername = $customernameArr->firstname.' '.@$customernameArr->lastname;
    											?>
    												<tr id="quoreremove_<?= $rrr->quote_id; ?>">
    													<td><?= $rrr->id?></td></td>
    													<td><?= $customername; ?></td>
    													<td><?= date('d M Y', strtotime($rrr->created_date)); ?></td>
    													<td><?= $rrr->price.' '.CURRENCY_NAME; ?></td>
    													<td>
    														<p class="btn-group" role="group" aria-label="Basic example">
    															<a href="<?= base_url('garages/messages/'.$rrr->customer_id); ?>" type="button" class="btn btn-msg mr-3" data-toggle="tooltip" title="Message">
    																<i class="fa fa-envelope" aria-hidden="true"></i>
    															</a>
    															<button type="button" class="btn btn-confirm mr-3" data-toggle="modal" onClick="getQuoteDetails(<?= $rrr->quote_id; ?>);" data-target="#quoteDetailsModal" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
    															<button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" onClick="removeQuotes_(<?= $rrr->quote_id ?>, <?= $rrr->garage_id ?>)" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
    															<button type="button" class="btn btn-appologise mr-10" data-toggle="modal" data-toggle="tooltip" title="Apologize" data-target="#apologizeModal" ><i class="fa fa-thumbs-up" aria-hidden="true"></i></button>
    															<?php
            														if($rrr->qty != null)
            														{
            															?>
            																<button type="button" class="btn btn-appologise" data-toggle="modal" onClick="customerReplyModel(<?= $rrr->id; ?>);" data-target="#customerReplyModel" title="Customer reply"><i class="fa fa-user"></i></button>
            															<?php
            														}
            														if($rrr->garage_view == 0)
            														{
            														    ?>
            														        <span id="notify<?= $rrr->id; ?>" class="badge navigationbadge">New</span>
            														    <?php
            														}
            														else
            														{
            														    echo '<span id="notify'.$rrr->id.'"><span>';
            														}
            													?>
    														</p>
    													</td>
    												</tr>
    											<?php   
										    }
										}
                                    }
                                    if($ss == 1)
                                    {
                                         ?>
                                            <tr>
                                                <td valign="top" colspan="5" class="dataTables_empty">No matching records found</td>
                                            </tr>
                                        <?php
                                    }
                                }
                                else
                                {
                                    ?>
                                        <tr>
                                            <td valign="top" colspan="5" class="dataTables_empty">No matching records found</td>
                                        </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div id="pending" class="tab-pane fade">
                    <table class="responsive-table dataTable no-footer pendingorde">
                        <thead>
                            <tr class="table-header">
                                <td>Id</td>
                                <td>Name</td>
                                <td>Quote Date</td>
                                <!--<td>Quote Status</td>-->
                                <td>Quote price</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
								if($confirmResult)
                                {
									$d=1;
                                    foreach($confirmResult as $rrr)
                                    {
										if($rrr->remove_id == 0)
										{
										    $bookingdetails = $this->db->get_where(db_prefix().'schedule_appoinment', array('quote_id' => $rrr->quote_id, 'garage_id' => $rrr->garage_id))->num_rows();
										    if($bookingdetails == 0)
										    {
										        $d++;
    											$customernameArr = $this->db->select('firstname,lastname')->get_where(db_prefix().'contacts', array('userid' => $rrr->customer_id))->row();
    											$customername = $customernameArr->firstname.' '.@$customernameArr->lastname;
    											?>
    												<tr id="quoreremove_<?= $rrr->quote_id; ?>">
    													<td><?= $rrr->id?></td></td>
    													<td><?= $customername; ?></td>
    													<td><?= date('d M Y', strtotime($rrr->created_date)); ?></td>
    													<td><?= $rrr->price.' '.CURRENCY_NAME; ?></td>
    													<td>
    														<p class="btn-group" role="group" aria-label="Basic example">
    															<a href="<?= base_url('garages/messages/'.$rrr->customer_id); ?>" type="button" class="btn btn-msg mr-3" data-toggle="tooltip" title="Message">
    																<i class="fa fa-envelope" aria-hidden="true"></i>
    															</a>
    															<button type="button" class="btn btn-confirm mr-3" data-toggle="modal" onClick="getQuoteDetails(<?= $rrr->quote_id; ?>);" data-target="#quoteDetailsModal" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
    															<button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip"onClick="removeQuotes_(<?= $rrr->quote_id ?>, <?= $rrr->garage_id ?>)"  title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
    														</p>
    													</td>
    												</tr>
    											<?php   
										    }
										}
                                    }
									if($d == 1)
									{
										?>
											<tr>
												<td valign="top" colspan="5" class="dataTables_empty">No matching records found</td>
											</tr>
										<?php
									}
                                }
                                else
                                {
                                    ?>
                                        <tr>
                                            <td valign="top" colspan="5" class="dataTables_empty">No matching records found</td>
                                        </tr>
                                    <?php
                                }
                            ?> 
                        </tbody>
                    </table>
                </div>
                <div id="cancelled" class="tab-pane fade">
                    <table class="responsive-table dataTable no-footer pendingorde">
                        <thead>
                            <tr class="table-header">
                                <td class="col col-1">Id</td>
                                <td class="col col-2">Name</td>
                                <td class="col col-4">Quote Date</td>
                                <td class="col col-6">Quote Price</td>
                                <td class="col col-7">Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
								if($confirmResult)
                                {
									$c = 1;
                                    foreach($confirmResult as $rrr)
                                    {
										if($rrr->remove_id > 0)
										{
											$c++;
											$customernameArr = $this->db->select('firstname,lastname')->get_where(db_prefix().'contacts', array('userid' => $rrr->customer_id))->row();
											$customername = $customernameArr->firstname.' '.@$customernameArr->lastname;
											?>
												<tr id="quoreremove_<?= $rrr->quote_id; ?>">
													<td><?= $rrr->id?></td></td>
													<td><?= $customername; ?></td>
													<td><?= date('d M Y', strtotime($rrr->created_date)); ?></td>
													<td><?= $rrr->price.' '.CURRENCY_NAME; ?></td>
													<td>
														<p class="btn-group" role="group" aria-label="Basic example">
															<a href="<?= base_url('garages/messages/'.$rrr->customer_id); ?>" type="button" class="btn btn-msg mr-3" data-toggle="tooltip" title="Message">
																<i class="fa fa-envelope" aria-hidden="true"></i>
															</a>
															<button type="button" class="btn btn-confirm mr-3" data-toggle="modal" onClick="getQuoteDetails(<?= $rrr->quote_id; ?>);" data-target="#quoteDetailsModal" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
															<button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" onClick="removeQuotes_(<?= $rrr->quote_id ?>, <?= $rrr->garage_id ?>)" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
														</p>
													</td>
												</tr>
											<?php
										}
                                    }
									if($c == 1)
									{
										?>
											<tr>
												<td valign="top" colspan="5" class="dataTables_empty">No matching records found</td>
											</tr>
										<?php
									}
                                }
                                else
                                {
                                    ?>
                                        <tr>
                                            <td valign="top" colspan="5" class="dataTables_empty">No matching records found</td>
                                        </tr>
                                    <?php
                                }
							?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Quote Details Modal -->
<div id="customerReplyModel" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Buyer reply</h4>
            </div>
            <div class="modal-body customerReply">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!--<button type="button" class="btn btn-reply mr-3" onclick="closeDetailsModel()" data-toggle="modal" data-target="#quoteModal"><i class="fa fa-reply" aria-hidden="true"></i></button>-->
            </div>
        </div>
    </div>
</div>

<!-- Quote Details Modal -->
<div id="quoteDetailsModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Quote Details</h4>
            </div>
            <div class="modal-body quoteDetails">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- apologizeModal Details Modal -->
<div id="apologizeModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Apologize Details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <textarea class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" data-dismiss="modal">Send</button>
            </div>
        </div>
    </div>
</div>
<script>
    function getQuoteDetails(Id)
    {
        $('.quoteDetails').html('<p>Loding....</p>');
        var token_name = csrfData['token_name'];
        var token_hash = csrfData['hash'];
        var str = {"id":Id,token_name:token_hash};
	    $.ajax({
	        url: '<?= site_url()?>garages/getQuoteDetails',
	        type: 'POST',
	        data: str,
	        //dataType: 'json',
	        cache: false,
	        success: function(resp){
	            if(resp)
	            {
	                $('.quoteDetails').html(resp);
	                return true;
	            }
	            else
	            {
	                
	                return false;
	            }
	        }
	    });
    }
    
    /* Customer Reply */
    function customerReplyModel(Id)
    {
        $('.customerReply').html('<p>Loading....</p>');
        var token_name = csrfData['token_name'];
        var token_hash = csrfData['hash'];
        var str = {"id":Id,token_name:token_hash};
	    $.ajax({
	        url: '<?= site_url()?>garages/customerReplyModel',
	        type: 'POST',
	        data: str,
	        //dataType: 'json',
	        cache: false,
	        success: function(resp){
	            if(resp)
	            {
	                $('#notify'+Id).css('display','none');
	                $('.customerReply').html(resp);
	                return true;
	            }
	            else
	            {
	                
	                return false;
	            }
	        }
	    });
    }
    
    $(document).ready(function() {
        $('.pendingorde').DataTable( {
            "order": [[ 0, "desc" ]]
        } );
    } );
    
    function removeQuotes_(id, garageid)
    {
        var x = confirm("Are you sure you want to delete?");
        if(x)
        {
            var token_name = csrfData['token_name'];
            var token_hash = csrfData['hash'];
            var str = {"id":id,"garageid":garageid,token_name:token_hash};
    	    $.ajax({
    	        url: '<?= site_url()?>myQuotes/removeQuotes',
    	        type: 'POST',
    	        data: str,
    	        //dataType: 'json',
    	        cache: false,
    	        success: function(resp){
    	            if(resp)
    	            {
    	                $('#quoreremove_'+id).empty();
    	                return true;
    	            }
    	            else
    	            {
    	                
    	                return false;
    	            }
    	        }
    	    }); 
        }
    }
    
</script>