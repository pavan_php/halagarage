<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="panel_s section-heading section-announcements">
    <div class="panel-body">
        <!--<h4 class="no-margin section-text"><?php echo _l($title); ?></h4>-->
        <div class="header-wrap p-10 bg-white">
            <div class="d-flex align-items-center">
                <div>
                    <h4 class="fw-700 mt-0">My Quotes</h4>
                </div>
                <div class="ml-auto align-items-center hide">
                    <div class="dl">
                        <form class="form-inline">
                            <div class="form-group">
                                <select class="form-control selectpicker" data-live-search="true">
                                    <option value="">Select filter</option>
                                    <option value="1">Status</option>
                                    <option value="2">Quote date</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input class="search__input" type="text" placeholder="Search">
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-theme">Reset</button>
                            </div>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel_s">
    <div class="panel-body panel-sm-body">
        <div class="stats-wrap p-10 bg-white">
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a data-toggle="pill" href="#confirmed">Confirmed</a></li>
                <li><a data-toggle="pill" href="#pending">Pending Confirmation from Buyer</a></li>
                <li><a data-toggle="pill" href="#cancelled">Cancelled</a></li>
            </ul>
            <br>
            <div class="tab-content">
                <div id="confirmed" class="tab-pane fade in active">
                    <!--<table id="example" class="responsive-table">-->
                    <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                        <thead>
                            <tr class="table-header">
                                <td style="width:8%;">Id</td>
                                <td>Name</td>
                                <td style="width: 13%;">Quote Date</td>
                                <td>Appointment Date</td>
                                <td>Quote Price <?= CURRENCY_NAME; ?></td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if($confirmResult)
                                {
                                    $ss = 1;
                                    foreach($confirmResult as $rrr)
                                    {
                                        if($rrr->remove_id == 0)
                                        {
                                            $bookingdetails = $this->db->get_where(db_prefix().'schedule_appoinment', array('quote_id' => $rrr->quote_id, 'garage_id' => $rrr->garage_id, 'status' => 2))->num_rows();
                                            if($bookingdetails > 0)
                                            {
                                                $appointmentdate = $this->db->get_where(db_prefix().'schedule_appoinment', array('quote_id' => $rrr->quote_id, 'garage_id' => $rrr->garage_id))->row('appointment_date');
                                                $ss++;
                                                $customernameArr = $this->db->select('firstname,lastname')->get_where(db_prefix().'contacts', array('userid' => $rrr->customer_id))->row();
                                                $customername = $customernameArr->firstname.' '.@$customernameArr->lastname;
                                                ?>
                                                    <tr id="quoreremove_<?= $rrr->quote_id; ?>">
                                                        <td><?= $rrr->id?></td></td>
                                                        <td><?= $customername; ?></td>
                                                        <td><span style="display:none;"><?php echo strtotime($rrr->created_date); ?></span><?= date('d-M-Y', strtotime($rrr->created_date)); ?></td>
                                                        <td><span style="display:none;"><?php echo $appointmentdate; ?></span><?= date('d-M-Y', $appointmentdate); ?></td>
                                                        <td><?= $rrr->price; ?></td>
                                                        <td>
                                                            <p class="btn-group" role="group" aria-label="Basic example">
                                                                <a href="<?= base_url('garages/messages/'.$rrr->customer_id); ?>" type="button" class="btn btn-msg mr-3" data-toggle="tooltip" title="Message">
                                                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                                                </a>
                                                                <button type="button" class="btn btn-confirm mr-3" data-toggle="modal" onClick="getQuoteDetails(<?= $rrr->quote_id; ?>);" data-target="#quoteDetailsModal" title="View"><i class="fa fa-eye" data-toggle="tooltip" aria-hidden="true"></i></button>
                                                                   <!-- 
                                                                <button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" onClick="removeQuotes_(<?= $rrr->quote_id ?>, <?= $rrr->garage_id ?>)" title="Delete"><i class="fa fa-trash" data-toggle="tooltip" aria-hidden="true"></i></button>-->

                                                                <button type="button" class="btn btn-appologise mr-3"  onclick="declineQuate(<?= $rrr->quote_id ?>, <?= $rrr->garage_id ?>)" title="Decline"   data-toggle="tooltip" title="Apologize"  ><i class="fa fa-thumbs-down"   aria-hidden="true"></i></button>
                                                                
                                                                <?php
                                                                    $quoteRes = $this->db->get_where(db_prefix().'schedule_appoinment', array('quote_id' => $rrr->quote_id, 'status >' => 2))->num_rows();
                                                                    if($quoteRes > 0)
                                                                    {
                                                                        ?>
                                                                            <button type="button" class="btn btn-confirm mr-3"  data-toggle="tooltip" title="Accept" onclick="acceptModelStatus()" ><i class="fa fa-thumbs-up"   data-toggle="tooltip"  aria-hidden="true"></i></button>        
                                                                        <?php
                                                                    }
                                                                    else
                                                                    {
                                                                        ?>
                                                                            <button type="button" class="btn btn-confirm mr-3"  data-toggle="tooltip" title="Accept" onclick="AcceptModel(<?php echo $rrr->quote_id; ?>,<?= $rrr->garage_id ?>)" ><i class="fa fa-thumbs-up"   data-toggle="tooltip"  aria-hidden="true"></i></button>        
                                                                        <?php
                                                                    }
                                                                    
                                                                    if($rrr->qty != null)
                                                                    {
                                                                        ?>
                                                                            <button type="button" class="btn btn-appologise" data-toggle="modal" onClick="customerReplyModel(<?= $rrr->id; ?>);" data-target="#customerReplyModel" title="Customer reply"><i class="fa fa-user"></i></button>
                                                                        <?php
                                                                    }
                                                                    if($rrr->garage_view == 0)
                                                                    {
                                                                        ?>
                                                                            <span id="notify<?= $rrr->id; ?>" class="badge navigationbadge">New</span>
                                                                        <?php
                                                                    }
                                                                    else
                                                                    {
                                                                        echo '<span id="notify'.$rrr->id.'"><span>';
                                                                    }
                                                                ?>
                                                            </p>
                                                        </td>
                                                    </tr>
                                                <?php   
                                            }
                                        }
                                    }
                                    if($ss == 1)
                                    {
                                         ?>
                                            <tr>
                                                <td valign="top" colspan="5" class="dataTables_empty">No matching records found</td>
                                            </tr>
                                        <?php
                                    }
                                }
                                else
                                {
                                    ?>
                                        <tr>
                                            <td valign="top" colspan="5" class="dataTables_empty">No matching records found</td>
                                        </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div id="pending" class="tab-pane fade">
                    <!--<table class="responsive-table dataTable no-footer pendingorde">-->
                    <table  class="table table-striped table-bordered dt-responsive nowrap pendingorde" style="width:100%">
                        <thead>
                            <tr class="table-header">
                                <td>Id</td>
                                <td>Name</td>
                                <td>Quote Date</td>
                                <!--<td>Quote Status</td>-->
                                <td>Quote price <?= CURRENCY_NAME; ?></td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if($pendingesult)
                                {
                                    $d=1;
                                    foreach($pendingesult as $rrr)
                                    {
                                        if($rrr->remove_id == 0)
                                        {
                                            $d++;
                                            $customernameArr = $this->db->select('firstname,lastname')->get_where(db_prefix().'contacts', array('userid' => $rrr->customer_id))->row();
                                            $customername = $customernameArr->firstname.' '.@$customernameArr->lastname;
                                            ?>
                                                <tr id="quoreremove_<?= $rrr->quote_id; ?>">
                                                    <td><?= $rrr->id?></td></td>
                                                    <td><?= $customername; ?></td>
                                                    <td><span style="display:none;"><?php echo strtotime($rrr->created_date); ?></span><?= date('d-M-Y', strtotime($rrr->created_date)); ?></td>
                                                    <td><?= $rrr->price; ?></td>
                                                    <td>
                                                        <p class="btn-group" role="group" aria-label="Basic example">
                                                            <a href="<?= base_url('garages/messages/'.$rrr->customer_id); ?>" type="button" class="btn btn-msg mr-3" data-toggle="tooltip" title="Message">
                                                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                                            </a>
                                                            <button type="button" class="btn btn-confirm mr-3" data-toggle="modal" onClick="getQuoteDetails(<?= $rrr->quote_id; ?>);" data-target="#quoteDetailsModal" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                                            <button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" onClick="removeQuotes_(<?= $rrr->quote_id ?>, <?= $rrr->garage_id ?>)"  title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                        </p>
                                                    </td>
                                                </tr>
                                            <?php   
                                        }
                                    }
                                    if($d == 1)
                                    {
                                        ?>
                                            <tr>
                                                <td valign="top" colspan="5" class="dataTables_empty">No matching records found</td>
                                            </tr>
                                        <?php
                                    }
                                }
                                else
                                {
                                    ?>
                                        <tr>
                                            <td valign="top" colspan="5" class="dataTables_empty">No matching records found</td>
                                        </tr>
                                    <?php
                                }
                            ?> 
                        </tbody>
                    </table>
                </div>
                <div id="cancelled" class="tab-pane fade">
                    <!--<table class="responsive-table dataTable no-footer pendingorde">-->
                    <table  class="table table-striped table-bordered dt-responsive nowrap pendingorde" style="width:100%">
                        <thead>
                            <tr class="table-header">
                                <td class="col col-1">Id</td>
                                <td class="col col-2">Name</td>
                                <td class="col col-4">Quote Date</td>
                                <td class="col col-6">Quote Price<?= CURRENCY_NAME; ?></td>
                                <td class="col col-7">Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if($canceledResult)
                                {
                                    $c = 1;
                                    foreach($canceledResult as $rrr)
                                    {
                                        if($rrr->remove_id > 0)
                                        {
                                            $quoteStr = $this->db->get_where(db_prefix().'requestAQuote', array('id' => $rrr->quote_id))->row('remove_garage_id');
                                            if($quoteStr)
                                            { 
                                                $quoteArr = explode(',',$quoteStr);
                                                if(in_array($rrr->garage_id, $quoteArr))
                                                {
                                                    $c++;
                                                    $customernameArr = $this->db->select('firstname,lastname')->get_where(db_prefix().'contacts', array('userid' => $rrr->customer_id))->row();
                                                    $customername = $customernameArr->firstname.' '.@$customernameArr->lastname;
                                                    ?>
                                                        <tr id="quoreremove_<?= $rrr->quote_id; ?>">
                                                            <td><?= $rrr->id?></td></td>
                                                            <td><?= $customername; ?></td>
                                                            <td><span style="display:none;"><?php echo strtotime($rrr->created_date); ?></span><?= date('d-M-Y', strtotime($rrr->created_date)); ?></td>
                                                            <td><?= $rrr->price; ?></td>
                                                            <td>
                                                                <p class="btn-group" role="group" aria-label="Basic example">
                                                                    <a href="<?= base_url('garages/messages/'.$rrr->customer_id); ?>" type="button" class="btn btn-msg mr-3" data-toggle="tooltip" title="Message">
                                                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                                                    </a>
                                                                    <button type="button" class="btn btn-confirm mr-3" data-toggle="modal" onClick="getQuoteDetails(<?= $rrr->quote_id; ?>);" data-target="#quoteDetailsModal" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                                                    <!--<button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" onClick="removeQuotes_(<?= $rrr->quote_id ?>, <?= $rrr->garage_id ?>)" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>-->
                                                                </p>
                                                            </td>
                                                        </tr>
                                                    <?php
                                                }
                                            }
                                            else
                                            {
                                                $c++;
                                                $customernameArr = $this->db->select('firstname,lastname')->get_where(db_prefix().'contacts', array('userid' => $rrr->customer_id))->row();
                                                $customername = $customernameArr->firstname.' '.@$customernameArr->lastname;
                                                ?>
                                                    <tr id="quoreremove_<?= $rrr->quote_id; ?>">
                                                        <td><?= $rrr->id?></td></td>
                                                        <td><?= $customername; ?></td>
                                                        <td><span style="display:none;"><?php echo strtotime($rrr->created_date); ?></span><?= date('d-M-Y', strtotime($rrr->created_date)); ?></td>
                                                        <td><?= $rrr->price; ?></td>
                                                        <td>
                                                            <p class="btn-group" role="group" aria-label="Basic example">
                                                                <a href="<?= base_url('garages/messages/'.$rrr->customer_id); ?>" type="button" class="btn btn-msg mr-3" data-toggle="tooltip" title="Message">
                                                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                                                </a>
                                                                <button type="button" class="btn btn-confirm mr-3" data-toggle="modal" onClick="getQuoteDetails(<?= $rrr->quote_id; ?>);" data-target="#quoteDetailsModal" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                                                <!--<button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" onClick="removeCancelQuotes(<?= $rrr->quote_id ?>, <?= $rrr->garage_id ?>)" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>-->
                                                            </p>
                                                        </td>
                                                    </tr>
                                                <?php
                                            }
                                        }
                                    }
                                    if($c == 1)
                                    {
                                        ?>
                                            <tr>
                                                <td valign="top" colspan="5" class="dataTables_empty">No matching records found</td>
                                            </tr>
                                        <?php
                                    }
                                }
                                else
                                {
                                    ?>
                                        <tr>
                                            <td valign="top" colspan="5" class="dataTables_empty">No matching records found</td>
                                        </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Quote Details Modal -->
<div id="customerReplyModel" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Buyer reply</h4>
            </div>
            <div class="modal-body customerReply">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!--<button type="button" class="btn btn-reply mr-3" onclick="closeDetailsModel()" data-toggle="modal" data-target="#quoteModal"><i class="fa fa-reply" aria-hidden="true"></i></button>-->
            </div>
        </div>
    </div>
</div>

<!-- Quote Details Modal -->
<div id="quoteDetailsModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Quote Details</h4>
            </div>
            <div class="modal-body quoteDetails">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- apologizeModal Details Modal -->
<div id="apologizeModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Apologize Details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <textarea class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" data-dismiss="modal">Send</button>
            </div>
        </div>
    </div>
</div>

<div id="AcceptModel" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md text-center">
        <div class="modal-content">
            <div class="modal-body">
                <h4>Accept</h4>
                <p>Are You want to accept</p>
                <button type="button" data-dismiss="modal" class="btn btn-success confirm confirmCal"> No Thanks</button>
                <button type="button" data-dismiss="modal" class="btn btn-danger discard"  id="deleteCal">Accept</button>
           </div>
        </div>
    </div>
</div>
  
<script>
    /*
    $(document).ready(function() {
        $('.pendingorde').DataTable();
    } );
    */
    function AcceptModel(id,garageid){

        var token_name = csrfData['token_name'];
        var token_hash = csrfData['hash'];
        var strstatus = {"qid":id,"garageid":garageid,token_name:token_hash};
        $.ajax({
            url: '<?= site_url()?>garages/checkAwardStatus',
            type: 'POST',
            data: strstatus,
            //dataType: 'json',
            cache: false,
            success: function(resp){
                if(resp)
                {
                    bootbox.confirm({
                        message: "Awarded to someone else!",
                        buttons: {
                            confirm: {
                                label: 'Ok',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'Close',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {}
                    });  
                }
                else
                {
                    bootbox.confirm({
                        message: "Are You want to accept ?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                 var str = {"id":id,"garageid":garageid,token_name:token_hash};
                                  $.ajax({
                                    url: '<?= site_url()?>myQuotes/acceptQuate',
                                    type: 'POST',
                                    data: str,
                                    //dataType: 'json',
                                    cache: false,
                                    success: function(resp){
                                        $('#quoreremove_'+id).empty();
                                    }
                                });
                            }
                        }
                    }); 
                }
            }
        });     
    }

    /* accept Model Status */
    function acceptModelStatus()
    {
        bootbox.confirm({
            message: "Awarded to someone else!",
            buttons: {
                confirm: {
                    label: 'Ok',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Close',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {}
        });  
    }

    function declineQuate(id,garageid){

         bootbox.confirm({
            message: "Are You want to decline ?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result){

                    var token_name = csrfData['token_name'];
                    var token_hash = csrfData['hash'];
                   var str = {"id":id,"garageid":garageid,token_name:token_hash};

                      $.ajax({
                            url: '<?= site_url()?>myQuotes/decineQuotes',
                            type: 'POST',
                            data: str,
                            //dataType: 'json',
                            cache: false,
                            success: function(resp){
                                $('#quoreremove_'+id).empty();   
                            }
                        }); 
                }
            }
        }); 
    }
    function getQuoteDetails(Id)
    {
        $('.quoteDetails').html('<p>Loading....</p>');
        var token_name = csrfData['token_name'];
        var token_hash = csrfData['hash'];
        var str = {"id":Id,token_name:token_hash};
        $.ajax({
            url: '<?= site_url()?>garages/getQuoteDetails',
            type: 'POST',
            data: str,
            //dataType: 'json',
            cache: false,
            success: function(resp){
                if(resp)
                {
                    $('.quoteDetails').html(resp);
                    return true;
                }
                else
                {
                    
                    return false;
                }
            }
        });
    }
    
    /* Customer Reply */
    function customerReplyModel(Id)
    {
        $('.customerReply').html('<p>Loading....</p>');
        var token_name = csrfData['token_name'];
        var token_hash = csrfData['hash'];
        var str = {"id":Id,token_name:token_hash};
        $.ajax({
            url: '<?= site_url()?>garages/customerReplyModel',
            type: 'POST',
            data: str,
            //dataType: 'json',
            cache: false,
            success: function(resp){
                if(resp)
                {
                    $('#notify'+Id).css('display','none');
                    $('.customerReply').html(resp);
                    return true;
                }
                else
                {
                    
                    return false;
                }
            }
        });
    }
    
    $(document).ready(function() {
        $('.pendingorde').DataTable( {
            "order": [[ 0, "desc" ]]
        } );
    } );
    
    function removeQuotes_(id, garageid)
    {
        var x = confirm("Are you sure you want to delete?");
        if(x)
        {
            var token_name = csrfData['token_name'];
            var token_hash = csrfData['hash'];
            var str = {"id":id,"garageid":garageid,token_name:token_hash};
            $.ajax({
                url: '<?= site_url()?>myQuotes/removeQuotes',
                type: 'POST',
                data: str,
                //dataType: 'json',
                cache: false,
                success: function(resp){
                    if(resp)
                    {
                        $('#quoreremove_'+id).empty();
                        return true;
                    }
                    else
                    {
                        
                        return false;
                    }
                }
            }); 
        }
    }
    
    function removeCancelQuotes(id, garageid)
    {
        var x = confirm("Are you sure you want to delete?");
        if(x)
        {
            var token_name = csrfData['token_name'];
            var token_hash = csrfData['hash'];
            var str = {"id":id,"garageid":garageid,token_name:token_hash};
            $.ajax({
                url: '<?= site_url()?>myQuotes/removeCancelQuotes',
                type: 'POST',
                data: str,
                //dataType: 'json',
                cache: false,
                success: function(resp){
                    if(resp)
                    {
                        $('#quoreremove_'+id).empty();
                        return true;
                    }
                    else
                    {
                        
                        return false;
                    }
                }
            }); 
        }
    }
    
</script>