<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="panel_s">
    <div class="panel-body">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h3 class="modal-title" id="expiryModalTitle">Plan Upgrade Successful</h3>
            </div>
            <div class="modal-body text-center">
                <div class="box-wrap">
                   <img src="<?= base_url() ?>assets/images/success.svg" class="img-responsive" style="width: 15%;margin: 0 auto;">
                   <br>
                   <h1 style="color:#1fa67a;">Payment Successful</h1>
                   <p style="font-size: 20px;">Congratulations, your plan has been successfully upgraded.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    setTimeout(function(){location.href="<?= base_url() ?>garages/billing", 10000} );  
</script>