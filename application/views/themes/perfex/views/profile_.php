<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="container">
	<div class="panel_s section-heading section-<?php echo _l($title); ?>">
		<div class="panel-body p-0">
			<!-- Empty Garage Profile Start -->
			<div class="empty-garage-wrap">
				<div class="header-wrap p-10 bg-white">
					<div class="d-flex align-items-center">
						<div><h4 class="fw-700">Profile</h4></div>
						<div class="ml-auto align-items-center">
							<div class="dl">
								<button class="btn btn-default" onClick="editProfile();" type="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="edit"> Edit Details</span></button>
							</div>
						</div>
					</div>
				</div>
				<hr class="mt-0 mb-0">
				<div class="row">
					<div class="col-lg-12 p-40 text-center">
						<h3>No Details Found</h3>
					</div>
				</div>
			</div>
			<!-- Empty Garage Profile Start -->
			<!-- Edit Garage Profile Start -->
			<div class="edit-garage-wrap">
				<div class="row">
					<div class="col-lg-12 p-40">
						<form>
							<div class="form-group">
								<label for="garage-name">Garage Name</label>
								<input type="text" maxlength="10" value="<?= $profileData->company; ?>" class="form-control" id="company" name="company" placeholder="Enter garage name">
							</div>
							<div class="form-group">
								<label for="address">Address</label>
								<input type="text" value="<?= $profileData->address; ?>" class="form-control" id="address" name="address" placeholder="Enter address">
							</div>
							<div class="form-group">
								<label for="telephone">Telephone</label>
								<input type="text" value="<?= $profileData->telephone; ?>" class="form-control" id="telephone" name="telephone" placeholder="Enter telephone">
							</div>
							<div class="form-group">
								<label for="mobile">Mobile</label>
								<input type="text" value="<?= $profileData->mobile; ?>" class="form-control" id="mobile" name="mobile" placeholder="Enter mobile">
							</div>
							<div class="form-group">
								<label for="rating">Rate</label>
								<div class="star-rating">
									<input type="radio" id="5-stars" name="rating" value="5"/>
									<label for="5-stars" class="star">&#9733;</label>
									<input type="radio" id="4-stars" name="rating" value="4"/>
									<label for="4-stars" class="star">&#9733;</label>
									<input type="radio" id="3-stars" name="rating" value="3"/>
									<label for="3-stars" class="star">&#9733;</label>
									<input type="radio" id="2-stars" name="rating" value="2"/>
									<label for="2-stars" class="star">&#9733;</label>
									<input type="radio" id="1-star" name="rating" value="1"/>
									<label for="1-star" class="star">&#9733;</label>
								</div>
							</div>
							<div class="form-group">
								<label for="review">Review</label>	
								<textarea class="form-control" rows="5" id="review" name="review" placeholder="Your review here..."></textarea>
							</div>
							<div class="form-group">
								<label for="review">Upload image</label>	
								<input type="file">
							</div>
							<button class="btn btn-default" type="button">Save</span></button>
						</form>
					</div>
				</div>
			</div>
			<!-- Edit Garage Profile End -->
			<!-- Filled Garage Profile Start -->
			<div class="filled-garage-wrap">
				<div class="banner-bg">
					<h2 class="no-margin section-text">CAR MASTER GARAGE <a href="javascript:void(0);" data-toggle="modal" data-target="#shareModal"><i class="fa fa-share-alt" aria-hidden="true"></i></a></h2>
					<hr>
					<h5 class="address mb-20"><i class="fa fa-map-marker" aria-hidden="true"></i> Location : 7 B St - Al Quoz - Dubai - United Arab Emirates</h5>
					<h5 class="address"><i class="fa fa-phone" aria-hidden="true"></i> Contact : +971 50 937 7714</h5>
					<div class="rating_wrap mt-0" style="display: flex;align-items: center;">
						<h5 class="rating-number"><i class="fa fa-star-o" aria-hidden="true"></i> Rating :</h5>
						<div class="rating" style="width: 85px;">
							<div id="full-stars-example-two">
								<div class="rating-group">
									<label aria-label="1 star" class="rating__label" for="rating3-1" style="font-size: 1.4rem;"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
									<label aria-label="2 stars" class="rating__label" for="rating3-2" style="font-size: 1.4rem;"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
									<label aria-label="3 stars" class="rating__label" for="rating3-3" style="font-size: 1.4rem;"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
									<label aria-label="4 stars" class="rating__label" for="rating3-4" style="font-size: 1.4rem;"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
									<label aria-label="5 stars" class="rating__label" for="rating3-5" style="font-size: 1.4rem;"><i class="rating__icon  fa fa-star"></i></label>
								</div>
							</div>
						</div>
						<a href="javascript:void(0);" class="total-count">[86 Reviews]</a>
					</div>
					<div class="custom-options">
						<ul>
							<li>
								<a href="javascript:void(0);">
									<i class="fa fa-envelope" aria-hidden="true"></i><br>
									<span>SMS/ Email</span>
								</a>
							</li>
							<li>
								<a href="javascript:void(0);">
									<i class="fa fa-whatsapp" aria-hidden="true"></i><br>
									<span>WhatsApp</span>
								</a>
							</li>
						</ul>
					</div>
					<div class="edit-wrap">
						<a href="javascript:void(0);" id="edit-bg">
							<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
						</a>
					</div>
				</div>
			</div>
			<!-- Filled Garage Profile End -->
			<div class="edit-bg-wrap">
				<div class="row">
					<div class="col-lg-12">
						
							<div class="box"></div>
							<div class="color-picker-wrap">
								<div id="bg-selector d-flex align-items-center">
									<label>background</label>
									<div class="color white" data-value="#fff"></div>
									<div class="color blue" data-value="#2998ff"></div>
									<div class="color violet" data-value="#7329ff"></div>
									<div class="color saffron" data-value="#f88d71"></div>
								</div>
								<input id="colorpicker" type="color" />
							</div>
							<div class="flex-wrap-wrap flex-row-centered">
								<div class="background-img-wrap">
									<img src="http://php.manageprojects.in/halagarage/assets/images/bg/1.jpg" id="image">
								</div>
								<div class="background-img-wrap">
									<img src="http://php.manageprojects.in/halagarage/assets/images/bg/2.png" id="image1">
								</div>
								<div class="background-img-wrap">
									<img src="http://php.manageprojects.in/halagarage/assets/images/bg/3.png" id="image2">
								</div>
								<div class="background-img-wrap">
									<img src="http://php.manageprojects.in/halagarage/assets/images/bg/4.png" id="image3">
								</div>
								<div class="background-img-wrap">
									<img src="http://php.manageprojects.in/halagarage/assets/images/bg/5.png" id="image4">
								</div>
								<div class="background-img-wrap">
									<img src="http://php.manageprojects.in/halagarage/assets/images/bg/6.png" id="image5">
								</div>
							</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xl-5 col-lg-5 col-12">
			<!-- Gallery Start -->
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-12">
					<div class="panel_s">
						<div class="panel-body">
							<!-- Empty Gallery Start -->
							<div class="empty-gallery-wrap">
								<div class="header-wrap p-10 bg-white">
									<div class="d-flex align-items-center">
										<div><h4 class="fw-700">Gallery</h4></div>
										<div class="ml-auto align-items-center">
											<div class="dl">
												<button class="btn btn-default" type="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="edit"> Edit</span></button>
											</div>
										</div>
									</div>
								</div>
								<hr class="mt-0 mb-0">
								<div class="row">
									<div class="col-lg-12 p-40 text-center">
										<h3>No Photos Found</h3>
									</div>
								</div>
							</div>
							<!-- Empty Gallery Start -->
							<!-- Empty Gallery Start -->
							<div class="edit-gallery-wrap">
								<div class="header-wrap p-10 bg-white">
									<div class="d-flex align-items-center">
										<div><h4 class="fw-700">Gallery</h4></div>
										<div class="ml-auto align-items-center">
											<div class="dl">
												<button class="btn btn-default" type="button"><i class="fa fa-cloud" aria-hidden="true"></i><span class="save"> Save</span></button>
											</div>
										</div>
									</div>
								</div>
								<hr class="mt-0 mb-0">
								<div class="row">
									<div class="col-lg-12 p-40 text-center">
										<div class="upload-btn-wrapper">
											<button class="btn">Upload a file</button>
											<input type="file" name="myfile" />
										</div>
									</div>
								</div>
							</div>
							<!-- Empty Gallery Start -->
							<!-- Filled Gallery Start -->
							<div class="filled-gallery-wrap">
								<div class="header-wrap p-10 bg-white">
									<div class="d-flex align-items-center">
										<div><h4 class="fw-700">Gallery</h4></div>
										<div class="ml-auto align-items-center">
											<div class="dl">
												<button class="btn btn-default" type="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="edit"> Edit</span></button>
											</div>
										</div>
									</div>
								</div>
								<hr class="mt-0 mb-0">
								<div class="row">
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery1.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery2.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery3.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery4.png" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery5.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery2.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery6.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery1.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery7.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery8.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery3.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<div class="col-lg-2 no-gutter">
										<div class="gallery">
											<div class="img-wrap">
												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal">
													<img src="http://php.manageprojects.in/halagarage/assets/images/gallery1.jpg" alt="image" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Filled Gallery End -->
						</div>
					</div>
				</div>
			</div>
			<!-- Gallery End -->

			<!-- Services Start -->
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-12">
					<div class="panel_s">
						<div class="panel-body">
							<!-- Empty Service Start -->
							<div class="empty-service-wrap">
								<div class="header-wrap p-10 bg-white">
									<div class="d-flex align-items-center">
										<div><h4 class="fw-700">Services</h4></div>
										<div class="ml-auto align-items-center">
											<div class="dl">
												<button class="btn btn-default" type="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="edit"> Edit</span></button>
											</div>
										</div>
									</div>
								</div>
								<hr class="mt-0 mb-0">
								<div class="row">
									<div class="col-lg-12 p-40 text-center">
										<h3>No Services Found</h3>
									</div>
								</div>
							</div>
							<!-- Empty Service Start -->
							<!-- Empty Service Start -->
							<div class="edit-service-wrap">
								<div class="header-wrap p-10 bg-white">
									<div class="d-flex align-items-center">
										<div><h4 class="fw-700">Services</h4></div>
										<div class="ml-auto align-items-center">
											<div class="dl">
												<button class="btn btn-default" type="button"><i class="fa fa-cloud" aria-hidden="true"></i><span class="save"> Save</span></button>
											</div>
										</div>
									</div>
								</div>
								<hr class="mt-0 mb-0">
								<div class="row">
									<div class="col-lg-12 p-40 text-center">
										<div class="upload-btn-wrapper">
											<button class="btn">Upload a file</button>
											<input type="file" name="myfile" />
										</div>
									</div>
								</div>
							</div>
							<!-- Empty Service Start -->
							<!-- Filled Services End -->
							<div class="filled-service-wrap">
								<div class="header-wrap p-10 bg-white">
									<div class="d-flex align-items-center">
										<div><h4 class="fw-700">Services</h4></div>
										<div class="ml-auto align-items-center">
											<div class="dl">
												<button class="btn btn-default" type="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="services-upload"> Edit</span></button>
											</div>
										</div>
									</div>
								</div>
								<hr class="mt-0 mb-0">
								<div class="garage-profile">
									<div class="product">
										<img src="http://php.manageprojects.in/halagarage/assets/images/air-conditioning.jpg">
										<h3 class="mt-5 mb-5">Car Air Conditioning Repairing</h3>
										<div class="shrt_des" style="margin-bottom:5px;"><p><i><span style="font-weight: 400;">For your sedan, small car or SUV or MUV, air-conditioning system is the most vital need to provide you relaxation from the outside atmosphere.</span></i></p>
										</div>
									</div>
								</div>
								<div class="garage-profile">
									<div class="product">
										<img src="http://php.manageprojects.in/halagarage/assets/images/painting.jpg">
										<h3 class="mt-5 mb-5">Car Painting Services</h3>
										<div class="shrt_des" style="margin-bottom:5px;"><p><i><span style="font-weight: 400;">Car painting services, the most common type of services required once in a year or at a certain years’ gap, is the routine service at workshop.</span></i></p>
										</div>
									</div>
								</div>
								<div class="garage-profile">
									<div class="product">
										<img src="http://php.manageprojects.in/halagarage/assets/images/denting.jpg">
										<h3 class="mt-5 mb-5">Auto Denting & Scratching Repairing</h3>
										<div class="shrt_des" style="margin-bottom:5px;"><p><i><span style="font-weight: 400;">An unwanted mark or scratch on your car after a road traffic accident or due to any other reason may spoil the overall look of your car or SUV.</span></i></p>
										</div>
									</div>
								</div>
							</div>
							<!-- Filled Services End -->
						</div>
					</div>
				</div>
			</div>
			<!-- Services End -->

			<!-- Products Start -->
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-12">
					<div class="panel_s">
						<div class="panel-body">
							<!-- Empty Products Start -->
							<div class="empty-product-wrap">
								<div class="header-wrap p-10 bg-white">
									<div class="d-flex align-items-center">
										<div><h4 class="fw-700">Products</h4></div>
										<div class="ml-auto align-items-center">
											<div class="dl">
												<button class="btn btn-default" type="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="edit"> Edit</span></button>
											</div>
										</div>
									</div>
								</div>
								<hr class="mt-0 mb-0">
								<div class="row">
									<div class="col-lg-12 p-40 text-center">
										<h3>No Products Found</h3>
									</div>
								</div>
							</div>
							<!-- Empty Products Start -->
							<!-- Empty Products Start -->
							<div class="edit-product-wrap">
								<div class="header-wrap p-10 bg-white">
									<div class="d-flex align-items-center">
										<div><h4 class="fw-700">Products</h4></div>
										<div class="ml-auto align-items-center">
											<div class="dl">
												<button class="btn btn-default" type="button"><i class="fa fa-cloud" aria-hidden="true"></i><span class="save"> Save</span></button>
											</div>
										</div>
									</div>
								</div>
								<hr class="mt-0 mb-0">
								<div class="row">
									<div class="col-lg-12 p-40 text-center">
										<div class="upload-btn-wrapper">
											<button class="btn">Upload a file</button>
											<input type="file" name="myfile" />
										</div>
									</div>
								</div>
							</div>
							<!-- Empty Products Start -->
							<!-- Filled Products Starts -->
							<div class="filled-product-wrap">
								<div class="header-wrap p-10 bg-white">
									<div class="d-flex align-items-center">
										<div><h4 class="fw-700">Products</h4></div>
										<div class="ml-auto align-items-center">
											<div class="dl">
												<button class="btn btn-default" type="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="products-upload"> Edit</span></button>
											</div>
										</div>
									</div>
								</div>
								<hr class="mt-0 mb-0">
								<div class="garage-profile-product">
									<div class="product">
										<img src="http://php.manageprojects.in/halagarage/assets/images/fog_light_cover.webp">
										<h3 class="mt-5 mb-5">Fog Light Cover</h3>
										<div class="shrt_des" style="margin-bottom:5px;"><p><span style="font-weight: 400;">Electrical Services</span></p>
										</div>
									</div>
								</div>
								<div class="garage-profile-product">
									<div class="product">
										<img src="http://php.manageprojects.in/halagarage/assets/images/wheel.webp">
										<h3 class="mt-5 mb-5">Wheel Rim</h3>
										<div class="shrt_des" style="margin-bottom:5px;"><p><span style="font-weight: 400;">Tyre Replacement & Services</span></p>
										</div>
									</div>
								</div>
								<div class="garage-profile-product">
									<div class="product">
										<img src="http://php.manageprojects.in/halagarage/assets/images/bumper_bracket.webp">
										<h3 class="mt-5 mb-5">Bumper Bracket</h3>
										<div class="shrt_des" style="margin-bottom:5px;"><p><span style="font-weight: 400;">Body Parts Repairing</span></p>
										</div>
									</div>
								</div>
							</div>
							<!-- Filled Products End -->
						</div>
					</div>
				</div>
			</div>
			<!-- Products End -->
		</div>
		<div class="col-xl-7 col-lg-7 col-12">
			<div class="panel_s">
				<div class="panel-body">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#home">Newsfeed</a></li>
						<li><a data-toggle="tab" href="#menu1">About</a></li>
						<li><a data-toggle="tab" href="#menu2">Reviews</a></li>
					</ul>

					<div class="tab-content">
						<div id="home" class="tab-pane fade in active">
							<!-- Empty Newsfeed Starts -->
							<div class="empty-newsfeed-wrap">
								<div class="row">
									<div class="col-lg-12 text-center margin-auto">
										<!-- New Post Starts -->
										<div class="new-post-wrap mb-20">
											<div class="post-new">
												<div class="post-new-media d-flex">
													<div class="post-new-media-user">
														<img src="http://php.manageprojects.in/halagarage/assets/images/user-placeholder.jpg" alt="user-img">
													</div>
													<div class="post-new-media-input w-100">
														<input type="text" class="form-control" placeholder="What's in your mind... ?">
														<div class="upload-btn-wrapper">
															<button class="btn"><i class="fa fa-camera" aria-hidden="true"></i> </button>
															<input type="file" name="myfile" />
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- New Post End -->
										<img src="http://php.manageprojects.in/halagarage/assets/images/newsfeed.svg" class="img-responsive w-15 margin-auto">
										<h3>No feeds yet</h3>
									</div>
								</div>
							</div>
							<!-- Empty Newsfeed End -->
							<!-- Filled Newsfeed Starts -->
							<div class="filled-newsfeed-wrap">
								<!-- New Post Starts -->
								<div class="new-post-wrap mb-20">
									<div class="post-new">
										<div class="post-new-media d-flex">
											<div class="post-new-media-user">
												<img src="http://php.manageprojects.in/halagarage/assets/images/user-placeholder.jpg" alt="user-img">
											</div>
											<div class="post-new-media-input w-100">
												<input type="text" class="form-control" placeholder="What's in your mind... ?">
												<div class="upload-btn-wrapper">
													<button class="btn"><i class="fa fa-camera" aria-hidden="true"></i> </button>
													<input type="file" name="myfile" />
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- New Post End -->
								<div class="newsfeed-wrap">
									<div class="card card-tabel mt-20 text-post newsfeed-data" style="display: block;">
										<div class="card-body">
											<div class="media media-author">
												<div class="posted-logo mr-2">
													<img src="http://php.manageprojects.in/halagarage/assets/images/user-placeholder.jpg" alt="" class="img-fluid">
												</div>
												<div class="media-body">
													<div class="tooltip-container">
														<h6><a href="javascript:void(0);" class="media"> Test  One </a></h6>
													</div> 
													<p>1 week ago</p>
												</div> 
											</div>
											<div class="post-description mt-10 ">
												<p class="title">xx </p>
												<p class="description">If you're in search of the best HD Wallpapers for Windows 7, you've come to the right place. We offer an extraordinary number of HD images that will instantly freshen up your smartphone or computer... <a href="javascript:void(0)" class="read-more">read more</a></p>
											</div>
										</div>
									</div>
									<div class="card card-tabel mt-20 text-post newsfeed-data" style="display: block;">
										<div class="card-body">
											<div class="media media-author">
												<div class="posted-logo mr-2">
													<img src="http://php.manageprojects.in/halagarage/assets/images/user-placeholder.jpg" alt="" class="img-fluid">
												</div>
												<div class="media-body">
													<div class="tooltip-container">
														<h6><a href="javascript:void(0);" class="media"> Test  One </a></h6>
													</div> 
													<p>1 week ago</p>
												</div> 
											</div>
											<div class="post-description mt-10 ">
												<div id="gallery1"></div>
											</div>
										</div>
									</div>
									<div class="card card-tabel mt-20 text-post newsfeed-data" style="display: block;">
										<div class="card-body">
											<div class="media media-author">
												<div class="posted-logo mr-2">
													<img src="http://php.manageprojects.in/halagarage/assets/images/user-placeholder.jpg" alt="" class="img-fluid">
												</div>
												<div class="media-body">
													<div class="tooltip-container">
														<h6><a href="javascript:void(0);" class="media"> Test  One </a></h6>
													</div> 
													<p>1 week ago</p>
												</div> 
											</div>
											<div class="post-description mt-10 ">
												<p>55 Windows 8 Wallpapers in HD For Free Download</p>
												<div id="gallery6"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Filled Newsfeed End -->
						</div>
						<div id="menu1" class="tab-pane fade">
							<!-- Empty About Start -->
							<div class="empty-about-wrap hide">
								<div class="row">
									<div class="col-lg-12 p-40 text-center">
										<h3>No Data Found</h3>
										<button class="btn btn-default" onClick="editGarageAbountUs()" type="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="edit"> Edit</span></button>
									</div>
								</div>
							</div>
							<!-- Empty About End -->
							<!-- Edit About Start -->
							<div class="edit-about-wrap">
								<div class="row">
									<div class="col-lg-12">
										<form>
											<div class="form-group hide">
												<label for="title">Title</label>
												<input type="text" class="form-control" id="title" name="title">
											</div>
											<div class="form-group">
												<textarea class="form-control summernote" onkeyup="errorremove('description');" rows="15" id="description" name="description"><?= @$garageAbountRes->content; ?></textarea>
												<p class="text-danger description"></p>
											</div>
											<button class="btn btn-default" onClick="garageAboutUsSave()" type="button"><span class="save"> Submit</span></button>
										</form>
									</div>
								</div>
							</div>
							<!-- Edit About End -->
							<!-- Filled About Start -->
							<div class="filled-about-wrap">
								<div class="row">
									<div class="col-lg-12">
										<div class="d-flex align-items-center">
											<div class="hide"><h4 class="fw-700"><?= $garageAbountRes->title; ?></h4></div>
											<div class="ml-auto align-items-center">
												<div class="dl">
													<button class="btn btn-default" onClick="editGarageAbountUs()" type="button"><i class="fa fa-edit" aria-hidden="true"></i><span class="edit"> Edit</span></button>
												</div>
											</div>
										</div>
								    	<div class="content"><?= $garageAbountRes->content; ?></div>
									</div>
								</div>
							</div>
							<!-- Filled About End -->
						</div>
						<div id="menu2" class="tab-pane fade">
							<!-- Empty Review Start -->
							<div class="empty-review-wrap">
								<div class="row">
									<div class="col-lg-12 p-40 text-center">
										<h3>No Revies Found</h3>
										<button class="btn btn-default" type="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="edit"> Post Review</span></button>
									</div>
								</div>
							</div>
							<!-- Empty Review End -->
							<!-- Edit Review Start -->
							<div class="edit-review-wrap">
								<div class="row">
									<div class="col-lg-12">
										<form>
											<div class="form-group">
												<div class="star-rating">
													<input type="radio" id="5-stars" name="rating" value="5" />
													<label for="5-stars" class="star">&#9733;</label>
													<input type="radio" id="4-stars" name="rating" value="4" />
													<label for="4-stars" class="star">&#9733;</label>
													<input type="radio" id="3-stars" name="rating" value="3" />
													<label for="3-stars" class="star">&#9733;</label>
													<input type="radio" id="2-stars" name="rating" value="2" />
													<label for="2-stars" class="star">&#9733;</label>
													<input type="radio" id="1-star" name="rating" value="1" />
													<label for="1-star" class="star">&#9733;</label>
												</div>
											</div>
											<div class="form-group">
												<textarea class="form-control" rows="5" id="review" name="review" placeholder="Your review here..."></textarea>
											</div>
											<button class="btn btn-default" type="button"><span class="save"> Submit Review</span></button>
										</form>
									</div>
								</div>
							</div>
							<!-- Edit Review End -->
							<!-- Filled Review Start -->
							<div class="filled-review-wrap">
								<div class="review-wrap">
									<div class="profile-wrap">
										<img src="http://php.manageprojects.in/halagarage/assets/images/user-placeholder.jpg" class="img-fluid">
									</div>
									<div class="profile-content">
										<span class="user-name">Harish Paul</span>
										<div class="date-time">
											<div class="rating_wrap mt-5">
												<div class="rating">
													<div id="full-stars-example-two">
														<div class="rating-group">
															<label aria-label="1 star" class="rating__label" for="rating3-1"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="2 stars" class="rating__label" for="rating3-2"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="3 stars" class="rating__label" for="rating3-3"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="4 stars" class="rating__label" for="rating3-4"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="5 stars" class="rating__label" for="rating3-5"><i class="rating__icon  fa fa-star"></i></label>
														</div>
													</div>
												</div>
												<a href="javascript:void(0);" class="rating_num">May 4, 2020</a>
											</div>
										</div>
										<div class="review-text"> 
											<span>he processes are constantly shifting and changing which makes it difficult at times to know what is expected of you and what the current objectives are. - Leadership can be unresponsive with follow through. A lot is promised to workers, and some of these things are swept aside or forgotten as other things gain priority. - Work life balance is lacking at times. Sales reps are often responding to emails or calls during their personal time</span>
										</div>
									</div>
								</div>
								<div class="review-wrap">
									<div class="profile-wrap">
										<img src="http://php.manageprojects.in/halagarage/assets/images/user-placeholder.jpg" class="img-fluid">
									</div>
									<div class="profile-content">
										<span class="user-name">Harish Paul</span>
										<div class="date-time">
											<div class="rating_wrap mt-5">
												<div class="rating">
													<div id="full-stars-example-two">
														<div class="rating-group">
															<label aria-label="1 star" class="rating__label" for="rating3-1"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="2 stars" class="rating__label" for="rating3-2"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="3 stars" class="rating__label" for="rating3-3"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="4 stars" class="rating__label" for="rating3-4"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="5 stars" class="rating__label" for="rating3-5"><i class="rating__icon  fa fa-star"></i></label>
														</div>
													</div>
												</div>
												<a href="javascript:void(0);" class="rating_num">May 4, 2020</a>
											</div>
										</div>
										<div class="review-text"> 
											<span>Start within 30 days, finish within 48 hours.</span>
										</div>
									</div>
								</div>
								<div class="review-wrap">
									<div class="profile-wrap">
										<img src="http://php.manageprojects.in/halagarage/assets/images/user-placeholder.jpg" class="img-fluid">
									</div>
									<div class="profile-content">
										<span class="user-name">Harish Paul</span>
										<div class="date-time">
											<div class="rating_wrap mt-5">
												<div class="rating">
													<div id="full-stars-example-two">
														<div class="rating-group">
															<label aria-label="1 star" class="rating__label" for="rating3-1"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="2 stars" class="rating__label" for="rating3-2"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="3 stars" class="rating__label" for="rating3-3"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="4 stars" class="rating__label" for="rating3-4"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="5 stars" class="rating__label" for="rating3-5"><i class="rating__icon  fa fa-star"></i></label>
														</div>
													</div>
												</div>
												<a href="javascript:void(0);" class="rating_num">May 4, 2020</a>
											</div>
										</div>
										<div class="review-text"> 
											<span>Start within 30 days, finish within 48 hours.</span>
										</div>
									</div>
								</div>
								<div class="review-wrap">
									<div class="profile-wrap">
										<img src="http://php.manageprojects.in/halagarage/assets/images/user-placeholder.jpg" class="img-fluid">
									</div>
									<div class="profile-content">
										<span class="user-name">Harish Paul</span>
										<div class="date-time">
											<div class="rating_wrap mt-5">
												<div class="rating">
													<div id="full-stars-example-two">
														<div class="rating-group">
															<label aria-label="1 star" class="rating__label" for="rating3-1"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="2 stars" class="rating__label" for="rating3-2"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="3 stars" class="rating__label" for="rating3-3"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="4 stars" class="rating__label" for="rating3-4"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="5 stars" class="rating__label" for="rating3-5"><i class="rating__icon  fa fa-star"></i></label>
														</div>
													</div>
												</div>
												<a href="javascript:void(0);" class="rating_num">May 4, 2020</a>
											</div>
										</div>
										<div class="review-text"> 
											<span>Start within 30 days, finish within 48 hours.</span>
										</div>
									</div>
								</div>
								<div class="review-wrap">
									<div class="profile-wrap">
										<img src="http://php.manageprojects.in/halagarage/assets/images/user-placeholder.jpg" class="img-fluid">
									</div>
									<div class="profile-content">
										<span class="user-name">Harish Paul</span>
										<div class="date-time">
											<div class="rating_wrap mt-5">
												<div class="rating">
													<div id="full-stars-example-two">
														<div class="rating-group">
															<label aria-label="1 star" class="rating__label" for="rating3-1"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="2 stars" class="rating__label" for="rating3-2"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="3 stars" class="rating__label" for="rating3-3"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="4 stars" class="rating__label" for="rating3-4"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="5 stars" class="rating__label" for="rating3-5"><i class="rating__icon  fa fa-star"></i></label>
														</div>
													</div>
												</div>
												<a href="javascript:void(0);" class="rating_num">May 4, 2020</a>
											</div>
										</div>
										<div class="review-text"> 
											<span>Start within 30 days, finish within 48 hours.</span>
										</div>
									</div>
								</div>
							</div>
							<!-- Filled Review End -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Share Modal -->
<div id="shareModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Share With Friends</h4>
            </div>
            <div class="modal-body" style="display:inline-block;">
                <ul class="share-social-menu">
                    <li><a href="javascript:void(0);"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-mobile" aria-hidden="true"></i></a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Gallery Modal -->
<div id="galleryModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="display:inline-block;">
                <div class="img-wrap">
					<img src="http://php.manageprojects.in/halagarage/assets/images/gallery1.jpg" class="img-responsive">
				</div>
            </div>
        </div>
    </div>
</div>
<script>

            var images = [
                'https://unsplash.it/1300/800?image=875',
                'https://unsplash.it/1300/800?image=874',
                'https://unsplash.it/1300/800?image=872',
                'https://unsplash.it/1300/800?image=868',
                'https://unsplash.it/1300/800?image=839',
                'https://unsplash.it/1300/800?image=838'
            ];

            $(function() {

                $('#gallery1').imagesGrid({
                    images: images
                });
                $('#gallery2').imagesGrid({
                    images: images.slice(0, 5)
                });
                $('#gallery3').imagesGrid({
                    images: images.slice(0, 4)
                });
                $('#gallery4').imagesGrid({
                    images: images.slice(0, 3)
                });
                $('#gallery5').imagesGrid({
                    images: images.slice(0, 2)
                });
                $('#gallery6').imagesGrid({
                    images: images.slice(0, 1)
                });
                $('#gallery7').imagesGrid({
                    images: [
                        'https://unsplash.it/660/440?image=875',
                        'https://unsplash.it/660/990?image=874',
                        'https://unsplash.it/660/440?image=872',
                        'https://unsplash.it/750/500?image=868',
                        'https://unsplash.it/660/990?image=839',
                        'https://unsplash.it/660/455?image=838'
                    ],
                    align: true,
                    getViewAllText: function(imgsCount) { return 'View all' }
                });

            });

        </script>
        <script>
            function editProfile()
            {
                $('.edit-garage-wrap').css('display','block');
                $('.filled-garage-wrap').css('display','block');
            }
            
            /* Edit Garage AbountUs */
            function editGarageAbountUs()
            {
                $('.edit-about-wrap').css('display','block');
                $('.empty-about-wrap').css('display','none');
                $('.filled-about-wrap').css('display','none');
            }
            
            /* Garage AboutUs Save */
            function garageAboutUsSave()
            {
                //var title = $('#title').val();
                //var textareaValue = $('#description').val();
                var token_name = csrfData['token_name'];
                var token_hash = csrfData['hash'];
                var description = $('#description').summernote('code');
                if(description != '')
                {
                    var str = { "description":description,token_name:token_hash};
            	    $.ajax({
            	        url: '<?= base_url()?>garages/garageAboutUsSave',
            	        type: 'POST',
            	        data: str,
            	        //dataType: 'json',
            	        cache: false,
            	        success: function(resp){
            	            if(resp)
            	            {
            	                $('#description').html(description);
            	                $('.content').html(description);
            	                $('.edit-about-wrap').css('display','none');
            	                $('.filled-about-wrap').css('display','block');
            	                $('.empty-about-wrap').css('display','none');
            	                //return true;
            	            }
            	            else
            	            {
            	                $('.empty-about-wrap').css('display','none');
            	                $('.edit-about-wrap').css('display','block');
            	                $('.filled-about-wrap').css('display','none');
            	                
            	            }
            	        }
            	    });
                }
                else
                {
                    if(title == '')
                    {
                        $('.title').text('Title filed is required');
                        return false;
                    }
                    if(description == '')
                    {
                        $('.description').text('Description filed is required');
                        return false;
                    }
                }
            }
		</script>
<!-- bg js -->

  <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>
	$(function() {
		//bg color selector
		$(".color").click(function(){
			var color = $(this).attr("data-value");
			$(".box").css("background-color", color);
		});
	
		//add color picker if supported
		if (Modernizr.inputtypes.color) {
			$(".picker").css("display", 'inline-block');
			var c = document.getElementById('colorpicker');
			c.addEventListener('change', function(e) {
			//d.innerHTML = c.value;
			var color = c.value;
			$(".box").css("background-color", color);
				}, false);
		}
	});
	function pickColor() {
		$("#colorpicker").click();
	}

    $(document).ready(function() {
         $('#description').summernote({
          height: 150
        });
    });


	$(document).ready(function(){
		// Set background image of a div on click of the button
		$("#image").click(function(){
			var imageUrl = "http://php.manageprojects.in/halagarage/assets/images/bg/bg-1.png";
			$(".box").css("background-image", "url(" + imageUrl + ")");
		});    
		$("#image1").click(function(){
			var imageUrl = "http://php.manageprojects.in/halagarage/assets/images/bg/bg-2.png";
			$(".box").css("background-image", "url(" + imageUrl + ")");
		}); 
		$("#image2").click(function(){
			var imageUrl = "http://php.manageprojects.in/halagarage/assets/images/bg/bg-3.png";
			$(".box").css("background-image", "url(" + imageUrl + ")");
		}); 
		$("#image3").click(function(){
			var imageUrl = "http://php.manageprojects.in/halagarage/assets/images/bg/bg-4.png";
			$(".box").css("background-image", "url(" + imageUrl + ")");
		}); 
		$("#image4").click(function(){
			var imageUrl = "http://php.manageprojects.in/halagarage/assets/images/bg/bg-5.png";
			$(".box").css("background-image", "url(" + imageUrl + ")");
		}); 
		$("#image5").click(function(){
			var imageUrl = "http://php.manageprojects.in/halagarage/assets/images/bg/bg-6.png";
			$(".box").css("background-image", "url(" + imageUrl + ")");
		}); 
	});

	$("#edit-bg").click(function(){
		$(".edit-bg-wrap").show();
	});
</script>