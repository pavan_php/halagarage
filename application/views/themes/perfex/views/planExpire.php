<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="panel_s">
    <div class="panel-body">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h3 class="modal-title" id="expiryModalTitle">Plan Expiry</h3>
             </div>
             <div class="modal-body text-center">
                <div class="box-wrap">
                   <i class="fa fa-exclamation-circle" aria-hidden="true" style="font-size:100px;color:#c10000;"></i>
                   <br>
                   <h1>Trial Expired</h1>
                   <p style="font-size: 20px;">Sorry, your trial period for this application has now expired, please review your plan.</p>
                   <br>
                   <button class="btn btn-info btn-lg" data-toggle="modal" data-target="#subscriptionModal" data-dismiss="modal">Upgrade</button>
                </div>
             </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="subscriptionModal" tabindex="-1" role="dialog" aria-labelledby="subscriptionModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="subscriptionModalTitle"><?= _l('Subscrioption Details');?></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position:absolute;right: 20px;top: 20px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php
                    //$plandetails_ = $this->db->get_where(db_prefix().'subscription')->row();
                ?>
                <table class="responsive-table dataTable no-footer">
                    <thead>
                        <tr style="box-shadow:none;">
                            <th style="font-size: 18px;font-weight: 600;padding: 10px;">Title</th>
                            <td style="font-size: 18px;font-weight: 400;padding: 10px;"><?= $plandetails_->title; ?></td>
                        </tr>
                        <tr style="box-shadow:none;">
                            <th style="font-size: 18px;font-weight: 600;padding: 10px;">Price</th>
                            <td style="font-size: 18px;font-weight: 400;padding: 10px;"><?= $plandetails_->price.' '.CURRENCY_NAME; ?></td>
                        </tr>
                        <tr style="box-shadow:none;">
                            <th style="font-size: 18px;font-weight: 600;padding: 10px;">Days</th>
                            <td style="font-size: 18px;font-weight: 400;padding: 10px;"><?= $plandetails_->day_limit; ?></td>
                        </tr>
                        <tr style="box-shadow:none;">
                            <th style="font-size: 18px;font-weight: 600;padding: 10px;">Description</th>
                            <td style="font-size: 18px;font-weight: 400;padding: 10px;"><?= $plandetails_->description; ?></td>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <form action="<?php echo PAYPAL_URL; ?>" method="post">
                    <input type="hidden" name="business" value="<?php echo PAYPAL_ID; ?>">
                    <input type="hidden" name="cmd" value="_xclick">                                                                                  
                    <input type="hidden" name="item_name" value="<?php echo $plandetails_->title; ?>">
                    <input type="hidden" name="item_number" value="<?php echo $plandetails_->id; ?>">
                    <input type="hidden" name="amount" value="<?php echo ($plandetails_->price/3.26); ?>">
                    <input type="hidden" name="currency_code" value="<?php echo PAYPAL_CURRENCY; ?>">
                    <input type="hidden" name="return" value="<?php echo PAYPAL_RETURN_URL; ?>">
                    <input type="hidden" name="cancel_return" value="<?php echo PAYPAL_CANCEL_URL; ?>">
                    <input type="hidden" name="notify_url" value="<?php echo PAYPAL_NOTIFY_URL; ?>">                               
                    <input type="submit" name="submit" border="0" class="btn btn-info btn-lg" value="Subscribe Now">
                </form>
            </div>
        </div>
    </div>
</div>