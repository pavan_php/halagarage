<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="panel_s section-heading section-announcements">
    <div class="panel-body">
        <!--<h4 class="no-margin section-text"><?php echo _l($title); ?></h4>-->
        <div class="header-wrap p-10 bg-white">
            <div class="d-flex align-items-center">
                <div>
                    <h4 class="fw-700 mt-0">My Orders</h4>
                </div>
                <div class="ml-auto align-items-center hide">
                    <div class="dl">
                        <form acton="#" class="form-inline">
                            <!--<div class="form-group">
                                <select class="form-control">
                                    <option>Select filter</option>
                                    <option>Product id</option>
                                    <option>Product name</option>
                                    <option>Product brand</option>
                                    <option>Order status</option>
                                    <option>Appointment date</option>
                                </select>
                            </div>-->
                            <div class="form-group">
                                <input class="search__input" type="text" placeholder="Search">
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-theme">Reset</button>
                            </div>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--
<div class="panel_s section-heading section-announcements hide">
    <div class="panel-body">
        <div class="tag-wrapper">
            <ul class="nav nav-pills nav-justified">
                <li><a data-toggle="pill" href="#menu1">Product name</a></li>
                <li><a data-toggle="pill" href="#menu2">Product brand</a></li>
                <li><a data-toggle="pill" href="#menu3">Order status</a></li>
                <li><a data-toggle="pill" href="#menu4">Appointment date</a></li>
            </ul>
            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    
                </div>
                <div id="menu1" class="tab-pane fade">
                    <div class="product-name-wrap">
                        <form class="form-cf">
                            <div class="checkbox-btn">
                                <input type="checkbox" name="checkbox2" id="zero" value="zero" checked=""><label class="zero-label four col sellfield hover active" for="zero">AC Services</label>
                                <input type="checkbox" name="checkbox2" id="one" value="one"><label class="one-label four col sellfield hover active" for="one">Body Parts Repairing</label>
                                <input type="checkbox" name="checkbox2" id="two" value="two"><label class="two-label four col sellfield hover active" for="two">Tyre Replacement & Services</label>
                                <input type="checkbox" name="checkbox2" id="three" value="three"><label class="three-label four col sellfield hover active" for="three">Painting Services</label>
                                <input type="checkbox" name="checkbox2" id="four" value="four"><label class="four-label four col sellfield hover active" for="four">Electrical Services</label>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="menu2" class="tab-pane fade">
                    <div class="product-brand-wrap">
                        <form class="form-cf">
                            <div class="checkbox-btn">
                                <input type="checkbox" name="checkbox2" id="five" value="five" checked=""><label class="five-label four col sellfield hover active" for="five">Toyota</label>
                                <input type="checkbox" name="checkbox2" id="six" value="six"><label class="six-label four col sellfield hover active" for="six">Honda</label>
                                <input type="checkbox" name="checkbox2" id="seven" value="seven"><label class="seven-label four col sellfield hover active" for="seven">BMW</label>
                                <input type="checkbox" name="checkbox2" id="eight" value="eight"><label class="eight-label four col sellfield hover active" for="eight">Audi</label>
                                <input type="checkbox" name="checkbox2" id="nine" value="nine"><label class="nine-label four col sellfield hover active" for="nine">Hyundai</label>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="menu3" class="tab-pane fade">
                    <div class="status-wrap">
                        <form class="form-cf">
                            <div class="checkbox-btn">
                                <input type="checkbox" name="checkbox2" id="ten" value="ten" checked=""><label class="ten-label four col sellfield hover active" for="ten">Confirmed</label>
                                <input type="checkbox" name="checkbox2" id="eleven" value="eleven"><label class="eleven-label four col sellfield hover active" for="eleven">Cancelled</label>
                                <input type="checkbox" name="checkbox2" id="twelve" value="twelve"><label class="twelve-label four col sellfield hover active" for="twelve">Pending</label>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="menu4" class="tab-pane fade">
                    <div class="date-wrap">
                        <form class="form-cf">
                            <div class="checkbox-btn">
                                <input type="checkbox" name="checkbox2" id="thirteen" value="thirteen" checked=""><label class="thirteen-label four col sellfield hover active" for="thirteen">02 July 2020</label>
                                <input type="checkbox" name="checkbox2" id="fourteen" value="fourteen"><label class="fourteen-label four col sellfield hover active" for="fourteen">07 July 2020</label>
                                <input type="checkbox" name="checkbox2" id="fifteen" value="fifteen"><label class="fifteen-label four col sellfield hover active" for="fifteen">15 July 2020</label>
                                <input type="checkbox" name="checkbox2" id="sixteen" value="sixteen"><label class="sixteen-label four col sellfield hover active" for="sixteen">22 July 2020</label>
                                <input type="checkbox" name="checkbox2" id="seventeen" value="seventeen"><label class="seventeen-label four col sellfield hover active" for="seventeen">18 July 2020</label>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
-->
<div class="panel_s">
    <div class="panel-body panel-sm-body">
        <div class="stats-wrap p-10 bg-white">
            <!--<ul class="responsive-table" id="example">
                <li class="table-header">
                    <div class="col col-1">Id</div>
                    <div class="col col-2">Name</div>
                    <div class="col col-3">Product Name</div>
                    <div class="col col-4">Order Date</div>
                    <div class="col col-5">Order Status</div>
                    <div class="col col-6">Appointment Date</div>
                    <div class="col col-8">Action</div>
                </li>
                <li >
                    <div class="col col-1">#001</div>
                    <div class="col col-2">Tanmay Sasvadkar</div>
                    <div class="col col-3">AC Services</div>
                    <div class="col col-4">13 May 2020</div>
                    <div class="col col-5"><span class="badge badge-active">Confirmed</span></div>
                    <div class="col col-6">02 July 2020</div>
                    <div class="col col-8">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-re-activate mr-3">Reschedule appointment</button>
                            <button type="button" class="btn btn-appologise mr-10">Apologise</button>
                        </div>
                        <a href="javascript:void(0);" data-toggle="tooltip" title="Detail" class="text-muted">
                            <i class="fa fa-ellipsis-v mt-8" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
                <li >
                    <div class="col col-1">#002</div>
                    <div class="col col-2">Pavan Prajapati</div>
                    <div class="col col-3">Body Parts Repairing</div>
                    <div class="col col-4">13 June 2020</div>
                    <div class="col col-5"><span class="badge badge-pending">Pending Confirmation</span></div>
                    <div class="col col-6">10 June 2020</div>
                    <div class="col col-8">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-confirm mr-3">Confirm Order</button>
                            <button type="button" class="btn btn-appologise mr-10">Apologise</button>
                        </div>
                        <a href="javascript:void(0);" data-toggle="tooltip" title="Detail" class="text-muted">
                            <i class="fa fa-ellipsis-v mt-8" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
                <li >
                    <div class="col col-1">#003</div>
                    <div class="col col-2">Krishnpal Yadav</div>
                    <div class="col col-3">Tyre Replacement & Services</div>
                    <div class="col col-4">03 Apr 2020</div>
                    <div class="col col-5"><span class="badge badge-cancelled">Cancelled</span></div>
                    <div class="col col-6">20 May 2020</div>
                    <div class="col col-8">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-confirm mr-3">Confirm Order</button>
                            <button type="button" class="btn btn-appologise mr-10">Apologise</button>
                        </div>
                        <a href="javascript:void(0);" data-toggle="tooltip" title="Detail" class="text-muted">
                            <i class="fa fa-ellipsis-v mt-8" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
                <li >
                    <div class="col col-1">#004</div>
                    <div class="col col-2">Basant Yaduvanshi</div>
                    <div class="col col-3">Painting Services</div>
                    <div class="col col-4">27 June 2020</div>
                    <div class="col col-5"><span class="badge badge-active">Confirmed</span></div>
                    <div class="col col-6">04 July 2020</div>
                    <div class="col col-8">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-re-activate mr-3">Reschedule appointment</button>
                            <button type="button" class="btn btn-appologise mr-10">Apologise</button>
                        </div>
                        <a href="javascript:void(0);" data-toggle="tooltip" title="Detail" class="text-muted">
                            <i class="fa fa-ellipsis-v mt-8" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
                <li >
                    <div class="col col-1">#005</div>
                    <div class="col col-2">Manas Phadnis</div>
                    <div class="col col-3">Electrical Services</div>
                    <div class="col col-4">21 May 2020</div>
                    <div class="col col-5"><span class="badge badge-cancelled">Cancelled</span></div>
                    <div class="col col-6">05 June 2020</div>
                    <div class="col col-8">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-confirm mr-3">Confirm Order</button>
                            <button type="button" class="btn btn-appologise mr-10">Apologise</button>
                        </div>
                        <a href="javascript:void(0);" data-toggle="tooltip" title="Detail" class="text-muted">
                            <i class="fa fa-ellipsis-v mt-8" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
            </ul>-->
            <div id="loaderHide" class="text-center"> <img src="https://i.gifer.com/ZZ5H.gif" style="width:4%;"></div>
            <!--<table id="example" class="responsive-table" style="display: none">-->
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%; display: none;">
                <thead>
                    <tr class="table-header">
                        <td>Order Id</td>
                        <td class="hide">Id</td>
                        <td>Buyer Name</td>
                        <td>Product/Services Name</td>
                        <td>Appointment Date</td>
                        <td>Order Date</td>
                        <td>Status</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($orderResult)
                        {
                            $s = 1;
                            foreach($orderResult as $rrr)
                            {
                                $customernameArr = $this->db->select('firstname,lastname')->get_where(db_prefix().'contacts', array('userid' => $rrr->customer_id))->row();
                                $customername = $customernameArr->firstname.' '.@$customernameArr->lastname;
                                if($rrr->quote_id > 0)
                                {
                                    $quoteDetails = $this->db->select('id,title,created_date')->get_where(db_prefix().'requestAQuote', array('id' => $rrr->quote_id))->row();
                                    $price_ = $this->db->get_where(db_prefix().'quoteRequest_garageReply', array('quote_id' => $rrr->quote_id))->row('price');
                                    
                                    ?>
                                        <tr>
                                            <td><?= $rrr->id; ?></td>
                                            <td class="hide"><?= $s++;?></td>
                                            <td><?= $customername; ?></td>
                                            <td><a href="javascript:void(0);" data-toggle="modal" onclick="getPreviewReq(<?= $quoteDetails->id; ?>)" data-target="#detailsModal"><?= $quoteDetails->title; ?></a></td>
                                            <td><span style="display:none;"><?php echo $rrr->appointment_date; ?></span><?= date('d-M-Y', $rrr->appointment_date); ?></td>
                                            <td><span style="display:none;"><?php echo strtotime($rrr->created_date); ?></span><?= date('d-M-Y', strtotime($rrr->created_date)); ?></td>
                                            <td>
                                                <?php
                                                    if(time() < $rrr->appointment_date)
                                                    {
                                                        if($rrr->status == 3)
                                                        {
                                                            echo '<span class="badge badge-active">Completed</span>';    
                                                        }
                                                        else if($rrr->status == 4)
                                                        {
                                                            echo '<span class="badge badge-pending" id="status_'.$rrr->id.'">On Schedule</span>';    
                                                        }
                                                        else if($rrr->status == 5)
                                                        {
                                                            echo '<span class="badge badge-cancelled" id="status_'.$rrr->id.'">Cancelled</span>';    
                                                        }
                                                        else
                                                        {
                                                            echo '<span class="badge badge-pending" id="status_'.$rrr->id.'">On Schedule</span>';
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if($rrr->status == 3)
                                                        {
                                                            echo '<span class="badge badge-active">Completed</span>';    
                                                        }
                                                        else if($rrr->status == 4)
                                                        {
                                                            echo '<span class="badge badge-pending">On Schedule</span>';    
                                                        }
                                                        else if($rrr->status == 5)
                                                        {
                                                            echo '<span class="badge badge-cancelled">Cancelled</span>';    
                                                        }
                                                        else
                                                        {
                                                            echo '<span class="badge badge-cancelled">Expired</span>';    
                                                        }
                                                    }
                                                ?>
                                            </td>
                                            <td>
                                                <p class="btn-group" role="group" aria-label="Basic example">
                                                    <!--<button type="button" class="btn btn-appologise" data-toggle="modal" onClick="customerReplyModel(<?= $rrr->quote_id; ?>);" data-target="#customerReplyModel" title="Customer reply"><i class="fa fa-user"></i></button>-->
                                                    <?php
                                                        if(time() < $rrr->appointment_date)
                                                        {
                                                            ?>
                                                                <!--<input type="checkbox" title="Apologise" id="apologise_<?= $rrr->id; ?>" onclick="confirmAppoinment(<?= $rrr->id ?>)" <?= ($rrr->status == 1)?"checked":""; ?>>-->
                                                                <button type="button" class="btn btn-confirm mr-3" disabled id="confirmid<?= $rrr->id; ?>" title="Confirm Order"><i class="fa fa-check"></i></button>
                                                                <?php
                                                                    if($rrr->status == 6 || $rrr->status == 4)
                                                                    {
                                                                        ?>
                                                                            <button type="button" class="btn btn-msg mr-3" id="cancelid<?= $rrr->id; ?>" title="Apologize" onClick="cancelOrder('<?= $rrr->id; ?>')"><i class="fa fa-hand-paper-o"></i></button>
                                                                        <?php
                                                                    }
                                                                    else
                                                                    {
                                                                        echo '<button type="button" class="btn btn-msg mr-3" title="Apologize" disabled><i class="fa fa-hand-paper-o"></i></button>';
                                                                    }
                                                                ?>
                                                                <button type="button" class="btn btn-confirm mr-3" id="compid<?= $rrr->id; ?>" data-toggle="tooltip" title="Mark as completed" <?= ($rrr->status == 5)?"disabled":""; ?> <?= ($rrr->status == 3)?"disabled":""; ?>  onclick="confirmAppoinment('<?= $rrr->id ?>', '<?= $rrr->quote_id; ?>')"  ><i class="fa fa-thumbs-up"   data-toggle="tooltip"  aria-hidden="true"></i></button>
                                                            <?php
                                                        }
                                                        else
                                                        {
                                                            ?>
                                                                <!--<input type="checkbox" title="Apologise" <?= ($rrr->status == 1)?"checked":""; ?> disabled>-->
                                                                <button type="button" class="btn btn-confirm mr-3" disabled title="Expired"><i class="fa fa-check"></i></button>
                                                                <button type="button" class="btn btn-msg mr-3" title="Expired" disabled><i class="fa fa-hand-paper-o"></i></button>
                                                                <button type="button" class="btn btn-confirm mr-3"  data-toggle="tooltip" title="Expired" disabled <?= ($rrr->status == 3)?"disabled":""; ?>><i class="fa fa-thumbs-up"   data-toggle="tooltip"  aria-hidden="true"></i></button>
                                                            <?php
                                                        }
                                                    ?>
                                                    <a href="<?= base_url('garages/messages/'.$rrr->customer_id); ?>" type="button" class="btn btn-msg mr-3" data-toggle="tooltip" title="Message">
                                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                                    </a>
                                                    <button type="button" class="btn btn-confirm mr-3" data-toggle="modal" onClick="getQuoteDetails(<?= $rrr->quote_id; ?>);" data-target="#quoteDetailsModal" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                                </p>
                                            </td>
                                        </tr>
                                    <?php
                                }
                                else
                                {
                                    $productDetails = $this->db->select('id,price_with_fitting,product_name')->get_where(db_prefix().'_product', array('id' => $rrr->product_id))->row();
                                    ?>
                                        <tr>
                                            <td><?= $rrr->id; ?></td>
                                            <td class="hide"><?= $s++;?></td>
                                            <td><?= $customername; ?></td>
                                            <td><a href="javascript:void(0);" ><?= $productDetails->product_name; ?></a>
                                                <?php
                                                    if($rrr->fitting_type)
                                                    {
                                                        echo '<br>( <small>'.str_replace('_',' ',$rrr->fitting_type).' </small>)';
                                                    }
                                                    if($rrr->qty != '')
                                                    {
                                                        echo '<br>QTY '.$rrr->qty;
                                                    }
                                                ?>
                                            </td>
                                            <td><span style="display:none;"><?php echo $rrr->appointment_date; ?></span><?= date('d-M-Y', $rrr->appointment_date); ?></td>
                                            <td><span style="display:none;"><?php echo strtotime($rrr->created_date); ?></span><?= date('d-M-Y', strtotime($rrr->created_date)); ?></td>
                                            <td>
                                                <?php
                                                    if(time() < $rrr->appointment_date)
                                                    {
                                                        if($rrr->status == 3)
                                                        {
                                                            echo '<span class="badge badge-active">Completed</span>';    
                                                        }
                                                        else if($rrr->status == 4)
                                                        {
                                                            echo '<span class="badge badge-pending" id="status_'.$rrr->id.'">On Schedule</span>';    
                                                        }
                                                        else if($rrr->status == 5)
                                                        {
                                                            echo '<span class="badge badge-cancelled" id="status_'.$rrr->id.'">Cancelled</span>';    
                                                        }
                                                        else
                                                        {
                                                            echo '<span class="badge badge-pending" id="status_'.$rrr->id.'">Pending confirmation from garage</span>';
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if($rrr->status == 3)
                                                        {
                                                            echo '<span class="badge badge-active">Completed</span>';    
                                                        }
                                                        else if($rrr->status == 4)
                                                        {
                                                            echo '<span class="badge badge-pending">On Schedule</span>';    
                                                        }
                                                        else if($rrr->status == 5)
                                                        {
                                                            echo '<span class="badge badge-cancelled">Cancelled</span>';    
                                                        }
                                                        else
                                                        {
                                                            echo '<span class="badge badge-cancelled">Expired</span>';    
                                                        }
                                                    }
                                                ?>
                                            </td>
                                            <td>
                                                <p class="btn-group" role="group" aria-label="Basic example">
                                                    <?php
                                                        if(time() < $rrr->appointment_date)
                                                        {
                                                            ?>
                                                                <!--<input type="checkbox" title="Apologise" id="apologise_<?= $rrr->id; ?>" onclick="confirmAppoinment(<?= $rrr->id ?>)" <?= ($rrr->status == 1)?"checked":""; ?>>-->
                                                                <?php
                                                                    if($rrr->status == 6){
                                                                    ?>
                                                                        <button type="button" class="btn btn-confirm mr-3 mb-3" id="confirmid<?= $rrr->id; ?>"  title="Confirm Order" onClick="acceptOrder('<?= $rrr->id; ?>')"><i class="fa fa-check"></i></button>
                                                                    <?php
                                                                    }
                                                                    else
                                                                    {
                                                                        echo '<button type="button" class="btn btn-confirm mr-3" id="confirmid'.$rrr->id.'"  disabled title="Confirm Order"><i class="fa fa-check"></i></button>';
                                                                    }
                                                                    if($rrr->status == 6 || $rrr->status == 4)
                                                                    {
                                                                        ?>
                                                                            <button type="button" class="btn btn-msg mr-3 mb-3" title="Apologize" id="cancelid<?= $rrr->id; ?>" onClick="cancelOrder('<?= $rrr->id; ?>')"><i class="fa fa-hand-paper-o"></i></button>
                                                                        <?php     
                                                                    }
                                                                    else
                                                                    {
                                                                       echo '<button type="button" class="btn btn-msg mr-3" title="Expired" disabled><i class="fa fa-hand-paper-o"></i></button>';
                                                                    }
                                                                ?>
                                                                <button type="button" class="btn btn-confirm mr-3 mb-3" data-toggle="tooltip" title="Mark as completed" <?= ($rrr->status == 5)?"disabled":""; ?> <?= ($rrr->status == 3)?"disabled":""; ?>   onclick="confirmAppoinment('<?= $rrr->id ?>', '<?= $rrr->quote_id; ?>')" ><i class="fa fa-thumbs-up"   data-toggle="tooltip"  aria-hidden="true"></i></button>
                                                            <?php
                                                        }
                                                        else
                                                        {
                                                            ?>
                                                               <!--<input type="checkbox" title="Apologise" <?= ($rrr->status == 1)?"checked":""; ?> disabled>-->
                                                               <button type="button" class="btn btn-confirm mr-3 mb-3" disabled title="Expired"><i class="fa fa-check"></i></button>
                                                               <button type="button" class="btn btn-msg mr-3 mb-3" title="Expired" disabled><i class="fa fa-hand-paper-o"></i></button>
                                                               <button type="button" class="btn btn-confirm mr-3 mb-3"  data-toggle="tooltip" disabled title="Expired" <?= ($rrr->status == 3)?"disabled":""; ?>><i class="fa fa-thumbs-up"   data-toggle="tooltip"  aria-hidden="true"></i></button>
                                                            <?php
                                                        }
                                                    ?>
                                                    <a href="<?= base_url('garages/messages/'.$rrr->customer_id); ?>" type="button" class="btn btn-msg mr-3 mb-3" data-toggle="tooltip" title="Message">
                                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                                    </a>
                                                    <button type="button" class="btn btn-confirm mr-3 mb-3" data-toggle="modal" onClick="getProductDetails(<?= $rrr->product_id; ?>);" data-target="#quoteDetailsModal" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                                </p>
                                            </td>
                                        </tr>
                                    <?php
                                }
                            }
                            if($s == 1)
                            {
                                ?>
                                    <tr>
                                        <td valign="top" colspan="7" class="dataTables_empty">No matching records found</td>
                                    </tr>
                                <?php
                            }
                            /*
                            $s = 1;
                            foreach($orderResult as $rrr)
                            {
                                if($rrr->remove_id == 0 && $rrr->qty != null)
                                {
                                    $scheduleResult = $this->db->get_where(db_prefix().'schedule_appoinment', array('quote_id' => $rrr->quote_id, 'customer_id' => $rrr->customer_id, 'garage_id' => $rrr->garage_id))->row('appointment_date');
                                    if($scheduleResult)
                                    {
                                        $s++;
                                        $customernameArr = $this->db->select('firstname,lastname')->get_where(db_prefix().'contacts', array('userid' => $rrr->customer_id))->row();
                                        $customername = $customernameArr->firstname.' '.@$customernameArr->lastname;
                                        $quoteDetails = $this->db->select('title,created_date')->get_where(db_prefix().'requestAQuote', array('id' => $rrr->quote_id))->row();
                                        ?>
                                            <tr>
                                                <td><?= $rrr->id?></td></td>
                                                <td><?= $customername; ?></td>
                                                <td><?= $quoteDetails->title; ?></td>
                                                <td><?= date('d M Y', strtotime($quoteDetails->created_date)); ?></td>                                              
                                                <td><span class="badge badge-active">Confirmed</span></td>
                                                <td><?= date('d M Y', strtotime($scheduleResult)); ?></td>
                                                <td>
                                                    <a class="btn-group" role="group" aria-label="Basic example">
                                                        <button type="button" class="btn btn-re-activate mr-3" data-toggle="tooltip" title="Reschedule appointment"><i class="fa fa-calendar" aria-hidden="true"></i></button>
                                                        <button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" title="Apologise"><i class="fa fa-thumbs-down" aria-hidden="true"></i></button>
                                                    </a>
                                                    <a href="javascript:void(0);" data-toggle="tooltip" title="Detail" class="text-muted">
                                                        <i class="fa fa-ellipsis-v mt-8" aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php
                                    }
                                }
                            }
                            if($s == 1)
                            {
                                ?>
                                    <tr>
                                        <td valign="top" colspan="7" class="dataTables_empty">No matching records found</td>
                                    </tr>
                                <?php
                            }
                            */
                        }
                        else
                        {
                            ?>
                                <tr>
                                    <td valign="top" colspan="7" class="dataTables_empty">No matching records found</td>
                                </tr>
                            <?php
                        }
                    ?>
                    <!--
                    <tr>
                        <td class="col col-1">#001</td>
                        <td class="col col-2">Tanmay Sasvadkar</td>
                        <td class="col col-3">AC Services</td>
                        <td class="col col-4">13 May 2020</td>
                        <td class="col col-5"><span class="badge badge-active">Confirmed</span></td>
                        <td class="col col-6">02 July 2020</td>
                        <td class="col col-8">
                            <a class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-re-activate mr-3" data-toggle="tooltip" title="Reschedule appointment"><i class="fa fa-calendar" aria-hidden="true"></i></button>
                                <button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" title="Apologise"><i class="fa fa-thumbs-down" aria-hidden="true"></i></button>
                            </a>
                            <a href="javascript:void(0);" data-toggle="tooltip" title="Detail" class="text-muted">
                                <i class="fa fa-ellipsis-v mt-8" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                    <tr >
                        <td class="col col-1">#002</td>
                        <td class="col col-2">Pavan Prajapati</td>
                        <td class="col col-3">Body Parts Repairing</td>
                        <td class="col col-4">13 June 2020</td>
                        <td class="col col-5"><span class="badge badge-pending">Pending Confirmation</span></td>
                        <td class="col col-6">10 June 2020</td>
                        <td class="col col-8">
                            <a class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-confirm mr-3" data-toggle="tooltip" title="Confirm Order"><i class="fa fa-check" aria-hidden="true"></i></button>
                                <button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" title="Apologise"><i class="fa fa-thumbs-down" aria-hidden="true"></i></button>
                            </a>
                            <a href="javascript:void(0);" data-toggle="tooltip" title="Detail" class="text-muted">
                                <i class="fa fa-ellipsis-v mt-8" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                    <tr >
                        <td class="col col-1">#003</td>
                        <td class="col col-2">Krishnpal Yadav</td>
                        <td class="col col-3">Tyre Replacement & Services</td>
                        <td class="col col-4">03 Apr 2020</td>
                        <td class="col col-5"><span class="badge badge-cancelled">Cancelled</span></td>
                        <td class="col col-6">20 May 2020</td>
                        <td class="col col-8">
                            <a class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-confirm mr-3" data-toggle="tooltip" title="Confirm Order"><i class="fa fa-check" aria-hidden="true"></i></button>
                                <button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" title="Apologise"><i class="fa fa-thumbs-down" aria-hidden="true"></i></button>
                            </a>
                            <a href="javascript:void(0);" data-toggle="tooltip" title="Detail" class="text-muted">
                                <i class="fa fa-ellipsis-v mt-8" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                    <tr >
                        <td class="col col-1">#004</td>
                        <td class="col col-2">Basant Yaduvanshi</td>
                        <td class="col col-3">Painting Services</td>
                        <td class="col col-4">27 June 2020</td>
                        <td class="col col-5"><span class="badge badge-active">Confirmed</span></td>
                        <td class="col col-6">04 July 2020</td>
                        <td class="col col-8">
                            <a class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-re-activate mr-3" data-toggle="tooltip" title="Reschedule appointment"><i class="fa fa-calendar" aria-hidden="true"></i></button>
                                <button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" title="Apologise"><i class="fa fa-thumbs-down" aria-hidden="true"></i></button>
                            </a>
                            <a href="javascript:void(0);" data-toggle="tooltip" title="Detail" class="text-muted">
                                <i class="fa fa-ellipsis-v mt-8" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td class="col col-1">#005</td>
                        <td class="col col-2">Manas Phadnis</td>
                        <td class="col col-3">Electrical Services</td>
                        <td class="col col-4">21 May 2020</td>
                        <td class="col col-5"><span class="badge badge-cancelled">Cancelled</span></td>
                        <td class="col col-6">05 June 2020</td>
                        <td class="col col-8">
                            <a class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-confirm mr-3" data-toggle="tooltip" title="Confirm Order"><i class="fa fa-check" aria-hidden="true"></i></button>
                                <button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" title="Apologise"><i class="fa fa-thumbs-down" aria-hidden="true"></i></button>
                            </a>
                            <a href="javascript:void(0);" data-toggle="tooltip" title="Detail" class="text-muted">
                                <i class="fa fa-ellipsis-v mt-8" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                    -->
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Quote Details Modal -->
<div id="customerReplyModel" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Buyer reply</h4>
            </div>
            <div class="modal-body customerReply">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Quote Details Modal -->
<div id="quoteDetailsModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable-2">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Order Details</h4>
            </div>
            <div class="modal-body quoteDetails">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('#quoteDetailsModal').on('hidden.bs.modal', function (e) {
        $('.close').click();
    })


    function getQuoteDetails(Id)
    {
        $('.quoteDetails').html('<p>Loading....</p>');
        var token_name = csrfData['token_name'];
        var token_hash = csrfData['hash'];
        var str = {"id":Id,token_name:token_hash};
        $.ajax({
            url: '<?= site_url()?>garages/getQuoteDetails',
            type: 'POST',
            data: str,
            //dataType: 'json',
            cache: false,
            success: function(resp){
                if(resp)
                {
                    $('.quoteDetails').html(resp);
                    return true;
                }
                else
                {
                    
                    return false;
                }
            }
        });
    }
    function getProductDetails(Id)
    {
        $('.quoteDetails').html('<p>Loading....</p>');
        var token_name = csrfData['token_name'];
        var token_hash = csrfData['hash'];
        var str = {"id":Id,token_name:token_hash};
        $.ajax({
            url: '<?= site_url()?>garages/getProductDetails',
            type: 'POST',
            data: str,
            //dataType: 'json',
            cache: false,
            success: function(resp){
                if(resp)
                {
                    $('.quoteDetails').html(resp);
                    return true;
                }
                else
                {
                    
                    return false;
                }
            }
        });
    }

    /* Customer Reply */
    function customerReplyModel(Id)
    {
        $('.customerReply').html('<p>Loading....</p>');
        var token_name = csrfData['token_name'];
        var token_hash = csrfData['hash'];
        var str = {"id":Id,token_name:token_hash};
        $.ajax({
            url: '<?= site_url()?>garages/customerReplyModelOrder',
            type: 'POST',
            data: str,
            //dataType: 'json',
            cache: false,
            success: function(resp){
                if(resp)
                {
                    //$('#notify'+Id).css('display','none');
                    $('.customerReply').html(resp);
                    return true;
                }
                else
                {
                    
                    return false;
                }
            }
        });
    }

    /* Confirm Appoinment */
    function confirmAppoinment(id, qid)
    {
        var token_name = csrfData['token_name'];
        var token_hash = csrfData['hash'];
        bootbox.confirm({
            message: "Are You want to Mark as Complete ?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result){
                    var str = {"id":id,"status":status,token_name:token_hash};
                    $.ajax({
                        url: '<?= site_url()?>garages/confirmAppoinment',
                        type: 'POST',
                        data: str,
                        //dataType: 'json',
                        cache: false,
                        success: function(resp){
                            if(resp)
                            {
                                $('#status_'+id).removeClass('badge-cancelled');
                                $('#status_'+id).removeClass('badge-pending');
                                $('#status_'+id).addClass('badge-active');
                                $('#status_'+id).text('Completed');
                                $('#cancelid'+id).prop('disabled', true); 
                                $('#confirmid'+id).prop('disabled', true); 
                                $('#compid'+id).prop('disabled', true); 
                                return true;
                            }
                            else
                            {
                                $('#cancelid'+id).prop('disabled', false); 
                                $('#confirmid'+id).prop('disabled', false); 
                                $('#compid'+id).prop('disabled', false); 
                                return false;
                            }
                        }
                    });
                }
            }
        });
        /*
        if(qid > 0)
        {
            var token_name = csrfData['token_name'];
            var token_hash = csrfData['hash'];
            var strstatus = {"id":id,"qid":qid,token_name:token_hash};
            $.ajax({
                url: '<?= site_url()?>garages/checkAwardStatus',
                type: 'POST',
                data: strstatus,
                //dataType: 'json',
                cache: false,
                success: function(resp){
                    if(resp)
                    {
                        bootbox.confirm({
                            message: "Awarded to someone else!",
                            buttons: {
                                confirm: {
                                    label: 'Ok',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'Close',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function (result) {}
                        });  
                    }
                    else
                    {
                        bootbox.confirm({
                            message: "Are You want to Mark as Complete ?",
                            buttons: {
                                confirm: {
                                    label: 'Yes',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function (result) {
                                if(result){
                                    var str = {"id":id,"status":status,token_name:token_hash};
                                    $.ajax({
                                        url: '<?= site_url()?>garages/confirmAppoinment',
                                        type: 'POST',
                                        data: str,
                                        //dataType: 'json',
                                        cache: false,
                                        success: function(resp){
                                            if(resp)
                                            {
                                                $('#status_'+id).removeClass('badge-cancelled');
                                                $('#status_'+id).removeClass('badge-pending');
                                                $('#status_'+id).addClass('badge-active');
                                                $('#status_'+id).text('Completed');
                                                $('#cancelid'+id).prop('disabled', true); 
                                                $('#confirmid'+id).prop('disabled', true); 
                                                $('#compid'+id).prop('disabled', true); 
                                                return true;
                                            }
                                            else
                                            {
                                                $('#cancelid'+id).prop('disabled', false); 
                                                $('#confirmid'+id).prop('disabled', false); 
                                                $('#compid'+id).prop('disabled', false); 
                                                return false;
                                            }
                                        }
                                    });
                                }
                            }
                        });   
                    }
                }
            });
        }
        else
        {
            bootbox.confirm({
                message: "Are You want to Mark as Complete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if(result){
                        var str = {"id":id,"status":status,token_name:token_hash};
                        $.ajax({
                            url: '<?= site_url()?>garages/confirmAppoinment',
                            type: 'POST',
                            data: str,
                            //dataType: 'json',
                            cache: false,
                            success: function(resp){
                                if(resp)
                                {
                                    $('#status_'+id).removeClass('badge-cancelled');
                                    $('#status_'+id).removeClass('badge-pending');
                                    $('#status_'+id).addClass('badge-active');
                                    $('#status_'+id).text('Completed');
                                    $('#cancelid'+id).prop('disabled', true); 
                                    $('#confirmid'+id).prop('disabled', true); 
                                    $('#compid'+id).prop('disabled', true); 
                                    return true;
                                }
                                else
                                {
                                    $('#cancelid'+id).prop('disabled', false); 
                                    $('#confirmid'+id).prop('disabled', false); 
                                    $('#compid'+id).prop('disabled', false); 
                                    return false;
                                }
                            }
                        });
                    }
                }
            });
        }
        */
    }

    function tagsNavBar() {
        $(".tag-dropdown").on("click", function(event) {
            event.stopPropagation();
            if (!$(this).hasClass('active')) {
                $('.tag-nav').removeClass('hidden-xs');
                $(this).addClass('active');
                $(this).next("i").addClass('active');
            } else {
                $('.tag-nav').addClass('hidden-xs');
                $(this).removeClass('active');
                $(this).next("i").removeClass('active');
            }
        });
        $(document).click(function() {
            $('.tag-nav').addClass('hidden-xs');
            $('.tag-dropdown').removeClass('active');
            $('.tag-dropdown').next("i").removeClass('active');
        });
        $(".tag-nav a").on("click", function() {
            var tagText = $(this).text();
            $(".tag-dropdown").text(tagText);
        });
    }
    
    //Filter TAB
    function filterTab(){
        var tablist=$('.tag-nav .tags')
        tablist.bind("click",function(){
            $('.tag-nav .tags').removeClass("selected");
            $(this).addClass("selected");
            $('.tab-content').hide();   
            var show2Tab=$('.'+$(this).attr('data-rel'));       
            show2Tab.fadeIn();
        });
    }
        
    function acceptOrder(id)
    {
        bootbox.confirm({
            message: "Are You want to On Schedule ?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result){
                    var token_name = csrfData['token_name'];
                    var token_hash = csrfData['hash'];
                    var str = {"id":id,"status":4,token_name:token_hash};
                    $.ajax({
                        url: '<?= site_url()?>garages/acceptOrder',
                        type: 'POST',
                        data: str,
                        //dataType: 'json',
                        cache: false,
                        success: function(resp){
                            if(resp)
                            {
                                $('#status_'+id).removeClass('badge-active');
                                $('#status_'+id).removeClass('badge-cancelled');
                                $('#status_'+id).addClass('badge-pending');
                                $('#status_'+id).text('On Schedule');
                                $('#confirmid'+id).prop('disabled', true); 
                                return true;
                            }
                            else
                            {
                                $('#confirmid'+id).prop('disabled', false); 
                                return false;
                            }
                        }
                    });
                }
            }
        }); 
    } 
    
    function cancelOrder(id)
    {
        bootbox.confirm({
            message: "Are You want to cancel this ?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result){
                    var token_name = csrfData['token_name'];
                    var token_hash = csrfData['hash'];
                    var str = {"id":id,"status":5,token_name:token_hash};
                    $.ajax({
                        url: '<?= site_url()?>garages/cancelOrder',
                        type: 'POST',
                        data: str,
                        //dataType: 'json',
                        cache: false,
                        success: function(resp){
                            if(resp)
                            {
                                $('#status_'+id).removeClass('badge-pending');
                                $('#status_'+id).removeClass('badge-active');
                                $('#status_'+id).addClass('badge-cancelled');
                                $('#status_'+id).text('Cancelled');
                                $('#compid'+id).prop('disabled', true); 
                                $('#cancelid'+id).prop('disabled', true); 
                                $('#confirmid'+id).prop('disabled', true); 
                                return true;
                            }
                            else
                            {
                                $('#compid'+id).prop('disabled', false); 
                                $('#cancelid'+id).prop('disabled', false); 
                                $('#confirmid'+id).prop('disabled', false); 
                                return false;
                            }
                        }
                    });
                }
            }
        }); 
    }
</script>