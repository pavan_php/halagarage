<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style>
	.mb-0{margin-bottom:0px!important;}
	.mb1-0{margin-bottom:10px!important;}
	.login-register {
		padding: 5px 0;
		z-index: 999;
		position: relative;
		min-height: 100vh;
		text-align: center;
		display: -webkit-box;
		display: -moz-box;
		display: -ms-flexbox;
		display: -webkit-flex;
		display: flex;
		justify-content: center;
		align-items: center;
	}

	.login-register .form-section .form-box {
		/*float: left;*/
		width: 100%;
		text-align: left;
		position: relative;
	}

	.login-register h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
		font-family: 'Jost', sans-serif;
	}

	.login-register .bg-img {
		background: url(http://php.manageprojects.in/halagarage/uploads/login-bg.jpg);
		background-size: cover;
		top: 0;
		bottom: 0;
		opacity: 1;
		/*width: 100%;*/
		z-index: 999;
		padding-left: 50px;
		position: relative;
		display: flex;
		justify-content: center;
		align-items: center;
		padding: 30px 50px;
		height: 100%;
		background-size: cover;
		background-repeat: no-repeat;
		background-position: center;
		min-height: 609px;
	}

	.login-register .bg-img::before {
		background: #3385d9;
	}
	.login-register .bg-img::before {
		opacity: 0.8;
		content: "";
		display: block;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		height: 100%;
		width: 100%;
		position: absolute;
	}

	.login-register .logo {
		border-radius: 50px;
		margin-bottom: 0;
		display: inline-block;
		top: 40px;
		position: absolute;
		left: 80px;
	}

	.login-register .logo img {
		height: 80px;
	}

	.login-register .login-box {
		background: #fff;
		margin: 0 auto;
		box-shadow: 0 0 35px rgba(0, 0, 0, 0.1);
	}

	.login-register .login-box .form-info {
		background: #fff;
	}

	.login-register .form-section {
		padding: 180px 80px 100px;
		border-radius: 10px 0 0 10px;
		text-align: left;
	}

	.login-register-bg {
		background: #f7f7f7;
	}

	.login-register .pad-0 {
		padding: 0;
	}

	.login-register label {
		color: #333;
		font-size: 16px;
		font-weight: 500;
		margin-bottom: 10px;
	}

	.login-register .form-section p {
		color: #717171;
		font-size: 13px;
		font-weight: 500;
	}
	
	.login-register .text-danger{
		bottom: -32px;
		color: #dc3545!important;
		position: absolute;
		bottom: -28px;
		font-size: 11px;
		font-weight: 400;
		text-transform: capitalize;
	}

	.login-register .form-section p a {
		color: #717171;
		font-weight: 500;
	}

	.login-register .form-section p a:hover {
	}

	.login-register .form-section ul {
		list-style: none;
		padding: 0;
		height: 50px!important;
		line-height: 32px;
		padding:6px 12px;
	}

	.login-register .form-section .social-list li {
		display: inline-block;
		margin-bottom: 5px;
	}

	.login-register .form-section .thembo {
		margin-left: 4px;
	}

	.login-register .form-section h1 {
		font-size: 27px;
		font-weight: 600;
		color: #3385d9;
		text-align: left;
	}

	.login-register .form-section h3 {
		margin: 0 0 50px;
		font-size: 18px;
		font-weight: 400;
		color: #313131;
	}

	.login-register .form-section .form-group {
		margin-bottom: 25px;
	}

	.login-register.form-section .form-box {
		/*float: left;*/
		width: 100%;
		text-align: left;
		position: relative;
	}

	.login-register .form-section .form-box input {
		float: none;
		width: 100%;
	}

	.login-register .form-section .input-text {
		padding: 10px 20px;
		font-size: 16px;
		outline: none;
		height: 50px;
		color: #717171;
		border-radius: 3px;
		font-weight: 500;
		border: 1px solid transparent;
		background: #fff;
		box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
	}

	.login-register .form-section .checkbox .terms {
		margin-left: 3px;
	}

	.login-register .form-section .btn-md {
		cursor: pointer;
		padding: 13px 50px 12px 50px;
		font-size: 17px;
		font-weight: 400;
		font-family: 'Jost', sans-serif;
		border-radius: 3px;
		text-align:center;
	}

	.login-register .form-section input[type=checkbox], input[type=radio] {
		margin-right: 3px;
	}

	.login-register .form-section button:focus {
		outline: none;
		outline: 0 auto -webkit-focus-ring-color;
	}

	.login-register .form-section .btn-theme.focus, .btn-theme:focus {
		box-shadow: none;
	}

	.login-register .form-section .btn-theme {
		background: #3385d9;
		border: 1px solid #3385d9;
		color: #fff;
		box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
		display:block;
	}

	.login-register .form-section .btn-theme:hover {
		background: #fff;
		border: 1px solid #3385d9;
		color: #3385d9;
		box-shadow:none;
	}
	
	.login-register .form-section .btn-theme-outline:hover {
		background: #3385d9;
		border: 1px solid #3385d9;
		color: #fff;
		box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
		display:block;
	}

	.login-register .form-section .btn-theme-outline {
		background: #fff;
		border: 1px solid #3385d9;
		color: #3385d9;
		box-shadow:none;
		text-align:center;
		display:block;
	}

	.login-register .none-2 {
		display: none;
	}

	.login-register .form-section .terms {
		margin-left: 3px;
	}

	.login-register .btn-section {
		border-radius: 50px;
		margin-bottom: 0;
		display: inline-block;
		top: 80px;
		position: absolute;
		right: 80px;
	}

	.login-register .info {
		max-width: 500px;
		margin: 0 auto;
		align-self: center !important;
	}

	.login-register .btn-section .link-btn {
		font-size: 14px;
		float: left;
		background: transparent;
		font-weight: 400;
		text-align: center;
		text-decoration: none;
		text-decoration: blink;
		width: 100px;
		padding: 6px 5px;
		margin-right: 5px;
		color: #000;
		border-radius: 3px;
		background: #fff;
		box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
	}

	.login-register .btn-section .active-bg {
		color: #fff;
		background: #3385d9;
	}

	.login-register .btn-section .link-btn:hover {
		color: #fff;
		background: #3385d9;
	}

	.login-register .form-section .checkbox {
		font-size: 14px;
	}

	.login-register .form-section .form-check {
		float: left;
		margin-bottom: 0;
	}

	.login-register .form-section .form-check a {
		color: #717171;
		float: right;
	}

	.login-register .form-section .form-check-input {
		position: absolute;
		margin-left: 0;
	}

	.login-register .form-section .form-check label::before {
		content: "";
		display: inline-block;
		position: absolute;
		width: 18px;
		height: 18px;
		top: 2px;
		margin-left: -25px;
		border: 1px solid #c5c3c3;
		border-radius: 3px;
		background-color: #fff;
	}

	.login-register .form-section .form-check-label {
		padding-left: 0;
		margin-bottom: 0;
		font-size: 16px;
		font-weight: 400;
		color: #b9b9b9;
	}

	.login-register .form-section .checkbox-theme input[type="checkbox"]:checked + label::before {
		background-color: #3385d9;
		border-color: #3385d9;
	}
	
	.login-register .checkbox label::after {
		display: inline-block;
		position: absolute;
		width: 16px;
		height: 16px;
		left: 0;
		top: 0;
		margin-left: -25px;
		padding-left: 3px;
		padding-top: 4px;
		font-size: 11px;
		color: #fff;
	}

	.login-register .form-section input[type=checkbox]:checked + label:before {
		font-weight: 300;
		color: #e6e6e6;
		line-height: 15px;
		font-size: 14px;
		content: "\2713";
	}

	.login-register .form-section input[type=checkbox], input[type=radio] {
		margin-top: 4px;
	}

	.login-register .form-section a.forgot-password {
		font-size: 16px;
		color: #3385d9;
		float: right;
	}

	.login-register .social-list a {
		text-align: center;
		display: inline-block;
		font-size: 18px;
		margin-right: 20px;
		color: #717171;
	}

	.login-register .social-list a:hover {
		color: #28a745;
	}

	
	.login-register sup {
		color: #c10000;
	}
	
	.login-register .chosen-container-multi .chosen-choices .search-field input[type="text"]{
		padding:17px 0px;
		font-size: 1rem;
		font-weight: 400;
	}
	
	.login-register .input-group-append{
		position: absolute;
		right: 0;
		height: 50px
	}
	
	.login-register .upload-wrap .file-upload{
		width: 150px;
		text-align: center;
	}
	.login-register #file-upload1-filename .text-danger, .login-register #file-upload-filename .text-danger{
		bottom: -20px!important;
	}
	.login-register input[type="text"], .login-register input[type="email"], .login-register input[type="url"], input[type="password"], .login-register input[type="search"], .login-register input[type="tel"], .login-register input[type="number"], select {
		outline: none;
		-webkit-box-shadow: none;
		box-shadow: none;
		/* border: 1px solid #a8a8a8; */
		border: 0;
		height: 50px;
		line-height: 32px;
		background-color: transparent;
		border-radius: 0px;
	}
	.login-register input {
		border: 1px solid #e4e5e7!important;
	}
	.login-register .form-group{
		position:relative;
	}
	.login-register .alert {
		padding: 10px 15px;
		font-size: 14px;
		position: absolute;
		background-color: transparent;
		border: 0;
		right: 66px;
	}

	@media (max-width: 1200px) {
		.login-register .form-section {
			padding: 150px 60px 60px;
		}

		.login-register .logo {
			left: 60px;
			top: 60px
		}

		.login-register .btn-section {
			right: 60px;
			top: 60px
		}
	}

	@media (max-width: 992px) {
		.login-register .form-section {
			width: 100%;
		}

		.login-register .bg-img {
			min-height: 100%;
			border-radius: 5px;
		}

		.none-992 {
			display: none !important;
		}

		.login-register .login-box {
			max-width: 500px;
			margin: 0 auto;
			padding: 0;
		}
	}

	@media (max-width: 768px) {
		.login-register .form-section {
			padding: 30px;
		}

		.login-register .form-section {
			padding: 150px 30px 60px;
		}

		.login-register .logo {
			left: 30px;
		}

		.login-register .btn-section {
			right: 30px;
		}
	}
</style>
<?php
  if($settingRes)
  {
    if($settingRes->background_type == 'color')
    {
        ?>
          <body style="background-color: <?= $settingRes->background_color; ?>;">
        <?php
    }
    else
    {
        ?>
          <body style="background-image: url('<?= base_url() ?>uploads/loginPage/<?= $settingRes->background_image; ?>');">
        <?php
    }
  }
  else
  {
    ?>
     <body class="login_admin"<?php if(is_rtl()){ echo ' dir="rtl"'; } ?>>
    <?php
  }
?>
<div class="sign padding-50">
	<div class="login-register">
		<div class="container">
			<div class="row login-box">
				<div class="col-lg-5 bg-color-15 pad-0 none-992 bg-img"></div>
				<div class="col-lg-7 pad-0 form-info">
					<div class="form-section align-self-center">
						<div class="btn-section clearfix">
							<a href="<?= base_url('authentication/login')?>" class="link-btn active btn-1 active-bg">Back to Login</a>
							<!--<a href="register-31.html" class="link-btn btn-2 default-bg">Register</a>-->
						</div>
						<div class="logo">
							<a href="<?= base_url(); ?>">
								<img src="<?= base_url() ?>uploads/loginPage/<?= $settingRes->logo_image; ?>" alt="logo">
							</a>
						</div>
						<h1>Welcome!</h1>
						<h3>Sign into your account</h3>
						<div class="clearfix"></div>
						<?php echo form_open($this->uri->uri_string(),['id'=>'forgot-password-form']); ?>
							<?php echo validation_errors('<div class="alert alert-danger text-center">', '</div>'); ?>
							<?php if($this->session->flashdata('message-danger')){ ?>
							<div class="alert alert-danger">
							  <?php echo $this->session->flashdata('message-danger'); ?>
							</div>
							<?php } ?>
							<?php echo render_input('email','customer_forgot_password_email','','email'); ?>
							<div class="form-group">
							  <button type="submit" class="btn-md btn-theme btn-block"><?php echo _l('customer_forgot_password_submit'); ?></button>
							</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	

