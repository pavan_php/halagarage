<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style>
    .filled-newsfeed-wrap .newsfeed-wrap .card-body p{
          line-break: anywhere;
      }
    .checkbox-btn{
        display: flex;
        flex-flow: row wrap;
        margin-bottom: 25px;
      }
      .checkbox-btn input {
        padding: 10px;
        width: 100%;
        font-size: 17px;
        border: 1px solid #aaaaaa;
      }
      .checkbox-btn input[type="checkbox"] {
        display: none;
      }
      .sellfield.active {
        background-color: #fff;
        box-shadow: 0 2px 8px 0 rgba(0,0,0,.1), inset 0 1px 4px 0 rgba(0,0,0,.1);
        border: 1px solid #d9d9d9;
      }
      .checkbox-btn label {
        position: relative;
        color: #000;
        background-color: #ffffff;
        font-size: 14px;
        text-align: center;
        height: 40px;
        line-height: 38px;
        display: block;
        cursor: pointer;
        border: 1px solid #aaa;
        border-radius: 5px;
        margin-right: 10px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        height: 50px;
        /*padding: 17px 7px 0px 7px!important;*/
        font-size: 12px!important;
        background-color: #f1f5f7!important;
        text-transform: uppercase;
        margin-bottom: 15px;
      }
      .sellfield {
        position: relative;
        margin-top: 16px;
        text-shadow: 0 0 0 #2fcc71;
        width: 100%;
        background-color: #f7f7f7;
        border: 1px solid #d9d9d9;
        -webkit-border-radius: 3px;
        box-shadow: 0 1px 4px 0 rgba(0,0,0,.1);
        border-radius: 3px;
        padding: 0;
        font-size: 16px;
        padding: 0 57px 0 12px;
      }
      input:checked + label, input:checked + label, input:checked + label {
        border: 2px solid #667c97;
        background-color: transparent;
      }
      input:checked + label:after, input:checked + label:after, input:checked + label:after {
        content: "\f00c";
        width: 20px;
        height: 20px;
        line-height: 20px;
        border-radius: 100%;
        border: 2px solid #667c97;
        background-color: #f1f5f7;
        z-index: 999;
        position: absolute;
        top: -10px;
        right: -10px;
        font: normal normal normal 14px/1 FontAwesome;
        font-size: 10px;
        padding: 3px;
        color: #667c97;
      }
      
</style>
<div class="container">
	<div class="panel_s section-heading section-<?php echo _l($title); ?>">
		<div class="panel-body p-0">
			<!-- Empty Garage Profile Start -->
			<div class="empty-garage-wrap <?= ($profileBackgroundRes->id != '')?'hide':'hide'; ?>">
				<div class="header-wrap p-10 bg-white">
					<div class="d-flex align-items-center">
						<div><h4 class="fw-700">Profile</h4></div>
						<div class="ml-auto align-items-center">
							
						</div>
					</div>
				</div>
				<hr class="mt-0 mb-0">
				<div class="row">
					<div class="col-lg-12 p-40 text-center">
						<h3>No Details Found</h3>
					</div>
				</div>
			</div>
			<!-- Empty Garage Profile Start -->
			<!-- Edit Garage Profile Start -->
			<div class="edit-garage-wrap">
				<div class="row">
					<div class="col-lg-12 p-40">
					    
					</div>
				</div>
			</div>
			<!-- Edit Garage Profile End -->
			<!-- Filled Garage Profile Start -->
			<div class="filled-garage-wrap">
			    <?php
			        if($profileBackgroundRes->id != '')
			        {
			            if($profileBackgroundRes->type == 'color')
			            {
			                ?>
    			                <div class="banner-bg" style="background-color:<?= $profileBackgroundRes->value; ?>;">
    			            <?php
			            }
			            elseif($profileBackgroundRes->type == 'image')
			            {
			                ?>
    			                <div class="banner-bg" style="background-image: url(<?= base_url() ?>assets/images/bg/<?= $profileBackgroundRes->value; ?>);">
    			            <?php
			            }
			            else
			            {
			                ?>
    			                <div class="banner-bg" style="background-color:#808080;">
    			            <?php
			            }
			        }
			        else
			        {
			            ?>
			                <div class="banner-bg" style="background-color:#808080;">
			            <?php
			        }
			    ?>
					<h2 class="no-margin section-text"><span class="company"><?= (isset($profileData))?$profileData->company:" Enter Garage Name "; ?></span> <!--<a href="javascript:void(0);" data-toggle="modal" data-target="#shareModal"><i class="fa fa-share-alt" aria-hidden="true"></i></a>--></h2>
					<hr>
					<h5 class="address mb-20"><i class="fa fa-map-marker" aria-hidden="true"></i> Location : <span class="address_"><?= (isset($profileData))?$profileData->address:" Enter Address"; ?></span></h5>
					<h5 class="address"><i class="fa fa-phone" aria-hidden="true"></i> Contact : <span class="telephone"><?= (isset($profileData))?$profileData->telephone:" Enter Contact Number"; ?></span></h5>
					<div class="rating_wrap mt-0" style="display: flex;align-items: center;">
						<h5 class="rating-number"><i class="fa fa-star-o" aria-hidden="true"></i> Rating :</h5>
						<div class="rating" style="width: 85px;">
							<div id="full-stars-example-two">
								<div class="rating-group">
								<!--
									<label aria-label="1 star" class="rating__label" for="rating3-1" style="font-size: 1.4rem;"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
									<label aria-label="2 stars" class="rating__label" for="rating3-2" style="font-size: 1.4rem;"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
									<label aria-label="3 stars" class="rating__label" for="rating3-3" style="font-size: 1.4rem;"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
									<label aria-label="4 stars" class="rating__label" for="rating3-4" style="font-size: 1.4rem;"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
									-->
									<?php
									    $garagerating = $garageRating[0]->rating;
									    if($garagerating > 0)
									    {
									        $ik = 1;
									        for($rt = 0; $rt < 5; $rt++)
									        {
									            if($rt < intval($garagerating))
									            {
									                ?>
									                    <label aria-label="<?= $ik; ?> stars" class="rating__label" for="rating3-<?= $ik; ?>" style="font-size: 1.4rem;"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
									                <?php
									            }
									            else
									            {
									                ?>
									                    <label aria-label="<?= $ik; ?> stars" class="rating__label" for="rating3-<?= $ik; ?>" style="font-size: 1.4rem;"><i class="rating__icon  fa fa-star"></i></label>
									                <?php
									            }
									            $ik++;
									        }
									    }
									    else
									    {
									        ?>
									            <label aria-label="1 star" class="rating__label" for="rating3-1" style="font-size: 1.4rem;"><i class="rating__icon  fa fa-star"></i></label>
            									<label aria-label="2 stars" class="rating__label" for="rating3-2" style="font-size: 1.4rem;"><i class="rating__icon  fa fa-star"></i></label>
            									<label aria-label="3 stars" class="rating__label" for="rating3-3" style="font-size: 1.4rem;"><i class="rating__icon  fa fa-star"></i></label>
            									<label aria-label="4 stars" class="rating__label" for="rating3-4" style="font-size: 1.4rem;"><i class="rating__icon  fa fa-star"></i></label>
            									<label aria-label="5 stars" class="rating__label" for="rating3-5" style="font-size: 1.4rem;"><i class="rating__icon  fa fa-star"></i></label>
									        <?php
									    }
									?>
								</div>
							</div>
						</div>
						<a href="javascript:void(0);" class="total-count">[<?= $garageRating_count; ?> Reviews]</a>
					</div>
					<div class="custom-options">
						<ul>
							<li>
								<a href="mailto:<?= $profileData->company_email; ?>">
									<i class="fa fa-envelope" aria-hidden="true"></i><br>
									<span>SMS/ Email</span>
								</a>
							</li>
							<li>
								<!--<a href="javascript:void(0);">-->
								<a target="_blank" href="https://api.whatsapp.com/send?phone=<?= @$profileData->mobile; ?>">
									<i class="fa fa-whatsapp" aria-hidden="true"></i><br>
									<span>WhatsApp</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xl-5 col-lg-5 col-12">
			<!-- Gallery Start -->
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-12">
					<div class="panel_s">
						<div class="panel-body">
							<!-- Empty Gallery Start -->
							<div class="empty-gallery-wrap <?= (count($garageGalleryRes) > 0)?'hide':''; ?>">
								<div class="header-wrap p-10 bg-white">
									<div class="d-flex align-items-center">
										<div><h4 class="fw-700">Gallery</h4></div>
										<div class="ml-auto align-items-center">
											
										</div>
									</div>
								</div>
								<hr class="mt-0 mb-0">
								<div class="row">
									<div class="col-lg-12 p-40 text-center">
										<h3>No Photos Found</h3>
									</div>
								</div>
							</div>
							<!-- Empty Gallery Start -->
							<!-- Empty Gallery Start -->
							<!-- Filled Gallery Start -->
							<div class="filled-gallery-wrap  <?= (count($garageGalleryRes) > 0)?'':'hide'; ?>">
								<div class="header-wrap p-10 bg-white">
									<div class="d-flex align-items-center">
										<div><h4 class="fw-700">Gallery</h4></div>
										<div class="ml-auto align-items-center">
											
										</div>
									</div>
								</div>
								<hr class="mt-0 mb-0">
								<div class="row">
								    <?php
    								    if($garageGalleryRes)
    								    {
    								        foreach($garageGalleryRes as $res)
    								        {
    								            $src = '#';
                                                $attachment_key = $this->db->get_where(db_prefix().'files', array('rel_id' => $res->id, 'rel_type' => "gallery"))->row('file_name');
                                                if($attachment_key != '')
                                                {
                                                    $src = base_url('uploads/gallery/'.$res->id.'/'.$attachment_key); 
                                                }
    								            ?>
    								                <div class="col-lg-2 no-gutter">
                										<div class="gallery">
                											<div class="img-wrap">
                												<a href="javascript:void(0);" data-toggle="modal" data-target="#galleryModal" onClick="showGalleryImg('<?= $src ?>')">
                													<img src="<?= $src; ?>" alt="" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';" class="img-responsive">
                												</a>
                											</div>
                										</div>
                									</div>
    								            <?php
    								        }
    								    }
    								?>								    
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Gallery End -->

			<!-- Services Start -->
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-12">
					<div class="panel_s">
						<div class="panel-body">
						    <div class="header-wrap p-10 bg-white">
								<div class="d-flex align-items-center">
									<div><h4 class="fw-700">Services</h4></div>
								</div>
							</div>
							<hr class="mt-0 mb-0">
							<div class="row">
                                <div class="col-md-12">
                                    <section class="panel-s">
                                        <div class="checkbox-btn">
                							<?php
                							    if($serviceList)
                							    {
                							        $s=1;
                							        $serviceArr = explode(',',$garageServicesRes);
                							        foreach($serviceList as $ss)
                							        {
                							            if(in_array($ss->id, $serviceArr))
                							            {
                							                ?>
                    							                <input type="checkbox" disabled id="service<?= $s; ?>" value="service<?= $s; ?>">
                                                                <label class="label label-default mleft5 inline-block customer-group-list pointer" disabled for="service<?= $s; ?>"><?= $ss->name; ?></label>
                    							            <?php
                							            }
                							            $s++;
                							        }
                							    }
                							    else
                							    {
                							        echo '<p class="dataTables_empty"></p>';
                							    }
                							?>
                						</div>
                                    </section>
        						</div>
                            </div>
						</div>
					</div>
				</div>
			</div>
			<!-- Services End -->

			<!-- Products Start -->
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-12">
					<div class="panel_s">
						<div class="panel-body">
							<!-- Empty Products Start -->
							<div class="empty-product-wrap hide">
								<div class="header-wrap p-10 bg-white">
									<div class="d-flex align-items-center">
										<div><h4 class="fw-700">Products</h4></div>
										<div class="ml-auto align-items-center">
										    <div class="dl">
										        <a href="<?= base_url('customers/allProductList/'.$profileData->userid); ?>" class="btn btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="products-upload"> View All</span></a>
										    </div>
										</div>
									</div>
								</div>
								<hr class="mt-0 mb-0">
								<div class="row">
									<div class="col-lg-12 p-40 text-center">
										<h3>No Products Found</h3>
									</div>
								</div>
							</div>
							<!-- Empty Products Start -->
							<!-- Filled Products Starts -->
							<div class="filled-product-wrap">
								<div class="header-wrap p-10 bg-white">
									<div class="d-flex align-items-center">
										<div><h4 class="fw-700">Products</h4></div>
										<div class="ml-auto align-items-center">
										    <div class="dl">
										        <a href="<?= base_url('customers/allProductList/'.$profileData->userid); ?>" class="btn btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="products-upload"> View All</span></a>
										    </div>
										</div>
									</div>
								</div>
								<hr class="mt-0 mb-0">
								<?php
								    if($latestProduct)
								    {
								        foreach($latestProduct as $res)
								        {
								            ?>
								                <div class="garage-profile-product">
                									<div class="product">
                									    <?php
                                                            $src = '#';
                                                            $attachment_key = $this->db->get_where(db_prefix().'files', array('rel_id' => $res->id, 'rel_type' => "product"))->row('file_name');
                                                            if($attachment_key != '')
                                                            {
                                                                $src = base_url('uploads/product/'.$res->id.'/'.$attachment_key); 
                                                            }
                                                        ?>
                                                        <img src="<?= $src; ?>" height="80px" width="80px" alt="" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';" >
                										<h3 class="mt-5 mb-5"><?= $res->product_name; ?></h3>
                										<div class="shrt_des" style="margin-bottom:5px;"><p><span style="font-weight: 400;"><?= categoryname($res->category);?></span></p>
                										</div>
                									</div>
                								</div>
								            <?php
								        }
								    }
								    else
								    {
								        ?>
								            <div class="row">
            									<div class="col-lg-12 p-40 text-center">
            										<h3>No Products Found</h3>
            									</div>
            								</div>
								        <?php
								    }
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Products End -->
		</div>
		<div class="col-xl-7 col-lg-7 col-12">
			<div class="panel_s">
				<div class="panel-body">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#home">Newsfeed</a></li>
						<li><a data-toggle="tab" href="#menu1">About</a></li>
						<li><a data-toggle="tab" href="#menu2">Reviews</a></li>
					</ul>

					<div class="tab-content">
						<div id="home" class="tab-pane fade in active">							
							<!-- Filled Newsfeed Starts -->
							<div class="filled-newsfeed-wrap">
								<div class="newsfeed-wrap">
									<div class="card card-tabel mt-20 text-post newsfeed-data contentnewsfeed" style="display: block;">
										<div class="card-body">
											
										</div>
									</div>
									<?php
									    if($newsfeedResult)
									    {
									        foreach($newsfeedResult as $res)
									        {
									            ?>
									                <div class="card card-tabel mt-20 text-post newsfeed-data" style="display: block;">
                										<div class="card-body">
                											<?php
                											    echo $res->newsfeed;
                											?>
                										</div>
                									</div>
									            <?php
									        }
									    }
									?>
								</div>
							</div>
							<!-- Filled Newsfeed End -->
						</div>
						<div id="menu1" class="tab-pane fade">
							<!-- Empty About Start -->
							<div class="empty-about-wrap hide">
								<div class="row">
									<div class="col-lg-12 p-40 text-center">
										<h3>No Data Found</h3>
									</div>
								</div>
							</div>
							<!-- Empty About End -->
							<!-- Filled About Start -->
							<div class="filled-about-wrap">
								<div class="row">
									<div class="col-lg-12">
										<div class="d-flex align-items-center">
											<div class="hide"><h4 class="fw-700"><?= $garageAbountRes->title; ?></h4></div>
											<div class="ml-auto align-items-center">
												
											</div>
										</div>
								    	<div class="content"><?= $garageAbountRes->content; ?></div>
									</div>
								</div>
							</div>
							<!-- Filled About End -->
						</div>
						<div id="menu2" class="tab-pane fade">
							<!-- Empty Review Start -->
							<div class="empty-review-wrap">
								<div class="row">
									<div class="col-lg-12 p-40 text-center">
										<h3>No Revies Found</h3>
										<!--<button class="btn btn-default" type="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="edit"> Post Review</span></button>-->
									</div>
								</div>
							</div>
							<!-- Empty Review End -->
							<!-- Filled Review Start -->
							<div class="filled-review-wrap">
								<div class="review-wrap">
									<div class="profile-wrap">
										<img src="<?= base_url() ?>assets/images/user-placeholder.jpg" class="img-fluid" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';" >
									</div>
									<div class="profile-content">
										<span class="user-name">Harish Paul</span>
										<div class="date-time">
											<div class="rating_wrap mt-5">
												<div class="rating">
													<div id="full-stars-example-two">
														<div class="rating-group">
															<label aria-label="1 star" class="rating__label" for="rating3-1"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="2 stars" class="rating__label" for="rating3-2"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="3 stars" class="rating__label" for="rating3-3"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="4 stars" class="rating__label" for="rating3-4"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="5 stars" class="rating__label" for="rating3-5"><i class="rating__icon  fa fa-star"></i></label>
														</div>
													</div>
												</div>
												<a href="javascript:void(0);" class="rating_num">May 4, 2020</a>
											</div>
										</div>
										<div class="review-text"> 
											<span>he processes are constantly shifting and changing which makes it difficult at times to know what is expected of you and what the current objectives are. - Leadership can be unresponsive with follow through. A lot is promised to workers, and some of these things are swept aside or forgotten as other things gain priority. - Work life balance is lacking at times. Sales reps are often responding to emails or calls during their personal time</span>
										</div>
									</div>
								</div>
								<div class="review-wrap">
									<div class="profile-wrap">
										<img src="<?= base_url() ?>assets/images/user-placeholder.jpg" class="img-fluid">
									</div>
									<div class="profile-content">
										<span class="user-name">Harish Paul</span>
										<div class="date-time">
											<div class="rating_wrap mt-5">
												<div class="rating">
													<div id="full-stars-example-two">
														<div class="rating-group">
															<label aria-label="1 star" class="rating__label" for="rating3-1"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="2 stars" class="rating__label" for="rating3-2"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="3 stars" class="rating__label" for="rating3-3"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="4 stars" class="rating__label" for="rating3-4"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="5 stars" class="rating__label" for="rating3-5"><i class="rating__icon  fa fa-star"></i></label>
														</div>
													</div>
												</div>
												<a href="javascript:void(0);" class="rating_num">May 4, 2020</a>
											</div>
										</div>
										<div class="review-text"> 
											<span>Start within 30 days, finish within 48 hours.</span>
										</div>
									</div>
								</div>
								<div class="review-wrap">
									<div class="profile-wrap">
										<img src="<?= base_url() ?>assets/images/user-placeholder.jpg" class="img-fluid" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';" >
									</div>
									<div class="profile-content">
										<span class="user-name">Harish Paul</span>
										<div class="date-time">
											<div class="rating_wrap mt-5">
												<div class="rating">
													<div id="full-stars-example-two">
														<div class="rating-group">
															<label aria-label="1 star" class="rating__label" for="rating3-1"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="2 stars" class="rating__label" for="rating3-2"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="3 stars" class="rating__label" for="rating3-3"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="4 stars" class="rating__label" for="rating3-4"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="5 stars" class="rating__label" for="rating3-5"><i class="rating__icon  fa fa-star"></i></label>
														</div>
													</div>
												</div>
												<a href="javascript:void(0);" class="rating_num">May 4, 2020</a>
											</div>
										</div>
										<div class="review-text"> 
											<span>Start within 30 days, finish within 48 hours.</span>
										</div>
									</div>
								</div>
								<div class="review-wrap">
									<div class="profile-wrap">
										<img src="<?= base_url() ?>assets/images/user-placeholder.jpg" class="img-fluid" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';" >
									</div>
									<div class="profile-content">
										<span class="user-name">Harish Paul</span>
										<div class="date-time">
											<div class="rating_wrap mt-5">
												<div class="rating">
													<div id="full-stars-example-two">
														<div class="rating-group">
															<label aria-label="1 star" class="rating__label" for="rating3-1"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="2 stars" class="rating__label" for="rating3-2"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="3 stars" class="rating__label" for="rating3-3"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="4 stars" class="rating__label" for="rating3-4"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="5 stars" class="rating__label" for="rating3-5"><i class="rating__icon  fa fa-star"></i></label>
														</div>
													</div>
												</div>
												<a href="javascript:void(0);" class="rating_num">May 4, 2020</a>
											</div>
										</div>
										<div class="review-text"> 
											<span>Start within 30 days, finish within 48 hours.</span>
										</div>
									</div>
								</div>
								<div class="review-wrap">
									<div class="profile-wrap">
										<img src="<?= base_url() ?>assets/images/user-placeholder.jpg" class="img-fluid" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';" >
									</div>
									<div class="profile-content">
										<span class="user-name">Harish Paul</span>
										<div class="date-time">
											<div class="rating_wrap mt-5">
												<div class="rating">
													<div id="full-stars-example-two">
														<div class="rating-group">
															<label aria-label="1 star" class="rating__label" for="rating3-1"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="2 stars" class="rating__label" for="rating3-2"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="3 stars" class="rating__label" for="rating3-3"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="4 stars" class="rating__label" for="rating3-4"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
															<label aria-label="5 stars" class="rating__label" for="rating3-5"><i class="rating__icon  fa fa-star"></i></label>
														</div>
													</div>
												</div>
												<a href="javascript:void(0);" class="rating_num">May 4, 2020</a>
											</div>
										</div>
										<div class="review-text"> 
											<span>Start within 30 days, finish within 48 hours.</span>
										</div>
									</div>
								</div>
							</div>
							<!-- Filled Review End -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Share Modal -->
<div id="shareModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Share With Friends</h4>
            </div>
            <div class="modal-body" style="display:inline-block;">
                <ul class="share-social-menu">
                    <li><a href="javascript:void(0);"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-mobile" aria-hidden="true"></i></a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Gallery Modal -->
<div id="galleryModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="display:inline-block;">
                <div class="img-wrap showgimg">
					
				</div>
            </div>
        </div>
    </div>
</div>
<script>

            var images = [
                'https://unsplash.it/1300/800?image=875',
                'https://unsplash.it/1300/800?image=874',
                'https://unsplash.it/1300/800?image=872',
                'https://unsplash.it/1300/800?image=868',
                'https://unsplash.it/1300/800?image=839',
                'https://unsplash.it/1300/800?image=838'
            ];

            $(function() {

                $('#gallery1').imagesGrid({
                    images: images
                });
                $('#gallery2').imagesGrid({
                    images: images.slice(0, 5)
                });
                $('#gallery3').imagesGrid({
                    images: images.slice(0, 4)
                });
                $('#gallery4').imagesGrid({
                    images: images.slice(0, 3)
                });
                $('#gallery5').imagesGrid({
                    images: images.slice(0, 2)
                });
                $('#gallery6').imagesGrid({
                    images: images.slice(0, 1)
                });
                $('#gallery7').imagesGrid({
                    images: [
                        'https://unsplash.it/660/440?image=875',
                        'https://unsplash.it/660/990?image=874',
                        'https://unsplash.it/660/440?image=872',
                        'https://unsplash.it/750/500?image=868',
                        'https://unsplash.it/660/990?image=839',
                        'https://unsplash.it/660/455?image=838'
                    ],
                    align: true,
                    getViewAllText: function(imgsCount) { return 'View all' }
                });

            });

        </script>
        <script>
            function editProfile()
            {
                $('.empty-garage-wrap').css('display','none');
                //$('.edit-garage-wrap').css('display','block');
                $('.filled-garage-wrap').css('display','block');
            }
            
            /* Edit Garage AbountUs */
            function editGarageAbountUs()
            {
                $('.edit-about-wrap').css('display','block');
                $('.empty-about-wrap').css('display','none');
                $('.filled-about-wrap').css('display','none');
            }
            
            /* Garage AboutUs Save */
            function garageAboutUsSave()
            {
                //var title = $('#title').val();
                //var textareaValue = $('#description').val();
                var token_name = csrfData['token_name'];
                var token_hash = csrfData['hash'];
                var description = $('#description').summernote('code');
                if(description != '')
                {
                    var str = { "description":description,token_name:token_hash};
            	    $.ajax({
            	        url: '<?= base_url()?>garages/garageAboutUsSave',
            	        type: 'POST',
            	        data: str,
            	        //dataType: 'json',
            	        cache: false,
            	        success: function(resp){
            	            if(resp)
            	            {
            	                $('#description').html(description);
            	                $('.content').html(description);
            	                $('.edit-about-wrap').css('display','none');
            	                $('.filled-about-wrap').css('display','block');
            	                $('.empty-about-wrap').css('display','none');
            	                //return true;
            	            }
            	            else
            	            {
            	                $('.empty-about-wrap').css('display','none');
            	                $('.edit-about-wrap').css('display','block');
            	                $('.filled-about-wrap').css('display','none');
            	                
            	            }
            	        }
            	    });
                }
                else
                {
                    if(title == '')
                    {
                        $('.title').text('Title filed is required');
                        return false;
                    }
                    if(description == '')
                    {
                        $('.description').text('Description filed is required');
                        return false;
                    }
                }
            }
            
            /* Garage AboutUs Save */
            function garageNewsfeedSave()
            {
                var token_name = csrfData['token_name'];
                var token_hash = csrfData['hash'];
                var newsfeed = $('#newsfeed').summernote('code');
                if(newsfeed != '')
                {
                    var str = { "newsfeed":newsfeed,token_name:token_hash};
            	    $.ajax({
            	        url: '<?= base_url()?>garages/garageNewsfeedSave',
            	        type: 'POST',
            	        data: str,
            	        //dataType: 'json',
            	        cache: false,
            	        success: function(resp){
            	            if(resp)
            	            {
            	                $('#newsfeed').html('');
            	                $('.contentnewsfeed').append(newsfeed);
            	            }
            	            else
            	            {
            	               return false;
            	            }
            	        }
            	    });
                }
                else
                {
                    if(newsfeed == '')
                    {
                        $('.newsfeed').text('Newsfeed filed is required');
                        return false;
                    }
                }
            }
		</script>
<!-- bg js -->

  <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>
    function editGallery()
    {
        $('.empty-gallery-wrap').css('display','none');
        $('.filled-gallery-wrap').css('display','none');
        $('.edit-gallery-wrap').css('display','block');
    }
    
    function editSevices()
    {
        $('.empty-service-wrap').css('display','none');
        $('.filled-service-wrap').css('display','none');
        $('.edit-service-wrap').css('display','block');
    }

	$(function() {
		//bg color selector
		$(".color").click(function(){
			var color = $(this).attr("data-value");
			$(".banner-bg").css("background-color", color);
		});
	
		//add color picker if supported
		if (Modernizr.inputtypes.color) {
			$(".picker").css("display", 'inline-block');
			var c = document.getElementById('colorpicker');
			c.addEventListener('change', function(e) {
			//d.innerHTML = c.value;
			var color = c.value;
			$(".banner-bg").css("background-color", color);
				}, false);
		}
	});
	function pickColor() {
		$("#colorpicker").click();
	}

    $(document).ready(function() {
         $('#description,#newsfeed').summernote({
          height: 150
        });
    });


	$(document).ready(function(){
		// Set background image of a div on click of the button
		$("#image").click(function(){
			var imageUrl = "<?= base_url() ?>assets/images/bg/bg-1.png";
			$(".banner-bg").css("background-image", "url(" + imageUrl + ")");
		});    
		$("#image1").click(function(){
			var imageUrl = "<?= base_url() ?>assets/images/bg/bg-2.png";
			$(".banner-bg").css("background-image", "url(" + imageUrl + ")");
		}); 
		$("#image2").click(function(){
			var imageUrl = "<?= base_url() ?>assets/images/bg/bg-3.png";
			$(".banner-bg").css("background-image", "url(" + imageUrl + ")");
		}); 
		$("#image3").click(function(){
			var imageUrl = "<?= base_url() ?>assets/images/bg/bg-4.png";
			$(".banner-bg").css("background-image", "url(" + imageUrl + ")");
		}); 
		$("#image4").click(function(){
			var imageUrl = "<?= base_url() ?>assets/images/bg/bg-5.png";
			$(".banner-bg").css("background-image", "url(" + imageUrl + ")");
		}); 
		$("#image5").click(function(){
			var imageUrl = "<?= base_url() ?>assets/images/bg/bg-6.png";
			$(".banner-bg").css("background-image", "url(" + imageUrl + ")");
		}); 
	});

	$("#edit-bg").click(function(){
		$(".edit-bg-wrap").show();
	});
	
	function setProfileBg(type, val)
	{
	    if(type == 'color')
	    {
	        $('.banner-bg').removeAttr('style');
	        //$('.banner-bg').addAttr('style', 'style="background-color:'+val);
	    }
	    var token_name = csrfData['token_name'];
        var token_hash = csrfData['hash'];
	    if(type != '' && val != '')
	    {
	        var str = {'type':type,'value':val,token_name:token_hash};
        	    $.ajax({
        	        url: '<?= base_url()?>garages/setProfileBg',
        	        type: 'POST',
        	        data: str,
        	        //dataType: 'json',
        	        cache: false,
        	        success: function(resp){
        	            if(resp)
        	            {
        	                successmsg('Background set successfully');
        	            }
        	            else
        	            {
        	                errormsg('Access denied');
        	               return false;
        	            }
        	        }
        	    });
	    }
	    return false;
	}
	
	/* Set form value */
	function setformvalue(name)
	{
	    var text = $('#'+name).val();
	    if(name == 'address')
	    {
	        $('.address_').text(text);
	    }
	    else
	    {
	        $('.'+name).text(text);
	    }
	    $('.error'+name).text('');
	}
	
	/* Save Profile Data */
	function saveProfileData()
	{
	    var token_name = csrfData['token_name'];
        var token_hash = csrfData['hash'];
        var company = $('#company').val();
        var address = $('#address').val();
        var telephone = $('#telephone').val();
        var mobile = $('#mobile').val();
        
	    if(mobile != '' && company != '' && address != '' && telephone != '')
	    {
	        var str = {'company':company,'address':address,'mobile':mobile,'telephone':telephone,token_name:token_hash};
    	    $.ajax({
    	        url: '<?= base_url()?>garages/saveProfileData',
    	        type: 'POST',
    	        data: str,
    	        //dataType: 'json',
    	        cache: false,
    	        success: function(resp){
    	            if(resp)
    	            {
    	                successmsg('Profile data update successfully');
    	            }
    	            else
    	            {
    	                errormsg('Access denied');
    	               return false;
    	            }
    	        }
    	    });
	    }
	    else
	    {
	        if(company == '')
	        {
	            $('.errorcompany').text('Company field is required');
	            return false;
	        }
	        if(address == '')
	        {
	            $('.erroraddress').text('Address field is required');
	            return false;
	        }
	        if(telephone == '')
	        {
	            $('.errortelephone').text('Telephone field is required');
	            return false;
	        }
	        if(mobile == '')
	        {
	            $('.errormobile').text('Mobile field is required');
	            return false;
	        }
	    }
	}
	
	function showGalleryImg(img)
	{
	    $('.showgimg').html('<img src="'+img+'" class="img-responsive">');
	}
</script>