        <table class="responsive-table dataTable no-footer">
            <tr>
                <th width="20%" class="text-center">Description</th>
                <td><?= $quoteResult->title; ?></td>
            </tr>
            <tr>
                <th width="20%" class="text-center">Location</th>
                <td><?= locationname($quoteResult->locationid); ?></td>
            </tr>
            <!--
            <tr>
                <th width="20%" class="text-center">Address</th>
                <td><?= $quoteResult->address; ?></td>
            </tr>
            -->
            <!--
            <tr>
                <th width="20%" class="text-center">Service Category</th>
                <td><?= categoryname($quoteResult->categoryid); ?></td>
            </tr>
            <tr>
                <th width="20%" class="text-center">Service Sub Category</th>
                <td><?= categoryname($quoteResult->subcategoryid); ?></td>
            </tr>
            -->
            <?php
                $categoryArr = explode(',',$quoteResult->categoryid);
                $categorySubArr = explode(',',$quoteResult->subcategoryid);
                if($categoryArr)
                {
                    for($k=0; $k < count($categoryArr); $k++)
                    {
                        ?>
                            <tr>
                                <th width="20%" class="text-center">Service Category</th>
                                <td><?= categoryname($categoryArr[$k]); ?></td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-center">Service Sub Category</th>
                                <td><?= categoryname($categorySubArr[$k]); ?></td>
                            </tr>
                        <?php
                    }
                }
            ?>
            <tr>
                <th width="20%" class="text-center">Vehicle Category</th>
                <td><?= $quoteResult->vehiclecategory; ?></td>
            </tr>
            <tr>
                <th width="20%" class="text-center">Car Brand</th>
                <td><?= makename($quoteResult->brandid); ?></td>
            </tr>
            <tr>
                <th width="20%" class="text-center">Car Model</th>
                <td><?= modelname($quoteResult->carmodelid); ?></td>
            </tr>
            <tr>
                <th width="20%" class="text-center">Car Make Year</th>
                <td><?= yearname($quoteResult->carmakeyearid); ?></td>
            </tr>
            <tr>
                <th width="20%" class="text-center">Detailed Description</th>
                <td colspan="2"><?= $quoteResult->description; ?></td>
            </tr>
            <tr>
                <th class="text-center">Vehicle Registration Card</th>
                <td><img id="blah" height="100px" width="100px" data-toggle="modal" data-target="#galleryModal" onClick="showGalleryImg('<?= base_url('uploads/quote/'.$quoteResult->registrationcard); ?>')" src="<?= base_url('uploads/quote/'.$quoteResult->registrationcard); ?>" alt="" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';" /></td>
                <th class="text-center">Photo/Audio/Video Description</th>
                <td>
                    <?php
                        if($quoteResult->ext == 'mp3')
                        {
                            ?>
                                <audio controls>
                                    <source src="horse.ogg" type="audio/ogg">
                                    <source src="<?= base_url('uploads/quote/'.$quoteResult->upload_photo_video); ?>" type="audio/mpeg" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';" >
                                </audio>
                            <?php
                        }
                        elseif($quoteResult->ext == 'mp4')
                        {
                            ?>
                                <video width="100" height="100" controls>
                                    <source src="<?= base_url('uploads/quote/'.$quoteResult->upload_photo_video); ?>" type="video/mp4" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';" >
                                    <source src="movie.ogg" type="video/ogg">
                                    Your browser does not support the video tag.
                                </video>
                            <?php
                        }
                        elseif($quoteResult->ext == 'pdf' || $quoteResult->ext == 'PDF' )
                        {
                            ?>
                                <a target="_blank" href="<?= base_url('uploads/quote/'.$quoteResult->upload_photo_video); ?>"><img id="blah" height="80px" width="80px" src="#" alt="" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';"/></a>
                            <?php
                        }
                        elseif($quoteResult->upload_photo_video != '')
                        {
                            ?>
                                <img id="blah" height="100px" width="100px" data-toggle="modal" data-target="#galleryModal" onClick="showGalleryImg('<?= base_url('uploads/quote/'.$quoteResult->upload_photo_video); ?>')" src="<?= base_url('uploads/quote/'.$quoteResult->upload_photo_video); ?>" alt="" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';" />
                            <?php
                        }
                        else
                        {
                            ?>
                                <img id="blah" height="100px" width="100px" src="#" alt="" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';" />
                            <?php
                        }
                    ?>
                </td>
            </tr>
            <tr>
                <th width="20%" class="text-center">Chassis Number</th>
                <td colspan="2"><?= $quoteResult->chassis_no; ?></td>
            </tr>
        </table>
        <!-- Gallery Modal -->
        <div id="galleryModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body" style="display:inline-block;">
                        <div class="img-wrap showgimg">
        					<img src="#" class="img-responsive" onerror="this.onerror=null;this.src='<?php echo base_url(); ?>uploads/No-image.jpeg';">
        				</div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showGalleryImg(img)
        	{
        	    $('.showgimg').html('<img src="'+img+'" class="img-responsive">');
        	}
        </script>