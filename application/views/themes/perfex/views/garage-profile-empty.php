<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="panel_s section-heading section-<?php echo _l($title); ?>">
    <div class="panel-body">
        <h4 class="no-margin section-text"><?php echo _l($title); ?></h4>
    </div>
</div>
<div class="panel_s">
    <div class="panel-body">
        <div class="empty-profile-wrap">
            <h4>No data available</h4>
        </div>
    </div>
</div>
