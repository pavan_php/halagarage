<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="panel_s section-heading section-announcements">
    <div class="panel-body">
        <!--<h4 class="no-margin section-text"><?php echo _l($title); ?></h4>-->
        <div class="header-wrap p-10 bg-white">
            <div class="d-flex align-items-center">
                <div>
                    <h4 class="fw-700 mt-0">Start Quote</h4>
                </div>
                <div class="ml-auto align-items-center hide">
                    <div class="dl">
                        <form class="form-inline">
                            <div class="form-group">
                                <select class="selectpicker" data-live-search="true">
                                    <option>Select filter</option>
                                    <option>Location</option>
                                    <option>Category </option>
                                    <option>Sub Category</option>
                                    <option>Vehicle Brand</option>
                                    <option>Vehicle Model</option>
                                    <option>Vehicle Year</option>
                                    <option>Vehicle category assigned</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input class="search__input" type="text" placeholder="Search">
                            </div>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel_s section-heading section-announcements hide">
    <div class="panel-body">
        <form class="form-inline hide" method="post">
            <div class="form-group">
                <select class="selectpicker" data-live-search="true" id="make_id" onChange="getModelList(this.value);">
                    <option value="">Vehicle Brand</option>
                    <?php
                        if($carmake_result)
                        {
                            foreach($carmake_result as $rrr){
                                ?>
                                    <option value="<?= $rrr->id?>"><?= $rrr->name; ?></option>
                                <?php
                            }
                        }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <select class="selectpicker" data-live-search="true" id="model_id" onChange="getYearList(this.value);">
                    <option value="">Vehicle Model</option>
                </select>
            </div>
            <div class="form-group">
                <select class="selectpicker form-control" id="year_id">
                    <option value="">Vehicle Year</option>
                </select>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-theme">Reset</button>
            </div>
        </form>
    </div>
</div>

<div class="panel_s">
    <div class="panel-body panel-sm-body">
        <div class="stats-wrap p-10 bg-white">
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr class="table-header">
                        <td class="col col-1">Id</td>
                        <td class="col col-4">Request Date</td>
                        <td class="col col-5">Location</td>
                        <td class="col col-6">Category</td>
                        <td class="col col-6">Sub <br> category</td>
                        <td class="col col-7">Action</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($quoteRequestResult)
                        {
                            foreach($quoteRequestResult as $res)
                            {
                                $requestStatus = $this->db->get_where(db_prefix().'quoteRequest_garageReply', array('garage_id' => get_client_user_id(), 'quote_id' => $res->id))->num_rows();
                                $replyCustomer = $this->db->get_where(db_prefix().'quoteRequest_garageReply', array('garage_id' => get_client_user_id(), 'quote_id' => $res->id))->row();
                                $removeGarageStr = $res->remove_garage_id;
                                $removeGarageArr = explode(',',$removeGarageStr);
                                if(!in_array(get_client_user_id(), $removeGarageArr))
                                {
                                    ?>
                                        <tr id="removequote<?= $res->id; ?>">
                                            <td class="col col-1"><?= $res->id; ?></td>
                                            <td class="col col-4"><span style="display:none;"><?php echo strtotime($res->created_date); ?></span><?= date('d M Y',strtotime($res->created_date)); ?></td>
                                            <td class="col col-5"><?= locationname($res->locationid); ?></td>
                                            <td class="col col-6"><?= categoryname($res->categoryid); ?></td>
                                            <td class="col col-6"><?= categoryname($res->subcategoryid); ?></td>
                                            <td class="col col-7">
                                                <p class="btn-group" role="group" aria-label="Basic example">
                                                    <button type="button" class="btn btn-confirm mr-3" data-toggle="modal" onClick="getQuoteDetails(<?= $res->id; ?>);" data-target="#quoteDetailsModal" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                                    <?php
                                                        if($requestStatus > 0)
                                                        {
                                                            ?>
                                                                <a href="<?= base_url('messages'); ?>" class="btn btn-msg mr-3" title="Send inquiry"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                                            <?php
                                                        }
                                                        else
                                                        {
                                                            ?>
                                                                <button type="button" class="btn btn-reply mr-3" data-toggle="modal" id="replymodel<?= $res->id; ?>" onClick="quoteReplyModel(<?= $res->id; ?>);" title="Start-quotes" data-target="#quoteModal"><i class="fa fa-reply" aria-hidden="true"></i><span class="badge navigationbadge">New</span></button>
                                                                <a href="<?= base_url('messages'); ?>" class="btn btn-msg mr-3 replymodelmsg" id="replymodelmsg<?= $res->id; ?>" title="Send inquiry"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                                            <?php
                                                        }
                                                    ?>
                                                    <button type="button" class="btn btn-appologise mr-3" onClick="removeQuote(<?= $res->id; ?>)" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                    <?php
                                                        if($replyCustomer->qty != null)
                                                        {
                                                            ?>
                                                                 <button type="button" class="btn btn-user" data-toggle="modal" onClick="customerReplyModel(<?= $replyCustomer->id; ?>);" data-target="#customerReplyModel" title="Customer reply"><i class="fa fa-user"></i></button>
                                                            <?php
                                                        }
                                                    ?>
                                                </p>
                                            </td>
                                        </tr>
                                    <?php
                                }
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="quoteModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Start Quote</h4>
            </div>
            <div class="modal-body">
                <form id="sendquoteform">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="price">Price</label>
                            <input type="text" class="form-control" onkeyup="removeError('quotePrice');" maxlength="9" onkeypress="javascript:return isNumber(event)" id="quotePrice">
                            <p class="text-danger quotePrice"></p>
                            <input type="text" id="quoteID" class="hide" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="msg">Message</label>
                            <textarea class="form-control" id="quoteMessage" onkeyup="removeError('quoteMessage');" rows="6"></textarea>
                            <p class="text-danger quoteMessage"></p>
                        </div>
                    </div>
               </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success replybtn" onClick="quoteReplyGarage();">Submit</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- Reject Modal -->
<div id="rejectModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Confirm Rejection</h4>
            </div>
            <div class="modal-body">
                <h4>Are you sure, You want to Reject this quote?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Confirm</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<!-- Quote Details Modal -->
<div id="quoteDetailsModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Quote Details</h4>
            </div>
            <div class="modal-body quoteDetails">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!--<button type="button" class="btn btn-reply mr-3" onclick="closeDetailsModel()" data-toggle="modal" data-target="#quoteModal"><i class="fa fa-reply" aria-hidden="true"></i></button>-->
            </div>
        </div>
    </div>
</div>
<!-- Quote Details Modal -->
<div id="customerReplyModel" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">User reply</h4>
            </div>
            <div class="modal-body customerReply">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!--<button type="button" class="btn btn-reply mr-3" onclick="closeDetailsModel()" data-toggle="modal" data-target="#quoteModal"><i class="fa fa-reply" aria-hidden="true"></i></button>-->
            </div>
        </div>
    </div>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet"/> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
    function removeQuote(Id)
    { 
        if (confirm("Confirm remove this")) {
            var token_name = csrfData['token_name'];
            var token_hash = csrfData['hash'];
            var garageID = <?= get_client_user_id(); ?>;
            var str = {"id":Id,"garageID":garageID,token_name:token_hash};
            $.ajax({
                url: '<?= site_url()?>garages/removeQuote',
                type: 'POST',
                data: str,
                //dataType: 'json',
                cache: false,
                success: function(resp){
                    if(resp)
                    {
                        $('#removequote'+Id).addClass('hide');
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            });
        } else {
            
        }
    }

    function closeDetailsModel()
    {
        $('.close').click();
    }
    
    function customerReplyModel(Id)
    {
        $('.customerReply').html('<p>Loading....</p>');
        var token_name = csrfData['token_name'];
        var token_hash = csrfData['hash'];
        var str = {"id":Id,token_name:token_hash};
        $.ajax({
            url: '<?= site_url()?>garages/customerReplyModel',
            type: 'POST',
            data: str,
            //dataType: 'json',
            cache: false,
            success: function(resp){
                if(resp)
                {
                    $('.customerReply').html(resp);
                    return true;
                }
                else
                {
                    
                    return false;
                }
            }
        });
    }
    
    function getQuoteDetails(Id)
    {
        $('.quoteDetails').html('<p>Loding....</p>');
        var token_name = csrfData['token_name'];
        var token_hash = csrfData['hash'];
        var str = {"id":Id,token_name:token_hash};
        $.ajax({
            url: '<?= site_url()?>garages/getQuoteDetails',
            type: 'POST',
            data: str,
            //dataType: 'json',
            cache: false,
            success: function(resp){
                if(resp)
                {
                    $('.quoteDetails').html(resp);
                    return true;
                }
                else
                {
                    
                    return false;
                }
            }
        });
    }
    
    var quoteID = '';
    function quoteReplyModel(id)
    {
        if(id)
        {
            $('#quoteID').val(id);
            quoteID = id;
        }
        return false;
    }
    
    function quoteReplyGarage()
    {
        var quotePrice = $('#quotePrice').val();
        if(quotePrice == 0 || quotePrice < 0)
        {
            if(quotePrice == '')
            {
                $('.quotePrice').text('Price field is required');
            }
            else{
                $('.quotePrice').text('Price should be greater than 0');
            }               
            return false;
        }
        var quoteMessage = $('#quoteMessage').val();
        var garageID = <?= get_client_user_id(); ?>;
        if(quoteID != '' && quoteMessage != '' && quotePrice != '')
        {
            $('.replybtn').prop('disabled', true); 
            $('.replybtn').css('disabled','disabled');
            $('.replybtn').text('submitting...');
            var token_name = csrfData['token_name'];
            var token_hash = csrfData['hash'];
            var str = {"id":quoteID,"quotePrice":quotePrice,"garageID":garageID,"quoteMessage":quoteMessage,token_name:token_hash};
            $.ajax({
                url: '<?= site_url()?>garages/quoteReplyGarage',
                type: 'POST',
                data: str,
                //dataType: 'json',
                cache: false,
                success: function(resp){
                    if(resp)
                    {
                        $('.replybtn').prop('disabled', false); 
                        $('#sendquoteform')[0].reset();
                        $('.close').click();
                        $('#replymodel'+quoteID).addClass('hide');
                        $('#replymodelmsg'+quoteID).removeClass('hide');
                        swal("Your request has been sent successfully!");
                        $('.replybtn').text('Submit');
                        return true;
                    }
                    else
                    {
                        $('.replybtn').text('submit');
                        swal("Access denied");
                        return false;
                    }
                }
            });
        }
        else
        {
            $('.replybtn').css('disabled','disabled');
            if(quoteID == '')
            {
                $('#sendquoteform')[0].reset();
                $('.close').click();
                swal("Please select any quote request");
            }
            if(quotePrice == '')
            {
                $('.quotePrice').text('Price field is required');
                return false;
            }
            if(quoteMessage == '')
            {
                $('.quoteMessage').text('Message field is required');
                return false;
            }
        }
    }
    
    function removeError(name)
    {
        $('.'+name).text('');
    }
    
    $('#quoteModal').on('hidden.bs.modal', function (e) {        
        $('#sendquoteform')[0].reset();   
        $('.replybtn').text('submit');
    });
</script>