<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style>
	.mb-0{margin-bottom:0px!important;}
	.mb1-0{margin-bottom:10px!important;}
	.login-register {
		padding: 5px 0;
		z-index: 999;
		position: relative;
		min-height: 100vh;
		text-align: center;
		display: -webkit-box;
		display: -moz-box;
		display: -ms-flexbox;
		display: -webkit-flex;
		display: flex;
		justify-content: center;
		align-items: center;
	}

	.login-register .form-section .form-box {
		/*float: left;*/
		width: 100%;
		text-align: left;
		position: relative;
	}

	.login-register h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
		font-family: 'Jost', sans-serif;
	}

	.login-register .bg-img {
		background: url(http://php.manageprojects.in/halagarage/uploads/login-bg.jpg);
		background-size: cover;
		top: 0;
		bottom: 0;
		opacity: 1;
		/*width: 100%;*/
		z-index: 999;
		padding-left: 50px;
		position: relative;
		display: flex;
		justify-content: center;
		align-items: center;
		padding: 30px 50px;
		height: 100%;
		background-size: cover;
		background-repeat: no-repeat;
		background-position: center;
		min-height: 875px;
	}

	.login-register .bg-img::before {
		background: #3385d9;
	}
	.login-register .bg-img::before {
		opacity: 0.8;
		content: "";
		display: block;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		height: 100%;
		width: 100%;
		position: absolute;
	}

	.login-register .logo {
		border-radius: 50px;
		margin-bottom: 0;
		display: inline-block;
		top: 40px;
		position: absolute;
		left: 80px;
	}

	.login-register .logo img {
		height: 80px;
	}

	.login-register .login-box {
		background: #fff;
		margin: 0 auto;
		box-shadow: 0 0 35px rgba(0, 0, 0, 0.1);
	}

	.login-register .login-box .form-info {
		background: #fff;
	}

	.login-register .form-section {
		padding: 180px 80px 100px;
		border-radius: 10px 0 0 10px;
		text-align: left;
	}

	.login-register-bg {
		background: #f7f7f7;
	}

	.login-register .pad-0 {
		padding: 0;
	}

	.login-register label {
		color: #333;
		font-size: 16px;
		font-weight: 500;
		margin-bottom: 10px;
	}

	.login-register .form-section p {
		color: #717171;
		font-size: 13px;
		font-weight: 500;
	}
	
	.login-register .text-danger{
		bottom: -32px;
		color: #dc3545!important;
		position: absolute;
		bottom: -28px;
		font-size: 11px;
		font-weight: 400;
		text-transform: capitalize;
	}

	.login-register .form-section p a {
		color: #717171;
		font-weight: 500;
	}

	.login-register .form-section p a:hover {
	}

	.login-register .form-section ul {
		list-style: none;
		padding: 0;
		height: 50px!important;
		line-height: 32px;
		padding:6px 12px;
	}

	.login-register .form-section .social-list li {
		display: inline-block;
		margin-bottom: 5px;
	}

	.login-register .form-section .thembo {
		margin-left: 4px;
	}

	.login-register .form-section h1 {
		font-size: 27px;
		font-weight: 600;
		color: #3385d9;
		text-align: left;
	}

	.login-register .form-section h3 {
		margin: 0 0 50px;
		font-size: 18px;
		font-weight: 400;
		color: #313131;
	}

	.login-register .form-section .form-group {
		margin-bottom: 25px;
	}

	.login-register.form-section .form-box {
		/*float: left;*/
		width: 100%;
		text-align: left;
		position: relative;
	}

	.login-register .form-section .form-box input {
		float: none;
		width: 100%;
	}

	.login-register .form-section .input-text {
		padding: 10px 20px;
		font-size: 16px;
		outline: none;
		height: 50px;
		color: #717171;
		border-radius: 3px;
		font-weight: 500;
		border: 1px solid transparent;
		background: #fff;
		box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
	}

	.login-register .form-section .checkbox .terms {
		margin-left: 3px;
	}

	.login-register .form-section .btn-md {
		cursor: pointer;
		padding: 13px 50px 12px 50px;
		font-size: 17px;
		font-weight: 400;
		font-family: 'Jost', sans-serif;
		border-radius: 3px;
		text-align:center;
	}

	.login-register .form-section input[type=checkbox], input[type=radio] {
		margin-right: 3px;
	}

	.login-register .form-section button:focus {
		outline: none;
		outline: 0 auto -webkit-focus-ring-color;
	}

	.login-register .form-section .btn-theme.focus, .btn-theme:focus {
		box-shadow: none;
	}

	.login-register .form-section .btn-theme {
		background: #3385d9;
		border: 1px solid #3385d9;
		color: #fff;
		box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
		display:block;
	}

	.login-register .form-section .btn-theme:hover {
		background: #fff;
		border: 1px solid #3385d9;
		color: #3385d9;
		box-shadow:none;
	}
	
	.login-register .form-section .btn-theme-outline:hover {
		background: #3385d9;
		border: 1px solid #3385d9;
		color: #fff;
		box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
		display:block;
	}

	.login-register .form-section .btn-theme-outline {
		background: #fff;
		border: 1px solid #3385d9;
		color: #3385d9;
		box-shadow:none;
		text-align:center;
		display:block;
	}

	.login-register .none-2 {
		display: none;
	}

	.login-register .form-section .terms {
		margin-left: 3px;
	}

	.login-register .btn-section {
		border-radius: 50px;
		margin-bottom: 0;
		display: inline-block;
		top: 80px;
		position: absolute;
		right: 80px;
	}

	.login-register .info {
		max-width: 500px;
		margin: 0 auto;
		align-self: center !important;
	}

	.login-register .btn-section .link-btn {
		font-size: 14px;
		float: left;
		background: transparent;
		font-weight: 400;
		text-align: center;
		text-decoration: none;
		text-decoration: blink;
		width: 100px;
		padding: 6px 5px;
		margin-right: 5px;
		color: #000;
		border-radius: 3px;
		background: #fff;
		box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
	}

	.login-register .btn-section .active-bg {
		color: #fff;
		background: #3385d9;
	}

	.login-register .btn-section .link-btn:hover {
		color: #fff;
		background: #3385d9;
	}

	.login-register .form-section .checkbox {
		font-size: 14px;
	}

	.login-register .form-section .form-check {
		float: left;
		margin-bottom: 0;
	}

	.login-register .form-section .form-check a {
		color: #717171;
		float: right;
	}

	.login-register .form-section .form-check-input {
		position: absolute;
		margin-left: 0;
	}

	.login-register .form-section .form-check label::before {
		content: "";
		display: inline-block;
		position: absolute;
		width: 18px;
		height: 18px;
		top: 2px;
		margin-left: -25px;
		border: 1px solid #c5c3c3;
		border-radius: 3px;
		background-color: #fff;
	}

	.login-register .form-section .form-check-label {
		padding-left: 0;
		margin-bottom: 0;
		font-size: 16px;
		font-weight: 400;
		color: #b9b9b9;
	}

	.login-register .form-section .checkbox-theme input[type="checkbox"]:checked + label::before {
		background-color: #ffffff;
		border-color: #3385d9;
	}
	
	.login-register .checkbox label::after {
		display: inline-block;
		position: absolute;
		width: 16px;
		height: 16px;
		left: 0;
		top: 0;
		margin-left: -24px;
		padding-left: 3px;
		padding-top: 3px;
		font-size: 11px;
		color: #3385d9;
	}

	.login-register .form-section input[type=checkbox]:checked + label:before {
		font-weight: 300;
		color: #ffffff;
		line-height: 15px;
		font-size: 14px;
		content: "\2713";
	}

	.login-register .form-section input[type=checkbox], input[type=radio] {
		margin-top: 4px;
	}

	.login-register .form-section a.forgot-password {
		font-size: 16px;
		color: #3385d9;
		float: right;
	}

	.login-register .social-list a {
		text-align: center;
		display: inline-block;
		font-size: 18px;
		margin-right: 20px;
		color: #717171;
	}

	.login-register .social-list a:hover {
		color: #28a745;
	}

	
	.login-register sup {
		color: #c10000;
	}
	
	.login-register .chosen-container-multi .chosen-choices .search-field input[type="text"]{
		padding:17px 0px;
		font-size: 1rem;
		font-weight: 400;
	}
	
	.login-register .input-group-append{
		position: absolute;
		right: 0;
		height: 50px
	}
	
	.login-register .upload-wrap .file-upload{
		width: 150px;
		text-align: center;
	}
	.login-register #file-upload1-filename .text-danger, .login-register #file-upload-filename .text-danger{
		bottom: -20px!important;
	}
	.login-register input[type="text"], .login-register input[type="email"], .login-register input[type="url"], input[type="password"], .login-register input[type="search"], .login-register input[type="tel"], .login-register input[type="number"], select {
		outline: none;
		-webkit-box-shadow: none;
		box-shadow: none;
		/* border: 1px solid #a8a8a8; */
		border: 0;
		height: 50px;
		line-height: 32px;
		background-color: transparent;
		border-radius: 0px;
	}
	.login-register input {
		border: 1px solid #e4e5e7!important;
	}
	.login-register .form-group{
		position:relative;
	}
	.hide{display:none;}

	@media (max-width: 1200px) {
		.login-register .form-section {
			padding: 150px 60px 60px;
		}

		.login-register .logo {
			left: 60px;
			top: 60px
		}

		.login-register .btn-section {
			right: 60px;
			top: 60px
		}
	}

	@media (max-width: 992px) {
		.login-register .form-section {
			width: 100%;
		}

		.login-register .bg-img {
			min-height: 100%;
			border-radius: 5px;
		}

		.none-992 {
			display: none !important;
		}

		.login-register .login-box {
			max-width: 500px;
			margin: 0 auto;
			padding: 0;
		}
	}

	@media (max-width: 768px) {
		.login-register .form-section {
			padding: 30px;
		}

		.login-register .form-section {
			padding: 150px 30px 60px;
		}

		.login-register .logo {
			left: 30px;
		}

		.login-register .btn-section {
			right: 30px;
		}
	}
	.checkbox label::before{
		background-color: #ffffff;
		width: 16px;
		height: 18px;
		margin-left: -22px;
	}
	.login-register .checkbox label::after{
		margin-left: -23px;
		padding-left: 3px;
		padding-top: 1px;
	}
</style>
<?php
  if($settingRes)
  {
    if($settingRes->background_type == 'color')
    {
        ?>
          <body style="background-color: <?= $settingRes->background_color; ?>;">
        <?php
    }
    else
    {
        ?>
          <body style="background-image: url('<?= base_url() ?>uploads/loginPage/<?= $settingRes->background_image; ?>');">
        <?php
    }
  }
  else
  {
    ?>
     <body class="login_admin"<?php if(is_rtl()){ echo ' dir="rtl"'; } ?>>
    <?php
  }
?>
<div class="login-register">
    <div class="container">
        <div class="row login-box">
            <div class="col-lg-5 bg-color-15 pad-0 none-992 bg-img"></div>
            <div class="col-lg-7 pad-0 form-info">
                <div class="form-section align-self-center">
                    <div class="btn-section clearfix">
                        <a href="<?php echo site_url('authentication/login'); ?>" class="link-btn active btn-1 active-bg"><?php echo _l('Back to Login'); ?></a>
                    </div>
                    <div class="logo">
                        <a href="<?= base_url(); ?>">
                            <img src="<?= base_url() ?>uploads/loginPage/<?= $settingRes->logo_image; ?>" alt="logo">
                        </a>
                    </div>
                    <h1>Welcome!</h1>
                    <h3>Register as a Customer!</h3>
                    <div class="clearfix"></div>
                    <?php echo form_open('authentication/register', ['id'=>'register-form']); ?>
						<div class="row">
							<div class="form-group register-firstname-group col-md-6">
								<label class="control-label" for="firstname"><?php echo _l('clients_firstname'); ?></label>
								<input type="text" class="form-control" name="firstname" id="firstname" onKeyUp="removeerror('firstname')" value="<?php echo set_value('firstname'); ?>">
								<span id="error_firstname"><?php echo form_error('firstname'); ?></span>
							</div>
							<div class="form-group register-lastname-group col-md-6">
								<label class="control-label" for="lastname"><?php echo _l('clients_lastname'); ?></label>
								<input type="text" class="form-control" name="lastname" id="lastname" onKeyUp="removeerror('lastname')" value="<?php echo set_value('lastname'); ?>">
								<span id="error_lastname"><?php echo form_error('lastname'); ?></span>
							</div>
						</div>
						<div class="row">
							<div class="form-group register-email-group col-md-12">
								<label class="control-label" for="email"><?php echo _l('clients_email'); ?></label>
								<input type="email" class="form-control" name="email" id="email" onKeyUp="removeerror('email')" value="<?php echo set_value('email'); ?>">
								<span id="error_email"><?php echo form_error('email'); ?></span>
							</div>
							<div class="form-group register-contact-phone-group hide col-md-6">
								<label class="control-label" for="contact_phonenumber"><?php echo _l('clients_phone'); ?></label>
								<input type="text" class="form-control" name="contact_phonenumber" onKeyUp="removeerror('firstname')" id="contact_phonenumber" value="<?php echo set_value('contact_phonenumber'); ?>">
							</div>
							<div class="form-group register-website-group hide col-md-6">
								<label class="control-label" for="website"><?php echo _l('client_website'); ?></label>
								<input type="text" class="form-control" name="website" id="website" onKeyUp="removeerror('firstname')" value="<?php echo set_value('website'); ?>">
							</div>
							<div class="form-group register-position-group hide col-md-6">
								<label class="control-label" for="title"><?php echo _l('contact_position'); ?></label>
								<input type="text" class="form-control" name="title" id="title" onKeyUp="removeerror('firstname')" value="<?php echo set_value('title'); ?>">
							</div>
						</div>
						<div class="row">
							<div class="form-group register-password-group col-md-6">
								<label class="control-label" for="password"><?php echo _l('clients_register_password'); ?></label>
								<input type="password" class="form-control" name="password" onKeyUp="removeerror('password')" id="password">
								<span id="error_password"><?php echo form_error('password'); ?></span>
							</div>
							<div class="form-group register-password-repeat-group col-md-6">
								<label class="control-label" for="passwordr"><?php echo _l('Confirm Password'); ?></label>
								<input type="password" class="form-control" onKeyUp="removeerror('passwordr')" name="passwordr" id="passwordr">
								<span id="error_passwordr"><?php echo form_error('passwordr'); ?></span>
							</div>
							<div class="register-contact-custom-fields col-md-6">
								<?php echo render_custom_fields( 'contacts','',array('show_on_client_portal'=>1)); ?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 hide">
								<h4 class="bold register-company-info-heading"><?php echo _l('client_register_company_info'); ?></h4>
								<div class="form-group mtop15 register-company-group">
									<label class="control-label" for="company"><?php echo _l('clients_company'); ?></label>
									<input type="text" class="form-control" name="company" id="company" value="<?php echo set_value('company'); ?>">
									<?php echo form_error('company'); ?>
								</div>
								<?php if(get_option('company_requires_vat_number_field') == 1){ ?>
								<div class="form-group register-vat-group">
									<label class="control-label" for="vat"><?php echo _l('clients_vat'); ?></label>
									<input type="text" class="form-control" name="vat" id="vat" value="<?php echo set_value('vat'); ?>">
								</div>
								<?php } ?>
								<div class="form-group register-company-phone-group">
									<label class="control-label" for="phonenumber"><?php echo _l('clients_phone'); ?></label>
									<input type="text" class="form-control" name="phonenumber" id="phonenumber" value="<?php echo set_value('phonenumber'); ?>">
								</div>
								<div class="form-group register-country-group">
									<label class="control-label" for="lastname"><?php echo _l('clients_country'); ?></label>
									<select data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" data-live-search="true" name="country" class="form-control" id="country">
										<option value=""></option>
										<?php foreach(get_all_countries() as $country){ ?>
										<option value="<?php echo $country['country_id']; ?>"<?php if(get_option('customer_default_country') == $country['country_id']){echo ' selected';} ?> <?php echo set_select('country', $country['country_id']); ?>><?php echo $country['short_name']; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group register-city-group">
									<label class="control-label" for="city"><?php echo _l('clients_city'); ?></label>
									<input type="text" class="form-control" name="city" id="city" value="<?php echo set_value('city'); ?>">
								</div>
								<div class="form-group register-address-group">
									<label class="control-label" for="address"><?php echo _l('clients_address'); ?></label>
									<input type="text" class="form-control" name="address" id="address" value="<?php echo set_value('address'); ?>">
								</div>
								<div class="form-group register-zip-group">
									<label class="control-label" for="zip"><?php echo _l('clients_zip'); ?></label>
									<input type="text" class="form-control" name="zip" id="zip" value="<?php echo set_value('zip'); ?>">
								</div>
								<div class="form-group register-state-group">
									<label class="control-label" for="state"><?php echo _l('clients_state'); ?></label>
									<input type="text" class="form-control" name="state" id="state" value="<?php echo set_value('state'); ?>">
								</div>
								<div class="register-company-custom-fields">
									<?php echo render_custom_fields( 'customers','',array('show_on_client_portal'=>1)); ?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 register-terms-and-conditions-wrapper">
								<div class="">
									<div class="form-group">
										<div class="checkbox">
											<input type="checkbox" name="accept_terms_and_conditions" onClick="removeerror('accept_terms_and_conditions')" id="accept_terms_and_conditions" <?php echo set_checkbox('accept_terms_and_conditions', 'on'); ?>>
											<label for="accept_terms_and_conditions">
												<?php echo _l('gdpr_terms_agree', terms_url()); ?>
											</label>
										</div>
										<span id="error_accept_terms_and_conditions"><?php echo form_error('accept_terms_and_conditions'); ?></span>
									</div>
								</div>
								<div class="">
									<div class="form-group">
										<button type="submit" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>" class="btn-md btn-theme btn-block"><?php echo _l('clients_register_string'); ?></button>
									</div>
								</div>
								<?php if (is_gdpr() && get_option('gdpr_enable_terms_and_conditions') == 1) { ?>
								<div class="col-md-12 register-terms-and-conditions-wrapper">
									<div class="text-center">
										<div class="checkbox">
											<input type="checkbox" name="accept_terms_and_conditions" id="accept_terms_and_conditions" <?php echo set_checkbox('accept_terms_and_conditions', 'on'); ?>>
											<label for="accept_terms_and_conditions">
												<?php echo _l('gdpr_terms_agree', terms_url()); ?>
											</label>
										</div>
										<?php echo form_error('accept_terms_and_conditions'); ?>
									</div>
								</div>
								<?php } ?>
								<?php if(get_option('use_recaptcha_customers_area') == 1 && get_option('recaptcha_secret_key') != '' && get_option('recaptcha_site_key') != ''){ ?>
								<div class="col-md-12 register-recaptcha">
								   <div class="g-recaptcha" data-sitekey="<?php echo get_option('recaptcha_site_key'); ?>"></div>
								   <?php echo form_error('g-recaptcha-response'); ?>
							   </div>
							   <?php } ?>
							</div>
						</div>
					<?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function removeerror(id)
    {
        $('#error_'+id).html('');
    }
</script>
