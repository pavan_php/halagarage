<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="panel_s section-heading section-announcements">
    <div class="panel-body">
        <!--<h4 class="no-margin section-text"><?php echo _l($title); ?></h4>-->
        <div class="header-wrap p-10 bg-white">
            <div class="d-flex align-items-center">
                <div>
                    <h4 class="fw-700 mt-0">Messages</h4>
                </div>
                <div class="ml-auto align-items-center">
                    <div class="dl">
                        <form class="form-inline">
                            <div class="form-group">
                                <input class="search__input" type="text" placeholder="Search">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-theme">Reset</button>
                                <button class="btn btn-create"><i class="fa fa-plus" aria-hidden="true"></i> Compose</button>
                            </div>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel_s">
    <div class="panel-body">
        <div class="stats-wrap p-10 bg-white">
            <div class="msg-wrap">
                <div class="mail-item mailInbox">
                    <div class="mail-item-heading collapsed" data-toggle="collapse" role="navigation" data-target="#mailCollapseFive" aria-expanded="false">
                        <div class="mail-item-inner">
                            <div class="d-flex">
                                <div class="f-body">
                                    <div class="meta-mail-time">
                                        <p class="user-email" data-mailto="kingAndy@mail.com">Tanmay Sasvadkar</p>
                                    </div>
                                    <div class="meta-title-tag">
                                        <p class="mail-content-excerpt" data-maildescription="{&quot;ops&quot;:[{&quot;insert&quot;:&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.\n&quot;}]}"><span class="mail-title" data-mailtitle="Hosting Payment Reminder">Hosting Payment Reminder -</span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.</p>
                                        <p class="meta-time align-self-center">6:28 PM</p>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <button type="button" class="btn btn-confirm mr-3" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                        <button type="button" class="btn btn-reply mr-3" data-toggle="tooltip" title="Reply"><i class="fa fa-retweet" aria-hidden="true"></i></button>
                                        <button type="button" class="btn btn-appologise mr-10" data-toggle="tooltip" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mail-item mailInbox">
                    <div class="mail-item-heading collapsed" data-toggle="collapse" role="navigation" data-target="#mailCollapseFive" aria-expanded="false">
                        <div class="mail-item-inner">
                            <div class="d-flex">
                                <div class="f-body">
                                    <div class="meta-mail-time">
                                        <p class="user-email" data-mailto="kingAndy@mail.com">Canva</p>
                                    </div>
                                    <div class="meta-title-tag">
                                        <p class="mail-content-excerpt" data-maildescription="{&quot;ops&quot;:[{&quot;insert&quot;:&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.\n&quot;}]}"><span class="mail-title" data-mailtitle="Hosting Payment Reminder">Canva for desktop -</span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.</p>
                                        <p class="meta-time align-self-center">3:15 PM</p>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <button type="button" class="btn btn-confirm mr-3"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                        <button type="button" class="btn btn-reply mr-3"><i class="fa fa-retweet" aria-hidden="true"></i></button>
                                        <button type="button" class="btn btn-appologise mr-10"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mail-item mailInbox">
                    <div class="mail-item-heading collapsed" data-toggle="collapse" role="navigation" data-target="#mailCollapseFive" aria-expanded="false">
                        <div class="mail-item-inner">
                            <div class="d-flex">
                                <div class="f-body">
                                    <div class="meta-mail-time">
                                        <p class="user-email" data-mailto="kingAndy@mail.com">Basecamp</p>
                                    </div>
                                    <div class="meta-title-tag">
                                        <p class="mail-content-excerpt" data-maildescription="{&quot;ops&quot;:[{&quot;insert&quot;:&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.\n&quot;}]}"><span class="mail-title" data-mailtitle="Hosting Payment Reminder">FSD for Halagarage -</span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.</p>
                                        <p class="meta-time align-self-center">2:00 AM</p>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <button type="button" class="btn btn-confirm mr-3"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                        <button type="button" class="btn btn-reply mr-3"><i class="fa fa-retweet" aria-hidden="true"></i></button>
                                        <button type="button" class="btn btn-appologise mr-10"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mail-item mailInbox">
                    <div class="mail-item-heading collapsed" data-toggle="collapse" role="navigation" data-target="#mailCollapseFive" aria-expanded="false">
                        <div class="mail-item-inner">
                            <div class="d-flex">
                                <div class="f-body">
                                    <div class="meta-mail-time">
                                        <p class="user-email" data-mailto="kingAndy@mail.com">Report Garden Team</p>
                                    </div>
                                    <div class="meta-title-tag">
                                        <p class="mail-content-excerpt" data-maildescription="{&quot;ops&quot;:[{&quot;insert&quot;:&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.\n&quot;}]}"><span class="mail-title" data-mailtitle="Hosting Payment Reminder">How to make your agency indespencible -</span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.</p>
                                        <p class="meta-time align-self-center">11:45 PM</p>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <button type="button" class="btn btn-confirm mr-3"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                        <button type="button" class="btn btn-reply mr-3"><i class="fa fa-retweet" aria-hidden="true"></i></button>
                                        <button type="button" class="btn btn-appologise mr-10"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mail-item mailInbox">
                    <div class="mail-item-heading collapsed" data-toggle="collapse" role="navigation" data-target="#mailCollapseFive" aria-expanded="false">
                        <div class="mail-item-inner">
                            <div class="d-flex">
                                <div class="f-body">
                                    <div class="meta-mail-time">
                                        <p class="user-email" data-mailto="kingAndy@mail.com">Incoice Monthly</p>
                                    </div>
                                    <div class="meta-title-tag">
                                        <p class="mail-content-excerpt" data-maildescription="{&quot;ops&quot;:[{&quot;insert&quot;:&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.\n&quot;}]}"><span class="mail-title" data-mailtitle="Hosting Payment Reminder">How to manage your invoices -</span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.</p>
                                        <p class="meta-time align-self-center">1:25 PM</p>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <button type="button" class="btn btn-confirm mr-3"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                        <button type="button" class="btn btn-reply mr-3"><i class="fa fa-retweet" aria-hidden="true"></i></button>
                                        <button type="button" class="btn btn-appologise mr-10"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--<ul class="responsive-table">
                <li class="table-header">
                    <div class="col col-1">No.</div>
                    <div class="col col-2">Name</div>
                    <div class="col col-3">Email</div>
                    <div class="col col-4">Subject</div>
                    <div class="col col-5">Message</div>
                    <div class="col col-6">Attatchment</div>
                    <div class="col col-7">Created Date</div>
                    <div class="col col-8">Action</div>
                </li>
                <li class="table-row">
                    <div class="col col-1">1</div>
                    <div class="col col-2">Tanmay Sasvadkar</div>
                    <div class="col col-3">tanmay@gmail.com</div>
                    <div class="col col-4">Reply by user</div>
                    <div class="col col-5">
                        <div class="msg-text">this message is reply by user</div>
                    </div>
                    <div class="col col-6">----</div>
                    <div class="col col-7">02 July 2020</div>
                    <div class="col col-8">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-confirm mr-3">View</button>
                            <button type="button" class="btn btn-reply mr-3">Reply</button>
                            <button type="button" class="btn btn-appologise mr-10">Delete</button>
                        </div>
                        <a href="javascript:void(0);" data-toggle="tooltip" title="Detail" class="text-muted">
                            <i class="fa fa-ellipsis-v mt-8" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
                <li class="table-row">
                    <div class="col col-1">2</div>
                    <div class="col col-2">Pavan Prajapati</div>
                    <div class="col col-3">pavan@gmail.com</div>
                    <div class="col col-4">Reply by user</div>
                    <div class="col col-5">
                        <div class="msg-text">this message is reply by user</div>
                    </div>
                    <div class="col col-6">----</div>
                    <div class="col col-7">02 July 2020</div>
                    <div class="col col-8">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-confirm mr-3">View</button>
                            <button type="button" class="btn btn-reply mr-3">Reply</button>
                            <button type="button" class="btn btn-appologise mr-10">Delete</button>
                        </div>
                        <a href="javascript:void(0);" data-toggle="tooltip" title="Detail" class="text-muted">
                            <i class="fa fa-ellipsis-v mt-8" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
                <li class="table-row">
                    <div class="col col-1">3</div>
                    <div class="col col-2">Basant Yaduvanshi</div>
                    <div class="col col-3">basant@gmail.com</div>
                    <div class="col col-4">Reply by user</div>
                    <div class="col col-5">
                        <div class="msg-text">this message is reply by user</div>
                    </div>
                    <div class="col col-6">----</div>
                    <div class="col col-7">02 July 2020</div>
                    <div class="col col-8">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-confirm mr-3">View</button>
                            <button type="button" class="btn btn-reply mr-3">Reply</button>
                            <button type="button" class="btn btn-appologise mr-10">Delete</button>
                        </div>
                        <a href="javascript:void(0);" data-toggle="tooltip" title="Detail" class="text-muted">
                            <i class="fa fa-ellipsis-v mt-8" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
                <li class="table-row">
                    <div class="col col-1">4</div>
                    <div class="col col-2">Krishnapal Yadav</div>
                    <div class="col col-3">krishna@gmail.com</div>
                    <div class="col col-4">Reply by user</div>
                    <div class="col col-5">
                        <div class="msg-text">this message is reply by user</div>
                    </div>
                    <div class="col col-6">----</div>
                    <div class="col col-7">02 July 2020</div>
                    <div class="col col-8">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-confirm mr-3">View</button>
                            <button type="button" class="btn btn-reply mr-3">Reply</button>
                            <button type="button" class="btn btn-appologise mr-10">Delete</button>
                        </div>
                        <a href="javascript:void(0);" data-toggle="tooltip" title="Detail" class="text-muted">
                            <i class="fa fa-ellipsis-v mt-8" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
                <li class="table-row">
                    <div class="col col-1">5</div>
                    <div class="col col-2">Om Verma</div>
                    <div class="col col-3">om@gmail.com</div>
                    <div class="col col-4">Reply by user</div>
                    <div class="col col-5">
                        <div class="msg-text">this message is reply by user</div>
                    </div>
                    <div class="col col-6">----</div>
                    <div class="col col-7">02 July 2020</div>
                    <div class="col col-8">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-confirm mr-3">View</button>
                            <button type="button" class="btn btn-reply mr-3">Reply</button>
                            <button type="button" class="btn btn-appologise mr-10">Delete</button>
                        </div>
                        <a href="javascript:void(0);" data-toggle="tooltip" title="Detail" class="text-muted">
                            <i class="fa fa-ellipsis-v mt-8" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
            </ul>-->
        </div>
    </div>
</div>
