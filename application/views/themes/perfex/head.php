<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="<?php echo $locale; ?>">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
	<title><?php if (isset($title)){ echo $title; } ?></title>
	<?php echo compile_theme_css(); ?>
	<link rel="stylesheet" type="text/css" id="jquery-comments-css" href="<?php echo base_url('assets/css/custom.css'); ?>">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/images-grid.css'); ?>">
	
	<script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js'); ?>"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
	<!--<link href="https://cdn.datatables.net/fixedheader/3.1.7/css/fixedHeader.dataTables.min.css" />
	<script src="https://cdn.datatables.net/fixedheader/3.1.7/js/dataTables.fixedHeader.min.js"></script>-->
	<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.2.7/js/responsive.bootstrap.min.js"></script>
	<script src="<?php echo base_url('assets/js/images-grid.js'); ?>"></script>
	<?php app_customers_head(); ?>
	<style>
	    .navigationbadge {
			background: #c10000!important;
			padding: 5px 9px!important;
			border-radius: 5px!important;
			position: absolute;
			top: 22px;
			right: 0px;
			color:#ffffff!important;
		}
	</style>
	<script>
	    var lid = <?= get_client_user_id() ?>;
	    if(lid)
	    {
	        function quoteNotification()
	        {
	            var token_name = csrfData['token_name'];
                var token_hash = csrfData['hash'];
                var str = {"id":lid,token_name:token_hash};
        	    $.ajax({
        	        url: '<?= site_url()?>notifications/notify',
        	        type: 'POST',
        	        data: str,
        	        //dataType: 'json',
        	        cache: false,
        	        success: function(resp){
        	            if(resp)
        	            {
        	                $('.newquotes').html(resp);
        	                return true;
        	            }
        	            else
        	            {
        	                
        	                return false;
        	            }
        	        }
        	    });
	        }
	        
	        setInterval(function(){ quoteNotification(); }, 3000);
	        quoteNotification();
	    }
	</script>
</head>
<body class="customers<?php if(is_mobile()){echo ' mobile';}?><?php if(isset($bodyclass)){echo ' ' . $bodyclass; } ?>" <?php if($isRTL == 'true'){ echo 'dir="rtl"';} ?>>
	<?php hooks()->do_action('customers_after_body_start'); ?>
