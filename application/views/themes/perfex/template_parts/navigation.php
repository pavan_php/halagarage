<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php
  if($settingRes)
  {
    if($settingRes->background_type == 'color')
    {
        ?>
          <body style="background-color: <?= $settingRes->background_color; ?>;">
        <?php
    }
    else
    {
        ?>
          <body style="background-image: url('<?= base_url() ?>uploads/loginPage/<?= $settingRes->background_image; ?>');">
        <?php
    }
  }
  else
  {
    ?>
     <body class="login_admin"<?php if(is_rtl()){ echo ' dir="rtl"'; } ?>>
    <?php
  }
?>
<nav class="navbar navbar-default header <?= ($this->uri->segment(1) == 'authentication' || $this->uri->segment(1) == 'login')?"hide":""; ?>">
   <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#theme-navbar-collapse" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
         </button>
         <?php get_company_logo('','navbar-brand logo'); ?>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="theme-navbar-collapse">
         <ul class="nav navbar-nav navbar-right">
            <?php hooks()->do_action('customers_navigation_start'); ?>
            <?php /* foreach($menu as $item_id => $item) { ?>
               <li class="customers-nav-item-<?php echo $item_id; ?>"
                  <?php echo _attributes_to_string(isset($item['li_attributes']) ? $item['li_attributes'] : []); ?>>
                  <a href="<?php echo $item['href']; ?>"
                     <?php echo _attributes_to_string(isset($item['href_attributes']) ? $item['href_attributes'] : []); ?>>
                     <?php
                     if(!empty($item['icon'])){
                        echo '<i class="'.$item['icon'].'"></i> ';
                     }
                     echo $item['name'];
                     ?>
                  </a>
               </li>
            <?php } */?>
            <?php
                if (is_client_logged_in() && $this->uri->segment(2) != 'garageProfile')
                {
                    $paidstatus = $this->db->order_by('payment_id', 'desc')->limit(1)->get_where(db_prefix().'_payments', array('garage_id' => get_client_user_id(), 'expiry_date >' => time(), 'item_number' => 1))->num_rows();
                    $featuredStatus = $this->db->order_by('payment_id', 'desc')->limit(1)->get_where(db_prefix().'_payments', array('garage_id' => get_client_user_id(), 'expiry_date >' => time(), 'item_number' => 2))->num_rows();
                    ?>
                        <li class="customers-nav-item-dashboard">
                            <a href="<?php echo site_url(); ?>">
                               <?php echo _l('Dashboard'); ?>
                            </a>
                        </li>
                        <li class="customers-nav-item-dashboard">
                            <a href="<?php echo site_url('garages/messages'); ?>">
                               <?php echo _l('Messages'); ?>
                            </a>
                        </li>
                        <li class="customers-nav-item-dashboard">
                            <a href="<?php echo site_url('myQuotes'); ?>">
                                <?php echo _l('My Quotes'); ?>
                                <span class="badge navigationbadge newquotes">0</span>
                            </a>
                        </li>
                        <li class="customers-nav-item-dashboard">
                            <a href="<?php echo site_url('myOrders'); ?>">
                               <?php echo _l('My Orders'); ?>
                            </a>
                        </li>
                        <!--
                        <li class="dropdown customers-nav-item-order">
                    	    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color: #606060;">
                    	        <?php echo _l('My Orders'); ?>
                    			<span class="caret"></span>
                    		</a>
                    		<ul class="dropdown-menu animated fadeIn">
                    			<li class="customers-nav-item-myorder">
                    				<a href="<?php echo site_url('myOrders'); ?>">
                                       <?php echo _l('Quote Orders'); ?>
                                    </a>
                    			</li>
                    			<li class="customers-nav-item-product">
                    				<a href="<?php echo site_url('productOrders'); ?>">
                                       <?php echo _l('Product Orders'); ?>
                                    </a>
                    			</li>
                    		</ul>
                    	</li>
                        -->
                        <li class="customers-nav-item-dashboard">
                            <a href="<?php echo site_url('myProducts'); ?>">
                               <?php echo _l('My Products'); ?>
                            </a>
                        </li>
                        <li class="customers-nav-item-edit-profile">
                            <a href="<?php echo site_url('garages/profile'); ?>">
                               <?php echo _l('clients_nav_profile'); ?>
                            </a>
                        </li>
                        <?php
                            if($featuredStatus == 0)
                            {
                                ?>
                                    <li class="customers-nav-item-upgrad">
                                        <a href="javascript:void(0);">
                                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#featuredModal">
                                                Featured Garage  
                                            </button>            
                                        </a>
                                    </li>       
                                <?php
                            }
                            if($paidstatus == 0)
                            {
                                ?>
                                    <li class="customers-nav-item-upgrad">
                                        <a href="javascript:void(0);">
                                            <!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#expiryModal">-->
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#subscriptionModal">
                                                Upgrade To Pro    
                                            </button>            
                                        </a>
                                    </li>
                                <?php
                            }
                        ?>
                    <?php
                }
                else
                {
					if($this->uri->segment(2) != 'garageProfile')
					{
						?>
							<li class="customers-nav-item-login">
								<a href="<?= base_url('login'); ?>">
									Login                  
								</a>
						   </li>
						<?php
					}
                }
            ?>
            <?php hooks()->do_action('customers_navigation_end'); ?>
            <?php if(is_client_logged_in() && $this->uri->segment(2) != 'garageProfile') { ?>
               <li class="dropdown customers-nav-item-profile">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                     <img src="<?php echo contact_profile_image_url($contact->id,'thumb'); ?>" data-toggle="tooltip" data-title="<?php echo html_escape($contact->firstname . ' ' .$contact->lastname); ?>" data-placement="bottom" class="client-profile-image-small mright5">
                     <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu animated fadeIn">

                     <?php /* if($contact->is_primary == 1){ ?>
                        <li class="customers-nav-item-company-info">
                           <a href="<?php echo site_url('garages/company'); ?>">
                              <?php echo _l('client_company_info'); ?>
                           </a>
                        </li>
                     <?php } ?>
                     <?php if(can_logged_in_contact_update_credit_card()){ ?>
                        <li class="customers-nav-item-stripe-card">
                           <a href="<?php echo site_url('garages/credit_card'); ?>">
                              <?php echo _l('credit_card'); ?>
                           </a>
                        </li>
                     <?php } ?>
                     <?php if (is_gdpr() && get_option('show_gdpr_in_customers_menu') == '1') { ?>
                        <li class="customers-nav-item-announcements">
                           <a href="<?php echo site_url('garages/gdpr'); ?>">
                              <?php echo _l('gdpr_short'); ?>
                           </a>
                        </li>
                     <?php } */?>
                     <li class="customers-nav-item-announcements">
                        <a href="<?php echo site_url('garages/announcements'); ?>">
                           <?php echo _l('announcements'); ?>
                           <?php /* if($total_undismissed_announcements != 0){ ?>
                              <span class="badge"><?php echo $total_undismissed_announcements; ?></span>
                           <?php } */ ?>
                        </a>
                     </li>
                     <li class="customers-nav-item-support">
                            <a href="<?php echo site_url('garages/tickets'); ?>">
                             <?= _l('Help'); ?>                  
                            </a>
                     </li>
                     <li class="customers-nav-item-invoices">
                        <a href="<?php echo site_url('garages/billing'); ?>">
                            <?= _l('Billing'); ?>                  
                        </a>
                     </li>
                     <?php /* if(can_logged_in_contact_change_language()) {
                        ?>
                        <li class="dropdown-submenu pull-left customers-nav-item-languages">
                           <a href="#" tabindex="-1">
                              <?php echo _l('language'); ?>
                           </a>
                           <ul class="dropdown-menu dropdown-menu-left">
                              <li class="<?php if($client->default_language == ""){echo 'active';} ?>">
                                 <a href="<?php echo site_url('garages/change_language'); ?>">
                                    <?php echo _l('system_default_string'); ?>
                                 </a>
                              </li>
                              <?php foreach($this->app->get_available_languages() as $user_lang) { ?>
                                 <li <?php if($client->default_language == $user_lang){echo 'class="active"';} ?>>
                                    <a href="<?php echo site_url('garages/change_language/'.$user_lang); ?>">
                                       <?php echo ucfirst($user_lang); ?>
                                    </a>
                                 </li>
                              <?php } ?>
                           </ul>
                        </li>
                     <?php } */?>
                     <li class="customers-nav-item-logout">
                        <a href="<?php echo site_url('authentication/logout'); ?>">
                           <?php echo _l('clients_nav_logout'); ?>
                        </a>
                     </li>
                  </ul>
               </li>
            <?php } ?>
            <?php hooks()->do_action('customers_navigation_after_profile'); ?>
         </ul>
      </div>
      <!-- /.navbar-collapse -->
   </div>
   <!-- /.container-fluid -->
</nav>
<!-- Modal -->
<div class="modal fade" id="subscriptionModal" tabindex="-1" role="dialog" aria-labelledby="subscriptionModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="subscriptionModalTitle"><?= _l('Subscrioption Details');?></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position:absolute;right: 20px;top: 20px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php
                    $plandetails_ = $this->db->get_where(db_prefix().'subscription', array('id' => 1))->row();
                ?>
                <table class="responsive-table dataTable no-footer">
                    <thead>
                        <tr style="box-shadow:none;">
                            <th style="font-size: 18px;font-weight: 600;padding: 10px;">Title</th>
                            <td style="font-size: 18px;font-weight: 400;padding: 10px;"><?= $plandetails_->title; ?></td>
                        </tr>
                        <tr style="box-shadow:none;">
                            <th style="font-size: 18px;font-weight: 600;padding: 10px;">Price</th>
                            <td style="font-size: 18px;font-weight: 400;padding: 10px;"><?= $plandetails_->price.' '.CURRENCY_NAME; ?></td>
                        </tr>
                        <tr style="box-shadow:none;">
                            <th style="font-size: 18px;font-weight: 600;padding: 10px;">Days</th>
                            <td style="font-size: 18px;font-weight: 400;padding: 10px;"><?= $plandetails_->day_limit; ?></td>
                        </tr>
                        <tr style="box-shadow:none;">
                            <th style="font-size: 18px;font-weight: 600;padding: 10px;">Description</th>
                            <td style="font-size: 18px;font-weight: 400;padding: 10px;"><?= $plandetails_->description; ?></td>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
               <form action="<?php echo PAYPAL_URL; ?>" method="post">
                    <input type="hidden" name="business" value="<?php echo PAYPAL_ID; ?>">
                    <input type="hidden" name="cmd" value="_xclick">                                                                                  
                    <input type="hidden" name="item_name" value="<?php echo $plandetails_->title; ?>">
                    <input type="hidden" name="item_number" value="<?php echo $plandetails_->id; ?>">
                    <input type="hidden" name="amount" value="<?php echo ($plandetails_->price/3.26); ?>">
                    <input type="hidden" name="currency_code" value="<?php echo PAYPAL_CURRENCY; ?>">
                    <input type="hidden" name="return" value="<?php echo PAYPAL_RETURN_URL; ?>">
                    <input type="hidden" name="cancel_return" value="<?php echo PAYPAL_CANCEL_URL; ?>">
                    <input type="hidden" name="notify_url" value="<?php echo PAYPAL_NOTIFY_URL; ?>">                               
                    <input type="submit" name="submit" border="0" class="btn btn-info btn-lg" value="Subscribe Now">
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="featuredModal" tabindex="-1" role="dialog" aria-labelledby="featuredModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="featuredModalTitle"><?= _l('Featured Subscrioption Details');?></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position:absolute;right: 20px;top: 20px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php
                    $fearturedetails_ = $this->db->get_where(db_prefix().'subscription', array('id' => 2))->row();
                ?>
                <table class="responsive-table dataTable no-footer">
                    <thead>
                        <tr style="box-shadow:none;">
                            <th style="font-size: 18px;font-weight: 600;padding: 10px;">Title</th>
                            <td style="font-size: 18px;font-weight: 400;padding: 10px;"><?= $fearturedetails_->title; ?></td>
                        </tr>
                        <tr style="box-shadow:none;">
                            <th style="font-size: 18px;font-weight: 600;padding: 10px;">Price</th>
                            <td style="font-size: 18px;font-weight: 400;padding: 10px;"><?= $fearturedetails_->price.' '.CURRENCY_NAME; ?></td>
                        </tr>
                        <tr style="box-shadow:none;">
                            <th style="font-size: 18px;font-weight: 600;padding: 10px;">Days</th>
                            <td style="font-size: 18px;font-weight: 400;padding: 10px;"><?= $fearturedetails_->day_limit; ?></td>
                        </tr>
                        <tr style="box-shadow:none;">
                            <th style="font-size: 18px;font-weight: 600;padding: 10px;">Description</th>
                            <td style="font-size: 18px;font-weight: 400;padding: 10px;"><?= $fearturedetails_->description; ?></td>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
               <form action="<?php echo PAYPAL_URL; ?>" method="post">
                    <input type="hidden" name="business" value="<?php echo PAYPAL_ID; ?>">
                    <input type="hidden" name="cmd" value="_xclick">                                                                                  
                    <input type="hidden" name="item_name" value="<?php echo $fearturedetails_->title; ?>">
                    <input type="hidden" name="item_number" value="<?php echo $fearturedetails_->id; ?>">
                    <input type="hidden" name="amount" value="<?php echo ($fearturedetails_->price/3.26); ?>">
                    <input type="hidden" name="currency_code" value="<?php echo PAYPAL_CURRENCY; ?>">
                    <input type="hidden" name="return" value="<?php echo PAYPAL_RETURN_URL; ?>">
                    <input type="hidden" name="cancel_return" value="<?php echo PAYPAL_CANCEL_URL; ?>">
                    <input type="hidden" name="notify_url" value="<?php echo PAYPAL_NOTIFY_URL; ?>">                               
                    <input type="submit" name="submit" border="0" class="btn btn-info btn-lg" value="Subscribe Now">
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="expiryModal" tabindex="-1" role="dialog" aria-labelledby="expiryModalTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header text-center">
            <h3 class="modal-title" id="expiryModalTitle">Plan Expiry</h3>
         </div>
         <div class="modal-body text-center">
            <div class="box-wrap">
               <i class="fa fa-exclamation-circle" aria-hidden="true" style="font-size:100px;color:#c10000;"></i>
               <br>
               <h1>Trial Expired</h1>
               <p style="font-size: 20px;">Sorry, your trial period for this application has now expired, please review your plan.</p>
               <br>
               <button class="btn btn-info btn-lg" data-toggle="modal" data-target="#subscriptionModal" data-dismiss="modal">Upgrade</button>
            </div>
         </div>
      </div>
   </div>
</div>


<div class="modal fade in" id="successModal" tabindex="-1" role="dialog" aria-labelledby="successModalTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header text-center">
            <h3 class="modal-title" id="expiryModalTitle">Plan Upgrade Successful</h3>
         </div>
         <div class="modal-body text-center">
            <div class="box-wrap">
               <img src="<?= base_url() ?>assets/images/success.svg" class="img-responsive" style="width: 15%;margin: 0 auto;">
               <br>
               <h1 style="color:#1fa67a;">Payment Successful</h1>
               <p style="font-size: 20px;">Congratulations, your plan has been successfully upgraded.</p>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade in" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="errorModalTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header text-center">
            <h3 class="modal-title" id="expiryModalTitle">Plan Upgrade Failure</h3>
         </div>
         <div class="modal-body text-center">
            <div class="box-wrap">
               <img src="<?= base_url() ?>assets/images/error.svg" class="img-responsive" style="width: 15%;margin: 0 auto;">
               <br>
               <h1 style="color:#c10000;">Payment Failed</h1>
               <p style="font-size: 20px;">Sorry, your plan upgradation has been failed.</p>
            </div>
         </div>
      </div>
   </div>
</div>