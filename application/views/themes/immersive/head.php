<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="<?php echo $locale; ?>">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
	<title><?php if (isset($title)){ echo $title; } ?></title>
	<?php echo compile_theme_css(); ?>
	<!--<script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js'); ?>"></script>-->
	<link rel=icon href=favicon.ico sizes="20x20" type="image/png">
    <!-- flaticon -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/themes/immersive/css/flaticon.css">
    <!-- Fonts Awesome Icons -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/themes/immersive/css/font-awesome.min.css">
    <!--Themefy Icons-->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/themes/immersive/css/themify-icons.css">
    <!-- bootstrap -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/themes/immersive/css/bootstrap.min.css">
    <!-- animate -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/themes/immersive/css/animate.css">
    <!--Video Popup-->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/themes/immersive/css/rpopup.min.css">
    <!--Slick Carousel-->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/themes/immersive/css/slick.css">
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/themes/immersive/css/style.css">
    <!-- responsive Stylesheet -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/themes/immersive/css/responsive.css">
	<?php app_customers_head(); ?>
</head>
<body class="customers<?php if(is_mobile()){echo ' mobile';}?><?php if(isset($bodyclass)){echo ' ' . $bodyclass; } ?>" <?php if($isRTL == 'true'){ echo 'dir="rtl"';} ?>>
	<?php hooks()->do_action('customers_after_body_start'); ?>
