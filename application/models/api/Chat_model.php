<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

class Chat_model extends CI_Model {
	function __construct() {
		parent::__construct ();
		$this->load->database();
	}
	public function insertAll($table, $data) {
		$this->db->insert ( $table, $data );
	 
		$insert_id = $this->db->insert_id ();
		if ($insert_id > 0) {
			return $insert_id;
		} else {
			return false;
		}
	}

	public function updateArrayWhereResult($table, $data, $where) {
		$this->db->where ( $where );
		$this->db->update ( $table, $data );
		$afftectedRows = $this->db->affected_rows ();
		return $afftectedRows;
	}

	public function getUser_model($val='',$data)
	{
	    $role = userrole($data['user_from']);
		$id = '';
	    $this->db->select('um.*');
	  	$this->db->from('tblcontacts as um');
	  	$this->db->join('tblclients b', 'b.userid = um.userid', 'inner');
	  	if($role == 1)
	  	{
	  	    $this->db->where('b.role !=', 1);
	  	}
	  	else
	  	{
	  	    $this->db->where('b.role != ', 2);
	  	}
			//	->where(["is_verified"=>"Y","is_deleted"=>"N", "is_active"=>"Y"]);
			if (!empty($val)) 
			{
		  		$this->db->where('CONCAT(um.firstname," ", um.lastname) LIKE "'.$val.'%" or CONCAT(um.firstname," ", um.lastname) LIKE "% '.$val.'%" ');
			}
		  if($id!=''){
			$this->db->where_in('um.userid',$id);
		  }
	 
		return $this->db->get()->result();
	}

	public function getBlockUser_model($val='',$data)
	{
		$id = '';
	    $this->db->select('*')
	  			->from('tbluser_block as ub')
	  			->join('tblcontacts as um','ub.blocked_by= um.userid','RIGHT');
	  			
			//	->where(["is_verified"=>"Y","is_deleted"=>"N", "is_active"=>"Y"]);
			if (!empty($val)) 
			{
		  		$this->db->where('CONCAT(um.firstname," ", um.lastname) LIKE "'.$val.'%" or CONCAT(um.firstname," ", um.lastname) LIKE "% '.$val.'%" ');
			}
		  if($id!=''){
			$this->db->where_in('um.blocked_by',$id);
		  }
		
		return $this->db->get()->result();
	}

	public function getUserLastMsg_model($user_from,$user_to)
	{
	   
	    
	    if($user_to==''){
	        $user_to = $user_from;
	        die;
	    }

    	$res['data'] = $this->db->select('id, IF(is_delete=1, "This message was deleted ", CONCAT(msg, IF(LENGTH(msg)>25, "...", ""))) AS msg, msg_status, create_timestamp as date')
						->where('((`chat_from`= '.$user_from.' AND `chat_with` = '.$user_to.') OR (`chat_from`= '.$user_to.' AND `chat_with` = '.$user_from.')) AND ((`chat_from`= '.$user_from.' AND `delete_userfrom`= 0 ) OR (`chat_with` = '.$user_from.' AND `delete_userto` = 0))')
						->order_by('id','DESC')	
						->limit(1)
						->get('tbl_chat')
						->result_array();
		$res['count'] = $this->db->select('count(msg_status) as no_msg')
						->where('(`chat_from`= '.$user_to.' AND `chat_with` = '.$user_from.') AND msg_status = 1')
						->limit(1)
						->get('tbl_chat')
						->result_array();
		return $res;
		/*
		$res['data'] = $this->db->select('id, IF(is_delete=1, "This message was deleted ", CONCAT(SUBSTRING(msg, 1, 25), IF(LENGTH(msg)>25, "...", ""))) AS msg, msg_status, create_timestamp as date')
						->where('((`chat_from`= '.$user_from.' AND `chat_with` = '.$user_to.') OR (`chat_from`= '.$user_to.' AND `chat_with` = '.$user_from.')) AND ((`chat_from`= '.$user_from.' AND `delete_userfrom`= 0 ) OR (`chat_with` = '.$user_from.' AND `delete_userto` = 0))')
						->order_by('id','DESC')	
						->limit(1)
						->get('tbl_chat')
						->result_array();
		$res['count'] = $this->db->select('count(msg_status) as no_msg')
						->where('(`chat_from`= '.$user_to.' AND `chat_with` = '.$user_from.') AND msg_status = 1')
						->limit(1)
						->get('tbl_chat')
						->result_array();
		return $res;*/
	}

	public function getChat($user_from, $user_to)
	{
		$res['chat'] = $this->db->SELECT('tc.*, DATE_FORMAT(tc.create_timestamp, "%e %b %y") date, DATE_FORMAT(tc.create_timestamp, "%h:%i %p") time, ud.*')
						->from('tbl_chat as tc')
						->join('tblcontacts as ud','ud.userid = tc.chat_from','left')
						->where('((`chat_from`= '.$user_from.' AND `chat_with` = '.$user_to.') OR (`chat_from`= '.$user_to.' AND `chat_with` = '.$user_from.')) AND ((`chat_from`= '.$user_from.' AND `delete_userfrom`= 0 ) OR (`chat_with` = '.$user_from.' AND `delete_userto` = 0))')
						->get()
						->result_array();
		$res['last_msg'] = $this->db->SELECT("IF(msg='' , 'blank','msg') as last_msg, DATE_FORMAT(create_timestamp, '%e %b %y') date")
						->where('(`delete_userfrom`= 0 AND `delete_userto` = 0) AND(`chat_from`= '.$user_to.' AND `chat_with` = '.$user_from.')' )
						->order_by('id','DESC')
						->limit(1)
						->get('tbl_chat')
						->result();
 
		$res['last_date'] = $this->db->SELECT('DATE_FORMAT(create_timestamp, "%e %b %y") date')
						->where('((`chat_from`= '.$user_from.' AND `chat_with` = '.$user_to.') OR (`chat_from`= '.$user_to.' AND `chat_with` = '.$user_from.')) AND ((`chat_from`= '.$user_from.' AND `delete_userfrom`= 0 ) OR (`chat_with` = '.$user_from.' AND `delete_userto` = 0 ))')
						->order_by('id','DESC')
						->limit(1)
						->get('tbl_chat')
						->result();

		$updateChat = $this->updateArrayWhereResult('tbl_chat', ['msg_status'=>0], '(`chat_from`= '.$user_to.' AND `chat_with` = '.$user_from.')');

		return $res;
	}

	public function updateMsg($user_from, $user_to)
	{

		$res = $this->db->select('*, DATE_FORMAT(create_timestamp, "%e %b %y") date, DATE_FORMAT(create_timestamp, "%h:%i %p") time')
						->where('(`chat_from`= '.$user_to.' AND `chat_with` = '.$user_from.')')
						->order_by('id','DESC')
						->limit(1)
						->get('tbl_chat')
						->result();
		$updateChat = $this->updateArrayWhereResult('tbl_chat', ['msg_status'=>0], '(`chat_from`= '.$user_to.' AND `chat_with` = '.$user_from.')');
		return $res;
	}

	public function UpdateChatStatus($user_from, $user_to)
	{

		$checkQuery = $this->db->select('count(chat_from) as count')
						->where('(`chat_from`= '.$user_from.' AND `chat_with` = '.$user_to.')')
						->get('tblchat_status')
						->result_array();

		if ($checkQuery[0]['count']>0) {
			$checkQuery = TRUE;
		}else{
			$query = $this->insertAll('tblchat_status', ['chat_from'=>$user_from, 'chat_with'=>$user_to]);
			$checkQuery = TRUE;	
		}

		return $checkQuery;
	}

	public function deleteChatStatus($user_from, $user_to)
	{

		$this->db->where ( '(`chat_from` = "'.$user_from.'" AND `chat_with`= "'.$user_to.'")' );
		$this->db->delete ( 'tblchat_status' );
		return TRUE;
	}

	public function getChatStatus($user_from, $user_to)
	{
return true;
/*
		$checkQuery = $this->db->select('count(chat_from) as count')
						->where('(`chat_from`= '.$user_to.' AND `chat_with` = '.$user_from.')')
						->get('tblchat_status')
						->result_array(); 
	 	$checkQuery = ($checkQuery[0]['count']>0)?TRUE:FALSE;
		
		return $checkQuery;
		*/
	}

	public function lastSendMsg($id)
	{
		$this->db->select('DATE_FORMAT(create_timestamp, "%e %b %y") date')
				 ->where('id',$id);
		return $this->db->get('tbl_chat')->result_array();
	}

	public function deleteChatHistory($user_from, $user_to)
	{
		
		$delete_userfrom = $this->updateArrayWhereResult('tbl_chat', ['delete_userfrom'=>1], '(`chat_from`= '.$user_from.' AND `chat_with` = '.$user_to.')');
		$delete_userto = $this->updateArrayWhereResult('tbl_chat', ['delete_userto'=>1], '(`chat_from`= '.$user_to.' AND `chat_with` = '.$user_from.')');
		return TRUE;
	}

	public function notifyMe($user_from)
	{
		
		$res = $this->db->select('CONCAT(ud.firstname," ", ud.lastname) as username, tc.msg')			
						->from('tbl_chat as tc')
						->join('tblcontacts as ud','ud.userid=tc.chat_from','left')
						->where(['chat_with' =>$user_from,'is_nitified'=>1])
						->order_by('tc.id','DESC')
						->limit(1)
						->get()
						->result();
			$this->updateArrayWhereResult('tbl_chat', ['is_nitified'=>0], ['chat_with'=>$user_from]);
		return $res;

	}

	public function archiveNow_modal($user_from, $user_to, $count='')
	{	
		if (empty($count)) {
			$res = $this->updateArrayWhereResult('tbl_chat', ['is_archive'=>1], '(`chat_from`= '.$user_from.' AND `chat_with` = '.$user_to.')');
			return $res;
		}else{
			$res = $this->db->select('count(is_archive) as archive')
						->where('(`chat_from`= '.$user_from.' AND `chat_with` = '.$user_to.') AND is_archive = 1')
						->limit(1)
						->get('tbl_chat')
						->result_array();
			return $res;
		}	
	}

	public function checkblock_modal($user_from, $user_to)
	{	
		$res = $this->db->select('is_blocked')
						->where('(`chat_from`= '.$user_from.' AND `chat_with` = '.$user_to.')')
						->limit(1)
						->get('tbl_chat')
						->row();
			return $res;
			
	}

	public function checkArchive_model($user_from, $user_to)
	{	
		$res = $this->updateArrayWhereResult('tbl_chat', ['is_archive'=>0], '(`chat_from`= '.$user_from.' AND `chat_with` = '.$user_to.')');
		return $res;
	}

	public function getColumn($table, $column, $where="")
    {
        $this->db->select( $column);
        $this->db->from($table);
       if (!empty($where)) {
       	$this->db->where($where);
       }
        return $this->db->get()->result_array();
    }

    public function getAllChatResult($first_date, $second_date, $user_from, $user_to) 
    {
		 $result = $this->db->select('CONCAT(udf.firstname," ", udf.lastname) as chat_from, CONCAT(uds.firstname," ", uds.lastname) as chat_with, tc.id as msg_id, tc.msg, IF(tc.is_type=0,"Text",IF(tc.is_type=1,"Image","File")) as type , IF(tc.msg_status=0,"Read","Unread") as msg_status, IF(tc.is_update=1,"Updated","-") as is_update, IF(tc.is_delete=1,"Deleted","-") as is_delete,  IF(tc.delete_userfrom=1,"Yes","-") as delete_userfrom')
		 					->from('tbl_chat as tc')
		 					->join('tblcontacts as udf','tc.chat_from = udf.userid','left')
		 					->join('tblcontacts as uds','tc.chat_with = uds.userid','left')
							->where('((tc.chat_from = '.$user_from.' AND tc.chat_with = '.$user_to.') OR (tc.chat_from = '.$user_to.' AND tc.chat_with = '.$user_from.')) AND (tc.create_timestamp <= "'.$first_date.'" AND tc.create_timestamp >= "'.$second_date.'") ')
							->get()
							->result_array();	
		 return $result;
	}

 
}
?>