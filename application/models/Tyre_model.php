<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Tyre_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = '', $slug = '')
    {
        $this->db->select('*');
        $this->db->from(db_prefix() . 'tyre_width');
        if($id)
        {
            $this->db->where('id', $id);
        }
        return $this->db->get()->row();
    }

    /**
     * Add new tyreWidth
     * @param array $data tyreWidth data
     */
    public function add_tyrewidth($data)
    {
        $this->db->insert(db_prefix() . 'tyre_width', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            log_activity('New Width Added [id: ' . $insert_id . ']');
        }
        return $insert_id;
    }

    /**
     * Update tyreWidth
     * @param  array $data tyreWidth data
     * @param  mixed $id   id
     * @return boolean
     */
    public function update_tyrewidth($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'tyre_width', $data);
        if ($this->db->affected_rows() > 0) {
            log_activity('Width Updated [id: ' . $id . ']');
            return true;
        }
        return false;
    }

    /**
     * Delete tyreWidth from database and all tyreWidth connections
     * @param  mixed $id tyreWidth ID
     * @return boolean
     */
    public function delete_tyrewidth($id)
    {
        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'tyre_width');
        if ($this->db->affected_rows() > 0) {
            
            log_activity('Width Deleted [id: ' . $id . ']');

            return true;
        }

        return false;
    }
    
    /**
     * Delete tyreWidth from database and all tyreWidth connections
     * @param  mixed $id tyreWidth ID
     * @return boolean
     */
    public function delete_tyreprofile($id)
    {
        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'tyre_profile');
        if ($this->db->affected_rows() > 0) {
            
            log_activity('Model Deleted [id: ' . $id . ']');

            return true;
        }

        return false;
    }
    /**
     * Delete tyreWidth from database and all tyreWidth connections
     * @param  mixed $id tyreWidth ID
     * @return boolean
     */
    public function delete_tyrediameter($id)
    {
        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'tyre_diameter');
        if ($this->db->affected_rows() > 0) {
            
            log_activity('Diameter Deleted [id: ' . $id . ']');

            return true;
        }

        return false;
    }
    /**
     * Delete tyreWidth from database and all tyreWidth connections
     * @param  mixed $id tyreWidth ID
     * @return boolean
     */
    public function delete_tyrebrand($id)
    {
        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'tyre_brand');
        if ($this->db->affected_rows() > 0) {
            
            log_activity('Brand Deleted [id: ' . $id . ']');

            return true;
        }

        return false;
    }
}
