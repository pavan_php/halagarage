<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



if ( ! function_exists('size_as_kb'))
{
			
		function size_as_kb($size)
		{
			if ($size < 1024) {
				return "{$size} bytes";
			} elseif ($size < 1048576) {
				$size_kb = round($size/1024);
				return "{$size_kb} KB";
			} else {
				$size_mb = round($size/1048576, 1);
				return "{$size_mb} MB";
			}
		}
}
	function pickup_status($status){
		if($status=='0')
		{
			$response = 'Pending';
		}
		else if($status=='1'){
			$response = 'On Pickup';
		}
		else if($status=='2'){
			$response = 'Pickuped';
		}
		return $response;
	}
	
	function shipping($amount){
		$CI =& get_instance();
		$ship = 0;
		$result = $CI->db->get('shipping_charge')->result();
		//print_r($result);
		foreach($result as $value){
			if($value->from <= $amount && $value->to >= $amount){
			
				$ship = $value->charge;
			}
		}
 		
		return $ship;
	}
/*
	 function time_Ago($timeago) 
    { 
		$timestamp = strtotime($timeago);	

		$strTime = array("second", "minute", "hour", "day", "month", "year");
		$length = array("60","60","24","30","12","10");

		$currentTime = time();
		if($currentTime >= $timestamp) {
			$diff     = time()- $timestamp;
			for($i = 0; $diff >= $length[$i] && $i < count($length)-1; $i++) {
			$diff = $diff / $length[$i];
			}

			$diff = round($diff);
			return $diff . " " . $strTime[$i] . "(s) ago "; 
		} 
	}
 */

	function getcustomprice()
	{
		$CI =& get_instance();
			if ($CI->session->userdata('user_category') == '1') {
			   $data ='sale_price_s';
		   }else if($CI->session->userdata('user_category') == '2'){
				$data ='sale_price_m';
		   }
		   else if($CI->session->userdata('user_category') == '3'){
				$data ='sale_price_l';
		   }else{
			   $data ='sale_price';
		   }
		   return   $data;
	}
	
	function getCGSTAmountInvoice($price, $qty, $product_id)
	{
		$CI =& get_instance();
		// Get Product Details
		$total_gst			=	0;
		$result_product		=	$CI->db->get_where('product', array('product_id' => $product_id))->row();	
			
		$gst_type			=	$result_product->gst_type;	
		$gst_id				=	$result_product->gst;
		
		$result_gst			=	$CI->db->get_where('gst', array('gst_id' => $gst_id))->row();
		
		if($result_product->gst_type=='2'){
			$price_with_qty		=	($price*$qty);
			$total_gst			=	($price_with_qty*$result_gst->value)/100;
		}
		
		return $total_gst;
	}

	/* success Massage */
	function succ_message($msg)
	{
		return '<section class="content"><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>';
	}
	/* error Message*/
	function err_message($msg)
	{
		return '<section class="content"><div class="col-xs-12"><div class="alert alert-danger alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>';
	}

	/* Send Sms */
	function send_sms($data)
	{
		$CI =& get_instance();
		//Your authentication key
	
		//$authKey = "268511A97ndPZVVDeE5c920ce8"; //TRIBE accout
		$authKey = "45773ASVm1jvCwzE5616662c"; //basant test accout

		//Multiple mobiles numbers separated by comma
		$mobileNumber = $data['moblie_no'];

		//Your message to send, Add URL encoding here.
		$message = urlencode($data['message']);

		//Define route 
		$route 		=	"4";
		$senderId 	=	"QTRIBE";
		if(isset($changeRoute)){ $route = "1"; }

		//Prepare you post parameters
		$postData = array(
		'authkey' => $authKey,
		'mobiles' => $mobileNumber,
		'message' => $message,
		'sender' => $senderId,
		'route' => $route
		);

		//API URL
		$url="https://control.msg91.com/api/sendhttp.php";

		// init the resource
		$ch = curl_init();
		curl_setopt_array($ch, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_POST => true,
		CURLOPT_POSTFIELDS => $postData
		//,CURLOPT_FOLLOWLOCATION => true
		));

		//Ignore SSL certificate verification
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		//get response
		$output = curl_exec($ch);


		$CI->db->insert('sms_function_hit_log',array('input'=>json_encode($postData)));
			$insert_id = $CI->db->insert_id();

			$CI->db->where('sms_function_hit_log_id',$insert_id);
			$CI->db->update('sms_function_hit_log',array('output'=>json_encode($output)));


		return true;
	}
	
	
	/* Upload Image */
	function ImageUpload($filename, $name, $imagePath, $fieldName)
	{	
		$CI =& get_instance();
		$CI->load->library('upload');
		$temp = explode(".",$filename);
		$extension = end($temp);
		$filenew =  date('d-M-Y').'_'.str_replace($filename,$name,$filename).'_'.rand(). "." .$extension;  		
		$config['file_name'] = $filenew;
		$config['upload_path'] = $imagePath;
		$config['allowed_types'] = 'GIF | PDF | gif | JPE | jpe | JPEG | jpeg | JPG | jpg | PNG | png';
		$CI->upload->initialize($config);
		$CI->upload->set_allowed_types('*');
		$CI->upload->set_filename($config['upload_path'],$filenew);
		  

		if(!$CI->upload->do_upload($fieldName))
		{
			$data = array('msg' => $CI->upload->display_errors());
			//print_r($data);
		}
		else 
		{ 
			$data = $CI->upload->data();				
			$imageName = $data['file_name'];			
			return $imageName;
		}		
	}
	/*
	*	@function:
	*/
	function sendPushNotification($userId, $message, $notify_type,$url)
    {
    	$CI =& get_instance();
       
     
        $device_id = $CI->db->get_where('user', array('user_id' => $userId))->row('device_id');
        if($device_id!='')
        {
        	//print_r($device_id);
            $registrationDeviceIds = array($device_id);
            // prep the bundle
            $msg = array
            (
                'message'   => $message,
                'image'   	=> $url,
                'notify_type'   => notify_type($notify_type),
                'title'     => 'TRIBE',
                'subtitle'  => 'New notification',
                'tickerText'=> 'Order Status',
                'vibrate'   => 1,
                'sound'     => 1,
                'largeIcon' => 'large_icon',
                'smallIcon' => 'small_icon',
                "priority"  => 'high'
            );
            $fields = array
            (
                'registration_ids'  => $registrationDeviceIds,
                'data'          => $msg
            );
             
            $headers = array
            (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );
             
            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec($ch );
        //    var_dump($result);//
            curl_close( $ch );
            //echo $result;
        }
    }

	/*
	*	@function:
	*/
	function sendPushNotificationVendor($userId, $message, $notify_type)
    {
    	$CI =& get_instance();
       
    // print_r($userId);
        $device_id = $CI->db->get_where('vendor', array('vendor_id' => $userId))->row('device_id');
        if($device_id!='')
        {
           
			 $registrationDeviceIds = array($device_id);
            // prep the bundle
            $msg['message'] 	=	$message;
			if($notify_type==3){
		 		$msg['image'] 		=	$message;
			}
            $msg['notify_type'] =	notify_type($notify_type);
            $msg['title'] 		=	'TRIBE';
            $msg['subtitle'] 	=	'New notification';
            $msg['tickerText'] 	=	'Order Status';
            $msg['vibrate'] 	=	1;
            $msg['sound'] 		=	1;
            $msg['largeIcon'] 	=	'large_icon';
            $msg['smallIcon'] 	=	'small_icon';
             $msg['priority']  	=   'high';
			
            // prep the bundle
           /* $msg = array
            (
                'message'   => $message,
                'notify_type'   => notify_type($notify_type),
                'title'     => 'TRIBE',
                'subtitle'  => 'New notification',
                'tickerText'    => 'Order Status',
                'vibrate'   => 1,
                'sound'     => 1,
                'largeIcon' => 'large_icon',
                'smallIcon' => 'small_icon'
            );*/
            $fields = array
            (
                'registration_ids'  => $registrationDeviceIds,
                'data'          => $msg
            );
             
            $headers = array
            (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );
             
            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec($ch );
            curl_close( $ch );
            //echo $result;
        }
    }
	
    function notify_type($id="")
    {
    	//print_r($id);
    	if($id=="1")
		{
			$type = "message";
		}
		else if($id=="2")
		{
			$type = "url";
		}
		else if($id=="3")
		{
			$type = "image";
		}
		else if($id=="4")
		{
			$type = "order";
		}
		else if($id=="5")
		{
			$type = "update";
		}
		else if($id=="6")
		{
			$type = "logout";
		}
		return $type;

    }
    function sendPushNotificationTopics($message, $notify_type,$url)
    {
			// prep the bundle
            $msg['message'] 	=	$message;
			if($notify_type==3){
		 		$msg['image'] 		=	$url;
			}
            $msg['notify_type'] =	notify_type($notify_type);
            $msg['title'] 		=	'TRIBE';
            $msg['subtitle'] 	=	'New notification';
            $msg['tickerText'] 	=	'Order Status';
            $msg['vibrate'] 	=	1;
            $msg['sound'] 		=	1;
            $msg['largeIcon'] 	=	'large_icon';
            $msg['smallIcon'] 	=	'small_icon';
       		$msg['priority']  	=   'high';
            // prep the bundle
           /* $msg = array
            (
                'message'   => $message,
                'notify_type'   => notify_type($notify_type),
                'title'     => 'smt',
                'subtitle'  => 'New notification',
                'tickerText'    => 'Order Status',
                'vibrate'   => 1,
                'sound'     => 1,
                'largeIcon' => 'large_icon',
                'smallIcon' => 'small_icon'
            );*/
			
			$fields = array
			(
				'to'  => '/topics/update',
		 		'data'          => $msg
			);
             
            $headers = array
            (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );
             
            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec($ch );
            curl_close( $ch );
            // echo '--------'.$result;
        
    }

	
	