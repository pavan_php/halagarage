<?php

defined('BASEPATH') or exit('No direct script access allowed');

function app_init_admin_sidebar_menu_items()
{
    $CI = &get_instance();

    $custommenu = $CI->db->get_where('tbloptions', array('name' => 'aside_menu_active'))->row('value');
    $M = json_decode($custommenu);
//echo '<pre>'; print_r(json_decode($custommenu)); die;

    $CI->app_menu->add_sidebar_menu_item('dashboard', [
        'name'     => _l($M->dashboard->label),
        'href'     => admin_url(),
        'position' => 1,
        'icon'     => 'fa fa-home',
        'label'    => _l($M->dashboard->label),
        'heading'  => _l($M->dashboard->heading),
    ]);
/*
    if (has_permission('customers', '', 'view')
        || (have_assigned_customers()
        || (!have_assigned_customers() && has_permission('customers', '', 'create')))) {
        $CI->app_menu->add_sidebar_menu_item('customers', [
            'name'     => _l('als_clients'),
            'href'     => admin_url('clients'),
            'position' => 5,
            'icon'     => 'fa fa-user-o',
        ]);
    }
*/
    $CI->app_menu->add_sidebar_menu_item('newsletter', [
        'name'     => _l($M->newsletter->label),
        'href'     => admin_url('newsletter'),
        'position' => 1,
        'icon'     => 'fa fa-home',
        'label'    => _l($M->newsletter->label),
        'heading'  => _l($M->newsletter->heading),
    ]);
    /*
    $CI->app_menu->add_sidebar_menu_item('content', [
        'name'     => _l($M->content->label),
        'href'     => admin_url('content'),
        'position' => 1,
        'icon'     => 'fa fa-home',
        'label'    => _l($M->content->label),
        'heading'  => _l($M->content->heading),
    ]);
    */
    
    // Category 
    $CI->app_menu->add_sidebar_menu_item('category', [
        'name'     => _l($M->category->label),
        'href'     => admin_url('category'),
        'position' => 1,
        'icon'     => 'fa fa-home',
        'label'    => _l($M->category->label),
        'heading'  => _l($M->category->heading),
    ]);
    
    // Video 
    $CI->app_menu->add_sidebar_menu_item('videoSection', [
        'name'     => _l($M->videoSection->label),
        'href'     => admin_url('videoSection'),
        'position' => 1,
        'icon'     => 'fa fa-home',
        'label'    => _l($M->videoSection->label),
        'heading'  => _l($M->videoSection->heading),
    ]);  
    
    // Message 
    $CI->app_menu->add_sidebar_menu_item('message', [
        'name'     => _l($M->message->label),
        'href'     => admin_url('message'),
        'position' => 6,
        'icon'     => 'fa fa-home',
        'label'    => _l($M->message->label),
        'heading'  => _l($M->message->heading),
    ]);
    
    // QuoteRequest 
    $CI->app_menu->add_sidebar_menu_item('quoteRequest', [
        'name'     => _l($M->quoteRequest->label),
        'href'     => admin_url('quoteRequest'),
        'position' => 6,
        'icon'     => 'fa fa-home',
        'label'    => _l($M->quoteRequest->label),
        'heading'  => _l($M->quoteRequest->heading),
    ]);
    
    // Products 
    $CI->app_menu->add_sidebar_menu_item('products', [
        'name'     => _l($M->products->label),
        'href'     => admin_url('products'),
        'position' => 6,
        'icon'     => 'fa fa-products',
        'label'    => _l($M->products->label),
        'heading'  => _l($M->products->heading),
    ]);
    
    // Free trail days 
    $CI->app_menu->add_sidebar_menu_item('freetrial', [
        'name'     => _l($M->freetrial->label),
        'href'     => admin_url('freetrial'),
        'position' => 12,
        'icon'     => 'fa fa-freetrial',
        'label'    => _l($M->freetrial->label),
        'heading'  => _l($M->freetrial->heading),
    ]);
    
    // Advertisement
    $CI->app_menu->add_sidebar_menu_item('advertisement', [
        'name'     => _l($M->advertisement->label),
        'href'     => admin_url('advertisement'),
        'position' => 13,
        'icon'     => 'fa fa-advertisement',
        'label'    => _l($M->advertisement->label),
        'heading'  => _l($M->advertisement->heading),
    ]);
    
    //Content
    $CI->app_menu->add_sidebar_menu_item('content', [
            'collapse' => true,
            'name'     => _l($M->content->label),
            'position' => 55,
            'icon'     => 'fa fa-cogs',
            'label'    => _l($M->content->label),
            'heading'  => _l($M->content->heading),
        ]);
    
    $CI->app_menu->add_sidebar_children_item('content', [
            'slug'     => 'aboutUs',
            'name'     => _l($M->content->children->aboutUs->label),
            'href'     => admin_url('aboutUs'),
            'position' => 5,
            'label'     => _l($M->content->children->aboutUs->label),
            'heading'     => _l($M->content->children->aboutUs->heading),
        ]);
    
    $CI->app_menu->add_sidebar_children_item('content', [
            'slug'     => 'contactUs',
            'name'     => _l($M->content->children->contactUs->label),
            'href'     => admin_url('contactUs'),
            'position' => 5,
            'label'     => _l($M->content->children->contactUs->label),
            'heading'     => _l($M->content->children->contactUs->heading),
        ]);
    $CI->app_menu->add_sidebar_children_item('content', [
            'slug'     => 'privacyPolicy',
            'name'     => _l($M->content->children->privacyPolicy->label),
            'href'     => admin_url('privacyPolicy'),
            'position' => 5,
            'label'     => _l($M->content->children->privacyPolicy->label),
            'heading'     => _l($M->content->children->privacyPolicy->heading),
        ]);
    $CI->app_menu->add_sidebar_children_item('content', [
            'slug'     => 'termsAndCondition',
            'name'     => _l($M->content->children->termsAndCondition->label),
            'href'     => admin_url('termsAndCondition'),
            'position' => 5,
            'label'     => _l($M->content->children->termsAndCondition->label),
            'heading'     => _l($M->content->children->termsAndCondition->heading),
        ]);
    $CI->app_menu->add_sidebar_children_item('content', [
            'slug'     => 'FAQ',
            'name'     => _l($M->content->children->FAQ->label),
            'href'     => admin_url('FAQ'),
            'position' => 7,
            'label'     => _l($M->content->children->FAQ->label),
            'heading'     => _l($M->content->children->FAQ->heading),
        ]);
/*
    //Video section
    $CI->app_menu->add_sidebar_children_item('content', [
            'slug'     => 'videoSection',
            'name'     => _l($M->content->children->videoSection->label),
            'href'     => admin_url('videoSection'),
            'position' => 5,
            'label'     => _l($M->content->children->videoSection->label),
            'heading'     => _l($M->content->children->videoSection->heading),
        ]);
    */
    $CI->app_menu->add_sidebar_menu_item('garages', [
        'name'     => _l($M->garages->label),
        'href'     => admin_url('garages'),
        'position' => 1,
        'icon'     => 'fa fa-home',
        'label'    => _l($M->garages->label),
        'heading'  => _l($M->garages->heading),
    ]);
    
    //Reports
    $CI->app_menu->add_sidebar_menu_item('report', [
        'name'     => _l($M->report->label),
        'href'     => admin_url('report'),
        'position' => 39,
        'icon'     => 'fa fa-report',
        'label'    => _l($M->report->label),
        'heading'  => _l($M->report->heading),
    ]);
    $CI->app_menu->add_sidebar_menu_item('customers', [
            'collapse' => true,
            'name'     => _l($M->customers->label),
            'position' => 5,
            'icon'     => 'fa fa-user-o',
            'label'     => _l($M->customers->label),
            'heading'  => _l($M->customers->heading),
        ]);

    $CI->app_menu->add_sidebar_children_item('customers', [
                'slug'     => 'customer',
                'name'     => _l($M->customers->children->customer->label),
                'href'     => admin_url('clients'),
                'position' => 5,
                'label'     => _l($M->customers->children->customer->label),
                'heading'     => _l($M->customers->children->customer->heading),
        ]);
        
    $CI->app_menu->add_sidebar_children_item('customers', [
                'slug'     => 'add_customer',
                'name'     => _l($M->customers->children->add_customer->label),
                'href'     => admin_url('clients/client'),
                'position' => 6,
                'label'     => _l($M->customers->children->add_customer->label),
                'heading'     => _l($M->customers->children->add_customer->heading),
        ]);
        
    $CI->app_menu->add_sidebar_children_item('customers', [
                'slug'     => 'import_customer',
                'name'     => _l($M->customers->children->import_customer->label),
                'href'     => admin_url('clients/import'),
                'position' => 7,
                'label'     => _l($M->customers->children->import_customer->label),
                'heading'     => _l($M->customers->children->import_customer->heading),
        ]);

    $CI->app_menu->add_sidebar_menu_item('sales', [
            'collapse' => true,
            'name'     => _l($M->sales->label),
            'position' => 10,
            'icon'     => 'fa fa-balance-scale',
            'label'     => _l($M->sales->label),
            'heading'  => _l($M->sales->heading),
        ]);

    if ((has_permission('proposals', '', 'view') || has_permission('proposals', '', 'view_own'))
        || (staff_has_assigned_proposals() && get_option('allow_staff_view_proposals_assigned') == 1)) {
        $CI->app_menu->add_sidebar_children_item('sales', [
                'slug'     => 'proposals',
                'name'     => _l($M->sales->children->proposals->label),
                'href'     => admin_url('proposals'),
                'position' => 5,
                'label'     => _l($M->sales->children->proposals->label),
                'heading'     => _l($M->sales->children->proposals->heading),
        ]);
    }

    if ((has_permission('estimates', '', 'view') || has_permission('estimates', '', 'view_own'))
        || (staff_has_assigned_estimates() && get_option('allow_staff_view_estimates_assigned') == 1)) {
        $CI->app_menu->add_sidebar_children_item('sales', [
                'slug'     => 'estimates',
                'name'     => _l($M->sales->children->estimates->label),
                'href'     => admin_url('estimates'),
                'position' => 10,
                'label'     => _l($M->sales->children->estimates->label),
                'heading'     => _l($M->sales->children->estimates->heading),
        ]);
    }

    if ((has_permission('invoices', '', 'view') || has_permission('invoices', '', 'view_own'))
         || (staff_has_assigned_invoices() && get_option('allow_staff_view_invoices_assigned') == 1)) {
        $CI->app_menu->add_sidebar_children_item('sales', [
                'slug'     => 'invoices',
                'name'     => _l($M->sales->children->invoices->label),
                'href'     => admin_url('invoices'),
                'position' => 15,
                'label'     => _l($M->sales->children->invoices->label),
                'heading'     => _l($M->sales->children->invoices->heading),
        ]);
    }

    if (has_permission('payments', '', 'view') || has_permission('invoices', '', 'view_own')
           || (get_option('allow_staff_view_invoices_assigned') == 1 && staff_has_assigned_invoices())) {
        $CI->app_menu->add_sidebar_children_item('sales', [
                'slug'     => 'payments',
                'name'     => _l($M->sales->children->payments->label),
                'href'     => admin_url('payments'),
                'position' => 20,
                'label'     => _l($M->sales->children->payments->label),
                'heading'     => _l($M->sales->children->payments->heading),
        ]);
    }

    if (has_permission('credit_notes', '', 'view') || has_permission('credit_notes', '', 'view_own')) {
        $CI->app_menu->add_sidebar_children_item('sales', [
                'slug'     => 'credit_notes',
                'name'     => _l($M->sales->children->credit_notes->label),
                'href'     => admin_url('credit_notes'),
                'position' => 25,
                'label'     => _l($M->sales->children->credit_notes->label),
                'heading'     => _l($M->sales->children->credit_notes->heading),
        ]);
    }

    if (has_permission('items', '', 'view')) {
        $CI->app_menu->add_sidebar_children_item('sales', [
                'slug'     => 'items',
                'name'     => _l($M->sales->children->items->label),
                'href'     => admin_url('invoice_items'),
                'position' => 30,
                'label'     => _l($M->sales->children->items->label),
                'heading'     => _l($M->sales->children->items->heading),
        ]);
    }

    if (has_permission('subscriptions', '', 'view') || has_permission('subscriptions', '', 'view_own')) {
        $CI->app_menu->add_sidebar_menu_item('subscriptions', [
                'name'     => _l($M->subscriptions->label),
                'href'     => admin_url('subscriptions'),
                'icon'     => 'fa fa-repeat',
                'position' => 15,
                'label'     => _l($M->subscriptions->label),
                'heading'  => _l($M->subscriptions->heading),
        ]);
    }
    /*
    //Content
    $CI->app_menu->add_sidebar_menu_item('subscription', [
            'collapse' => true,
            'name'     => _l($M->subscription->label),
            'position' => 15,
            'icon'     => 'fa fa-cogs',
            'label'    => _l($M->subscription->label),
            'heading'  => _l($M->subscription->heading),
        ]);
    
    $CI->app_menu->add_sidebar_children_item('subscription', [
            'slug'     => 'garageSubscription',
            'name'     => _l($M->subscription->children->garageSubscription->label),
            'href'     => admin_url('subscription'),
            'position' => 20,
            'label'     => _l($M->subscription->children->garageSubscription->label),
            'heading'     => _l($M->subscription->children->garageSubscription->heading),
        ]);
        
    $CI->app_menu->add_sidebar_children_item('subscription', [
            'slug'     => 'featuredSubscription',
            'name'     => _l($M->subscription->children->featuredSubscription->label),
            'href'     => admin_url('subscription/featured'),
            'position' => 21,
            'label'     => _l($M->subscription->children->featuredSubscription->label),
            'heading'     => _l($M->subscription->children->featuredSubscription->heading),
        ]);
        */
    
    //Subscription   
    $CI->app_menu->add_sidebar_menu_item('subscription', [
            'name'     => _l($M->subscription->label),
            'href'     => admin_url('subscription'),
            'icon'     => 'fa fa-repeat',
            'position' => 15,
            'label'     => _l($M->subscription->label),
            'heading'  => _l($M->subscription->heading),
    ]);
    
    //Featured Subscription   
    $CI->app_menu->add_sidebar_menu_item('featured', [
            'name'     => _l($M->featured->label),
            'href'     => admin_url('subscription/featured'),
            'icon'     => 'fa fa-tag',
            'position' => 15,
            'label'     => _l($M->featured->label),
            'heading'  => _l($M->featured->heading),
    ]);

    if (has_permission('expenses', '', 'view') || has_permission('expenses', '', 'view_own')) {
        $CI->app_menu->add_sidebar_menu_item('expenses', [
                'name'     => _l($M->expenses->label),
                'href'     => admin_url('expenses'),
                'icon'     => 'fa fa-file-text-o',
                'position' => 20,
                'label'     => _l($M->expenses->label),
                'heading'  => _l($M->expenses->heading),
        ]);
    }

    if (has_permission('contracts', '', 'view') || has_permission('contracts', '', 'view_own')) {
        $CI->app_menu->add_sidebar_menu_item('contracts', [
                'name'     => _l($M->contracts->label),
               // 'href'     => admin_url('contracts'),
                'href'     => '#',
                'icon'     => 'fa fa-file',
                'position' => 25,
                'label'     => _l($M->contracts->label),
                'heading'  => _l($M->contracts->heading),
        ]);
    }

    $CI->app_menu->add_sidebar_menu_item('projects', [
                'name'     => _l($M->projects->label),
                //'href'     => admin_url('projects'),
                'href'     => '#',
                'icon'     => 'fa fa-bars',
                'position' => 30,
                'label'     => _l($M->projects->label),
                'heading'  => _l($M->projects->heading),
        ]);

    $CI->app_menu->add_sidebar_menu_item('tasks', [
                'name'     => _l($M->tasks->label),
                //'href'     => admin_url('tasks'),
                'href'     => '#',
                'icon'     => 'fa fa-tasks',
                'position' => 35,
                'label'     => _l($M->tasks->label),
                'heading'  => _l($M->tasks->heading),
        ]);
        
    $CI->app_menu->add_sidebar_menu_item('lead', [
                'name'     => _l($M->lead->label),
                'href'     => admin_url('lead'),
                'icon'     => 'fa fa-tty',
                'position' => 37,
                'label'     => _l($M->lead->label),
                'heading'  => _l($M->lead->heading),
        ]);
        
    $CI->app_menu->add_sidebar_menu_item('orders', [
                'name'     => _l($M->orders->label),
                'href'     => admin_url('orders'),
                'icon'     => 'fa fa-tasks',
                'position' => 36,
                'label'     => _l($M->orders->label),
                'heading'  => _l($M->orders->heading),
        ]);

    if ((!is_staff_member() && get_option('access_tickets_to_none_staff_members') == 1) || is_staff_member()) {
        $CI->app_menu->add_sidebar_menu_item('support', [
                'name'     => _l($M->support->label),
                'href'     => admin_url('tickets'),
                'icon'     => 'fa fa-ticket',
                'position' => 40,
                'label'     => _l($M->support->label),
                'heading'  => _l($M->support->heading),
        ]);
    }

    if (is_staff_member()) {
        $CI->app_menu->add_sidebar_menu_item('leads', [
                'name'     => _l($M->leads->label),
                'href'     => admin_url('leads'),
                'icon'     => 'fa fa-tty',
                'position' => 45,
                'label'     => _l($M->leads->label),
                'heading'  => _l($M->leads->heading),
        ]);
    }

    if (has_permission('knowledge_base', '', 'view')) {
        $CI->app_menu->add_sidebar_menu_item('knowledge_base', [
                'name'     => _l($M->knowledge_base->label),
                'href'     => admin_url('knowledge_base'),
                'icon'     => 'fa fa-folder-open-o',
                'position' => 50,
                'label'    => _l($M->knowledge_base->label),
                'heading'  => _l($M->knowledge_base->heading),
        ]);
    }
    
    //Communicate
    $CI->app_menu->add_sidebar_menu_item('communicate', [
            'collapse' => true,
            'name'     => _l($M->communicate->label),
            'position' => 55,
            'icon'     => 'fa fa-cogs',
            'label'    => _l($M->communicate->label),
            'heading'  => _l($M->communicate->heading),
        ]);
    
    $CI->app_menu->add_sidebar_children_item('communicate', [
                'slug'     => 'sendemail',
                'name'     => _l($M->communicate->children->sendemail->label),
                'href'     => admin_url('communicate/sendemail'),
                'position' => 5,
                'label'     => _l($M->communicate->children->sendemail->label),
                'heading'     => _l($M->communicate->children->sendemail->heading),
        ]);
    $CI->app_menu->add_sidebar_children_item('communicate', [
                'slug'     => 'sendsms_',
                'name'     => _l($M->communicate->children->sendsms_->label),
                'href'     => admin_url('communicate/sendsms_'),
                'position' => 5,
                'label'     => _l($M->communicate->children->sendsms_->label),
                'heading'     => _l($M->communicate->children->sendsms_->heading),
        ]);
    $CI->app_menu->add_sidebar_children_item('communicate', [
                'slug'     => 'emailsmdlog',
                'name'     => _l($M->communicate->children->emailsmdlog->label),
                'href'     => admin_url('communicate/emailsmdlog'),
                'position' => 5,
                'label'     => _l($M->communicate->children->emailsmdlog->label),
                'heading'     => _l($M->communicate->children->emailsmdlog->heading),
        ]);

    // Utilities
    $CI->app_menu->add_sidebar_menu_item('utilities', [
            'collapse' => true,
            'name'     => _l($M->utilities->label),
            'position' => 55,
            'icon'     => 'fa fa-cogs',
            'label'    => _l($M->utilities->label),
            'heading'  => _l($M->utilities->heading),
        ]);

    $CI->app_menu->add_sidebar_children_item('utilities', [
                'slug'     => 'media',
                'name'     => _l($M->utilities->children->media->label),
                'href'     => admin_url('utilities/media'),
                'position' => 5,
                'label'     => _l($M->utilities->children->media->label),
                'heading'     => _l($M->utilities->children->media->heading),
        ]);

    if (has_permission('bulk_pdf_exporter', '', 'view')) {
        $CI->app_menu->add_sidebar_children_item('utilities', [
                'slug'     => 'bulk_pdf_exporter',
                'name'     => _l($M->utilities->children->bulk_pdf_exporter->label),
                'href'     => admin_url('utilities/bulk_pdf_exporter'),
                'position' => 10,
                'label'     => _l($M->utilities->children->bulk_pdf_exporter->label),
                'heading'     => _l($M->utilities->children->bulk_pdf_exporter->heading),
        ]);
    }

    $CI->app_menu->add_sidebar_children_item('utilities', [
                'slug'     => 'calendar',
                'name'     => _l($M->utilities->children->calendar->label),
                'href'     => admin_url('utilities/calendar'),
                'position' => 15,
                'label'     => _l($M->utilities->children->calendar->label),
                'heading'     => _l($M->utilities->children->calendar->heading),
        ]);

    if (is_admin()) {
        $CI->app_menu->add_sidebar_children_item('utilities', [
                'slug'     => 'announcements',
                'name'     => _l($M->utilities->children->announcements->label),
                'href'     => admin_url('announcements'),
                'position' => 20,
                'label'     => _l($M->utilities->children->announcements->label),
                'heading'     => _l($M->utilities->children->announcements->heading),
        ]);

        $CI->app_menu->add_sidebar_children_item('utilities', [
                'slug'     => 'activity_log',
                'name'     => _l($M->utilities->children->activity_log->label),
                'href'     => admin_url('utilities/activity_log'),
                'position' => 25,
                'label'     => _l($M->utilities->children->activity_log->label),
                'heading'     => _l($M->utilities->children->activity_log->heading),
        ]);

        $CI->app_menu->add_sidebar_children_item('utilities', [
                'slug'     => 'ticket_pipe_log',
                'name'     => _l($M->utilities->children->ticket_pipe_log->label),
                'href'     => admin_url('utilities/pipe_log'),
                'position' => 30,
                'label'     => _l($M->utilities->children->ticket_pipe_log->label),
                'heading'     => _l($M->utilities->children->ticket_pipe_log->heading),
        ]);
    }

    if (has_permission('reports', '', 'view')) {
        $CI->app_menu->add_sidebar_menu_item('reports', [
                'collapse' => true,
                'name'     => _l($M->reports->label),
                'icon'     => 'fa fa-area-chart',
                'position' => 60,
                'label'     => _l($M->reports->label),
                'heading'  => _l($M->reports->heading),
        ]);
        $CI->app_menu->add_sidebar_children_item('reports', [
                'slug'     => 'sales_reports',
                'name'     => _l($M->reports->children->sales_reports->label),
                //'href'     => admin_url('reports/sales'),
                'href'     => '#',
                'position' => 5,
                'label'     => _l($M->reports->children->sales_reports->label),
                'heading'     => _l($M->reports->children->sales_reports->heading),
        ]);
        $CI->app_menu->add_sidebar_children_item('reports', [
                'slug'     => 'expenses_reports',
                'name'     => _l($M->reports->children->expenses_reports->label),
                //'href'     => admin_url('reports/expenses'),
                'href'     => '#',
                'position' => 10,
                'label'     => _l($M->reports->children->expenses_reports->label),
                'heading'     => _l($M->reports->children->expenses_reports->heading),
        ]);
        $CI->app_menu->add_sidebar_children_item('reports', [
                'slug'     => 'expenses_vs_income_reports',
                'name'     => _l($M->reports->children->expenses_vs_income_reports->label),
                //'href'     => admin_url('reports/expenses_vs_income'),
                'href'     => '#',
                'position' => 15,
                'label'     => _l($M->reports->children->expenses_vs_income_reports->label),
                'heading'     => _l($M->reports->children->expenses_vs_income_reports->heading),
        ]);
        $CI->app_menu->add_sidebar_children_item('reports', [
                'slug'     => 'leads_reports',
                'name'     => _l($M->reports->children->leads_reports->label),
                'href'     => admin_url('reports/leads'),
                'position' => 20,
                'label'     => _l($M->reports->children->leads_reports->label),
                'heading'     => _l($M->reports->children->leads_reports->heading),
        ]);

        if (is_admin()) {
            $CI->app_menu->add_sidebar_children_item('reports', [
                    'slug'     => 'timesheets_reports',
                    'name'     => _l($M->reports->children->timesheets_reports->label),
                    'href'     => admin_url('staff/timesheets?view=all'),
                    'position' => 25,
                    'label'     => _l($M->reports->children->timesheets_reports->label),
                    'heading'     => _l($M->reports->children->timesheets_reports->heading),
            ]);
        }

        $CI->app_menu->add_sidebar_children_item('reports', [
                    'slug'     => 'knowledge_base_reports',
                    'name'     => _l($M->reports->children->knowledge_base_reports->label),
                    'href'     => admin_url('reports/knowledge_base_articles'),
                    'position' => 30,
                    'label'     => _l($M->reports->children->knowledge_base_reports->label),
                    'heading'     => _l($M->reports->children->knowledge_base_reports->heading),
            ]);
    }

    $customsub_menu = $CI->db->get_where('tbloptions', array('name' => 'setup_menu_active'))->row('value');
    $sM = json_decode($customsub_menu);
    //echo '<pre>'; print_r($sM); die;
    
    // Setup menu
    
    $CI->app_menu->add_setup_menu_item('master', [
                    'collapse' => true,
                    'name'     => _l($sM->master->label),
                    'position' => 10,
                    'label'     => _l($sM->master->label),
                    'heading'     => _l($sM->master->heading),
            ]);
            
        $CI->app_menu->add_setup_children_item('master', [
                    'slug'     => 'car',
                    'name'     => _l($sM->master->children->car->label),
                    'href'     => admin_url('car'),
                    'position' => 5,
                    'label'     => _l($sM->master->children->car->label),
                    'heading'     => _l($sM->master->children->car->heading),
            ]);
            
        $CI->app_menu->add_setup_children_item('master', [
                    'slug'     => 'tyre',
                    'name'     => _l($sM->master->children->tyre->label),
                    'href'     => admin_url('tyre'),
                    'position' => 5,
                    'label'     => _l($sM->master->children->tyre->label),
                    'heading'     => _l($sM->master->children->tyre->heading),
            ]);
        // Location
        $CI->app_menu->add_setup_children_item('master', [
                    'slug'     => 'location',
                    'name'     => _l($sM->master->children->location->label),
                    'href'     => admin_url('location'),
                    'position' => 5,
                    'label'     => _l($sM->master->children->location->label),
                    'heading'     => _l($sM->master->children->location->heading),
            ]);
        // Garage Type
        $CI->app_menu->add_setup_children_item('master', [
                    'slug'     => 'garageType',
                    'name'     => _l($sM->master->children->garageType->label),
                    'href'     => admin_url('garageType'),
                    'position' => 5,
                    'label'     => _l($sM->master->children->garageType->label),
                    'heading'     => _l($sM->master->children->garageType->heading),
            ]);
    
    if (has_permission('settings', '', 'view')) {
        $CI->app_menu->add_setup_menu_item('settings', [
                    'href'     => admin_url('settings'),
                    'name'     => _l($sM->settings->label),
                    'position' => 3,
                    'label'     => _l($sM->settings->label),
                    'heading'     => _l($sM->settings->heading),
            ]);
    }
    
    $CI->app_menu->add_setup_menu_item('dashboard_settings', [
                    'href'     => admin_url('dashboardSetting'),
                    'name'     => _l($sM->dashboard_settings->label),
                    'position' => 4,
                    'label'     => _l($sM->dashboard_settings->label),
                    'heading'     => _l($sM->dashboard_settings->heading),
            ]);

    if (has_permission('staff', '', 'view')) {
        $CI->app_menu->add_setup_menu_item('login', [
                    'name'     => _l($sM->login->label),
                    'href'     => admin_url('loginPage'),
                    'position' => 5,
                    'label'     => _l($sM->login->label),
                    'heading'     => _l($sM->login->heading),
            ]);
    }

        $CI->app_menu->add_setup_menu_item('roles', [
                    'href'     => admin_url('roles'),
                    'name'     => _l($sM->roles->label),
                    'label'     => _l($sM->roles->label),
                    'heading'     => _l($sM->roles->heading),
                    'position' => 6,
            ]);

    if (has_permission('staff', '', 'view')) {
        $CI->app_menu->add_setup_menu_item('staff', [
                    'name'     => _l($sM->staff->label),
                    'href'     => admin_url('staff'),
                    'position' => 7,
                    'label'    => _l($sM->staff->label),
                    'heading'    => _l($sM->staff->heading),
            ]);
    }

    if (is_admin()) {
        $CI->app_menu->add_setup_menu_item('customers', [
                    'collapse' => true,
                    'name'     => _l($sM->customers->label),
                    'position' => 10,
                    'label'     => _l($sM->customers->label),
                    'heading'     => _l($sM->customers->heading),
            ]);

        $CI->app_menu->add_setup_children_item('customers', [
                    'slug'     => 'custom_fields',
                    'name'     => _l($sM->customers->children->custom_fields->label),
                    'href'     => admin_url('clients/customFields'),
                    'position' => 5,
                    'label'     => _l($sM->customers->children->custom_fields->label),
                    'heading'     => _l($sM->customers->children->custom_fields->heading),
            ]);
            
        $CI->app_menu->add_setup_children_item('customers', [
                    'slug'     => 'customer_groups',
                    'name'     => _l($sM->customers->children->customer_groups->label),
                    'label'     => _l($sM->customers->children->customer_groups->label),
                    'heading'     => _l($sM->customers->children->customer_groups->heading),
                    'href'     => admin_url('clients/groups'),
                    'position' => 5,
            ]);

        $CI->app_menu->add_setup_menu_item('product_services', [
                    'href'     => admin_url('#'),
                    'name'     => _l('Product / Services'),
                    'label'     => _l('Product / Services'),
                    'position' => 11,
            ]);
            
        $CI->app_menu->add_setup_menu_item('gdpr', [
                    'href'     => admin_url('gdpr'),
                    'name'     => _l($sM->gdpr->label),
                    'label'     => _l($sM->gdpr->label),
                    'heading'     => _l($sM->gdpr->heading),
                    'position' => 50,
            ]);

        $CI->app_menu->add_setup_menu_item('finance', [
                    'collapse' => true,
                    'name'     => _l($sM->finance->label),
                    'label'     => _l($sM->finance->label),
                    'heading'     => _l($sM->finance->heading),
                    'position' => 25,
            ]);
        $CI->app_menu->add_setup_children_item('finance', [
                    'slug'     => 'taxes',
                    'name'     => _l($sM->finance->children->taxes->label),
                    'label'     => _l($sM->finance->children->taxes->label),
                    'heading'     => _l($sM->finance->children->taxes->heading),
                    'href'     => admin_url('taxes'),
                    'position' => 5,
            ]);
        $CI->app_menu->add_setup_children_item('finance', [
                    'slug'     => 'currencies',
                    'name'     => _l($sM->finance->children->currencies->label),
                    'label'     => _l($sM->finance->children->currencies->label),
                    'heading'     => _l($sM->finance->children->currencies->heading),
                    'href'     => admin_url('currencies'),
                    'position' => 10,
            ]);
        $CI->app_menu->add_setup_children_item('finance', [
                    'slug'     => 'payment_modes',
                    'name'     => _l($sM->finance->children->payment_modes->label),
                    'label'     => _l($sM->finance->children->payment_modes->label),
                    'heading'     => _l($sM->finance->children->payment_modes->heading),
                    'href'     => admin_url('paymentmodes'),
                    'position' => 15,
            ]);
        $CI->app_menu->add_setup_children_item('finance', [
                    'slug'     => 'expenses_categories',
                    'heading'     => _l($sM->finance->children->expenses_categories->heading),
                    'name'     => _l($sM->finance->children->expenses_categories->label),
                    'label'     => _l($sM->finance->children->expenses_categories->label),
                    'href'     => admin_url('expenses/categories'),
                    'position' => 20,
            ]);

        $CI->app_menu->add_setup_menu_item('subscription', [
                    'href'     => admin_url('settings?group=subscriptions'),
                    'name'     => _l($sM->subscription->label),
                    'label'     => _l($sM->subscription->label),
                    'heading'     => _l($sM->subscription->heading),
                    'position' => 26,
            ]);

        $CI->app_menu->add_setup_menu_item('payment_gateways', [
                    'href'     => admin_url('settings?group=payment_gateways'),
                    'name'     => _l('Payment Gateway'),
                    'label'     => _l('Payment Gateway'),
                    'position' => 27,
            ]);

        $CI->app_menu->add_setup_menu_item('_sms', [
                    'href'     => admin_url('settings?group=sms'),
                    'name'     => _l('SMS'),
                    'label'     => _l('SMS'),
                    'position' => 28,
            ]);
            
        $CI->app_menu->add_setup_menu_item('leads', [
                    'collapse' => true,
                    'name'     => _l($sM->leads->label),
                    'label'     => _l($sM->leads->label),
                    'heading'     => _l($sM->leads->heading),
                    'position' => 20,
            ]);
        $CI->app_menu->add_setup_children_item('leads', [
                    'slug'     => 'leads_sources',
                    'name'     => _l($sM->leads->children->leads_sources->label),
                    'label'     => _l($sM->leads->children->leads_sources->label),
                    'heading'     => _l($sM->leads->children->leads_sources->heading),
                    'href'     => admin_url('leads/sources'),
                    'position' => 5,
            ]);
        $CI->app_menu->add_setup_children_item('leads', [
                    'slug'     => 'leads_statuses',
                    'name'     => _l($sM->leads->children->leads_statuses->label),
                    'label'     => _l($sM->leads->children->leads_statuses->label),
                    'heading'     => _l($sM->leads->children->leads_statuses->heading),
                    'href'     => admin_url('leads/statuses'),
                    'position' => 10,
            ]);
        $CI->app_menu->add_setup_children_item('leads', [
                    'slug'     => 'leads_email_integration',
                    'name'     => _l($sM->leads->children->leads_email_integration->label),
                    'label'     => _l($sM->leads->children->leads_email_integration->label),
                    'heading'     => _l($sM->leads->children->leads_email_integration->heading),
                    'href'     => admin_url('leads/email_integration'),
                    'position' => 15,
            ]);
        $CI->app_menu->add_setup_children_item('leads', [
                    'slug'     => 'web_to_lead',
                    'name'     => _l($sM->leads->children->web_to_lead->label),
                    'label'     => _l($sM->leads->children->web_to_lead->label),
                    'heading'     => _l($sM->leads->children->web_to_lead->heading),
                    'href'     => admin_url('leads/forms'),
                    'position' => 20,
            ]);
        
        $CI->app_menu->add_setup_menu_item('support', [
                    'collapse' => true,
                    'name'     => _l($sM->support->label),
                    'label'     => _l($sM->support->label),
                    'heading'     => _l($sM->support->heading),
                    'position' => 15,
            ]);

        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'departments',
                    'name'     => _l($sM->support->children->departments->label),
                    'label'     => _l($sM->support->children->departments->label),
                    'heading'     => _l($sM->support->children->departments->heading),
                    'href'     => admin_url('departments'),
                    'position' => 5,
            ]);
        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'tickets_predefined_replies',
                    'name'     => _l($sM->support->children->tickets_predefined_replies->label),
                    'label'     => _l($sM->support->children->tickets_predefined_replies->label),
                    'heading'     => _l($sM->support->children->tickets_predefined_replies->heading),
                    'href'     => admin_url('tickets/predefined_replies'),
                    'position' => 10,
            ]);
        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'tickets_priorities',
                    'name'     => _l($sM->support->children->tickets_priorities->label),
                    'label'     => _l($sM->support->children->tickets_priorities->label),
                    'heading'     => _l($sM->support->children->tickets_priorities->heading),
                    'href'     => admin_url('tickets/priorities'),
                    'position' => 15,
            ]);
        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'tickets_statuses',
                    'name'     => _l($sM->support->children->tickets_statuses->label),
                    'label'     => _l($sM->support->children->tickets_statuses->label),
                    'heading'     => _l($sM->support->children->tickets_statuses->heading),
                    'href'     => admin_url('tickets/statuses'),
                    'position' => 20,
            ]);

        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'tickets_services',
                    'name'     => _l($sM->support->children->tickets_services->label),
                    'label'     => _l($sM->support->children->tickets_services->label),
                    'heading'     => _l($sM->support->children->tickets_services->heading),
                    'href'     => admin_url('tickets/services'),
                    'position' => 25,
            ]);
        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'tickets_spam_filters',
                    'name'     => _l($sM->support->children->tickets_spam_filters->label),
                    'label'     => _l($sM->support->children->tickets_spam_filters->label),
                    'heading'     => _l($sM->support->children->tickets_spam_filters->heading),
                    'href'     => admin_url('spam_filters/view/tickets'),
                    'position' => 30,
            ]);
/*
        $CI->app_menu->add_setup_menu_item('support', [
                    'collapse' => true,
                    'name'     => _l('support'),
                    'position' => 15,
            ]);

        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'departments',
                    'name'     => _l('acs_departments'),
                    'href'     => admin_url('departments'),
                    'position' => 5,
            ]);
        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'tickets-predefined-replies',
                    'name'     => _l('acs_ticket_predefined_replies_submenu'),
                    'href'     => admin_url('tickets/predefined_replies'),
                    'position' => 10,
            ]);
        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'tickets-priorities',
                    'name'     => _l('acs_ticket_priority_submenu'),
                    'href'     => admin_url('tickets/priorities'),
                    'position' => 15,
            ]);
        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'tickets-statuses',
                    'name'     => _l('acs_ticket_statuses_submenu'),
                    'href'     => admin_url('tickets/statuses'),
                    'position' => 20,
            ]);

        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'tickets-services',
                    'name'     => _l('acs_ticket_services_submenu'),
                    'href'     => admin_url('tickets/services'),
                    'position' => 25,
            ]);
        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'tickets-spam-filters',
                    'name'     => _l('spam_filters'),
                    'href'     => admin_url('spam_filters/view/tickets'),
                    'position' => 30,
            ]);

        $CI->app_menu->add_setup_menu_item('leads', [
                    'collapse' => true,
                    'name'     => _l('acs_leads'),
                    'position' => 20,
            ]);
        $CI->app_menu->add_setup_children_item('leads', [
                    'slug'     => 'leads-sources',
                    'name'     => _l('acs_leads_sources_submenu'),
                    'href'     => admin_url('leads/sources'),
                    'position' => 5,
            ]);
        $CI->app_menu->add_setup_children_item('leads', [
                    'slug'     => 'leads-statuses',
                    'name'     => _l('acs_leads_statuses_submenu'),
                    'href'     => admin_url('leads/statuses'),
                    'position' => 10,
            ]);
        $CI->app_menu->add_setup_children_item('leads', [
                    'slug'     => 'leads-email-integration',
                    'name'     => _l('leads_email_integration'),
                    'href'     => admin_url('leads/email_integration'),
                    'position' => 15,
            ]);
        $CI->app_menu->add_setup_children_item('leads', [
                    'slug'     => 'web-to-lead',
                    'name'     => _l('web_to_lead'),
                    'href'     => admin_url('leads/forms'),
                    'position' => 20,
            ]);

        $CI->app_menu->add_setup_menu_item('contracts', [
                    'collapse' => true,
                    'name'     => _l('acs_contracts'),
                    'position' => 30,
            ]);
        $CI->app_menu->add_setup_children_item('contracts', [
                    'slug'     => 'contracts-types',
                    'name'     => _l('acs_contract_types'),
                    'href'     => admin_url('contracts/types'),
                    'position' => 5,
            ]);

        $modules_name = _l('modules');

        if ($modulesNeedsUpgrade = $CI->app_modules->number_of_modules_that_require_database_upgrade()) {
            $modules_name .= '<span class="badge menu-badge bg-warning">' . $modulesNeedsUpgrade . '</span>';
        }

        $CI->app_menu->add_setup_menu_item('modules', [
                    'href'     => admin_url('modules'),
                    'name'     => $modules_name,
                    'position' => 35,
            ]);

        $CI->app_menu->add_setup_menu_item('custom-fields', [
                    'href'     => admin_url('custom_fields'),
                    'name'     => _l('asc_custom_fields'),
                    'position' => 45,
            ]);

        $CI->app_menu->add_setup_menu_item('gdpr', [
                    'href'     => admin_url('gdpr'),
                    'name'     => _l('gdpr_short'),
                    'position' => 50,
            ]);
        */

        

/*             $CI->app_menu->add_setup_menu_item('api', [
                          'href'     => admin_url('api'),
                          'name'     => 'API',
                          'position' => 65,
                  ]);*/

    }

    /*

    if (has_permission('email_templates', '', 'view')) {
        $CI->app_menu->add_setup_menu_item('email-templates', [
                    'href'     => admin_url('emails'),
                    'name'     => _l('acs_email_templates'),
                    'position' => 40,
            ]);
    }
    */
}
