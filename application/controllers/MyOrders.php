<?php

defined('BASEPATH') or exit('No direct script access allowed');

use app\services\ValidatesContact;

class MyOrders extends ClientsController
{
    use ValidatesContact;
    
    public function __construct()
    {
        parent::__construct();
        hooks()->do_action('clients_authentication_constructor', $this);
        if(get_client_role_type() == 1)
        {
           redirect(site_url('garages'));
        }
        $this->load->library('session');
        /*
        $paidstatus = $this->db->order_by('payment_id', 'desc')->limit(1)->get_where(db_prefix().'_payments', array('garage_id' => get_client_user_id(), 'expiry_date >' => time(), 'item_number' => 1))->num_rows();
        if($paidstatus == 0)
        {
            set_alert('warning', _l('Plan expired'));
            redirect(site_url('planExpire'));
        }
        $freetrialstatus = $this->db->get_where(db_prefix().'_payments', array('garage_id' => get_client_user_id()))->num_rows();
        if($freetrialstatus == 0)
        {
            if(garagecreatedate(get_client_user_id()) == 1)
            {
                set_alert('warning', _l('Plan expired'));
                redirect(site_url('planExpire'));
            }
        }
        */
    }
    
    public function index()
    {
        $data['title']     = 'My Orders';
		//$data['orderResult'] = $this->db->order_by('id', 'desc')->get_where(db_prefix().'quoteRequest_garageReply', array('garage_id' => get_client_user_id(), 'remove_id' => 0, 'qty != ' => null))->result();
		$data['orderResult'] = $this->db->order_by('id', 'desc')->get_where(db_prefix().'schedule_appoinment', array('garage_id' => get_client_user_id(), 'status > ' => 2))->result();
        $this->data($data);
        $this->view('myOrders');
        $this->layout();
    }
    
    
    /**
    *   @function: removeProduct
    **/
    public function removeQuotes()
    {
        $id = $_POST['id'];
        if($id)
        {
            $this->db->delete(db_prefix().'_product', array('id' => $id));
        }
        echo 1;
    }
}
