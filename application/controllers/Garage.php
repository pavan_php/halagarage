<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Garage extends ClientsController
{
    public function __construct()
    {
        parent::__construct();
        // hooks()->do_action('clients_authentication_constructor', $this);
        $this->is_login = $this->session->userdata('is_login');
        $this->garage_id = $this->session->userdata('garage_id');
        $this->garage_name = $this->session->userdata('garage_name');
        $this->garage_email = $this->session->userdata('garage_email');
    }

    public function index()
    {
        if (is_client_logged_in()) {
            if(get_client_role_type() == 1)
            {
                redirect(site_url('customers'));
            }
            else
            {
                redirect(site_url('garages'));
            }
        }
        elseif ($this->is_login) {
            $data['title']            = $this->garage_name;
            $this->data($data);
            $this->view('home');
            $this->layout();
        }
        else
        {
            $data['settingRes'] = $this->db->get_where(db_prefix().'login_setting')->row();
            $data['title'] = 'HalaGarage';
            $data['video_home'] = $this->db->get_where(db_prefix().'video_section', array('id' => 1))->row();
            $data['activity_result'] = $this->db->get_where(db_prefix().'garage_type', array('status' => 1))->result();
            $this->load->view('front/index', $data);
        }
    }
    
    /******************************************************************************************
    *   @Function: getFeaturedGarage
    *******************************************************************************************/
    public function getFeaturedGarage()
    {
        $id = $_POST['id'];
        if($id > 0)
        {
            $featuredGarageId = [];
            $garageId = [];
            $featuredGarageList = $this->db->select('garage_id')->get_where(db_prefix().'_payments', array('expiry_date >' => time(), 'item_number' => 2))->result();
            if($featuredGarageList)
            {
                foreach($featuredGarageList as $r)
                {
                    array_push($featuredGarageId, $r->garage_id);
                }
            }
            
            $where = "FIND_IN_SET('".$id."', service_id)";
            $this->db->select('garage_id');
            $this->db->from(db_prefix().'garage_services');
            $this->db->where($where);
            $query = $this->db->get();
            $activiryResult = $query->result();
            
            if($activiryResult)
            {
                foreach($activiryResult as $s)
                {
                    array_push($garageId, $s->garage_id);
                }
            }
            
            $this->db->select('userid,company');
            $this->db->from(db_prefix().'clients');
            if(count($featuredGarageId) > 0)
            {
                $this->db->where_in('userid', $featuredGarageId);
            }
            $result = $this->db->get()->result();
            
            if($activiryResult)
            {
                $html = '';
                $html .= '<li onClick="getFeaturedGarage(0)"><a href="javascript:void(0);"> Back </a></li>';
                if($activiryResult)
                {
                    foreach($result as $rr)
                    {
                        if(in_array($rr->userid, $garageId))
                        {
                            $html .= '<li><a href="'.base_url().'garage/profile/'.$rr->userid.'" target="_blank">'.$rr->company.'</a></li>';
                        }
                    }
                }
                if($html != '')
                {
                    echo $html;
                }
                else
                {
                    echo '<li onClick="getFeaturedGarage(0)"><a href="javascript:void(0);"> Back </a></li><li class="dataTables_empty">No entries found</li>';
                }
            }
            else
            {
                echo '<li onClick="getFeaturedGarage(0)"><a href="javascript:void(0);"> Back </a></li><li class="dataTables_empty">No entries found</li>';
            }
        }
        else
        {
            $activity_result = $this->db->get_where(db_prefix().'garage_type', array('status' => 1))->result();
            if($activity_result)
            {
                foreach($activity_result as $rr)
                {
                    $html .= '<li onClick="getFeaturedGarage('.$rr->id.')"><a href="javascript:void(0);">'.$rr->name.'</a></li>';
                }
                echo $html;
            }
            else
            {
                echo '<li class="dataTables_empty">No entries found</li>';
            }
        }
    }

    /**
	*	@Function: Garage profile
	*/
	public function profile($id)
    {
        $data['profileData'] = $this->db->get_where(db_prefix().'clients', array('userid' => $id))->row();
        $data['garageAbountRes'] = $this->db->get_where(db_prefix().'garage_aboutus', array('garage_id' => $id))->row();
        $data['profileBackgroundRes'] = $this->db->get_where(db_prefix().'profile_background', array('garage_id' => $id))->row();
        $data['newsfeedResult'] = $this->db->order_by('id', 'desc')->get_where(db_prefix().'garage_newsfeed', array('garage_id' => $id))->result();
        $data['latestProduct'] = $this->db->order_by('id', 'desc')->limit(3)->get_where(db_prefix().'_product', array('garage_id' => $id))->result();
        $data['garageServicesRes'] = $this->db->order_by('id', 'desc')->limit(3)->get_where(db_prefix().'garage_services', array('garage_id' => $id))->row('service_id');
        $data['garageGalleryRes'] = $this->db->order_by('id', 'desc')->limit(3)->get_where(db_prefix().'garage_gallery', array('garage_id' => $id))->result();
        //echo '<pre>'; print_r($data['garageAbountRes']); die; 
        $data['serviceList'] = $this->db->get_where(db_prefix().'garage_type', array('status' => 1))->result();
        $data['garageRating'] = $this->db->select('AVG(garage_rating) as rating')->get_where(db_prefix().'review_rating', array('garage_id' => $id))->result(); 
        $data['garageRating_count'] = $this->db->get_where(db_prefix().'review_rating', array('garage_id' => $id))->num_rows(); 
        $data['title'] = _l($data['profileData']->company);
        $this->data($data);
        $this->view('garageProfile');
        $this->layout();
    }

    // Added for backward compatibilies
    public function admin()
    {
        redirect(admin_url('authentication'));
    }
    
    /* News Letter */
    public function newsletter()
    {
        $response = [];
        $this->form_validation->set_rules('email', _l('clients_login_email'), 'trim|required|valid_email');
        if ($this->form_validation->run() !== false) {
            $email = $_POST['email'];
            $result = $this->db->get_where(db_prefix().'newsletter', array('email' => $email))->num_rows();
            if($result == 0)
            {
                $data['email'] = $email;
                $this->db->insert(db_prefix().'newsletter', $data);
                $this->db->insert_id();
                $response = array(
                        'status' => 'success',
                        'msg' => $email.' is subscribed'
                    );
            }
            else
            {
                $response = array(
                        'status' => 'error',
                        'msg' => $email.' is already subscribed'
                    );
            }
        }
        else
        {
            $response = array(
                'status' => 'error',
                'msg' => 'Invalid email, try again'
                );
        }
        echo json_encode($response);
    }
    
    /* Function: Abountus */
    public function aboutUs()
    {
        $data['title'] = 'About-us';
        $data['content'] = $this->db->get_where(db_prefix().'content', array('id' => 1))->row();
        $this->load->view('front/about-us', $data);
    }
    
    /* Function: faq */
    public function faq()
    {
        $data['title'] = 'FAQ`s';
        $data['content'] = $this->db->get_where(db_prefix().'content', array('id' => 5))->row();
        $this->load->view('front/faq', $data);
    }
    /* Function: services */
    public function services()
    {
        $data['title'] = 'Services List';
        $data['content'] = $this->db->get_where(db_prefix().'content', array('id' => 5))->row();
        $this->load->view('front/services-list', $data);
    }
    /* Function: faq */
    public function privacy_policy()
    {
        $data['title'] = 'Privacy-Policy';
        $data['content'] = $this->db->get_where(db_prefix().'content', array('id' => 3))->row();
        $this->load->view('front/privacy-policy', $data);
    }
    
    /* Function: faq */
    public function blog()
    {
        $data['title'] = 'Blogs';
        $data['content'] = $this->db->get_where(db_prefix().'content', array('id' => 6))->row();
        $this->load->view('front/blog', $data);
    }
    
    /* Function: Contact Us */
    public function contactUs()
    {
        $data['title'] = 'Contact-us';
        $data['content'] = $this->db->get_where(db_prefix().'content', array('id' => 2))->row();
        $this->load->view('front/contact-us', $data);
    }

    /* Function: findCarPart */
    public function findCarPart()
    {
        $data['title'] = 'Find Car Part';
        $this->load->view('front/find-car-part', $data);
    }

    /* Function: getAQuote */
    public function getAQuote()
    {
        $data['title'] = 'Get A Quote';
        $this->load->view('front/get-a-quote', $data);
    }

    /* Function: termsAndConditions */
    public function termsAndConditions()
    {
        $data['title'] = 'Terms And Conditions';
        $data['content'] = $this->db->get_where(db_prefix().'content', array('id' => 4))->row();
        $this->load->view('front/terms-and-conditions', $data);
    }

    /* Function: requestNewQuote */
    public function requestNewQuote()
    {
        $data['title'] = 'Request New Quote';
        $this->load->view('front/request-new-quote', $data);
    }

    public function login()
    {
        /*
        if ($this->is_login == true) {
            redirect(site_url());
        }
*/
        $this->form_validation->set_rules('password', _l('clients_login_password'), 'required');
        $this->form_validation->set_rules('email', _l('clients_login_email'), 'trim|required|valid_email');

        if (get_option('use_recaptcha_customers_area') == 1
            && get_option('recaptcha_secret_key') != ''
            && get_option('recaptcha_site_key') != '') {
            $this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'callback_recaptcha');
        }
        if ($this->form_validation->run() !== false) {
            /*
            $email = $this->input->post('email');
            $password = md5($this->input->post('password'));
            
            $garageres = $this->db->get_where(db_prefix().'garage', array('email' => $email, 'password' => $password))->result();
            if(count($garageres) > 0)
            {
                $this->session->set_userdata('is_login', true);
                $this->session->set_userdata('garage_id', $garageres[0]->id);
                $this->session->set_userdata('garage_name', $garageres[0]->name);
                $this->session->set_userdata('garage_email', $garageres[0]->email);
                redirect(site_url('garage'));
            }
            else
            {
                $data['msg'] = '<span text-danger>login faild</span>';
                $this->load->view('front/garage-login', $data);
            }
            */
            $this->load->model('Authentication_model');

            $success = $this->Authentication_model->login(
                $this->input->post('email'),
                $this->input->post('password', false),
                $this->input->post('remember'),
                false
            );

            if (is_array($success) && isset($success['memberinactive'])) {
                set_alert('danger', _l('inactive_account'));
                redirect(site_url('login'));
            } elseif ($success == false) {
                set_alert('danger', _l('client_invalid_username_or_password'));
                redirect(site_url('login'));
            }

            $this->load->model('announcements_model');
            $this->announcements_model->set_announcements_as_read_except_last_one(get_contact_user_id());

            hooks()->do_action('after_contact_login');

            maybe_redirect_to_previous_url();
            
            redirect(site_url('clients'));
        }
        if (get_option('allow_registration') == 1) {
            $data['title'] = _l('clients_login_heading_register');
        } else {
            $data['title'] = _l('clients_login_heading_no_register');
        }
        $data['bodyclass'] = 'customers_login';
        
        $data['settingRes'] = $this->db->get_where('tbllogin_setting')->row();

        $this->load->view('front/garage-login', $data);
    }

    public function register()
    {
        /*
        if (get_option('allow_registration') != 1 || is_client_logged_in()) {
            redirect(site_url());
        }

        if (get_option('company_is_required') == 1) {
            $this->form_validation->set_rules('company', _l('client_company'), 'required');
        }

        if (is_gdpr() && get_option('gdpr_enable_terms_and_conditions') == 1) {
            $this->form_validation->set_rules(
                'accept_terms_and_conditions',
                _l('terms_and_conditions'),
                'required',
                    ['required' => _l('terms_and_conditions_validation')]
            );
        }
        */
        $this->form_validation->set_rules('name', _l('garage name'), 'required');
        //$this->form_validation->set_rules('lastname', _l('client_lastname'), 'required');
        //$this->form_validation->set_rules('email', _l('client_email'), 'trim|required|is_unique[' . db_prefix() . 'garage.email]|valid_email');
        $this->form_validation->set_rules('email', _l('client_email'), 'trim|required|is_unique[tblclients.company_email]|valid_email');
        $this->form_validation->set_rules('email', _l('client_email'), 'trim|required|is_unique[tblcontacts.email]|valid_email');
        $this->form_validation->set_rules('password', _l('clients_register_password'), 'required');
        
        $this->form_validation->set_rules('passwordr', _l('clients_register_password_repeat'), 'required|matches[password]');

        if ($this->input->post()) {
            if ($this->form_validation->run() !== false) {                                                   
                $data = $this->input->post();
                
                /*
                $postData['email'] = $data['email']; 
                $postData['password'] = md5($data['password']); 
                $postData['name'] = $data['name']; 
                $postData['fax'] = $data['fax']; 
                $postData['company_email'] = $data['company_email']; 
                $postData['website_url'] = $data['website_url']; 
                $postData['city_emirate'] = $data['city_emirate']; 
                $postData['address'] = $data['address']; 
                $postData['latitude'] = $data['latitude']; 
                $this->db->insert(db_prefix().'garage', $postData);
                $lastid = $this->db->insert_id();
                if($lastid)
                {
                    redirect(site_url('garage/login'));
                }
                */
                /*
                $clientdata['company'] = $data['name']; 
                $clientdata['city'] = $data['city_emirate']; 
                $clientdata['fax'] = $data['fax']; 
                $clientdata['company_email'] = $data['company_email']; 
                $clientdata['website'] = $data['website_url']; 
                $clientdata['address'] = $data['address']; 
                $clientdata['active'] = 1; 
                $clientdata['role'] = 2; 
                $clientdata['registration_confirmed'] = 1; 
                $clientdata['datecreated'] = date('Y-m-d H:i:s');
                
                $this->db->insert(db_prefix() . 'clients', $clientdata);
                $userid = $this->db->insert_id();
                
                $user_data['userid'] = $userid;
                $user_data['firstname'] = $data['name'];
                $user_data['email'] = $data['email'];
                $user_data['password'] = app_hash_password(@$data['password']);
                $user_data['active'] = 1;
                $user_data['datecreated'] = date('Y-m-d H:i:s');
                $user_data['email_verified_at'] = date('Y-m-d H:i:s');
                $password_before_hash = @$data['password'];
                
                $this->db->insert(db_prefix() . 'contacts', $user_data);
                $contact_id = $this->db->insert_id();
                if ($user_data['email'] != '') {
                    send_mail_template('customer_created_welcome_mail', $user_data['email'], $userid, $contact_id, $password_before_hash);
                }
    
                if (defined('CONTACT_REGISTERING')) {
                    $this->send_verification_email($contact_id);
                }
                */
               // echo '<pre>'; print_r($data); die;
                $clientid = $this->clients_model->add_garage($data, true);

                if ($clientid) {
                    hooks()->do_action('after_client_register', $clientid);

                    if (get_option('customers_register_require_confirmation') == '1') {
                        send_customer_registered_email_to_administrators($clientid);

                        $this->clients_model->require_confirmation($clientid);
                        set_alert('success', _l('customer_register_account_confirmation_approval_notice'));
                        redirect(site_url('authentication/login'));
                    }

                    $this->load->model('authentication_model');

                    $logged_in = $this->authentication_model->login(
                        $this->input->post('email'),
                        $this->input->post('password', false),
                        false,
                        false
                    );

                    $redUrl = site_url();

                    if ($logged_in) {
                        hooks()->do_action('after_client_register_logged_in', $clientid);
                        set_alert('success', _l('clients_successfully_registered'));
                    } else {
                        set_alert('warning', _l('clients_account_created_but_not_logged_in'));
                        $redUrl = site_url('authentication/login');
                    }

                    send_customer_registered_email_to_administrators($clientid);
                    redirect($redUrl);
                }
            }
        }

        $data['title']     = _l('clients_register_heading');
        /*
        $data['bodyclass'] = 'register';
        $this->data($data);
        $this->view('register');
        $this->layout();
        */
        $data['city_result'] = $this->db->get_where(db_prefix().'city', array('status' => 1))->result();
        $data['ServiceResult'] = $this->db->get_where(db_prefix().'garage_type', array('status' => 1))->result();
        
        $data['settingRes'] = $this->db->get_where('tbllogin_setting')->row();
        $this->load->view('front/garage-register', $data);
    }

    /**
    *   Function: checkemailvalidation
    */
    public function checkemailvalidation()
    {
        $email = $_POST['email'];
        $html = '';
        if($email)
        {
            $resp = $this->db->get_where(db_prefix().'contacts', array('email' => $email))->num_rows();
            if($resp > 0)
            {
                $html = 1;
            }
            $response = $this->db->get_where(db_prefix().'clients', array('company_email' => $email))->num_rows();
            if($response > 0)
            {
                $html = 1;
            }
        }
        else
        {
            $html = '';
        }
        echo $html;
    }

    /**
    *   @Function: getAreaList
    */
    public function getAreaList()
    {
        $catid = $_POST['catid'];
        $html = '';
        if($catid)
        {
            $html = $this->db->get_where(db_prefix().'area', array('city_id' => $catid))->result();
        }
        echo json_encode($html);
    }

    public function forgot_password()
    {
        if ($this->is_login) {
            redirect(site_url());
        }

        $this->form_validation->set_rules(
            'email',
            _l('customer_forgot_password_email'),
            'trim|required|valid_email'
        );

        if ($this->input->post()) {
            if ($this->form_validation->run() !== false) {
                $this->load->model('Authentication_model');
                $success = $this->Authentication_model->forgot_password_garage($this->input->post('email'));
                if (is_array($success) && isset($success['memberinactive'])) {
                    set_alert('danger', _l('inactive_account'));
                } elseif ($success == true) {
                    set_alert('success', _l('check_email_for_resetting_password'));
                } else {
                    set_alert('danger', _l('error_setting_new_password_key'));
                }
                redirect(site_url('garage/forgot_password'));
            }
        }
        /*
        $data['title'] = _l('customer_forgot_password');
        $this->data($data);
        $this->view('forgot_password');

        $this->layout();
        */
        $data['title'] = 'Garage Forgot Password';
        $data['settingRes'] = $this->db->get_where('tbllogin_setting')->row();
        $this->load->view('front/garage-forgot-password', $data);
    }

    public function reset_password($staff, $userid, $new_pass_key)
    {
        $this->load->model('Authentication_model');
        if (!$this->Authentication_model->can_reset_password($staff, $userid, $new_pass_key)) {
            set_alert('danger', _l('password_reset_key_expired'));
            redirect(site_url('authentication/login'));
        }

        $this->form_validation->set_rules('password', _l('customer_reset_password'), 'required');
        $this->form_validation->set_rules('passwordr', _l('customer_reset_password_repeat'), 'required|matches[password]');
        if ($this->input->post()) {
            if ($this->form_validation->run() !== false) {
                hooks()->do_action('before_user_reset_password', [
                    'staff'  => $staff,
                    'userid' => $userid,
                ]);
                $success = $this->Authentication_model->reset_password(
                        0,
                        $userid,
                        $new_pass_key,
                        $this->input->post('passwordr', false)
                );
                if (is_array($success) && $success['expired'] == true) {
                    set_alert('danger', _l('password_reset_key_expired'));
                } elseif ($success == true) {
                    hooks()->do_action('after_user_reset_password', [
                        'staff'  => $staff,
                        'userid' => $userid,
                    ]);
                    set_alert('success', _l('password_reset_message'));
                } else {
                    set_alert('danger', _l('password_reset_message_fail'));
                }
                redirect(site_url('authentication/login'));
            }
        }
        $data['settingRes'] = $this->db->get_where('tbllogin_setting')->row();
        $data['title'] = _l('admin_auth_reset_password_heading');
        $this->data($data);
        $this->view('reset_password');
        $this->layout();
    }

    public function logout()
    {
        $this->session->unset_userdata('client_user_id');
        $this->session->unset_userdata('client_logged_in');
        $this->session->sess_destroy();
        redirect(site_url());
    }

    public function contact_email_exists($email = '')
    {
        $this->db->where('email', $email);
        $total_rows = $this->db->count_all_results(db_prefix() . 'contacts');

        if ($total_rows == 0) {
            $this->form_validation->set_message('contact_email_exists', _l('auth_reset_pass_email_not_found'));

            return false;
        }

        return true;
    }

    public function recaptcha($str = '')
    {
        return do_recaptcha_validation($str);
    }
}
