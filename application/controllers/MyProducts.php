<?php

defined('BASEPATH') or exit('No direct script access allowed');

use app\services\ValidatesContact;

class MyProducts extends ClientsController
{
    use ValidatesContact;

    public function __construct()
    {
        parent::__construct();
        hooks()->do_action('after_clients_area_init', $this);
        if(get_client_role_type() == 1)
        {
            redirect(site_url('customers'));
        }
        /*
        $paidstatus = $this->db->order_by('payment_id', 'desc')->limit(1)->get_where(db_prefix().'_payments', array('garage_id' => get_client_user_id(), 'expiry_date >' => time(), 'item_number' => 1))->num_rows();
        if($paidstatus == 0)
        {
            set_alert('warning', _l('Plan expired'));
            redirect(site_url('planExpire'));
        }
        $freetrialstatus = $this->db->get_where(db_prefix().'_payments', array('garage_id' => get_client_user_id()))->num_rows();
        if($freetrialstatus == 0)
        {
            if(garagecreatedate(get_client_user_id()) == 1)
            {
                set_alert('warning', _l('Plan expired'));
                redirect(site_url('planExpire'));
            }
        }
        */
    }
    
    public function index()
    {
        $data['title']          = 'My Products';
        $data['product_result'] = $this->db->order_by('id', 'desc')->get_where(db_prefix().'_product', array('garage_id' => get_client_user_id()))->result();
        $data['carmake_result'] = $this->db->get_where(db_prefix().'car_make', array('status' => 1))->result();
        $this->data($data);
        $this->view('myProducts');
        $this->layout();
    }
    
    public function searchfillter()
    {
        $make_id        =       $_POST['make_id'];
        $model_id       =       $_POST['model_id'];
        $year_id        =       $_POST['year_id'];
        $data['carmake_result'] = $this->db->get_where(db_prefix().'car_make', array('status' => 1))->result();
        $this->db->select('*');
        $this->db->from('tbl_product');
        $this->db->where('garage_id', get_client_user_id());
        if($make_id != '')
        {
            $where = "FIND_IN_SET('".$make_id."', car_brand)";
            $this->db->where( $where );
            //$data['product_result'] = $this->db->order_by('id', 'desc')->get_where(db_prefix().'_product', array('garage_id' => get_client_user_id()))->result();
        }
        if($model_id != '')
        {
            $where_ = "FIND_IN_SET('".$model_id."', car_model)";
            $this->db->where( $where_ );
            //$data['product_result'] = $this->db->order_by('id', 'desc')->get_where(db_prefix().'_product', array('garage_id' => get_client_user_id()))->result();
        }
        if($year_id != '')
        {
            $where__ = "FIND_IN_SET('".$year_id."', make_year)";
            $this->db->where( $where__ );
            //$data['product_result'] = $this->db->order_by('id', 'desc')->get_where(db_prefix().'_product', array('garage_id' => get_client_user_id()))->result();
        }
        $this->db->order_by('id', 'desc');
        $data['product_result'] = $this->db->get()->result();
        echo $this->load->view('themes/perfex/searchfillter.php', $data, true);
    }
    
    public function publishProduct()
    {
        $last_id = $this->db->limit(1)->order_by('id', 'desc')->get_where(db_prefix().'_product')->row('id');
        $product_id = $last_id + 1;
        if ($this->input->post()) {
            $this->form_validation->set_rules('product_name', _l('product_name'), 'required');
            $this->form_validation->set_rules('category', _l('category'), 'required');
            //$this->form_validation->set_rules('car_category', _l('car_category'), 'required');
            //$this->form_validation->set_rules('car_brand', _l('car_brand'), 'required');
            //$this->form_validation->set_rules('car_model[]', _l('car_model'), 'required');
            //$this->form_validation->set_rules('make_year[]', _l('make_year'), 'required');
            $this->form_validation->set_rules('product_brand', _l('product_brand'), 'required');
            $this->form_validation->set_rules('description', _l('description'), 'required');
            //$this->form_validation->set_rules('price_with_fitting', _l('price with fitting'), 'required');
            //$this->form_validation->set_rules('price_without_fitting', _l('price without fitting'), 'required');
            /*
            $this->form_validation->set_rules('price_before_offer', _l('price before offer'), 'required');
            $this->form_validation->set_rules('price_after_offer', _l('price after offer'), 'required');
            $this->form_validation->set_rules('offer_start_date', _l('offer start date'), 'required');
            $this->form_validation->set_rules('offer_end_date', _l('offer end date'), 'required');
            */
            if ($this->form_validation->run() !== false) {
                $data = $this->input->post();
                $postdata['product_name'] = $data['product_name'];
                $postdata['category'] = $data['category'];
                $postdata['subcategory'] = implode(',',$data['subcategory']);
                $postdata['car_category'] = implode(',',$data['car_category']);
                /*$postdata['car_brand'] = $data['car_brand'];*/
                $postdata['car_brand'] = implode(',',$data['car_brand']);
                $postdata['car_model'] = implode(',',$data['car_model']);
                $postdata['make_year'] = implode(',',$data['make_year']);
                $postdata['tyre_brand'] = @$data['tyre_brand'];
                $postdata['width'] = @$data['width'];
                $postdata['height'] = @$data['height'];
                $postdata['rim_size'] = @$data['rim_size'];
                $postdata['product_brand'] = $data['product_brand'];
                $postdata['description'] = $data['description'];
                $postdata['fitting_filter'] = $data['fitting_filter'];
                $postdata['price_with_fitting'] = $data['price_with_fitting'];
                $postdata['price_without_fitting'] = $data['price_without_fitting'];
                $postdata['price_before_offer'] = $data['price_before_offer'];
                $postdata['price_after_offer'] = $data['price_after_offer'];
                $postdata['offer_start_date'] = strtotime($data['offer_start_date']);
                $postdata['offer_end_date'] = strtotime($data['offer_end_date']);
                
                $postdata['price_before_offer_without'] = $data['price_before_offer_without'];
                $postdata['price_after_offer_without'] = $data['price_after_offer_without'];
                $postdata['offer_start_date_without'] = strtotime($data['offer_start_date_without']);
                $postdata['offer_end_date_without'] = strtotime($data['offer_end_date_without']);
                $postdata['created_at'] = date('Y-m-d h:i:s');
                
                if($data['submit'] == 'save')
                {
                    $postdata['status'] = 1;
                }
                else
                {
                    $postdata['status'] = 2;
                }
                $postdata['garage_id'] = get_client_user_id();
                //echo '<pre>'; print_r($postdata); die;
                $this->db->insert(db_prefix().'_product', $postdata);
                $lid = $this->db->insert_id();
                
                if($lid)
                {
                    $uploadedFiles = handle_file_array($lid,'product', 'product');
                    if ($uploadedFiles && is_array($uploadedFiles)) {
                        foreach ($uploadedFiles as $file) {
                            $this->misc_model->add_attachment_to_database($lid, 'product', [$file]);
                        }
                    }
                    
                    for($i=1; $i<7; $i++)
                    {
                        if($_FILES['productimg'.$i]['name'] != '')
                        {
                            $uploadedFiles = product_image_upload($i,$lid,'productimg'.$i);
                            if ($uploadedFiles && is_array($uploadedFiles)) {
                                foreach ($uploadedFiles as $file) {
                                    $this->misc_model->add_attachment_to_database($lid, 'productimg'.$i, [$file]);
                                }
                            }   
                        }
                    }
                    
                    set_alert('success', _l('Product added successful', $lid));
                    redirect(site_url('myProducts'));
                }
                else
                {
                    set_alert('warning', _l('access_denied'));
                    redirect(site_url('myProducts/publishProduct'));
                }
            }
        }
        $data['category_result'] = $this->db->get_where(db_prefix().'category', array('status' => 1, 'parent_id' => 0))->result();
        $data['carmake_result'] = $this->db->order_by('name', 'asc')->get_where(db_prefix().'car_make', array('status' => 1))->result();
        $data['tyrebrand_result'] = $this->db->order_by('name', 'asc')->get_where(db_prefix().'tyre_brand', array('status' => 1))->result();
        $data['city_result'] = $this->db->get_where(db_prefix().'city', array('status' => 1))->result();
        $data['title']     = 'Publish Product';
        $data['product_id'] = $product_id;
        $this->data($data);
        $this->view('publishProduct');
        $this->layout();
    }
    
    /**
    *   @function @editProduct
    */
    public function editProduct($id = '')
    {
        if($id)
        {
            if ($this->input->post()) {
                $this->form_validation->set_rules('product_name', _l('product_name'), 'required');
                $this->form_validation->set_rules('category', _l('category'), 'required');
                //$this->form_validation->set_rules('car_category', _l('car_category'), 'required');
                //$this->form_validation->set_rules('car_brand', _l('car_brand'), 'required');
                //$this->form_validation->set_rules('car_model[]', _l('car_model'), 'required');
                //$this->form_validation->set_rules('make_year[]', _l('make_year'), 'required');
                $this->form_validation->set_rules('product_brand', _l('product_brand'), 'required');
                $this->form_validation->set_rules('description', _l('description'), 'required');
                //$this->form_validation->set_rules('price_with_fitting', _l('price with fitting'), 'required');
                //$this->form_validation->set_rules('price_without_fitting', _l('price without fitting'), 'required');
                /*
                $this->form_validation->set_rules('price_before_offer', _l('price before offer'), 'required');
                $this->form_validation->set_rules('price_after_offer', _l('price_after_offer'), 'required');
                $this->form_validation->set_rules('offer_start_date', _l('offer start date'), 'required');
                $this->form_validation->set_rules('offer_end_date', _l('offer end date'), 'required');
                */
                if ($this->form_validation->run() !== false) {
                    $data = $this->input->post();
                    $postdata['product_name'] = $data['product_name'];
                    $postdata['category'] = $data['category'];
                    $postdata['subcategory'] = implode(',',$data['subcategory']);
                    $postdata['car_category'] = implode(',',$data['car_category']);
                    /*$postdata['car_brand'] = $data['car_brand'];*/
                    $postdata['car_brand'] = implode(',',$data['car_brand']);
                    $postdata['car_model'] = implode(',',$data['car_model']);
                    $postdata['make_year'] = implode(',',$data['make_year']);
                    $postdata['tyre_brand'] = $data['tyre_brand'];
                    $postdata['width'] = $data['width'];
                    $postdata['height'] = $data['height'];
                    $postdata['rim_size'] = $data['rim_size'];
                    $postdata['product_brand'] = $data['product_brand'];
                    $postdata['description'] = $data['description'];
                    $postdata['fitting_filter'] = $data['fitting_filter'];
                    $postdata['price_with_fitting'] = $data['price_with_fitting'];
                    $postdata['price_without_fitting'] = $data['price_without_fitting'];
                    $postdata['price_before_offer'] = $data['price_before_offer'];
                    $postdata['price_after_offer'] = $data['price_after_offer'];
                    $postdata['offer_start_date'] = strtotime($data['offer_start_date']);
                    $postdata['offer_end_date'] = strtotime($data['offer_end_date']);
                    
                    $postdata['price_before_offer_without'] = $data['price_before_offer_without'];
                    $postdata['price_after_offer_without'] = $data['price_after_offer_without'];
                    $postdata['offer_start_date_without'] = strtotime($data['offer_start_date_without']);
                    $postdata['offer_end_date_without'] = strtotime($data['offer_end_date_without']);
                    
                    //$postdata['created_at'] = date('Y-m-d h:i:s');
                    if($data['submit'] == 'save')
                    {
                        $postdata['status'] = 1;
                    }
                    else
                    {
                        $postdata['status'] = 2;
                    }
                    //$postdata['garage_id'] = get_client_user_id();
                    //$this->db->insert(db_prefix().'_product', $postdata);
                    
                    //$id = $this->db->insert_id();
                    $this->db->where('id', $id);
                    $this->db->update(db_prefix().'_product', $postdata);
                    
                    if($id)
                    {
                        if($_FILES['product']['name'] != '')
                        {
                            $this->db->where('rel_id', $id);
                            $this->db->where('rel_type', 'product');
                            $attachment = $this->db->get(db_prefix() . 'files')->row();
                    
                            if ($attachment) {
                                if (empty($attachment->external)) {
                                    $relPath  = 'uploads/product'.$attachment->rel_id . '/';
                                    $fullPath = $relPath . $attachment->file_name;
                                    unlink($fullPath);
                                }
                    
                                $this->db->where('id', $attachment->id);
                                $this->db->delete(db_prefix() . 'files');
                                if ($this->db->affected_rows() > 0) {
                                    $deleted = true;
                                    //log_activity('audio_book Attachment Deleted [audio_bookID: ' . $attachment->rel_id . ']');
                                }
                            }
                            
                            $uploadedFiles = handle_file_array($id,'product', 'product');
                            if ($uploadedFiles && is_array($uploadedFiles)) {
                                foreach ($uploadedFiles as $file) {
                                    $this->misc_model->add_attachment_to_database($id, 'product', [$file]);
                                }
                            }
                        }
                          
                        for($i=1; $i<7; $i++)
                        {
                            if($_FILES['productimg'.$i]['name'] != '')
                            {
                                $uploadedFiles = product_image_upload($i,$id,'productimg'.$i);
                                if ($uploadedFiles && is_array($uploadedFiles)) {
                                    foreach ($uploadedFiles as $file) {
                                        $this->misc_model->add_attachment_to_database($id, 'productimg'.$i, [$file]);
                                    }
                                }   
                            }
                        }
                           
                        set_alert('success', _l('Product updated successful', $id));
                        redirect(site_url('myProducts'));
                    }
                    else
                    {
                        set_alert('warning', _l('access_denied'));
                        redirect(site_url('myProducts/publishProduct/'.$id));
                    }
                }
            }
            $data['productedit_result'] = $this->db->get_where(db_prefix().'_product', array('id' => $id))->row();
            $data['category_result'] = $this->db->get_where(db_prefix().'category', array('status' => 1, 'parent_id' => 0))->result();
            $data['carmake_result'] = $this->db->order_by('name', 'asc')->get_where(db_prefix().'car_make', array('status' => 1))->result();
            $data['tyrebrand_result'] = $this->db->order_by('name', 'asc')->get_where(db_prefix().'tyre_brand', array('status' => 1))->result();
            $data['city_result'] = $this->db->get_where(db_prefix().'city', array('status' => 1))->result();
            $data['title']     = 'Publish Product';
            $data['product_id'] = $id;
            $this->data($data);
            $this->view('publishEditProduct');
            $this->layout();
        }
        else
        {
            set_alert('warning', _l('access_denied'));
            redirect(site_url('myProducts/publishProduct'));
        }
    }
    
    /**
    * @Function: viewProduct
    **/
    public function viewProduct($id)
    {
        if($id)
        {
            $data['productedit_result'] = $this->db->get_where(db_prefix().'_product', array('id' => $id))->row();
            $data['category_result'] = $this->db->get_where(db_prefix().'category', array('status' => 1, 'parent_id' => 0))->result();
            $data['carmake_result'] = $this->db->get_where(db_prefix().'car_make', array('status' => 1))->result();
            $data['tyrebrand_result'] = $this->db->get_where(db_prefix().'tyre_brand', array('status' => 1))->result();
            $data['city_result'] = $this->db->get_where(db_prefix().'city', array('status' => 1))->result();
            $data['title']     = 'Product Details';
            $data['product_id'] = $id;
            $this->data($data);
            $this->view('publishViewProduct');
            $this->layout();
        }
        else
        {
            set_alert('warning', _l('access_denied'));
            redirect(site_url('myProducts/publishProduct'));
        }
    }
    
    /**
    *   @function: removeProduct
    **/
    public function removeProduct()
    {
        $id = $_POST['id'];
        if($id)
        {
            $this->db->delete(db_prefix().'_product', array('id' => $id));
        }
        echo 1;
    }
    
    /**
    *   Function: getSubCategory
    */
    public function getSubCategory()
    {
        $catid = $_POST['catid'];
        $html = '';
        if($catid)
        {
            $html = $this->db->get_where(db_prefix().'category', array('parent_id' => $catid))->result();
        }
        echo json_encode($html);
    }
    
    /**
    *   Function: getMultiModelList
    */
    public function getMultiModelList()
    {
        $catid = $_POST['catid'];
        $where_in = explode(',', $catid);
        
        $html = '';
        if($catid)
        {
            $this->db->select('*');
            $this->db->from('car_model');
            $this->db->where_in('make_id',$where_in);
            $query = $this->db->get();
            $html = $query->result();
        }
        echo json_encode($html);
    }
    
    /**
    *   Function: getMultiYearList
    */
    public function getMultiYearList()
    {
        $catid = $_POST['catid'];
        $where_in = explode(',', $catid);
        $makeid = $_POST['make_id'];
        $make_id = explode(',', $makeid);
        $html = '';
        if($catid)
        {
            $this->db->select('*');
            $this->db->from('car_year');
            $this->db->where_in('make_id', $make_id);
            $this->db->where_in('model_id',$where_in);
            $query = $this->db->get();
            $html = $query->result();
            
            //$html = $this->db->get_where(db_prefix().'car_year', array('model_id' => $catid, 'make_id' => $make_id))->result();
        }
        echo json_encode($html);
    }
    
    /**
    *   Function: getModelList
    */
    public function getModelList()
    {
        $catid = $_POST['catid'];
        $html = '';
        if($catid)
        {
            $html = $this->db->get_where(db_prefix().'car_model', array('make_id' => $catid))->result();
        }
        echo json_encode($html);
    }
    
    /**
    *   Function: getYearList
    */
    public function getYearList()
    {
        $catid = $_POST['catid'];
        $where_in = explode(',', $catid);
        $make_id = $_POST['make_id'];
        $html = '';
        if($catid)
        {
            $this->db->select('*');
            $this->db->from('car_year');
            $this->db->where('make_id', $make_id);
            $this->db->where_in('model_id',$where_in);
            $query = $this->db->get();
            $html = $query->result();
            
            //$html = $this->db->get_where(db_prefix().'car_year', array('model_id' => $catid, 'make_id' => $make_id))->result();
        }
        echo json_encode($html);
    }
    
    /**
    *   Function: getWidthList
    */
    public function getWidthList()
    {
        $catid = $_POST['catid'];
        $html = '';
        if($catid)
        {
            $html = $this->db->get_where(db_prefix().'tyre_width', array('brand_id' => $catid))->result();
        }
        echo json_encode($html);
    }
    
    /**
    *   Function: getHeightList
    */
    public function getHeightList()
    {
        $catid = $_POST['catid'];
        $brand_id = $_POST['brand_id'];
        $html = '';
        if($catid)
        {
            $html = $this->db->get_where(db_prefix().'tyre_profile', array('width_id' => $catid, 'brand_id' => $brand_id))->result();
        }
        echo json_encode($html);
    }
    
    /**
    *   Function: getRimList
    */
    public function getRimList()
    {
        $catid = $_POST['catid'];
        $brand_id = $_POST['brand_id'];
        $width_id = $_POST['width_id'];
        $html = '';
        if($catid)
        {
            $html = $this->db->get_where(db_prefix().'tyre_diameter', array('profile_id' => $catid, 'brand_id' => $brand_id, 'width_id' => $width_id))->result();
        }
        echo json_encode($html);
    }
    
    
    /**
    *   Function: getAreaList
    */
    public function getAreaList()
    {
        $catid = $_POST['catid'];
        $html = '';
        if($catid)
        {
            $html = $this->db->get_where(db_prefix().'area', array('city_id' => $catid))->result();
        }
        echo json_encode($html);
    }
}
