<?php

defined('BASEPATH') or exit('No direct script access allowed');

use app\services\ValidatesContact;

class Payment extends ClientsController
{
    /**
     * @since  2.3.3
     */
    use ValidatesContact;

    public function __construct()
    {
        parent::__construct();
        hooks()->do_action('after_clients_area_init', $this);
        if(get_client_role_type() == 1)
        {
            redirect(site_url('customers'));
        }
    }

    /**
     * 
     * @Function: Success payment
    **/
    public function success()
    {
        $pay_ = '';
        $txn_id = $_GET['tx']; 
        if($txn_id)
        {
            $pay_ = 1;
            $expayment = $this->db->get_where(db_prefix().'_payments', array('txn_id' => $txn_id))->num_rows();
            if($expayment)
            {
                
            }
            else
            {
                $details = $this->db->get_where(db_prefix().'subscription', array('id' => $_GET['item_number']))->row();
                
                $postdata['garage_id']          = get_client_user_id();
                $postdata['item_number']        = $_GET['item_number'];  
                $postdata['txn_id']             = $_GET['tx'];  
                $postdata['payment_gross']      = $_GET['amt']*3.26;
                $postdata['currency_code']      = $_GET['cc'];  
                $postdata['payment_status']     = $_GET['st'];  
                $postdata['subscription_details']     = json_encode($details);  
                $date_ = date('Y-m-d');
                $postdata['expiry_date']     = strtotime($date_ . " +$details->day_limit days");
                //echo '<pre>'; print_r($postdata); die;
                $this->db->insert(db_prefix().'_payments', $postdata);
            }
            set_alert('success', _l('Payment has been successfully done!'));
        }
        else
        {
            set_alert('danger', _l('Found some error, please try again...'));
        }
        //redirect($_SERVER['HTTP_REFERER']);
        $data['title'] = _l('Payment Success');
        $this->data($data);
        $this->view('paymentSuccess');
        $this->layout();
    }
    
    /**
     * 
     * @Function: paymentcancel payment
    **/
    public function paymentcancel()
    {
        set_alert('danger', _l('Your PayPal Transaction has been Canceled'));
        //redirect($_SERVER['HTTP_REFERER']);
        $data['title'] = _l('Payment Canceled');
        $this->data($data);
        $this->view('paymentCancel');
        $this->layout();
    }
    /**
     * 
     * @Function: ipn payment
    **/
    public function ipn()
    {
        $raw_post_data = file_get_contents('php://input'); 
        $raw_post_array = explode('&', $raw_post_data); 
        $myPost = array(); 
        foreach ($raw_post_array as $keyval) { 
         $keyval = explode ('=', $keyval); 
         if (count($keyval) == 2) 
             $myPost[$keyval[0]] = urldecode($keyval[1]); 
        } 
        $req = 'cmd=_notify-validate'; 
        if(function_exists('get_magic_quotes_gpc')) { 
         $get_magic_quotes_exists = true; 
        } 
        foreach ($myPost as $key => $value) {
         if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
             $value = urlencode(stripslashes($value)); 
        } else { 
             $value = urlencode($value); 
        } 
         $req .= "&$key=$value"; 
        } 
        $paypalURL = PAYPAL_URL; 
        $ch = curl_init($paypalURL); 
        if ($ch == FALSE) { 
         return FALSE; 
        } 
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1); 
        curl_setopt($ch, CURLOPT_POST, 1); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req); 
        curl_setopt($ch, CURLOPT_SSLVERSION, 6); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); 
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1); 
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close', 'User-Agent: company-name')); 
        $res = curl_exec($ch); 
        $tokens = explode("\r\n\r\n", trim($res)); 
        $res = trim(end($tokens)); 
        if (strcmp($res, "VERIFIED") == 0 || strcasecmp($res, "VERIFIED") == 0) { 
             $item_number    = $_POST['item_number']; 
             $txn_id         = $_POST['txn_id']; 
             $payment_gross     = $_POST['mc_gross']; 
             $currency_code     = $_POST['mc_currency']; 
             $payment_status = $_POST['payment_status']; 
            
            $expayment = $this->db->get_where(db_prefix().'_payments', array('txn_id' => $txn_id))->num_rows();
            if($expayment)
            {
                
            }
            else
            { 
                $details = $this->db->get_where(db_prefix().'subscription', array('id' => $_POST['item_number']))->row();
                
                $postdata['garage_id']          = get_client_user_id();
                $postdata['item_number']        = $item_number;
                $postdata['txn_id']             = $txn_id;
                $postdata['payment_gross']      = $payment_gross*3.26;
                $postdata['currency_code']      = $currency_code; 
                $postdata['payment_status']     = $payment_status;
                $postdata['subscription_details']     = json_encode($details);    
                $date_ = date('Y-m-d');
                $postdata['expiry_date']     = strtotime($date_ . " +$details->day_limit days");
                $this->db->insert(db_prefix().'_payments', $postdata);
            }
        }
        set_alert('success', _l('Payment has been successfully done!'));
        //redirect($_SERVER['HTTP_REFERER']);
        $data['title'] = _l('Payment Success');
        $this->data($data);
        $this->view('paymentSuccess');
        $this->layout();
    }
}
