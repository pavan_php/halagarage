<?php

defined('BASEPATH') or exit('No direct script access allowed');
use app\services\ValidatesContact;

class Messages extends ClientsController
{
    /**
     * @since  2.3.3
     */
    use ValidatesContact;

    public function __construct()
    {
        parent::__construct();
        hooks()->do_action('after_clients_area_init', $this);
        if(get_client_role_type() == 1)
        {
            redirect(site_url('customers'));
        }
    }
    
    public function index()
    {
        redirect(site_url('garages/messages'));
        $data['title']     = 'Messages';
        $this->data($data);
        $this->view('messages');
        $this->layout();
    }
}
