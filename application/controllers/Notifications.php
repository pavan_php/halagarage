<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Notifications extends ClientsController
{
    
    /**
    *   @Function: notify
    */
    public function notify()
    {
        $id = $_POST['id'];
        $notifyCount = 0;
        if($id)
        {
            $notifyCount = $this->db->get_where(db_prefix().'quoteRequest_garageReply', array('garage_id' => $id, 'garage_view' => 0))->num_rows();
        }
        echo $notifyCount;
    }
    
    /**
    *   @Function: notify
    */
    public function customerNotify()
    {
        $id = get_client_user_id();
        $notifyCount = 0;
        if($id)
        {
            $notifyCount = $this->db->get_where(db_prefix().'quoteRequest_garageReply', array('customer_id' => $id, 'customer_view' => 0))->num_rows();
        }
        echo $notifyCount;
    }
    
    /**
    *   @Function: Contact-us
    */
    public function contactUs()
    {
        $notifyCount = '';
        $data = $_POST;
        $data['created_date'] = date('Y-m-d h:i:s');
        $this->db->insert(db_prefix().'contact_us', $data);
        $notifyCount = $this->db->insert_id();
        if($notifyCount)
        {
            echo $notifyCount;
            exit();
        }
        else
        {
            echo $notifyCount;   
            exit();
        }
    }
}
