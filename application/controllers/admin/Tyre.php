<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Tyre extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('tyre_model');
        $this->load->library('CSVReader');
    }

    /* List all knowledgebase tyrewidths */
    public function index()
    {
        if (!has_permission('master', '', 'view')) {
            access_denied('master');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('tyre_Width');
        }
      
        $subheader_text = setupTitle_text('setup_menu_active', 'master', 'tyre');
        $data['sheading_text'] = $subheader_text;
        $data['sh_text'] = $subheader_text;
        
        $data['tyrewidth'] = $this->db->get_where(db_prefix().'tyre_width')->result();
        $data['tyreprofile'] = $this->db->get_where(db_prefix().'tyre_profile')->result();
        $data['tyrebrand'] = $this->db->get_where(db_prefix().'tyre_brand')->result();
        $data['title']     = _l($subheader_text);
        $this->load->view('admin/tyre/tyres', $data);
    }

    /* Profile list */
    public function getprofilelist()
    {
        $profileResult = [];
        $widthid = $_POST['widthid'];
        $profileResult = $this->db->get_where(db_prefix().'tyre_profile', array('width_id' => $widthid))->result();
        echo json_encode($profileResult);
    }

    /* Add new article or edit existing*/
    public function add($id = '')
    {
        if (!has_permission('master', '', 'view')) {
            access_denied('master');
        }
        if ($this->input->post()) {
            $data                = $this->input->post();
            
            if ($id == '') {
                if (!has_permission('master', '', 'create')) {
                    access_denied('master');
                }
                $id = $this->tyre_model->add_tyrewidth($data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('Width')));
                    redirect(admin_url('tyre'));
                }
            } else {
                if (!has_permission('master', '', 'edit')) {
                    access_denied('master');
                }
                
                $success = $this->tyre_model->update_tyrewidth($data, $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('Width')));
                }
                redirect(admin_url('tyre'));
            }
        }
        
        $subheader_text = setupTitle_text('setup_menu_active', 'master', 'tyre');
        $data['sheading_text'] = $subheader_text;
        $data['sh_text'] = $subheader_text;
        
        $data['article'] = $this->db->get_where(db_prefix().'tyre_width',array('id' => $id))->row();
        $data['tyreprofile'] = $this->db->get_where(db_prefix().'tyre_profile')->result();
        $data['tyrebrand'] = $this->db->get_where(db_prefix().'tyre_brand')->result();

        $data['title']     = _l($sheader_text);
        $this->load->view('admin/tyre/tyre', $data);
    }

    /* brandtable */
    public function brandtable()
    {
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('tyre_Brand');
        }
    }
    /* gettyreprofile */
    public function profiletable()
    {
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('tyre_Profile');
        }
    }
    /* diametertable */
    public function diametertable()
    {
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('tyre_Diameter');
        }
    }

    /* Add new article or edit existing*/
    public function addbrand($id = '')
    {
        if (!has_permission('master', '', 'view')) {
            access_denied('master');
        }
        if ($this->input->post()) {
            $data                = $this->input->post();
            
            if ($id == '') {
                if (!has_permission('master', '', 'create')) {
                    access_denied('master');
                }
                $this->db->insert(db_prefix().'tyre_brand', $data);
                $id = $this->db->insert_id();
                //$id = $this->tyre_model->add_tyrewidth($data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('Brand')));
                    redirect(admin_url('tyre'));
                }
            } else {
                if (!has_permission('master', '', 'edit')) {
                    access_denied('master');
                }
                $this->db->where('id', $id);
                $this->db->update(db_prefix().'tyre_brand', $data);
                //$success = $this->tyre_model->update_tyrewidth($data, $id);
                if ($this->db->affected_rows() > 0) {
                    set_alert('success', _l('updated_successfully', _l('Brand')));
                }
                redirect(admin_url('tyre'));
            }
        }
        $data['tyrewidth'] = $this->db->get_where(db_prefix().'tyre_width')->result();
        $data['tyreprofile'] = $this->db->get_where(db_prefix().'tyre_profile')->result();
        $data['brand_result'] = $this->db->get_where(db_prefix().'tyre_brand', array('id' => $id))->row();
        
        $subheader_text = setupTitle_text('setup_menu_active', 'master', 'tyre');
        $data['sheading_text'] = $subheader_text;
        $data['sh_text'] = $subheader_text;

        $data['title']     = _l($sheader_text);
        $this->load->view('admin/tyre/tyrebrand', $data);
    }

    /* Add new article or edit existing*/
    public function addprofile($id = '')
    {
        if (!has_permission('master', '', 'view')) {
            access_denied('master');
        }
        if ($this->input->post()) {
            $data                = $this->input->post();
            
            if ($id == '') {
                if (!has_permission('master', '', 'create')) {
                    access_denied('master');
                }
                $this->db->insert(db_prefix().'tyre_profile', $data);
                $id = $this->db->insert_id();
                //$id = $this->tyre_model->add_tyrewidth($data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('Profile')));
                    redirect(admin_url('tyre#tyreprofile'));
                }
            } else {
                if (!has_permission('master', '', 'edit')) {
                    access_denied('master');
                }
                $this->db->where('id', $id);
                $this->db->update(db_prefix().'tyre_profile', $data);
                //$success = $this->tyre_model->update_tyrewidth($data, $id);
                if ($this->db->affected_rows() > 0) {
                    set_alert('success', _l('updated_successfully', _l('Profile')));
                }
                redirect(admin_url('tyre#tyreprofile'));
            }
        }
        $data['tyrewidth'] = $this->db->get_where(db_prefix().'tyre_width')->result();
        $data['profile_result'] = $this->db->get_where(db_prefix().'tyre_profile', array('id' => $id))->row();
        
        $subheader_text = setupTitle_text('setup_menu_active', 'master', 'tyre');
        $data['sheading_text'] = $subheader_text;
        $data['sh_text'] = $subheader_text;

        $data['title']     = _l($sheader_text);
        $this->load->view('admin/tyre/tyreprofile', $data);
    }

    /* Add new article or edit existing*/
    public function adddiameter($id = '')
    {
        if (!has_permission('master', '', 'view')) {
            access_denied('master');
        }
        if ($this->input->post()) {
            $data                = $this->input->post();
            
            if ($id == '') {
                if (!has_permission('master', '', 'create')) {
                    access_denied('master');
                }
                $this->db->insert(db_prefix().'tyre_diameter', $data);
                $id = $this->db->insert_id();
                //$id = $this->tyre_model->add_tyrewidth($data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('Diameter')));
                    redirect(admin_url('tyre#tyrediameter'));
                }
            } else {
                if (!has_permission('master', '', 'edit')) {
                    access_denied('master');
                }
                $this->db->where('id', $id);
                $this->db->update(db_prefix().'tyre_diameter', $data);
                //$success = $this->tyre_model->update_tyrewidth($data, $id);
                if ($this->db->affected_rows() > 0) {
                    set_alert('success', _l('updated_successfully', _l('Diameter')));
                }
                redirect(admin_url('tyre#tyrediameter'));
            }
        }
        $data['tyrewidth'] = $this->db->get_where(db_prefix().'tyre_width')->result();
        $data['tyreprofile'] = $this->db->get_where(db_prefix().'tyre_profile')->result();
        $data['tyrebrand'] = $this->db->get_where(db_prefix().'tyre_brand')->result();
        $data['diameter_result'] = $this->db->get_where(db_prefix().'tyre_diameter', array('id' => $id))->row(); 
        $subheader_text = setupTitle_text('setup_menu_active', 'master', 'tyre');
        $data['sheading_text'] = $subheader_text;
        $data['sh_text'] = $subheader_text;

        $data['title']     = _l($sheader_text);
        $this->load->view('admin/tyre/tyrediameter', $data);
    }
    
    public function getprofile(){
        echo 'make profile ';
    }

    public function save() {
        if(is_uploaded_file($_FILES['fileURL']['tmp_name'])) {      
            $csvData = $this->csvreader->parse_csv($_FILES['fileURL']['tmp_name']);       
            //echo '<pre>'; print_r($csvData); die;
            if(!empty($csvData)){
                foreach($csvData as $element){  
                    $data = array(
                        'name' => $element['Brand']
                    );
                    //echo '<pre>'; print_r($data); die;
                    $brandname = $this->db->get_where(db_prefix().'tyre_brand', array('name' => $data['name']))->row('name');
                    if($brandname == '')
                    {
                        $this->db->insert(db_prefix() . 'tyre_brand', $data);
                        $data[] = '';
                    }
                }
                set_alert('success', _l('Data are stored successful!'));
            }
            else
            {
                set_alert('warning', _l('File is required'));
            }
        }
        else
        {
            set_alert('warning', _l('File is required'));
        } 
        redirect(admin_url('tyre'));
    }
    
    // export Data
    public function sampledata() {
        $storData = array();
        $data[] = array('name' => 'Brand');       
        
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"csv-sample-brand".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        $handle = fopen('php://output', 'w');
        foreach ($data as $data) {
            fputcsv($handle, $data);
        }
            fclose($handle);
        exit;
    }
    
    public function saveWidth() {
        if(is_uploaded_file($_FILES['fileURL']['tmp_name'])) {     
            $csvData = $this->csvreader->parse_csv($_FILES['fileURL']['tmp_name']);       
            //echo '<pre>'; print_r($csvData); die;
            if(!empty($csvData)){
            $data[] = array('brand_id' => 'Brand', 'name' => 'Width');  
                foreach($csvData as $element){   
                    $name_brand =   $element['Brand'];
                    $name_width =   $element['Width'];
                    if($name_brand != '' && $name_width != '')
                    {
                        $brand_id = $this->db->get_where(db_prefix().'tyre_brand', array('name' => $name_brand))->row();
                        if($brand_id)
                        {
                            $yeardata['brand_id'] = $brand_id;
                        }
                        else
                        {
                            $data_['name'] = $name_brand;
                            $this->db->insert(db_prefix() . 'tyre_brand', $data_);
                            $brandid = $this->db->insert_id();
                            $yeardata['brand_id'] = $brandid;
                            $name_brand = '';
                            $data_ = '';
                        }
                        $width_id = $this->db->get_where(db_prefix().'tyre_width', array('name' => $name_width))->row();
                        if($width_id)
                        {
                            $yeardata['width_id'] = $width_id;
                        }
                        else
                        {
                            $datamodel_['brand_id'] = $yeardata['brand_id'];
                            $datamodel_['name'] = $name_width;
                            $this->db->insert(db_prefix() . 'tyre_width', $datamodel_);
                            $widthid = $this->db->insert_id();
                            $yeardata[] = '';
                            $name_width = '';
                            $datamodel_[] = '';
                        }
                    }
                }
                set_alert('success', _l('Data are stored successful!'));
            }
            else
            {
                set_alert('warning', _l('File is required'));
            }
        }
        else
        {
            set_alert('warning', _l('File is required'));
        } 
        redirect(admin_url('tyre'));
    }
    
    // export Data
    public function sampledataWidth() {
        $storData = array();
        $data[] = array('brand_id' => 'Brand', 'name' => 'Width');       
        
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"csv-sample-width".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        $handle = fopen('php://output', 'w');
        foreach ($data as $data) {
            fputcsv($handle, $data);
        }
            fclose($handle);
        exit;
    }

    public function saveDiameter() {
        if(is_uploaded_file($_FILES['fileURL']['tmp_name'])) {     
            $csvData = $this->csvreader->parse_csv($_FILES['fileURL']['tmp_name']);       
            //echo '<pre>'; print_r($csvData); die;
           if(!empty($csvData)){
           $data[] = array('brand_id' => 'Brand', 'width_id' => 'Width', 'profile_id' => 'Profile', 'diameter' => 'Diameter');  
                foreach($csvData as $element){   
                    $name_brand =   ucfirst($element['Brand']);
                    $name_width =   $element['Width'];
                    $name_profile =   $element['Profile'];
                    if($name_brand != '' && $name_width != '' && $name_profile != '')
                    {
                        $brand_id = $this->db->get_where(db_prefix().'tyre_brand', array('name' => $name_brand))->row('id');
                        if($brand_id)
                        {
                            $yeardata['brand_id'] = $brand_id;
                        }
                        else
                        {
                            $data_['name'] = ucfirst($name_brand);
                            $this->db->insert(db_prefix() . 'tyre_brand', $data_);
                            $brandid = $this->db->insert_id();
                            $yeardata['brand_id'] = $brandid;
                            $name_brand = '';
                            $data_ = '';
                            $brandid = '';
                        }
                        $width_id = $this->db->get_where(db_prefix().'tyre_width', array('name' => $name_width))->row('id');
                        if($width_id)
                        {
                            $yeardata['width_id'] = $width_id;
                        }
                        else
                        {
                            $datamodel_['brand_id'] = $yeardata['brand_id'];
                            $datamodel_['name'] = $name_width;
                            $this->db->insert(db_prefix() . 'tyre_width', $datamodel_);
                            $widthid = $this->db->insert_id();
                            $yeardata['width_id'] = $widthid;
                            $name_width = '';
                            $datamodel_ = '';
                            $widthid = '';
                        }
                        $profile_id = $this->db->get_where(db_prefix().'tyre_profile', array('value' => $name_profile))->row('id');
                        if($profile_id)
                        {
                            $yeardata['profile_id'] = $profile_id;
                        }
                        else
                        {
                            $dataprofile['brand_id'] = $yeardata['brand_id'];
                            $dataprofile['width_id'] = $yeardata['width_id'];
                            $dataprofile['value'] = $name_profile;
                            $this->db->insert(db_prefix() . 'tyre_profile', $dataprofile);
                            $profileid = $this->db->insert_id();
                            $yeardata['profile_id'] = $profileid;
                            $name_profile = '';
                            $dataprofile = '';
                            $profileid = '';
                        }

                        $yeardata['diameter'] = $element['Diameter'];
                        $this->db->insert(db_prefix() . 'tyre_diameter', $yeardata);
                        $yeardata = '';
                    }
                }
                set_alert('success', _l('Data are stored successful!'));
            }
            else
            {
                set_alert('warning', _l('File is required'));
            }
        }
        else
        {
            set_alert('warning', _l('File is required'));
        } 
        redirect(admin_url('tyre'));
    }
    
    public function saveProfile() {
        if(is_uploaded_file($_FILES['fileURL']['tmp_name'])) {     
            $csvData = $this->csvreader->parse_csv($_FILES['fileURL']['tmp_name']);       
            //echo '<pre>'; print_r($csvData); die;
           if(!empty($csvData)){
           $data[] = array('brand_id' => 'Brand', 'width_id' => 'Width', 'profile_id' => 'Profile');  
                foreach($csvData as $element){   
                    $name_brand =   $element['Brand'];
                    $name_width =   $element['Width'];
                    $name_profile =   $element['Profile'];
                    if($name_brand != '' && $name_width != '' && $name_profile != '')
                    {
                        $brand_id = $this->db->get_where(db_prefix().'tyre_brand', array('name' => $name_brand))->row();
                        if($brand_id)
                        {
                            $yeardata['brand_id'] = $brand_id;
                        }
                        else
                        {
                            $data_['name'] = $name_brand;
                            $this->db->insert(db_prefix() . 'tyre_brand', $data_);
                            $brandid = $this->db->insert_id();
                            $yeardata['brand_id'] = $brandid;
                            $name_brand = '';
                            $data_ = '';
                        }
                        $width_id = $this->db->get_where(db_prefix().'tyre_width', array('name' => $name_width))->row();
                        if($width_id)
                        {
                            $yeardata['width_id'] = $width_id;
                        }
                        else
                        {
                            $datamodel_['brand_id'] = $yeardata['brand_id'];
                            $datamodel_['name'] = $name_width;
                            $this->db->insert(db_prefix() . 'tyre_width', $datamodel_);
                            $widthid = $this->db->insert_id();
                            $yeardata['width_id'] = $widthid;
                            $name_width = '';
                            $datamodel_ = '';
                        }
                        $profile_id = $this->db->get_where(db_prefix().'tyre_profile', array('value' => $name_profile))->row();
                        if($profile_id)
                        {
                            $yeardata['profile_id'] = $profile_id;
                        }
                        else
                        {
                            $dataprofile['brand_id'] = $yeardata['brand_id'];
                            $dataprofile['width_id'] = $yeardata['width_id'];
                            $dataprofile['value'] = $name_profile;
                            $this->db->insert(db_prefix() . 'tyre_profile', $dataprofile);
                            $profileid = $this->db->insert_id();
                            $yeardata = '';
                            $name_profile = '';
                            $dataprofile = '';
                        }
                    }
                }
                set_alert('success', _l('Data are stored successful!'));
            }
            else
            {
                set_alert('warning', _l('File is required'));
            }
        }
        else
        {
            set_alert('warning', _l('File is required'));
        } 
        redirect(admin_url('tyre'));
    }
    
    // export Data
    public function sampledataProfile() {
        $storData = array();
        $data[] = array('brand_id' => 'Brand', 'width_id' => 'Width', 'value' => 'Profile');       
        
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"csv-sample-profile".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        $handle = fopen('php://output', 'w');
        foreach ($data as $data) {
            fputcsv($handle, $data);
        }
            fclose($handle);
        exit;
    }
    
    // export Data
    public function sampledataDiameter() {
        $storData = array();
        $data[] = array('brand_id' => 'Brand', 'width_id' => 'Width', 'profile_id' => 'Profile', 'diameter' => 'Diameter');       
        
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"csv-sample-diameter".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        $handle = fopen('php://output', 'w');
        foreach ($data as $data) {
            fputcsv($handle, $data);
        }
            fclose($handle);
        exit;
    }
    
    /* Delete article from database */
    public function delete_tyrewidth($id)
    {
        if (!has_permission('master', '', 'delete')) {
            access_denied('master');
        }
        if (!$id) {
            redirect(admin_url('tyre'));
        }
        $response = $this->tyre_model->delete_tyrewidth($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('Width')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('Width')));
        }
        redirect(admin_url('tyre'));
    }
    
    /* Delete article from database */
    public function delete_tyreprofile($id)
    {
        if (!has_permission('master', '', 'delete')) {
            access_denied('master');
        }
        if (!$id) {
            redirect(admin_url('tyre'));
        }
        $response = $this->tyre_model->delete_tyreprofile($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('Profile')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('Profile')));
        }
        redirect(admin_url('tyre#tyreprofile'));
    }
    /* Delete article from database */
    public function delete_tyrediameter($id)
    {
        if (!has_permission('master', '', 'delete')) {
            access_denied('master');
        }
        if (!$id) {
            redirect(admin_url('tyre'));
        }
        $response = $this->tyre_model->delete_tyrediameter($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('Diameter')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('Diameter')));
        }
        redirect(admin_url('tyre#tyrediameter'));
    }
    /* Delete article from database */
    public function delete_tyrebrand($id)
    {
        if (!has_permission('master', '', 'delete')) {
            access_denied('master');
        }
        if (!$id) {
            redirect(admin_url('tyre'));
        }
        $response = $this->tyre_model->delete_tyrebrand($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('Brand')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('Brand')));
        }
        redirect(admin_url('tyre'));
    }
}
