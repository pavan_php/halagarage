<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Message extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    /* Function: @Message  */
    public function index()
    {
        if (!has_permission('message', '', 'view')) {
            access_denied('message');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('message');
        }
      
        $header_text = title_text('aside_menu_active', 'message');
        $data['heading_text'] = $header_text;
        $data['title']    = _l($header_text);
        $this->load->view('admin/message/messages', $data);
    }
    
    /* Function: @sendboxList  */
    public function sendboxList()
    {
        if (!has_permission('message', '', 'view')) {
            access_denied('message');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('sendboxList');
        }
        
        $header_text = title_text('aside_menu_active', 'message');
        $data['heading_text'] = $header_text;
        $data['title']    = _l($header_text);
        $this->load->view('admin/message/sendboxList', $data);
    }
    
    /**
    *   @Function: composemsg
    */
    public function composemsg()
    {
        $msg = '';
        if($this->input->post())
        {
           $postData['sender_id']   = 0;
           $postData['receiver_id'] = $_POST['receiver_id'];
           $postData['token_id']    = $_POST['tokenID'];
           $postData['title']       = $this->input->post('title');
           $postData['message']     = $this->input->post('message');
           
           $this->db->insert(db_prefix().'message', $postData);
           $lastid = $this->db->insert_id();
           if($lastid)
           {
                $msg = 'Message are send successfully';
           }
           else
           {
                $msg = '';
           }
        }
        else
        {
            $msg = '';
        }
        echo $msg;
    }
}