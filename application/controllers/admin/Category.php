<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Category extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('category_model');
        $this->load->library('CSVReader');
    }

    /* List all articles */
    public function index()
    {
        if (!has_permission('category', '', 'view')) {
            access_denied('category');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('category');
        }
       
        $sheader_text = title_text('aside_menu_active', 'category');
        $data['sheading_text'] = $sheader_text;
        $data['sh_text'] = $sheader_text;
        
        $data['title']     = _l($sheader_text);
        $data['category_result'] = $this->db->get_where(db_prefix().'category', array('parent_id' => 0))->result();
        $this->load->view('admin/category/categories', $data);
    }

    /* sub_category */
    public function subCategory()
    {
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('sub_category');
        }
    }

    /* Add new article or edit existing*/
    public function add($id = '')
    {
        if (!has_permission('category', '', 'view')) {
            access_denied('category');
        }
        if ($this->input->post()) {
            $data                = $this->input->post();
            //echo '<pre>'; print_r($data); die;
            if ($id == '') {
                if (!has_permission('category', '', 'create')) {
                    access_denied('category');
                }
                $id = $this->category_model->add_article($data);
                if ($id) {
                    
                    $uploadedFiles = handle_task_attachments_array($id,'category');
                    if ($uploadedFiles && is_array($uploadedFiles)) {
                        foreach ($uploadedFiles as $file) {
                            $this->misc_model->add_attachment_to_database($id, 'category', [$file]);
                        }
                    }
                    set_alert('success', _l('added_successfully', _l('Category')));
                    redirect(admin_url('category'));
                }
            } else {
                if (!has_permission('category', '', 'edit')) {
                    access_denied('category');
                }
                $success = $this->category_model->update_article($data, $id);
                $uploadedFiles = handle_task_attachments_array($id,'category');
                if ($uploadedFiles && is_array($uploadedFiles)) {
                    foreach ($uploadedFiles as $file) {
                        $this->db->delete(db_prefix().'files', array('rel_id' => $id, 'rel_type' => 'category'));
                        $this->misc_model->add_attachment_to_database($id, 'category', [$file]);
                    }
                }
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('Category')));
                }
                redirect(admin_url('category'));
            }
        }
        if ($id == '') {
            $title = _l('add_new', _l('Category'));
            $data['category_result'] = $this->db->get_where(db_prefix().'category', array('parent_id' => 0))->result();
        } else {
            $article         = $this->category_model->get($id);
            //echo '<pre>'; print_r($article); die;
            $data['article'] = $article;
            $data['category_result'] = $this->db->get_where(db_prefix().'category', array('parent_id' => 0, 'id != '=> $id))->result();
        }
        $sheader_text = title_text('aside_menu_active', 'category');
        $data['sheading_text'] = $sheader_text;
        $data['sh_text'] = $sheader_text;
        
        $data['title']     = _l($sheader_text);
        $this->load->view('admin/category/category', $data);
    }
    
    /* Edit sub category */
    public function edit($id = '')
    {
        if (!has_permission('category', '', 'view')) {
            access_denied('category');
        }
        if ($this->input->post()) {
            $data                = $this->input->post();
            //echo '<pre>'; print_r($data); die;
            if ($id == '') {
                if (!has_permission('category', '', 'create')) {
                    access_denied('category');
                }
                $id = $this->category_model->add_article($data);
                if ($id) {
                    
                    $uploadedFiles = handle_task_attachments_array($id,'category');
                    if ($uploadedFiles && is_array($uploadedFiles)) {
                        foreach ($uploadedFiles as $file) {
                            $this->misc_model->add_attachment_to_database($id, 'category', [$file]);
                        }
                    }
                    
                    set_alert('success', _l('added_successfully', _l('Category')));
                    redirect(admin_url('category'));
                }
            } else {
                if (!has_permission('category', '', 'edit')) {
                    access_denied('category');
                }
                $success = $this->category_model->update_article($data, $id);
                $uploadedFiles = handle_task_attachments_array($id,'category');
                if ($uploadedFiles && is_array($uploadedFiles)) {
                    foreach ($uploadedFiles as $file) {
                        $this->db->delete(db_prefix().'files', array('rel_id' => $id, 'rel_type' => 'category'));
                        $this->misc_model->add_attachment_to_database($id, 'category', [$file]);
                    }
                }
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('Sub Category')));
                }
                redirect(admin_url('category'));
            }
        }
        if ($id == '') {
            $title = _l('add_new', _l('Category'));
            $data['category_result'] = $this->db->get_where(db_prefix().'category', array('parent_id' => 0))->result();
        } else {
            $article         = $this->category_model->get($id);
            //echo '<pre>'; print_r($article); die;
            $data['article'] = $article;
            $data['category_result'] = $this->db->get_where(db_prefix().'category', array('parent_id' => 0, 'id != '=> $id))->result();
        }
        $sheader_text = title_text('aside_menu_active', 'category');
        $data['sheading_text'] = $sheader_text;
        $data['sh_text'] = $sheader_text;
        
        $data['title']     = _l($sheader_text);
        $this->load->view('admin/category/sub_category', $data);
    }

    public function save() {
        // If file uploaded
        if(is_uploaded_file($_FILES['fileURL']['tmp_name'])) {                            
            // Parse data from CSV file
            $csvData = $this->csvreader->parse_csv($_FILES['fileURL']['tmp_name']);       
            //echo '<pre>'; print_r($csvData); die;
            // create array from CSV file
            if(!empty($csvData)){
                foreach($csvData as $element){                    
                    // Prepare data for Database insertion
                    $data = array(
                        'name' => $element['Category']
                    );
                    $exitcategory = $this->db->get_where(db_prefix().'category', array('name' => $data['name']))->row('name');
                    if($exitcategory != '')
                    {
                        
                    }
                    else
                    {
                        $this->db->insert(db_prefix() . 'category', $data);
                        $data[] = '';
                    }
                }
                set_alert('success', _l('Data are stored successful!'));
            }
            else
            {
                set_alert('warning', _l('File is required'));
            }
        }
        else
        {
            set_alert('warning', _l('File is required'));
        } 
        redirect(admin_url('category'));
    }
    
    // export Data
    public function sampledata() {
        $storData = array();
        $data[] = array('name' => 'Category');       
        
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"csv-sample-category".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        $handle = fopen('php://output', 'w');
        foreach ($data as $data) {
            fputcsv($handle, $data);
        }
            fclose($handle);
        exit;
    }

    /* Delete article from database */
    public function delete_category($id)
    {
        if (!has_permission('category', '', 'delete')) {
            access_denied('category');
        }
        if (!$id) {
            redirect(admin_url('category'));
        }
        $response = $this->category_model->delete_article($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('Category')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('Category')));
        }
        redirect(admin_url('category'));
    }
}
