<?php

defined('BASEPATH') or exit('No direct script access allowed');

class QuoteRequest extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    /* Function: @quoteRequest  */
    public function index()
    {
        if (!has_permission('quoteRequest', '', 'view')) {
            access_denied('quoteRequest');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('quoteRequest');
        }
      
        $header_text = title_text('aside_menu_active', 'quoteRequest');
        $data['heading_text'] = $header_text;
        $data['title']    = _l($header_text);
        $this->load->view('admin/quoteRequest/quoteRequests', $data);
    }
    
    /**
    *   @Function: 
    *
    */
}