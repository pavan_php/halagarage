<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Orders extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    /* Function: @orders  */
    public function index()
    {
        if (!has_permission('orders', '', 'view')) {
            access_denied('orders');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('orders');
        }
      
        $header_text = title_text('aside_menu_active', 'orders');
        $data['heading_text'] = $header_text;
        $data['title']    = _l($header_text);
        $this->load->view('admin/orders/orders', $data);
    }
}