<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Car extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('car_model');
        $this->load->library('CSVReader');
    }

    /* List all knowledgebase carmakes */
    public function index()
    {
        if (!has_permission('master', '', 'view')) {
            access_denied('master');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('car_make');
        }
      
        $subheader_text = setupTitle_text('setup_menu_active', 'master', 'car');
        $data['sheading_text'] = $subheader_text;
        $data['sh_text'] = $subheader_text;
        
        $data['carmake'] = $this->db->get_where(db_prefix().'car_make')->result();
        $data['carmodel'] = $this->db->get_where(db_prefix().'car_model')->result();
        $data['title']     = _l($subheader_text);
        $this->load->view('admin/car/cars', $data);
    }

    /* getmodellist */
    public function getmodellist()
    {
        $profileResult = [];
        $makeid = $_POST['makeid'];
        $profileResult = $this->db->get_where(db_prefix().'car_model', array('make_id' => $makeid))->result();
        echo json_encode($profileResult);
    }

    /* Add new article or edit existing*/
    public function add($id = '')
    {
        if (!has_permission('master', '', 'view')) {
            access_denied('master');
        }
        if ($this->input->post()) {
            $data                = $this->input->post();
            
            if ($id == '') {
                if (!has_permission('master', '', 'create')) {
                    access_denied('master');
                }
                
                $existingname = $this->db->get_where('tblcar_make', array('name' => $data['name']))->num_rows();
                if($existingname > 0)
                {
                    set_alert('warning', _l($data['name'].' make name is already exists'));
                    redirect(admin_url('car'));
                }
                else
                {
                    $id = $this->car_model->add_carmake($data);
                    if ($id) {
                        set_alert('success', _l('added_successfully', _l('Make')));
                        redirect(admin_url('car'));
                    }   
                }
            } else {
                if (!has_permission('master', '', 'edit')) {
                    access_denied('master');
                }
                
                $success = $this->car_model->update_carmake($data, $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('Make')));
                }
                redirect(admin_url('car'));
            }
        }
        
        $subheader_text = setupTitle_text('setup_menu_active', 'master', 'car');
        $data['sheading_text'] = $subheader_text;
        $data['sh_text'] = $subheader_text;
        
        $data['article'] = $this->db->get_where(db_prefix().'car_make',array('id' => $id))->row();

        $data['title']     = _l($sheader_text);
        $this->load->view('admin/car/car', $data);
    }

    /* getCarModel */
    public function modeltable()
    {
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('car_model');
        }
    }
    /* yeartable */
    public function yeartable()
    {
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('car_year');
        }
    }
    
    /* varienttable */
    public function varienttable()
    {
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('car_varient');
        }
    }

    /* Add new article or edit existing*/
    public function addmodel($id = '')
    {
        if (!has_permission('master', '', 'view')) {
            access_denied('master');
        }
        if ($this->input->post()) {
            $data                = $this->input->post();
            
            if ($id == '') {
                if (!has_permission('master', '', 'create')) {
                    access_denied('master');
                }
                
                $existingname = $this->db->get_where('tblcar_model', array('name' => $data['name']))->num_rows();
                if($existingname > 0)
                {
                    set_alert('warning', _l($data['name'].' make name is already exists'));
                    redirect(admin_url('car#carmodel'));
                }
                else
                {
                    $this->db->insert(db_prefix().'car_model', $data);
                    $id = $this->db->insert_id();
                    //$id = $this->car_model->add_carmake($data);
                    if ($id) {
                        set_alert('success', _l('added_successfully', _l('Model')));
                        redirect(admin_url('car#carmodel'));
                    }
                }
            } else {
                if (!has_permission('master', '', 'edit')) {
                    access_denied('master');
                }
                $this->db->where('id', $id);
                $this->db->update(db_prefix().'car_model', $data);
                //$success = $this->car_model->update_carmake($data, $id);
                if ($this->db->affected_rows() > 0) {
                    set_alert('success', _l('updated_successfully', _l('Model')));
                }
                redirect(admin_url('car#carmodel'));
            }
        }
        $data['carmake'] = $this->db->get_where(db_prefix().'car_make')->result();
        $data['model_result'] = $this->db->get_where(db_prefix().'car_model', array('id' => $id))->row();
        
        $subheader_text = setupTitle_text('setup_menu_active', 'master', 'car');
        $data['sheading_text'] = $subheader_text;
        $data['sh_text'] = $subheader_text;

        $data['title']     = _l($sheader_text);
        $this->load->view('admin/car/carmodel', $data);
    }

    /* Add new article or edit existing*/
    public function addyear($id = '')
    {
        if (!has_permission('master', '', 'view')) {
            access_denied('master');
        }
        if ($this->input->post()) {
            $data                = $this->input->post();
            
            if ($id == '') {
                if (!has_permission('master', '', 'create')) {
                    access_denied('master');
                }
                $existingname = $this->db->get_where('tblcar_year', array('year' => $data['year']))->num_rows();
                if($existingname > 0)
                {
                    set_alert('warning', _l($data['year'].' year is already exists'));
                    redirect(admin_url('car#caryear'));
                }
                else
                {
                    $this->db->insert(db_prefix().'car_year', $data);
                    $id = $this->db->insert_id();
                    //$id = $this->car_model->add_carmake($data);
                    if ($id) {
                        set_alert('success', _l('added_successfully', _l('Year')));
                        redirect(admin_url('car#caryear'));
                    }   
                }
            } else {
                if (!has_permission('master', '', 'edit')) {
                    access_denied('master');
                }
                $this->db->where('id', $id);
                $this->db->update(db_prefix().'car_year', $data);
                //$success = $this->car_model->update_carmake($data, $id);
                if ($this->db->affected_rows() > 0) {
                    set_alert('success', _l('updated_successfully', _l('Year')));
                }
                redirect(admin_url('car#caryear'));
            }
        }
        $data['carmake'] = $this->db->get_where(db_prefix().'car_make')->result();
        $data['carmodel'] = $this->db->get_where(db_prefix().'car_model')->result();
        $data['year_result'] = $this->db->get_where(db_prefix().'car_year', array('id' => $id))->row(); 
        $subheader_text = setupTitle_text('setup_menu_active', 'master', 'car');
        $data['sheading_text'] = $subheader_text;
        $data['sh_text'] = $subheader_text;

        $data['title']     = _l($sheader_text);
        $this->load->view('admin/car/caryear', $data);
    }
    
    /* Add new article or edit existing*/
    public function edidVarient($id = '')
    {
        if (!has_permission('master', '', 'view')) {
            access_denied('master');
        }
        if ($this->input->post()) {
            $data                = $this->input->post();
            
            if (!has_permission('master', '', 'edit')) {
                access_denied('master');
            }
            $this->db->where('id', $id);
            $this->db->update(db_prefix().'car_varient', $data);
            //$success = $this->car_model->update_carmake($data, $id);
            if ($this->db->affected_rows() > 0) {
                set_alert('success', _l('updated_successfully', _l('Varient')));
            }
            redirect(admin_url('car#carVarient'));
        }
        $data['carmake'] = $this->db->get_where(db_prefix().'car_make')->result();
        $data['carmodel'] = $this->db->get_where(db_prefix().'car_model')->result();
        $data['yearResult'] = $this->db->get_where(db_prefix().'car_year')->result(); 
        $data['year_result'] = $this->db->get_where(db_prefix().'car_varient', array('id' => $id))->row(); 
        $subheader_text = setupTitle_text('setup_menu_active', 'master', 'car');
        $data['sheading_text'] = $subheader_text;
        $data['sh_text'] = $subheader_text;

        $data['title']     = _l($sheader_text);
        $this->load->view('admin/car/edidVarient', $data);
    }
    
    public function getmodel(){
        echo 'make model ';
    }

    public function save() {
        // If file uploaded
        if(is_uploaded_file($_FILES['fileURL']['tmp_name'])) {                            
            // Parse data from CSV file
            $csvData = $this->csvreader->parse_csv($_FILES['fileURL']['tmp_name']);       
            //echo '<pre>'; print_r($csvData); die;
            // create array from CSV file
            if(!empty($csvData)){
                foreach($csvData as $element){    
                    if($element['Make'] != '')
                    {
                        // Prepare data for Database insertion
                        $data = array(
                            'name' => $element['Make']
                        );
                        //echo '<pre>'; print_r($data); die;
                        $existingname = $this->db->get_where('tblcar_make', array('name' => $element['Make']))->num_rows();
                        if($existingname > 0)
                        {
                            
                        }
                        else
                        {
                            $this->db->insert(db_prefix() . 'car_make', $data);
                            $data[] = '';   
                        }   
                    }
                }
                set_alert('success', _l('Data are stored successful!'));
            }
            else
            {
                set_alert('warning', _l('File is required'));
            }
        }
        else
        {
            set_alert('warning', _l('File is required'));
        } 
        redirect(admin_url('car'));
    }
    
    // export Data
    public function sampledata() {
        $storData = array();
        $data[] = array('name' => 'Make');       
        
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"csv-sample-make".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        $handle = fopen('php://output', 'w');
        foreach ($data as $data) {
            fputcsv($handle, $data);
        }
            fclose($handle);
        exit;
    }

    public function saveModel() {
        if(is_uploaded_file($_FILES['fileURL']['tmp_name'])) {     
            $csvData = $this->csvreader->parse_csv($_FILES['fileURL']['tmp_name']);       
            //echo '<pre>'; print_r($csvData); die;
           if(!empty($csvData)){
                foreach($csvData as $element){   
                    $name_make =   $element['Make'];
                    if($name_make != '')
                    {
                        $make_id = $this->db->get_where(db_prefix().'car_make', array('name' => $name_make))->row('id');
                        if($make_id)
                        {
                            $modeldata['make_id'] = $make_id;
                        }
                        else
                        {
                            $data_['name'] = $name_make;
                            $this->db->insert(db_prefix() . 'car_make', $data_);
                            $makelid = $this->db->insert_id();
                            $modeldata['make_id'] = $makelid;
                            $name_make = '';
                            $data_ = '';
                        }
                        if($element['Model'] != '')
                        {
                            $modeldata['name'] = $element['Model'];
                            $model_id = $this->db->get_where(db_prefix().'car_model', array('name' => $modeldata['name']))->row('id');
                            if($model_id == '')
                            {
                                $this->db->insert(db_prefix() . 'car_model', $modeldata);
                                $modeldata[] = '';
                            }
                        }
                    }
                }
                set_alert('success', _l('Data are stored successful!'));
            }
            else
            {
                set_alert('warning', _l('File is required'));
            }
        }
        else
        {
            set_alert('warning', _l('File is required'));
        } 
        redirect(admin_url('car'));
    }
    
    // export Data
    public function sampledataModel() {
        $storData = array();
        $data[] = array('make_id' => 'Make', 'name' => 'Model');       
        
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"csv-sample-model".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        $handle = fopen('php://output', 'w');
        foreach ($data as $data) {
            fputcsv($handle, $data);
        }
            fclose($handle);
        exit;
    }
    
    public function saveYear() {
        if(is_uploaded_file($_FILES['fileURL']['tmp_name'])) {     
            $csvData = $this->csvreader->parse_csv($_FILES['fileURL']['tmp_name']);       
            //echo '<pre>'; print_r($csvData); die;
           if(!empty($csvData)){
                foreach($csvData as $element){   
                    $name_make =   $element['Make'];
                    $name_model =   $element['Model'];
                    if($name_make != '' && $name_model != '')
                    {
                        $make_id = $this->db->get_where(db_prefix().'car_make', array('name' => $name_make))->row('id');
                        if($make_id)
                        {
                            $yeardata['make_id'] = $make_id;
                        }
                        else
                        {
                            $data_['name'] = $name_make;
                            $this->db->insert(db_prefix() . 'car_make', $data_);
                            $makelid = $this->db->insert_id();
                            $yeardata['make_id'] = $makelid;
                            $name_make = '';
                            $data_ = '';
                        }
                        $model_id = $this->db->get_where(db_prefix().'car_model', array('name' => $name_model))->row('id');
                        if($model_id)
                        {
                            $yeardata['model_id'] = $model_id;
                        }
                        else
                        {
                            $datamodel_['make_id'] = $yeardata['make_id'];
                            $datamodel_['name'] = $name_model;
                            $this->db->insert(db_prefix() . 'car_model', $datamodel_);
                            $modellid = $this->db->insert_id();
                            $yeardata['model_id'] = $modellid;
                            $name_model = '';
                            $datamodel_ = '';
                        }
                        if($element['Model_number'] != '')
                        {
                            $yeardata['model_number'] = $element['Model_number'];
                            $yeardata['year'] = $element['Year'];
                            $yeardata['tyre_info'] = $element['Tyre_info'];
                            $model_ids = $this->db->get_where(db_prefix().'car_year', array('make_id' => $yeardata['make_id'], 'model_id' => $yeardata['model_id'], 'year' => $yeardata['year']))->row('id');
                            if($model_ids == '')
                            {
                                $this->db->insert(db_prefix() . 'car_year', $yeardata);
                                $yeardata = '';
                            }
                        }
                    }
                }
                set_alert('success', _l('Data are stored successful!'));
            }
            else
            {
                set_alert('warning', _l('File is required'));
            }
        }
        else
        {
            set_alert('warning', _l('File is required'));
        } 
        redirect(admin_url('car'));
    }
    
    // export Data
    public function sampledataYear() {
        $storData = array();
        $data[] = array('make_id' => 'Make', 'model_id' => 'Model', 'year' => 'Year');       
        
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"csv-sample-year".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        $handle = fopen('php://output', 'w');
        foreach ($data as $data) {
            fputcsv($handle, $data);
        }
            fclose($handle);
        exit;
    }

    public function saveVarient() {
        if(is_uploaded_file($_FILES['fileURL']['tmp_name'])) {     
            $csvData = $this->csvreader->parse_csv($_FILES['fileURL']['tmp_name']);       
            //echo '<pre>'; print_r($csvData); die;
           if(!empty($csvData)){
                foreach($csvData as $element){   
                    $name_make =   $element['Make'];
                    $name_model =   $element['Model'];
                    $name_year =   $element['Year'];
                    if($name_make != '' && $name_model != '' && $name_year != '')
                    {
                        $make_id = $this->db->get_where(db_prefix().'car_make', array('name' => $name_make))->row('id');
                        if($make_id)
                        {
                            $yeardata['make_id'] = $make_id;
                            $varientdata['make_id'] = $make_id;
                        }
                        else
                        {
                            $data_['name'] = $name_make;
                            $this->db->insert(db_prefix() . 'car_make', $data_);
                            $makelid = $this->db->insert_id();
                            $yeardata['make_id'] = $makelid;
                            $varientdata['make_id'] = $makelid;
                            $name_make = '';
                            $data_ = '';
                        }
                        $model_id = $this->db->get_where(db_prefix().'car_model', array('name' => $name_model))->row('id');
                        if($model_id)
                        {
                            $yeardata['model_id'] = $model_id;
                            $varientdata['model_id'] = $model_id;
                        }
                        else
                        {
                            $datamodel_['make_id'] = $yeardata['make_id'];
                            $datamodel_['name'] = $name_model;
                            $this->db->insert(db_prefix() . 'car_model', $datamodel_);
                            $modellid = $this->db->insert_id();
                            $yeardata['model_id'] = $modellid;
                            $varientdata['model_id'] = $modellid;
                            $name_model = '';
                            $datamodel_ = '';
                        }
                        $year_id = $this->db->get_where(db_prefix().'car_year', array('year' => $name_year))->row('id');
                        if($year_id)
                        {
                            $varientdata['year_id'] = $year_id;
                        }
                        else
                        {
                            //$yeardata['model_number'] = $element['Model_number'];
                            $yeardata['model_number'] = $element['Year'];
                            $yeardata['year'] = $element['Year'];
                            //$yeardata['tyre_info'] = $element['Tyre_info'];
                            $yeardata['tyre_info'] = $element['Year'];
                            //$model_ids = $this->db->get_where(db_prefix().'car_year', array('year' => $yeardata['year']))->row('id');
                            $this->db->insert(db_prefix() . 'car_year', $yeardata);
                            $yearlid = $this->db->insert_id();
                            $varientdata['year_id'] = $yearlid;
                            $yeardata = '';
                        }
                        $varient_ids = $this->db->get_where(db_prefix().'car_varient', array('varient' => $element['Varient'], 'make_id' => $varientdata['make_id'], 'model_id' => $varientdata['model_id'], 'year_id' => $varientdata['year_id']))->row('id');
                        if($varient_ids == '')
                        {
                            $varientdata['varient'] = $element['Varient'];
                            $varientdata['created_at'] = date('Y-m-d h:i:s');
                            $this->db->insert(db_prefix() . 'car_varient', $varientdata);
                            $varientdata = '';
                        }
                    }
                }
                set_alert('success', _l('Data are stored successful!'));
            }
            else
            {
                set_alert('warning', _l('File is required'));
            }
        }
        else
        {
            set_alert('warning', _l('File is required'));
        } 
        redirect(admin_url('car'));
    }
    
    // export Data
    public function sampledataVarient() {
        $storData = array();
        $data[] = array('make_id' => 'Make', 'model_id' => 'Model', 'year_id' => 'Year', 'varient' => 'Varient');       
        
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"csv-sample-varient".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        $handle = fopen('php://output', 'w');
        foreach ($data as $data) {
            fputcsv($handle, $data);
        }
            fclose($handle);
        exit;
    }

    /* Delete article from database */
    public function delete_carmake($id)
    {
        if (!has_permission('master', '', 'delete')) {
            access_denied('master');
        }
        if (!$id) {
            redirect(admin_url('car'));
        }
        $response = $this->car_model->delete_carmake($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('Make')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('Make')));
        }
        redirect(admin_url('car'));
    }
    
    /* Delete article from database */
    public function delete_carmodel($id)
    {
        if (!has_permission('master', '', 'delete')) {
            access_denied('master');
        }
        if (!$id) {
            redirect(admin_url('car'));
        }
        $response = $this->car_model->delete_carmodel($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('Model')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('model')));
        }
        redirect(admin_url('car#carmodel'));
    }
    /* Delete article from database */
    public function delete_caryear($id)
    {
        if (!has_permission('master', '', 'delete')) {
            access_denied('master');
        }
        if (!$id) {
            redirect(admin_url('car'));
        }
        $response = $this->car_model->delete_caryear($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('Year')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('Year')));
        }
        redirect(admin_url('car#caryear'));
    }
    /* Delete article from database */
    public function deleteVarient($id)
    {
        if (!has_permission('master', '', 'delete')) {
            access_denied('master');
        }
        if (!$id) {
            redirect(admin_url('car'));
        }
        $this->db->delete('tblcar_varient', array('id' => $id));
        set_alert('success', _l('deleted', _l('Varient')));
        /*
        $response = $this->car_model->delete_caryear($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('Varient')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('Year')));
        }*/
        redirect(admin_url('car#carVarient'));
    }
}
