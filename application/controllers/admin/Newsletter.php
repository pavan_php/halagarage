<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Newsletter extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    /* List all newsletter */
    public function index()
    {
        if (!has_permission('newsletter', '', 'view')) {
            access_denied('newsletter');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('newsletter');
        }
      
        $header_text = title_text('aside_menu_active', 'newsletter');
        $data['heading_text'] = $header_text;
        $data['title']    = _l($header_text);
        $this->load->view('admin/newsletter/newsletter', $data);
    }
}