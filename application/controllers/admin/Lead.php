<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Lead extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    /* List all leads */
    public function index()
    {
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('leads');
        }
        
        $header_text = title_text('aside_menu_active', 'leads');
        $data['heading_text'] = $header_text;
        $data['title']    = _l($header_text);
        $this->load->view('admin/leads_/leads_', $data);
    }
    
    /* Delete article from database */
    public function delete_lead($id)
    {
        if (!has_permission('master', '', 'delete')) {
            access_denied('master');
        }
        
        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'contact_us');
        if ($this->db->affected_rows() > 0) {
            set_alert('success', _l('deleted', _l('Lead')));
        }
        else
        {
            set_alert('warning', _l('problem_deleting', _l('Lead')));
        }
        redirect(admin_url('lead'));
    }
}
