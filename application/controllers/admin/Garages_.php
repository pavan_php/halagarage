<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Garages extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('garages_model');
    }

    /* List all garages articles */
    public function index()
    {
        if (!has_permission('garages', '', 'view')) {
            access_denied('garages');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('garages');
        }
       
        $header_text = title_text('aside_menu_active', 'garages');
        $data['heading_text'] = $header_text;
        $data['sh_text'] = $header_text;
        
        $data['title']     = _l($header_text);
        $this->load->view('admin/garages/garages', $data);
    }

    /* Add new article or edit existing*/
    public function add($id = '')
    {
        if (!has_permission('garages', '', 'view')) {
            access_denied('garages');
        }
        if ($this->input->post()) {
            $data                = $this->input->post();
            if ($id == '') {
                if (!has_permission('garages', '', 'create')) {
                    access_denied('garages');
                }
                $id = $this->garages_model->add_article($data);
                if ($id) {
                    
                    $uploadedFiles = handle_task_attachments_array($id,'logo');
                    if ($uploadedFiles && is_array($uploadedFiles)) {
                        foreach ($uploadedFiles as $file) {
                            $this->misc_model->add_attachment_to_database($id, 'garageslogo', [$file]);
                        }
                    }
                    $uploadedFiles_ = handle_task_attachments_array($id,'image');
                    if ($uploadedFiles_ && is_array($uploadedFiles_)) {
                        foreach ($uploadedFiles_ as $file) {
                            $this->misc_model->add_attachment_to_database($id, 'garagesimage', [$file]);
                        }
                    }
                    set_alert('success', _l('added_successfully', _l('garages')));
                    redirect(admin_url('garages'));
                }
            } else {
                if (!has_permission('garages', '', 'edit')) {
                    access_denied('garages');
                }
                $success = $this->garages_model->update_article($data, $id);
                
                $uploadedFiles = handle_task_attachments_array($id,'logo');
                if ($uploadedFiles && is_array($uploadedFiles)) {
                    foreach ($uploadedFiles as $file) {
                        $this->misc_model->add_attachment_to_database($id, 'garageslogo', [$file]);
                    }
                }
                $uploadedFiles_ = handle_task_attachments_array($id,'image');
                if ($uploadedFiles_ && is_array($uploadedFiles_)) {
                    foreach ($uploadedFiles_ as $file) {
                        $this->misc_model->add_attachment_to_database($id, 'garagesimage', [$file]);
                    }
                }
                
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('garages')));
                }
                // redirect(admin_url('garages'));
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
        if ($id == '') {
            $title = _l('add_new', _l('garages'));
        } else {
            $article         = $this->garages_model->get($id);
           // echo '<pre>'; print_r($article); die;
            $data['article'] = $article;
            $title           = _l('edit', _l('garages')) . ' ' . $article->subject;
        }
        $header_text = title_text('aside_menu_active', 'garages');
        $data['heading_text'] = $header_text;
        $data['sh_text'] = $header_text;

        $data['title']     = _l($header_text);
        $this->load->view('admin/garages/garage', $data);
    }

    /* Edit client or add new client*/
    public function garage($id = '')
    {
        if (!has_permission('garages', '', 'view')) {
            if ($id != '' && !is_customer_admin($id)) {
                access_denied('garages');
            }
        }
        
        $header_text = title_text('aside_menu_active', 'garages');
        $data['heading_text'] = $header_text;

        if ($this->input->post() && !$this->input->is_ajax_request()) {
            if ($id == '') {
                if (!has_permission('garages', '', 'create')) {
                    access_denied('garages');
                }

                $data = $this->input->post();
               // echo '<pre>'; print_r($data); die;
                $save_and_add_contact = false;
                if (isset($data['save_and_add_contact'])) {
                    unset($data['save_and_add_contact']);
                    $save_and_add_contact = true;
                }
                $id = $this->garages_model->add($data);
                if (!has_permission('garages', '', 'view')) {
                    $assign['customer_admins']   = [];
                    $assign['customer_admins'][] = get_staff_user_id();
                    $this->garages_model->assign_admins($assign, $id);
                }
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('client')));
                    if ($save_and_add_contact == false) {
                        redirect(admin_url('garages/garage/' . $id));
                    } else {
                        redirect(admin_url('garages/garage/' . $id . '?group=contacts&new_contact=true'));
                    }
                }
            } else {
                if (!has_permission('customers', '', 'edit')) {
                    if (!is_customer_admin($id)) {
                        access_denied('customers');
                    }
                }
                $success = $this->garages_model->update($this->input->post(), $id);
                if ($success == true) {
                    set_alert('success', _l('updated_successfully', _l('client')));
                }
                redirect(admin_url('garages/garage/' . $id));
            }
        }

        $group         = !$this->input->get('group') ? 'profile' : $this->input->get('group');
        $data['group'] = $group;

        if ($group != 'contacts' && $contact_id = $this->input->get('contactid')) {
            redirect(admin_url('garages/garage/' . $id . '?group=contacts&contactid=' . $contact_id));
        }

        // Customer groups
       // $data['groups'] = $this->garages_model->get_groups();

        if ($id == '') {
            $title = _l('add_new', _l('client_lowercase'));
        } else {
            $client                = $this->garages_model->get($id);
            $data['article'] = $client;
            //echo '<pre>'; print_r($client); die;
           // $data['customer_tabs'] = get_customer_profile_tabs();
            //$data['customer_tabs'] = get_customer_garage_tabs();
            $customer_tabs = [
                  'profile' => array
			        (
			            'slug' => 'profile',
			            'name' => 'Profile',
			            'icon' => 'fa fa-user-circle',
			            'view' => 'admin/garages/groups/profile',
			            'position' => 5,
			            'href' => '#',
			            'children' => array
			                (
			                )

			        ),

			    'contacts' => array
			        (
			            'slug' => 'contacts',
			            'name' => 'Contacts',
			            'icon' => 'fa fa-users',
			            'view' => 'admin/garages/groups/contacts',
			            'position' => 10,
			            'href' => '#',
			            'children' => array
			                (
			                )

			        ),

			    'notes' => array
			        (
			            'slug' => 'notes',
			            'name' => 'Notes',
			            'icon' => 'fa fa-sticky-note-o',
			            'view' => 'admin/garages/groups/notes',
			            'position' => 15,
			            'href' => '#',
			            'children' => array
			                (
			                ),

			        ),

			    'statement' => array
			        (
			            'slug' => 'statement',
			            'name' => 'Statement',
			            'icon' => 'fa fa-area-chart',
			            'view' => 'admin/garages/groups/statement',
			            'visible' => 1,
			            'position' => '20',
			            'href' => '#',
			            'children' => array
			                (
			                )

			        ),

			    'invoices' => array
			        (
			            'slug' => 'invoices',
			            'name' => 'Invoices',
			            'icon' => 'fa fa-file-text',
			            'view' => 'admin/garages/groups/invoices',
			            'visible' => '1',
			            'position' => '25',
			            'href' => '#',
			            'children' => array
			                (
			                )

			        ),

			    'payments' => array
			        (
			            'slug' => 'payments',
			            'name' => 'Payments',
			            'icon' => 'fa fa-line-chart',
			            'view' => 'admin/garages/groups/payments',
			            'visible' => 1,
			            'position' => 30,
			            'href' => '#',
			            'children' => array
			                (
			                )

			        ),

			    'proposals' => array
			        (
			            'slug' => 'proposals',
			            'name' => 'Proposals',
			            'icon' => 'fa fa-file-powerpoint-o',
			            'view' => 'admin/garages/groups/proposals',
			            'visible' => '1',
			            'position' => '35',
			            'href' => '#',
			            'children' => array
			                (
			                )

			        ),

			    'credit_notes' => array
			        (
			            'slug' => 'credit_notes',
			            'name' => 'Credit Notes',
			            'icon' => 'fa fa-sticky-note-o',
			            'view' => 'admin/garages/groups/credit_notes',
			            'visible' => '1',
			            'position' => '40',
			            'href' => '#',
			            'children' => array
			                (
			                )

			        ),

			    'estimates' => array
			        (
			            'slug' => 'estimates',
			            'name' => 'Estimates',
			            'icon' => 'fa fa-clipboard',
			            'view' => 'admin/garages/groups/estimates',
			            'visible' => '1',
			            'position' => '45',
			            'href' => '#',
			            'children' => array
			                (
			                )

			        ),

			    'subscriptions' => array
			        (
			            'slug' => 'subscriptions',
			            'name' => 'Subscriptions',
			            'icon' => 'fa fa-repeat',
			            'view' => 'admin/garages/groups/subscriptions',
			            'visible' => '1',
			            'position' => '50',
			            'href' => '#',
			            'children' => array
			                (
			                )

			        ),

			    'expenses' => array
			        (
			            'slug' => 'expenses',
			            'name' => 'Expenses',
			            'icon' => 'fa fa-file-text-o',
			            'view' => 'admin/garages/groups/expenses',
			            'visible' => '1',
			            'position' => '55',
			            'href' => '#',
			            'children' => array
			                (
			                )

			        ),

			    'contracts' => array
			        (
			            'slug' => 'contracts',
			            'name' => 'Contracts',
			            'icon' => 'fa fa-file',
			            'view' => 'admin/garages/groups/contracts',
			            'visible' => '1',
			            'position' => '60',
			            'href' => '#',
			            'children' => array
			                (
			                )

			        ),

			    'projects' =>array
			        (
			            'slug' => 'projects',
			            'name' => 'Projects',
			            'icon' => 'fa fa-bars',
			            'view' => 'admin/garages/groups/projects',
			            'position' => '65',
			            'href' => '#',
			            'children' => array
			                (
			                )

			        ),

			    'tasks' => array
			        (
			            'slug' => 'tasks',
			            'name' => 'Tasks',
			            'icon' => 'fa fa-tasks',
			            'view' => 'admin/garages/groups/tasks',
			            'position' => '70',
			            'href' => '#',
			            'children' => array
			                (
			                )

			        ),

			    'tickets' => array
			        (
			            'slug' => 'tickets',
			            'name' => 'Tickets',
			            'icon' => 'fa fa-ticket',
			            'view' => 'admin/garages/groups/tickets',
			            'visible' => '1',
			            'position' => '75',
			            'href' => '#',
			            'children' => array
			                (
			                )

			        ),

			    'attachments' => array
			        (
			            'slug' => 'attachments',
			            'name' => 'Files',
			            'icon' => 'fa fa-paperclip',
			            'view' => 'admin/garages/groups/attachments',
			            'position' => '80',
			            'href' => '#',
			            'children' => array
			                (
			                )

			        ),

			    'vault' => array
			        (
			            'slug' => 'vault',
			            'name' => 'Vault',
			            'icon' => 'fa fa-lock',
			            'view' => 'admin/garages/groups/vault',
			            'position' => '85',
			            'href' => '#',
			            'children' => array
			                (
			                )

			        ),

			    'reminders' => array
			        (
			            'slug' => 'reminders',
			            'name' => 'Reminders',
			            'icon' => 'fa fa-clock-o',
			            'view' => 'admin/garages/groups/reminders',
			            'position' => '90',
			            'href' => '#',
			            'children' => array
			                (
			                )
			        ),
			    'map' => array
			        (
			            'slug' => 'map',
			            'name' => 'Map',
			            'icon' => 'fa fa-map-marker',
			            'view' => 'admin/garages/groups/map',
			            'position' => '95',
			            'href' => '#',
			            'children' => array
			                (
			                )

			        )
                ];
            $data['customer_tabs'] = $customer_tabs;
            if (!$client) {
                show_404();
            }

           // $data['contacts'] = $this->garages_model->get_contacts($id);
            $data['tab']      = isset($data['customer_tabs'][$group]) ? $data['customer_tabs'][$group] : null;
//echo '<pre>'; print_r($data['customer_tabs']); die;
            if (!$data['tab']) {
                show_404();
            }

            // Fetch data based on groups
            if ($group == 'profile') {
                //$data['customer_groups'] = $this->garages_model->get_customer_groups($id);
                //$data['customer_admins'] = $this->garages_model->get_admins($id);
            } elseif ($group == 'attachments') {
                $data['attachments'] = get_all_customer_attachments($id);
            } elseif ($group == 'vault') {
                $data['vault_entries'] = hooks()->apply_filters('check_vault_entries_visibility', $this->garages_model->get_vault_entries($id));

                if ($data['vault_entries'] === -1) {
                    $data['vault_entries'] = [];
                }
            } elseif ($group == 'estimates') {
                $this->load->model('estimates_model');
                $data['estimate_statuses'] = $this->estimates_model->get_statuses();
            } elseif ($group == 'invoices') {
                $this->load->model('invoices_model');
                $data['invoice_statuses'] = $this->invoices_model->get_statuses();
            } elseif ($group == 'credit_notes') {
                $this->load->model('credit_notes_model');
                $data['credit_notes_statuses'] = $this->credit_notes_model->get_statuses();
                $data['credits_available']     = $this->credit_notes_model->total_remaining_credits_by_customer($id);
            } elseif ($group == 'payments') {
                $this->load->model('payment_modes_model');
                $data['payment_modes'] = $this->payment_modes_model->get();
            } elseif ($group == 'notes') {
                $data['user_notes'] = $this->misc_model->get_notes_garage($id, 'garage');
            } elseif ($group == 'projects') {
                $this->load->model('projects_model');
                $data['project_statuses'] = $this->projects_model->get_project_statuses();
            } elseif ($group == 'statement') {
                if (!has_permission('invoices', '', 'view') && !has_permission('payments', '', 'view')) {
                    set_alert('danger', _l('access_denied'));
                    redirect(admin_url('garages/garage/' . $id));
                }

                $data = array_merge($data, prepare_mail_preview_data('customer_statement', $id));
            } elseif ($group == 'map') {
                if (get_option('google_api_key') != '' && !empty($client->latitude) && !empty($client->longitude)) {
                    $this->app_scripts->add('map-js', base_url($this->app_scripts->core_file('assets/js', 'map.js')) . '?v=' . $this->app_css->core_version());

                    $this->app_scripts->add('google-maps-api-js', [
                        'path'       => 'https://maps.googleapis.com/maps/api/js?key=' . get_option('google_api_key') . '&callback=initMap',
                        'attributes' => [
                            'async',
                            'defer',
                            'latitude'       => "$client->latitude",
                            'longitude'      => "$client->longitude",
                            'mapMarkerTitle' => "$client->company",
                        ],
                        ]);
                }
            }

            $data['staff'] = $this->staff_model->get('', ['active' => 1]);

            $data['client'] = $client;
            $title          = $client->name;

            // Get all active staff members (used to add reminder)
            $data['members'] = $data['staff'];
/*
            if (!empty($data['client']->name)) {
                // Check if is realy empty client company so we can set this field to empty
                // The query where fetch the client auto populate firstname and lastname if company is empty
                if (is_empty_customer_company($data['client']->userid)) {
                    $data['client']->name = '';
                }
            }
            */
        }

        $data['title']     = $title;

        $this->load->view('admin/garages/client', $data);
    }

    public function save() {
        // If file uploaded
        if(is_uploaded_file($_FILES['fileURL']['tmp_name'])) {                            
            // Parse data from CSV file
            $csvData = $this->csvreader->parse_csv($_FILES['fileURL']['tmp_name']);       
            //echo '<pre>'; print_r($csvData); die;
            // create array from CSV file
           if(!empty($csvData)){
                foreach($csvData as $element){                    
                    // Prepare data for Database insertion
                    $data = array(
                        'name' => $element['Name'],
                        'mobile_one' => $element['Mobile First'],
                        'mobile_second' => $element['Mobile Second'],
                        'address' => $element['Address'],
                        'from_company' => $element['From Company'],
                        'from_vendore' => $element['From Vendor'],
                    );
                    //echo '<pre>'; print_r($data); die;
                    $this->db->insert(db_prefix() . 'master_garages_reminder', $data);
                    $data[] = '';
                }
                set_alert('success', _l('Data are stored successful!'));
            }
            else
            {
                set_alert('warning', _l('File is required'));
            }
        }
        else
        {
            set_alert('warning', _l('File is required'));
        } 
        redirect(admin_url('garages'));
    }
    
    
    /* Garage an login as client */
    public function login_as_garage($id)
    {
        if (is_admin()) {
            login_as_garage($id);
        }
        hooks()->do_action('after_contact_login');
        redirect(site_url());
    }
    
    // export Data
    public function sampledata() {
        $storData = array();
        $data[] = array('name' => 'Name', 'mobile_one' => 'Mobile First', 'mobile_second' => 'Mobile Second', 'address' => 'Address', 'from_company' => 'From Company', 'from_vendore' => 'From Vendor');       
        
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"csv-sample-customer".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        $handle = fopen('php://output', 'w');
        foreach ($data as $data) {
            fputcsv($handle, $data);
        }
            fclose($handle);
        exit;
    }
    
     /* Change client status / active / inactive */
    public function change_client_status($id, $status)
    {
        if ($this->input->is_ajax_request()) {
            $postdata['status'] = $status;
            $this->db->where('id', $id);
            $this->db->update(db_prefix().'garage', $postdata);
           // set_alert('success', _l('Status update successful!'));
            exit;
        }
    }

    /* Delete article from database */
    public function delete_garages($id)
    {
        if (!has_permission('garages', '', 'delete')) {
            access_denied('garages');
        }
        if (!$id) {
            redirect(admin_url('garages'));
        }
        $response = $this->garages_model->delete_article($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('garages')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('garages')));
        }
        redirect(admin_url('garages'));
    }
}
