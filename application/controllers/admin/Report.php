<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Report extends AdminController
{
    /* List all report */
    public function index()
    {
        if (!has_permission('report', '', 'view')) {
            access_denied('report');
        }

        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('report');
        }

        $header_text = title_text('aside_menu_active', 'report');
        $data['heading_text'] = $header_text;
        $data['sh_text'] = $header_text;
        $data['title']   = _l($header_text);
        $this->load->view('admin/report/reports', $data);
    }
}
