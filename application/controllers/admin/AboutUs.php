<?php

defined('BASEPATH') or exit('No direct script access allowed');

class AboutUs extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    /* List all announcements */
    public function index()
    {
        if (!has_permission('content', '', 'view')) {
            access_denied('content');
        }
        if($this->input->post())
        {
            $postData['description'] = $this->input->post('description');
            $this->db->where('id', 1);
            $this->db->update(db_prefix().'content',$postData);
            set_alert('success', _l('updated_successfully', _l('About Us')));
            redirect(admin_url('aboutUs'));
        }
      
        $subheader_text = setupTitle_text('aside_menu_active', 'content', 'aboutUs');
        $data['aboutus'] = $this->db->get_where(db_prefix().'content', array('id' => 1))->row('description');
        $data['sheading_text'] = $subheader_text;
        $data['sh_text'] = $subheader_text;
        $data['title'] = $subheader_text;
        $this->load->view('admin/content/content', $data);
    }
}
