<?php

defined('BASEPATH') or exit('No direct script access allowed');

class GarageType extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('garagetype_model');
    }

    /* List all knowledgebase articles */
    public function index()
    {
        if (!has_permission('garageType', '', 'view')) {
            access_denied('garageType');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('garageType');
        }
       
        $subheader_text = setupTitle_text('setup_menu_active', 'master', 'garageType');
        $data['sheading_text'] = $subheader_text;
        $data['sh_text'] = $subheader_text;
        
        $data['title']     = _l($subheader_text);
        $this->load->view('admin/garageType/garageTypes', $data);
    }

    /* Add new article or edit existing*/
    public function add($id = '')
    {
        if (!has_permission('garageType', '', 'view')) {
            access_denied('garageType');
        }
        if ($this->input->post()) {
            $data                = $this->input->post();
            
            if ($id == '') {
                if (!has_permission('garageType', '', 'create')) {
                    access_denied('garageType');
                }
                
                $existname = $this->db->get_where(db_prefix().'garage_type', array('name' => $data['name']))->row('name');
                if($existname)
                {
                    set_alert('warning', _l($data['name'].' name is existing'));
                    redirect(admin_url('garageType'));
                }
                else
                {
                    $data['created_date'] = date('Y-m-d h:i:s');
                    $id = $this->garagetype_model->add_article($data);
                    if ($id) {
                        set_alert('success', _l('added_successfully', _l('Garage Type')));
                        redirect(admin_url('garageType'));
                    }
                }
            } else {
                if (!has_permission('garageType', '', 'edit')) {
                    access_denied('garageType');
                }
                $success = $this->garagetype_model->update_article($data, $id);
                
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('Garage Type')));
                }
                redirect(admin_url('garageType'));
            }
        }
        if ($id == '') {
            $title = _l('add_new', _l('Garage Type'));
        } else {
            $article         = $this->garagetype_model->get($id);
           // echo '<pre>'; print_r($article); die;
            $data['article'] = $article;
            $title           = _l('edit', _l('Garage Type')) . ' ' . $article->name;
        }
        $subheader_text = setupTitle_text('setup_menu_active', 'master', 'garageType');
        $data['sheading_text'] = $subheader_text;
        $data['sh_text'] = $subheader_text;

        $data['title']     = _l($subheader_text);
        $this->load->view('admin/garageType/garageType', $data);
    }

    public function save() {
        // If file uploaded
        if(is_uploaded_file($_FILES['fileURL']['tmp_name'])) {                            
            // Parse data from CSV file
            $csvData = $this->csvreader->parse_csv($_FILES['fileURL']['tmp_name']);       
            //echo '<pre>'; print_r($csvData); die;
            // create array from CSV file
           if(!empty($csvData)){
                foreach($csvData as $element){                    
                    // Prepare data for Database insertion
                    $data = array(
                        'name' => $element['Name'],
                        'mobile_one' => $element['Mobile First'],
                        'mobile_second' => $element['Mobile Second'],
                        'address' => $element['Address'],
                        'from_company' => $element['From Company'],
                        'from_vendore' => $element['From Vendor'],
                    );
                    //echo '<pre>'; print_r($data); die;
                    $this->db->insert(db_prefix() . 'master_garageType_reminder', $data);
                    $data[] = '';
                }
                set_alert('success', _l('Data are stored successful!'));
            }
            else
            {
                set_alert('warning', _l('File is required'));
            }
        }
        else
        {
            set_alert('warning', _l('File is required'));
        } 
        redirect(admin_url('garageType'));
    }
    
    // export Data
    public function sampledata() {
        $storData = array();
        $data[] = array('name' => 'Name', 'mobile_one' => 'Mobile First', 'mobile_second' => 'Mobile Second', 'address' => 'Address', 'from_company' => 'From Company', 'from_vendore' => 'From Vendor');       
        
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"csv-sample-customer".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        $handle = fopen('php://output', 'w');
        foreach ($data as $data) {
            fputcsv($handle, $data);
        }
            fclose($handle);
        exit;
    }

    /* Delete article from database */
    public function delete_garageType($id)
    {
        if (!has_permission('garageType', '', 'delete')) {
            access_denied('garageType');
        }
        if (!$id) {
            redirect(admin_url('garageType'));
        }
        $response = $this->garagetype_model->delete_article($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('Garage Type')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('Garage Type')));
        }
        redirect(admin_url('garageType'));
    }
}
