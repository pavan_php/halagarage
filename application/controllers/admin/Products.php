<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Products extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    /* Function: @products  */
    public function index()
    {
        if (!has_permission('products', '', 'view')) {
            access_denied('products');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('products');
        }
      
        $header_text = title_text('aside_menu_active', 'products');
        $data['heading_text'] = $header_text;
        $data['title']    = _l($header_text);
        $this->load->view('admin/products/products', $data);
    }
}