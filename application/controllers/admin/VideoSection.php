<?php

defined('BASEPATH') or exit('No direct script access allowed');

class VideoSection extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('videosection_model');
        $this->load->library('CSVReader');
    }

    /* List all articles */
    public function index()
    {
        if (!has_permission('videoSection', '', 'view')) {
            access_denied('videoSection');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('videoSection');
        }
       
        $sheader_text = title_text('aside_menu_active', 'videoSection');
        $data['sheading_text'] = $sheader_text;
        $data['sh_text'] = $sheader_text;
        
        $data['title']     = _l($sheader_text);
        $data['video_home'] = $this->db->get_where(db_prefix().'video_section', array('id' => 1))->row(); 
        $data['video_customer'] = $this->db->get_where(db_prefix().'video_section', array('id' => 2))->row(); 
        $data['video_garage'] = $this->db->get_where(db_prefix().'video_section', array('id' => 3))->row(); 
        $this->load->view('admin/videoSection/videoSections', $data);
    }

    /* sub_videoSection */
    public function subvideoSection()
    {
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('sub_videoSection');
        }
    }
    
    /**
    *   @Function: homeVideo_update
    **/
    public function homeVideo_update()
    {
        if (!has_permission('videoSection', '', 'create')) {
            access_denied('videoSection');
        }
        if ($this->input->post()) 
        {
            $id = 1;
            if($_FILES)
            {
                $this->db->where('rel_id', $id);
                $this->db->where('rel_type', 'homevideo');
                $attachment = $this->db->get(db_prefix() . 'files')->row();
        
                if ($attachment) {
                    if (empty($attachment->external)) {
                        $relPath  = get_upload_path_by_type('homevideo') . $attachment->rel_id . '/';
                        $fullPath = $relPath . $attachment->file_name;
                        unlink($fullPath);
                    }
        
                    $this->db->where('id', $attachment->id);
                    $this->db->delete(db_prefix() . 'files');
                    if ($this->db->affected_rows() > 0) {
                        $deleted = true;
                    }
                }
                
                $uploadedFiles = handle_task_attachments_array($id,'homevideo');
                if ($uploadedFiles && is_array($uploadedFiles)) {
                    foreach ($uploadedFiles as $file) {
                        $this->misc_model->add_attachment_to_database($id, 'homevideo', [$file]);
                    }
                }
            }
            $data['code'] = $this->input->post('code');
            $this->db->where('id', $id);
            $this->db->update(db_prefix().'video_section', $data);
            
            set_alert('success', _l('updated_successfully', _l('Update home video')));
            redirect(admin_url('videoSection'));
        }
        else
        {
            set_alert('warning', _l('Problem ', _l('update home video')));
            redirect(admin_url('videoSection'));
        }
    }
    
    
    /**
    *   @Function: customerVideo_update
    **/
    public function customerVideo_update()
    {
        if (!has_permission('videoSection', '', 'create')) {
            access_denied('videoSection');
        }
        if ($this->input->post()) 
        {
            $id = 2;
            if($_FILES)
            {
                $this->db->where('rel_id', $id);
                $this->db->where('rel_type', 'customervideo');
                $attachment = $this->db->get(db_prefix() . 'files')->row();
        
                if ($attachment) {
                    if (empty($attachment->external)) {
                        $relPath  = get_upload_path_by_type('customervideo') . $attachment->rel_id . '/';
                        $fullPath = $relPath . $attachment->file_name;
                        unlink($fullPath);
                    }
        
                    $this->db->where('id', $attachment->id);
                    $this->db->delete(db_prefix() . 'files');
                    if ($this->db->affected_rows() > 0) {
                        $deleted = true;
                    }
                }
                
                $uploadedFiles = handle_task_attachments_array($id,'customervideo');
                if ($uploadedFiles && is_array($uploadedFiles)) {
                    foreach ($uploadedFiles as $file) {
                        $this->misc_model->add_attachment_to_database($id, 'customervideo', [$file]);
                    }
                }
            }
            $data['code'] = $this->input->post('code');
            $this->db->where('id', $id);
            $this->db->update(db_prefix().'video_section', $data);
            
            set_alert('success', _l('updated_successfully', _l('Update customer video')));
            redirect(admin_url('videoSection'));
        }
        else
        {
            set_alert('warning', _l('Problem ', _l('update home video')));
            redirect(admin_url('videoSection'));
        }
    }
    
    /**
    *   @Function: garageVideo_update
    **/
    public function garageVideo_update()
    {
        if (!has_permission('videoSection', '', 'create')) {
            access_denied('videoSection');
        }
        if ($this->input->post()) 
        {
            $id = 3;
            if($_FILES)
            {
                $this->db->where('rel_id', $id);
                $this->db->where('rel_type', 'garagevideo');
                $attachment = $this->db->get(db_prefix() . 'files')->row();
        
                if ($attachment) {
                    if (empty($attachment->external)) {
                        $relPath  = get_upload_path_by_type('garagevideo') . $attachment->rel_id . '/';
                        $fullPath = $relPath . $attachment->file_name;
                        unlink($fullPath);
                    }
        
                    $this->db->where('id', $attachment->id);
                    $this->db->delete(db_prefix() . 'files');
                    if ($this->db->affected_rows() > 0) {
                        $deleted = true;
                    }
                }
                
                $uploadedFiles = handle_task_attachments_array($id,'garagevideo');
                if ($uploadedFiles && is_array($uploadedFiles)) {
                    foreach ($uploadedFiles as $file) {
                        $this->misc_model->add_attachment_to_database($id, 'garagevideo', [$file]);
                    }
                }
            }
            $data['code'] = $this->input->post('code');
            $this->db->where('id', $id);
            $this->db->update(db_prefix().'video_section', $data);
            
            set_alert('success', _l('updated_successfully', _l('Update customer video')));
            redirect(admin_url('videoSection'));
        }
        else
        {
            set_alert('warning', _l('Problem ', _l('update home video')));
            redirect(admin_url('videoSection'));
        }
    }

    /* Add new article or edit existing*/
    public function add($id = '')
    {
        if (!has_permission('videoSection', '', 'view')) {
            access_denied('videoSection');
        }
        if ($this->input->post()) {
            $data                = $this->input->post();
            //echo '<pre>'; print_r($data); die;
            if ($id == '') {
                if (!has_permission('videoSection', '', 'create')) {
                    access_denied('videoSection');
                }
                $id = $this->videosection_model->add_article($data);
                if ($id) {
                    
                    $uploadedFiles = handle_task_attachments_array($id,'videoSection');
                    if ($uploadedFiles && is_array($uploadedFiles)) {
                        foreach ($uploadedFiles as $file) {
                            $this->misc_model->add_attachment_to_database($id, 'videoSection', [$file]);
                        }
                    }
                    set_alert('success', _l('added_successfully', _l('videoSection')));
                    redirect(admin_url('videoSection'));
                }
            } else {
                if (!has_permission('videoSection', '', 'edit')) {
                    access_denied('videoSection');
                }
                $success = $this->videosection_model->update_article($data, $id);
                
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('videoSection')));
                }
                redirect(admin_url('videoSection'));
            }
        }
        if ($id == '') {
            $title = _l('add_new', _l('videoSection'));
            $data['videoSection_result'] = $this->db->get_where(db_prefix().'videoSection', array('parent_id' => 0))->result();
        } else {
            $article         = $this->videosection_model->get($id);
            //echo '<pre>'; print_r($article); die;
            $data['article'] = $article;
            $data['videoSection_result'] = $this->db->get_where(db_prefix().'videoSection', array('parent_id' => 0, 'id != '=> $id))->result();
        }
        $sheader_text = title_text('aside_menu_active', 'videoSection');
        $data['sheading_text'] = $sheader_text;
        $data['sh_text'] = $sheader_text;
        
        $data['title']     = _l($sheader_text);
        $this->load->view('admin/videoSection/videoSection', $data);
    }
    
    /* Edit sub videoSection */
    public function edit($id = '')
    {
        if (!has_permission('videoSection', '', 'view')) {
            access_denied('videoSection');
        }
        if ($this->input->post()) {
            $data                = $this->input->post();
            //echo '<pre>'; print_r($data); die;
            if ($id == '') {
                if (!has_permission('videoSection', '', 'create')) {
                    access_denied('videoSection');
                }
                $id = $this->videosection_model->add_article($data);
                if ($id) {
                    
                    $uploadedFiles = handle_task_attachments_array($id,'videoSection');
                    if ($uploadedFiles && is_array($uploadedFiles)) {
                        foreach ($uploadedFiles as $file) {
                            $this->misc_model->add_attachment_to_database($id, 'videoSection', [$file]);
                        }
                    }
                    
                    set_alert('success', _l('added_successfully', _l('videoSection')));
                    redirect(admin_url('videoSection'));
                }
            } else {
                if (!has_permission('videoSection', '', 'edit')) {
                    access_denied('videoSection');
                }
                $success = $this->videosection_model->update_article($data, $id);
                
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('Sub videoSection')));
                }
                redirect(admin_url('videoSection'));
            }
        }
        if ($id == '') {
            $title = _l('add_new', _l('videoSection'));
            $data['videoSection_result'] = $this->db->get_where(db_prefix().'videoSection', array('parent_id' => 0))->result();
        } else {
            $article         = $this->videosection_model->get($id);
            //echo '<pre>'; print_r($article); die;
            $data['article'] = $article;
            $data['videoSection_result'] = $this->db->get_where(db_prefix().'videoSection', array('parent_id' => 0, 'id != '=> $id))->result();
        }
        $sheader_text = title_text('aside_menu_active', 'videoSection');
        $data['sheading_text'] = $sheader_text;
        $data['sh_text'] = $sheader_text;
        
        $data['title']     = _l($sheader_text);
        $this->load->view('admin/videoSection/sub_videoSection', $data);
    }

    public function save() {
        // If file uploaded
        if(is_uploaded_file($_FILES['fileURL']['tmp_name'])) {                            
            // Parse data from CSV file
            $csvData = $this->csvreader->parse_csv($_FILES['fileURL']['tmp_name']);       
            //echo '<pre>'; print_r($csvData); die;
            // create array from CSV file
            if(!empty($csvData)){
                foreach($csvData as $element){                    
                    // Prepare data for Database insertion
                    $data = array(
                        'name' => $element['videoSection']
                    );
                    $exitvideoSection = $this->db->get_where(db_prefix().'videoSection', array('name' => $data['name']))->row('name');
                    if($exitvideoSection != '')
                    {
                        
                    }
                    else
                    {
                        $this->db->insert(db_prefix() . 'videoSection', $data);
                        $data[] = '';
                    }
                }
                set_alert('success', _l('Data are stored successful!'));
            }
            else
            {
                set_alert('warning', _l('File is required'));
            }
        }
        else
        {
            set_alert('warning', _l('File is required'));
        } 
        redirect(admin_url('videoSection'));
    }
    
    // export Data
    public function sampledata() {
        $storData = array();
        $data[] = array('name' => 'videoSection');       
        
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"csv-sample-videoSection".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        $handle = fopen('php://output', 'w');
        foreach ($data as $data) {
            fputcsv($handle, $data);
        }
            fclose($handle);
        exit;
    }

    /* Delete article from database */
    public function delete_videoSection($id)
    {
        if (!has_permission('videoSection', '', 'delete')) {
            access_denied('videoSection');
        }
        if (!$id) {
            redirect(admin_url('videoSection'));
        }
        $response = $this->videosection_model->delete_article($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('videoSection')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('videoSection')));
        }
        redirect(admin_url('videoSection'));
    }
}
