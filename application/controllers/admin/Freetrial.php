<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Freetrial extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    /* List all freetrial */
    public function index()
    {
        if (!has_permission('freetrial', '', 'view')) {
            access_denied('freetrial');
        }
        if($this->input->post())
        {
            $postData['free_limit'] = $this->input->post('free_limit');
            $postData['status'] = $this->input->post('status');
            $this->db->where('id', 1);
            $this->db->update(db_prefix().'free_trial',$postData);
            set_alert('success', _l('updated_successfully', _l('Free trail day')));
            redirect(admin_url('freetrial'));
        }
      
        $data['daysLimit_res'] = $this->db->get_where(db_prefix().'free_trial')->row();
        $header_text = title_text('aside_menu_active', 'freetrial');
        $data['heading_text'] = $header_text;
        $data['sh_text'] = $header_text;
        $data['title']   = _l($header_text);
        $this->load->view('admin/freetrial/freetrial', $data);
    }
}
