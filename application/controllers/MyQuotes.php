<?php

defined('BASEPATH') or exit('No direct script access allowed');

use app\services\ValidatesContact;

class MyQuotes extends ClientsController
{
    use ValidatesContact;
    
    public function __construct()
    {
        parent::__construct();
        hooks()->do_action('clients_authentication_constructor', $this);
        if(get_client_role_type() == 1)
        {
           redirect(site_url('garages'));
        }
        $this->load->library('session');
        /*
        $paidstatus = $this->db->order_by('payment_id', 'desc')->limit(1)->get_where(db_prefix().'_payments', array('garage_id' => get_client_user_id(), 'expiry_date >' => time(), 'item_number' => 1))->num_rows();
        if($paidstatus == 0)
        {
            set_alert('warning', _l('Plan expired'));
            redirect(site_url('planExpire'));
        }
        $freetrialstatus = $this->db->get_where(db_prefix().'_payments', array('garage_id' => get_client_user_id()))->num_rows();
        if($freetrialstatus == 0)
        {
            if(garagecreatedate(get_client_user_id()) == 1)
            {
                set_alert('warning', _l('Plan expired'));
                redirect(site_url('planExpire'));
            }
        }
        */
    }
    
    public function index()
    {
        $data['title']     = 'My Quotes';
        $data['confirmResult'] = $this->db->get_where(db_prefix().'quoteRequest_garageReply', array('garage_id' => get_client_user_id(),'booking_request_status'=>1))->result();
        $data['pendingesult'] = $this->db->get_where(db_prefix().'quoteRequest_garageReply', array('garage_id' => get_client_user_id(),'booking_request_status'=>0))->result();
        $data['canceledResult'] = $this->db->get_where(db_prefix().'quoteRequest_garageReply', array('garage_id' => get_client_user_id(),'garage_confirm'=>2))->result();
        //echo '<pre>'; print_r($data['confirmResult']); die;
        $this->data($data);
        $this->view('myQuotes');
        $this->layout();
    }
    
    /**
    *   @Function: Remove Quotes
    */
    public function removeQuotes_ols()
    {
        $data['remove_id'] = 1;
        $id = $_POST['id'];
        $garageid = $_POST['garageid'];
        $this->db->where('quote_id', $id);
        $this->db->update(db_prefix().'quoteRequest_garageReply', $data);
        
        $quoteDetails = $this->db->get_where(db_prefix().'requestAQuote', array('id' => $id))->row('remove_garage_id');
        if($quoteDetails)
        {
            $data_['remove_garage_id'] = $quoteDetails.','.$garageid;
            $this->db->where('id', $id);
            $this->db->update(db_prefix().'requestAQuote', $data_);
        }
        else
        {
            $data_['remove_garage_id'] = $garageid;
            $this->db->where('id', $id);
            $this->db->update(db_prefix().'requestAQuote', $data_);
        }
        echo 1;
    }
    
    
    /**
    *   @Function: Remove Quotes
    */
    public function removeQuotes()
    {
        $data['remove_id'] = 1;
        $id = $_POST['id'];
        $garageid = $_POST['garageid'];
        $this->db->where('quote_id', $id);
        $this->db->update(db_prefix().'quoteRequest_garageReply', $data);
        
        echo 1;
    }
    
    /**
    *   @Function: Remove Quotes
    */
    public function removeCancelQuotes()
    {
        $data['remove_id'] = 1;
        $id = $_POST['id'];
        $garageid = $_POST['garageid'];
        $this->db->where('quote_id', $id);
        $this->db->update(db_prefix().'quoteRequest_garageReply', $data);
    
        $quoteDetails = $this->db->get_where(db_prefix().'requestAQuote', array('id' => $id))->row('remove_garage_id');
        if($quoteDetails)
        {
            $quoteArr = explode(',',$quoteDetails);
            if(in_array($garageid,$quoteArr))
            {}
            else
            {
                array_push($quoteArr, $garageid);
                $quoteStr = implode(',',$quoteArr);
                
                $data_['remove_garage_id'] = $quoteStr;
                $this->db->where('id', $id);
                $this->db->update(db_prefix().'requestAQuote', $data_);
            }
        }
        else
        {
            $data_['remove_garage_id'] = $garageid;
            $this->db->where('id', $id);
            $this->db->update(db_prefix().'requestAQuote', $data_);
        }
        echo 1;
    }
    
    /**
    *   @Function: Remove Quotes
    */
    public function decineQuotes()
    {
        //$data['remove_id'] = 1;
        $id = $_POST['id'];
        
        $garageid = $_POST['garageid'];
   
        $this->db->where('garage_id', $garageid);
        $this->db->where('quote_id', $id);
        $this->db->update(db_prefix().'schedule_appoinment', array('status'=>2));

        $postData['garage_confirm']= '2';
        $postData['remove_id']= 1;
        $this->db->where('quote_id', $id);
        $this->db->where('garage_id', $garageid);
        $this->db->update(db_prefix().'quoteRequest_garageReply', $postData);

        $quoteDetails = $this->db->get_where(db_prefix().'requestAQuote', array('id' => $id))->row('remove_garage_id');
        if($quoteDetails)
        {
            $data_['remove_garage_id'] = $quoteDetails.','.$garageid;
            $this->db->where('id', $id);
            $this->db->update(db_prefix().'requestAQuote', $data_);
        }
        else
        {
            $data_['remove_garage_id'] = $garageid;
            $this->db->where('id', $id);
            $this->db->update(db_prefix().'requestAQuote', $data_);
        }
        echo 1;
    }

    /**
    *   @Function: Remove Quotes
    */
    public function acceptQuate()
    {
        $data['status'] = 4;
        $id = $_POST['id'];
        $garageid = $_POST['garageid'];
        if($id!=''){
            $postData['garage_confirm']= '1';
            $this->db->where('quote_id', $id);
            $this->db->where('garage_id', $garageid);
            $this->db->update(db_prefix().'quoteRequest_garageReply', $postData);
           
            $this->db->where('garage_id', $garageid);
            $this->db->where('quote_id', $id);
            $this->db->update(db_prefix().'schedule_appoinment', $data);
           // echo  $this->db->last_query();
        }
        echo 1;
    }   
}
