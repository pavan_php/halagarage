<?php
//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
defined('BASEPATH') or exit('No direct script access allowed');

use app\services\ValidatesContact;

class Customers extends ClientsController
{
    use ValidatesContact;
    
    public function __construct()
    {
        parent::__construct();
        hooks()->do_action('clients_authentication_constructor', $this);
        if(get_client_role_type() == 2)
        {
            redirect(site_url('garages'));
        }
        $this->load->library('session');
    }

    /**
    *   @Function: Index function
    */
    public function index()
    {
        $data['settingRes'] = $this->db->get_where('tbllogin_setting')->row();
        $data['title'] = 'HalaGarage';
        if(get_client_user_id() != '')
        {
            $data['video_customer'] = $this->db->get_where(db_prefix().'video_section', array('id' => 3))->row(); 
            $this->load->view('front/dashboard', $data);
        }
        else
        {
            $this->load->view('front/index', $data);
        }
    }
    
    /**
    *   @Function: notify
    */
    public function notify()
    {
        $id = get_client_user_id();
        $notifyCount = 0;
        if($id)
        {
            $notifyCount = $this->db->get_where(db_prefix().'quoteRequest_garageReply', array('customer_id' => $id, 'customer_view' => 0))->num_rows();
        }
        echo $notifyCount;
    }
    
    /**
    *   @Function: customer profile
    */
    public function profile()
    {
        $data['title']            = get_company_name(get_client_user_id());
        $data['profileResult'] = $this->db->get_where(db_prefix().'contacts', array('userid' => get_client_user_id()))->row();
        $this->load->view('front/profile', $data);
    }
    
	/**
	*	@Function: Garage profile
	*/
	public function garageProfile($id)
    {
        $data['profileData'] = $this->db->get_where(db_prefix().'clients', array('userid' => $id))->row();
        $data['garageAbountRes'] = $this->db->get_where(db_prefix().'garage_aboutus', array('garage_id' => $id))->row();
        $data['profileBackgroundRes'] = $this->db->get_where(db_prefix().'profile_background', array('garage_id' => $id))->row();
        $data['newsfeedResult'] = $this->db->order_by('id', 'desc')->get_where(db_prefix().'garage_newsfeed', array('garage_id' => $id))->result();
        $data['latestProduct'] = $this->db->order_by('id', 'desc')->limit(3)->get_where(db_prefix().'_product', array('garage_id' => $id))->result();
        $data['garageServicesRes'] = $this->db->order_by('id', 'desc')->limit(3)->get_where(db_prefix().'garage_services', array('garage_id' => $id))->row('service_id');
        $data['garageGalleryRes'] = $this->db->order_by('id', 'desc')->limit(3)->get_where(db_prefix().'garage_gallery', array('garage_id' => $id))->result();
        //echo '<pre>'; print_r($data['garageAbountRes']); die; 
        $data['serviceList'] = $this->db->get_where(db_prefix().'garage_type', array('status' => 1))->result();
        $data['garageRating'] = $this->db->select('AVG(garage_rating) as rating')->get_where(db_prefix().'review_rating', array('garage_id' => $id))->result(); 
        $data['garageRating_count'] = $this->db->get_where(db_prefix().'review_rating', array('garage_id' => $id))->num_rows(); 
        $data['title'] = _l($data['profileData']->company);
        $this->data($data);
        $this->view('garageProfile');
        $this->layout();
    }
	
    /**
    *   @Function: profileUpdate
    **/
    public function profileUpdate()
    {
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $oldpassword = $_POST['oldpassword'];
        $newpassword = $_POST['newpassword'];
        $confirmpassword = $_POST['confirmpassword'];
        $msg = [];
        if($firstname != '' && $lastname != '')
        {
            $data['firstname'] = $firstname;
            $data['lastname'] = $lastname;
            $this->db->where('userid', get_client_user_id());
            $this->db->update(db_prefix().'contacts', $data);
            $s= '';
            if ($this->db->affected_rows() > 0) {
                $s = 1;
            }
            if($newpassword != '')
            {
                if($newpassword == $confirmpassword)
                {
                    $table    = db_prefix() . 'contacts';
                    $password = app_hash_password($newpassword);
                    $postupdate['password'] = $password;
                    $this->db->where('userid', get_client_user_id());
                    //$this->db->update(db_prefix().'contacts', $postupdate);
                    //$this->db->where('id', $id);
                    $this->db->update(db_prefix() . 'contacts', [
                        'last_password_change' => date('Y-m-d H:i:s'),
                        'password'             => app_hash_password($newpassword),
                    ]);
                    if ($this->db->affected_rows() > 0) {
                        $s = 1;
                    }
                    $msg = '';
                    $msg .= '<p>Your password has been change successfully</p>';
                    $msg .= '<p>New password is:'.$newpassword.'</p>';
                    $msg .= '<p><b>Hala Garage Team</b></p>';
                    $email = $this->db->get_where($table, array('userid' => get_client_user_id()))->row('email');
                    send_mail_SMT($email, 'Change Password', $msg);
                    $msg = '';
                }
            }
            if($s == 1)
            {
                $msg['type'] = 'success';
                $msg['msg'] = 'Profile are updated';
            }
        }
        else
        {
            $msg['type'] = 'error';
            $msg['msg'] = 'First and last name are required';
        }
        echo json_encode($msg);
    }
    
    /**
    *   @Function: getpassword
    */
    public function getpassword()
    {
        $oldPassword = $_POST['oldpassword'];
        $this->db->where('userid', get_client_user_id());
        $client = $this->db->get(db_prefix() . 'contacts')->row();

        if (!app_hasher()->CheckPassword($oldPassword, $client->password)) {
            echo ''; die;
        }
        echo 1; die;
        /*
        $table    = db_prefix() . 'contacts';
        $oldpass = $_POST['oldpassword'];
        $password = app_hash_password($oldpass);
        $res = $this->db->get_where($table, array('password' => $password, 'userid' => get_client_user_id()))->num_rows();
        if($res)
        {
            echo 1; die;
        }
        else
        {
            echo ''; die;
        }
        */
    }
    
    /**
    *   @Function: customer profile
    */
    public function garageDetails()
    {
        $data['title']            = get_company_name(get_client_user_id());
        $this->load->view('front/garage-details', $data);
    }
    
    /**
    *   @Function: message function
    */
    public function message($id=null)
    {
        //$data['settingRes'] = $this->db->get_where('tbllogin_setting')->row();
        $data['message_list'] = $this->db->order_by('id', 'desc')->get_where(db_prefix().'message', array('receiver_id' => get_client_user_id()))->result();
        $data['title'] = 'Message';
        $this->load->view('front/message', $data);
    }
    
    /**
    *   @Function:  removemsg
    **/
    function removemsg()
    {
        $id = $_POST['id'];
        if($id)
        {
            $this->db->delete(db_prefix().'message', array('id' => $id));
            echo 1; 
            die;
        }
        echo '';
    }
    
    /**
    *   @Function:  filterctegory
    **/
    function filterctegory()
    {
        $name = $_POST['name'];
        $parentid = $_POST['parentid'];
        if($name)
        {
            $this->db->select('*');
            $this->db->from(db_prefix().'category');
            $this->db->where('parent_id', $parentid);
            $this->db->like('name', $name);
            $query = $this->db->get();
            $data['category_result'] = $query->result();
            $html = $this->load->view('front/filterctegory', $data, true);
            echo $html; 
            die;
        }
        else
        {
            $this->db->select('*');
            $this->db->from(db_prefix().'category');
            $this->db->where('parent_id', $parentid);
            $query = $this->db->get();
            $data['category_result'] = $query->result();
            $html = $this->load->view('front/filterctegory', $data, true);
            echo $html; 
            die;
        }
    }
    
    /**
    *   @Function:  filtersubctegory
    **/
    function filtersubctegory()
    {
        $name = $_POST['name'];
        $parentid = $_POST['parentid'];
        if($name)
        {
            $this->db->select('*');
            $this->db->from(db_prefix().'category');
            $this->db->where('parent_id', $parentid);
            $this->db->like('name', $name);
            $query = $this->db->get();
            $data['category_result'] = $query->result();
            $html = $this->load->view('front/filtersubctegory', $data, true);
            echo $html; 
            die;
        }
        else
        {
            $this->db->select('*');
            $this->db->from(db_prefix().'category');
            $this->db->where('parent_id', $parentid);
            $query = $this->db->get();
            $data['category_result'] = $query->result();
            $html = $this->load->view('front/filtersubctegory', $data, true);
            echo $html; 
            die;
        }
    }
    
    /**
    *   @Function: save message function
    */
    public function messageSave()
    {
       if($this->input->post())
       {
           $postData['sender_id'] = get_client_user_id();
           $postData['token_id'] = rand();
           $postData['title'] = $this->input->post('title');
           $postData['message'] = $this->input->post('message');
           
           $this->db->insert(db_prefix().'message', $postData);
           $lastid = $this->db->insert_id();
           if($lastid)
           {
               $msg = 'success';
               $status = 'Message are send successfully!';
               
           }
           else
           {
                $status = 'error';
                $msg = 'Please try again';
           }
       }
       else
       {
           $status = 'error';
           $msg = 'Fill are all required data';
       }
       $this->session->set_flashdata('status', $status);
       $this->session->set_flashdata('msg', $msg);
       redirect(base_url('customers/message'));
    }
    
    /**
    *   @Function: composemsg
    */
    public function composemsg()
    {
        $msg = '';
        if($this->input->post())
        {
           $postData['sender_id'] = get_client_user_id();
           $postData['token_id'] = rand();
           $postData['title'] = $this->input->post('title');
           $postData['message'] = $this->input->post('message');
           
           $this->db->insert(db_prefix().'message', $postData);
           $lastid = $this->db->insert_id();
           if($lastid)
           {
                $msg = 'Message are send successfully';
           }
           else
           {
                $msg = '';
           }
        }
        else
        {
            $msg = '';
        }
        echo $msg;
    }
    
    /**
    *   @Function: composemsg
    */
    public function replymsg()
    {
        $msg = '';
        if($this->input->post())
        {
           $postData['sender_id'] = get_client_user_id();
           $postData['receiver_id'] = 0;
           $postData['token_id'] = $_POST['tokenID'];
           $postData['title'] = $this->input->post('title');
           $postData['message'] = $this->input->post('message');
           
           $this->db->insert(db_prefix().'message', $postData);
           $lastid = $this->db->insert_id();
           if($lastid)
           {
                $msg = 'Message are send successfully';
           }
           else
           {
                $msg = '';
           }
        }
        else
        {
            $msg = '';
        }
        echo $msg;
    }
    
    /**
    *   @Function: sendboxList
    */
    public function sendboxList()
    {
        $message_list = $this->db->order_by('id', 'desc')->get_where(db_prefix().'message', array('sender_id' => get_client_user_id()))->result();
        $html = '';
        if($message_list)
        {
            $data['message_list'] = $message_list;
            $html = $this->load->view('front/sendboxList', $data);
        }
        echo $html;
    }
    
    /**
    *   @Function: inboxList
    */
    public function inboxList()
    {
        $message_list = $this->db->order_by('id', 'desc')->get_where(db_prefix().'message', array('receiver_id' => get_client_user_id()))->result();
        $html = '';
        if($message_list)
        {
            $data['message_list'] = $message_list;
            $html = $this->load->view('front/inboxList', $data);
        }
        echo $html;
    }
    
    /**
    *   @Function: Request a quote
    */
    public function requestAQuote()
    {
        $data['category_result'] = $this->db->get_where(db_prefix().'category', array('status' => 1, 'parent_id' => 0))->result();
        $data['title'] = 'Request a quote';
        $this->load->view('front/request-new-quote', $data);
    }
    
    /**
    *   Function: getSubCategory
    */
    public function getSubCategory()
    {
        $catid = $_POST['catid'];
        $html = '';
        if($catid)
        {
            $html = $this->db->get_where(db_prefix().'category', array('parent_id' => $catid))->result();
        }
        echo json_encode($html);
    }
    
    /**
    *   Function: getModelList
    */
    public function getModelList()
    {
        $catid = $_POST['catid'];
        $html = '';
        if($catid)
        {
            $html = $this->db->get_where(db_prefix().'car_model', array('make_id' => $catid))->result();
        }
        echo json_encode($html);
    }
    
    /**
    *   Function: getYearList
    */
    public function getYearList()
    {
        $catid = $_POST['catid'];
        $where_in = explode(',', $catid);
        $make_id = $_POST['make_id'];
        $html = '';
        if($catid)
        {
            $this->db->select('*');
            $this->db->from('car_year');
            $this->db->where('make_id', $make_id);
            $this->db->where_in('model_id',$where_in);
            $query = $this->db->get();
            $html = $query->result();
            
            //$html = $this->db->get_where(db_prefix().'car_year', array('model_id' => $catid, 'make_id' => $make_id))->result();
        }
        echo json_encode($html);
    }
    
    /**
    *   Function: getWidthList
    */
    public function getWidthList()
    {
        $catid = $_POST['catid'];
        $html = '';
        if($catid)
        {
            $html = $this->db->get_where(db_prefix().'tyre_width', array('brand_id' => $catid))->result();
        }
        echo json_encode($html);
    }
    
    /**
    *   Function: getHeightList
    */
    public function getHeightList()
    {
        $catid = $_POST['catid'];
        $brand_id = $_POST['brand_id'];
        $html = '';
        if($catid)
        {
            $html = $this->db->get_where(db_prefix().'tyre_profile', array('width_id' => $catid, 'brand_id' => $brand_id))->result();
        }
        echo json_encode($html);
    }
    
    /**
    *   Function: getRimList
    */
    public function getRimList()
    {
        $catid = $_POST['catid'];
        $brand_id = $_POST['brand_id'];
        $width_id = $_POST['width_id'];
        $html = '';
        if($catid)
        {
            $html = $this->db->get_where(db_prefix().'tyre_diameter', array('profile_id' => $catid, 'brand_id' => $brand_id, 'width_id' => $width_id))->result();
        }
        echo json_encode($html);
    }
    
    public function getWidthIds($filterWidth){
         $width  = array();
        $this->db->select('id');
        $this->db->from(db_prefix().'tyre_width');
        $this->db->where_in('name',array_unique($filterWidth)); 
        $query = $this->db->get();
        $rse  = $query->result();
        foreach($rse as $rs){
            $width[] = $rs->id;
        }
        return array_unique($width);  
    }
    
    public function getHeightsIds($filterWidth){
        $Heights  = array();
        $this->db->select('id');
        $this->db->from(db_prefix().'tyre_profile');
        $this->db->where_in('value',array_unique($filterWidth)); 
        $query = $this->db->get();
        $rse  = $query->result();
        foreach($rse as $rs){
            $Heights[] = $rs->id;
        }
        return array_unique($Heights);  
    }
    
    public function getRimSizeIds($filterRimSize){
        $RimSize  = array();
        $this->db->select('id');
        $this->db->from(db_prefix().'tyre_diameter');
        $this->db->where_in('diameter',array_unique($filterRimSize)); 
        $query = $this->db->get();
        $rse  = $query->result();
        foreach($rse as $rs){
            $RimSize[] = $rs->id;
        }
        return array_unique($RimSize);  
    }
     
    /* getBrandList */ 
    public function getBrandList($para1)
    {
        $brandList = $this->db->like('name', $para1)->get_where(db_prefix().'car_make')->result_array();
        $result = [];
        if($brandList)
        {
            foreach($brandList as $rrr)
            {
                $data['checked'] = false;
                $data['location'] = $rrr['name'];
                $data['location_id'] = $rrr['id'];
                $result[] = $data;
            }
        }
        $msg = array('result'=>$result);
        echo json_encode($msg);
    }
     
    /* getBrandList */ 
    public function getTyreList($para1)
    {
        $brandList = $this->db->like('name', $para1)->get_where(db_prefix().'tyre_brand')->result_array();
        /*$html = $this->db->get_where(db_prefix().'tyre_width', array('brand_id' => $catid))->result();*/
        $result = [];
        if($brandList)
        {
            foreach($brandList as $rrr)
            {
                $data['checked'] = false;
                $data['tyres'] = $rrr['name'];
                $data['tyres_id'] = $rrr['id'];
                $result[] = $data;
            }
        }
        $msg = array('result'=>$result);
        echo json_encode($msg);
    }
     
    /**
    *  @Function: filterProduct
    */
    public function filterProduct()
    {
        $car_brand      = $_POST['car_brand'];
        $car_model      = $_POST['car_model'];
        $car_year       = $_POST['car_year'];
        $city_id        = $_POST['city_id'];
        $area_id        = $_POST['area_id'];
        $id             = $_POST['catid'];
        $fittingval     = $_POST['fittingval'];
        
        $brand_name   = $this->db->get_where(db_prefix().'car_make',array('id'=>$car_brand))->row('name');
         
        $model_name   = $this->db->get_where(db_prefix().'car_model',array('id'=>$car_model))->row('name');
         
        $year   = $this->db->get_where(db_prefix().'car_year',array('id'=>$car_year))->row('year');
        
        $ma = [];
        if($brand_name!=''){
            $ma['make'] = $brand_name;
        }
        if($model_name!=''){
            $ma['model'] = $model_name;
        }
        
        if($model_name!=''){
            $ma['year'] = $year;
        }
        
        $Width      =  array();
        $Height     =  array();
        $RimSize    =  array();
         
        if(count($ma) > 0){ 
        
           $widHigRes =  $this->db->get_where(db_prefix().'master_car_tyre',$ma)->result();
          
           foreach($widHigRes as $whVal){
                $filterWidth[]      = $whVal->width;
                $filterHeight[]     = $whVal->height;
                $filterRimSize[]    = $whVal->rim_size;
                $filterVarient[]    = $whVal->varient;
           }
           
            $Width      =  $this->getWidthIds($filterWidth);
            $Height     =  $this->getHeightsIds($filterHeight);
            $RimSize    =  $this->getRimSizeIds($filterRimSize);
        } 
        
        $this->db->select('a.*');
        $this->db->from(db_prefix().'_product as a');
        $this->db->join(db_prefix().'clients b', 'b.userid = a.garage_id','inner');
        //$where = "FIND_IN_SET('".$id."', a.subcategory)";
        //  $this->db->where($where);
        if(count($Width) > 0 ){
            $this->db->where_in('a.width',array_unique($Width)); 
        }    
        if(count($Height) > 0 ){
            $this->db->where_in('a.height',array_unique($Height)); 
        } 
        if(count($RimSize) > 0 ){
            $this->db->where_in('a.rim_size',array_unique($RimSize)); 
        } 
        
        /*if($car_brand > 0)
        {
            $brandarr = explode(',', $car_brand);
            $this->db->where_in('a.car_brand',$brandarr); 
        }
        if($car_model > 0)
        {
            $modelarr = explode(',', $car_model);
            $this->db->where_in('a.car_model',$modelarr); 
        }
        if($car_year > 0)
        {
            $yeararr = explode(',', $car_year);
            $this->db->where_in('a.make_year',$yeararr); 
        }
        */
        if($city_id > 0)
        {
           $this->db->where('b.city', $city_id); 
        }
        
        //$this->db->where('a.fitting_filter', $fittingval);
        
        $query = $this->db->get();
        $data['product_list'] = $query->result();
       //echo $this->db->last_query();
        //die;
        
        $resp = $this->load->view('front/filterProduct', $data, true);
        echo $resp;
        die;
    }
    
    /**
    *  @Function: filterTyre
    */
    public function filterTyre()
    {
        $brand_id       = $_POST['brand_id'];
        $width_id       = $_POST['width_id'];
        $height_id      = $_POST['height_id'];
        $rim_id         = $_POST['rim_id'];
        $city_id        = $_POST['city_id'];
        $area_id        = $_POST['area_id'];
        $id             = $_POST['catid'];
        $fittingval     = $_POST['fittingval'];
        
        
        $this->db->select('a.*');
        $this->db->from(db_prefix().'_product as a');
        $this->db->join(db_prefix().'clients b', 'b.userid = a.garage_id','inner');
        $where = "FIND_IN_SET('".$id."', a.subcategory)";
        $this->db->where($where);
        
        if($brand_id > 0)
        {
            $this->db->where('a.tyre_brand', $brand_id); 
        }
        if($width_id > 0)
        {
            $this->db->where('a.width', $width_id); 
        }
        if($height_id > 0)
        {
            $this->db->where('a.height', $height_id); 
        }
        if($rim_id > 0)
        {
            $this->db->where('a.rim_size', $rim_id); 
        }
        
        if($city_id > 0)
        {
           $this->db->where('b.city', $fittingval); 
        }
        
        $this->db->where('a.fitting_filter', $fittingval);
        
        $query = $this->db->get();
        $data['product_list'] = $query->result();
        
        $resp = $this->load->view('front/filterProduct', $data, true);
        echo $resp;
        die;
    }
    
    /**
    *   Function: getAreaList
    */
    public function getAreaList()
    {
        $catid = $_POST['catid'];
        $html = '';
        if($catid)
        {
            $html = $this->db->get_where(db_prefix().'area', array('city_id' => $catid))->result();
        }
        echo json_encode($html);
    }
    
    /**
    *   Function: removeQuote
    */
    public function removeQuote()
    {
        $catid = $_POST['id'];
        $html = '';
        if($catid)
        {
            $data['remove_by_customer'] = 1;
            $this->db->where('id', $catid);
            $this->db->update(db_prefix().'requestAQuote', $data);
            
            $data_['remove_id'] = 2;
            $this->db->where('quote_id', $catid);
            $this->db->update(db_prefix().'quoteRequest_garageReply', $data_);
            $html = 1;
        }
        echo json_encode($html);
    }
    
    /**
    *   Function: removeQuoteReq
    */
    public function removeQuoteReq()
    {
        $catid = $_POST['id'];
        $html = '';
        if($catid)
        {
            $this->db->delete(db_prefix().'requestAQuote', array('id' => $catid));
            $html = 1;
        }
        echo json_encode($html);
    }
    
    /**
    *   Function: reActivateQuote
    */
    public function reActivateQuote()
    {
        $catid = $_POST['id'];
        $html = '';
        if($catid)
        {
            $data['remove_by_customer'] = 0;
            $this->db->where('id', $catid);
            $this->db->update(db_prefix().'requestAQuote', $data);
            
            $data_['remove_id'] = 0;
            $this->db->where('quote_id', $catid);
            $this->db->update(db_prefix().'quoteRequest_garageReply', $data_);
            $html = 1;
        }
        echo json_encode($html);
    }
    
    /**
    *   Function: sendReplyToGarage
    */
    public function sendReplyToGarage()
    {
        $id = $_POST['id'];
        $qty = $_POST['qty'];
        $msg = $_POST['msg'];
        $html = '';
        if($id)
        {
            $data['qty'] = $qty;
            $data['customer_msg'] = $msg;
            $this->db->where('id', $id);
            $this->db->update(db_prefix().'quoteRequest_garageReply', $data);
            
            $html = 1;
            
            $quoteDetails = $this->db->select('customer_id,garage_id')->get_where(db_prefix().'quoteRequest_garageReply', array('id' => $id))->row();
            
            /** Message chat **/
            $chatdata['chat_from'] = $quoteDetails->customer_id;
            $chatdata['chat_with'] = $quoteDetails->garage_id;
            $chatdata['create_timestamp'] = date('Y-m-d H:i:s');
            //$chatdata['msg'] = $qty.'qty - '.$msg;
            $chatdata['msg'] = $msg;
            $this->db->insert(db_prefix().'_chat', $chatdata);
        }
        echo json_encode($html);
    }
    
    /**
    *   @Function: My Quote
    */
    public function myQuote()
    {
        $data['title'] = 'My Quote';
        $data['quoteResult'] = $this->db->select('id,title,created_date')->order_by('id', 'desc')->get_where(db_prefix().'requestAQuote', array('userid' => get_client_user_id(), 'status' => 1, 'garage_id' => null, 'remove_by_customer' => 0))->result();
        $data['quoteResult_close'] = $this->db->select('id,title,created_date')->order_by('id', 'desc')->get_where(db_prefix().'requestAQuote', array('userid' => get_client_user_id(), 'status' => 1, 'garage_id' => null, 'remove_by_customer' => 1))->result();
        $this->load->view('front/my-quote', $data);
    }
    
    /**
    *   @Function: My-orders
    */
    public function myOrder()
    {
        if($this->input->post())
        {
            $data = $this->input->post();
            if($data['ordertype'] = 'product')
            {
                $postdata['quote_id'] = 0;
                $postdata['product_id'] = $data['quote_id'];
                $ratingData = $this->db->get_where(db_prefix().'review_rating', array('customer_id' => $data['customer_id'],'garage_id' => $data['garage_id'],'product_id' => $data['quote_id']))->row('id');
            }
            else
            {
                $postdata['quote_id'] = $data['quote_id'];
                $postdata['product_id'] = 0;
                $ratingData = $this->db->get_where(db_prefix().'review_rating', array('customer_id' => $data['customer_id'],'garage_id' => $data['garage_id'],'quote_id' => $data['quote_id']))->row('id');
            }
            if($ratingData)
            {
                $postdata['order_rating'] = $data['order_rating'];
                $postdata['garage_rating'] = $data['garage_rating'];
                
                $this->db->where('id', $ratingData);
                $this->db->update(db_prefix().'review_rating', $postdata);
                
                $status = 'success';
                $msg = 'Rating are updated successful!';
            }
            else
            {
                $postdata['customer_id'] = $data['customer_id'];
                $postdata['garage_id'] = $data['garage_id'];
                
                $postdata['order_rating'] = $data['order_rating'];
                $postdata['garage_rating'] = $data['garage_rating'];
                
                $postdata['created_date'] = date('Y-m-d h:i:s');
                
                $this->db->insert(db_prefix().'review_rating', $postdata);
                $lastid = $this->db->insert_id();
                $status = 'success';
                $msg = 'Rating are submitted successful!';
            }
                
            $this->session->set_flashdata('status', $status);
            $this->session->set_flashdata('msg', $msg);
            redirect(base_url('customers/myOrder'));
        }
        $data['title'] = 'My Order';
		$data['orderResult'] = $this->db->order_by('id', 'desc')->get_where(db_prefix().'schedule_appoinment', array('customer_id' => get_client_user_id(), 'status !=' => 2))->result();
        $this->load->view('front/my-order', $data);
    }
    
    /**
    *   @Function: send-enquiry
    */
    public function sendEnquiry()
    {
        $data['title'] = 'Send Enquiry';
        $this->load->view('front/send-enquiry', $data);
    }
    
    /**
    *   @Function: buy now
    */
    public function buyNow($id,$pid)
    {
        $data['title'] = 'Buy now';
        if($this->input->post())
        {
            $data = $this->input->post();
           // echo '<pre>'; print_r($data); die;
            $postdata['product_id'] = $data['productid'];
            if($data['productid'] != '' && $data['garageid'] != '')
            { 
                $postdata['product_details'] = json_encode($this->db->get_where(db_prefix().'_product', array('id' => $data['productid']))->row());
                $postdata['garage_id'] = $data['garageid'];
                $postdata['pickup_address'] = @$data['pickup_address'];
                $postdata['delivery_address'] = @$data['delivery_address'];
                $postdata['appointment_date'] = strtotime($data['appointment_date']);
                $postdata['bookingtime'] = $data['bookingtime'];
                $postdata['fitting_type'] = @$data['fitting_type'];
                $postdata['qty'] = $data['qty'];
                $postdata['customer_id'] = get_client_user_id();
                $postdata['created_date'] = date('Y-m-d h:i:s');
                $postdata['quote_id'] = 0;
                $postdata['status'] = 6;
                
                //$this->db->insert(db_prefix().'buy_parts', $postdata);
                $this->db->insert(db_prefix().'schedule_appoinment', $postdata);
                $lastid = $this->db->insert_id();
                if($lastid)
                {
                    $status = 'success';
                    $msg = 'Request are submitted successful!';
                }
                else
                {
                    $status = 'error';
                    $msg = 'Access denied';
                }
            }
            else
            {
                $status = 'error';
                $msg = 'Product or garage details are not match';
            }
            
            $this->session->set_flashdata('status', $status);
            $this->session->set_flashdata('msg', $msg);
            //redirect(base_url('customers/productDetails/'.$data['productid']));
            redirect(base_url('customers/findCarPartService'));
        }
        $data['id'] = $id; 
        $data['pid'] = $pid; 
        
        $data['product_price'] = $this->db->select('price_with_fitting,price_before_offer,price_without_fitting,price_before_offer_without,offer_start_date,offer_end_date,offer_start_date_without,offer_end_date_without')->get_where(db_prefix().'_product', array('id' => $pid))->row();
        //echo '<pre>'; print_r($data['product_price']); die;
        $garagedetails = $this->db->get_where(db_prefix().'clients', array('userid' => $id))->row();
        $data['garageRating'] = $this->db->select('AVG(garage_rating) as rating')->get_where(db_prefix().'review_rating', array('garage_id' => $garagedetails->userid))->result(); 
        $data['garageRating_count'] = $this->db->get_where(db_prefix().'review_rating', array('garage_id' => $garagedetails->userid))->num_rows(); 
        $data['address'] = $garagedetails->address;
        $data['garagename'] = $garagedetails->company;
        $this->load->view('front/buy-now', $data);
    }
    
    /**
    *   @Function: product-listing
    */
    public function productListing($id)
    {
        //$data['title'] = 'Product-listing';
        $data['category_result'] = $this->db->get_where(db_prefix().'category', array('status' => 1))->result();
        $data['carmake_result'] = $this->db->get_where(db_prefix().'car_make', array('status' => 1))->result();
        $data['tyrebrand_result'] = $this->db->get_where(db_prefix().'tyre_brand', array('status' => 1))->result();
        $data['city_result'] = $this->db->get_where(db_prefix().'city', array('status' => 1))->result();
        $where = "FIND_IN_SET('".$id."', subcategory)";
        $this->db->select('*');
        $this->db->from(db_prefix().'_product');
        $this->db->where($where);
        $query = $this->db->get();
        $data['product_list'] = $query->result();
        //$data['product_list'] = $this->db->select('id,product_name,garage_id,description,price_with_fitting,price_without_fitting')->get_where(db_prefix().'_product', array('subcategory' => $id))->result();
        $catname = $this->db->get_where(db_prefix().'category', array('id' => $id))->row('name');
        $data['title'] = $catname;
        $data['id'] = $id;
        $this->load->view('front/product-listing', $data);
    }
    /**
    *   @Function: product-listing
    */
    public function allProductList($id)
    {
        //$data['title'] = 'Product-listing';
        $garagedetails = $this->db->select('company,userid')->get_where(db_prefix().'clients', array('userid' => $id))->row();
        $data['category_result'] = $this->db->get_where(db_prefix().'category', array('status' => 1))->result();
        $data['carmake_result'] = $this->db->get_where(db_prefix().'car_make', array('status' => 1))->result();
        $data['tyrebrand_result'] = $this->db->get_where(db_prefix().'tyre_brand', array('status' => 1))->result();
        $data['city_result'] = $this->db->get_where(db_prefix().'city', array('status' => 1))->result();
        //$where = "FIND_IN_SET('".$id."', subcategory)";
        $this->db->select('*');
        $this->db->from(db_prefix().'_product');
        $this->db->where('garage_id', $id);
        $query = $this->db->get();
        $data['product_list'] = $query->result();
        //$data['product_list'] = $this->db->select('id,product_name,garage_id,description,price_with_fitting,price_without_fitting')->get_where(db_prefix().'_product', array('subcategory' => $id))->result();
        //$catname = $this->db->get_where(db_prefix().'category', array('id' => $id))->row('name');
        $data['title'] = $garagedetails->company;
        $data['id'] = $data['product_list'][0]->category;
        $this->load->view('front/product-listing', $data);
    }
    /**
    *   @Function: tyre-listing
    */
    public function tyreListing($id)
    {
        //$data['title'] = 'Product-listing';
        $data['category_result'] = $this->db->get_where(db_prefix().'category', array('status' => 1))->result();
        $data['carmake_result'] = $this->db->get_where(db_prefix().'car_make', array('status' => 1))->result();
        $data['tyrebrand_result'] = $this->db->get_where(db_prefix().'tyre_brand', array('status' => 1))->result();
        $data['city_result'] = $this->db->get_where(db_prefix().'city', array('status' => 1))->result();
        $where = "FIND_IN_SET('".$id."', subcategory)";
        $this->db->select('*');
        $this->db->from(db_prefix().'_product');
        $this->db->where($where);
        $query = $this->db->get();
        $data['product_list'] = $query->result();
        //$data['product_list'] = $this->db->select('id,product_name,garage_id,description,price_with_fitting,price_without_fitting')->get_where(db_prefix().'_product', array('subcategory' => $id))->result();
        $catname = $this->db->get_where(db_prefix().'category', array('id' => $id))->row('name');
        $data['title'] = $catname;
        $data['id'] = $id;
        $this->load->view('front/tyre-listing', $data);
    }

    /**
    *   @Function: My Quote
    */
    public function productDetails($id)
    {
        $data['productDetails'] = $this->db->get_where(db_prefix().'_product', array('id' => $id))->row();
        $data['title'] = $data['productDetails']->product_name;
        $garagedetails = $this->db->get_where(db_prefix().'clients', array('userid' => $data['productDetails']->garage_id))->row();
        $data['garageRating'] = $this->db->select('AVG(garage_rating) as rating')->get_where(db_prefix().'review_rating', array('garage_id' => $data['productDetails']->garage_id))->result(); 
        $data['address'] = $garagedetails->address;
        $data['garagename'] = $garagedetails->company;
        $this->load->view('front/product-details', $data);
    }

    /**
    *   @Function: Request-details
    */
    public function requestDetails($id)
    {
        $postData['customer_view'] = 1;
        $this->db->where('quote_id', $id);
        $this->db->update(db_prefix().'quoteRequest_garageReply', $postData);
        
        $data['title'] = 'Request details';
        $data['id'] = $id;
        $data['quoteTitle'] = $this->db->get_where(db_prefix().'requestAQuote', array('id' => $id))->row('title');
        $data['request_result'] = $this->db->order_by('price','desc')->get_where(db_prefix().'quoteRequest_garageReply', array('quote_id' => $id ))->result(); 
        $this->load->view('front/request-details', $data);
    }

    /**
    *   @Function: Schedule-appointment
    */
    public function scheduleAppointment($id)
    {
        if($id)
        {
            if($this->input->post())
            {
                $post = $this->input->post();
                $quoteid = $post['quoteid'];
                
                $editBooking = $this->db->get_where(db_prefix().'schedule_appoinment', array('quote_id' => $quoteid, 'garage_id' => $post['garageid']))->num_rows();
                if($editBooking > 0)
                {
                    //$postData['quote_id'] = $post['quoteid'];
                    //$postData['garage_id'] = $post['garageid'];
                    //$postData['customer_id'] = get_client_user_id();
                    $postData['appointment_date'] = strtotime($post['bookingdate']);
                    $postData['bookingtime'] = $post['bookingtime'];
                    //$postData['created_date'] = date('Y-m-d h:i:s');
                    $this->db->where('quote_id', $quoteid);
                    $this->db->update(db_prefix().'schedule_appoinment', $postData);
                    
                    $status = 'success';
                    $msg = 'Request are updated successful.';
                }
                else
                {
                    $postData['quote_id'] = $post['quoteid'];
                    $postData['garage_id'] = $post['garageid'];
                    $postData['customer_id'] = get_client_user_id();
                    $postData['appointment_date'] = strtotime($post['bookingdate']);
                    $postData['bookingtime'] = $post['bookingtime'];
                    $postData['created_date'] = date('Y-m-d h:i:s');
                    
                    $this->db->insert(db_prefix().'schedule_appoinment', $postData);
                    $lastid = $this->db->insert_id();
                    
                    $status = 'success';
                    $msg = 'Request are submitted successful.';
                }
                
                $confirmdata['booking_request_status'] = 1;
                $this->db->where('id', $id);
                $this->db->update(db_prefix().'quoteRequest_garageReply', $confirmdata);
                
                $this->session->set_flashdata('status', $status);
                $this->session->set_flashdata('msg', $msg);
                //redirect(base_url('customers/myQuote'));
                redirect(base_url('customers/requestdetails/'.$quoteid));
            }
            else
            {
                $data['title'] = 'Schedule appointment';
                $quoteDetails = $this->db->get_where(db_prefix().'quoteRequest_garageReply', array('id' => $id))->row();
                $data['garageID'] = $quoteDetails->garage_id;
                $data['id'] = $quoteDetails->quote_id;
                $data['address'] = $this->db->get_where(db_prefix().'clients', array('userid' => $quoteDetails->garage_id))->row('address');
                $this->load->view('front/schedule-appointment', $data);
            }
        }
        else
        {
            set_alert('error', _l('Access denied'));
            redirect($_SERVER['HTTP_REFERER']); 
        }
    }

    /**
    *   @Function: Find car part/service
    */
    public function findCarPartService()
    {
        $data['title'] = 'Find car part/service';
        $data['category_result'] = $this->db->get_where(db_prefix().'category', array('status' => 1, 'parent_id' => 0))->result();
        $data['carmake_result'] = $this->db->get_where(db_prefix().'car_make', array('status' => 1))->result();
        $data['tyrebrand_result'] = $this->db->get_where(db_prefix().'tyre_brand', array('status' => 1))->result();
        $data['city_result'] = $this->db->get_where(db_prefix().'city', array('status' => 1))->result();
        $this->load->view('front/find-car-part', $data);
    }
    /**
    *   @Function: Find car part/service
    */
    public function carPartService($id)
    {
        //$data['category_result'] = $this->db->get_where(db_prefix().'category', array('status' => 1))->result();
        $data['carmake_result'] = $this->db->get_where(db_prefix().'car_make', array('status' => 1))->result();
        $data['tyrebrand_result'] = $this->db->get_where(db_prefix().'tyre_brand', array('status' => 1))->result();
        $data['city_result'] = $this->db->get_where(db_prefix().'city', array('status' => 1))->result();
        $catname = $this->db->get_where(db_prefix().'category', array('id' => $id))->row('name');
        $data['id'] = $id;
        $data['title'] = $catname;
        $data['category_result'] = $this->db->get_where(db_prefix().'category', array('status' => 1, 'parent_id' => $id))->result();
        $this->load->view('front/find-car-part-sub-category', $data);
    }
    
    /**
    *   @Function: Get a quote
    */
    public function getAQuote()
    {
        $data['title'] = 'Get a quote';
        $data['category_result'] = $this->db->get_where(db_prefix().'category', array('status' => 1))->result();
        $data['carmake_result'] = $this->db->get_where(db_prefix().'car_make', array('status' => 1))->result();
        $data['tyrebrand_result'] = $this->db->get_where(db_prefix().'tyre_brand', array('status' => 1))->result();
        $data['city_result'] = $this->db->get_where(db_prefix().'city', array('status' => 1))->result();
        $data['quoteResult_draft'] = $this->db->select('id,title,created_date,categoryid')->order_by('id', 'desc')->get_where(db_prefix().'requestAQuote', array('userid' => get_client_user_id(), 'status' => 2))->result();
        $data['quoteResult_reuse'] = $this->db->select('id,title,created_date,categoryid')->order_by('id', 'desc')->get_where(db_prefix().'requestAQuote', array('userid' => get_client_user_id(), 'status' => 1))->result();
        $this->load->view('front/get-a-quote', $data);
    }
    
    /**
    *   @Function: getDraftReq
    */
    public function getDraftReq()
    {
        $html = '';
        $id = $_POST['id'];
        if($id)
        {
            $data['category_result'] = $this->db->get_where(db_prefix().'category', array('status' => 1))->result();
            $data['carmake_result'] = $this->db->get_where(db_prefix().'car_make', array('status' => 1))->result();
            $data['tyrebrand_result'] = $this->db->get_where(db_prefix().'tyre_brand', array('status' => 1))->result();
            $data['city_result'] = $this->db->get_where(db_prefix().'city', array('status' => 1))->result();
            $data['quoteResult_draft'] = $this->db->select('id,title')->get_where(db_prefix().'requestAQuote', array('userid' => get_client_user_id(), 'status' => 2))->result();
            $data['quoteResult_reuse'] = $this->db->select('id,title')->get_where(db_prefix().'requestAQuote', array('userid' => get_client_user_id(), 'status' => 1))->result();
            $data['quoteData'] = $this->db->get_where(db_prefix().'requestAQuote', array('id' => $id, 'userid' => get_client_user_id()))->row();
            $html = $this->load->view('front/draftrequest', $data, true);
        }
        echo $html;
    }
    
    /**
    *   @Function: getPreviewReq
    */
    public function getPreviewReq()
    {
        $html = '';
        $id = $_POST['id'];
        if($id)
        {
            $data['category_result'] = $this->db->get_where(db_prefix().'category', array('status' => 1))->result();
            $data['carmake_result'] = $this->db->get_where(db_prefix().'car_make', array('status' => 1))->result();
            $data['tyrebrand_result'] = $this->db->get_where(db_prefix().'tyre_brand', array('status' => 1))->result();
            $data['city_result'] = $this->db->get_where(db_prefix().'city', array('status' => 1))->result();
            $data['quoteResult_draft'] = $this->db->select('id,title')->get_where(db_prefix().'requestAQuote', array('userid' => get_client_user_id(), 'status' => 2))->result();
            $data['quoteResult_reuse'] = $this->db->select('id,title')->get_where(db_prefix().'requestAQuote', array('userid' => get_client_user_id(), 'status' => 1))->result();
            $data['quoteData'] = $this->db->get_where(db_prefix().'requestAQuote', array('id' => $id, 'userid' => get_client_user_id()))->row();
            $html = $this->load->view('front/draftrequest', $data, true);
        }
        echo $html;
    }
	
    /**
    *   @Function: getReuseReq
    */
    public function getReuseReq()
    {
        $html = '';
        $id = $_POST['id'];
        if($id)
        {
            $data['category_result'] = $this->db->get_where(db_prefix().'category', array('status' => 1))->result();
            $data['carmake_result'] = $this->db->get_where(db_prefix().'car_make', array('status' => 1))->result();
            $data['tyrebrand_result'] = $this->db->get_where(db_prefix().'tyre_brand', array('status' => 1))->result();
            $data['city_result'] = $this->db->get_where(db_prefix().'city', array('status' => 1))->result();
            $data['quoteResult_draft'] = $this->db->select('id,title')->get_where(db_prefix().'requestAQuote', array('userid' => get_client_user_id(), 'status' => 2))->result();
            $data['quoteResult_reuse'] = $this->db->select('id,title')->get_where(db_prefix().'requestAQuote', array('userid' => get_client_user_id(), 'status' => 1))->result();
            $data['quoteData'] = $this->db->get_where(db_prefix().'requestAQuote', array('id' => $id, 'userid' => get_client_user_id()))->row();
            $html = $this->load->view('front/reuserequest', $data, true);
        }
        echo $html;
    }
    /**
    *   @Function: getQuoteDetails
    */
    public function getQuoteDetails()
    {
        $html = '';
        $id = $_POST['id'];
        if($id)
        {
            $data['category_result'] = $this->db->get_where(db_prefix().'category', array('status' => 1))->result();
            $data['carmake_result'] = $this->db->get_where(db_prefix().'car_make', array('status' => 1))->result();
            $data['tyrebrand_result'] = $this->db->get_where(db_prefix().'tyre_brand', array('status' => 1))->result();
            $data['city_result'] = $this->db->get_where(db_prefix().'city', array('status' => 1))->result();
            $data['quoteResult_draft'] = $this->db->select('id,title')->get_where(db_prefix().'requestAQuote', array('userid' => get_client_user_id(), 'status' => 2))->result();
            $data['quoteResult_reuse'] = $this->db->select('id,title')->get_where(db_prefix().'requestAQuote', array('userid' => get_client_user_id(), 'status' => 1))->result();
            $data['quoteData'] = $this->db->get_where(db_prefix().'requestAQuote', array('id' => $id, 'userid' => get_client_user_id()))->row();
            $html = $this->load->view('front/getQuoteDetails', $data, true);
        }
        echo $html;
    }
    
    /**
    *   @Function: getPreviewProductReq
    */
    public function getPreviewProductReq()
    {
        $html = '';
        $id = $_POST['id'];
        if($id)
        {
            $quoteResult = $this->db->get_where(db_prefix().'_product', array('id' => $id))->row();
            if($quoteResult)
            {
                $data['quoteResult'] = $quoteResult;
                $html = $this->load->view('themes/perfex/views/getProductDetails', $data, true);
            }
        }
        echo $html;
    }
    
    /**
    *   @Function: getAQuotePost
    */
    public function getAQuotePost($id = '')
    {
        //echo '<pre>'; print_r($_POST);die;
        $data = $this->input->post();
        
        $postData['title'] = $data['title'];
        $postData['userid'] = get_client_user_id();
        $postData['locationid'] = $data['locationid'];
        $postData['categoryid'] = implode(',',$data['categoryid']);
        $postData['subcategoryid'] = implode(',',$data['subcategoryid']);
        $postData['vehiclecategory'] = $data['vehiclecategory'];
        $postData['brandid'] = $data['brandid'];
        $postData['carmodelid'] = $data['carmodelid'];
        $postData['carmakeyearid'] = $data['carmakeyearid'];
        $postData['description'] = $data['description'];
        $postData['chassis_no'] = $data['chassis_no'];
        $postData['address'] = $data['address'];
        
        if($data['submit'] == 'save')
        {
            $postData['status'] = 2;
        }
        else
        {
            $postData['status'] = 1;
        }
        
        $name = $_FILES["registrationcard"]["name"];
        $photovideo = $_FILES["upload_photo_video"]["name"];
        $lastIDAss = $this->db->order_by('id', 'desc')->get_where(db_prefix().'requestAQuote')->row('id');
        if($name)
        {
            $ext  = end((explode(".", $name)));
            if($lastIDAss != '')
            {
                $lastIDAss = $lastIDAss + 1;
                $logoname = 'rc_'.$lastIDAss.'.'.$ext;
            }
            else
            {
                $logoname = 'rc_'.time().'.'.$ext;
            }
            $allowed = array('pdf', 'png', 'jpg', 'jpeg', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'zip', 'rar', 'txt', 'PNG', 'JPG', 'JPEG');
            $filename = $_FILES['registrationcard']['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if (!in_array($ext, $allowed)) {
               
            }
            else
            {
                move_uploaded_file($_FILES["registrationcard"]['tmp_name'], 'uploads/quote/'.$logoname);
                $postData['registrationcard'] = $logoname;
               // echo $this->db->last_query(); 
            } 
            //$this->db->insert(db_prefix().'requestAQuote',  $postData);
        }
        if($photovideo)
        {
            $ext = '';
            $logoname = '';
            $filename = '';
            $allowed = '';
            $ext  = end((explode(".", $photovideo)));
            $postData['ext'] = $ext;
            if($lastIDAss != '')
            {
                $lastIDAss = $lastIDAss + 1;
                $logoname = 'photovideo_'.$lastIDAss.'.'.$ext;
            }
            else
            {
                $logoname = 'photovideo_'.time().'.'.$ext;
            }
            $allowed = array("jpg", "jpeg", "gif", "png", "mp3", "mp4", "wma", "PNG", "JPG", "JPEG");
            $filename = $_FILES['upload_photo_video']['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if (!in_array($ext, $allowed)) {
               
            }
            else
            {
                move_uploaded_file($_FILES["upload_photo_video"]['tmp_name'], 'uploads/quote/'.$logoname);
                $postData['upload_photo_video'] = $logoname;
            }
        }
        if($id != '')
        {
            $this->db->where('id', $id);
            $this->db->update(db_prefix().'requestAQuote', $postData);
            $lid = $id;
        }
        else
        {
			$postData['created_date'] = date('Y-m-d h:i:s');
            $this->db->insert(db_prefix().'requestAQuote',  $postData);
            $lid = $this->db->insert_id(); 
            $status = 'success';
            $msg = 'Request are submitted successful.';
        }
            
        if($lid)
        {
           $status = 'success';
           $msg = 'Request are submitted successful.';
           
        }
        else
        {
            $status = 'error';
            $msg = 'Please try again';
        }
        $this->session->set_flashdata('status', $status);
        $this->session->set_flashdata('msg', $msg);
        //redirect(base_url('customers/getAQuote'));
        redirect(base_url('customers'));
        //echo '<pre>'; print_r($_POST); print_r($_FILES); die;
    }
    
    /**
    *   @Function: getAQuotePostReuse
    */
    public function getAQuotePostReuse()
    {
        $data = $this->input->post();
        
        $postData['title'] = $data['title'];
        $postData['userid'] = get_client_user_id();
        $postData['locationid'] = $data['locationid'];
        $postData['categoryid'] = implode(',',$data['categoryid']);
        $postData['subcategoryid'] = implode(',',$data['subcategoryid']);
        $postData['vehiclecategory'] = $data['vehiclecategory'];
        $postData['brandid'] = $data['brandid'];
        $postData['carmodelid'] = $data['carmodelid'];
        $postData['carmakeyearid'] = $data['carmakeyearid'];
        $postData['description'] = $data['description'];
        $postData['address'] = $data['address'];
        $postData['chassis_no'] = $data['chassis_no'];
        
        if($data['submit'] == 'save')
        {
            $postData['status'] = 2;
        }
        else
        {
            $postData['status'] = 1;
        }
                       
        $name = $_FILES["registrationcard"]["name"];
        $photovideo = $_FILES["upload_photo_video"]["name"];
        $lastIDAss = $this->db->order_by('id', 'desc')->get_where(db_prefix().'requestAQuote')->row('id');
        if($name)
        {
            $ext  = end((explode(".", $name)));
            if($lastIDAss != '')
            {
                $lastIDAss = $lastIDAss + 1;
                $logoname = 'rc_'.$lastIDAss.'.'.$ext;
            }
            else
            {
                $logoname = 'rc_'.time().'.'.$ext;
            }
            $allowed = array('pdf', 'png', 'jpg', 'jpeg', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'zip', 'rar', 'txt', 'PNG', 'JPG', 'JPEG');
            $filename = $_FILES['registrationcard']['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if (!in_array($ext, $allowed)) {
               
            }
            else
            {
                move_uploaded_file($_FILES["registrationcard"]['tmp_name'], 'uploads/quote/'.$logoname);
                $postData['registrationcard'] = $logoname;
               // echo $this->db->last_query(); 
            } 
            //$this->db->insert(db_prefix().'requestAQuote',  $postData);
        }
		else{
			$oldfiename = $data['registrationcard_old'];
			if($oldfiename)
			{
				$postData['registrationcard'] = $oldfiename;
			}
		}
        if($photovideo)
        {
            $ext = '';
            $logoname = '';
            $filename = '';
            $allowed = '';
            $ext  = end((explode(".", $photovideo)));
            $postData['ext'] = $ext;
            if($lastIDAss != '')
            {
                $lastIDAss = $lastIDAss + 1;
                $logoname = 'photovideo_'.$lastIDAss.'.'.$ext;
            }
            else
            {
                $logoname = 'photovideo_'.time().'.'.$ext;
            }
            $allowed = array("jpg", "jpeg", "gif", "png", "mp3", "mp4", "wma", "PNG", "JPG", "JPEG");
            $filename = $_FILES['upload_photo_video']['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if (!in_array($ext, $allowed)) {
               
            }
            else
            {
                move_uploaded_file($_FILES["upload_photo_video"]['tmp_name'], 'uploads/quote/'.$logoname);
                $postData['upload_photo_video'] = $logoname;
            }
        }
		else{
			$oldvideoname = $data['upload_photo_video_old'];
			if($oldvideoname)
			{
				$postData['upload_photo_video'] = $oldvideoname;
			}
		}
		
        $postData['created_date'] = date('Y-m-d h:i:s');
		$this->db->insert(db_prefix().'requestAQuote',  $postData);
		$lid = $this->db->insert_id(); 
            
        if($lid)
        {
           $status = 'success';
           $msg = 'Request are submitted successful.';
           
        }
        else
        {
            $status = 'error';
            $msg = 'Please try again';
        }
        $this->session->set_flashdata('status', $status);
        $this->session->set_flashdata('msg', $msg);
        redirect(base_url('customers'));
        //echo '<pre>'; print_r($_POST); print_r($_FILES); die;
    }
    
    /**
    *   @Function: start-quote
    */
    public function startQuote()
    {
        $data['title'] = 'Start-quote';
        $this->load->view('front/start-quote', $data);
    }
}