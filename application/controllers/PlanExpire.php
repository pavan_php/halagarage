<?php

defined('BASEPATH') or exit('No direct script access allowed');

use app\services\ValidatesContact;

class PlanExpire extends ClientsController
{
    /**
     * @since  2.3.3
     */
    use ValidatesContact;

    public function __construct()
    {
        parent::__construct();
        hooks()->do_action('after_clients_area_init', $this);
        if(get_client_role_type() == 1)
        {
            redirect(site_url('customers'));
        }
    }

    /**
    *   Plan Expire
    */
    public function index()
    {
        $data['title'] = _l('Payment Expire');
        $this->data($data);
        $this->view('planExpire');
        $this->layout();
    }
}
