<?php defined('BASEPATH') OR exit('No direct script access allowed');
//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
/**
 * Chat
 * @package	Chat
 * @author	Immersive Dev Team
 * @link	http://immersiveinfotech.com
 * @since	Version 1.0.0
 * @filesource
 */

require_once(APPPATH . 'libraries/REST_Controller.php');

class Chat extends REST_Controller {
/* 
 *  Variable for providing data
 *
 */
    //private $data;
/* 
 *  Variable for Access control
 *
 */
    private $access;
    private $table = 'tbl_chat';

    public function __construct() {
       parent::__construct();
       $this->load->model('api/Chat_model');
       $this->load->model('crud_model');
       $this->load->helper(array('custom_helper'),NULL);
       date_default_timezone_set('Asia/Kolkata');
       $this->base_url = 'http://php.manageprojects.in/halagarage';
      // $this->auth(); 
       // error_reporting(-1);
        //ini_set('display_errors', 1);
        
       error_reporting(-1);
			ini_set('display_errors', 1);
    }
  
    // ======================================= dateDiffer ===================================================

    private function dateDiffer($new_date)
    {   
        if ($new_date==NULL && $new_date=='') {
            return 'Not started yet';
        }else{
            $interval = date_diff(date_create(), date_create($new_date));
            $out = ['year'=>$interval->format("%Y")+0,
                         'month'=>$interval->format("%M")+0,
                         'day'=>$interval->format("%d")+0,
                         'hour'=>$interval->format("%H")+0,
                         'minute'=>$interval->format("%i")+0,
                         'second'=>$interval->format("%s")+0,
                        ];
            if ($out['year']>0) {
                $gap='past year ago';
            }else if ($out['month']>0) {
                $gap=$out['month'].' month ago';
            }else if ($out['day']>0) {
                $gap=$out['day'].' day ago';
            }else if ($out['hour']>0) {
                $gap=$out['hour'].' hour ago';
            }else if ($out['minute']>0) {
                $gap=$out['minute'].' minute ago';
            }else if ($out['second']>10) {
                $gap= 'Less a minute';
            }else{
                $gap='Now';
            }
            return $gap;
        }
    }

    // ======================================= get users MSG ===================================================

    public function getUser()
    {  
        
       $postData = array_merge($_POST,json_decode(file_get_contents('php://input'),true));
        
        $postData['user_from'] = get_client_user_id();
      
		if($postData['user_from']!=''){
		 
            $response = array();
            $usersdata = array();
            $data = $postData;
          
            $search = (!empty($postData['search']))?$postData['search']:'';
            $unread = (!empty($postData['unread']))?$postData['unread']:'';
            $archive = (!empty($postData['archive']))?$postData['archive']:'';
            $users = $this->Chat_model->getUser_model($search,$postData);
         //print_r( $users );
       //  die;
            //echo  $this->db->last_query();
 
            foreach ($users as $key => $value) {
              if ($postData['user_from']==$value->userid) 
              {
                    continue;
				}else{
                  
                     
                $res = $this->Chat_model->getUserLastMsg_model($postData['user_from'], $value->userid);
			    $archive_count = $this->Chat_model->archiveNow_modal($postData['user_from'], $value->userid, 'count');
			  
			    $chat_status = $this->Chat_model->getChatStatus($postData['user_from'], $value->userid);
                $block_status = $this->Chat_model->checkblock_modal($postData['user_from'], $value->userid);
				
				$filename = $this->db->get_where('tblfiles', array('rel_id' => $value->userid, 'rel_type' => 'garageslogo'))->row('file_name');
				
                $msgDate = (empty($res['data'][0]['date']))?NULL:$res['data'][0]['date'];
                $msg_gap = $this->dateDiffer($msgDate);
                $message = (empty($res['data'][0]['msg']))?NULL:$res['data'][0]['msg'];
                $_msgCount = (empty($res['count'][0]['no_msg']))?NULL:$res['count'][0]['no_msg'];
                $result = array('msg_id'=> (empty($res['data'][0]['id']))?NULL:$res['data'][0]['id'],
                                'is_typing'=> ($chat_status>0)?1:0,
                                'msg'=> ($chat_status>0)?'':$message,
                                'msg_status'=> (empty($res['data'][0]['msg_status']))?NULL:$res['data'][0]['msg_status'],
                                'count'=>$_msgCount,
                                'id' => (empty($value->id))?NULL:$value->userid,
                                'user_id' => (empty($value->userid))?NULL:$value->userid,
                                'name'=> $value->firstname.' '.$value->lastname,
                                'email'=> $value->email,
                                'status'=> 1,//($value->is_online == 'yes')?1:0,
                                'msg_date'=> $msgDate,
                                'time_differ'=> $msg_gap,
                                'image'=>'',// $value->photo,
                                'profile_picture_path'=> site_url('uploads/garage/'.$value->userid.'/'.$filename),//  site_url('assets/uploads/profiles/thumbs/'.$value->photo),
                                'profileType'=> '',
                                'archive_count'=> $archive_count[0]['archive'],
                                'is_blocked'=> (empty($block_status->is_blocked))?0:$block_status->is_blocked,
                        );
                    
                    
                   
                       if(@$postData['topHeader']!=''){
                            if( $_msgCount!=''){
                                array_push($usersdata, $result);
                            }
                    
                        }else{
                            array_push($usersdata, $result);
                        }
                   
                }
            }
                
            if ($search!='') {
                $response = $usersdata;
            }else if($unread!=''){
                foreach ($usersdata as $key => $val) {
                    if (!empty($val['msg_id']) && $val['count']>0) {
                        array_push($response, $val);
                    }else{
                        continue;
                    }
                }

            }else if($archive!=''){
                foreach ($usersdata as $key => $val) {
                    if (!empty($val['msg_id']) && $val['archive_count']>0) {
                        array_push($response, $val);
                    }else{
                        continue;
                    }
                }

            }else{
                foreach ($usersdata as $key => $val) {
                    if (!empty($val['msg_id']) && $val['archive_count']==0) {
                        array_push($response, $val);
                    }else{
                        // if($postData['job_id']!=''){
                        //     array_push($response, $val);
                        // }else{
                            continue;
                        // }
                        
                        
                    }
                }
            }

            foreach($response as $val){
                 $rrTime[strtotime($val['msg_date'])] = $val;
            }
            
            krsort($rrTime);
            foreach ($rrTime as $key => $val) {
                $re[] = $val;
            } 
            
         //  print_r($usersdata);
              //      die;
           $msg = array('status' => false, 'message' => "Please ", 'result' => $response);
        }else{
			$msg = array('status' => false, 'message' => "Please login", 'result' => array());
		}
	 
		$this->response($msg, REST_Controller::HTTP_OK); 
		 
    }

    // ======================================= get Chat ===================================================

    public function getChat()
    {        
        $postData = array_merge($_POST,json_decode(file_get_contents('php://input'),true));
        $postData['user_from'] = get_client_user_id();
       // $postData['user_from'] = $this->user_data->id;
	 	if (empty($postData['user_from']) || empty($postData['user_to'] )) {
            $message = (empty($postData['user_from']))?'user_from field required':'user_to field required';
            $msg = array('status' => false, 'message' => $message, 'result' => array());
        }else{
         
            $lastdate = '';
            $response['chat_data']=array();
            $response['last_msg']=array();
            $response['last_date']=array();
            $response['is_blocked']='0';
            $username  = $this->db->select('firstname as first_name,lastname as last_name')->where(['userid'=>$postData['user_to']])->get('tblcontacts')->row();
          //  echo  $this->db->last_query();
        //    print_r($username );
        //    die;
            $response['user_name']=   $username->first_name.' '. $username->last_name;
          //  $check = $this->Chat_model->checkArchive_model($postData['user_from'], $postData['user_to']);
            $result = $this->Chat_model->getChat($postData['user_from'], $postData['user_to']);
          //echo '<pre>'; print_r($result); die;
            foreach ($result['chat'] as $key => $value) {
                preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $value['msg'], $text);
                $value['profile_picture_path']  = $this->crud_model->file_view('user',$postData['user_to'],'','','','src','','','.png');
                if (!empty($text[0])) {
                    $value['is_anc'] = 1;
                    $value['anc_array'] = $text[0];
                }
                if ($lastdate=='') {
                    $lastdate=$value['date'];
                    $value['is_new_date'] = 1;
                }else if ($lastdate!='' && $lastdate===$value['date']) {
                    $value['is_new_date'] = 0;
                }else{$value['is_new_date'] = 1;
                        $lastdate=$value['date'];}
                array_push($response['chat_data'], $value);
            }

           

            if (empty($result['last_msg'])) {$result['last_msg'][0]='';}
            array_push($response['last_msg'], $result['last_msg'][0]);
            if (empty($result['last_date'])) {$result['last_date'][0]='';}
            array_push($response['last_date'], $result['last_date'][0]);

            $block_status_check = $this->db->get_where('user_block',array('blocked_by' =>$postData['user_from'],'block_user' =>$postData['user_to']))->result();

            if(count($block_status_check)!=0)
            {
                $lastdate = '';
                $response['chat_data']='';
                $response['last_msg']='';
                $response['last_date']='';
                $response['user_name']='';
                 $response['is_blocked']='1';
                
            }
			
            $msg = array('status' => true, 'message' => "Please ", 'result' => $response);
        }
		 
		$this->response($msg, REST_Controller::HTTP_OK); 
    }
       
    // ======================================= sendMsg ===================================================

    public function sendMsg()
    {        
        $postData = array_merge($_POST,json_decode(file_get_contents('php://input'),true));
        //$postData['user_from'] = $this->user_data->id;
           $postData['user_from'] = get_client_user_id();
        if (empty($postData['user_from']) || empty($postData['user_to'])) {
			$message = (empty($postData['user_from']))?'user_from field required':'user_to field required';
            $msg = array('status' => false, 'message' => $message, 'result' => array());
	     }else{
            
            $result = $this->Chat_model->insertAll($this->table,['chat_from'=>$postData['user_from'], 'chat_with'=>$postData['user_to'], 'msg'=>$postData['msg'], 'create_timestamp'=>date('Y-m-d H:i:s')]);
            $currentDate = $this->Chat_model->lastSendMsg($result);
            $response['date']= $currentDate[0]['date'];
            $response['id']= $result;
            $response['is_new_date']= ($currentDate[0]['date']===$postData['last_date'])? 0:1;

            //send mail
            /* $block_status_check = $this->db->get_where('user_block',array('blocked_by' =>$postData['user_from'],'block_user' =>$postData['user_to']))->result();

            if(count($block_status_check)==0)
            {
                $this->crud_model->chat_notification($postData['user_from'],$postData['user_to'],$postData['msg']);
            }*/
            $msg = array('status' => true, 'message' => "Please ", 'result' => $response);
        }
		$this->response($msg, REST_Controller::HTTP_OK); 
    }

    // ======================================= updateMsg ===================================================

    public function updateMsg()
    {        
        $postData = array_merge($_POST,json_decode(file_get_contents('php://input'),true));
       // $postData['user_from'] = $this->user_data->id;
        if (empty($postData['user_from']) || empty($postData['user_to'])) {
            $message = (empty($postData['user_from']))?'user_from field required':'user_to field required';
			$msg = array('status' => false, 'message' => $message, 'result' => array());
        }else{
            $response = $this->Chat_model->updateMsg($postData['user_from'], $postData['user_to']);
            $msg = array('status' => true, 'message' => "Please ", 'result' => $response);
        }
		$this->response($msg, REST_Controller::HTTP_OK); 
    }

    // ======================================= UpdateChatStatus ===================================================

    public function UpdateChatStatus()
    {   
        $postData = array_merge($_POST,json_decode(file_get_contents('php://input'),true));
       // $postData['user_from'] = $this->user_data->id;
        if (empty($postData['user_from']) || empty($postData['user_to'])) {
			$message = (empty($postData['user_from']))?'user_from field required':'user_to field required';
            $msg = array('status' => false, 'message' => $message, 'result' => array());
        }else{
            $response = $this->Chat_model->UpdateChatStatus($postData['user_from'], $postData['user_to']);
            $msg = array('status' => true, 'message' => "Please ", 'result' => $response);
        }
		$this->response($msg, REST_Controller::HTTP_OK); 
    }

    // ======================================= deleteChatStatus ===================================================

    public function deleteChatStatus()
    {        
        $postData = array_merge($_POST,json_decode(file_get_contents('php://input'),true));
      //  $postData['user_from'] = $this->user_data->id;
        if (empty($postData['user_from']) || empty($postData['user_to'])) {
			$message = (empty($postData['user_from']))?'user_from field required':'user_to field required';
            $msg = array('status' => false, 'message' => $message, 'result' => array());
        }else{
            $response = $this->Chat_model->deleteChatStatus($postData['user_from'], $postData['user_to']);
            $msg = array('status' => true, 'message' => "Please ", 'result' => $response);
        }
		$this->response($msg, REST_Controller::HTTP_OK);
    }

    // ======================================= getChatStatus ===================================================

    public function getChatStatus()
    {        
       $postData = array_merge($_POST,json_decode(file_get_contents('php://input'),true));
      //  $postData['user_from'] = $this->user_data->id;
        if (empty($postData['user_from']) || empty($postData['user_to'])) {
			$message = (empty($postData['user_from']))?'user_from field required':'user_to field required';
            $msg = array('status' => false, 'message' => $message, 'result' => array());
        }else{
            $response = $this->Chat_model->getChatStatus($postData['user_from'], $postData['user_to']);
			$msg = array('status' => true, 'message' => "Please ", 'result' => $response);
        }
		$this->response($msg, REST_Controller::HTTP_OK);
    }

    // ======================================= deleteChatHistory ===================================================

    public function deleteChatHistory()
    {        
       $postData = array_merge($_POST,json_decode(file_get_contents('php://input'),true));
      // $postData['user_from'] = $this->user_data->id;
        if (empty($postData['user_from']) || empty($postData['user_to'])) {
			$message = (empty($postData['user_from']))?'user_from field required':'user_to field required';
            $msg = array('status' => false, 'message' => $message, 'result' => array());
        }else{
            $response = $this->Chat_model->deleteChatHistory( $postData['user_from'],  $postData['user_to']);
            $msg = array('status' => true, 'message' => "Please ", 'result' => $response);
        }
		$this->response($msg, REST_Controller::HTTP_OK);
    }

    // ======================================= deleteMsg ===================================================

    public function deleteMsg()
    {      
       $postData = array_merge($_POST,json_decode(file_get_contents('php://input'),true));
      //  $postData['user_from'] = $this->user_data->id;
        if(empty($postData['id'])) {
            $message = 'id field required';
            $msg = array('status' => false, 'message' => $message, 'result' => array());
        }else{
            $path = $this->db->where(['id'=>$postData['id']])->get($this->table)->result();
            if ($path[0]->is_type==0) {
                $response = $this->Chat_model->updateArrayWhereResult($this->table, ['is_delete'=>1], ['id'=>$postData['id']]);
                $msg = array('status' => true, 'message' => "Please ", 'result' => $response);
            }else{
                $file_pointer = './uploads/chat/'.$path[0]->msg;
                if (unlink($file_pointer)) { 
                    $response = $this->Chat_model->updateArrayWhereResult($this->table, ['is_delete'=>1], ['id'=>$postData['id']]);
                    $msg = array('status' => true, 'message' => "Please ", 'result' => $response);
                } 
                else { 
                    $response = "somthing went wrong"; 
                    $msg = array('status' => false, 'message' => "Please ", 'result' => $response);
                }
            } 
          
        }
		$this->response($msg, REST_Controller::HTTP_OK);
    }

    // ======================================= notifyMe ===================================================

    public function notifyMe()
    {        
       $postData = array_merge($_POST,json_decode(file_get_contents('php://input'),true));
       // $postData['user_from'] = $this->user_data->id;
        if (empty($postData['user_from'])) {
            $message = 'user_from field required';
            $msg = array('status' => false, 'message' => $message, 'result' => array());
        }else{
            $response = $this->Chat_model->notifyMe($postData['user_from']);
            $msg = array('status' => true, 'message' => "Please ", 'result' => $response);
        }
		$this->response($msg, REST_Controller::HTTP_OK);
    }

   // ======================================= uplaodData ===================================================

    public function uplaodData()
    {        
		//	$postData = array_merge($_POST,json_decode(file_get_contents('php://input'),true));
		//	$postData['user_from'] = $this->user_data->id;
            $profile="No Image";
            $is_image=2; //substr(str_replace(' ', '', $_FILES["image"]["name"]),0,10).$time6
            $time6 = date('hi');
            if(strlen(explode('.', $_FILES["image"]["name"])[0])>10)
            {
                $name = substr(str_replace(' ', '', explode('.', $_FILES["image"]["name"])[0]),0,13).$time6;
            }else
            {
                $name = str_replace(' ', '', explode('.', $_FILES["image"]["name"])[0]).$time6;
            }

            if(isset($_FILES["image"]["name"]))
            {
                $config['upload_path']= './uploads/chat/';
                $config['remove_spaces'] = true;
                $config['allowed_types']='png|jpeg|JPEG|jpg|JPG|csv|xls|xlsx|word|doc|docx|pdf|ppt|pptx|txt|html|htm|mp4|3gpp|mkv|avi|3gp';
                $config['file_name']=$name;
                $config['max_size']= 40960;

                $this->load->library('upload',$config);
                $this->upload->initialize($config);
              
                if($this->upload->do_upload('image'))
                {

                   
                    $uploadData=$this->upload->data();
                 
                    $picture=$uploadData['file_name'];
                        if ($uploadData['is_image']==1) 
                        {
                            $is_image=1;
                        }
                    $profile=$uploadData['orig_name'];
                   
                }
              }


            $size = size_as_kb($_FILES["image"]["size"]);
            $response=['filename'=>$profile, 'is_type'=>$is_image,'size'=>$size];

            


			$msg = array('status' => true, 'message' => "Please ", 'result' => $response);
         
			$this->response($msg, REST_Controller::HTTP_OK);
             
    }

    // ======================================= uploadFileMsg ===================================================

    public function uploadFileMsg()
    {        
        $postData = array_merge($_POST,json_decode(file_get_contents('php://input'),true));
        //$postData['user_from'] = $this->user_data->id;
        if (empty($postData['user_from']) || empty($postData['user_to'])) {
			$message = (empty($postData['user_from']))?'user_from field required':'user_to field required';
            $msg = array('status' => false, 'message' => $message, 'result' => array());
        }else{
            $response = $this->Chat_model->insertAll($this->table,['chat_from'=>$postData['user_from'], 'chat_with'=>$postData['user_to'], 'msg'=>$postData['msg'], 'create_timestamp'=>date('Y-m-d H:i:s'), 'is_type'=>$postData['is_type'],'size'=>$postData['size']]);
           $msg = array('status' => true, 'message' => "Please ", 'result' => $response);
		}
		$this->response($msg, REST_Controller::HTTP_OK);
    }

    public function chat_notification_email()
    {        
        $postData = array_merge($_POST,json_decode(file_get_contents('php://input'),true));
        
         //send mail
         $block_status_check = $this->db->get_where('user_block',array('blocked_by' =>$postData['user_from'],'block_user' =>$postData['user_to']))->result();

        if(count($block_status_check)==0)
        {
            $this->crud_model->chat_notification($postData['user_from'],$postData['user_to'],$postData['message']);
        }
        $this->response($msg, REST_Controller::HTTP_OK);
    }

    // ======================================= archiveNow ===================================================

    public function archiveNow()
    {        
        $postData = array_merge($_POST,json_decode(file_get_contents('php://input'),true));
        //$postData['user_from'] = $this->user_data->id;
        if (empty($postData['user_from']) || empty($postData['user_to'])) {
			$message = (empty($postData['user_from']))?'user_from field required':'user_to field required';
            $msg = array('status' => false, 'message' => $message, 'result' => array());
        }else{
            $response = $this->Chat_model->archiveNow_modal( $postData['user_from'],$postData['user_to']);
            $msg = array('status' => true, 'message' => "Please ", 'result' => $response);
		}
		$this->response($msg, REST_Controller::HTTP_OK);
    }
    // ======================================= archiveNow ===================================================

    public function blockNow()
    {        
        $postData = array_merge($_POST,json_decode(file_get_contents('php://input'),true));
        //$postData['user_from'] = $this->user_data->id;
        if (empty($postData['user_from']) || empty($postData['user_to'])) {
            $message = (empty($postData['user_from']))?'user_from field required':'user_to field required';
            $msg = array('status' => false, 'message' => $message, 'result' => array());
        }
        else if($postData['action'] == 'add'){

            $response = $this->Chat_model->insertAll('user_block',['blocked_by'=>$postData['user_from'], 'block_user'=>$postData['user_to'],  'create_timestamp'=>date('Y-m-d H:i:s')]);

            $response2 = $this->Chat_model->updateArrayWhereResult('tbl_chat', ['is_blocked'=>1], '(`chat_from`= '.$postData['user_from'].' AND `chat_with` = '.$postData['user_to'].')');




            //$response = $this->Chat_model->blockNow_modal( $postData['user_from'],$postData['user_to']);
            $msg = array('status' => true, 'message' => "Please ", 'result' => $response);
        }
        else if($postData['action'] == 'delete'){

            $this->db->where ( '(`blocked_by` = "'.$postData['user_from'].'" AND `block_user`= "'.$postData['user_to'].'")' );
            $this->db->delete ( 'user_block' );

              $response2 = $this->Chat_model->updateArrayWhereResult('tbl_chat', ['is_blocked'=>0], '(`chat_from`= '.$postData['user_from'].' AND `chat_with` = '.$postData['user_to'].')');

            //$response = $this->Chat_model->blockNow_modal( $postData['user_from'],$postData['user_to']);
            $msg = array('status' => true, 'message' => "Please ", 'result' => $response);
        }
        $this->response($msg, REST_Controller::HTTP_OK);
    }
   
}
?>